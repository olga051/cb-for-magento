<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Block_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer {

	public function getProductThumbnail() {
		return $this->helper('catalog/image')->init($this->getProduct(), 'thumbnail');
	}

}