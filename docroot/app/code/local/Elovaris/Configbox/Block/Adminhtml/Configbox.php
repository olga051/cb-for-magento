<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Block_Adminhtml_Configbox extends Mage_Adminhtml_Block_Template {
	
	var $output;

	protected function _prepareLayout() {
		ob_start();
		require(KPATH_DIR_CB.'/configbox.php');
		$this->output = ob_get_clean();

		parent::_prepareLayout();
	}

	protected function _toHtml() {
		return $this->output;
	}

}