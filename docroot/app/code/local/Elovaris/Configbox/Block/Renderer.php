<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Block_Renderer extends Mage_Core_Block_Template {

	protected function prepare() {
		$model = Mage::getModel('configbox/prepare');
		$model->prepareConfigurator();
	}

}