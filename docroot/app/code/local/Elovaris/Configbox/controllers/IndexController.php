<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_IndexController extends Mage_Core_Controller_Front_Action {

	public function indexAction() {

		// Set all GET/POST parameters again - ungodly stuff, but hey!
		$params = $this->getRequest()->getParams();
		foreach ($params as $key=>$value) {
			$_REQUEST[$key] = $value;
		}

		if (isset($_REQUEST['format']) && $_REQUEST['format'] == 'raw') {
			require(KPATH_DIR_CB.'/configbox.php');
		}
		else {
			ob_start();
			require(KPATH_DIR_CB.'/configbox.php');
			$body = ob_get_clean();
			?>
			<html>
			<head>
			<base href="<?php echo KPATH_URL_BASE;?>/" />
			<meta http-equiv="content-type" content="text/html; charset=utf-8" />
			
			<?php if (!empty($GLOBALS['document']['stylesheets'])) { ?>
				<?php foreach ($GLOBALS['document']['stylesheets'] as $url=>$some) { ?>
					<link rel="stylesheet" href="<?php echo $url;?>" type="text/css" />
				<?php } ?>
			<?php } ?>
			
			<?php if (!empty($GLOBALS['document']['scripts'])) { ?>
				<?php foreach ($GLOBALS['document']['scripts'] as $url=>$some) { ?>
					<script type="text/javascript" src="<?php echo $url;?>"></script>
				<?php } ?>
			<?php } ?>
			
			<?php 
			if (!empty($GLOBALS['document']['script_codes'])) {
				foreach ($GLOBALS['document']['script_codes'] as $code) {
					echo $code;
				}
			}
			?>
			</head>
			
			<body>
			<?php echo $body;?>
			</body>
			</html>
			<?php
		}
			
		
	}

}