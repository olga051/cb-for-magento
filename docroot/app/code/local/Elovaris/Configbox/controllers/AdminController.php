<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_AdminController extends Mage_Adminhtml_Controller_Action {

	public function indexAction() {

		// Set all GET/POST parameters again - ungodly stuff, but hey!
		$params = $this->getRequest()->getParams();
		foreach ($params as $key=>$value) {
			$_REQUEST[$key] = $value;
		}

		// Set a default controller in case no controller or view is requested
		if (empty($_REQUEST['controller']) && empty($_REQUEST['view'])) {
			$_POST['controller'] = $_GET['controller'] = $_REQUEST['controller'] = 'admindashboard';
		}

		$getRawOutput = ((isset($_REQUEST['format']) && $_REQUEST['format'] == 'raw')) || !empty($_REQUEST['ajax_sub_view']);
		$getAsHtmlDoc = (!empty($_REQUEST['in_modal']) || (isset($_REQUEST['tmpl']) && $_REQUEST['tmpl'] == 'component'));

		if ($getRawOutput) {
			$this->loadLayout();
			$this->_addContent( $this->getLayout()->createBlock('configbox/adminhtml_configbox', 'configboxadmin') );
			$this->getLayout()->removeOutputBlock('root');
			$this->getLayout()->addOutputBlock('configboxadmin');
			$this->renderLayout();
		}
		elseif ($getAsHtmlDoc) {

			// Set the entry file var again, Magento knows only now if it's backend or not
			$js = "com_configbox.entryFile = '".KLink::getRoute('index.php?option=com_configbox')."';\n";
			KenedoPlatform::p()->addScriptDeclaration($js,true);

			ob_start();
			require(KPATH_DIR_CB.'/configbox.php');
			$body = ob_get_clean();
			?>
			<html>
			<head>
				<base href="<?php echo KPATH_SCHEME.'://'.KPATH_HOST;?>" />
				<meta http-equiv="content-type" content="text/html; charset=utf-8" />

				<?php if (!empty($GLOBALS['document']['stylesheets'])) { ?>
					<?php foreach ($GLOBALS['document']['stylesheets'] as $url=>$some) { ?>
						<link rel="stylesheet" href="<?php echo $url;?>" type="text/css" />
					<?php } ?>
				<?php } ?>

				<?php if (!empty($GLOBALS['document']['scripts'])) { ?>
					<?php foreach ($GLOBALS['document']['scripts'] as $url=>$some) { ?>
						<script type="text/javascript" src="<?php echo $url;?>"></script>
					<?php } ?>
				<?php } ?>

				<?php
				if (!empty($GLOBALS['document']['script_codes'])) {
					foreach ($GLOBALS['document']['script_codes'] as $code) {
						echo $code;
					}
				}
				?>
			</head>

			<body>
			<?php echo $body;?>
			</body>
			</html>
			<?php
		}
		else {

			// Set the entry file var again, Magento knows only now if it's backend or not
			$js = "com_configbox.entryFile = '".KLink::getRoute('index.php?option=com_configbox')."';\n";
			KenedoPlatform::p()->addScriptDeclaration($js,true);

			$this->loadLayout()->_addContent( $this->getLayout()->createBlock('configbox/adminhtml_configbox') );
			$this->renderLayout();
		}

	}

	protected function _isAllowed() {
		return Mage::getSingleton('admin/session')->isAllowed('admin/configbox');
	}

}