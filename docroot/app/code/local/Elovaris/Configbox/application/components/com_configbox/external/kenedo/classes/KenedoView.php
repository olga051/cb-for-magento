<?php
class KenedoView {

	/**
	 * Returns the default model for the view. Is overwritten in each sub class.
	 *
	 * @return KenedoModel
	 */
	function getDefaultModel() {
		return new KenedoModel($this->component);
	}

	/**
	 * @var string $component Name of the component dealing with tasks in this view (e.g. com_configbox). To be
	 * overridden in sub classes
	 */
	public $component = '';

	/**
	 * @var string $controllerName Name of the controller dealing with tasks in this view (e.g. admindashboard). To be
	 * overridden in sub classes
	 */
	public $controllerName = '';

	/**
	 * @var string $viewPath Absolute filesystem path to the view's class file. The value is set automatically in the
	 * constructor
	 * @see KenedoView::__construct(), KenedoView::getViewPath()
	 */
	public $viewPath = '';

	/**
	 * @var string $className PHP class name of the view (think sub classes). Is automatically set in the constructor.
	 */
	public $className = '';

	/**
	 * @var string $view Name of the view
	 * @see KenedoView::getViewNameFromClass()
	 */
	public $view = '';

	/**
	 * @var array $viewCssClasses Holds strings with CSS classes to add to the view's HTML wrapper
	 * @see KenedoView::addViewCssClasses(), KenedoView::renderViewCssClasses()
	 */
	public $viewCssClasses = array();

	/**
	 * @var array $pageTasks Holds information about the available tasks for this view
	 * @see KenedoView::display(), KenedoModel::getListingTasks(), KenedoModel::getDetailsTasks()
	 */
	public $pageTasks = array();

	/**
	 * @var string[] Array of HTML strings for filtering a listing
	 * @see KenedoView::getFilterInputs
	 */
	public $filterInputs;

	/**
	 * @var array $instances Holds all instances of KenedoView subclasses (think singleton)
	 * @see KenedoView::getView()
	 */
	static $instances = array();

	/**
	 * @var string Page title, shown on top of the page
	 * @see KenedoView::getPageTitle()
	 */
	public $pageTitle = '';

	/**
	 * @var object[] $records Used in listings. Holds an array of objects with data used in a kenedo listing
	 * @see KenedoModel::getRecords()
	 */
	public $records;

	/**
	 * @var object $record Used in forms. Holds the data object of the view's default model
	 * @see KenedoModel::getRecord()
	 */
	public $record;

	/**
	 * @var KenedoProperty[] $properties (Sub-classes of KenedoProperty) Properties of the model used in this view
	 * @see KenedoModel::getProperties()
	 */
	public $properties;

	/**
	 * Can be set from outside in order to display a view programatically with arbitrary filters
	 *
	 * @var string[] Key/value pairs, key is the filter name (table alias . column name), value is the chosen value
	 * @see KenedoView::getFiltersFromUpdatedState()
	 */
	public $filters = array();

	/**
	 * @var string[] An array of 2 strings, keys are propertyName and direction
	 * @see KenedoView::getOrderingFromUpdatedState()
	 */
	public $orderingInfo;

	/**
	 * @var int[] An array of 2 ints, keys are start and limit
	 * @see KenedoView::getPaginationFromUpdatedState()
	 */
	public $paginationInfo;

	/**
	 * @var string $pagination Ready-made HTML for pagination
	 * @see KenedoViewHelper::getListingPagination
	 */
	public $pagination;

	/**
	 * Used in listings. Key/value pairs which will be sent as POST data when the listing gets updated.
	 * Has things like
	 * @var string[]
	 */
	public $listingData;

	/**
	 * @var bool $listing Indicates that the view deals with a listing, not an edit form
	 * @see KenedoController::display(), KenedoController::edit()
	 */
	public $listing = false;

	/**
	 * @var bool $isAjaxView Indicates that the view is used as an ajaxView
	 * @see KenedoController::wrapViewAndDisplay()
	 */
	public $isAjaxView = false;

	/**
	 * @var bool $isIntralisting Indicates if the view displays an intralisting
	 * @see KenedoPropertyChildentries
	 */
	public $isIntralisting = false;

	/**
	 * @var string $foreignKeyField Used for add-buttons of intra-listings. Holds the foreign key field name for child tables
	 */
	public $foreignKeyField;

	/**
	 * @var string $foreignKeyPresetValue Used for add-buttons of intra-listings. Holds the init value for $foreignKeyField in forms.
	 */
	public $foreignKeyPresetValue;

	/**
	 * @var string URL used for redirection after saving in edit forms, cancel etc. GET/POST param 'return' or referrer is used typically
	 * @see KenedoView::__construct
	 */
	public $returnUrl = '';

	/**
	 * Name of the template (normally called default). Some views can an individual custom template defined (products,
	 * configurator pages, elements, possibly more in the future). Most templates will have the name in the ID attribute
	 * of a wrapping div for styling etc.
	 *
	 * @var string $template Name of the template
	 */
	public $template = '';

	/**
	 * Gets you a singleton object of a view
	 *
	 * @param string $className
	 * @param string $path
	 *
	 * @return KenedoView subclass of KenedoView
	 *
	 * @throws Exception if file is not found or class in file is not found
	 */
	static function getView($className, $path = NULL) {

		// MERGELECACY
		if ($path && strstr($path,'com_cbcheckout')) {
			$path = str_replace('components'.DS.'com_cbcheckout', 'components'.DS.'com_configbox', $path);
			KLog::logLegacyCall('Change second parameter $path from ..com_cbcheckout.. to ...com_configbox...');
		}

		// MERGELECACY
		if ($path && strstr($path,'views'.DS.'block_')) {
			$path = str_replace('views'.DS.'block_', 'views'.DS.'block', $path);
			KLog::logLegacyCall('Change second parameter $path from ..block_xxx.. to ...blockxxx...');
		}

		if (!isset(self::$instances[$className])) {

			if (!$path) {

				// Figure out the component name by class name
				$component = self::getComponentNameFromClass($className);

				// MERGELECACY
				if ($component == 'com_cbcheckout') {
					KLog::logLegacyCall('Change first parameter $className from CbcheckoutView... to ConfigboxView.. and also the class name in the custom view file in customization folder.');
					$component = 'com_configbox';
				}

				// Figure out the view name by class name
				$viewName = strtolower( substr($className, strpos($className, 'View') + 4 ) );

				// Prepare paths for both system and customization file location
				$regularPath 	= KenedoPlatform::p()->getComponentDir($component) .DS. 'views' .DS. strtolower($viewName) .DS. 'view.html.php';
				$customPath 	= CONFIGBOX_DIR_CUSTOMIZATION .DS. 'views' .DS. strtolower($viewName) .DS. 'view.html.php';

				// Overwrite $path with the right one based on existence
				if (is_file($regularPath)) {
					$path = $regularPath;
				}
				elseif (is_file($customPath)) {
					$path = $customPath;
				}

			}

			// Abort if the view file cannot be found
			if (is_file($path) == false) {
				$identifier = KLog::log('View file not found in expected path "'.$path.'" for class "'.$className.'".', 'error');
				throw new Exception('View file for view class "'.$className.'". Identifier: '.$identifier);
			}

			// Load the file
			require($path);

			// MERGELECACY
			$legacyClassName = str_replace('ConfigboxView', 'CbcheckoutView', $className);
			if (class_exists($className) == false && class_exists($legacyClassName)) {
				$className = $legacyClassName;
			}

			// Abort if the view class is not found in the file
			if (class_exists($className) == false) {
				$identifier = KLog::log('View class "'.$className.'" not found in file "'.$path.'".', 'error');
				throw new Exception('View class "'.$className.'" not found in the view file (File was found though). Identifier: '.$identifier);
			}

			// Finally store the object in instances
			self::$instances[$className] = new $className($className, $path);

		}

		return self::$instances[$className];

	}

	function __construct($className, $path) {

		$this->component 	= KenedoView::getComponentNameFromClass($className);
		$this->view 		= KenedoView::getViewNameFromClass($className);
		$this->className 	= $className;
		$this->viewPath		= $path;

		if (KRequest::getString('return')) {
			$returnUrl = KLink::base64UrlDecode( KRequest::getString('return'));
		}
		elseif (!empty($_SERVER['HTTP_REFERER'])) {
			$returnUrl = $_SERVER['HTTP_REFERER'];
		}
		else {
			$returnUrl = '';
		}

		$this->assign('returnUrl', $returnUrl);

	}

	static function getComponentNameFromClass($className) {
		return 'com_' . strtolower( substr($className, 0, strpos($className, 'View') ) );
	}

	static function getViewNameFromClass($className) {
		return strtolower( substr($className, strpos($className, 'View') + 4 ) );
	}

	function getPageTitle() {
		return '';
	}

	function display() {
		$this->prepareTemplateVars();
		$this->renderView();
	}

	function prepareTemplateVars() {

		$this->addViewCssClasses();

		if ($this->listing) {
			$this->prepareTemplateVarsList();
		}
		else {
			$this->prepareTemplateVarsForm();
		}

	}

	protected function prepareTemplateVarsList() {

		$model = $this->getDefaultModel();

		$this->assignRef('pageTitle', $this->getPageTitle());

		$filters = array_merge($this->getFiltersFromUpdatedState(), $this->filters);

		$paginationInfo = $this->getPaginationFromUpdatedState();
		$orderingInfo = $this->getOrderingFromUpdatedState();

		$records = $model->getRecords($filters, $paginationInfo, $orderingInfo);
		$properties = $model->getPropertiesForListing();

		$filterInputs = $this->getFilterInputs($filters);

		$this->assignRef('filterInputs', $filterInputs);
		$this->assignRef('orderingInfo', $orderingInfo);
		$this->assignRef('paginationInfo', $paginationInfo);
		$this->assignRef('records', $records);
		$this->assignRef('properties', $properties);
		$this->assignRef('filters', $filters);

		// Add pagination HTML
		$totalCount = count($model->getRecords($filters));
		$pagination = KenedoViewHelper::getListingPagination($totalCount, $paginationInfo);
		$this->assignRef('pagination', $pagination);

		$this->assignRef('pageTasks', $model->getListingTasks());

		if (empty($this->isIntralisting)) {
			$this->isIntralisting = false;
		}
		if (KRequest::getKeyword('intralisting')) {
			$this->isIntralisting = true;
		}

		$listingData = array(
			'base-url'				=> KLink::getRoute('index.php?option='.hsc($this->component).'&controller='.hsc($this->controllerName).'&lang='.hsc(KText::$languageCode)),
			'option'				=> hsc($this->component),
			'task'					=> 'display',
			'ajax_sub_view'			=> ($this->isAjaxSubview()) ? '1':'0',
			'tmpl'					=> hsc(KRequest::getKeyword('tmpl','component')),
			'in_modal'				=> hsc(KRequest::getInt('in_modal','0')),
			'intralisting'			=> $this->isIntralisting,
			'format'				=> 'raw',
			'groupKey'				=> hsc(KenedoViewHelper::getGroupingKey($this->properties)),
			'limitstart'			=> hsc($paginationInfo['start']),
			'limit'					=> hsc($paginationInfo['limit']),
			'listing_order_property_name'	=> hsc($orderingInfo['propertyName']),
			'listing_order_dir'				=> hsc($orderingInfo['direction']),
			'parampicker'			=> hsc(KRequest::getInt('parampicker',0)),
			'pickerobject'			=> hsc(KRequest::getKeyword('pickerobject','')),
			'pickermethod'			=> hsc(KRequest::getKeyword('pickermethod','')),
			'return'				=> KLink::base64UrlEncode( KLink::getRoute('index.php?option='.hsc($this->component).'&controller='.hsc($this->controllerName).'&lang='.hsc(KText::$languageCode), false) ),
			'ids'					=> '',
			'ordering-items'		=> '',
			'foreignKeyField'		=> KRequest::getKeyword('foreignKeyField', (!empty($this->foreignKeyField)) ? $this->foreignKeyField : ''),
			'foreignKeyPresetValue'	=> KRequest::getKeyword('foreignKeyPresetValue', (!empty($this->foreignKeyPresetValue)) ? $this->foreignKeyPresetValue : ''),
		);

		if ($this->isIntralisting) {

			// Override the href for for the add button
			$link = 'index.php?option='.hsc($this->component).'&controller='.hsc($this->controllerName).'&task=edit&id=0&in_modal=1&tmpl=component';
			if (!empty($this->foreignKeyField)) {
				$link .= '&'.$this->foreignKeyField.'='.$this->foreignKeyPresetValue;
			}
			if (KRequest::getKeyword('foreignKeyField')) {
				$link .= '&'.KRequest::getKeyword('foreignKeyField').'='.KRequest::getInt('foreignKeyPresetValue', '0');
			}
			$link .= '&return='.$this->listingData['return'];
			$listingData['add-link'] = KLink::base64UrlEncode( KLink::getRoute($link, false) );

		}
		else {

			// Prepare the href for for the add button
			$link = 'index.php?option='.hsc($this->component).'&controller='.hsc($this->controllerName).'&task=edit&id=0';

			if ($this->isInModal()) {
				$link.= '&in_modal=1';
			}

			if (!empty($this->foreignKeyField)) {
				$link .= '&'.$this->foreignKeyField.'='.$this->foreignKeyPresetValue;
			}
			if (KRequest::getKeyword('foreignKeyField')) {
				$link .= '&'.KRequest::getKeyword('foreignKeyField').'='.KRequest::getInt('foreignKeyPresetValue', '0');
			}
			$link .= '&return='.$listingData['return'];
		}

		$listingData['add-link'] = KLink::base64UrlEncode( KLink::getRoute($link, false) );

		$this->assignRef('listingData', $listingData);

	}

	protected function prepareTemplateVarsForm() {

		$id = KRequest::getInt('id');
		$model = $this->getDefaultModel();

		if ($id) {
			$record = $model->getRecord($id);
		}
		else {
			$record = $model->initData();
		}

		$this->assignRef('record', $record);
		$this->assignRef('recordUsage', $model->getRecordUsage($id));
		$this->assignRef('properties', $model->getProperties());

		if (!empty($this->record->title)) {
			$this->assignRef('pageTitle', $this->getPageTitle() . ': ' . $this->record->title);
		} elseif (!empty($this->record->name)) {
			$this->assignRef('pageTitle', $this->record->name);
		} else {
			$this->assignRef('pageTitle', $this->getPageTitle());
		}

		$this->assignRef('pageTasks', $model->getDetailsTasks());
	}

	function isAjaxSubview() {
		return $this->isAjaxView;
	}

	function isInModal() {
		return (KRequest::getInt('in_modal') == 1);
	}

	function addViewCssClasses() {
		$this->viewCssClasses[] = 'kenedo-view';
		if ($this->isAjaxSubview()) {
			$this->viewCssClasses[] = 'kenedo-ajax-sub-view';
		}
		$this->viewCssClasses[] = (KRequest::getInt('parampicker') == 1) ? 'param-picker':'no-parampicker';
		$this->viewCssClasses[] = ($this->isInModal()) ? 'in-modal':'in-tab';
		$this->viewCssClasses[] = 'platform-'.KenedoPlatform::getName();
		$this->viewCssClasses[] = ConfigboxVersionHelper::getIdForPlatformVersion();
		$this->viewCssClasses[] = ConfigboxDeviceHelper::getDeviceClasses();
		$this->viewCssClasses[] = KenedoPlatform::p()->isAdminArea() ? 'in-backend':'in-frontend';
	}

	function renderViewCssClasses() {
		echo hsc(implode(' ',$this->viewCssClasses));
	}

	function getViewOutput($template = NULL) {
		ob_start();
		$this->renderView($template);
		$content = ob_get_clean();
		return $content;
	}

	/**
	 * @param string|null $template Template name to use
	 */
	function renderView($template = NULL) {

		if ($template === NULL) {
			$template = KRequest::getKeyword('layout','default');
		}

		$template = str_replace(DS , '', $template);
		$template = str_replace('.', '', $template);

		$viewFolder = dirname($this->getViewPath());
		$viewName = strtolower(substr($viewFolder,strrpos($viewFolder, DS) + 1));

		$templatePaths['templateOverride'] 	= KenedoPlatform::p()->getTemplateOverridePath('com_configbox', $viewName, $template);
		$templatePaths['customTemplate'] 	= CONFIGBOX_DIR_CUSTOMIZATION .DS. 'templates' .DS. $viewName .DS. $template.'.php';
		$templatePaths['defaultTemplate'] 	= dirname($this->getViewPath()).DS.'tmpl'.DS.$template.'.php';

		$output = '';

		foreach ($templatePaths as $templatePath) {
			if (is_file($templatePath)) {
				ob_start();
				include($templatePath);
				$output = ob_get_clean();
				break;
			}
		}

		if ($output === false) {
			KLog::log('Template "'.$template.'" not found in "'.$templatePaths['defaultTemplate'].'" for view "'.get_class($this).'".', 'error', 'Template "'.$template.'" not found for view "'.get_class($this).'".');
		}

		echo $output;

	}

	function getViewPath() {
		return $this->viewPath;
	}

	function assign($key,$value) {
		$this->$key = $value;
	}

	function assignRef($key,$value) {
		$this->$key =& $value;
	}

	/**
	 * @return string[] Array with filter names as keys and chosen values as value
	 */
	function getFiltersFromUpdatedState() {

		$model = $this->getDefaultModel();

		$filterNames = $model->getFilterNames();

		$filters = array();

		foreach ($filterNames as $filterName) {
			$path = $this->view.'.'.$filterName;
			$requestName = 'filter_'. str_replace('.', '$', $filterName);
			$value = KenedoViewHelper::getUpdatedState($path, $requestName, '', 'string');
			if ($value == 'all' || $value === '0' || $value === '') {
				KenedoViewHelper::unsetState($path);
			}
			else {
				$filters[$filterName] = $value;
			}

		}

		return $filters;

	}

	function getPaginationFromUpdatedState() {

		// Remember the previous limit
		$prevLimit = KenedoViewHelper::getState(strtolower($this->view).'.listing_limit');

		// Get the info from updated state
		$paginationInfo = array(
			'start'=>KenedoViewHelper::getUpdatedState( strtolower($this->view).'.listing_start',	 	'limitstart', 	0,	'int'),
			'limit'=>KenedoViewHelper::getUpdatedState( strtolower($this->view).'.listing_limit', 		'limit', 		25,	'int'),
		);

		// Limit of 0 means show all, so set the start to 0 so we don't miss out on records
		if ($paginationInfo['limit'] == '0') {
			$paginationInfo['start'] = 0;
		}

		// If the limit (so the items per page) changed, reset the start
		if ($paginationInfo['limit'] != $prevLimit ) {
			$paginationInfo['start'] = 0;
		}

		return $paginationInfo;

	}

	function getOrderingFromUpdatedState() {
		return array(
			'propertyName'	=> KenedoViewHelper::getUpdatedState( strtolower($this->view).'.listing_order_property_name',	'listing_order_property_name', 	'ordering', 'string'),
			'direction'		=> KenedoViewHelper::getUpdatedState( strtolower($this->view).'.listing_order_dir',				'listing_order_dir', 			'ASC', 		'string'),
		);
	}

	/**
	 * @param string[] $filters array as from self::getFiltersFromUpdatedState
	 * @return string[] Array of HTML for each filter
	 * @see KenedoView::getFiltersFromUpdatedState
	 */
	function getFilterInputs($filters) {

		$model = $this->getDefaultModel();
		$props = $model->getProperties();

		$filterInputs = array();

		foreach ($props as $prop) {
			$input = $prop->getFilterInput($this, $filters);

			if (is_array($input)) {
				$filterInputs = array_merge($input, $filterInputs);
			}
			elseif(!empty($input)) {
				$name = $prop->getFilterName();
				$filterInputs[$name] = $input;
			}
		}

		return $filterInputs;

	}

}
