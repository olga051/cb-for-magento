<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewAdminpaymentmethod */
?>
<div class="payment-class-settings">
	<div class="payment-class-settings-heading"><?php echo KText::_('Payment service provider settings');?></div>
	<?php
				
	if (!empty($this->record->connector_name)) {
	
		$tag = KenedoPlatform::p()->getLanguageTag();
		
		$connectorFolder = ConfigboxPspHelper::getPspConnectorFolder($this->record->connector_name);
		
		$file = $connectorFolder.DS.'language'.DS. $tag . DS . $tag.'.ini';
		if (is_file($file)) {
			KText::load($file);
		}
		$settingsFile = $connectorFolder.DS.'settings.php';
		
		if (is_file($settingsFile) && strlen(trim(file_get_contents($settingsFile)))) {
			$this->settings = new KStorage($this->record->params);
			include($settingsFile);
		}
		else {
			echo KText::_('There are no specific settings for this payment service provider.');
		}
		
	}
	else {
		?>
		<p><?php echo KText::_('Please save your payment method to see payment service provider specific settings here.');?></p>
		<p><?php echo KText::_('Please note that your payment method is inactive by default until you activate it with the field called Active.');?></p>
		<?php
	}
	?>
</div>