<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */
?>
<label class="configbox-label configbox-label-text" for="element-widget-<?php echo (int)$this->element->id;?>"><?php echo hsc($this->element->title);?></label>

<?php if ($this->element->as_textarea) { ?>
	<textarea id="element-widget-<?php echo (int)$this->element->id;?>" cols="40" rows="3" class="configbox-widget configbox-widget-text" <?php echo (!$this->element->applies()) ? 'disabled="disabled"':'';?>><?php echo strip_tags($this->element->getRawValue());?></textarea>
<?php } else { ?>
	<input id="element-widget-<?php echo (int)$this->element->id;?>" class="configbox-widget configbox-widget-text" value="<?php echo strip_tags($this->element->getRawValue());?>" type="text" <?php echo (!$this->element->applies()) ? 'disabled="disabled"':'';?> />
<?php } ?>

<span class="element-unit"><?php echo hsc($this->element->unit);?></span>