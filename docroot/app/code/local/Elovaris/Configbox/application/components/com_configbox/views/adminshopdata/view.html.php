<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdminshopdata extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'adminshopdata';

	/**
	 * @return ConfigboxModelAdminshopdata
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdminshopdata');
	}

	function getPageTitle() {
		return KText::_('Store Information');
	}

	protected function prepareTemplateVarsForm() {

		$model = $this->getDefaultModel();
		$record = $model->getRecord(1);

		$this->assignRef('record', $record);
		$this->assign('recordUsage', array());
		$this->assignRef('properties', $model->getProperties());
		$this->assignRef('pageTitle', $this->getPageTitle());
		$this->assignRef('pageTasks', $model->getDetailsTasks());
	}

}
