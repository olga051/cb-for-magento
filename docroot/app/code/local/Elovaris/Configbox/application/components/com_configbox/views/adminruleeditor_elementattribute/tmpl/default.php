<?php
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this ConfigboxViewAdminRuleeditor_elementattribute
 */
?>

<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>">

	<div id="element-picker">

		<h2><?php echo KText::_('Pick an element');?></h2>

		<div class="product-filter">
			<?php echo $this->productFilterHtml;?>
		</div>

		<div class="page-filters">
			<?php foreach ($this->pageFilterDropdowns as $productId => $pageFilter) { ?>
				<div class="page-filter page-filter-<?php echo intval($productId);?>" style="display: <?php echo ($productId == $this->selectedProductId) ? 'block':'none';?>">
					<?php echo $pageFilter;?>
				</div>
			<?php } ?>
		</div>

		<input type="text" name="element-filter" id="element-filter" autocomplete="off" placeholder="<?php echo (CONFIGBOX_USE_INTERNAL_ELEMENT_NAMES) ? KText::_('Element internal name') : KText::_('Filter by Element title');?>" />

		<ul class="element-list">
			<?php
			foreach ($this->elements as $elementId => $element) {
				if ($element->product_id == $this->selectedProductId && ($this->selectedPageId == 0 || $element->page_id == $this->selectedPageId)) $displayCssClass = 'shown';
				else $displayCssClass = '';
				?>
				<li class="product-<?php echo $element->product_id;?> page-<?php echo $element->page_id;?> <?php echo $displayCssClass;?>" id="element-<?php echo $element->element_id;?>"><span><?php echo hsc($element->element_title);?></span></li>
				<?php
			}
			?>
		</ul>

	</div>

	<div id="element-attributes">

		<h2><?php echo KText::_('Drag conditions into the rule area');?></h2>

		<ul class="conditions-list">
			<?php foreach ($this->elements as $elementId => $element) { ?>

				<li class="xref-group" id="xref-group-<?php echo intval($elementId);?>">
					<ul class="conditions-list xref-list">

						<?php

						// Loop through the xref-selected conditions
						if (isset($this->elementXrefs[$elementId])) {

							foreach ($this->elementXrefs[$elementId] as $xrefId => $xref) {

								$conditionData = array(
									'type'=>'ElementAttribute',
									'elementId' => $elementId,
									'field' => 'selectedOption.id',
									'operator' => '==',
									'value' => $xref->xref_id,
								);

								echo '<li>';
								echo ConfigboxCondition::getCondition('ElementAttribute')->getConditionHtml($conditionData);
								echo '</li>';

							}

						}

						// Loop through the element's attributes that are usable for conditions
						foreach ($this->elementAttributes as $fieldPath => $elementAttribute) {

							// 'selected' is the text entry for free entry elements
							if ($element->xref_count > 0  && $fieldPath == 'selected') continue;
							// selectedOption.* is for elements with options
							if ($element->xref_count == 0 && strstr($fieldPath, 'selectedOption')) continue;

							$conditionData = array(
								'type'=>'ElementAttribute',
								'elementId' => $elementId,
								'field' => $fieldPath,
								'operator' => '==',
								'value' => '',
							);

							echo '<li>';
							echo ConfigboxCondition::getCondition('ElementAttribute')->getConditionHtml($conditionData);
							echo '</li>';

						}

						?>
					</ul>
				</li>
			<?php } ?>
		</ul>
	</div>

	<div class="notes">
		<h2><?php echo KText::_('Notes');?></h2>
		<p><?php echo KText::_('With the compatiblity rule editor you drag conditions into your rule and combine them with the combinators AND and OR.');?></p>
		<p><?php echo KText::_('Select an element from the left side and drag the condition of your choice into the rule.');?><b><?php echo KText::_('You can change the operator - e.g. is, is not, is above - by clicking on the word IS.');?></b></p>
		<p><?php echo KText::_('You can select multiple conditions in the rule by holding the shift key while clicking.');?></p>
		<p><?php echo KText::_('Select multiple conditions and click put in brackets to let these conditions be evaluated together. Removing a bracket does not remove its content.');?></p>
		<p><?php echo KText::_('Make sure each condition and bracket is combined with a combinator - AND or OR - no elements from other products are used and that all values are filled. ConfigBox cannot fully validate your rule, but you should easily spot errors in it.');?></p>
	</div>

</div>

