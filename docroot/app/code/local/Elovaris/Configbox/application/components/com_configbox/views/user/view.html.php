<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewUser extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'user';

	/**
	 * @var object Customer data record
	 * @see ConfigboxUserHelper::getUser
	 */
	public $customer;

	/**
	 * @var object[] Order records that belong to the current user
	 * @see ConfigboxModelOrderrecord::getOrderrecord
	 */
	public $orderRecords;

	/**
	 * @var boolean Indicates if the current user has a temporary account (hasn't been registered yet)
	 */
	public $isTemporaryAccount;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	/**
	 * @var string $customerFormHtml
	 * @see ConfigboxViewCustomerform
	 */
	public $customerFormHtml;

	/**
	 * @var string $urlCustomerAccount SEF-URL to customer account page
	 */
	public $urlCustomerAccount;

	/**
	 * @var string $urlPasswordReset The URL comes from the platform method
	 * @see InterfaceKenedoPlatform::getPasswordResetLink
	 */
	public $urlPasswordReset;

	function prepareTemplateVars() {

		if (ConfigboxUserHelper::getUserId() == 0) {
			$userId = ConfigboxUserHelper::createNewUser();
			ConfigboxUserHelper::setUserId($userId);
		}

		$view = KenedoView::getView('ConfigboxViewCustomerform');
		$view->formType = 'profile';
		$view->prepareTemplateVars();
		$formHtml = $view->getViewOutput('default');
		$this->assign('customerFormHtml', $formHtml);

		$urlCustomerAccount = KLink::getRoute('index.php?option=com_configbox&view=user');
		$this->assign('urlCustomerAccount', $urlCustomerAccount);

		$customer = ConfigboxUserHelper::getUser();
		$this->assignRef('customer', $customer);
		$this->assign('isTemporaryAccount', $customer->id == 0 || $customer->is_temporary == 1);

		$orderModel  	= KenedoModel::getModel('ConfigboxModelOrderrecord');
		$ordersModel  	= KenedoModel::getModel('ConfigboxModelAdminorders');

		$orderRecords = $ordersModel->getUserOrders();
		$orderStatuses = $orderModel->getOrderStatuses();
		$this->assignRef('orderStatuses', $orderStatuses);
		
		$orderPageStatuses = array(2,3,4,5,6,7,11,13,14);
		
		foreach ($orderRecords as $orderRecord) {
			$orderRecord->statusString = $orderStatuses[$orderRecord->status]->title;
			$orderRecord->toUserOrders = (in_array($orderRecord->status,$orderPageStatuses));
		}
		
		$this->assignRef('orderRecords',$orderRecords);

	}
}
