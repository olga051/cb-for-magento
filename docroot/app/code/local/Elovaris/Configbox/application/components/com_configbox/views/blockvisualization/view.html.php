<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewBlockvisualization extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var KStorage $params Joomla module parameters
	 */
	public $params;

	/**
	 * @var int Pixel width of the visualization image
	 */
	public $imageWidth;

	/**
	 * @var int Pixel height of the visualization image
	 */
	public $imageHeight;

	/**
	 * @var object[] Data of all images the visualization could contain
	 * @see ConfigboxProductImageHelper::getVisualizationImageSlots
	 */
	public $images;

	/**
	 * @var object Product data relevant for the product information
	 * @see ConfigboxProductImageHelper::getProductVisualizationInfo
	 */
	public $productInfo;

	/**
	 * @var int ID of the product that should be visualized
	 */
	public $productId;

	/**
	 * @var int ID of the current page
	 */
	public $pageId;

	/**
	 * @var string Absolute path to the visualization images
	 */
	public $baseDir;

	/**
	 * @var string Complete URL to the transparent pixel image
	 */
	public $blankImage;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {

		if (empty($this->params)) {
			$params = new KStorage();
			$this->assignRef('params', $params);
		}

		$configuration = ConfigboxConfiguration::getInstance();
		$productId = $configuration->getProductId();

		$this->assign('productId', $productId);
		$this->assign('pageId', KRequest::getInt('page_id'));

		$productInfo = ConfigboxProductImageHelper::getProductVisualizationInfo($productId);
		
		$this->assign('productInfo', $productInfo);
		$this->assign('imageWidth', intval($productInfo->opt_image_x) );
		$this->assign('imageHeight', intval($productInfo->opt_image_y) );
		$this->assign('images', ConfigboxProductImageHelper::getVisualizationImageSlots());
		
		$this->assign('blankImage', KPATH_URL_ASSETS.'/images/blank.gif');
		$this->assign('baseDir', CONFIGBOX_URL_VIS_PRODUCT_BASE_IMAGES);

	}

}
