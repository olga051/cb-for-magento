<?php 
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this KenedoPropertyDatetime
 */

$rand = rand(1,1000);
?>
<input type="text" class="datepicker class-rand-<?php echo intval($rand);?>" name="<?php echo hsc($this->propertyName);?>" id="<?php echo hsc($this->propertyName);?>" value="<?php echo hsc($this->data->{$this->propertyName});?>" />

<script type="text/javascript">
	// Check if we can get that done in Kenedo.js
	cbj(document).ready(function(){
		cbj('.class-rand-<?php echo intval($rand);?>').datepicker();
	});
</script>