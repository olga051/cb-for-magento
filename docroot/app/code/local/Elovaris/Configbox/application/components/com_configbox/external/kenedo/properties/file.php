<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyFile extends KenedoProperty {
	
	function delete($id, $tableName) {
		
		$filename = $this->getCurrentFilename($id, $tableName);
		
		if (!empty($filename)) {
			if (is_file($this->propertyDefinition['dirBase'].DS.$filename)) {
				unlink($this->propertyDefinition['dirBase'].DS.$filename);
			}
		}
		
	}
	
	function check($data) {
		
		$this->resetErrors();
		
		$file = KRequest::getFile($this->propertyName);
		
		if (!$file && $this->propertyDefinition['required'] && $data->id == 0) {
			$this->setError( KText::sprintf('You need to upload a file for field %s.',$this->propertyDefinition['label']) );
			return false;
		}

		if ($file && !empty($file['tmp_name'])) {
			
			// Check file size
			if (!empty($this->propertyDefinition['size'])) {
				
				$fileSizeInBytes = filesize($file['tmp_name']);
				$allowedSizeInBytes = $this->propertyDefinition['size'] * 1024;
				
				if ($fileSizeInBytes > $allowedSizeInBytes) {					
					$this->setError( KText::sprintf('The file for %s is not valid.',$this->propertyDefinition['label']). ' '.KText::sprintf('The file must not be bigger than %s KB.',$this->propertyDefinition['size']) );
					return false;
				}
				
			}
			
			// Check file mime-type
			if (!empty($this->propertyDefinition['allow'])) {
				
				$mimeType = KenedoFileHelper::getMimeType( $file['tmp_name'] );
				
				// Mime type determination may not be available on the system, only check if possible
				if ($mimeType && !in_array($mimeType,$this->propertyDefinition['allow'])) {
					$this->setError( KText::sprintf('The file for %s is not valid.',$this->propertyDefinition['label']). ' '.KText::sprintf('The file has an invalid MIME-Type. MIME-Type is %s. Allowed are %s.',$mimeType, implode(', ',$this->propertyDefinition['allow'])) );
					return false;
				}
				
			}
			
			if (!empty($this->propertyDefinition['allowedExtensions'])) {
				
				$extension = KenedoFileHelper::getExtension( $filename = $file['name'] );
				
				if (!in_array($extension,$this->propertyDefinition['allowedExtensions'])) {
					$this->setError( KText::sprintf('The file for %s is not valid.',$this->propertyDefinition['label']). ' '.KText::sprintf('The file has an invalid extension of %s. Allowed are %s.',$extension, implode(', ',$this->propertyDefinition['allowedExtensions'])) );
					return false;
				
				}
				
			}
			
		}
		
		return true;
		
	}
	
	protected function getNewFileName( $data ) {
		
		$file = KRequest::getFile($this->propertyName);
		
		if (!$file || empty($file['tmp_name'])) {
			return '';
		}
		
		// Prepare file's filename and extension
		$filenameRaw = mb_strtolower($file['name']);
		
		$fileExtension = KenedoFileHelper::getExtension($filenameRaw);
		
		$fileName = preg_replace('#\.[^.]*$#', '', $filenameRaw);
		
		// If option FILENAME_TO_RECORD_ID is set, prepare the full path with new filename
		if (isset($this->propertyDefinition['optionTags']['FILENAME_TO_RECORD_ID'])) {
			$newFilename = $data->id;
		}
		// Else use the sanitized filename
		elseif (!empty($this->propertyDefinition['filename'])) {
			$newFilename = $this->propertyDefinition['filename'];
		}
		else {
			$newFilename = $fileName;
		}
		
		if (!empty($this->propertyDefinition['appendSerial'])) {
			// Append a number to circumvent outdated cache data
			$newFilename .= '-'.str_pad(rand(1,1000),4,0);
		}
		
		return $newFilename.'.'.$fileExtension;
		
	}
	
	protected function getCurrentFilename($id, $tableName) {
		
		if (!$id) {
			return '';
		}
		
		$query = "SELECT `".$this->propertyName."` FROM `".$tableName."` WHERE `id` = ".(int)$id;
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$filename = $db->loadResult();
		
		return $filename;
		
	}
	
	function store(&$data) {

		$tableName = $this->model->getTableName();

		$storageFolder = $this->getPropertyDefinition('dirBase');
		
		$file = KRequest::getFile($this->propertyName);
		
		// With no upload, just get the old filename in the data record
		if (!$file or empty($file['tmp_name'])) {
			$currentFilename = $this->getCurrentFilename($data->id, $tableName);
			// Change data referenced data record to sneak the filename in
			$data->{$this->propertyName} = $currentFilename;
		}
		
		$canDelete = (!isset($this->propertyDefinition['optionTags']['NODELETEFILE']) && $this->isRequired() === false );
		
		if ($canDelete && KRequest::getInt($this->propertyName.'-delete',0) == 1) {
			$currentFilename = $this->getCurrentFilename($data->id, $tableName);
			
			$succ = unlink($storageFolder.DS.$currentFilename);
			if (!$succ && is_file( $storageFolder.DS.$currentFilename )) {
				$errorMsg = KText::sprintf('Could not remove the file for field %s in %s. Please check if your data folder and its content are writable and try again.',$this->propertyDefinition['label'],$storageFolder.DS.$currentFilename);
				$this->setError($errorMsg);
				
				$data->{$this->propertyName} = '';
			}
			else {
				$data->{$this->propertyName} = '';
			}
		}
		
		if ($file && !empty($file['tmp_name'])) {
						
			$currentFilename = $this->getCurrentFilename($data->id, $tableName);

			if ($this->getPropertyDefinition('keepFilename') == false || $currentFilename == '') {
				// Get the file name as intended
				$filename = $this->getNewFileName($data);
			}
			else {
				$filename = $currentFilename;
			}
			
			if (!is_dir($storageFolder)) {
				$succ = mkdir($storageFolder,0777,true);
				if (!$succ) {
					$errorMsg = KText::sprintf('Could not create the file folder for %s in %s. Please check if your data folder and its content are writable and try again.', $this->getPropertyDefinition('label'), $storageFolder);
					$this->setError($errorMsg);
					return false;
				}
			}
						
			$destination = $storageFolder.DS.$filename;
			
			$copySuccess = rename($file['tmp_name'],$destination);
			
			if ($copySuccess == false) {
				$errorMsg = KText::sprintf('Could not store file for field %s in %s. Please check if your data folder and its content are writable and try again.', $this->getPropertyDefinition('label'), $destination);
				$this->setError($errorMsg);
				return false;
			}
			
			chmod($destination,0755);
			// Change data referenced data record to sneak the new filename in
			if (isset($this->propertyDefinition['optionTags']['SAVE_FILENAME'])) {
				$data->{$this->propertyName} = $filename;
			}
			
			if ($this->getPropertyDefinition('appendSerial') && $currentFilename && is_file($storageFolder.DS.$currentFilename)) {
				$succ = unlink($storageFolder.DS.$currentFilename);
				if (!$succ) {
					$errorMsg = KText::sprintf('Could not remove the previous file for field %s in %s. Please check if your data folder and its content are writable and try again.', $this->getPropertyDefinition('label'), $storageFolder.DS.$currentFilename);
					$this->setError($errorMsg);
					return false;
				}
			}
			
			
		}
		
		return true;
		
	}
	
}