<?php
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this ConfigboxViewAdmincalcformula_elementattribute
 */
?>

<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>">

	<div id="element-picker">

		<h2><?php echo KText::_('Pick an element');?></h2>

		<div class="product-filter">
			<?php echo $this->productFilterHtml;?>
		</div>

		<div class="page-filters">
			<?php foreach ($this->pageFilterDropdowns as $productId => $pageFilter) { ?>
				<div class="page-filter page-filter-<?php echo intval($productId);?>" style="display: <?php echo ($productId == $this->selectedProductId) ? 'block':'none';?>">
					<?php echo $pageFilter;?>
				</div>
			<?php } ?>
		</div>

		<input type="text" name="element-filter" id="element-filter" autocomplete="off" placeholder="<?php echo (CONFIGBOX_USE_INTERNAL_ELEMENT_NAMES) ? KText::_('Element internal name') : KText::_('Filter by Element title');?>" />

		<ul class="element-list">
			<?php
			foreach ($this->elements as $elementId => $element) {
				if ($element->product_id == $this->selectedProductId && ($this->selectedPageId == 0 || $element->page_id == $this->selectedPageId)) $displayCssClass = 'shown';
				else $displayCssClass = '';
				?>
				<li class="product-<?php echo $element->product_id;?> page-<?php echo $element->page_id;?> <?php echo $displayCssClass;?>" id="element-<?php echo $element->element_id;?>"><span><?php echo hsc($element->element_title);?></span></li>
				<?php
			}
			?>
		</ul>

	</div>

	<div id="element-attributes">

		<h2><?php echo KText::_('Drag conditions into the rule area');?></h2>

		<ul class="conditions-list">
			<?php foreach ($this->elements as $elementId => $element) { ?>

				<li class="xref-group" id="xref-group-<?php echo intval($elementId);?>">
					<ul class="conditions-list xref-list">

						<?php

						// Loop through the element's attributes that are usable for conditions
						foreach ($this->elementAttributes as $fieldPath => $elementAttribute) {

							// 'selected' is the text entry for free entry elements
							if ($element->xref_count > 0  && $fieldPath == 'selected') continue;
							// selectedOption.* is for elements with options
							if ($element->xref_count == 0 && strstr($fieldPath, 'selectedOption')) continue;

							$termData = array(
								'type'=>'ElementAttribute',
								'elementId' => $elementId,
								'fieldPath' => $fieldPath,
								'fallbackValue' => '',
							);

							echo '<li>';
							echo ConfigboxCalcTerm::getTerm('ElementAttribute')->getTermHtml($termData);
							echo '</li>';

						}

						?>
					</ul>
				</li>
			<?php } ?>
		</ul>
	</div>
</div>

