<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdmincustomer extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'admincustomers';

	/**
	 * @var string $customerFormHtml
	 * @see ConfigboxViewCustomerform::getViewOutput
	 */
	public $customerFormHtml;

	/**
	 * @return ConfigboxModelAdmincustomers
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdmincustomers');
	}

	function getPageTitle() {
		return KText::_('Customer');
	}

	function prepareTemplateVarsForm() {

		$id = KRequest::getInt('id');
		$model = $this->getDefaultModel();

		if ($id) {
			$record = $model->getRecord($id);
		}
		else {
			$record = $model->initData();
		}

		$view = KenedoView::getView('ConfigboxViewCustomerform');
		$view->customerFields = ConfigboxUserHelper::getUserFields($record->group_id);
		$view->customerData = $record;
		$view->formType = 'profile';
		$view->useLoginForm = false;
		$view->prepareTemplateVars();
		$formHtml = $view->getViewOutput();

		$this->assign('customerFormHtml', $formHtml);

		$this->assignRef('recordUsage', $model->getRecordUsage($id));

		if (!empty($this->record->title)) {
			$this->assignRef('pageTitle', $this->getPageTitle() . ': ' . $this->record->title);
		} elseif (!empty($this->record->name)) {
			$this->assignRef('pageTitle', $this->record->name);
		} else {
			$this->assignRef('pageTitle', $this->getPageTitle());
		}

		$this->assignRef('pageTasks', $model->getDetailsTasks());

	}

}
