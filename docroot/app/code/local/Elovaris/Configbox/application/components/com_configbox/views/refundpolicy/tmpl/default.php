<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewRefundpolicy */
?>

<div id="com_configbox">
<div id="view-refundpolicy">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

<?php if (KRequest::getKeyword('tmpl') == 'component') { ?>
	<div style="margin:10px">
<?php } ?>

<h2 class="componentheading"><?php echo hsc(KText::_('Refund Policy'));?></h2>

<?php echo $this->refundPolicy;?>

<?php if (KRequest::getKeyword('tmpl') == 'component') { ?>
	</div>
<?php } ?>

</div>
</div>
</div>