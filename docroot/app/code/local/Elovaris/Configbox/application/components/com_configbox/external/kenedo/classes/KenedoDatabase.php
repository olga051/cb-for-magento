<?php
defined('CB_VALID_ENTRY') or die();

class KenedoDatabase {
	
	protected $db;
	
	protected $queryCount = 0;

	protected $failedQueryCount = 0;
	protected $failedQueryLog = array();

	protected $queryList = array();
	
	protected $link;
	protected $start = 0;
	protected $limit = NULL;
	
	protected $query;
	protected $result;

	protected $hostname;
	protected $username;
	protected $password;
	protected $database;
	protected $prefix;
	protected $port;
	protected $socket;
	
	public function __construct() {
		
		$this->createDatabaseLink();
		mysqli_set_charset($this->link, "utf8");
		return $this;
	
	}
	
	function __destruct() {
		if (is_a($this->link , 'mysqli') || is_resource($this->link)) {
			mysqli_close($this->link);
		}
	}
	
	protected function createDatabaseLink() {

		$connectionData = KenedoPlatform::p()->getDbConnectionData();
		
		$this->hostname = $connectionData->hostname;
		$this->username = $connectionData->username;
		$this->password = $connectionData->password;
		$this->database = $connectionData->database;
		$this->prefix 	= $connectionData->prefix;
		
		$this->port = 3306;
		$this->socket = NULL;
		
		if (strpos($this->hostname,':') !== false) {
			$ex = explode(':',$this->hostname);
			$this->hostname = $ex[0];
			if (is_numeric($ex[1])) {
				$this->port = $ex[1];
			}
			else {
				$this->socket = $ex[1];
			}
		}
		
		$this->link = mysqli_connect( $this->hostname, $this->username, $this->password, $this->database, $this->port, $this->socket );
		
		if ($this->link == false) {
			$internalLogMessage = 'Could not establish a connection to the database. Connection error message is "'.mysqli_connect_error().'".';
			KLog::log($internalLogMessage,'db_error');
			KLog::log($internalLogMessage,'error', 'Could not establish a connection to the database. Check the configbox error log file for more information.');
			return false;
		}
		
		$query = "SET SESSION sql_mode = '';";
		$this->setQuery($query);
		$this->query();

		return true;

	}

	public function getQueryCount() {
		return $this->queryCount;
	}
	
	public function getTotalQueryTime() {
		$t = 0;
		foreach ($this->queryList as $caller=>$items) {
			foreach ($items as $item) {
				$t += $item['time'];
			}
		}
		return $t;
	}
	
	public function getQueryList() {
		$queryList = array();
		foreach ($this->queryList as $caller=>$items) {
			$t = 0;
			foreach ($items as $item) {
				$t += $item['time'];
			}
			$key = $t.'-'.count($items).'-'.$caller;
			$queryList[$key] = $items;
		}
		return $queryList;
	}
	
	public function getPrefix() {
		return $this->prefix;
	}
	
	public function getEscaped($text) {
		return mysqli_real_escape_string($this->link, $text);
	}
	
	public function getQuoted($text) {
		return '`'.$text.'`';
	}
	
	public function getErrorNum() {
		return mysqli_errno($this->link);
	}
	
	public function getErrorMsg() {
		return mysqli_error($this->link);
	}
	
	public function splitSql($string) {
		return explode("\n",$string);
	}
	
	public function setQuery($query, $start = 0, $limit = 0) {

		$this->query = $query;

		if ($start > 0 || $limit > 0) {
			$limiter = ' LIMIT ' . max($start, 0) . ', ' . max($limit, 0);
			$this->query .= $limiter;
		}

	}
	
	public function getQuery() {
		return $this->query;
	}

	public function resetFailedQueryCount() {
		$this->failedQueryCount = 0;
	}

	public function resetFailedQueryLog() {
		$this->failedQueryLog = array();
	}

	public function getFailedQueryCount() {
		return $this->failedQueryCount;
	}

	public function getFailedQueryLog() {
		return $this->failedQueryLog;
	}
	
	public function query() {

		// Return false if there is no query
		if (!$this->query) {
			return false;
		}

		// Replace the table prefix
		$query = str_replace('#__', $this->prefix, $this->query);

		// Increment the query count
		$this->queryCount++;

		// Do the query
		$this->result = mysqli_query($this->link, $query, MYSQLI_STORE_RESULT);

		// Log failed queries including the caller
		if ($this->result === false) {

			// Get the backtrace
			$stack = debug_backtrace(false);

			// Figure out which call started the query. Go into the stack and find the first entry that is outside this class
			$i = -1;
			foreach ($stack as $line) {
				if (empty($line['class']) || $line['class'] != __CLASS__ ) {
					break;
				}
				$i++;
			}

			// Prepare the caller info for the log entry
			$callerInfo = array(
				'file' => !empty($stack[$i]['file']) ? str_replace(KPATH_ROOT,'KPATH_ROOT',$stack[$i]['file']) : 'File unkown',
				'line' => !empty($stack[$i]['line']) ? $stack[$i]['line'] : 'Line unkown',
				'class' => !empty($stack[$i+1]['class']) ? $stack[$i+1]['class'] : 'No class',
				'function' => !empty($stack[$i+1]['function']) ? $stack[$i+1]['function'] : 'No function',
			);

			$data = array(
				'caller_file'=>$callerInfo['file'],
				'caller_line'=>$callerInfo['line'],
				'caller_class'=>$callerInfo['class'],
				'caller_function'=>$callerInfo['function'],
				'query'=>$query,
				'error_num'=>$this->getErrorNum(),
				'error_msg'=>$this->getErrorMsg(),
			);

			// Prepare the log entry
			$logEntry = $data['caller_class'].'::'.$data['caller_function'].'(), File '.$data['caller_file'].' on line: '.$data['caller_line'].', Error num: "'.$data['error_num'].'", error msg: "'.$data['error_msg'].'", query: "'.$data['query'].'"';

			// Increment failed query count
			$this->failedQueryCount++;

			// Add to the failed query log
			$this->failedQueryLog[] = $data;

			KLog::log($logEntry, 'db_error');

		}
		
		return $this->result;
	}
	
	public function getAffectedRows() {
		return mysqli_affected_rows($this->result);
	}
	
	public function getReturnedRows() {
		return mysqli_num_rows($this->result);
	}
	
	protected function addQueryListItem($startTime) {
		if (defined('CONFIGBOX_ENABLE_PERFORMANCE_TRACKING') && CONFIGBOX_ENABLE_PERFORMANCE_TRACKING) {
			$timing['time'] = sprintf("%F", (microtime(true) - $startTime) * 1000);
			$timing['query'] = $this->getQuery();
		
			$stack = debug_backtrace(false);
			$info['class'] = isset($stack[2]['class']) ? $stack[2]['class'] : NULL;
			$info['method'] = isset($stack[2]['function']) ? $stack[2]['function'] : NULL;
			$info['line'] = isset($stack[1]['line']) ? $stack[1]['line'] : NULL;
			$info['file'] = isset($stack[1]['file']) ? $stack[1]['file'] : NULL;
			
			$timing['caller'] = $info['class'].':'.$info['method']. ' Line:'.$info['line'];
			
			$this->queryList[$timing['caller']][] = $timing;
		}
	}

	/**
	 * @return null|string
	 */
	public function loadResult() {

		$start = microtime(true);
		
		if (!($res = $this->query())) {
			return null;
		}
		
		$ret = null;
		if ($row = mysqli_fetch_row( $res )) {
			$ret = $row[0];
		}
		mysqli_free_result( $res );
		
		$this->addQueryListItem($start);
		return $ret;
		
	}

	/**
	 * Gets you a flat array with the key as in $keyField and value as in $valueField
	 * You can omit both params, then you get a flat array with the first field coming out of the query
	 * @param string $indexField The column for writing the list's array keys. No key field gets you a numeric array.
	 * @param string $valueField The column for the list's values. No value field gets you the first column.
	 * @return null|string[]|int[]
	 */
	public function loadResultList($indexField = NULL, $valueField = NULL) {

		$start = microtime(true);

		if (!($res = $this->query())) {
			return null;
		}
		$array = array();

		if (!empty($indexField)) {
			while ($row = mysqli_fetch_assoc($res)) {
				if ($valueField !== null) {
					$array[$row[$indexField]] = $row[$valueField];
				} else {
					$array[$row[$indexField]] = reset($row);
				}
			}
		}
		else {
			while ($row = mysqli_fetch_row($res)) {
				if (!empty($valueField)) {
					$array[] = $row[$valueField];
				} else {
					$array[] = $row[0];
				}
			}
		}

		mysqli_free_result($res);

		$this->addQueryListItem($start);
		return $array;
	}

	/**
	 * @return null|string[]
	 */
	public function loadAssoc() {
		
		$start = microtime(true);
		
		if (!($res = $this->query())) {
			return null;
		}
		$ret = null;
				
		if ($array = mysqli_fetch_assoc( $res )) {
			$ret = $array;
		}
		mysqli_free_result( $res );
		
		$this->addQueryListItem($start);
		return $ret;
	}

	/**
	 * @param string $indexField The column for writing the list's array keys. No key field gets you a numeric array.
	 * @return null|array[]
	 */
	public function loadAssocList($indexField = '') {
		
		$start = microtime(true);
		
		if (!($res = $this->query())) {
			return null;
		}
		$array = array();
		
		while ($row = mysqli_fetch_assoc( $res )) {
			if ($indexField) {
				$array[$row[$indexField]] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysqli_free_result( $res );
		
		$this->addQueryListItem($start);
		return $array;
	}

	/**
	 * @return null|object
	 */
	public function loadObject() {
		
		$start = microtime(true);
		
		if (!($res = $this->query())) {
			return null;
		}
		$ret = null;
		
		if ($object = mysqli_fetch_object( $res, 'KenedoObject' )) {
			$ret = $object;
		}
		mysqli_free_result( $res );
		
		$this->addQueryListItem($start);
		return $ret;
	}

	/**
	 * @param string $indexField The column for writing the list's array keys. No key field gets you a numeric array.
	 * @return null|object[]
	 */
	public function loadObjectList($indexField = '') {
		
		$start = microtime(true);
		
		if (!($res = $this->query())) {
			return null;
		}
		$array = array();
		
		while ($row = mysqli_fetch_object( $res , 'KenedoObject')) {
			if ($indexField) {
				$array[$row->$indexField] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysqli_free_result( $res );
		
		$this->addQueryListItem($start);
		return $array;
	}

	/**
	 * @return null|string[]
	 */
	public function loadRow() {
		
		$start = microtime(true);
		
		if (!($res = $this->query())) {
			return null;
		}
		$ret = null;
		
		if ($row = mysqli_fetch_row( $res )) {
			$ret = $row;
		}
		mysqli_free_result( $res );
		
		$this->addQueryListItem($start);
		return $ret;
	}

	/**
	 * @param string $indexField The column for writing the list's array keys. No key field gets you a numeric array.
	 * @return null|string[]
	 */
	public function loadRowList($indexField = '') {
		
		$start = microtime(true);
		
		if ($indexField == '') $indexField = NULL;
		
		if (!($res = $this->query())) {
			return null;
		}
		$array = array();
		
		while ($row = mysqli_fetch_row( $res )) {
			if ($indexField !== null) {
				$array[$row[$indexField]] = $row;
			} else {
				$array[] = $row;
			}
		}
		mysqli_free_result( $res );
		
		$this->addQueryListItem($start);
		return $array;
	}
	
	public function replaceObject($table, $object) {
	
		$start = microtime(true);
	
		$fmtsql = "REPLACE INTO `".$table."` ( %s ) VALUES ( %s ) ";
		$fields = array();
		$values = array();
		foreach (get_object_vars( $object ) as $k => $v) {
			if (is_array($v) or is_object($v) or $v === NULL) {
				continue;
			}
			if ($k[0] == '_') {
				continue;
			}
			$fields[] = $this->getQuoted( $k );
			$values[] = "'".$this->getEscaped($v)."'";
		}
		$query = sprintf( $fmtsql, implode( ",", $fields ) ,  implode( ",", $values ) );
		$this->setQuery($query);
		if (!$this->query()) {
			return false;
		}

		$this->addQueryListItem($start);
		return true;
	}
	
	public function insertObject($table, &$object, $keyName = NULL) {
		
		$start = microtime(true);
		
		$query = 'REPLACE INTO `'.$table.'` ( %s ) VALUES ( %s ) ';
		$fields = array();
		$values = array();
		foreach (get_object_vars( $object ) as $k => $v) {
			if (is_array($v) or is_object($v) or $v === NULL) {
				continue;
			}
			if ($k[0] == '_') {
				continue;
			}
			$fields[] = $this->getQuoted( $k );
			$values[] = "'".$this->getEscaped($v)."'";
		}
		$query = sprintf( $query, implode( ",", $fields ) ,  implode( ",", $values ) );
		$this->setQuery($query);
		if (!$this->query()) {
			return false;
		}
		$insertId = $this->insertid();
		if ($keyName && $insertId) {
			$object->$keyName = $insertId;
		}
		
		$this->addQueryListItem($start);
		return true;
	}
	
	public function updateObject($table, &$object, $keyName = NULL, $updateNulls = true) {
		
		$start = microtime(true);
		
		$query = 'REPLACE INTO `'.$table.'` SET %s , %s';
		$fields = array();
		$where = '';
		foreach (get_object_vars( $object ) as $k => $v) {
			if( is_array($v) or is_object($v) or $k[0] == '_' ) {
				continue;
			}
			if( $k == $keyName ) {
				$where = $keyName . '=' ."'". $this->getEscaped( $v )."'";
				continue;
			}
			if ($v === null)
			{
				if ($updateNulls) {
					$val = 'NULL';
				} else {
					continue;
				}
			} else {
				$val = "'".$this->getEscaped($v)."'";
			}
			$fields[] = $this->getQuoted( $k ) . '=' . $val;
		}
		$query = sprintf($query, implode(',', $fields), $where);
		$this->setQuery($query);
		
		$succ = $this->query();
		$this->addQueryListItem($start);
		return $succ;
	}
	
	public function insertid() {
		return mysqli_insert_id( $this->link );
	}
	
	public function getTableFields($tables, $typeOnly = true) {
		
		$start = microtime(true);
		
		settype($tables, 'array');
		$result = array();
		
		foreach ($tables as $tblval) {
			$query = 'SHOW FIELDS FROM ' . $tblval;
			$this->setQuery($query);
			$fields = $this->loadObjectList();

			if($typeOnly) {
				foreach ($fields as $field) {
					$result[$tblval][$field->Field] = preg_replace("/[(0-9)]/",'', $field->Type );
				}
			}
			else {
				foreach ($fields as $field) {
					$result[$tblval][$field->Field] = $field;
				}
			}
		}
		$this->addQueryListItem($start);
		return $result;
	}
	
}

