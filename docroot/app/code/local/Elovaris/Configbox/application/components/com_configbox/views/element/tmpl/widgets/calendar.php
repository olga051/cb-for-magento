<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */
?>

<a class="configbox-widget-calendar-button"><span><?php echo KText::_('Change date');?></span></a>

<label for="element-widget-<?php echo (int)$this->element->id;?>" style="display:none"><?php echo KText::_('Date');?></label>
<input id="element-widget-<?php echo (int)$this->element->id;?>" class="configbox-widget configbox-widget-calendar" type="hidden" readonly="readonly" />

<label for="configbox-widget-display-<?php echo (int)$this->element->id;?>" style="display:none"><?php echo KText::_('Date Display');?></label>
<input id="configbox-widget-display-<?php echo (int)$this->element->id;?>" class="configbox-widget-calendar-display" value="<?php echo ($this->element->getOutputValue()) ? $this->element->getOutputValue() : KText::_('No date selected');?>" type="text" readonly="readonly" />