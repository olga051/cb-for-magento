<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyString extends KenedoProperty {

	function getDataFromRequest(&$data) {

		parent::getDataFromRequest($data);
	
		$stringType = $this->getPropertyDefinition('stringType', 'string');

		// In case we got a number, we change the localized decimal symbol to the normalized dot
		// BE AWARE: type 'time' is actually meant as 'duration', it's a number in days/hours/minutes and stored in seconds
		if ($stringType == 'number' or $stringType == 'price' or $stringType == 'time') {
			$data->{$this->propertyName} = str_replace(KText::$decimalSymbol, '.', $data->{$this->propertyName});
		}

		// Convert durations to seconds, depending on what's set in the field data
		if ($stringType == 'time') {
	
			if (!empty($this->propertyDefinition['displayin'])) {

				switch($this->propertyDefinition['displayin']) {
					case 'days':
						$data->{$this->propertyName} *= 86400;
						break;
					case 'hours':
						$data->{$this->propertyName} *= 3600;
						break;
					case 'minutes':
						$data->{$this->propertyName} *= 60;
						break;
				}
			}
			
		}
		
		return true;
	
	}

	function renderListingField($item, $items) {
		
		$value = $item->{$this->propertyName};

		$stringType = $this->getPropertyDefinition('stringType', 'string');
			
		if ($stringType == 'time') {
			
			if (!empty($this->propertyDefinition['displayin'])) {
				switch($this->propertyDefinition['displayin']) {
					case 'days':
						$value /= 86400;
						break;
					case 'hours':
						$value /= 3600;
						break;
					case 'minutes':
						$value /= 60;
						break;
				}
			}
				
		}

		if ($stringType == 'number' or $stringType == 'time') {
			$value = str_replace(KText::$decimalSymbol, '.', $value);
		}
		
		if ($stringType == 'price') {
			$value = cbprice($value);
		}
		else {
			$value = $value . $this->getPropertyDefinition('unit', '');
		}

		// Wrap the whole thing for prices to have them nicely right-aligned and non-breaking
		if ($stringType == 'price') {
			echo '<div style="text-align:right;white-space:nowrap;">';
		}

		if ($this->getPropertyDefinition('listinglink')) {

			$option = $this->getPropertyDefinition('component');
			$controller = $this->getPropertyDefinition('controller');

			$returnUrl = KLink::base64UrlEncode(KLink::getRoute( 'index.php?option='.hsc($option).'&controller='.hsc($controller), false ));
			$href = KLink::getRoute('index.php?option='.hsc($option).'&controller='.hsc($controller).'&task=edit&id='.intval($item->id).'&return='.$returnUrl);

			?>
			<a class="listing-link" href="<?php echo $href;?>"><?php echo hsc($value);?></a>
			<?php
		}
		else {
			echo hsc($value);
		}
		
		if ($stringType == 'price') {
			echo '</div>';
		}
		
	}
	
	function getJoinQuerySelectsAndJoins($selectAlias = '', $foreignField,  $joinField = '', $propNameDisplay, $tableName) {
	
		if (!$selectAlias) {
			$selectAlias = $this->propertyName;
		}
	
		if (!$joinField) {
			$joinField = 'a.id';
		}
	
		$joinAlias = 'string_'.$selectAlias;
	
		$response['joins'][] = "LEFT JOIN `".$tableName.'` AS `'.$joinAlias.'` ON `'.$joinAlias.'`.`'.$joinField.'` = `a`.`'.$foreignField.'`';
		$response['selects'][] = '`'.$joinAlias.'`.`'.$propNameDisplay.'` AS `'.$selectAlias.'`';
		
		return $response;
	}
	
}