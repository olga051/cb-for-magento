<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxCalcTermOperator extends ConfigboxCalcTerm {

	function containsElementId($termData, $elementId) {
		return false;
	}

	function containsXrefId($termData, $xrefId) {
		return false;
	}

	/**
	 * Called by ConfigboxRulesHelper::getTermsCode to get the term result
	 *
	 * @param string[] $termData
	 * @param array[] $selections
	 * @param int|NULL $regardingElementId The Element ID to which the calculation is assigned to
	 * @param int|NULL $regardingXrefId The option assignment ID to which the calculation is assigned to
	 * @param boolean $allowNonNumeric If the result can be non-numeric
	 * @return float The calculated result
	 *
	 * @see ConfigboxCalculation::getTermsCode, ConfigboxCalculation::getTerms, ConfigboxCalculation::getSelections
	 */
	function getTermResult($termData, $selections, $regardingElementId = NULL, $regardingXrefId = NULL, $allowNonNumeric = false) {
		return $termData['value'];
	}

	function getTermsPanelHtml() {
		return '';
	}

	/**
	 * Called by ConfigboxCalculation::getTermHtml to display the term (either for editing or display)
	 *
	 * @param string[] $termData
	 * @param bool $forEditing If edit controls or plain display should come out
	 * @return string HTML for that term
	 * @see ConfigboxCalculation::getTermHtml
	 */
	function getTermHtml($termData, $forEditing = true) {
		ob_start();
		?>
		<span class="item operator"
			  data-type="operator"
			  data-value="<?php echo hsc($termData['value']);?>">
			<?php echo hsc($termData['value']);?>
		</span>
		<?php
		return ob_get_clean();
	}

	function getTypeTitle() {
		return KText::_('Calculations');
	}

}