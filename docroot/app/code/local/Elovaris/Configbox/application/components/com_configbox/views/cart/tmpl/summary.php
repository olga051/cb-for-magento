<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCart */
?>

<?php if ($this->displayPricing) { ?>

	<?php
	// Helper variables to make the code below better to read
	$b2b = ($this->cart->groupData->b2b_mode == 1);
	$taxIncEx = ($b2b) ? 'excl.':'incl.';
	$taxPlusInc = ($b2b) ? 'Plus':'Incl.';
	?>

	<div class="cart-summary cart-summary-<?php echo ($b2b) ? 'b2b':'b2c';?>">
		<table>
			<thead>

			<?php // Headings ?>

			<tr>
				<th class="item-name"><?php echo KText::_('Item');?></th>
				<?php if ($this->cart->usesRecurring) { ?>
					<th class="item-price item-price-recurring"><?php echo hsc($this->cart->labelRecurring);?></th>
				<?php } ?>
				<th class="item-price"><?php echo hsc($this->cart->labelRegular);?></th>
			</tr>

			</thead>

			<tbody>

			<?php // Each position ?>

			<?php foreach ($this->cart->positions as $positionId=>$position) { ?>
				<tr class="position-total">
					<td class="item-name">
						<span class="product-title"><?php echo intval($position->quantity);?> x <?php echo hsc( $position->productTitle );?></span>
						<?php if ($position->quantity > 1) { ?>
							<span class="product-item-price"><?php echo '('.KText::sprintf('Price per item %s', cbprice( ( ($b2b) ? $position->totalUnreducedNet : $position->totalUnreducedGross ) / $position->quantity ) ).')';?></span>
						<?php } ?>
						<span class="product-taxrate"><?php echo KText::sprintf(($position->totalUnreducedRecurringNet && $position->productData->taxRate != $position->productData->taxRateRecurring) ? '('.$taxIncEx.' %s regular, %s tax recurring)' : '('.$taxIncEx.' %s tax)' , cbtaxrate($position->productData->taxRate), cbtaxrate($position->productData->taxRateRecurring));?></span>
					</td>
					<?php if ($this->cart->usesRecurring) { ?>
						<td class="item-price item-price-recurring">
							<span class="recurring-interval"><?php echo hsc($position->productData->recurring_interval);?></span>
							<span class="order-recurring-price"><?php echo cbprice( ($b2b) ? $position->totalUnreducedRecurringNet : $position->totalUnreducedRecurringGross );?></span>
						</td>
					<?php } ?>
					<td class="item-price"><?php echo cbprice( ($b2b) ? $position->totalUnreducedNet : $position->totalUnreducedGross );?></td>
				</tr>
			<?php } ?>

			<?php // The discount ?>

			<?php if ($this->cart->totalDiscountNet != 0 or $this->cart->totalDiscountRecurringNet != 0) { ?>
				<tr class="sub-total sub-total-discount">
					<td class="item-name">
						<span class="discount-title"><?php echo hsc($this->cart->discount->title);?></span>
						<?php if ($this->cart->discount->percentage) { ?>
							<span class="discount-percentage"><?php echo KText::sprintf( ($this->cart->usesRecurring) ? '(%s regular, %s recurring)' : '(%s)' , cbtaxrate($this->cart->discount->percentage), cbtaxrate($this->cart->discountRecurring->percentage));?></span>
						<?php } ?>
					</td>
					<?php if ($this->cart->usesRecurring) { ?>
						<td class="item-price item-price-recurring"><?php echo cbprice( ($b2b) ? $this->cart->totalDiscountRecurringNet : $this->cart->totalDiscountRecurringGross );?></td>
					<?php } ?>
					<td class="item-price"><?php echo cbprice( ($b2b) ? $this->cart->totalDiscountNet : $this->cart->totalDiscountGross );?></td>
				</tr>
			<?php } ?>

			<?php // The product subtotal (only when there's more than one product or a discount applies ?>

			<?php if (count($this->cart->positions) > 1 || ($this->cart->totalDiscountNet != 0 or $this->cart->totalDiscountRecurringNet != 0)) { ?>
				<tr class="sub-total sub-total-merchandise">
					<td class="item-name"><?php echo KText::_('Subtotal Products');?></td>
					<?php if ($this->cart->usesRecurring) { ?>
						<td class="item-price item-price-recurring"><?php echo cbprice( ($b2b) ? $this->cart->totalRecurringNet : $this->cart->totalRecurringGross );?></td>
					<?php } ?>
					<td class="item-price"><?php echo cbprice( ($b2b) ? $this->cart->totalNet : $this->cart->totalGross );?></td>
				</tr>
			<?php } ?>

			<?php // Delivery details ?>

			<?php if ($this->cart->delivery && CONFIGBOX_DISABLE_DELIVERY == 0) { ?>
				<tr class="sub-total sub-total-delivery">
					<td class="item-name" colspan="<?php echo (1 + intval($this->cart->usesRecurring));?>">

						<?php if ($this->cart->delivery->priceNet != 0) { ?>
							<span class="delivery-text"><?php echo KText::_('Plus delivery');?></span>
						<?php } else { ?>
							<span class="delivery-text"><?php echo KText::_('Delivery:');?></span>
						<?php } ?>

						<span class="delivery-title"><?php echo hsc($this->cart->delivery->rateTitle);?></span>

						<?php if ($this->cart->delivery->priceNet != 0) { ?>
							<span class="delivery-tax"><?php echo KText::sprintf('('.$taxIncEx.' %s tax)', cbtaxrate($this->cart->delivery->taxRate));?></span>
						<?php } ?>

						<span class="delivery-time"><?php echo ($this->cart->delivery->deliverytime) ? KText::sprintf('Delivery time %s days',$this->cart->delivery->deliverytime) : '';?></span>

						<span class="delivery-tooltip"><?php echo KenedoHtml::getTooltip('<a class="fa fa-info-circle"></a>', KText::_('This is method is automatically chosen. At checkout you can choose alternatives.'));?></span>

					</td>

					<td class="item-price item-price-payable"><?php echo cbprice( ($b2b) ? $this->cart->delivery->priceNet : $this->cart->delivery->priceGross, true );?></td>

				</tr>
			<?php } ?>

			<?php // Payment details ?>

			<?php if ($this->cart->payment) { ?>
				<tr class="sub-total sub-total-payment">

					<td class="item-name" colspan="<?php echo (1 + intval($this->cart->usesRecurring));?>">

						<?php if ($this->cart->payment->priceNet != 0) { ?>
							<span class="payment-text"><?php echo KText::_('Plus payment fee');?></span>
						<?php } else { ?>
							<span class="payment-text"><?php echo KText::_('Payment method');?>:</span>
						<?php } ?>

						<span class="payment-title"><?php echo hsc($this->cart->payment->title);?></span>

						<span class="payment-tooltip"><?php echo KenedoHtml::getTooltip('<a class="fa fa-info-circle"></a>', KText::_('This is method is automatically chosen. At checkout you can choose alternatives.'));?></span>

						<?php if ($this->cart->payment->priceNet != 0) { ?>
							<span class="payment-tax"><?php echo KText::sprintf('('.$taxIncEx.' %s tax)', cbtaxrate($this->cart->payment->taxRate));?></span>
						<?php } ?>

					</td>

					<td class="item-price item-price-payable"><?php echo cbprice( ($b2b) ? $this->cart->payment->priceNet : $this->cart->payment->priceGross );?></td>

				</tr>
			<?php } ?>

			<?php // Tax summary ?>

			<?php if ($this->cart->isVatFree == false) { ?>
				<?php foreach ($this->cart->taxes as $taxRate=>$tax) { ?>
					<tr class="sub-total sub-total-tax">
						<td class="item-name">
							<span class="tax-rate-name"><?php echo KText::sprintf($taxPlusInc.' %s tax',cbtaxrate($taxRate));?></span>
							<span class="tax-rate-tooltip"><?php echo KenedoHtml::getTooltip('<a class="fa fa-info-circle"></a>', KText::_('The tax rate may change when you enter your billing and delivery address.'));?></span>
						</td>
						<?php if ($this->cart->usesRecurring) { ?>
							<td class="item-price item-price-recurring"><?php echo cbprice( $tax['recurring'] );?></td>
						<?php } ?>
						<td class="item-price"><?php echo cbprice( $tax['regular'] );?></td>
					</tr>
				<?php } ?>
			<?php } ?>


			<?php // Total ?>

			<tr class="grand-total">
				<td class="item-name"><?php echo KText::_('Payable amount');?></td>

				<?php if ($this->cart->usesRecurring) { ?>
					<td class="item-price item-price-recurring"><?php echo cbprice( $this->cart->totalRecurringGross);?></td>
				<?php } ?>

				<td class="item-price"><?php echo cbprice( $this->cart->totalGross + $this->cart->delivery->priceGross + $this->cart->payment->priceGross );?></td>
			</tr>

			<?php // VAT free notice ?>

			<?php if ($this->cart->isVatFree) { ?>
				<tr class="total-no-tax-notice">
					<td class="item-name" colspan="<?php echo (2 + intval($this->cart->usesRecurring));?>">
						<span class="no-tax-notice"><?php echo KText::_('No tax is charged. The tax liability is shifted to the recipient of the supply.');?></span>
						<?php if ($this->cart->userInfo->vatin) { ?><span class="vat-in"><?php echo KText::sprintf('Customer VAT IN: %s', hsc($this->cart->userInfo->vatin));?></span><?php } ?>
					</td>
				</tr>
			<?php } ?>

			</tbody>
		</table>

	</div>

<?php } ?>