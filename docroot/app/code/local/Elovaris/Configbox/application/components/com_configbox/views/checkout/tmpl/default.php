<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCheckout */
?>
<div id="com_configbox">
<div id="view-checkout">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">
	
	<h1 class="page-title page-title-checkout-page"><?php echo KText::_('Order Placement');?></h1>
	
	<div class="wrapper-order-address">
		<h2 class="step-title"><?php echo KText::_('Your address');?></h2>
		<div class="order-address-form"<?php echo ($this->orderAddressComplete) ? 'style="display:none"':'';?>>
			<?php
			// Coming from view ConfigboxViewCustomerform
			echo $this->orderAddressFormHtml;
			?>
			<div class="order-address-buttons">
				<a class="trigger-save-order-address navbutton-medium leftmost"><span class="nav-center"><i class="fa fa-spin fa-spinner loading-indicator"></i><?php echo KText::_('Update Address');?></span></a>
			</div>
		</div>
		<div class="order-address-display"<?php echo ($this->orderAddressComplete) ? '':'style="display:none"';?>>
			<?php
			// Coming from view checkoutaddress
			echo $this->orderAddressHtml;
			?>
		</div>
	</div>
	
	<?php if ($this->useDelivery) { ?>
		<div class="wrapper-delivery-options">
			<?php 
			// Coming from view checkoutdelivery
			echo $this->deliveryHtml;
			?>
		</div>
	<?php } ?>
	
	<div class="wrapper-payment-options">
		<?php 
		// Coming from view checkoutpayment
		echo $this->paymentHtml;
		?>
	</div>
	
	<h2 class="step-title"><?php echo KText::_('Review your order');?></h2>
	<div class="wrapper-order-record">
			<?php 
			// Coming from view record
			echo $this->orderRecordHtml;
			?>
	</div>
	
	<div class="wrapper-agreements">
		<?php 
		// Render the template default_orderaddress
		$this->renderView('default_agreements');
		?>
	</div>
	
	<div class="wrapper-psp-bridge" style="display:none"></div>
	
	<div class="wrapper-order-buttons">
		<?php 
		// Render the template default_orderaddress
		$this->renderView('default_orderbuttons');
		?>
	</div>
	
	
</div>	
</div>
</div>