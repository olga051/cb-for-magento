<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCustomerform */
?>
<div class="customer-form-sections<?php echo ($this->customerData->samedelivery) ? '':' show-delivery-fields';?>">

	<div class="customer-form-section customer-form-section-billing">

		<div class="customer-form-section-heading">
			<div class="text-address-heading"><?php echo KText::_('Billing Address');?></div>
		</div>

		<div class="customer-form-fields">

			<div class="<?php echo $this->fieldCssClasses['billingcompanyname'];?>">
				<label for="billingcompanyname" class="field-label"><?php echo KText::_('Company');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingcompanyname" name="billingcompanyname" value="<?php echo hsc($this->customerData->billingcompanyname);?>" />
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['billingsalutation_id'];?>">
				<label for="billingsalutation_id" class="field-label"><?php echo KText::_('Salutation');?></label>
				<div class="form-field">
					<?php echo ConfigboxUserHelper::getSalutationDropdown('billingsalutation_id', $this->customerData->billingsalutation_id);?>
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['billingfirstname'];?>">
				<label for="billingfirstname" class="field-label"><?php echo KText::_('First Name');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingfirstname" name="billingfirstname" value="<?php echo hsc($this->customerData->billingfirstname);?>" />
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['billinglastname'];?>">
				<label for="billinglastname" class="field-label"><?php echo KText::_('Last Name');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billinglastname" name="billinglastname" value="<?php echo hsc($this->customerData->billinglastname);?>" />
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['billingaddress1'];?>">
				<label for="billingaddress1" class="field-label"><?php echo KText::_('Address');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingaddress1" name="billingaddress1" value="<?php echo hsc($this->customerData->billingaddress1);?>" />
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['billingaddress2'];?>">
				<label for="billingaddress2" class="field-label"><?php echo KText::_('Address 2');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingaddress2" name="billingaddress2" value="<?php echo hsc($this->customerData->billingaddress2);?>" />
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<?php if ($this->useCityLists == false) { ?>

				<div class="<?php echo $this->fieldCssClasses['billingzipcode'];?>">
					<label for="billingzipcode" class="field-label"><?php echo KText::_('ZIP code');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="billingzipcode" name="billingzipcode" value="<?php echo hsc($this->customerData->billingzipcode);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['billingcity'];?>">
					<label for="billingzipcode" class="field-label"><?php echo KText::_('City');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="billingcity" name="billingcity" value="<?php echo hsc($this->customerData->billingcity);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

			<?php } ?>

			<div class="<?php echo $this->fieldCssClasses['billingcountry'];?>">
				<label for="billingcountry" class="field-label"><?php echo KText::_('Country');?></label>
				<div class="form-field">
					<?php echo ConfigboxCountryHelper::createCountrySelect('billingcountry', $this->customerData->billingcountry, KText::_('Select a country'), 'billingstate');?>
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['billingstate'];?>">
				<label for="billingstate" class="field-label"><?php echo KText::_('State');?></label>
				<div class="form-field">
					<?php echo ConfigboxCountryHelper::createStateSelect('billingstate', $this->customerData->billingstate, $this->customerData->billingcountry, NULL, 'billingcounty_id');?>
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['billingcounty_id'];?>">
				<label for="billingcounty" class="field-label"><?php echo KText::_('County');?></label>
				<div class="form-field">
					<?php echo ConfigboxCountryHelper::createCountySelect('billingcounty_id', $this->customerData->billingcounty_id, $this->customerData->billingstate, KText::_('Select County'), 'billingcity_id');?>
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<?php if ($this->useCityLists == true) { ?>

				<div class="<?php echo $this->fieldCssClasses['billingzipcode'];?>">
					<label for="billingzipcode" class="field-label"><?php echo KText::_('ZIP code');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="billingzipcode" name="billingzipcode" value="<?php echo hsc($this->customerData->billingzipcode);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['billingcity'];?>">
					<label for="billingcity" class="field-label"><?php echo KText::_('City');?></label>
					<div class="form-field">
						<?php echo ConfigboxCountryHelper::getCityTextInput('billingcity', $this->customerData->billingcity);?>
					</div>
					<div class="validation-tooltip"></div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['billingcity_id'];?>">
					<label for="billingcity_id" class="field-label"><?php echo KText::_('City');?></label>
					<div class="form-field">
						<?php echo ConfigboxCountryHelper::getCitySelect('billingcity_id', $this->customerData->billingcity_id, $this->customerData->billingcounty_id, KText::_('Select City'));?>
						<div class="validation-tooltip"></div>
					</div>
				</div>

			<?php } ?>

			<div class="<?php echo $this->fieldCssClasses['billingemail'];?>">
				<label for="billingemail" class="field-label"><?php echo KText::_('Email');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingemail" name="billingemail" value="<?php echo hsc($this->customerData->billingemail);?>" />
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['billingphone'];?>">
				<label for="billingphone" class="field-label"><?php echo KText::_('Phone');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingphone" name="billingphone" value="<?php echo hsc($this->customerData->billingphone);?>" />
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['vatin'];?>">
				<label for="vatin" class="field-label"><?php echo KText::_('VAT IN');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="vatin" name="vatin" value="<?php echo hsc($this->customerData->vatin);?>" />
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['language_tag'];?>">
				<label for="language_tag" class="field-label"><?php echo KText::_('Language');?></label>
				<div class="form-field">
					<?php echo ConfigboxLanguageHelper::getLangSelect($this->customerData->language_tag, 'language_tag'); ?>
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<div class="<?php echo $this->fieldCssClasses['newsletter'];?>">
				<label class="field-label"><?php echo KText::_('Newsletter');?></label>
				<div class="form-field">
					<input type="radio" <?php echo ($this->customerData->newsletter == 1)? 'checked="checked"':'';?> id="newsletter-yes" name="newsletter" value="1" />
					<label class="radio-button-label" for="newsletter-yes"><?php echo KText::_('CBYES');?></label>
					<input type="radio" <?php echo ($this->customerData->newsletter != 1)? 'checked="checked"':'';?> id="newsletter-no" name="newsletter" value="0" />
					<label class="radio-button-label" for="newsletter-no"><?php echo KText::_('CBNO');?></label>
					<div class="validation-tooltip"></div>
				</div>
			</div>

			<?php if ($this->useOptionalRegistration) { ?>
				<div class="customer-field customer-field-register">
					<label class="field-label"><?php echo KText::_('Set up an Account');?><?php echo KenedoHtml::getTooltip('', KText::_('TOOLTIP_TEXT_CUSTOMER_FORM_REGISTER'));?></label>
					<div class="form-field">
						<input type="radio" checked="checked" id="register-yes" name="register" value="1" />
						<label class="radio-button-label" for="newsletter-yes"><?php echo KText::_('CBYES');?></label>
						<input type="radio" id="register-no" name="register" value="0" />
						<label class="radio-button-label" for="newsletter-no"><?php echo KText::_('CBNO');?></label>
						<div class="validation-tooltip"></div>
					</div>
				</div>
			<?php } ?>

		</div> <!-- .customer-form-fields -->

	</div> <!-- .customer-form-section-billing -->

	<?php if ($this->allowDeliveryAddress) { ?>
		<div class="customer-form-section customer-form-section-delivery">

			<div class="customer-form-section-heading">
				<div class="text-address-heading"><?php echo KText::_('Delivery Address');?></div>

				<div class="different-shipping-address-toggle">
					<input class="trigger-toggle-same-delivery" type="checkbox" value="1" name="samedelivery" id="samedelivery"<?php echo ($this->customerData->samedelivery) ? ' checked="checked"':'';?> />
					<label for="samedelivery"><?php echo KText::_('Same as billing');?></label>
				</div>

			</div>

			<div class="customer-form-fields">

				<div class="<?php echo $this->fieldCssClasses['companyname'];?>">
					<label for="companyname" class="field-label"><?php echo KText::_('Company Name');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="companyname" name="companyname" value="<?php echo hsc($this->customerData->companyname);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['salutation_id'];?>">
					<label for="salutation_id" class="field-label"><?php echo KText::_('Salutation');?></label>
					<div class="form-field">
						<?php echo ConfigboxUserHelper::getSalutationDropdown('salutation_id',$this->customerData->salutation_id);?>
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['firstname'];?>">
					<label for="firstname" class="field-label"><?php echo KText::_('First Name');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="firstname" name="firstname" value="<?php echo hsc($this->customerData->firstname);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['lastname'];?>">
					<label for="lastname" class="field-label"><?php echo KText::_('Last Name');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="lastname" name="lastname" value="<?php echo hsc($this->customerData->lastname);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['address1'];?>">
					<label for="address1" class="field-label"><?php echo KText::_('Address');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="address1" name="address1" value="<?php echo hsc($this->customerData->address1);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['address2'];?>">
					<label for="address2" class="field-label"><?php echo KText::_('Address 2');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="address2" name="address2" value="<?php echo hsc($this->customerData->address2);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<?php if ($this->useCityLists == false) { ?>

					<div class="<?php echo $this->fieldCssClasses['zipcode'];?>">
						<label for="zipcode" class="field-label"><?php echo KText::_('ZIP code');?></label>
						<div class="form-field">
							<input class="textfield" type="text" id="zipcode" name="zipcode" value="<?php echo hsc($this->customerData->zipcode);?>" />
							<div class="validation-tooltip"></div>
						</div>
					</div>

					<div class="<?php echo $this->fieldCssClasses['city'];?>">
						<label for="zipcode" class="field-label"><?php echo KText::_('City');?></label>
						<div class="form-field">
							<input class="textfield" type="text" id="city" name="city" value="<?php echo hsc($this->customerData->city);?>" />
							<div class="validation-tooltip"></div>
						</div>
					</div>

				<?php } ?>

				<div class="<?php echo $this->fieldCssClasses['country'];?>">
					<label for="country" class="field-label"><?php echo KText::_('Country');?></label>
					<div class="form-field">
						<?php echo ConfigboxCountryHelper::createCountrySelect('country', $this->customerData->country, KText::_('Select a country'), 'state');?>
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['state'];?>">
					<label for="state" class="field-label"><?php echo KText::_('State');?></label>
					<div class="form-field">
						<?php echo ConfigboxCountryHelper::createStateSelect('state', $this->customerData->state, $this->customerData->country, NULL, 'county_id');?>
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['county_id'];?>">
					<label for="county" class="field-label"><?php echo KText::_('County');?></label>
					<div class="form-field">
						<?php echo ConfigboxCountryHelper::createCountySelect('county_id', $this->customerData->county_id, $this->customerData->state, KText::_('Select County'), 'city_id');?>
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<?php if ($this->useCityLists == true) { ?>

					<div class="<?php echo $this->fieldCssClasses['zipcode'];?>">
						<label for="zipcode" class="field-label"><?php echo KText::_('ZIP code');?></label>
						<div class="form-field">
							<input class="textfield" type="text" id="zipcode" name="zipcode" value="<?php echo hsc($this->customerData->zipcode);?>" />
							<div class="validation-tooltip"></div>
						</div>
					</div>

					<div class="<?php echo $this->fieldCssClasses['city'];?>">
						<label for="city" class="field-label"><?php echo KText::_('City');?></label>
						<div class="form-field">
							<?php echo ConfigboxCountryHelper::getCityTextInput('city', $this->customerData->city);?>
							<div class="validation-tooltip"></div>
						</div>
					</div>

					<div class="<?php echo $this->fieldCssClasses['city_id'];?>">
						<label for="city_id" class="field-label"><?php echo KText::_('City');?></label>
						<div class="form-field">
							<?php echo ConfigboxCountryHelper::getCitySelect('city_id', $this->customerData->city_id, $this->customerData->county_id, KText::_('Select City'));?>
							<div class="validation-tooltip"></div>
						</div>
					</div>

				<?php } ?>

				<div class="<?php echo $this->fieldCssClasses['email'];?>">
					<label for="email" class="field-label"><?php echo KText::_('Email');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="email" name="email" value="<?php echo hsc($this->customerData->email);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

				<div class="<?php echo $this->fieldCssClasses['phone'];?>">
					<label for="phone" class="field-label"><?php echo KText::_('Phone');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="phone" name="phone" value="<?php echo hsc($this->customerData->phone);?>" />
						<div class="validation-tooltip"></div>
					</div>
				</div>

			</div> <!-- .customer-form-fields -->

		</div> <!-- .customer-form-section-delivery -->
	<?php } ?>

</div> <!-- .customer-form-sections -->
