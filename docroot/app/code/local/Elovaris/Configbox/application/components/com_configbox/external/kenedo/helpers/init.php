<?php
function initKenedo($component = 'com_configbox') {

	if (defined('KENEDO_INIT_DONE')) {
		return;
	}
	else {
		define('KENEDO_INIT_DONE', true);
	}

	if (!defined('CB_VALID_ENTRY')) {
		define('CB_VALID_ENTRY', true);
	}

	// Shorthand for the DS
	if (!defined('DS')) {
		define('DS', DIRECTORY_SEPARATOR);
	}

	// Load the auto load class file
	require_once (dirname(__FILE__).'/../classes/KenedoAutoload.php');

	// Register the autoload method
	spl_autoload_register('KenedoAutoload::loadClass');

	// Somehow this makes it play nicely with other software using autoload
	if (function_exists('__autoload') && in_array('__autoload', spl_autoload_functions())) {
		spl_autoload_register('__autoload');
	}

	// Load the helpers classes
	KenedoAutoload::registerClass( 'InterfaceKenedoPlatform', dirname(__FILE__).DS.'..'.DS.'interfaces'.DS.'KenedoPlatform.php' );
	KenedoAutoload::registerClass( 'KenedoProfiler', 		dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoProfiler.php' );
	KenedoAutoload::registerClass( 'KenedoPlatform', 		dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoPlatform.php' );
	KenedoAutoload::registerClass( 'KenedoController', 		dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoController.php' );
	KenedoAutoload::registerClass( 'KenedoObserver', 		dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoObserver.php' );
	KenedoAutoload::registerClass( 'KenedoModelLight',		dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoModelLight.php' );
	KenedoAutoload::registerClass( 'KenedoModel', 			dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoModel.php' );
	KenedoAutoload::registerClass( 'KenedoView', 			dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoView.php' );
	KenedoAutoload::registerClass( 'KenedoHtml', 			dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoHtml.php' );
	KenedoAutoload::registerClass( 'KenedoProperty',		dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoProperty.php' );
	KenedoAutoload::registerClass( 'KenedoDatabase', 		dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoDatabase.php' );
	KenedoAutoload::registerClass( 'KLog', 					dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KLog.php' );
	KenedoAutoload::registerClass( 'KLink', 				dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KLink.php' );
	KenedoAutoload::registerClass( 'KRequest', 				dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KRequest.php' );
	KenedoAutoload::registerClass( 'KSession', 				dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KSession.php' );
	KenedoAutoload::registerClass( 'KStorage', 				dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KStorage.php' );
	KenedoAutoload::registerClass( 'KText', 				dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KText.php' );
	KenedoAutoload::registerClass( 'KenedoObject', 			dirname(__FILE__).DS.'..'.DS.'classes'.DS.'KenedoObject.php' );
	KenedoAutoload::registerClass( 'KenedoLanguageHelper', 	dirname(__FILE__).DS.'..'.DS.'helpers'.DS.'language.php' );
	KenedoAutoload::registerClass( 'KenedoRouterHelper', 	dirname(__FILE__).DS.'..'.DS.'helpers'.DS.'router.php' );
	KenedoAutoload::registerClass( 'KenedoViewHelper', 		dirname(__FILE__).DS.'..'.DS.'helpers'.DS.'view.php' );
	KenedoAutoload::registerClass( 'KenedoTimeHelper', 		dirname(__FILE__).DS.'..'.DS.'helpers'.DS.'time.php' );
	KenedoAutoload::registerClass( 'KenedoFileHelper', 		dirname(__FILE__).DS.'..'.DS.'helpers'.DS.'file.php' );
	KenedoAutoload::registerClass( 'KenedoDirHelper', 		dirname(__FILE__).DS.'..'.DS.'helpers'.DS.'dir.php' );

	// Try to overcome the class name change on updates, class name reference is in cache files
	if (class_exists('KObject') == false && function_exists('class_alias')) {
		class_alias('KenedoObject', 'KObject');
	}

	// Run any platform specific init stuff
	KenedoPlatform::p()->initialize();

	// Let the platform start the session
	KenedoPlatform::p()->startSession();

	if (substr(PHP_SAPI, 0, 3) == 'cli') {
		$scheme = '';
	}
	else {
		// Figure out if on http or https (praying for a definite and straight-forward way in future)
		if(!empty($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
			$scheme = strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']);
		}
		elseif(!empty($_SERVER['HTTPS'])) {
			$scheme = (strtolower($_SERVER['HTTPS']) !== 'off') ? 'http':'https';
		}
		else {
			$scheme = ($_SERVER['SERVER_PORT'] == 443) ? 'https':'http';
		}
	}

	// Define paths
	/**
	 * URL scheme (without colons or backslashes)
	 * E.g. https
	 * @const  KPATH_ROOT
	 */
	define('KPATH_SCHEME', 	$scheme);

	/**
	 * HTTP Hostname
	 * E.g. configbox.dev
	 * @const  KPATH_HOST
	 */
	define('KPATH_HOST', 	(substr(PHP_SAPI, 0, 3) == 'cli') ? '' : $_SERVER['HTTP_HOST']);

	/**
	 * Platform base URL (scheme://host/path) - without a trailing slash
	 * @const  KPATH_URL_BASE
	 */
	define('KPATH_URL_BASE', KenedoPlatform::p()->getUrlBase());

	/**
	 * Full path to the application's root directory (not the web server's root)
	 * @const  KPATH_ROOT
	 */
	define('KPATH_ROOT', KenedoPlatform::p()->getRootDirectory());

	// KenedoView template paths
	define('KPATH_TABLE_TMPL', 	 dirname(__FILE__).DS.'..'.DS.'tmpl'.DS.'default-table.php');
	define('KPATH_LISTING_TMPL', dirname(__FILE__).DS.'..'.DS.'tmpl'.DS.'default-listing.php');
	define('KPATH_DETAILS_TMPL', dirname(__FILE__).DS.'..'.DS.'tmpl'.DS.'default-editform.php');

	// Set error and shutdown handlers
	// We got issues doing our own error handling on Magento.
	// Until we figured it out, we do not interfere. See end of configbox.php for restoring old handlers
	if (KenedoPlatform::getName() != 'magento' ) {
		set_error_handler( array( 'KLog', 'handleError' ) );
		register_shutdown_function( array( 'KLog', 'handleShutdown' ) );
	}

	// Do application autoloads (does some initialization too now)
	$componentAutoLoadFile = KenedoPlatform::p()->getComponentDir($component).DS.'helpers'.DS.'init.php';

	if (is_file($componentAutoLoadFile)) {
		require_once($componentAutoLoadFile);
	}

}