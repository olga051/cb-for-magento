<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewRfq extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'rfq';

	/**
	 * @var string $customerFormHtml
	 * @see ConfigboxViewCustomerform
	 */
	public $customerFormHtml;

	/**
	 * @var int $cartId Cart ID to make a quote from
	 */
	public $cartId;

	/**
	 * @var string $urlCart URL to the cart page
	 */
	public $urlCart;

	/**
	 * @return ConfigboxModelRfq $model
	 */
	public function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelRfq');
	}

	function prepareTemplateVars() {

		$view = KenedoView::getView('ConfigboxViewCustomerform');
		$view->assignRef('formType', 'quotation');
		$view->prepareTemplateVars();
		$formHtml = $view->getViewOutput();

		$this->assign('customerFormHtml', $formHtml);

		$cartId = KRequest::getInt('cart_id');
		$this->assign('cartId', $cartId);
		$this->assign('urlCart', KLink::getRoute('index.php?option=com_configbox&view=cart&cart_id='.$cartId));

		// Get tracking code (comes from overridden templates, empty otherwise)
		$trackingCode = $this->getViewOutput('tracking_code');
		$this->assign('trackingCode', $trackingCode);

	}

}