<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdminLicense extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'adminlicense';
	public $tree;

	/**
	 * @var int[][] Array with keys 'products', 'pages' or 'elements' holding an array with record ids that are currently open in the product tree
	 */
	public $openIds;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function display() {
		$this->renderView();
	}

}