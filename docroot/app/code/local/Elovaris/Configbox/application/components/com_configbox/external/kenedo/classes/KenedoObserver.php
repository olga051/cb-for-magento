<?php
class KenedoObserver {
	
	static public $observers = array();

	static public $legacyEventNames = array(
		'onConfigBoxGrandorderBeforeDiscounts' => 'onConfigBoxCartProcessingBeforeDiscounts',
		'onConfigBoxAfterProcessOrder'			=> 'onConfigBoxCartProcessingAfterPositions'
	);

	static function registerObservers($folder) {
		
		$files = KenedoFileHelper::getFiles($folder,'.php',false,true);
		
		foreach ($files as $file) {
			
			// Remove this when updates actually remove outdated files
			if (basename($file) == 'Cbcheckout.php') continue;
			
			self::registerObserver($file);
		}
		
	}
	
	static function registerObserver($file) {
		$className = 'Observer'.KenedoFileHelper::stripExtension(basename($file));
		
		require_once($file);
		self::$observers[$className] = new $className();
		
	}
	
	static function triggerEvent($eventName, $parameters = array(), $returnLast = false) {

		if ($eventName && !empty(self::$legacyEventNames[$eventName])) {
			KLog::logLegacyCall('Event was renamed, "'.$eventName.'" is now "'.self::$legacyEventNames[$eventName].'". Change the first parameter of ::triggerEvent');
			$eventName = self::$legacyEventNames[$eventName];
		}

		$returns = array();
		
		foreach (self::$observers as &$observer) {
			
			if (method_exists($observer, $eventName)) {
				$returns[get_class($observer)] = call_user_func_array(array($observer, $eventName), $parameters);
			}
			
		}
		if ($returnLast) {
			if (count($returns) > 0) {
				return array_pop($returns);
			}
			else {
				return NULL;
			}
		}
		else {
			return $returns;
		}
		
		
	}
	
}