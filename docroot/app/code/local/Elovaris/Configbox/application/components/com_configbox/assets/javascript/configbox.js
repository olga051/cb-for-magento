/* global cbj: false */
/* global Kenedo: true */
/* global tinyMCE: true */
/* global CodeMirror: true */
/* global alert: false */
/* global confirm: false */
/* global alert: false */
/* global console: false */
/* jshint -W116 */

cbj(document).ready(function() {

	// Handlers for configuration updates
	cbj('#com_configbox').on('change keyup', '.configbox-widget-text', com_configbox.setTextSelectionFromControl);
	cbj('#com_configbox').on('change',		'.configbox-control', com_configbox.setSelectionFromControl);

	cbj('#com_configbox').on('serverResponseReceived', com_configbox.processServerResponse);

	// Handlers for activating/deactivating elements
	cbj('#com_configbox').on('activateElement','.element', com_configbox.activateElement);
	cbj('#com_configbox').on('deactivateElement','.element', com_configbox.deactivateElement);

	// Handlers for activating/deactivating xrefs
	cbj('#com_configbox').on('activateXref','.element', com_configbox.activateXref);
	cbj('#com_configbox').on('deactivateXref','.element', com_configbox.deactivateXref);

	// Handlers for price changes
	cbj('#com_configbox').on('pricingChanged',com_configbox.updatePricing);

	// When automatic selections are made, update the com_configbox.element array
	cbj('#com_configbox').on('systemChangedElementSelection', function(event, elementId, xrefId, text) {
		var genericValue = null;

		if(text) {
			genericValue = text;
		}

		if (xrefId) {
			genericValue = xrefId;
		}

		// Update the selection in the element's array
		com_configbox.setElementValue(elementId, genericValue);
	});

	// Handlers for when required elements don't have a selection
	cbj('#com_configbox').on('requiredElementsMissing',function(){
		if (com_configbox.lockPageOnRequired) {
			cbj('#com_configbox .add-to-cart-button, #com_configbox .next-button').addClass('configbox-disabled');
		}
	});

	// Handlers for when all required elements have a selection
	cbj('#com_configbox').on('requiredElementsSelected',function(){
		if (com_configbox.lockPageOnRequired) {
			cbj('#com_configbox .add-to-cart-button, #com_configbox .next-button').removeClass('configbox-disabled');
		}
	});

	// ELEMENT IMAGE PICKERS: Clicking on image pickers shall trigger a selection
	cbj('#com_configbox').on('click', '.element-applying .xref-applying .configbox-image-button-wrapper', function() {
		if (cbj(this).closest('.xrefwrapper').hasClass('selected') === false) {
			cbj(this).closest('.xrefwrapper').find('.configbox-control').prop('selected',true).prop('checked',true).trigger('change');
		}
		else {
			cbj(this).closest('.xrefwrapper').find('.configbox-control-checkbox').prop('selected',false).prop('checked',false).trigger('change');
		}
	});

	// CONFIGURATOR PAGE: Currency changer functionality
	cbj('#curr_id').change(function(){
		cbj(this).closest('form').submit();
	});

	// CART: Quantity dropdown functionality
	cbj('#com_configbox').on('change', 'select.input-product-quantity', function(){
		cbj(this).closest('form').submit();
	});

	// CART: Update quantity button functionality
	cbj('#com_configbox').on('click', '.button-update-quantity', function(){
		cbj(this).closest('form').submit();
	});

	// ANYWHERE: New tab links
	cbj('#com_configbox').on('click','.new-tab', function(event){
		window.open(this.href);
		event.preventDefault();
	});

	// ANYWHERE: Form submit buttons
	cbj('#com_configbox').on('click','.form-submit-button', function(){
		cbj(this).closest('form').submit();
	});

	// ANYWHERE: Defer clicks on links while AJAX requests are running
	cbj('.onAjaxCompleteOnly').click(function(event){
		com_configbox.redirectUrl = '';
		// If a XHR is in progress, prevent redirection, store the URL, on ajaxStop the redirection will happen.
		if (com_configbox.requestInProgress === true) {
			event.preventDefault();
			event.stopImmediatePropagation();
			com_configbox.redirectUrl = cbj(this).attr('href');
		}
	});

	// ANYWHERE: Do chosen dropdowns where needed
	cbj('#com_configbox .chosen-dropdown').each(function(){
		cbj(this).chosen();
	});

	// Store license key button
	cbj('#com_configbox').on('click', '.trigger-store-license-key', function(){
		cbj(this).closest('form').submit();
	});

});

var com_configbox = {

	urlBase : '',
	tinyMceBaseUrl : '',
	canQuickEdit : false,
	isRequestError : false,
	redirectUrl : '',
	lockPageOnRequired : 0,
	entryFile : '',
	decimal_symbol: '',
	configuratorUpdateUrl : '',
	langTag : '',
	langSuffix : '',
	modalOverlayOpacity : '',
	agreeToTerms : null,
	agreeToRefundPolicy : null,
	pspBridgeViewUrl : '',
	addressViewUrl : '',
	deliveryViewUrl : '',
	paymentViewUrl : '',
	recordViewUrl : '',
	customerFieldDefinitions : '',

	lang : {
		valueNotNumeric : '',
		valueNotInteger : '',
		valueToLow : '',
		valueToHigh : '',
		DATEFORMAT_CALENDAR_JS : '',
		isInvalid : '',
		fillAllFields : '',
		checkoutPleaseAgreeBoth : '',
		checkoutPleaseAgreeTerms : '',
		checkoutPleaseAgreeRefundPolicy : '',
		matrixParametersNotUnique : '',
		userFieldsNoHiding : '',
		userFieldsNoOptional : '',
		followlink : '',
		areyousure: '',
		selectCountryFirst: '',
		selectStateFirst: ''
	},

	/**
	 * @property {JsonResponses.addOrderFileResponseData} addOrderFileResponseData
	 */
	addOrderFileResponseData : {},

	/**
	 * Data set during configurator change file uploads (contains the same data as for processServerResponse)
	 * @property {JsonResponses.configuratorUpdates} configuratorFileUploadResponseData
	 */
	configuratorFileUploadResponseData : {},

	cartPositionId: 0,

	timers: [],

	/**
	 * Method that can be overwritten in custom js to validate a configuration change
	 * This does not override the system validation, this check comes before the system's check
	 * @param {int} elementId
	 * @param {string} value - Either the entered text or the option assignment id
	 * @return {boolean} result - false if not valid, true otherwise
	 */
	customConfigChangeCheck : function(elementId, value) {
		// Do something to avoid code inspector flags ;)
		if (elementId && value) {
			return true;
		}
		return true;
	},

	/**
	 * Handler for change and keyup on .configbox-widget-text.
	 * Does client validation during typing and makes a delayed selection when entry is valid.
	 */
	setTextSelectionFromControl: function(event) {

		var elementId = com_configbox.getElementId(cbj(this));

		// Set keys that shall be ignored
		var ignoreKeys = [9,13,16,17,18,37,38,39,40];

		// Return false on these keys and stop
		if (cbj.inArray(event.keyCode,ignoreKeys) !== -1) {
			return false;
		}

		var debug = false;

		// Get the value, replace any localized decimal symbol to english symbol
		var value = cbj(this).val().replace(com_configbox.decimal_symbol,'.');

		// If only integer values are allowed, use this
		var integerOnly = com_configbox.getElementAttribute(elementId,'integer_only');

		if (integerOnly == 1 && debug === false) {

			var regExIsInteger = new RegExp("^-?\\d*$",'i');

			// Validate on integer
			if (regExIsInteger.test(value) === false && value !== '') {
				com_configbox.showValidationError( elementId , com_configbox.lang.valueNotInteger);
				return false;
			}
		}

		// Check if validation is necessary
		if (com_configbox.getElementAttribute(elementId,'validate') == 1 && debug === false) {

			var regExIsNumber = new RegExp("^-?\\d*(?:\\.\\d*)?$",'i');

			// Validate on number
			if (regExIsNumber.test(value) === false && value !== '') {
				com_configbox.showValidationError( elementId ,com_configbox.lang.valueNotNumeric);
				return false;
			}

			var minValue = com_configbox.getElementAttribute(elementId,'minval');
			if (minValue !== null) {
				// Validate on min value
				minValue = parseFloat(minValue);
				if (value < minValue) {
					com_configbox.showValidationError( elementId , com_configbox.lang.valueToLow.replace('%s',minValue) );
					return false;
				}
			}

			var maxValue = com_configbox.getElementAttribute(elementId,'maxval');
			if (maxValue !== null) {

				// Validate on max value
				maxValue = parseFloat(com_configbox.getElementAttribute(elementId,'maxval'));
				if (value > maxValue) {
					com_configbox.showValidationError( elementId, com_configbox.lang.valueToHigh.replace('%s',maxValue) );
					return false;
				}
			}

		}

		if (value != cbj(this).data('lastval')) {
			cbj(this).data('lastval',value);

			// Store the value in the control field
			cbj('#element-'+elementId).val(value);

			// Clear any delayed update starts
			if (typeof(com_configbox.timers[elementId])) {
				window.clearTimeout(com_configbox.timers[elementId]);
			}

			// Start the update with a delay
			com_configbox.timers[elementId] = window.setTimeout( function() { com_configbox.sendSelectionToServer(elementId, String(value)); } , 400);

		}

	},

	/**
	 * Handler for changes to the configurator controls. Listens for change on .configbox-control elements. It gets
	 * element id and selection value from the HTML and calls sendSelectionToServer for making the call. Point of this
	 * method is to separate dealing with the HTML from finding element id and value from making a selection.
	 *
	 * @see com_configbox.sendSelectionToServer
	 *
	 * @param event
	 * @param {boolean} confirmed - Indicates if user confirmed inconsistency resolution
	 * @param {boolean} doNotSendSelection - true if selection should not be sent to the server
	 */
	setSelectionFromControl: function(event, confirmed, doNotSendSelection) {

		if (doNotSendSelection) {
			return;
		}

		var elementId = com_configbox.getElementId(cbj(this));
		var value = cbj(this).val();

		if (cbj(this).attr('type') === 'checkbox' && cbj(this).prop('checked') == false) {
			value = 0;
		}

		com_configbox.sendSelectionToServer(elementId, value, confirmed);
	},

	/**
	 * Does the actual server-side selection change.
	 * On success, it triggers 'serverResponseReceived' on #com_configbox, invoking com_configbox.processServerResponse()
	 *
	 * @param {int} elementId - The ID of the element to make a selection for
	 * @param {string, int} value - The selection value (xref ID or text entry)
	 * @param {boolean=} confirmed - If the user confirmed resolution of inconsistencies
	 *
	 * @fires serverResponseReceived
	 * @fires configurationUpdated
	 */
	sendSelectionToServer: function(elementId, value, confirmed) {

		// Update the visualization immediately for better responsiveness
		com_configbox.blockVisualization.changeVisualization(elementId, parseInt(value));

		// If a custom validation is in place, check, restore and quit in case
		var result = com_configbox.customConfigChangeCheck(elementId, value);
		if (result === false) {
			// Restore the old value
			var oldValue = com_configbox.getElementValue( elementId );
			com_configbox.updateElementSelection( elementId, oldValue);
			return;
		}

		// Set CSS class for the selected xref
		cbj('#elementwrapper-' + elementId + ' .xrefwrapper').removeClass('selected');
		cbj('#xrefwrapper-' + parseInt(value)).addClass('selected');

		// Prepare XHR request data
		var requestData = {
			option: 'com_configbox',
			controller: 'configuratorpage',
			task: 'makeSelection',
			format: 'json',
			lang: com_configbox.langTag,
			cart_position_id: com_configbox.cartPositionId,
			element_id: elementId,
			value: value,
			confirmed: (confirmed) ? '1':'0',
			prod_id: com_configbox.prod_id,
			page_id: com_configbox.page_id
		};

		// Do the XHR request
		cbj.ajax({

			url: com_configbox.configuratorUpdateUrl,
			dataType: 'json',
			type: 'post',
			data: requestData,

			beforeSend: function() {
				// Display the loading symbol
				com_configbox.showAjaxLoader(elementId, parseInt(value));
			},

			success: function(response) {
				/**
				 * @event serverResponseReceived
				 * @property {JsonResponses.configuratorUpdates} response
				 */
				cbj('#com_configbox').trigger('serverResponseReceived', [response]);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				// Log the errors
				console.log(textStatus);
				console.log(errorThrown);
			},

			complete: function() {
				// Remove the loading symbol
				com_configbox.clearAjaxLoader(elementId,parseInt(value));
				/**
				 * @event configurationUpdated
				 */
				cbj('#com_configbox').trigger('configurationUpdated');
			}

		});

	},

	/**
	 * Handler for event 'serverResponseReceived'. Processes the response from a selection change.
	 *
	 * @param {Event} event - jQuery event object
	 * @param {JsonResponses.configuratorUpdates} data
	 *
	 * @listens Event:serverResponseReceived
	 * @fires requiredElementsMissing When required elements of the current page lack selections
	 * @fires requiredElementsSelected When all required elements of the current page got selections
	 * @fires selectionHasChanged To get the selection made by the function visible
	 */
	processServerResponse: function( event, data ) {

		cbp.start('process data');

		// Since it seems like sometimes the complete handler is not triggered
		com_configbox.clearAjaxLoader(data.requestedChange.elementId);

		// Do error handling
		cbp.start('error handling');
		if (typeof(data.error) !== 'undefined') {
			com_configbox.showValidationError( data.requestedChange.elementId, data.error);
			com_configbox.isRequestError = false;
			return;
		}
		else {
			com_configbox.clearValidationError( data.requestedChange.elementId );
		}
		cbp.stop('error handling');

		// In case the server asks for confirmation for conflict resolution, ask the user
		if (data.confirmationText) {

			var confirmed = com_configbox.confirmCollison( data.requestedChange.elementId , data.confirmationText);

			if (confirmed) {
				// Do another run, this time with the confirmed flag on
				var value = (data.requestedChange.text) ? data.requestedChange.text : data.requestedChange.xrefId;
				com_configbox.sendSelectionToServer( data.requestedChange.elementId , value, true);
				return;
			}
			else {
				// Restore the original selection
				com_configbox.updateElementSelection(data.requestedChange.elementId, data.originalValue.xrefId, data.originalValue.text, data.originalValue.outputValue);
				return;
			}

		}

		// Apply new validation values
		if (data.validationValues) {
			com_configbox.processValidationUpdate(data.validationValues);
		}

		// Update cart position details
		if (com_configbox.cartPositionDetails) {
			com_configbox.cartPositionDetails = data.cartPositionDetails;
		}

		// Set the element's selected value to new value
		cbp.start('do set element value');
		if (data.requestedChange.text) {
			com_configbox.setElementValue(data.requestedChange.elementId, data.requestedChange.text);
		}
		else {
			com_configbox.setElementValue(data.requestedChange.elementId, data.requestedChange.xrefId);
		}
		cbp.stop('do set element value');

		// Item visibility (apply/unapply elements and xrefs)
		if(data.itemVisibility) {
			cbp.start('do page item visibility');
			com_configbox.processItemVisibility(data.itemVisibility);
			cbp.stop('do page item visibility');
		}

		// Trigger the event 'selectionHasChanged' with the customer's selection
		if (data.requestedChange) {
			var params = [data.requestedChange.elementId, data.requestedChange.xrefId, data.requestedChange.text, data.requestedChange.outputValue];
			/**
			 * @event selectionHasChanged
			 */
			cbj('#com_configbox').trigger('selectionHasChanged', params);
		}

		// Configuration changes (do auto-selects, auto removals etc.)
		if (data.configurationChanges) {
			cbp.start('do configuration changes');
			com_configbox.processAutomaticSelections(data.configurationChanges);
			cbp.stop('do configuration changes');
		}

		// Update pricing
		if (data.pricing) {
			cbp.start('do pricing update');
			/**
			 * @event pricingChanged
			 * @property {JsonResponses.configuratorUpdates.pricing}
			 * @see com_configbox.updatePricing
			 */
			cbj('#com_configbox').trigger('pricingChanged',[data.pricing]);
			com_configbox.pricing = data.pricing;
			cbp.stop('do pricing update');
		}

		// Deal with required elements and the page blocker
		cbp.start('do required elements');
		if (com_configbox.requiredElementsMissing()) {
			/**
			 * @event requiredElementsMissing
			 */
			cbj('#com_configbox').trigger('requiredElementsMissing');
		}
		else {
			/**
			 * @event requiredElementsSelected
			 */
			cbj('#com_configbox').trigger('requiredElementsSelected');
		}
		cbp.stop('do required elements');

		cbp.stop('process data');

	},

	/**
	 * Called by com_configbox.processServerResponse when a collision was detected
	 * @param {int} elementId - Regarding element ID
	 * @param {string} text - Text shown to the visitor
	 * @return {boolean} goAhead - Whether to auto-change invalid selections
	 */
	confirmCollison: function( elementId, text ) {
		return confirm(text);
	},

	/**
	 * Called by com_configbox.processServerResponse when validation values have changed
	 * @param {JsonResponses.configuratorUpdates.validationValues} validationValues - validation values
	 */
	processValidationUpdate : function ( validationValues ) {

		cbj.each(validationValues, function (elementId, validationValue) {

			if ( com_configbox.elements[elementId].minval != validationValue.minval || com_configbox.elements[elementId].maxval != validationValue.maxval) {

				// Change the data in the elements array for reference
				com_configbox.elements[elementId].minval = validationValue.minval;
				com_configbox.elements[elementId].maxval = validationValue.maxval;

				// Trigger the event, pass data
				/**
				 * @event changeValidationValues
				 */
				cbj('#elementwrapper-' + elementId).trigger('changeValidationValues',[elementId, validationValue]);
			}


		});
	},

	/**
	 * Called by com_configbox.processServerResponse when elements or options get activated/deactivated
	 * Triggers events on the elements' wrapper HTML element, handlers do the work
	 * @param {JsonResponses.configuratorUpdates.itemVisibility} itemVisibility - validation values (array of arrays containing a min and max val)
	 */
	processItemVisibility : function( itemVisibility ) {

		com_configbox.lastConfiguration = com_configbox.elements;

		cbj.each(itemVisibility.elements, function (elementId, applies){

			// Check whether visibility has changed
			if (com_configbox.elements[elementId].applies != applies) {

				// Update value in elements array
				com_configbox.elements[elementId].applies = applies;

				if (applies) {
					/**
					 * @event activateElement
					 */
					cbj('#elementwrapper-'+ elementId).trigger('activateElement',[elementId]);
				}
				else {
					/**
					 * @event deactivateElement
					 */
					cbj('#elementwrapper-'+ elementId).trigger('deactivateElement',[elementId]);
				}
			}

		});

		cbj.each(itemVisibility.xrefs, function (elementId, xrefs){
			cbj.each(xrefs,function(xrefId,applies){

				// Update value in elements array
				com_configbox.elements[elementId].options[xrefId].applies = applies;

				if (applies) {
					/**
					 * @event activateXref
					 */
					cbj('#elementwrapper-' + elementId).trigger('activateXref',[elementId, xrefId]);
				}
				else {
					/**
					 * @event deactivateXref
					 */
					cbj('#elementwrapper-' + elementId).trigger('deactivateXref',[elementId, xrefId]);
				}

			});
		});

	},


	/**
	 * Generic handler for activating an element. Bound on each element's wrapper div. Handling the event 'activateElement'
	 */
	activateElement: function( event, elementId ) {

		// console.log('activateElement called on element "'+ elementId + '" ('+ com_configbox.elements[elementId].title +').');

		// Get the element wrapper
		var element = cbj('#elementwrapper-'+ elementId);

		// Remove element-non-applying
		element.removeClass('element-not-applying').addClass('element-applying');

		// Enable all applying xrefs (if any)
		element.find('.xref-applying.available .configbox-control').prop('disabled',false);

		// In case we deal with a text field, enable it too
		element.find('.configbox-control-textfield').prop('disabled',false);
		element.find('.configbox-widget-text').prop('disabled',false);

	},

	/**
	 * Generic handler for deactivating an element. Bound on each element's wrapper div. Handling the event 'deactivateElement'
	 */
	deactivateElement: function( event, elementId ) {

		// console.log('deactivateElement called on elementId "'+ elementId + '" ('+ com_configbox.elements[elementId].title +').');

		// Get the element wrapper
		var element = cbj('#elementwrapper-'+ elementId);

		// Put element-non-applying on the element's wrapper div
		element.addClass('element-not-applying').removeClass('element-applying');

		// Disable all form elements
		element.find('.configbox-control').prop('disabled',true);
		element.find('.configbox-widget-text').prop('disabled',true);

	},

	/**
	 * Generic handler for activating an element's xref. Bound on each ELEMENT'S wrapper div. Handling the event 'activateXref'
	 */
	activateXref: function ( event, elementId, xrefId ) {

		// console.log('activateXref called on xref "'+ xrefId + '".');

		var xref = cbj('.element-applying #xref-'+xrefId);
		var xrefwrapper = xref.closest('.element-applying .xrefwrapper');

		// Remove xref-non-applying tag
		xrefwrapper.removeClass('xref-not-applying').addClass('xref-applying');

		// Enable the form element
		if (xrefwrapper.hasClass('not-available') === false) {
			xref.prop('disabled',false);
		}

	},

	/**
	 * Generic handler for deactivating an element's xref. Bound on each ELEMENT'S wrapper div. Handling the event 'deactivateXref'
	 */
	deactivateXref: function ( event, elementId, xrefId ) {

		// console.log('deactivateXref called on xref "'+ xrefId + '".');

		var xref = cbj('#xref-'+xrefId);
		var xrefwrapper = xref.closest('.xrefwrapper');

		// Add xref-non-applying tag
		xrefwrapper.addClass('xref-not-applying').removeClass('xref-applying');

		// Disable the form element (which one?)
		xref.prop('disabled',true);

	},

	/**
	 * Called by com_configbox.processServerResponse when automatic selection changes occured on the server. It calls
	 * com_configbox.updateElementSelection which delegates the job of changing all controls, overviews etc.
	 *
	 * @param {JsonResponses.configuratorUpdates.configurationChanges} changes - Instructions on what to add/change/remove
	 */
	processAutomaticSelections: function ( changes ) {

		if (changes.remove) {
			cbj.each(changes.remove,function(elementId) {
				com_configbox.updateElementSelection(elementId, 0, '');
			});
		}

		if (changes.add) {
			cbj.each(changes.add,function(elementId,item) {
				com_configbox.updateElementSelection(elementId, item.xrefId, item.text, item.outputValue );
			});
		}

	},

	/**
	 * This method is called for automated selection changes and updates anything that deals with the regarding
	 * element's selection.
	 *
	 * It delegates that work by firing the events systemChangedElementSelection and selectionHasChanged.
	 * Listeners for these events are supposed to reflect that selection visually (select the right radio button, update
	 * the overview etc). It sends data about the selection along with the event.
	 *
	 * @param {int} elementId - The element ID
	 * @param {int=} xrefId - The xref ID, if any
	 * @param {string=} text - The free text, if any
	 * @param {string=} outputValue - The selection text for the visitor
	 *
	 * @fires systemChangedElementSelection
	 * @fires selectionHasChanged
	 */
	updateElementSelection: function(elementId, xrefId, text, outputValue) {


		// console.log('updateElementSelection called: elementId = "'+elementId+'" ('+ com_configbox.elements[elementId].title +') with xrefId = "'+xrefId+'", text = "'+text+'" - outputValue = "'+outputValue+'" - ' + ((!xrefId && !text) ? 'REMOVAL':'CHANGE') );

		/**
		 * @event systemChangedElementSelection - Fired when the system makes changes (leaving the frontend out of date)
		 * @property {int} elementId - ID of element that gets a new selection
		 * @property {int} xrefId - New option assignment ID (if applicable, 0 otherwise)
		 * @property {string} text - New text value (if applicable, '' otherwise)
		 * @property {string} outputValue - The human-readable selection text (like option title or parsed text entry)
		 */
		cbj('#elementwrapper-'+elementId).trigger('systemChangedElementSelection', [elementId, xrefId, text, outputValue]);

		/**
		 * @event selectionHasChanged - Fired whenever a selection has changed (by the system or the user)
		 * @property {int} elementId - ID of element that gets a new selection
		 * @property {int} xrefId - New option assignment ID (if applicable, 0 otherwise)
		 * @property {string} text - New text value (if applicable, '' otherwise)
		 * @property {string} outputValue - The human-readable selection text (like option title or parsed text entry)
		 */
		cbj('#com_configbox').trigger('selectionHasChanged', [elementId, xrefId, text, outputValue]);

	},

	/**
	 * Handler to update element and xref prices in the configurator.
	 * Bound to the #com_configbox wrapper, firing on event 'pricingChanged'.
	 *
	 * @param {object} event - jQuery event object
	 * @param {JsonResponses.configuratorUpdates.pricing} pricing - All prices, taxes
	 *
	 * @listens Event:pricingChanged
	 */
	updatePricing: function(event, pricing) {

		cbp.start('update element pricing');

		cbj.each(pricing.elements,function(elementId,element){
			if (com_configbox.pricing.elements[elementId].price != element.price) {

				cbj('.element-price-' + elementId).html( element.priceFormatted );

				if (element.price == 0) {
					cbj('.element-price-' + elementId).closest('.element-price-wrapper').hide();
				}
				else {
					cbj('.element-price-' + elementId).closest('.element-price-wrapper').show();
				}

			}
			if (com_configbox.pricing.elements[elementId].priceRecurring != element.priceRecurring) {

				cbj('.element-price-recurring-' + elementId).html( element.priceRecurringFormatted );

				if (element.priceRecurring == 0) {
					cbj('.element-price-recurring-' + elementId).closest('.element-price-recurring-wrapper').hide();
				}
				else {
					cbj('.element-price-recurring-' + elementId).closest('.element-price-recurring-wrapper').show();
				}

			}
		});

		cbp.stop('update element pricing');

		cbp.start('update xref pricing');

		cbj.each(pricing.xrefs,function(xrefId,xref){
			if (com_configbox.pricing.xrefs[xrefId].price != xref.price) {
				// console.log('Updating price for xref ' + xrefId);

				cbj('.xref-price-' + xrefId).html( xref.priceFormatted );

				if (xref.price == 0) {
					cbj('.xref-price-' + xrefId).closest('.xref-price-wrapper').hide();
				}
				else {
					cbj('.xref-price-' + xrefId).closest('.xref-price-wrapper').show();
				}
			}
			if (com_configbox.pricing.xrefs[xrefId].priceRecurring != xref.priceRecurring) {

				cbj('.xref-price-recurring-' + xrefId).html( xref.priceRecurringFormatted );

				if (xref.priceRecurring == 0) {
					cbj('.xref-price-recurring-' + xrefId).closest('.xref-price-recurring-wrapper-recurring').hide();
				}
				else {
					cbj('.xref-price-recurring-' + xrefId).closest('.xref-price-recurring-wrapper').show();
				}

			}
		});

		cbp.stop('update xref pricing');

	},

	/**
	 * Called by com_configbox.processServerResponse to check if all required page elements have a selection
	 *
	 * @return {boolean} requiredMissing - true or false
	 */
	requiredElementsMissing: function() {

		var requiredMissing = false;

		cbj.each(com_configbox.elements, function(i,element) {

			if(element.applies && element.required == 1) {
				requiredMissing = (com_configbox.getElementValue(element.id) == null);
			}
			// Break the loop if missing
			if (requiredMissing) {
				return false;
			}
		});

		return requiredMissing;
	},

	/**
	 * Fills and displays the validation error div for element selections
	 */
	showValidationError: function( elementId , text) {
		cbj('#elementwrapper-' + elementId + ' .validation-feedback').html(text).fadeIn(200);
	},

	/**
	 * Hides and empties validation error div for element selections
	 */
	clearValidationError: function( elementId ) {
		cbj('#elementwrapper-' + elementId + ' .validation-feedback').fadeOut( 200, function() { cbj(this).empty(); } );
	},

	/**
	 * Method to show the loading symbol next to the control element
	 */
	showAjaxLoader: function( elementId, value) {

		elementId = parseInt(elementId);
		value = parseInt(value);

		var xrefWrapper = cbj('#elementwrapper-' + elementId + ' #xrefwrapper-' + value);

		if (xrefWrapper.length) {
			xrefWrapper.find('.position-loading-symbol').fadeIn(10);
		}
		else {
			cbj('#elementwrapper-'+elementId+' .position-loading-symbol').fadeIn(10);
		}

	},

	/**
	 * Method to hide the loading symbol next to the control element
	 */
	clearAjaxLoader: function( elementId, value ) {

		elementId = parseInt(elementId);
		if (value) {
			value = parseInt(value);
			var xrefWrapper = cbj('#xrefwrapper-' + value);

			if (xrefWrapper.length === 1) {
				xrefWrapper.find('.position-loading-symbol').fadeOut(10);
			}
		}
		else {
			cbj('#elementwrapper-'+ elementId +' .position-loading-symbol').fadeOut(10);
		}

	},

	/**
	 * Method to get the element ID for any jQuery collection with an element wrapper
	 */
	getElementId: function(input) {

		var elementId = cbj(input).closest('.element').attr('id').replace('elementwrapper-','');

		if (!elementId) {

			if (cbj(input).hasClass('element')) {
				return cbj(input).attr('id').replace('elementwrapper-','');
			}
			else {
				throw "com_configbox.getElementId() could not determine ID.";
			}

		}
		else {
			return elementId;
		}

	},

	/**
	 * Does nothing but setting the current element value in the elements array
	 * @param {int} elementId - The element ID
	 * @param {string|int} value - The value to be set for it
	 */
	setElementValue: function( elementId, value) {

		if (typeof(com_configbox.elements[elementId]) !== 'undefined') {
			com_configbox.elements[elementId].value = value;
		}

	},

	/**
	 * Does nothing but setting the current element value in the elements array
	 * @param {int} elementId - The element ID
	 */
	getElementValue: function( elementId ) {

		if (typeof(com_configbox.cartPositionDetails.elements[elementId]) === 'undefined' ) {
			return null;
		}
		else {
			return com_configbox.cartPositionDetails.elements[elementId].selection.value;
		}

	},

	/**
	 * Method to get element attributes from the element array
	 */
	getElementAttribute: function (elementId,attribute) {
		return com_configbox.elements[elementId][attribute];
	},

	/**
	 * Submits the payment method ID for the current order
	 *
	 * @param {int} id
	 * @returns {xhr}
	 */
	setPaymentOption : function(id) {

		// Prepare request base data
		var requestData = {
			lang		: com_configbox.langSuffix,
			option		: 'com_configbox',
			controller	: 'checkout',
			task		: 'storePaymentOption',
			format		: 'raw',
			id			: id
		};

		// Do the request, pass it back
		return cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type:'post'
		});

	},

	/**
	 * Submits the delivery method ID for the current order
	 *
	 * @param {int} id
	 * @returns {xhr}
	 */
	setDeliveryOption : function(id) {

		// Prepare request base data
		var requestData = {
			lang		: com_configbox.langSuffix,
			option		: 'com_configbox',
			controller	: 'checkout',
			task		: 'storeDeliveryOption',
			format		: 'raw',
			id			: id
		};

		// Do the request, pass it back
		return cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type:'post'
		});

	},

	refreshOrderAddress : function() {
		// Refresh the order address display and toggle
		cbj('.wrapper-order-address .order-address-display').load(com_configbox.addressViewUrl,function(){
			cbj('.wrapper-order-address .order-address-display').slideDown();
			cbj('.wrapper-order-address .order-address-form').slideUp();
		});
	},

	refreshDeliveryOptions : function() {
		cbj('.wrapper-delivery-options').load(com_configbox.deliveryViewUrl);
	},

	refreshPaymentOptions : function() {
		cbj('.wrapper-payment-options').load(com_configbox.paymentViewUrl);
	},

	refreshOrderRecord : function() {
		cbj('.wrapper-order-record').load(com_configbox.recordViewUrl);
	},

	refreshPositionDetails : function() {

	},

	placeOrder : function() {

		// Prepare request base data
		var requestData = {
			lang		: com_configbox.langSuffix,
			option		: 'com_configbox',
			controller	: 'checkout',
			task		: 'placeOrder',
			format		: 'raw'
		};

		// Do the request, pass it back
		return cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type:'post'
		});

	},

	requestPasswordChangeVerificationCode : function(email) {
		// Prepare request base data
		var requestData = {
			lang		: com_configbox.langSuffix,
			option		: 'com_configbox',
			controller	: 'user',
			task		: 'sendPasswordChangeVerificationCode',
			format		: 'raw',
			email		: email
		};

		// Do the request, pass it back
		return cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type:'post'
		});
	},

	/**
	 * Make the product tree (product tree structure in the backend) refresh. Careful - refreshes a part of the tree,
	 * namely the product node that had a change.
	 * The method expects a kenedo-form with the usual data on the page.
	 */
	refreshProductTree: function(recordId, recordType, productId, isProductInsert) {

		// No product tree, no refresh
		if (cbj('#view-adminproducttree').length == 0) {
			return;
		}

		// Get the URL for product tree updates
		var url = cbj('#view-adminproducttree .product-list').data('update-url');

		// Get the currently opened nodes in the tree and make it query string ready json
		var treeIds = encodeURIComponent(JSON.stringify( com_configbox.getOpenProductTreeNodeIds() ));

		if (com_configbox.platform == 'magento') {
			// Add the info to the url (only_product_id is there to get only the tree data of a single product)
			url += 'open_tree_ids/'+treeIds+'/only_product_id/'+productId;
		}
		else {
			// Append & or ? and the params later because we take it easy
			url += (url.indexOf('?') == -1) ? '?' : '&';

			// Add the info to the url (only_product_id is there to get only the tree data of a single product)
			url += 'open_tree_ids='+treeIds+'&only_product_id='+productId;
		}

		// On new products, add a list item so that reloading later less complicated
		if (isProductInsert) {
			cbj('.product-tree-wrapper .product-list .product-item.add-item').before('<li class="product-item" id="product-'+productId+'"></li>');
		}

		// Now load the view (insert only part of it)
		cbj('#product-' + productId).load(url + ' #product-' + productId + '>div', function() {

			// Run ready functions on the whole view (works out great luckily)
			Kenedo.runSubviewReadyFunctions('view-adminproducttree');

			// Apply address() on the new links so that they use pushState (see jquery.address)
			if (typeof(cbj.address) != 'undefined') {
				cbj('#product-' + productId + ' .ajax-target-link').address();
			}

			// Remove any 'active' CSS classes (they mark the currently selected item in the tree)
			cbj('.product-tree-wrapper .active').removeClass('active');

			// Mark the right item
			cbj('#'+recordType+'-' + recordId).addClass('active');

		});

	},

	/**
	 * Returns an object with the ids of currently open nodes in the product tree (admin area left side at 'Products')
	 *
	 * @returns {{products: Array, pages: Array, elements: Array}}
	 */
	getOpenProductTreeNodeIds: function() {

		var treeData = {
			products 	: [],
			pages		: [],
			elements	: []
		};

		cbj('.sub-list-trigger').each(function(i,item){
			if (cbj(item).is('.trigger-opened')) {

				var id = cbj(this).attr('id');

				if (id) {
					if (id.indexOf('product-trigger') != -1) {
						treeData.products.push(id.replace('product-trigger-',''));
					}
					if (id.indexOf('page-trigger') != -1) {
						treeData.pages.push(id.replace('page-trigger-',''));
					}
					if (id.indexOf('element-trigger') != -1) {
						treeData.elements.push(id.replace('element-trigger-',''));
					}
				}
			}
		});

		return treeData;

	}

};

/* CONFIGBOX JS PROFILER - START */

var cbp = {

	clocks : {},

	start : function( tag ) {

		if (!tag) {
			tag = 'default';
		}

		// console.log('clock for ' + tag + ' started.');

		if (typeof(this.clocks[tag]) === 'undefined') {
			this.clocks[tag] = {};
		}

		this.clocks[tag].start = this.getMs();
	},

	stop : function ( tag ) {

		if (!tag) {
			tag = 'default';
		}

		if (typeof(this.clocks[tag]) === 'undefined') {
			this.clocks[tag] = {};
		}

		this.clocks[tag].end = this.getMs();

		// console.log( this.getTime(tag) );

	},

	getTime : function ( tag ) {

		if (!tag) {
			tag = 'default';
		}

		if (typeof(this.clocks[tag]) === 'undefined') {
			return false;
		}

		return  (this.clocks[tag].end - this.clocks[tag].start) + 'ms: ' + tag;
	},

	getMs : function() {
		return new Date().getTime();
	}

};

/* CONFIGBOX JS PROFILER - END */

/* DELAYING LINK CLICKS ON RUNNING AJAX REQUESTS - START */

cbj(document).ajaxStart(function(){
	// Set the requestInProgress flag to true.
	if (typeof(com_configbox) === "undefined") {
		com_configbox = {};
	}
	com_configbox.requestInProgress = true;
});

cbj(document).ajaxStop(function(){

	// Set the requestInProgress flag to false
	com_configbox.requestInProgress = false;

	// Remove the ajax-loader span, wherever there are any
	cbj('#com_configbox .ajaxloader').fadeOut(500, function () {
		cbj(this).remove();
	});

	// In case a redirect is scheduled, do so if there are no errors to display
	if (com_configbox.isRequestError === false && com_configbox.redirectUrl != '') {
		window.location.href = com_configbox.redirectUrl;
	}

});

/* DELAYING LINK CLICKS ON RUNNING AJAX REQUESTS - END */


/* WIDGET CUSTOM FUNCTIONALITY - START */

/* ANY ELEMENT USING OPTIONS - START */
cbj(document).ready(function(){

	// CUSTOM VALUE CHANGE HANDLER
	cbj('#com_configbox').on('systemChangedElementSelection', '.element.type-dropdown, .element.type-radio, .element.type-checkbox', function (event, elementId, xrefId) {
		// Removal
		if (!xrefId) {
			cbj(this).find('.xref.selected').removeClass('selected');
			cbj(this).find('.configbox-control').prop('checked', false).prop('selected', false);
		}
		// Update
		if (xrefId) {
			cbj(this).find('#xrefwrapper-'+xrefId).addClass('selected').siblings().removeClass('selected');
			cbj(this).find('#xrefwrapper-'+xrefId+ ' .configbox-control').prop('checked', true).prop('selected', true);
		}
	});

});
/* ANY ELEMENT USING OPTIONS - END */

/* REGULAR TEXT WIDGET - START */
cbj(document).ready(function(){

	// CUSTOM VALUE CHANGE HANDLER
	cbj('#com_configbox').on('systemChangedElementSelection', '.widget-text', function ( event, elementId, xrefId, text ) {
		if (!text) {
			text = '';
		}
		cbj(this).find('.configbox-widget').val(text);
		cbj(this).find('.configbox-control-textfield').val(text);
	});

});

/* CALENDAR WIDGET - START */
cbj(document).ready(function(){

	// Init the calendars
	cbj('#com_configbox .widget-calendar').each(function(i,item){

		var elementId = com_configbox.getElementId(cbj(item));

		var parameters = {
			hideIfNoPrevNext: true,
			altField: '#configbox-widget-display-' + elementId,
			altFormat: com_configbox.lang.DATEFORMAT_CALENDAR_JS,
			onSelect: function(dateText) {
				cbj('#element-' + elementId ).val(dateText).change();
			}
		};

		if ( com_configbox.getElementAttribute(elementId,'validate') == 1 ) {

			var minVal = com_configbox.getElementAttribute(elementId,'minval');
			var maxVal = com_configbox.getElementAttribute(elementId,'maxval');

			if (minVal !== null && com_configbox.getElementAttribute(elementId,'calcmodel_id_min_val') == 0) {
				parameters.minDate = minVal;
			}
			if (maxVal !== null && com_configbox.getElementAttribute(elementId,'calcmodel_id_max_val') == 0) {
				parameters.maxDate = maxVal;
			}

		}

		// Init the calendar and set the date
		cbj(this).find('.configbox-widget-calendar').datepicker(parameters);

	});

	// Set click handlers for displaying the calendar
	cbj('#com_configbox').on('click','.configbox-widget-calendar-button, .configbox-widget-calendar-display',function(){
		if (cbj(this).closest('.element').hasClass('element-applying')) {
			cbj(this).closest('.element').find('.configbox-widget-calendar').datepicker('show');
		}
	});

	// CUSTOM VALUE CHANGE HANDLER
	cbj('#com_configbox').on('systemChangedElementSelection', '.widget-calendar', function( event, elementId, xrefId, text) {
		if (!text) {
			text = '';
		}
		cbj(this).find('.configbox-widget-calendar').datepicker('setDate',text);
	});

	// CUSTOM VALIDATION CHANGE HANDLER
	cbj('#com_configbox').on('changeValidationValues', '.widget-calendar', function( event, elementId, values) {

		cbj(this).find('.configbox-widget-calendar').datepicker('option','minDate',values.minval);
		cbj(this).find('.configbox-widget-calendar').datepicker('option','maxDate',values.maxval);
		cbj(this).find('.configbox-widget-calendar').datepicker('refresh');

	});

});
/* CALENDAR WIDGET - END */


/* SLIDER WIDGET - START */
cbj(document).ready(function(){

	// INIT
	cbj('#com_configbox .widget-slider').each(function(i,item){

		var elementId = com_configbox.getElementId(cbj(item));

		var displayElement = cbj('#configbox-widget-display-' + elementId);
		var controlElement = cbj('#element-' + elementId);

		// Get the parameters
		var parameters = {

			slide: function(event, ui) {
				displayElement.text(ui.value);
			},
			change: function(event, ui) {
				if(cbj(this).data('noRequest')) {
					controlElement.val(ui.value);
				}
				else {
					controlElement.val(ui.value).change();
				}

			},
			animate: true,
			value: com_configbox.getElementValue(elementId)
		};

		// Set min and max in case
		if ( com_configbox.getElementAttribute(elementId,'validate') == 1 ) {

			var minVal = com_configbox.getElementAttribute(elementId, 'minval');
			var maxVal = com_configbox.getElementAttribute(elementId, 'maxval');

			if (minVal !== null) {
				parameters.min = parseFloat(minVal);
			}
			if (maxVal !== null) {
				parameters.max = parseFloat(maxVal);
			}

		}

		// Set steps
		var steps = parseFloat(com_configbox.getElementAttribute(elementId,'slider_steps'));

		if (typeof(steps) === 'number' && steps !== 0) {
			parameters.step = steps;
		}

		// Work around issue with MooTools
		cbj(this).find('.configbox-widget-slider').each(function(i,item){
			item.slide = null;
		});

		// Init the slider
		cbj(this).find('.configbox-widget-slider').slider(parameters);

	});

	// CUSTOM VALUE CHANGE HANDLER
	cbj('#com_configbox').on('systemChangedElementSelection', '.widget-slider', function ( event, elementId, xrefId, text ) {

		if (!text) {
			text = 0;
		}
		cbj(this).find('.configbox-widget-slider').data('noRequest',true);
		cbj(this).find('.configbox-widget-slider').slider('value',text);
		cbj(this).find('.configbox-widget-slider').data('noRequest',false);
		cbj(this).find('.configbox-widget-slider-display').html(text);

	});

	// CUSTOM VALIDATION CHANGE HANDLER
	cbj('#com_configbox').on('changeValidationValues', '.widget-slider', function( event, elementId, values) {

		// Change type to float just in case
		values.minval = parseFloat(values.minval);
		values.maxval = parseFloat(values.maxval);

		cbj(this).find('.configbox-widget-slider').slider('option','min',values.minval);
		cbj(this).find('.configbox-widget-slider').slider('option','max',values.maxval);

	});
});
/* SLIDER WIDGET - END */


/* FILE UPLOAD WIDGET - START */
cbj(document).ready(function(){

	// File Upload: Show loading symbol on upload
	cbj('#com_configbox .configbox-widget-fileupload-form').submit(function(){
		com_configbox.showAjaxLoader(cbj(this));
	});

	// File Upload server comm functionality
	cbj('#com_configbox .element-widget-fileupload-iframe').on('load', function() {

		com_configbox.clearAjaxLoader(cbj(this));

		var elementId = com_configbox.getElementId(cbj(this));

		// That object is set from within the iframe, see ConfigboxControllerJson::sendResponse for reference
		// Map over to data..
		var data = com_configbox.configuratorFileUploadResponseData;
		// ..then remove the object to avoid any badness from stale data
		com_configbox.configuratorFileUploadResponseData = {};

		if(data) {
			cbj('#element-' + elementId).val(data.requestedChange.text);
			cbj('#configbox-widget-display-' + elementId).html(data.requestedChange.outputValue);
			// Trigger data received to continue the normal workflow
			/**
			 * @event serverResponseReceived
			 */
			cbj('#com_configbox').trigger('serverResponseReceived', [data] );
			/**
			 * @event configurationUpdated
			 */
			cbj('#com_configbox').trigger('configurationUpdated');
		}

	});

	// CUSTOM VALUE CHANGE HANDLER
	cbj('#com_configbox').on('systemChangedElementSelection', '.widget-fileupload', function( event, elementId, xrefId, text ) {

		if (!text) {
			text = '';
		}

		cbj(this).find('.configbox-widget-fileupload-display').html(text);
		cbj(this).find('.configbox-widget-fileupload-filefield').val('');

	});

	// CUSTOM ELEMENT ACTIVATION HANDLER
	cbj('#com_configbox').on('activateElement', '.widget-fileupload', function() {
		cbj(this).find(':input').prop('disabled',false);
	});

	// CUSTOM ELEMENT DEACTIVATION HANDLER
	cbj('#com_configbox').on('deactivateElement', '.widget-fileupload', function() {
		cbj(this).find(':input').prop('disabled',true);
	});

});
/* FILE UPLOAD WIDGET - END */


/* DROP DOWN "WIDGET" - START */
cbj(document).ready(function(){

	// Dropdown open functionality
	cbj('#com_configbox').on('click','.element-applying .configbox-dropdown-trigger', function(event) {
		cbj(this).closest('.element').find('.xref-list').show();
		event.stopPropagation();
		event.preventDefault();
	});

	// Dropdown close functionality
	cbj(document).click(function(){
		cbj('.element.type-dropdown .xref-list').hide();
	});

	// Keep the dropdown trigger text for later
	cbj('#com_configbox .configbox-dropdown-trigger-default').each(function(i,item){
		cbj(item).closest('.element').data('triggerDefault', cbj(item).clone(false));
	});

	// Init the dropdown trigger text
	cbj('.element.type-dropdown .selected .configbox-label-dropdown').each(function(i,item){
		var el = cbj(item).clone(false);
		el.find('.xref-description').remove();

		cbj(item).closest('.element').find('.configbox-dropdown-trigger').empty().append(el);
	});

	// Update the shown dropdown xref on change
	cbj('.element.type-dropdown .configbox-control-dropdown').change(function(){

		var el = cbj(this).closest('.xrefwrapper').find('.configbox-label-dropdown').clone(false);
		el.find('.xref-description').remove();
		cbj(this).closest('.element').find('.configbox-dropdown-trigger').empty().append(el);

	});

	// CUSTOM VALUE CHANGE HANDLER
	cbj('#com_configbox').on('systemChangedElementSelection', '.element.type-dropdown', function ( event, elementId, xrefId ) {
		// Update the shown dropdown xref on change by system to default
		if (!xrefId) {
			cbj(this).find('.configbox-dropdown-trigger').html( cbj(this).data('triggerDefault') );
		}
		else {
			var html = cbj(this).find('.xref.selected .configbox-label-dropdown').html();
			cbj(this).find('.configbox-dropdown-trigger').html( html );
		}
	});
});
/* DROP DOWN "WIDGET" - END */


/* CHOICES WIDGET - START */
cbj(document).ready(function(){

	// Deal with clicks on choice radio buttons, pretend they are regular radio buttons
	cbj('#com_configbox').on('change','.configbox-choice-field',function() {

		var value = cbj(this).val();
		var elementId = Number;

		if (value !== 'custom') {
			elementId = com_configbox.getElementId(cbj(this));
			cbj('#element-' + elementId).val(value).change();
		}
		else {
			var freeValue = cbj(this).closest('.choice-list').find('.configbox-choice-free-field').val();
			if (freeValue) {
				elementId = com_configbox.getElementId(cbj(this));
				cbj('#element-' + elementId).val(freeValue).change();
			}

		}
	});

	// Select the free entry field once the user clicks in it
	cbj('#com_configbox').on('focus','.configbox-choice-free-field',function() {
		cbj(this).closest('li').find('.configbox-choice-field').prop('checked',true);
	});

	// Store the value when typing in the free entry field
	cbj('#com_configbox').on('keyup','.configbox-choice-free-field',function() {
		var value = cbj(this).val();
		var elementId = com_configbox.getElementId(cbj(this));
		cbj('#element-' + elementId).val(value).change();
	});

	// CUSTOM VALUE CHANGE HANDLER
	cbj('#com_configbox').on('systemChangedElementSelection', '.widget-choices', function( event, elementId, xrefId, text) {

		if (!text) {
			text = '';
		}

		// We got a text, so that means we're making a selection
		if (text) {

			// Uncheck all radio buttons first (we'll check the right one in the following code)
			cbj(this).find('.configbox-choice-field').prop('checked', false);

			// If there's a radio button with a matching val attribute value, we can be sure it's a choice we check
			if (cbj(this).find('.configbox-choice-field[val="'+text+'"]').length) {
				// Now check the right one
				cbj(this).find('.configbox-choice-field[val="'+text+'"]').prop('checked', true);
			}
			// Otherwise it gotta be a custom value
			else {
				// Check the custom radio button..
				cbj(this).find('.configbox-choice-field[val="custom"]').prop('checked', true);
				// ..and fill in the value
				cbj(this).find('.configbox-choice-free-field').val(text);

			}

		}
		// No text gotta mean it's removing any selection that may have been made earlier
		else {
			cbj(this).find('.configbox-choice-field').prop('checked', false);
			cbj(this).find('.configbox-choice-free-field').val('');
		}

	});

	// CUSTOM ELEMENT ACTIVATION HANDLER
	cbj('#com_configbox').on('activateElement', '.widget-choices', function() {
		cbj(this).find(':input').prop('disabled',false);
	});

	// CUSTOM ELEMENT DEACTIVATION HANDLER
	cbj('#com_configbox').on('deactivateElement', '.widget-choices', function() {
		cbj(this).find(':input').prop('disabled',true);
	});

});
/* CHOICES WIDGET - END */

/* POPUP PICKER WIDGET - START */
cbj(document).ready(function(){

	// Text Field: Picker functionality
	cbj('.value-picker').click(function(event){

		event.preventDefault();

		if (cbj(this).closest('.element').hasClass('element-not-applying')) {
			return;
		}

		var params = {
			transition 		: 'fade',
			href 			: cbj(this).attr('href'),
			overlayClose	: true,
			iframe 			: true,
			fastIframe		: false,
			width			: '1200px',
			height			: '700px'
		};

		cbj.colorbox(params);
	});

});
/* POPUP PICKER WIDGET - END */


/* WIDGET CUSTOM FUNCTIONALITY - START */

/* CONFIGBOX RECOMMENDATIONS - START */

cbj(document).ready(function(){

	// Show the recommendation form
	cbj('#com_configbox .recommend-bundle-button').click(function(){
		cbj('#com_configbox #view-bundle .bundle-recommendation').slideDown();
		cbj('#com_configbox #view-bundle .recommendation-send').show();
	});

	// Hide the recommendation form
	cbj('#com_configbox .recommendation-cancel, #com_configbox .recommendation-close').click(function(){
		cbj('#com_configbox #view-bundle .bundle-recommendation .feedback').text('');
		cbj('#com_configbox #view-bundle .bundle-recommendation').slideUp();
	});

	// Send a recommendation
	cbj('#com_configbox .recommendation-send').click(function(){

		var form = cbj(this).closest('.recommendation-form');
		form.find('.feedback span').hide();

		var formData = {};
		formData.bundle_id = form.find('#recommend-bundle-id').val();
		formData.name = form.find('#recommend-sender-name').val();
		formData.senderEmail = form.find('#recommend-sender-email').val();
		formData.email = form.find('#recommend-email').val();
		formData.message = form.find('#recommend-message').val();

		var formOk = true;

		cbj.each(formData,function(key,value){

			if (cbj.trim(value) == '') {
				form.find('.feedback .fields-empty').show().siblings().hide();
				formOk = false;
				return false;
			}

			return true;

		});

		formData.recaptcha_challenge_field = form.find('#recaptcha_challenge_field').val();
		formData.recaptcha_response_field = form.find('#recaptcha_response_field').val();

		if (formOk) {

			form.find('.feedback span').hide();

			var requestData = {
				lang		: com_configbox.langSuffix,
				option		: 'com_configbox',
				controller	: 'bundle',
				task		: 'recommendBundle',
				format		: 'json',
				bundle_id	: formData.bundle_id,
				name		: formData.name,
				senderEmail	: formData.senderEmail,
				email		: formData.email,
				message:	formData.message,
				recaptcha_challenge_field	: formData.recaptcha_challenge_field,
				recaptcha_response_field	: formData.recaptcha_response_field

			};

			cbj.ajax({
				url: com_configbox.entryFile,
				data: requestData,
				dataType: 'json',

				success: function(data) {
					form.find('.feedback').text(data.text);

					if (data.success === true) {
						cbj('#com_configbox #view-bundle .recommendation-send').hide();
						cbj('#com_configbox #view-bundle .bundle-recommendation').hide();
						alert(data.text);
					}

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				},

				beforeSend: function(XMLHttpRequest) {

				},

				complete: function(XMLHttpRequest, textStatus) {

				}

			});

		}

	});
});
/* CONFIGBOX RECOMMENDATIONS - END */


/* CONFIGBOX REVIEWS - START */

cbj(document).ready(function(){

	cbj('#com_configbox').on('mousewheel', '.kenedo-popup .height-limiter', function(e) {
		var delta = e.originalEvent.wheelDelta || -e.originalEvent.detail;
		cbj(this).scrollTop(cbj(this).scrollTop() - delta);
		return false;
	});

	cbj('#com_configbox .listing-product-reviews .kenedo-popup-trigger').on('popup-open',function(){

		if (cbj(this).data('dataLoaded') === true) {
			return true;
		}

		var id = cbj(this).closest('.listing-product-reviews').data('product-id');

		cbj(this).data('dataLoaded',true);

		var requestData = {
			option	: 'com_configbox',
			view	: 'review',
			layout	: 'popup',
			lang	: com_configbox.langSuffix,
			format	: 'raw',
			tmpl	: 'component',
			kind	: 'product',
			id		: id
		};

		cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'html',
			context: cbj(this),

			success: function(data) {
				cbj(this).find('.kenedo-popup-content-inner').html(data);
			}

		});

	});

	cbj('#com_configbox .xref-reviews .kenedo-popup-trigger').on('popup-open',function(){

		if (cbj(this).data('dataLoaded') === true) {
			return true;
		}

		var xrefId = cbj(this).closest('.xrefwrapper').attr('id').replace('xrefwrapper-','');
		cbj(this).data('dataLoaded',true);

		var requestData = {
			option	: 'com_configbox',
			view	: 'review',
			layout	: 'popup',
			lang	: com_configbox.langSuffix,
			format	: 'raw',
			tmpl	: 'component',
			kind	: 'xref',
			id		: xrefId
		};

		cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'html',

			success: function(data) {
				cbj('#com_configbox #xrefwrapper-'+xrefId+' .xref-reviews .kenedo-popup-content-inner').html(data);
			}

		});

	});

	cbj('#com_configbox').on('click','.trigger-show-review-form', function(){
		cbj(this).closest('.review-page').find('.review-page-with-description .short-review-list').hide();
		cbj(this).closest('.review-page').find('.review-page-with-description .review-form').show();
	});

	cbj('#com_configbox').on('click','.trigger-cancel-review', function(){
		cbj(this).closest('.review-page').find('.review-page-with-description .short-review-list').show();
		cbj(this).closest('.review-page').find('.review-page-with-description .review-form').hide();
	});

	cbj('#com_configbox').on('click','.trigger-read-reviews', function(){
		cbj(this).closest('.review-page').find('.review-page-long-list').show();
		cbj(this).closest('.review-page').find('.review-page-with-description').hide();
	});

	cbj('#com_configbox').on('click','.trigger-back-to-description', function(){
		cbj(this).closest('.review-page').find('.review-page-long-list').hide();
		cbj(this).closest('.review-page').find('.review-page-with-description').show();
	});

	cbj('#com_configbox').on('mousemove click','.review-form .rating-star', function(event){

		var x = event.pageX - cbj(this).offset().left;
		var w = cbj(this).innerWidth();
		var wr = x / w * 100;
		var stars;

		if (wr < 50) {
			stars = cbj(this).index() + 0.5;
		}
		else {
			stars = cbj(this).index() + 1.0;
		}

		cbj(this).closest('.rating-stars').data('rating',stars);

		for (var i = 1; i <= 5; i = i + 0.5) {

			if (i < stars) {
				cbj(this).closest('.rating-stars').find('.rating-star').eq( Math.floor(i) ).addClass('full-star').removeClass('half-star').removeClass('empty-star');
			}
			if (i === stars) {
				cbj(this).closest('.rating-stars').find('.rating-star').eq( Math.floor(i) ).addClass('half-star').removeClass('full-star').removeClass('empty-star');
			}
			if (i > stars) {
				cbj(this).closest('.rating-stars').find('.rating-star').eq( Math.floor(i) ).addClass('empty-star').removeClass('half-star').removeClass('full-star');
			}

		}

	});

	cbj('#com_configbox').on('click', '.trigger-send-review', function(){

		var kind = cbj(this).data('kind');
		var id = cbj(this).data('itemid');
		var name = cbj.trim(cbj(this).closest('.review-form').find('.name input').val());
		var comment = cbj.trim(cbj(this).closest('.review-form').find('.comment textarea').val());
		var rating = cbj(this).closest('.review-form').find('.rating-stars').data('rating');

		var borderColorName = '';
		var borderColorComment = '';

		if (!name) {
			borderColorName = 'red';
		}

		if (!comment) {
			borderColorComment = 'red';
		}

		cbj(this).closest('.review-form').find('.name input').css('border-color', borderColorName);
		cbj(this).closest('.review-form').find('.comment textarea').css('border-color', borderColorComment);

		if (name && comment) {

			var requestData = {
				lang		: com_configbox.langSuffix,
				option		: 'com_configbox',
				controller 	: 'review',
				task		: 'storeReview',
				format		: 'raw',
				kind		: kind,
				id			: id,
				name		: name,
				comment		: comment,
				rating		: rating
			};

			cbj.ajax({
				url: com_configbox.entryFile,
				data: requestData,
				dataType: 'json',

				success: function() {
					cbj(this).closest('.review-form').find('.form-part').hide();
					cbj(this).closest('.review-form').find('.feedback-part').show();
				}

			});

		}

	});


});

/* CONFIGBOX REVIEWS - END */


/* CONFIGBOX DASHBOARD - START */

Kenedo.registerSubviewReadyFunction('view-admin', function(){

	cbj(document).ready(function(){

		// Update news and software update check
		KenedoDashboard.getUpdate();

		// Set up the toggles in the middle
		cbj('.toggle-wrapper').on('click', '.toggle-handle', function(){
			cbj(this).closest('.toggle-wrapper').find('.toggle-content').toggle();
			cbj(this).toggleClass('opened');
		});

		// Even out the height of the three boxes
		var height = cbj('.left-part').outerHeight();
		cbj('.news').outerHeight(height);
		cbj('.configbox-mainmenu').css('min-height',parseInt(height) - 27);

	});

});

var KenedoDashboard = {

	getUpdate : function() {

		var data = KenedoDashboard.getDataFromCache();
		if (data) {
			KenedoDashboard.insertData(data);
			return;
		}

		var parameters = {
			'platform': com_configbox.platform,
			'lang': com_configbox.langSuffix,
			'version': com_configbox.version
		};

		cbj.ajax({
			url: 'https://www.configbox.at/external/dashboard/dashboard.php',
			dataType: 'jsonp',
			crossDomain: true,
			data: parameters,
			success: function(data){
				KenedoDashboard.cacheData(data);
				KenedoDashboard.insertData(data);
			},
			error: function() {
				cbj('#view-admindashboard .news').html('Cannot load news at this time.');
			}

		});

	},

	cacheData: function(data) {
		if (typeof(sessionStorage) != 'undefined') {
			sessionStorage.updateInfo = JSON.stringify(data);
		}
	},

	getDataFromCache: function() {
		if (typeof(sessionStorage) != 'undefined' && typeof(sessionStorage.updateInfo) != 'undefined') {
			return JSON.parse(sessionStorage.updateInfo);
		}
	},

	/**
	 * Inserts json-derived data into the dashboard
	 * @param {JsonResponses.dashboardData} data
	 */
	insertData: function(data) {
		cbj('#view-admindashboard .news .news-target').html(data.news);
		cbj('#view-admindashboard .news').css('visibility','visible');

		cbj('.checking-for-update').hide();

		if (data.softwareUpdate.url) {
			cbj('.software-update-link').attr('href',data.softwareUpdate.url);
		}

		if (data.softwareUpdate.patchLevel) {
			cbj('.latest-version-patchlevel').text(data.softwareUpdate.patchLevel);
			cbj('.patchlevel-update-available').show();
		}

		if (data.softwareUpdate.mayor) {
			cbj('.latest-version-mayor').text(data.softwareUpdate.mayor);
			cbj('.mayor-update-available').show();
		}

		if (!data.softwareUpdate.mayor && !data.softwareUpdate.patchLevel) {
			cbj('.no-update-available').show();
		}
	}

};


/* CONFIGBOX DASHBOARD - END */


/* ORDER FILE UPLOADS - START */

Kenedo.registerSubviewReadyFunction('view-adminorder',function(){

	// Shows the file upload form on click on the upload button
	cbj('.order-files .upload-file-toggle').click(function(){
		cbj(this).hide();
		cbj('.upload-file-content').slideDown(100);
	});

	// Hides the file upload form on click of the cancel button
	cbj('.order-files .upload-file-content .form-cancel').click(function(){
		cbj('.order-files .upload-file-toggle').show();
		cbj(this).closest('.upload-file-content').slideUp(100);
	});

	// Sends the file (using the iframe method) on click of the send button
	cbj('.order-files .upload-file-content .form-send').click(function(){
		cbj(this).closest('form').submit();
	});

	// Processes the file upload form submit (iframe method)
	cbj('.order-files #file-upload-iframe').on('load', function(){

		if (com_configbox.addOrderFileResponseData.success) {

			var li = cbj('#order-file-blueprint').clone();
			li.attr('id','file-' + com_configbox.addOrderFileResponseData.fileId).find('.file-name').text(com_configbox.addOrderFileResponseData.fileName).attr('title',com_configbox.addOrderFileResponseData.comment);
			cbj('.order-file-list').append(li);
			cbj('.order-files .upload-file-content .form-cancel').click();
		}
		else {
			alert(com_configbox.addOrderFileResponseData.message);
		}

	});

	// Triggers the file download on click of the file link
	cbj('.order-files .order-file-list').on('click','.file-name',function(){
		var id = cbj(this).closest('.order-file').attr('id').replace('file-','');
		window.location.href = 'index.php?option=com_configbox&controller=userorder&task=getOrderFile&format=raw&fileId='+id;
	});

	// Makes a file getting deleted on the file delete button
	cbj('.order-files').on('click', '.file-delete', function(){

		var id = cbj(this).closest('.order-file').attr('id').replace('file-','');

		var requestData = {
			lang: com_configbox.langSuffix,
			option: 'com_configbox',
			controller: 'userorder',
			format: 'raw',
			task: 'deleteOrderFile',
			fileId:	id
		};

		cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',

			success: function(data) {
				if (data.success == true) {
					cbj('#file-'+id).remove();
				}
				else {
					alert(data.message);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(textStatus);
				console.log(errorThrown);
			}
		});

	});

});

cbj(document).ready(function(){

	if (cbj('#view-userorder').length) {
		Kenedo.runSubviewReadyFunctions('view-adminorder');
	}

	if (cbj('#view-inforequest').length) {
		window.parent.cbj.colorbox.resize({
			innerHeight: cbj('body').outerHeight() + 20 + 'px'
		});
	}

	if (cbj('#view-user #layout-login').length) {
		cbj('#configbox_email').focus();
	}

	if (cbj('#view-user #layout-register').length) {
		cbj('#configbox_firstname').focus();
	}

	cbj('.button-cancel-info-request').click(function(){
		if (typeof(window.parent.cbj) != 'undefined') {
			window.parent.cbj.colorbox.close();
		}
	});

	cbj('#modalbox table td').click(function() {

		var value = cbj(this).text();

		var target = cbj(this).closest('#modalbox').data('target');
		var displayTarget = cbj(this).closest('#modalbox').data('displayTarget');

		window.parent.cbj('#'+target).val(value);
		window.parent.cbj('#'+displayTarget).html(value);
		window.parent.cbj('#'+target).trigger('change');
		window.parent.cbj.colorbox.close();

	});

});

/* ORDER FILE UPLOADS - END */

/* KENEDO MODAL - START */

cbj(document).ready(function(){

	cbj(document).bind('cbox_open', function(){
		cbj('body').css('overflow','hidden');
	});

	cbj(document).bind('cbox_closed', function(){
		cbj('body').css('overflow','auto');
	});

	cbj('body').on('click', '.rovedo-modal', function(event){
		event.preventDefault();

		var params = {
			transition 		: 'fade',
			href 			: cbj(this).attr('href'),
			overlayClose 	: true,
			iframe 			: true,
			fastIframe		: false,
			width			: cbj(this).data('modal-width'),
			height			: cbj(this).data('modal-height'),
			opacity			: com_configbox.modalOverlayOpacity
		};

		cbj.colorbox(params);

	});

});

/* KENEDO MODAL - END */

/* CHECKOUT PAGE - START */

cbj(document).ready(function(){

	// Show the order address form
	cbj('#com_configbox').on('click', '.trigger-show-order-address-form', function(){
		cbj('.wrapper-order-address .order-address-display').slideUp();
		cbj('.wrapper-order-address .order-address-form').slideDown();
	});

	// Store the order address
	cbj('#com_configbox').on('click', '.trigger-save-order-address', function(){

		// Add the spinner to the button
		cbj(this).addClass('processing');

		// Get the customer data from the customer form
		var requestData = com_configbox.getCustomerFormData();

		// Add the application parameters
		requestData.option = 'com_configbox';
		requestData.controller = 'checkout';
		requestData.task = 'storeOrderAddress';
		requestData.format = 'raw';
		requestData.lang = com_configbox.langSuffix;
		requestData.comment = cbj('#comment').val();

		// Submit the order address information
		cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type: 'post',
			context: cbj(this)
		})

			// When done..
			.done(function(response) {

				// Remove the spinner
				cbj('.trigger-save-order-address').removeClass('processing');

				// Show validation issues (or errors)
				if (response.success === false) {
					if (response.validationIssues.length) {
						com_configbox.displayValidationIssues(response.validationIssues);
						return;
					}
					else {
						alert(response.errors.join("\n"));
						return;
					}
				}

				// Remove all invalid classes from fields
				cbj('.order-address-field').removeClass('invalid').removeClass('valid');

				// Refresh the rest of the checkout sub-views
				com_configbox.refreshOrderAddress();
				com_configbox.refreshDeliveryOptions();
				com_configbox.refreshPaymentOptions();
				com_configbox.refreshOrderRecord();
				com_configbox.refreshPositionDetails();

			});

	});

	// Store delivery method
	cbj('#com_configbox').on('change','#subview-delivery .option-control',function(){
		cbj(this).closest('li').addClass('processing');
		var id = cbj(this).val();
		com_configbox.setDeliveryOption(id)
			.done(function() {
				// Refresh the sub views
				com_configbox.refreshPaymentOptions();
				com_configbox.refreshOrderRecord();
				cbj('#subview-delivery li.processing').removeClass('processing');
			});
	});

	// Store payment method
	cbj('#com_configbox').on('change','#subview-payment .option-control',function(){
		cbj(this).closest('li').addClass('processing');
		var id = cbj(this).val();
		com_configbox.setPaymentOption(id)
			.done(function() {
				// Refresh the sub views
				com_configbox.refreshOrderRecord();
				cbj('#subview-payment li.processing').removeClass('processing');
			});
	});

	// Place the order
	cbj('#com_configbox').on('click','.trigger-place-order',function(){

		// Don't process if clicked already
		if (cbj(this).hasClass('processing')) {
			return;
		}
		// Add the css class flag
		cbj(this).addClass('processing');

		var termsAgreementMissing = false;
		var refundsAgreementMissing = false;

		if (com_configbox.agreeToTerms) {
			var agreed = cbj('#agreement-terms').prop('checked');
			if (agreed == false) {
				termsAgreementMissing = true;
			}
		}
		if (com_configbox.agreeToRefundPolicy) {
			var agreedRp = cbj('#agreement-refund-policy').prop('checked');
			if (agreedRp == false) {
				refundsAgreementMissing = true;
			}
		}

		var agreementsMissingMessage = '';

		// If both terms and refund policy missing
		if (termsAgreementMissing && refundsAgreementMissing) {
			agreementsMissingMessage = com_configbox.lang.checkoutPleaseAgreeBoth;
		}
		// If the terms missing
		if (termsAgreementMissing && !refundsAgreementMissing) {
			agreementsMissingMessage = com_configbox.lang.checkoutPleaseAgreeTerms;
		}
		// If the refund policy missing
		if (!termsAgreementMissing && refundsAgreementMissing) {
			agreementsMissingMessage = com_configbox.lang.checkoutPleaseAgreeRefundPolicy;
		}

		// Show and bounce if agreements are missing
		if (agreementsMissingMessage) {
			cbj(this).removeClass('processing');
			alert(agreementsMissingMessage);
			return false;
		}

		// Place the order (set's the status to 'ordered');
		com_configbox.placeOrder()
			.done(function(response){

				if (response.success == false) {
					cbj('.trigger-place-order').removeClass('processing');
					alert(response.errors.join("\n"));
				}
				else {
					// Then load the PSP bridge and get going with the payment
					cbj('.wrapper-psp-bridge').load(com_configbox.pspBridgeViewUrl,function(){
						cbj('.trigger-redirect-to-psp').trigger('click');
					});
				}
			});

	});

	// Make sure firing click events on the .trigger-redirect-to-psp link redirect (apparantly there are cases where
	// it's not the case). After order placement the system triggers that click.
	cbj('#com_configbox').on('click', '.trigger-redirect-to-psp', function(){
		if (typeof(cbj(this).attr('href')) != 'undefined') {
			window.location = cbj(this).attr('href');
		}
	});

});

/* CHECKOUT PAGE - END */


/* CONFIGBOX VISUALIZATION IMAGE PRELOADER - START */
cbj(document).ready(function(){

	if (cbj('#view-configurator-page').length != 0) {

		com_configbox.preloadVisualizationDelay = 1000;

		com_configbox.preloadVisualization = function(){
			cbj('.preload-image').each(function(i,item){
				item.src = cbj(item).data('src');
			});
		};

		// Start a timeout to work around situations where the load event is not fired(not sure anymore but some browsers to not
		// trigger the load event when stuff comes from cache only
		com_configbox.preloadTimeout = setTimeout(com_configbox.preloadVisualization, com_configbox.preloadVisualizationDelay);

		// Postpone the image preload for as long regular images are loading
		cbj('img:not(.preload-image)').on('load',function(){
			clearTimeout(com_configbox.preloadTimeout);
			com_configbox.preloadTimeout = setTimeout(com_configbox.preloadVisualization, com_configbox.preloadVisualizationDelay);
		});

	}

});
/* CONFIGBOX VISUALIZATION IMAGE PRELOADER - END */


/* BLOCK PRICING - START */

cbj(document).ready(function(){

	// Assign handler for configuration changes
	cbj('#com_configbox').on('selectionHasChanged', com_configbox.blockPricing.updateSelection);

	// Assign handler for price updates
	cbj('#com_configbox').on('pricingChanged', com_configbox.blockPricing.updatePricing);

	// Click handler for the page title -> hide and show elements
	cbj('.block-pricing').on('click', '.configurator-page-title', com_configbox.blockPricing.toggleSelectionsVisibility);

});

window.com_configbox.blockPricing = {

	/**
	 * Toggles visibility of the list of selections.
	 * @listens click on configurator pages
	 */
	toggleSelectionsVisibility : function() {

		// If page is empty don't proceed
		if (cbj(this).closest('.no-elements').length) {
			return;
		}

		// Toggle the page pricing
		cbj(this).find('.pricing-configurator-page').slideToggle(100);

		// Toggle the element list
		cbj(this).closest('.configurator-page').find('.element-list').slideToggle(100,function(){
			cbj(this).closest('.configurator-page').toggleClass('configurator-page-expanded');
		});
	},

	/**
	 * Handler to update prices in the overview block.
	 * Bound to the #com_configbox wrapper, firing on event 'pricingChanged'.
	 * @param {object} event - jQuery event object
	 * @param {JsonResponses.configuratorUpdates.pricing} pricing
	 */
	updatePricing: function (event, pricing) {

		cbp.start('do price module update');

		cbj('.pricing-regular .item-quantity').html(pricing.quantity);
		cbj('.pricing-recurring .item-quantity').html(pricing.quantity);

		cbj('.pricing-regular .pricing-per-item-total').html(pricing.total.pricePerItemFormatted);
		cbj('.pricing-recurring .pricing-per-item-total').html(pricing.total.pricePerItemRecurringFormatted);

		cbj('.pricing-regular .pricing-total').html(pricing.total.priceFormatted);
		cbj('.pricing-recurring .pricing-total').html(pricing.total.priceRecurringFormatted);

		cbj.each(pricing.pages,function(pageId,page){

			if (com_configbox.pricing.pages[pageId].price != page.price) {
				cbj('.pricing-regular .pricing-configurator-page-' + pageId).html( page.priceFormatted );
			}
			if (com_configbox.pricing.pages[pageId].priceRecurring != page.priceRecurring) {
				cbj('.pricing-recurring .pricing-configurator-page-' + pageId).html( page.priceRecurringFormatted );
			}

		});

		cbj.each(pricing.elements,function(elementId,element){
			if (com_configbox.pricing.elements[elementId].price != element.price) {
				cbj('.pricing-regular .pricing-element-' + elementId).html( element.priceFormatted );
			}

			if (com_configbox.pricing.elements[elementId].priceRecurring != element.priceRecurring) {
				cbj('.pricing-recurring .pricing-element-' + elementId).html( element.priceRecurringFormatted );
			}
		});


		// Update price per item regular
		cbj('.pricing-regular .pricing-per-item-net').html(pricing.total.pricePerItemNetFormatted);
		cbj('.pricing-regular .pricing-per-item-tax').html(pricing.total.pricePerItemTaxFormatted);
		cbj('.pricing-regular .pricing-per-item-gross').html(pricing.total.pricePerItemGrossFormatted);

		// Update price per item recurring
		cbj('.pricing-recurring .pricing-per-item-net').html(pricing.total.pricePerItemRecurringNetFormatted);
		cbj('.pricing-recurring .pricing-per-item-tax').html(pricing.total.pricePerItemRecurringTaxFormatted);
		cbj('.pricing-recurring .pricing-per-item-gross').html(pricing.total.pricePerItemRecurringGrossFormatted);

		// Update regular product totals
		cbj('.pricing-regular .pricing-total-net').html(pricing.total.priceNetFormatted);
		cbj('.pricing-regular .pricing-total-tax').html(pricing.total.priceTaxFormatted);
		cbj('.pricing-regular .pricing-total-gross').html(pricing.total.priceGrossFormatted);

		// Update recurring product totals
		cbj('.pricing-recurring .pricing-total-net').html(pricing.total.priceRecurringNetFormatted);
		cbj('.pricing-recurring .pricing-total-tax').html(pricing.total.priceRecurringTaxFormatted);
		cbj('.pricing-recurring .pricing-total-gross').html(pricing.total.priceRecurringGrossFormatted);

		// Update total plus shipping and delivery
		cbj('.pricing-total-plus-extras-net').html(pricing.totalPlusExtras.priceTaxFormatted);
		cbj('.pricing-total-plus-extras-tax').html(pricing.totalPlusExtras.priceNetFormatted);
		cbj('.pricing-total-plus-extras-gross').html(pricing.totalPlusExtras.priceGrossFormatted);

		// Update taxes
		if (pricing.taxesFormatted) {
			cbj.each(pricing.taxesFormatted,function(taxRate,taxAmount){
				var strTaxRate = String(taxRate);
				cbj('.pricing-regular .pricing-taxrate-' + strTaxRate.replace('.','-')).html(taxAmount);
			});
		}

		// Update delivery data
		if (pricing.delivery) {
			cbj('.best-delivery-title').text(pricing.delivery.title);
			cbj('.pricing-total-delivery-net').html(pricing.delivery.priceNetFormatted);
			cbj('.pricing-total-delivery-tax').html(pricing.delivery.priceTaxFormatted);
			cbj('.pricing-total-delivery-gross').html(pricing.delivery.priceGrossFormatted);
			cbj('.delivery-cost').each(function(){
				if (pricing.delivery.priceGross === 0) {
					cbj(this).slideUp();
				}
				else {
					cbj(this).slideDown();
				}
			});
		}

		// Update payment option data
		if (pricing.payment) {
			cbj('.best-payment-title').text(pricing.payment.title);
			cbj('.pricing-total-payment-net').html(pricing.payment.priceNetFormatted);
			cbj('.pricing-total-payment-tax').html(pricing.payment.priceTaxFormatted);
			cbj('.pricing-total-payment-gross').html(pricing.payment.priceGrossFormatted);
			cbj('.payment-cost').each(function(){
				if (pricing.payment.priceGross === 0) {
					cbj(this).slideUp();
				}
				else {
					cbj(this).slideDown();
				}
			});
		}

		cbj('.pricing-quantity').text(pricing.quantity);

		// Show/hide the total per item lines
		if (pricing.quantity > 1) {

			cbj('.total-per-item').each(function(){
				if (cbj(this).css('display') == 'none') {
					cbj(this).slideDown(100);
				}
			});

			cbj('.quantity-display').each(function(){
				if (cbj(this).css('display') == 'none') {
					cbj(this).slideDown(100);
				}
			});

		}
		else {

			cbj('.total-per-item').each(function(){
				if (cbj(this).css('display') != 'none') {
					cbj(this).slideUp(100);
				}
			});

			cbj('.quantity-display').each(function(){
				if (cbj(this).css('display') != 'none') {
					cbj(this).slideUp(100);
				}
			});

		}

		cbp.stop('do price module update');

	},

	/**
	 * Handler for event selectionHasChanged. Changes the price module content when the system changed an element.
	 */
	updateSelection: function ( event, elementId, xrefId, text, outputValue) {

		//console.log('com_configbox.blockPricing.changeSelection called: elementId = "'+elementId+'" ('+ com_configbox.elements[elementId].title +') with xrefId = "'+xrefId+'", text = "'+text+'" - outputValue = "'+outputValue+'"');

		// Removal
		if (!outputValue) {
			// Hide the item
			cbj('.element-item-' + elementId ).slideUp(100,function(){
				cbj(this).addClass('hidden-item');
			});
		}
		// Change
		else {
			// Change the output value
			cbj('.element-item-outputvalue-' + elementId ).html( outputValue );
			// Show in case item is hidden
			cbj('.hidden-item.element-item-' + elementId ).slideDown(100,function(){
				cbj(this).removeClass('hidden-item');
			});
		}

	}

};

/* BLOCK PRICING - END */

/* BLOCK VISUALIZATION - START */

cbj(document).ready(function(){

	// Assign handler for configuration changes
	cbj('#com_configbox').on('selectionHasChanged', com_configbox.blockVisualization.updateElementImage);

});

window.com_configbox.blockVisualization = {

	/**
	 * Changes the visualization content when an element has changed in the configuration.
	 * @listens Event:selectionHasChanged
	 */
	updateElementImage: function (event, elementId, xrefId, text, outputValue) {

		//console.log('com_configbox.blockVisualization.updateElementImage called: elementId = "'+elementId+'" ('+ com_configbox.elements[elementId].title +') with xrefId = "'+xrefId+'", text = "'+text+'" - outputValue = "'+outputValue+'"');

		// Removal
		if (!outputValue) {
			com_configbox.blockVisualization.removeElementImage(elementId);
		}
		// Change
		else {
			com_configbox.blockVisualization.changeVisualization(elementId, xrefId);
		}

	},

	removeElementImage: function(elementId) {

		// Remove either the xref or all images of the element
		cbj('.image-element-id-' + elementId).fadeOut(200);

	},

	changeVisualization: function(elementId, xrefId) {

		//console.log('updateVisualization called with params:  elementId = "'+elementId+'" and xrefId = "'+xrefId+'"');

		if (xrefId != 0) {
			// If there is no image for the xref, go straight to fade out of the others (case one assignment has no image, others do)
			if (cbj('.image-xref-id-' + xrefId).length == 0) {
				cbj('.image-element-id-' + elementId + ':not(.image-xref-id-'+ xrefId +')').fadeOut(200);
			}
			// Fade in the image, then fade out the others
			else {
				var otherImages = cbj('.image-element-id-' + elementId + ':not(.image-xref-id-'+ xrefId +')');
				if (otherImages.length) {
					otherImages.fadeOut(200,function(){
						cbj('.image-xref-id-' + xrefId).fadeIn(200);
					});
				}
				else {
					cbj('.image-xref-id-' + xrefId).fadeIn(200);
				}

			}
		}
		else {
			cbj('.image-xref-id-' + xrefId).fadeOut(200);
		}

	}

};

/* BLOCK VISUALIZATION - END */

/* ELEMENT PAGE FUNCTIONALITY - START */

Kenedo.registerSubviewReadyFunction('view-adminelement', function(){

	cbj('#view-adminelement .kenedo-details-form').on('extendedSettingsChanged',function( event, settings ){

		if (settings.widget === 'text') {
			cbj('#property-name-as_textarea, #property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val, #property-name-integer_only, #property-name-validate, #property-name-minval, #property-name-maxval, #property-name-default_value, #property-name-picker_table, #property-name-picker_table, #property-name-unit').show();
			cbj('#property-name-choices, #property-name-upload_extensions, #property-name-upload_mime_types, #property-name-upload_size_mb, #property-name-picker_table, #property-name-slider_steps').hide();
		}

		if (settings.widget === 'choices') {
			cbj('#property-name-choices, #property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val, #property-name-integer_only, #property-name-validate, #property-name-minval, #property-name-maxval, #property-name-default_value, #property-name-picker_table, #property-name-picker_table, #property-name-unit').show();
			cbj('#property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val, #property-name-integer_only, #property-name-validate, #property-name-minval, #property-name-maxval, #property-name-default_value, #property-name-unit, #property-name-as_textarea, #property-name-upload_extensions, #property-name-upload_mime_types, #property-name-upload_size_mb, #property-name-picker_table, #property-name-slider_steps').hide();
		}

		if (settings.widget === 'calendar') {
			cbj('#property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val, #property-name-integer_only, #property-name-validate, #property-name-minval, #property-name-maxval, #property-name-default_value, #property-name-picker_table, #property-name-picker_table').show();
			cbj('#property-name-choices, #property-name-as_textarea, #property-name-integer_only, #property-name-upload_extensions, #property-name-upload_mime_types, #property-name-upload_size_mb, #property-name-picker_table, #property-name-slider_steps, #property-name-unit').hide();
		}

		if (settings.widget === 'slider') {
			cbj('#property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val, #property-name-integer_only, #property-name-validate, #property-name-minval, #property-name-maxval, #property-name-default_value, #property-name-picker_table, #property-name-picker_table, #property-name-slider_steps, #property-name-unit').show();
			cbj('#property-name-choices, #property-name-as_textarea, #property-name-upload_extensions, #property-name-upload_mime_types, #property-name-upload_size_mb, #property-name-picker_table').hide();
		}

		if (settings.widget === 'fileupload') {
			cbj('#property-name-upload_extensions, #property-name-upload_mime_types, #property-name-upload_size_mb').show();
			cbj('#property-name-choices, #property-name-as_textarea, #property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val, #property-name-integer_only, #property-name-validate, #property-name-minval, #property-name-maxval, #property-name-default_value, #property-name-picker_table, #property-name-picker_table, #property-name-slider_steps, #property-name-unit').hide();
		}

		if (settings.widget === 'popuppicker') {
			cbj('#property-name-picker_table').show();
			cbj('#property-name-choices, #property-name-as_textarea, #property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val, #property-name-upload_extensions, #property-name-upload_mime_types, #property-name-upload_size_mb,#property-name-integer_only, #property-name-validate, #property-name-minval, #property-name-maxval, #property-name-default_value, #property-name-slider_steps, #property-name-unit').hide();
		}

		// Show validation fields only when used
		if (settings.validate == 1 && settings.widget !== 'choices' && settings.widget !== 'fileupload' && settings.widget !== 'popuppicker') {
			cbj('#property-name-minval, #property-name-maxval, #property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val').show();
		}

		// Hide if not
		if (settings.validate == 0) {
			cbj('#property-name-minval, #property-name-maxval, #property-name-calcmodel_id_min_val, #property-name-calcmodel_id_max_val').hide();
		}

	});

});

/* ELEMENT PAGE FUNCTIONALITY - END */


/* ADMIN LISTING AND FORM FUNCTIONALITY NOT COVERED BY KENEDO-PAGES - START */

Kenedo.registerSubviewReadyFunction('view-adminorders', function(){

	if (cbj('.in-frontend').length !== 0) {
		cbj('.listing-link').click(function(event){
			event.stopPropagation();
			window.location.href = cbj(this).attr('href');
		});
	}

	cbj('#view-adminorders .kenedo-limit-select').attr('name','limit');

	cbj('#view-adminorders .kenedo-limit-select').change(function(){
		cbj(this).closest('form').submit();
	});

	cbj('#view-adminorders .kenedo-pagination-list a').click(function(){
		var limitStart = cbj(this).attr('class').replace('page-start-','');
		cbj('#start').val(limitStart);
		cbj(this).closest('form').submit();
	});

	cbj('#view-adminorders .kenedo-filter-list select').change(function(){
		cbj(this).closest('form').submit();
	});

	cbj('#view-adminorders .kenedo-task-list li.task').click(function(){
		var task = cbj(this).attr('class').replace('task-','').replace('task','').replace(' ','');
		cbj('#task').val(task);
		cbj(this).closest('form').submit();
	});

});

Kenedo.registerSubviewReadyFunction('view-adminusers', function(){
	cbj('#view-adminusers .kenedo-listing-form .order-property').on('click',function() {

		var fieldName = cbj(this).attr('id').replace('order-property-','');

		var direction = 'asc';

		if (cbj(this).hasClass('active')) {
			direction = (cbj(this).hasClass('direction-asc')) ? 'desc' : 'asc';
		}

		cbj('#order_field').val(fieldName);
		cbj('#order_dir').val(direction);
		cbj(this).closest('form').submit();

	});

	cbj('#view-adminusers .kenedo-limit-select').attr('name','limit');

	cbj('#view-adminusers .kenedo-limit-select').change(function(){
		cbj(this).closest('form').submit();
	});

	cbj('#view-adminusers .kenedo-pagination-list a').click(function(){
		var limitStart = cbj(this).attr('class').replace('page-start-','');
		cbj('#start').val(limitStart);
		cbj(this).closest('form').submit();
	});

	cbj('#view-adminusers .kenedo-filter-list select').change(function(){
		cbj(this).closest('form').submit();
	});

});

Kenedo.registerSubviewReadyFunction('view-adminuser', function(){
	cbj('.kenedo-details-form').addClass('no-validation');
});

/* ADMIN LISTING AND FORM FUNCTIONALITY NOT COVERED BY KENEDO-PAGES - END */

/* RULE EDITOR FIELD - START */
cbj(document).ready(function(){

	cbj('#kenedo-page').on('click','.trigger-edit-rule',function(){

		var returnFieldId = cbj(this).closest('.kenedo-property').find('.data-field').data('return-field-id');
		var filterPageId = cbj(this).closest('.kenedo-property').find('.data-field').data('return-page-id');
		var rule = cbj(this).closest('.kenedo-property').find('.data-field').val();
		var url = cbj(this).data('editor-url');

		// Prevent scrolling in the parent
		cbj(document).bind('cbox_open', function(){
			cbj('body').css('overflow','hidden');
		});

		cbj(document).bind('cbox_closed', function(){
			cbj('body').css('overflow','auto');
		});

		// Change the page filter (in case page was changed since last store)
		if (cbj('#page_id').val()) {
			filterPageId = cbj('#page_id').val();
		}

		// If we have a modal in a modal, store the original dimensions (because we maximize width and height later on)
		if (cbj('.in-modal').length) {
			cbj('.in-modal').data('originalWidth', window.parent.cbj('#cboxWrapper').outerWidth());
			cbj('.in-modal').data('originalHeight', window.parent.cbj('#cboxWrapper').outerHeight());
			cbj('#cboxContent').css('margin-top','0px');
			window.parent.cbj('#cboxContent').css('margin-top','0px');
		}

		var maximizeEditor = function() {

			if (cbj('.in-modal').length) {
				window.parent.cbj.colorbox.resize( { innerWidth:'100%', innerHeight:'100%' });
			}
			cbj.colorbox.resize( { width:'95%', height:'95%' } );

		};

		var params = {
			transition 		: 'fade',
			href 			: url,
			overlayClose 	: false,
			iframe 			: false,
			width			: '1100px',
			height			: '95%',
			data			: {
				return_field_id : returnFieldId,
				rule			: rule,
				filter_page_id	: filterPageId
			},
			onComplete		: function(){

				// Prevent scrolling in top window
				window.top.cbj('html').css('overflow','hidden');
				cbj('html').css('overflow','hidden');

				// Make the editor full size on init and resize
				maximizeEditor();
				cbj(window.top).on('resize',maximizeEditor);

				// Run the ready functions
				Kenedo.runSubviewReadyFunctions('view-adminruleeditor');

			},
			onClosed		: function(){

				// Turn off the resize handler
				cbj(window.top).off('resize',maximizeEditor);

				// Allow scrolling in top window (but only if not in a modal already, then the modal code takes care of it)
				if (cbj('.in-modal').length == 0) {
					window.top.cbj('html').css('overflow','auto');
				}
				else {
					cbj('html').css('overflow','auto');
				}

				// Restore the original dimensions of a possible parent modal
				if (cbj('.in-modal').length) {
					var width = cbj('.in-modal').data('originalWidth');
					var height = cbj('.in-modal').data('originalHeight');
					window.parent.cbj.colorbox.resize( { width: width , height: height } );
				}
			}
		};

		cbj.colorbox(params);

	});

});
/* RULE EDITOR FIELD - END */

/* ADMIN PRODUCT TREE - START */

Kenedo.registerSubviewReadyFunction('view-adminproducttree',function(){

	cbj('.product-tree-wrapper .page-list, .product-tree-wrapper .element-list, .product-tree-wrapper .option-list').sortable({
		placeholder: "ui-state-highlight",
		items: "li:not(.add-item)",
		update: function() {

			var items = cbj(this).sortable('toArray');

			cbj(this).sortable('disable');

			Kenedo.disabledSortable = cbj(this);

			var controller = '';
			var key = '';

			if(cbj(this).is('.page-list')) {
				controller = 'adminpages';
				key = 'page-';
			}
			if(cbj(this).is('.element-list')) {
				controller = 'adminelements';
				key = 'element-';
			}
			if(cbj(this).is('.option-list')) {
				controller = 'adminoptionassignments';
				key = 'option-';
			}

			var ordering = [];
			var position = 10;

			for (var i in items) {
				if (items.hasOwnProperty(i)) {
					if (items[i].indexOf(key) != -1) {
						ordering.push( '"' + items[i].replace(key,'') + '" : ' + position );
						position += 10;
					}
				}
			}

			var serial = '{' + ordering.join(',') + '}';

			// Set the data as query string
			var query = 'option=com_configbox&controller='+controller+'&task=storeOrdering&format=raw&tmpl=component&ordering-items=' + encodeURIComponent(serial);

			cbj.post(com_configbox.entryFile, query, function(){
				Kenedo.disabledSortable.sortable('enable');
			});

		}
	});
	cbj('.product-tree-wrapper .product-item ul').disableSelection();
});

cbj(document).ready(function() {

	// Make product tree refresh when form data of certain views was changed
	cbj('#kenedo-page').on('kenedoFormResponseReceived', function(event, data){

		if (data.response.success != true) {
			return;
		}
		if (typeof(data.viewName) == 'undefined' || data.viewName == '' || !data.response.data || !data.response.data.id) {
			return;
		}

		var productId;
		var recordId = data.response.data.id;
		var recordType = data.viewName.replace('admin', '');
		var isProductInsert = false;

		if (data.viewName == 'adminproduct') {
			productId = data.response.data.id;
			recordId = productId;
			isProductInsert = data.response.wasInsert;
		}
		if (data.viewName == 'adminpage') {
			productId = cbj('#product_id').val();
		}

		if (data.viewName == 'adminelement') {
			productId = cbj('#page-'+ cbj('#page_id').val()).closest('.product-item').attr('id').replace('product-','');
		}

		com_configbox.refreshProductTree(recordId, recordType, productId, isProductInsert);

	});

	// Tree toggles for the product tree
	cbj('.product-tree-wrapper').on('click','.sub-list-trigger',function(){
		if (cbj(this).is('.trigger-opened')) {
			cbj(this).removeClass('trigger-opened');
			cbj(this).siblings('.sub-list').removeClass('list-opened');
		}
		else {
			cbj(this).addClass('trigger-opened');
			cbj(this).siblings('.sub-list').addClass('list-opened');
		}
	});

	// Mark the last product tree item that has been clicked
	cbj('#kenedo-page').on('click','.product-tree-wrapper .edit-link, .product-tree-wrapper .add-link', function(){
		cbj(this).closest('.product-tree-wrapper').find('.active').removeClass('active');
		cbj(this).closest('li').addClass('active');
	});

	// Tree toggles for the main menu
	cbj('.configbox-mainmenu').on('click','.trigger-toggle-sub-items',function(){
		cbj(this).toggleClass('opened');
		cbj(this).closest('.menu-list-item').children('.sub-items').toggleClass('opened');
	});

	// Toggle the tree edit button display
	cbj('.product-tree-wrapper').on('click','.toggle-tree-edit',function(){
		cbj(this).toggleClass('active');
		cbj(this).closest('.product-tree-wrapper').toggleClass('shows-edit-buttons');
	});

	cbj('.product-tree-wrapper').on('click','.trigger-remove',function(){

		cbj(this).addClass('processing');

		Kenedo.removalListItem = cbj(this).closest('li');

		// Do the XHR request
		cbj.ajax({

			url: cbj(this).data('url'),
			dataType: 'json',
			//data: requestData,

			beforeSend: function(XMLHttpRequest) {

			},

			success: function(data) {
				cbj('.product-tree-wrapper .processing').removeClass('processing');

				if (data.success == false) {
					alert(data.errors.join("\n"));
				}
				else {
					Kenedo.removalListItem.remove();
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(textStatus);
				console.log(errorThrown);
			},

			complete: function() {
				cbj('.product-tree-wrapper .processing').removeClass('processing');
			}

		});

	});


});

/* ADMIN PRODUCT TREE - END */

/* ITEM PICKER - START */
cbj(document).ready(function() {
	// Tree toggles for the product tree
	cbj('body').on('click','.view-item-picker .sub-list-trigger',function(){
		if (cbj(this).is('.trigger-opened')) {
			cbj(this).removeClass('trigger-opened');
			cbj(this).closest('li').children('.sub-list').removeClass('list-opened');
		}
		else {
			cbj(this).addClass('trigger-opened');
			cbj(this).closest('li').children('.sub-list').addClass('list-opened');
		}
	});
});
/* ITEM PICKER - END */


/* PRODUCT DETAIL PANES - START */
cbj(document).ready(function() {

	cbj('body').on('click','.product-detail-panes-type-tabs .product-detail-panes-heading-item', function(){

		cbj(this).addClass('active-tab').siblings().removeClass('active-tab');

		com_configbox.requestedPaneId = cbj(this).data('pane-id');
		cbj(this).closest('.product-detail-panes-type-tabs').find('.product-detail-panes-content').each(function(){
			if (cbj(this).data('pane-id') == com_configbox.requestedPaneId) {
				cbj(this).addClass('active-pane').siblings().removeClass('active-pane');
			}
		});
	});

});
/* PRODUCT DETAIL PANES - END */

/* ADMIN CALCULATION PAGE - START */

Kenedo.registerSubviewReadyFunction('view-admincalculation', function(){

	// All for when the view is loaded within a modal window (e.g. via join links)
	if (Kenedo.isViewInModal()) {

		// Prevent scrolling of the main window
		window.parent.cbj('html').css('overflow', 'hidden');

		// Allow scrolling again after the modal has closed
		window.parent.cbj(window.parent.document).bind('cbox_closed', function() {
			cbj(this).find('html').css('overflow', 'auto');
		});

		// Quick helper function for stuff below
		var resizeCalcView = function () {
			if (typeof(window.parent) != 'undefined') {
				window.parent.cbj.colorbox.resize( { innerHeight: '95%' });
			}
			else {
				window.cbj.colorbox.resize( { innerHeight: '95%' });
			}
		};

		// Resize the calc view as soon as the browser viewport changes
		cbj(window.top).on('resize', resizeCalcView);

		// Resize the view when it got loaded. Avoids the need for having more configuration in the KenedoFields
		resizeCalcView();

	}

	// Copy feature for calculations
	cbj('#kenedo-page').on('click', '#view-admincalculation .task-copy', function(event){
		event.stopPropagation();
		cbj('#id').val('');
		cbj('#task').val('apply');
		cbj('.task-apply').click();
	});

	// Load the right calculation type view when type is changed in the form
	cbj('#view-admincalculation #form-property-type input').change(function(){

		var selectedType = cbj(this).val();
		var url = cbj('.calc-type-subview').data('url-'+selectedType);

		Kenedo.loadSubview(url, '.calc-type-subview');

	});

});

// view-admincalculation : You find the code in a separate JS file (calc-editor.js)

Kenedo.registerSubviewReadyFunction('view-admincalccode', function(){

	// Add IDs to the placeholder elements in calculation code
	cbj('#view-admincalccode select option').each(function() {
		if (cbj(this).attr('value') != 0) {
			var text = cbj(this).text() + " (ID: " + cbj(this).attr('value') + ")";
			cbj(this).text(text);
		}
	});
	cbj('#view-admincalccode select').chosen();
	cbj('#view-admincalccode select').trigger('chosen:update');
});

// Ready function for view-admincalcmatrix (since the stuff here is in a KenedoPopup outside #kenedo-page, we got to
// Make those handlers like that.
cbj(document).ready(function(){

	// Functionality for picking an element as axis parameter
	// Fills the axis parameters with the element's options or makes some text fields ready (works for both row and col parameters)
	cbj('body').on('click', '.input-picker .element-item', function(){

		var elementId = cbj(this).closest('.element-item').data('id');

		var data = {
			'option' : 'com_configbox',
			'controller' : 'ajaxapi',
			'view' : 'ajaxapi',
			'format' : 'raw',
			'task' : 'getElementOptions',
			'element_id' : elementId
		};

		cbj.ajax({
			url: com_configbox.entryFile,
			data: data,
			dataType: 'json',
			context : cbj(this),
			success: function(options) {

				var hasXrefs = options.length;

				// Depending on if we're dealing with col or row paramters, figure out the KenedoField IDs for the
				// data we need to set for storing

				var fieldType = '';
				var fieldCalcId = '';
				var fieldElementId = '';
				var inputTargetSelector = '';

				if (cbj(this).closest('.column-parameter-picker').length != 0) {
					fieldType = 'column_type';
					fieldCalcId = 'column_calc_id';
					fieldElementId = 'column_element_id';
					inputTargetSelector = '.column-parameter';
				}
				else {
					fieldType = 'row_type';
					fieldCalcId = 'row_calc_id';
					fieldElementId = 'row_element_id';
					inputTargetSelector = '.row-parameter';
				}

				// Get the selected element's id and title
				var elementId = cbj(this).data('id');
				var elementTitle = cbj(this).children('.picker-link').text();

				// Set the KenedoField values for storing axis information and helper meta data
				cbj('#' + fieldType).val('element');
				cbj('#' + fieldElementId).val(elementId);
				cbj('#' + fieldCalcId).val('0');

				// Prepare the axis parameter input field
				var inputHtml = '';

				// Add drag table handle, hide all the axis labels, later the right label get's unhidden
				if (fieldType == 'column_type') {
					cbj('.cell-column-parameter').find('.axis-label').hide();
					inputHtml += '<i class="dragtable-drag-handle column-sort-handle fa fa-reorder"></i>';
				}
				else {
					cbj('.cell-row-parameter').find('.axis-label').hide();
					inputHtml += '<i class="dragtable-drag-handle row-sort-handle fa fa-reorder"></i>';
				}

				if (hasXrefs) {

					// Show and fill the axis label with the element's title
					if (fieldType == 'column_type') {
						cbj('.cell-column-parameter').find('.label-options').show().find('.parameter-title').text(elementTitle);
					}
					else {
						cbj('.cell-row-parameter').find('.label-options').show().find('.parameter-title').text(elementTitle);
					}

					// Prepare the dropdown HTML
					inputHtml += '<select>';

					// Add each option to it
					cbj.each(options, function(i, item){
						inputHtml += '<option value="'+ item.id + '">'+ item.title +'</option>';
					});

					// And finish up
					inputHtml += '</select>';

				}
				else {

					// Show and fill the label for free entry elements
					if (fieldType == 'column_type') {
						cbj('.cell-column-parameter').find('.label-textfield').show().find('.parameter-title').text(elementTitle);
					}
					else {
						cbj('.cell-row-parameter').find('.label-textfield').show().find('.parameter-title').text(elementTitle);
					}

					// Make the input HTML element
					inputHtml += '<input class="input-value" type="text" value="" />';
				}

				// In any case, add the removal trigger element
				inputHtml += '<span class="trigger-remove"></span>';

				// Increment var to make preselections for each dropdown axis parameter
				var nthOption = 0;

				// Insert the input for axis parameters one by one
				cbj( inputTargetSelector ).each(function(){

					// Bump up the increment var
					nthOption++;

					// Insert the parameter input
					cbj(this).html(inputHtml);

					// If it's a select field, pre-select each
					cbj(this).find('select option:nth-child(' + nthOption + ')').prop('selected', true);

				});

				// Now close the picker popup
				cbj(this).closest('.kenedo-popup').hide();

			}
		});


	});

	// Functionality to pick a calculation as axis parameter
	cbj('body').on('change', '.calculation-picker', function(){

		var fieldType = '';
		var fieldElementId = '';
		var fieldCalcId = '';

		if (cbj(this).closest('.column-parameter-picker').length != 0) {
			fieldType = 'column_type';
			fieldElementId = 'column_element_id';
			fieldCalcId = 'column_calc_id';
		}
		else {
			fieldType = 'row_type';
			fieldElementId = 'row_element_id';
			fieldCalcId = 'row_calc_id';
		}

		var id = cbj(this).val();
		var title = cbj(this).find('option[value='+id+']').text();

		cbj('#'+fieldType).val('calculation');
		cbj('#'+fieldElementId).val('0');
		cbj('#'+fieldCalcId).val(id);

		if (fieldType == 'column_type') {
			cbj('.cell-column-parameter').find('.axis-label').hide();
			cbj('.cell-column-parameter').find('.label-calculation').show().find('.parameter-title').text(title);
		}
		else {
			cbj('.cell-row-parameter').find('.axis-label').hide();
			cbj('.cell-row-parameter').find('.label-calculation').show().find('.parameter-title').text(title);
		}

		var inputHtml = '<input class="input-value" type="text" value="" />';
		inputHtml += '<span class="trigger-remove"></span>';

		if (fieldType == 'column_type') {
			cbj('.column-parameter').each(function(){
				cbj(this).html(inputHtml);
			});
		}
		else {
			cbj('.row-parameter').each(function(){
				cbj(this).html(inputHtml);
			});
		}

		cbj(this).closest('.kenedo-popup').hide();

	});
});

Kenedo.registerSubviewReadyFunction('view-admincalcmatrix', function(){

	// Tabs in the picker popup
	cbj('body').on('click', '.input-picker .tab', function(){
		cbj(this).addClass('tab-open').siblings().removeClass('tab-open');
		var id = cbj(this).attr('id').replace('tab-', '');
		cbj(this).closest('.input-picker').find('#pane-'+id).addClass('pane-open').siblings().removeClass('pane-open');
	});

	cbj('.matrix-wrapper-table').on('click', '.cell-column-parameter .kenedo-popup-trigger-content', function(){
		cbj(this).closest('.kenedo-popup-trigger').find('.kenedo-popup').fadeOut(200, function() { cbj(this).css('visible','hidden'); });
		cbj(this).closest('.kenedo-popup-trigger').trigger('popup-close');
	});


	// Add a row in the matrix
	cbj('#view-admincalcmatrix').on('click','.trigger-add-row',function(){
		cbj('.calc-matrix tr:last').clone().appendTo('.calc-matrix');
		cbj('.calc-matrix tr:last .input-value').val('');
		cbj('.calc-matrix tr:last .price').val('');
	});

	// Add a column in the matrix
	cbj('#view-admincalcmatrix').on('click','.trigger-add-column',function(){

		cbj('.calc-matrix thead tr:first-child').find('th:last').clone().appendTo( cbj('.calc-matrix thead tr:first-child') ).find('input').val('');

		cbj('.calc-matrix tbody tr').each(function(){
			cbj(this).find('td:last').clone().appendTo(cbj(this)).find('input').val('');
		});
	});

	// Removing a row or a column in the matrix
	cbj('#view-admincalcmatrix').on('click','.trigger-remove',function(){
		if (cbj(this).closest('.column-parameter').length) {
			var num = cbj(this).closest('th').index();

			cbj('.calc-matrix tr').each(function(){
				cbj(this).find('th:nth-child('+(num+1)+')').remove();
				cbj(this).find('td:nth-child('+(num+1)+')').remove();
			});
		}
		else {
			cbj(this).closest('tr').remove();
		}
	});

	cbj('#view-admincalcmatrix').on('click','.toggle-matrix-tools',function(){
		cbj(this).toggleClass('active');
		cbj(this).closest('.matrix-wrapper-table').toggleClass('show-matrix-tools');
	});

	cbj('#view-admincalcmatrix .calc-matrix').dragtable({
		items: '.column-parameter .column-sort-handle',
		boundary: ':not(.column-parameter)'
	});

	cbj('#view-admincalcmatrix .calc-matrix tbody').sortable({
		appendTo	: 'parent',
		items		: 'tr:not(.column-parameters)',
		helper		: 'clone',
		handle		: '.row-sort-handle',
		placeholder	: 'ui-state-highlight',
		axis		: 'y',
		start		: function(event, ui) {
			cbj(ui.helper).find('td').each(function(){
				var index = cbj(this).index();
				var width = cbj('.calc-matrix .column-parameters th').eq(index).outerWidth();
				cbj(this).outerWidth(width);
			});
		}
	});

	var taskHandler = function(viewName, task) {

		// Close or cancel can be handled with the default handler
		if (task == 'close' || task == 'cancel') {
			return Kenedo.defaultFormTaskHandler(viewName, 'cancel');
		}

		// All that is only for storing calculation matrix data. If matrix isn't selected, let the default handler work.
		if (cbj('#view-admincalcmatrix').length == 0) {
			return Kenedo.defaultFormTaskHandler(viewName, task);
		}

		cbj('.calc-matrix .missing-value').removeClass('missing-value');

		var columnParameters = [];
		var rowParameters = [];
		var valuesMissing = false;
		var duplicateValues = false;

		cbj('.calc-matrix .column-parameter').each(function(){

			var value = '';

			if (cbj(this).find('select').length) {
				value = cbj(this).find('select').val();
			}
			if (cbj(this).find('input').length) {
				value = cbj(this).find('input').val();
			}

			if (!value) {
				cbj(this).addClass('missing-value');
				valuesMissing = true;
			}
			else {

				if (columnParameters.indexOf(value) != -1) {
					cbj(this).addClass('duplicate-value');
					duplicateValues = true;
				}
				else {
					columnParameters.push(value);
				}

			}

		});

		cbj('.calc-matrix .row-parameter').each(function(){

			var value = '';

			if (cbj(this).find('select').length) {
				value = cbj(this).find('select').val();
			}
			if (cbj(this).find('input').length) {
				value = cbj(this).find('input').val();
			}

			if (!value) {
				cbj(this).addClass('missing-value');
				valuesMissing = true;
			}
			else {

				if (rowParameters.indexOf(value) != -1) {
					cbj(this).addClass('duplicate-value');
					duplicateValues = true;
				}
				else {
					rowParameters.push(value);
				}

			}

		});

		if (duplicateValues) {
			alert(com_configbox.lang.matrixParametersNotUnique);
			return false;
		}

		if (valuesMissing) {
			return false;
		}

		var matrixItems = [];
		var rowCount = 0;
		var ordering = 1;

		cbj('.calc-matrix tr:not(.column-parameters)').each(function(){
			var columnCount = 0;
			cbj(this).find('.price').each(function(){
				var input = cbj(this).val();
				if (input) {
					input.replace(',','.');
				}
				var matrixItem = {
					x 		: columnParameters[columnCount],
					y 		: rowParameters[rowCount],
					value 	: input,
					ordering: ordering
				};
				matrixItems.push(matrixItem);
				columnCount++;
				ordering++;
			});
			rowCount++;
		});

		cbj('#matrix').val( JSON.stringify(matrixItems) );

		// All done, let the default task handler do the rest
		return Kenedo.defaultFormTaskHandler(viewName, task);

	};

	Kenedo.setFormTaskHandler('admincalculation', taskHandler);

});

/* ADMIN CALCULATION PAGE - END */


/* TEMPLATES SUBVIEW - START */
Kenedo.registerSubviewReadyFunction('view-admintemplates', function(){
	// Make the .open links to use pushState
	cbj('.open').address();
});
/* TEMPLATES SUBVIEW - END */


/* ADMIN TEMPLATE EDITOR - START */
Kenedo.registerSubviewReadyFunction('view-admintemplate', function(){

	// Create the CodeMirror editor with content of template-code
	com_configbox.codeMirrorEditor = CodeMirror.fromTextArea(document.getElementById("template-code"), {
		lineNumbers: true,
		matchBrackets: true,
		mode: "php",
		indentUnit: 4,
		indentWithTabs: true
	});

	// Make CodeMirror copy over the contents to the right form field on change
	com_configbox.codeMirrorEditor.on('change', function(cm){
		cbj('#template-code').val(cm.getValue());
	});

});

/* ADMIN TEMPLATE EDITOR - END */

Kenedo.registerSubviewReadyFunction('view-adminoptionassignment', function(){

	cbj('#kenedo-page').on('change','#view-adminoptionassignment #option_id', function(){
		var optionId = cbj(this).val();
		var urlTemplate = cbj('#option_assignment_load_url').val();
		var url = urlTemplate.replace('%', optionId);

		// Remove all TinyMCE editors (hopefully there aren't any outside the ajax-target)
		if (window.tinyMCE) {
			// Some sites overwrite tinyMCE with a lower version, call remove only if we're on 4 or higher (3 has a
			// different function signature)
			if (window.tinyMCE.majorVersion > 3) {
				window.tinyMCE.remove();
			}
		}

		cbj('.option-data .option-fields-target').animate( {'opacity':'.0'}, 200, function(){
			cbj('.option-data .option-fields-target').load(url + ' .kenedo-properties', function(){
				Kenedo.initNewContent( cbj('.option-data .option-fields-target') );

				tinyMCE.baseURL = com_configbox.tinyMceBaseUrl;
				tinyMCE.suffix = '.min';

				//Init HTML editors
				tinyMCE.init({
					// General options
					mode 		: "textareas",
					selector 	: '.kenedo-html-editor',
					theme 		: "modern",
					plugins		: [
						"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
						"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
						"save table contextmenu directionality emoticons template paste textcolor"
					],
					toolbar		: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
					content_css : "",
					template_external_list_url 	: "js/template_list.js",
					external_link_list_url 		: "js/link_list.js",
					external_image_list_url 	: "js/image_list.js",
					media_external_list_url 	: "js/media_list.js"
				});

				cbj('.option-data .option-fields-target').animate( {'opacity':'1'}, 400);
			});
		});

	});

});

Kenedo.registerSubviewReadyFunction('view-adminuserfields', function(){

	cbj('.browser-validation input, .server-validation input').each(function(){
		cbj(this).data('invalidEntry',false);
	});

	cbj('.server-validation input').focus(function(){
		cbj(this).data('invalidEntry',null);
	});

	cbj('.server-validation input').change(function(){

		var expression = cbj(this).val();
		var testElement = cbj(this);

		if (!expression) {
			testElement.css('border','1px solid #ccc');
			testElement.data('invalidEntry',false);
		}

		if (expression) {

			testElement.parent().find('.loading-symbol').show();

			// Use the entry file property since J1.6 redirects without a perfect URL
			var url = com_configbox.entryFile;

			var params = {};
			params.option = 'com_configbox';
			params.expression = expression;
			params.controller = 'ajaxapi';
			params.view = 'ajaxapi';
			params.task = 'validateRegex';
			params.format = 'raw';
			params.tmpl = 'component';
			params.lang= com_configbox.langSuffix;

			var options = {};
			options.data = params;
			options.async = true;
			options.success = function(data) {
				var response = cbj.trim(data);

				if (response !== 'OK') {
					testElement.css('border','1px solid red');
					alert(response);
					testElement.data('invalidEntry',true);
				}
				else {
					testElement.css('border','1px solid #ccc');
					testElement.data('invalidEntry',false);
				}

				testElement.parent().find('.loading-symbol').hide();

			};

			cbj.ajax(url,options);
		}

	});

	cbj('.browser-validation input').change(function(){
		var expression = cbj(this).val();
		var testElement = cbj(this);
		testElement.data('invalidEntry',null);
		testElement.css('border','1px solid #ccc');
		var testString = 'dummytext';
		try {
			var regex = new RegExp(expression);
			regex.exec(testString);

		} catch (e) {
			testElement.css('border','1px solid red');
			alert(e.message);
			testElement.data('invalidEntry',true);
		}

	});

	var disabledValidation		  = ['salutation_id','country','email','language','billingsalutation_id','billingcountry','billingemail','billinglanguage'];
	var disabledCheckoutDisplay   = ['salutation_id','firstname','lastname','country','email','billingsalutation_id','billingfirstname','billinglastname','billingcountry','billingemail'];
	var disabledCheckoutRequire   = ['salutation_id','firstname','lastname','country','email','billingsalutation_id','billingfirstname','billinglastname','billingcountry','billingemail'];

	var disabledQuotationDisplay  = ['billingsalutation_id','billingfirstname','billinglastname','billingcountry','billingemail'];
	var disabledQuotationRequire  = ['billingsalutation_id','billingfirstname','billinglastname','billingcountry','billingemail'];

	cbj.each(disabledValidation,function(key,value){
		cbj('.userfield-' + value + ' .browser-validation input, .userfield-' + value + ' .server-validation input').prop('disabled',true);
	});

	cbj.each(disabledCheckoutDisplay,function(key,value){

		cbj('.userfield-' + value + ' .show-checkout input').click(function(event) {
			if (!cbj(this).prop('checked')) {
				event.stopPropagation();
				event.preventDefault();
				alert(com_configbox.lang.userFieldsNoHiding);
			}
		});

	});

	cbj.each(disabledCheckoutRequire,function(key,value){

		cbj('.userfield-' + value + ' .require-checkout input').click(function(event) {
			if (!cbj(this).prop('checked')) {
				event.stopPropagation();
				event.preventDefault();
				alert(com_configbox.lang.userFieldsNoOptional);
			}
		});

	});

	cbj.each(disabledQuotationDisplay,function(key,value){

		cbj('.userfield-' + value + ' .show-quotation input').click(function(event) {
			if (!cbj(this).prop('checked')) {
				event.stopPropagation();
				event.preventDefault();
				alert(com_configbox.lang.userFieldsNoHiding);
			}
		});

	});

	cbj.each(disabledQuotationRequire,function(key,value){

		cbj('.userfield-' + value + ' .require-quotation input').click(function(event) {
			if (!cbj(this).prop('checked')) {
				event.stopPropagation();
				event.preventDefault();
				alert(com_configbox.lang.userFieldsNoOptional);
			}
		});

	});

}); // register subview function

Kenedo.registerSubviewReadyFunction('view-adminusergroup', function(){
	cbj('#view-adminusergroup .kenedo-details-form').on('extendedSettingsChanged',function( event, settings ){

		if (settings.discount_type_1 == 'amount') {
			cbj('#property-name-discount_amount_1').show();
			cbj('#property-name-discount_factor_1').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_amount_1').hide().find('input').val('');
			cbj('#property-name-discount_factor_1').show();
		}

		if (settings.discount_type_2 == 'amount') {
			cbj('#property-name-discount_amount_2').show();
			cbj('#property-name-discount_factor_2').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_amount_2').hide().find('input').val('');
			cbj('#property-name-discount_factor_2').show();
		}

		if (settings.discount_type_3 == 'amount') {
			cbj('#property-name-discount_amount_3').show();
			cbj('#property-name-discount_factor_3').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_amount_3').hide().find('input').val('');
			cbj('#property-name-discount_factor_3').show();
		}

		if (settings.discount_type_4 == 'amount') {
			cbj('#property-name-discount_amount_4').show();
			cbj('#property-name-discount_factor_4').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_amount_4').hide().find('input').val('');
			cbj('#property-name-discount_factor_4').show();
		}

		if (settings.discount_type_5 == 'amount') {
			cbj('#property-name-discount_amount_5').show();
			cbj('#property-name-discount_factor_5').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_amount_5').hide().find('input').val('');
			cbj('#property-name-discount_factor_5').show();
		}


		if (settings.discount_recurring_type_1 == 'amount') {
			cbj('#property-name-discount_recurring_amount_1').show();
			cbj('#property-name-discount_recurring_factor_1').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_recurring_amount_1').hide().find('input').val('');
			cbj('#property-name-discount_recurring_factor_1').show();
		}

		if (settings.discount_recurring_type_2 == 'amount') {
			cbj('#property-name-discount_recurring_amount_2').show();
			cbj('#property-name-discount_recurring_factor_2').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_recurring_amount_2').hide().find('input').val('');
			cbj('#property-name-discount_recurring_factor_2').show();
		}

		if (settings.discount_recurring_type_3 == 'amount') {
			cbj('#property-name-discount_recurring_amount_3').show();
			cbj('#property-name-discount_recurring_factor_3').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_recurring_amount_3').hide().find('input').val('');
			cbj('#property-name-discount_recurring_factor_3').show();
		}

		if (settings.discount_recurring_type_4 == 'amount') {
			cbj('#property-name-discount_recurring_amount_4').show();
			cbj('#property-name-discount_recurring_factor_4').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_recurring_amount_4').hide().find('input').val('');
			cbj('#property-name-discount_recurring_factor_4').show();
		}

		if (settings.discount_recurring_type_5 == 'amount') {
			cbj('#property-name-discount_recurring_amount_5').show();
			cbj('#property-name-discount_recurring_factor_5').hide().find('input').val('');
		}
		else {
			cbj('#property-name-discount_recurring_amount_5').hide().find('input').val('');
			cbj('#property-name-discount_recurring_factor_5').show();
		}

	});
});

/* CUSTOMER FORM - START */
cbj(document).ready(function(){

	// Hide or show the delivery address section
	cbj('body').on('change', '#view-customerform .trigger-toggle-same-delivery', function(){

		if (cbj(this).prop('checked') == true) {
			// Hide the delivery fields
			cbj(this).closest('.customer-form-sections').removeClass('show-delivery-fields');
		}
		else {

			// Copy over billing to delivery (but only if user didn't toggle before)
			if (cbj(this).data('got-toggled') == 'undefined') {

				cbj(this).data('got-toggled', true);

				var customerFields = cbj(this).closest('#view-customerform').data('customer-fields');
				var formType = cbj(this).closest('#view-customerform').find('#form_type').val();

				// Copy info over from billing
				for (var key in customerFields) {
					if (customerFields.hasOwnProperty(key)) {
						var obj = customerFields[key];
						if (obj['show_'+formType] == '1') {
							if (cbj('#'+obj.field_name).length && cbj('#billing'+obj.field_name).length) {
								var billingFieldValue = cbj('#billing'+obj.field_name).val();
								cbj('input[name='+obj.field_name+']').val(billingFieldValue);
							}
						}
					}
				}

			}

			// Make the delivery fields show up
			cbj(this).closest('.customer-form-sections').addClass('show-delivery-fields');

		}

	});

	// CUSTOMER FORM: Country dropdowns updating state dropdowns
	cbj('body').on('change', '#view-customerform select.updates-states', function(){

		// Get the selected ID
		var countryId = cbj(this).val();

		// Get the ID of the state dropdown
		var stateSelectId = cbj(this).data('state-select-id');

		// No country ID means we replace any options
		if (countryId == 0) {
			cbj('#'+stateSelectId).html('<option value="0" selected="selected">'+com_configbox.lang.selectCountryFirst+'</option>');
			cbj('#'+stateSelectId).prop('disabled', true);
			cbj('#'+stateSelectId).trigger('chosen:updated').trigger('change');
			return;
		}

		// Prepare request data
		var data = {
			'option' : 'com_configbox',
			'controller' : 'customerform',
			'task' : 'getStates',
			'format' : 'raw',
			'country_id' : countryId
		};

		// Do the ajax call
		cbj.ajax({
			url: com_configbox.entryFile,
			data: data,
			dataType: 'json',
			context: cbj('#'+stateSelectId),
			success: function(data) {

				// Remove any existing options and un-disable it
				cbj(this).html('').prop('disabled', false);

				// Mark the field in case we got no options for it
				if (data.length == 0) {
					cbj(this).closest('.customer-field').addClass('has-no-data');
				}
				else {
					cbj(this).closest('.customer-field').removeClass('has-no-data');
				}

				// Get a reference to the dropdown for the loop
				var select = cbj(this);

				// Loop and add the options
				cbj.each(data, function(i, item) {
					cbj(select).append('<option value="'+item.id+'">'+item.name+'</option>');
				});

				// Dispatch the events for chosen:updated/change
				cbj(this).trigger('chosen:updated').trigger('change');

			}
		});

	});

	cbj('body').on('change', '#view-customerform select.updates-counties', function(){

		// Get the selected ID
		var stateId = cbj(this).val();

		// Get the ID of the state dropdown
		var countySelectId = cbj(this).data('county-select-id');

		// No country ID means we replace any options
		if (stateId == 0) {
			cbj('#'+countySelectId).html('<option value="0" selected="selected">'+com_configbox.lang.selectStateFirst+'</option>');
			cbj('#'+countySelectId).prop('disabled', true);
			cbj('#'+countySelectId).trigger('chosen:updated').trigger('change');
			return;
		}

		// Prepare request data
		var data = {
			'option' : 'com_configbox',
			'controller' : 'customerform',
			'task' : 'getCounties',
			'format' : 'raw',
			'state_id' : stateId
		};

		// Do the ajax call
		cbj.ajax({
			url: com_configbox.entryFile,
			data: data,
			dataType: 'json',
			context: cbj('#'+countySelectId),
			/**
			 * @param {(Array|JsonResponses.customerform.getCounties)} counties - County data
			 */
			success: function(counties) {

				// Remove any existing options and un-disable it
				cbj(this).html('').prop('disabled', false);

				// Mark the field in case we got no options for it
				if (counties.length == 0) {
					cbj(this).closest('.customer-field').addClass('has-no-data');
				}
				else {
					cbj(this).closest('.customer-field').removeClass('has-no-data');
				}

				// Get a reference to the dropdown for the loop
				var select = cbj(this);

				// Loop and add the options
				cbj.each(counties, function(i, county) {
					cbj(select).append('<option value="'+county.id+'">'+county.county_name+'</option>');
				});

				// Dispatch the events for chosen:updated/change
				cbj(this).trigger('chosen:updated').trigger('change');

			}
		});

	});

	cbj('body').on('change', '#view-customerform select.updates-cities', function(){

		// Get the selected ID
		var countyId = cbj(this).val();

		// Get the ID of the state dropdown
		var citySelectId = cbj(this).data('city-select-id');

		if (!countyId) {
			// Unselect a possible value in the city dropdown
			cbj('#'+citySelectId).val('0');
			// Mark it as not used
			cbj('#'+citySelectId).closest('.customer-field').addClass('uses-textfield-instead');
			// Get the CSS class part for the city text field
			var textField = citySelectId.replace('_id', '');
			// Remove the unused mark
			cbj(this).closest('#view-customerform').find('.customer-field-'+textField).removeClass('uses-dropdown-instead');
			return;
		}

		// Prepare request data
		var data = {
			'option' : 'com_configbox',
			'controller' : 'customerform',
			'task' : 'getCities',
			'format' : 'raw',
			'county_id' : countyId
		};

		// Do the ajax call
		cbj.ajax({
			url: com_configbox.entryFile,
			data: data,
			dataType: 'json',
			context: cbj('#'+citySelectId),
			/**
			 * @param {(Array|JsonResponses.customerform.getCities)} cities - City Data
			 */
			success: function(cities) {

				// Remove any existing options and un-disable it
				cbj(this).html('').prop('disabled', false);

				var cityFieldName = '';

				// Mark the field in case we got no options for it
				if (cities.length == 0) {
					// Unselect a possible value in the city dropdown
					cbj(this).val('0');
					// Mark it as not used
					cbj(this).closest('.customer-field').addClass('has-no-data uses-textfield-instead');
					// Get the CSS class part for the city text field
					cityFieldName = citySelectId.replace('_id', '');
					// Remove the unused mark
					cbj(this).closest('#view-customerform').find('.customer-field-'+cityFieldName).removeClass('uses-dropdown-instead');
					return;
				}

				// Remove the mark for having no data
				cbj(this).closest('.customer-field').removeClass('has-no-data');
				// Mark it as not used
				cbj(this).closest('.customer-field').removeClass('uses-textfield-instead');
				// Get the CSS class part for the city text field
				cityFieldName = citySelectId.replace('_id', '');
				// Empty the value in city text field
				cbj('#'+cityFieldName).val('');
				// Remove the unused mark
				cbj(this).closest('#view-customerform').find('.customer-field-'+cityFieldName).addClass('uses-dropdown-instead');

				// Get a reference to the dropdown for the loop
				var select = cbj(this);

				// Loop and add the options
				cbj.each(cities, function(i, city) {
					cbj(select).append('<option value="'+city.id+'">'+city.city_name+'</option>');
				});

				// Dispatch the events for chosen:updated/change
				cbj(this).trigger('change').trigger('chosen:updated');

			}
		});

	});


	// Checking the box 'I have an account' makes the login box appear (and vice versa)
	cbj('body').on('change', '.recurring-customer-login #show-login', function() {
		var loginBox = cbj(this).closest('.recurring-customer-login').find('.login-wrapper').show();
		if (cbj(this).prop('checked') == true) {
			loginBox.show();
		}
		else {
			loginBox.hide();
		}
	});

	// Clicks on the customer form login button make a call to the user controller and then the customer form reloads
	cbj('body').on('click', '.recurring-customer-login .trigger-login', function() {

		// Deal with multiple mouse clicks
		if (cbj(this).hasClass('processing')) {
			return;
		}

		// Add the spinner to the button
		cbj(this).addClass('processing');

		var wrapper = cbj(this).closest('.recurring-customer-login');

		// Reset any feedback
		wrapper.find('.feedback').text('');

		var username = wrapper.find('.input-username').val();
		var password = wrapper.find('.input-password').val();

		com_configbox.requestLogin(username, password)

			.done(function(response){
				if (response.success === false) {
					if (wrapper.find('.login-box .feedback').length !== 0) {
						wrapper.find('.login-box .feedback').text(response.errorMessage);
					}
					else {
						alert(response.errorMessage);
					}
				}
				else {
					var refreshUrl = cbj('#view-customerform').data('view-url');
					cbj('#view-customerform').load(refreshUrl);
				}
			})
			.always(function(){
				wrapper.find('.login-box .processing').removeClass('processing');
			});

	});

	// Clicking on 'Recover Password' makes the right box appear
	cbj('body').on('click', '.recurring-customer-login .trigger-recover-password', function() {

		// Copy the email address from login-box over to recover box
		var email = cbj(this).closest('.login-wrapper').find('.input-username').val();
		if (email) {
			cbj(this).closest('.recurring-customer-login').find('.recover-box .input-username').val(email);
		}

		cbj(this).closest('.recurring-customer-login').find('.login-box').hide();
		cbj(this).closest('.recurring-customer-login').find('.recover-box').show();
		cbj(this).closest('.recurring-customer-login').find('.change-password-box').hide();
	});

	// Clicking on 'Cancel' brings back the login box
	cbj('body').on('click', '.recurring-customer-login .trigger-cancel-recovery', function() {
		cbj(this).closest('.recurring-customer-login').find('.login-box').show();
		cbj(this).closest('.recurring-customer-login').find('.recover-box').hide();
		cbj(this).closest('.recurring-customer-login').find('.change-password-box').hide();
	});

	// Clicks on 'Recover Password' make the server send out an email with a code and the next panel appears
	cbj('body').on('click', '.recurring-customer-login .trigger-request-verification-code', function() {

		// Deal with multiple mouse clicks
		if (cbj(this).hasClass('processing')) {
			return;
		}

		// Add the spinner to the button
		cbj(this).addClass('processing');

		// Get the email address from the form
		var email = cbj(this).closest('.recover-box').find('.input-username').val();

		// Get a reference to the box wrapper
		var wrapper = cbj(this).closest('.recurring-customer-login');

		// Reset any feedback
		wrapper.find('.feedback').text('');

		// Get the verification email sent
		com_configbox.requestPasswordChangeVerificationCode(email)
			.done(function(response){
				if (response.success === false) {
					if (wrapper.find('.recover-box .feedback').length !== 0) {
						wrapper.find('.recover-box .feedback').text(response.errorMessage);
					}
					else {
						alert(response.errorMessage);
					}
				}
				else {
					wrapper.find('.recover-box').hide();
					wrapper.find('.change-password-box').show();
				}
			})
			.always(function(){
				wrapper.find('.recover-box .processing').removeClass('processing');
			});

	});

	// Clicking on 'Change Password' sends code and new password to the server. If all goes well, the customer
	// gets logged in and the customer form reloads.
	cbj('body').on('click', '.recurring-customer-login .trigger-change-password', function() {

		// Deal with multiple mouse clicks
		if (cbj(this).hasClass('processing')) {
			return;
		}

		// Add the spinner to the button
		cbj(this).addClass('processing');

		// Get the email address from the form
		var code = cbj(this).closest('.change-password-box').find('.input-verification').val();
		var password = cbj(this).closest('.change-password-box').find('.input-new-password').val();

		// Get a reference to the box wrapper
		var wrapper = cbj(this).closest('.recurring-customer-login');

		// Reset any feedback
		wrapper.find('.feedback').text('');

		// Send the email for the verification code
		com_configbox.requestPasswordChange(code, password, true)

			.done(function(response) {

				if (response.success === false) {
					cbj.each(response.errors, function(i, error){
						wrapper.find('.change-password-box .feedback').append('<div>'+error.message+'</div>');
					});
				}
				else {
					var refreshUrl = cbj('#view-customerform').data('view-url');
					cbj('#view-customerform').load(refreshUrl);
				}
			})
			.always(function(){
				wrapper.find('.processing').removeClass('processing');
			});

	});

	// Hitting the enter key on the customer form login form triggers a click on the respective primary button
	cbj('body').on('keyup', '.recurring-customer-login', function(event) {

		// We're only interested in 'Enter'
		if(event.which != 13) {
			return;
		}

		// Prepare the wrapper for convenience
		var wrapper = cbj(this).closest('.recurring-customer-login');

		// See what's the origin, afterwards check in which box we're in
		var origin = cbj(event.target);

		if (origin.closest('.login-box').length !== 0) {
			wrapper.find('.trigger-login').trigger('click');
		}

		if (origin.closest('.recover-box').length !== 0) {
			wrapper.find('.trigger-request-verification-code').trigger('click');
		}
		if (origin.closest('.change-password-box').length !== 0) {
			wrapper.find('.trigger-change-password').trigger('click');
		}

	});

});

/**
 * Returns the current contents of a customer form
 * @returns {{String}}
 * @see ConfigboxViewCustomerform
 */
com_configbox.getCustomerFormData = function() {

	var customerData = {};

	// Loop through all inputs and collect customer data
	cbj('#view-customerform :input').each(function(i, item) {

		if (!cbj(item).attr('name')) {
			return;
		}
		if (cbj(item).is('input[type=radio]') && cbj(item).prop('checked') == false) {
			return;
		}
		if (cbj(item).is('input[type=checkbox]') && cbj(item).prop('checked') == false) {
			return;
		}
		if (cbj(item).is('input[type=checkbox]') && cbj(item).prop('checked') == true) {
			customerData[cbj(item).attr('name')] = '0';
		}

		customerData[cbj(item).attr('name')] = cbj(item).val();

	});

	// If delivery is same, replace any delivery values with their billing counterparts
	if (customerData.samedelivery) {
		cbj.each(customerData, function(fieldName, value) {
			if (fieldName.indexOf('billing') == 0) {
				var pendant = fieldName.substr(7);

				if (typeof(customerData[pendant]) != 'undefined') {
					customerData[pendant] = value;
				}
			}
		});
	}

	return customerData;

};

/**
 * Shows validation issues in customer form
 * @param {(Array|JsonResponses.storeCustomerResponseData.validationIssues)} issues
 * @see ConfigboxViewCustomerform
 */
com_configbox.displayValidationIssues = function(issues) {

	// Remove any css flags for invalid fields
	cbj('#view-customerform .customer-field:visible').removeClass('invalid').addClass('valid');

	// Set flags for fields with issues, set the issue message
	for (var i in issues) {
		if (issues.hasOwnProperty(i)) {
			cbj('.customer-field-'+issues[i].fieldName).removeClass('valid').addClass('invalid');
			cbj('.customer-field-'+issues[i].fieldName).find('.validation-tooltip').data('message', issues[i].message);
		}
	}

	// Set up the tooltips
	com_configbox.initValidationTooltips();

};

/**
 *
 */
com_configbox.removeValidationIssues = function() {
	// Remove any css flags for invalid fields
	cbj('#view-customerform .customer-field:visible').removeClass('invalid').removeClass('valid');
	cbj('#view-customerform .validation-tooltip').data('message', '');
	com_configbox.initValidationTooltips();
};

/**
 * Inits jQueryUI tooltips on the customer data form
 */
com_configbox.initValidationTooltips = function() {

	// Set up the tooltips
	cbj(document).tooltip({
		items: ".validation-tooltip",
		content : function(){
			return cbj(this).data('message');
		},
		position : {
			my: "center bottom-20",
			at: "center top",
			using: function( position, feedback ) {
				cbj( this ).css( position );
				cbj( "<div>" )
					.addClass( "arrow" )
					.addClass( feedback.vertical )
					.addClass( feedback.horizontal )
					.appendTo( this );
			}
		}
	});

};

com_configbox.requestLogin = function(username, password) {

	var requestData = {
		lang		: com_configbox.langSuffix,
		option		: 'com_configbox',
		controller	: 'user',
		task		: 'loginUser',
		format		: 'json',
		username	: username,
		password	: password
	};

	// Do the request, pass it back
	return cbj.ajax({
		url: com_configbox.entryFile,
		data: requestData,
		dataType: 'json',
		type:'post',
		context:cbj(this)
	});

};

com_configbox.requestPasswordChangeVerificationCode = function(email) {

	var requestData = {
		lang		: com_configbox.langSuffix,
		option		: 'com_configbox',
		controller	: 'user',
		task		: 'sendPasswordChangeVerificationCode',
		format		: 'raw',
		email		: email
	};

	// Do the request, pass it back
	return cbj.ajax({
		url: com_configbox.entryFile,
		data: requestData,
		dataType: 'json',
		type:'post'
	});

};

com_configbox.requestPasswordChange = function(code, password, loginUser) {

	var requestData = {
		lang		: com_configbox.langSuffix,
		option		: 'com_configbox',
		controller	: 'user',
		task		: 'changePasswordWithCode',
		format		: 'json',
		code		: code,
		password	: password,
		login		: (loginUser === true) ? 1 : 0
	};

	// Do the request, pass it back
	return cbj.ajax({
		url: com_configbox.entryFile,
		data: requestData,
		dataType: 'json',
		type:'post'
	});

};

/* CUSTOMER FORM - END */

cbj(document).ready(function(){

	Kenedo.setFormTaskHandler('admincustomer', function(viewName, task) {

		if (task == 'cancel') {
			window.location.href = Kenedo.base64UrlDecode(cbj('#return').val());
		}

		// Add the spinner to the button
		cbj(this).addClass('processing');

		var requestData = com_configbox.getCustomerFormData();

		requestData.option = 'com_configbox';
		requestData.controller = 'user';
		requestData.task = 'store';
		requestData.format = 'raw';
		requestData.lang = com_configbox.langSuffix;

		// Do the request, pass it back
		cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type: 'post',
			context: cbj(this),
			success: function(response) {
				// Remove the spinner
				cbj('.trigger-store-customer-form').removeClass('processing');

				if (response.success === false) {
					if (response.validationIssues.length) {
						com_configbox.displayValidationIssues(response.validationIssues);
						return;
					}
					else {
						alert(response.errors.join("\n"));
						return;
					}
				}

				// Remove all invalid classes from fields
				cbj('.customer-field').removeClass('invalid').removeClass('valid');

				if (task == 'apply') {
					Kenedo.clearMessages();
					Kenedo.addMessage(response.message, 'notice');
					Kenedo.showMessages();
				}
				if (task == 'store') {
					window.location.href = Kenedo.base64UrlDecode(cbj('#return').val());
				}


			}
		});

	});

	// Customer profile: Storing customer data on save button
	cbj('#view-user').on('click', '.trigger-store-customer-form', function() {

		// Add the spinner to the button
		cbj(this).addClass('processing');

		var requestData = com_configbox.getCustomerFormData();

		requestData.option = 'com_configbox';
		requestData.controller = 'user';
		requestData.task = 'store';
		requestData.format = 'raw';
		requestData.lang = com_configbox.langSuffix;

		// Do the request, pass it back
		cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type: 'post',
			context: cbj(this),
			success: function(response) {
				// Remove the spinner
				cbj('.trigger-store-customer-form').removeClass('processing');

				if (response.success === false) {
					if (response.validationIssues.length) {
						com_configbox.displayValidationIssues(response.validationIssues);
						return;
					}
					else {
						alert(response.errors.join("\n"));
						return;
					}
				}

				// Remove all invalid classes from fields
				cbj('.customer-field').removeClass('invalid').removeClass('valid');

				var url = cbj('.trigger-store-customer-form').data('back-url');
				if (url) {
					window.location.href = url;
				}

			}
		});

	});

	cbj('#view-rfq').on('click', '.trigger-request-quotation', function() {

		// Deal with multiple clicks
		if (cbj(this).hasClass('processing')) {
			return;
		}

		// Add the spinner to the button
		cbj(this).addClass('processing');

		// Get the form data
		var requestData = com_configbox.getCustomerFormData();

		requestData.cartId = cbj('#view-rfq').data('cart-id');

		// Append request data for the store request
		requestData.option = 'com_configbox';
		requestData.controller = 'rfq';
		requestData.task = 'processQuotationRequest';
		requestData.format = 'json';
		requestData.lang = com_configbox.langSuffix;

		// Do the request, pass it back
		cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type: 'post',
			context: cbj(this),
			success: function(response) {

				if (response.success === false) {

					if (typeof(response.validationIssues) != 'undefined' && response.validationIssues.length) {
						com_configbox.displayValidationIssues(response.validationIssues);
						// Remove the spinner
						cbj(this).removeClass('processing');
						return;
					}
					else {
						alert(response.errors.join("\n"));
						return;
					}

				}

				// Remove any validation issues
				com_configbox.removeValidationIssues();

				if (response.redirectUrl) {
					window.location.href = response.redirectUrl;
				}

			},
			complete: function() {
				cbj(this).removeClass('processing');
			}

		});

	});

	cbj('#view-saveorder').on('click', '.trigger-save-order', function() {

		// Deal with multiple clicks
		if (cbj(this).hasClass('processing')) {
			return;
		}

		// Add the spinner to the button
		cbj(this).addClass('processing');

		// Get the form data
		var requestData = com_configbox.getCustomerFormData();

		requestData.cartId = cbj('#view-saveorder').data('cart-id');

		// Append request data for the store request
		requestData.option = 'com_configbox';
		requestData.controller = 'saveorder';
		requestData.task = 'saveOrder';
		requestData.format = 'json';
		requestData.lang = com_configbox.langSuffix;

		// Do the request, pass it back
		cbj.ajax({
			url: com_configbox.entryFile,
			data: requestData,
			dataType: 'json',
			type: 'post',
			context: cbj(this),
			success: function(response) {

				cbj(this).removeClass('processing');

				if (response.success === false) {

					if (typeof(response.validationIssues) != 'undefined' && response.validationIssues.length) {
						com_configbox.displayValidationIssues(response.validationIssues);
						// Remove the spinner
						cbj(this).removeClass('processing');
						return;
					}
					else {
						alert(response.errors.join("\n"));
						return;
					}

				}

				// Remove any validation issues
				com_configbox.removeValidationIssues();

				if (response.redirectUrl) {
					window.location.href = response.redirectUrl;
				}

			}
		});

	});

});

/**
 *
 * @param {Number} cartId
 * @returns {jqXHR}
 */
com_configbox.prepareQuote = function(cartId) {

	var requestData = {
		option: 'com_configbox',
		controller: 'rfq',
		task: 'createQuotation',
		cartId: cartId,
		format: 'json'
	};

	return cbj.ajax({
		url: com_configbox.entryFile,
		data: requestData,
		dataType: 'json',
		type: 'get'
	});

};