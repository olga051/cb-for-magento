<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */
?>
<div id="com_configbox">
	<div id="view-configurator-page">
		<div id="template-pagenotfound">
			<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">
				<p><?php echo KText::_('This page does not exist or is not active.');?></p>
			</div>
		</div>
	</div>
</div>