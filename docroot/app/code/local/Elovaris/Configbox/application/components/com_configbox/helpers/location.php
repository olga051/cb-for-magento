<?php
class ConfigboxLocationHelper {
	
	protected static $maxMindWebserviceUrl = 'http://geoip.maxmind.com';
	protected static $maxMindGeoIpCityFilename = 'GeoIPCity.dat';
	protected static $maxMindGeoLiteCityFilename = 'GeoLiteCity.dat';
	protected static $maxMindCache = array();
	
	protected static $isoCodeFipsMap;
	protected static $defaultIpV4Address = '173.194.35.37';
	
	static public function getCoordsByIp($ipAddress = NULL) {
		
		if ($ipAddress === NULL) {
			$ipAddress = self::getClientIpV4Address();
		}
		
		$location = self::getLocationByIp($ipAddress);
		
		if (!$location) {
			return false;
		}
		else {
			return $location->coords;
		}
		
	}
	
	static public function getCountryCodeByIp($ipAddress = NULL) {
	
		if ($ipAddress === NULL) {
			$ipAddress = self::getClientIpV4Address();
		}
	
		$location = self::getLocationByIp($ipAddress);
	
		if (!$location) {
			return false;
		}
		else {
			return $location->countryCode;
		}
		
	}
	
	static public function getCityByIp($ipAddress = NULL) {
	
		if ($ipAddress === NULL) {
			$ipAddress = self::getClientIpV4Address();
		}
	
		$location = self::getLocationByIp($ipAddress);
		
		if (!$location) {
			return false;
		}
		else {
			return $location->city;
		}
	
	}
	
	static public function getZipCodeByIp($ipAddress = NULL) {
	
		if ($ipAddress === NULL) {
			$ipAddress = self::getClientIpV4Address();
		}
	
		$location = self::getLocationByIp($ipAddress);
		
		if (!$location) {
			return false;
		}
		else {
			return $location->zipcode;
		}
	
	}

	/**
	 * @param string $ipAddress optional - figures it out if empty
	 * @return StdClass|bool Location object or false on failure
	 */
	static public function getLocationByIp($ipAddress = NULL) {
		
		if ($ipAddress === NULL) {
			$ipAddress = self::getClientIpV4Address();
		}
		
		if (function_exists('overrideGetLocationByIp')) {
			$location = overrideGetLocationByIp($ipAddress);
			
			// NULL as response means that the regular location service should be used
			if ($location === NULL) {
				$location = self::getMaxMindLocation($ipAddress);
			}
			elseif($location === false) {
				return false;
			}
			else {
				$structureOk = self::checkLocationObjectStructure($location);
				if ($structureOk == false) {
					return false;
				}
			}
		}
		else {
			$location = self::getMaxMindLocation($ipAddress);
		}
		
		if (!$location) {
			return false;
		}
		else {
			return $location;
		}
		
	}
	
	static protected function checkLocationObjectStructure($locationObject) {
		
		$fields = array('countryCode','stateFips','city','zipcode','coords','metrocode','areacode');
		
		$missingFields = array();
		
		foreach ($fields as $field) {
			if (!isset($locationObject->$field)) {
				$missingFields[] = $field;
			}
		}
		
		if (!isset($locationObject->coords->lat)) {
			$missingFields[] = 'coords->lat';
		}
		if (!isset($locationObject->coords->lon)) {
			$missingFields[] = 'coords->lon';
		}
		
		if (count($missingFields)) {
			$message = 'Location object is missing the properties '.implode(',',$missingFields).'. Check your override function overrideGetLocationByIp().';
			KLog::log($message,'warning');
			return false;
		}
		else {
			return true;
		}
	}

	/**
	 * @param string $ipAddress
	 * @return StdClass|NULL Location object or NULL if no record
	 */
	static protected function getMaxMindLocation($ipAddress) {
	
		if (!isset(self::$maxMindCache[$ipAddress])) {
						
			$databaseFolder = KenedoPlatform::p()->getDirDataStore().DS.'maxmind_geoip';
			
			$maxMindGeoLiteCityPathFile = $databaseFolder .DS. self::$maxMindGeoLiteCityFilename;
			$maxMindGeoIpCityPathFile 	= $databaseFolder .DS. self::$maxMindGeoIpCityFilename;
			
			if (is_file($maxMindGeoLiteCityPathFile) or is_file($maxMindGeoIpCityPathFile)) {
				$location = self::getMaxMindLocationGeoIpCity($ipAddress);
			}
			elseif (defined('CONFIGBOX_MAXMIND_LICENSE_KEY') && CONFIGBOX_MAXMIND_LICENSE_KEY) {
				$location = self::getMaxMindLocationWebserviceCity($ipAddress);
			}
			else {
				$location = NULL;
			}
			
			self::$maxMindCache[$ipAddress] = $location;
			
		}
	
		return self::$maxMindCache[$ipAddress];
	
	}

	/**
	 * @param string $ipAddress
	 * @return StdClass|NULL|bool Location object, false on errors or NULL if no record
	 */
	static protected function getMaxMindLocationGeoIpCity($ipAddress) {
		
		require_once( KPATH_DIR_CB.DS.'external'.DS.'maxmind_geoip'.DS.'geoipcity.php');

		$databaseFolder = KenedoPlatform::p()->getDirDataStore().DS.'maxmind_geoip';
		
		$maxMindGeoLiteCityPathFile = $databaseFolder .DS. self::$maxMindGeoLiteCityFilename;
		$maxMindGeoIpCityPathFile 	= $databaseFolder .DS. self::$maxMindGeoIpCityFilename;
		
		
		if (is_file($maxMindGeoIpCityPathFile)) {
			$gi = geoip_open($maxMindGeoIpCityPathFile,GEOIP_STANDARD);
		}
		elseif (is_file($maxMindGeoLiteCityPathFile)) {
			$gi = geoip_open($maxMindGeoLiteCityPathFile,GEOIP_STANDARD);
		}
		else {
			KLog::log('Neither GeoLiteCity nor GeoIPCity database file was found in "'.$databaseFolder.'". DB files should be named "'.self::$maxMindGeoLiteCityFilename.'" and "'.self::$maxMindGeoIpCityFilename.'" respectively.','error','MaxMind GeoIP database files not found. See error log for detailed information.');
			return false;
		}
		
		$record = GeoIP_record_by_addr($gi, $ipAddress);
		
		if (!$record) {
			KLog::log('Could not get MaxMind GeoLiteCity location for IP "'.$ipAddress.'". There is no detailed error message.', 'debug');
			return false;
		}
		
		if ($record->country_code == 'A1') {
			KLog::log('Anonymous proxy detected at IP Geolocation lookup for IP "'.$ipAddress.'"','debug');
			return NULL;
		}
		
		$location = new StdClass();
	
		$location->countryCode = $record->country_code;
		$location->stateFips = $record->region;
		$location->city = $record->city;
		$location->zipcode = $record->postal_code;
		$location->coords = new StdClass();
		$location->coords->lat = $record->latitude;
		$location->coords->lon = $record->longitude;
		$location->metrocode = $record->metro_code;
		$location->areacode = $record->area_code;
		
		if ($location->countryCode == 'US' or $location->countryCode == 'CA') {
			$location->stateFips = self::getFipsNumberFromIsoCode($location->stateFips);
		}
		
		return $location;
		
	}
	
	static protected function getMaxMindLocationWebserviceCity($ipAddress) {
		
		$url = self::$maxMindWebserviceUrl . '/f?l=' . CONFIGBOX_MAXMIND_LICENSE_KEY . '&i=' . $ipAddress;
			
		$response = self::request($url);
		
		if (!$response) {
			return false;
		}
			
		$geo = explode(",",$response);
		
		if (!empty($geo[10])) {
			KLog::log('Could not get MaxMind location because of this error: "'.$geo[10].'"','warning');
			return false;
		}
		
		if ($geo[0] == 'A1') {
			KLog::log('Anonymous proxy detected at IP Geolocation lookup for IP "'.$ipAddress.'"','debug');
			return NULL;
		}
		
		$location = new StdClass();
			
		$location->countryCode = $geo[0];
		$location->stateFips = $geo[1];
		$location->city = utf8_encode($geo[2]);
		$location->zipcode = $geo[3];
		$location->coords = new StdClass();
		$location->coords->lat = $geo[4];
		$location->coords->lon = $geo[5];
		$location->metrocode = $geo[6];
		$location->areacode = $geo[7];
		
		if ($location->countryCode == 'US' or $location->countryCode == 'CA') {
			$location->stateFips = self::getFipsNumberFromIsoCode($location->stateFips);
		}
				
		return $location;
	}
	
	/**
	 * Convenience function to get the current client's IPV4 address.
	 * @return string $ipAddress IPv4 address of client or default IP address
	 */
	static public function getClientIpV4Address() {

		if (!empty($_SERVER['HTTP_CLIENT_IP']))
			$ipAddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(!empty($_SERVER['HTTP_X_FORWARDED_IP']))
			$ipAddress = $_SERVER['HTTP_X_FORWARDED_IP'];
		else if(!empty($_SERVER['X_FORWARDED_FOR']))
			$ipAddress = $_SERVER['X_FORWARDED_FOR'];
		else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(!empty($_SERVER['HTTP_X_FORWARDED']))
			$ipAddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(!empty($_SERVER['HTTP_FORWARDED_FOR']))
			$ipAddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(!empty($_SERVER['HTTP_FORWARDED']))
			$ipAddress = $_SERVER['HTTP_FORWARDED'];
		else if(!empty($_SERVER['REMOTE_ADDR']))
			$ipAddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipAddress = self::$defaultIpV4Address;

		if ($ipAddress == '127.0.0.1') {
			$ipAddress = self::$defaultIpV4Address;
		}

		return $ipAddress;

	}
	
	static protected function request( $url, $method = 'GET', $postArray = NULL ) {
		
		$url = parse_url($url);
		$host = $url['host'];
		$path = $url['path'].'?'.$url['query'];
		
		$timeout = 3;
		$fp = fsockopen ($host, 80, $errno, $errstr, $timeout);
		$buf = '';
		if ($fp) {
			
			fputs ($fp, "$method $path HTTP/1.0\nHost: " . $host . "\n\n");
			
			if ($postArray) {
				$data = http_build_query($postArray);
				fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
				fputs($fp, "Content-length: ". strlen($data) ."\r\n");
				fputs($fp, "Connection: close\r\n\r\n");
				fputs($fp, $data);
				
			}
			
			while (!feof($fp)) {
				$buf .= fgets($fp, 128);
			}
			$lines = explode("\n", $buf);
			$data = $lines[count($lines)-1];
			fclose($fp);
			return $data;
		}
		else {
			return false;
		}
		
	}
	
	protected static function getFipsNumberFromIsoCode($iso3166_2Code) {
		
		if (!self::$isoCodeFipsMap) {
			
			self::$isoCodeFipsMap = array (
			  'AB' => '01',
			  'BC' => '02',
			  'MB' => '03',
			  'NB' => '04',
			  'NL' => '05',
			  'NT' => '13',
			  'NS' => '07',
			  'NU' => '14',
			  'ON' => '08',
			  'PE' => '09',
			  'QC' => '10',
			  'SK' => '11',
			  'YT' => '12',
			  'AK' => '02',
			  'AL' => '01',
			  'AZ' => '04',
			  'AR' => '05',
			  'CA' => '06',
			  'CO' => '08',
			  'CT' => '09',
			  'DE' => '10',
			  'DC' => '11',
			  'FL' => '12',
			  'GA' => '13',
			  'HI' => '15',
			  'ID' => '16',
			  'IL' => '17',
			  'IN' => '18',
			  'IA' => '19',
			  'KS' => '20',
			  'KY' => '21',
			  'LA' => '22',
			  'ME' => '23',
			  'MD' => '24',
			  'MA' => '25',
			  'MI' => '26',
			  'MN' => '27',
			  'MS' => '20',
			  'MO' => '29',
			  'MT' => '30',
			  'NE' => '31',
			  'NV' => '32',
			  'NH' => '33',
			  'NJ' => '34',
			  'NM' => '35',
			  'NY' => '36',
			  'NC' => '37',
			  'ND' => '38',
			  'OH' => '39',
			  'OK' => '40',
			  'OR' => '41',
			  'PA' => '42',
			  'PR' => '',
			  'RI' => '44',
			  'SC' => '45',
			  'SD' => '46',
			  'TN' => '47',
			  'TX' => '48',
			  'UT' => '49',
			  'VT' => '50',
			  'VA' => '51',
			  'WA' => '53',
			  'WV' => '54',
			  'WI' => '55',
			  'WY' => '56',
			);	
		}
		return (isset(self::$isoCodeFipsMap[$iso3166_2Code])) ? self::$isoCodeFipsMap[$iso3166_2Code] : NULL;
		
	}
	
}