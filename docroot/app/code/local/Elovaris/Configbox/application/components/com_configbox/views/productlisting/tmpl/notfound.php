<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewProductlisting */
?>
<div id="com_configbox">
	<div id="view-products">
		<div id="template-notfound">
			<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">
				<?php echo KText::_('Product listing not found.');?>
			</div>
		</div>
	</div>
</div>