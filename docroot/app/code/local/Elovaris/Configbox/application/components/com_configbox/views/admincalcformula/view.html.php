<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdmincalcformula extends KenedoView {

	/**
	 * @var string[] Tab info for the condition panels (key is condition type, value is the condition type's title)
	 * @see ConfigboxCondition::getTermTypeNames, ConfigboxCalcTerm::getTypeTitle
	 */
	public $termTabs;

	/**
	 * @var string[] HTML of the condition type's panel, will show up when the user clicks on the type's tab
	 * @see ConfigboxCalcTerm::getTermsPanelHtml
	 */
	public $termPanels;

	/**
	 * @var string The selected panel (fixed)
	 */
	public $selectedTypeName;

	/**
	 * @var string The calculation JSON
	 */
	public $calcJson;

	/**
	 * @var array[] Holding the operator terms data
	 */
	public $operatorData;

	function prepareTemplateVars() {

		// Get the id of the requested calculation
		$id = KRequest::getInt('id', 0);

		// Get the calculation json string
		$calcModel = KenedoModel::getModel('ConfigboxModelAdmincalcformulas');
		$calcJson = $calcModel->getCalculationJson($id);
		$this->assignRef('calcJson', $calcJson);

		$operatorData = array(
			array('type'=>'Operator', 'value'=>'+'),
			array('type'=>'Operator', 'value'=>'-'),
			array('type'=>'Operator', 'value'=>'*'),
			array('type'=>'Operator', 'value'=>'/'),
		);
		$this->assignRef('operatorData', $operatorData);

		// Get all available condition type names
		$conditionTypeNames = ConfigboxCalcTerm::getTermTypeNames();

		// The intended ordering for tabs (other types will be appended after those)
		$ordering = array(
			'ElementAttribute',
			'Calculations',
			'Functions',
			'CustomerGroups',
		);

		// Go through that list and add the real
		$orderedTypeNames = array();
		foreach ($ordering as $typeName) {
			$key = array_search($typeName, $conditionTypeNames);
			if ($key) {
				$orderedTypeNames[] = $conditionTypeNames[$key];
				unset($conditionTypeNames[$key]);
			}
		}
		foreach ($conditionTypeNames as $typeName) {
			$orderedTypeNames[] = $typeName;
		}
		unset($conditionTypeNames);

		// Set up all panels for available conditions
		$tabs = array();
		$panels = array();
		foreach ($orderedTypeNames as $typeName) {
			$term = ConfigboxCalcTerm::getTerm($typeName);
			$tabs[$typeName] = $term->getTypeTitle();
			$panels[$typeName] = $term->getTermsPanelHtml();
			if (empty($panels[$typeName])) {
				unset($panels[$typeName], $tabs[$typeName]);
			}
		}

		$this->assignRef('selectedTypeName', $ordering[0]);
		$this->assignRef('termTabs', $tabs);
		$this->assignRef('termPanels', $panels);

		$this->addViewCssClasses();

	}
	
}
