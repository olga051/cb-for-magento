<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */
?>
<div id="com_configbox">
<div id="view-configurator-page" class="<?php echo hsc($this->pageCssClasses);?>">
<div id="template-<?php echo hsc($this->template);?>">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

	<?php if (ConfigboxPermissionHelper::canQuickEdit()) echo ConfigboxQuickeditHelper::renderConfigurationPageButtons($this->configuratorPage, $this->product);?>

	<?php if ($this->showPageHeading) { ?>
		<h1 class="componentheading configurator-page-title"><span><?php echo hsc($this->configuratorPage->title);?></span></h1>
	<?php } ?>

	<?php if (!empty($this->configuratorPage->description)) { ?>
		<div class="configurator-page-description"><?php echo $this->configuratorPage->description; ?></div>
	<?php } ?>

	<?php if ($this->showProductDetailPanes) { ?>
		<div class="product-detail-panes-wrapper"><?php echo $this->productDetailPanes; ?></div>
	<?php } ?>

	<?php if (empty($this->elements)) { ?>
		<div class="configurator-page-no-elements-note">
			<p><?php echo KText::_('There are no elements on this page.');?></p>
		</div>
	<?php } else { ?>
		<div class="configurator-page-elements">
			<?php
			foreach ($this->elements as $element) {
				$this->element = $element;
				if ($element->calcmodel && $element->text_calcmodel == 0) {
					continue;
				}
				if (!empty($element->class) && method_exists($element, 'loadTemplate')) {
					echo $element->loadTemplate();
				}
				else {
					require($element->templateFile);
				}
			}
			?>
			<div class="clear"></div>
		</div>
	<?php } ?>

	<?php $this->renderView('navigation');?>

	<div class="clear"></div>

</div>
</div>
</div>
</div>