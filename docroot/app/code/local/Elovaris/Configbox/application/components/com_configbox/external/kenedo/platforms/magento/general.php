<?php
defined('CB_VALID_ENTRY') or die();

class KenedoPlatformMagento implements InterfaceKenedoPlatform {
	
	protected $db;

	/**
	 * @var string[] $errors
	 */
	protected $errors;

	public $scriptDeclarations = array();
	
	public function initialize() {

		// Set the option request var like for Joomla, as all is built around that
		if (empty($_REQUEST['option'])) {
			$_REQUEST['option'] = 'com_configbox';
		}

	}
	
	public function getDbConnectionData() {
		
		$config  = Mage::getConfig()->getResourceConnectionConfig("default_setup");

		$connection = new stdClass();

		/** @noinspection PhpUndefinedFieldInspection */
		$connection->hostname 	= $config->host;
		/** @noinspection PhpUndefinedFieldInspection */
		$connection->username 	= $config->username;
		/** @noinspection PhpUndefinedFieldInspection */
		$connection->password 	= $config->password;
		/** @noinspection PhpUndefinedFieldInspection */
		$connection->database 	= $config->dbname;
		$connection->prefix 	= Mage::getConfig()->getTablePrefix();

		return $connection;
		
	}
	
	public function &getDb() {
		if (!$this->db) {
			require_once(dirname(__FILE__).DS.'database.php');
			$this->db = new KenedoDatabaseMagento();
		}
		
		return $this->db;
	}
	
	public function redirect($url) {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.$url);
		die();
	}
	
	//TODO: Implement
	public function logout() {
		Mage::getSingleton('customer/session')->logout();
	}

	//TODO: Implement
	public function authenticate($username, $passwordClear) {
		return true;
	}

	//TODO: Implement
	public function login($username) {
		return true;
		/** @var $session Mage_Customer_Model_Session */
		/*$session = Mage::getSingleton( 'customer/session' );
		
		try {
			$session->login( $username, $password );
			$session->setCustomerAsLoggedIn( $session->getCustomer() );
			return true;
		}
		catch( Exception $e ) {
			return false;
		}*/
		
	}
	
	//TODO: Implement
	public function getTemplateName() {
		return 'template';
	}

	public function sendSystemMessage($text, $type = NULL) {
		
		switch ($type) {
			case 'error':
				Mage::getSingleton('core/session')->addError($text);
				break;
			case 'success':
				Mage::getSingleton('core/session')->addSuccess($text);
				break;
			default:
				Mage::getSingleton('core/session')->addNotice($text);
				break;
		}
		
	}
	//TODO: Implement
	public function getVersionShort() {
		return Mage::getVersion();
	}

	public function getDebug() {
		return intval(Mage::getIsDeveloperMode());
	}
	
	public function getDefaultListlimit() {
		return 50;
	}
	//TODO: Implement
	public function getConfigOffset() {
		return Mage::app()->getStore()->getConfig('general/locale/timezone');
	}
	//TODO: Implement
	public function getMailerFromName() {
		return Mage::getStoreConfig('trans_email/ident_general/name');
	}
	//TODO: Implement
	public function getMailerFromEmail() {
		return Mage::getStoreConfig('trans_email/ident_general/email');
	}

	public function getTmpPath() {
		return Mage::getBaseDir('tmp');
	}

	public function getLogPath() {
		return Mage::getBaseDir('log');
	}

	public function getLanguageTag() {
		$code = Mage::app()->getLocale()->getLocaleCode();
		$code = str_replace('_','-',$code);
		return $code;
	}
	
	public function getLanguageUrlCode($languageTag = NULL) {

		if ($languageTag == NULL) {
			$languageTag = $this->getLanguageTag();
		}

		$languages = $this->getLanguages();

		if (!empty($languages[$languageTag])) {
			return $languages[$languageTag]->urlCode;
		}
		else {
			return NULL;
		}

	}
	
	//TODO: Check if good enough
	public function getDocumentType() {
		return KRequest::getKeyword('format','html');
	}
	
	public function addScript($path, $type = "text/javascript", $defer = false, $async = false) {
		$GLOBALS['document']['scripts'][$path] = $path;
	}
	
	public function addScriptDeclaration($js, $newTag = false, $toBody = false) {
		$tag = '<script type="text/javascript">'."\n//<![CDATA[\n";
		$tag.= $js;
		$tag.= "\n//]]>\n".'</script>';
		$this->scriptDeclarations[] = $tag;
	}
	
	public function addStylesheet($path, $type='text/css', $media = 'all') {
		$GLOBALS['document']['stylesheets'][$path] = $path;
	}
	
	public function addStyleDeclaration($css) {
		$css = '<style type="text/css">'.$css.'</style>';
		$GLOBALS['document']['styles'][] = $css;
	}
	
	public function isAdminArea() {

		if(Mage::getDesign()->getArea() == 'adminhtml') {
			return true;
		}

		return Mage::app()->getStore()->isAdmin();

	}

	public function isSiteArea() {
		return Mage::app()->getStore()->isAdmin() == false;
	}

	public function autoload($className, $classPath) {
		include_once($classPath);
	}

	public function processContentModifiers($text) {
		return $text;
	}

	public function triggerEvent($eventName, $data) {
		return array(true);
	}

	public function raiseError($errorCode, $errorMessage) {
		die($errorCode . ' - '.$errorMessage);
	}

	public function renderHtmlEditor($dataFieldKey, $content, $width, $height, $cols, $rows) {
		return '<textarea name="'.hsc($dataFieldKey).'" id="'.hsc($dataFieldKey).'" class="kenedo-html-editor" style="width:'.(int)$width.'px; height:'.$height.'px" rows="'.(int)$rows.'" cols="'.(int)$cols.'">'.hsc($content).'</textarea>';
	}
	//TODO: Implement
	public function sendEmail($from, $fromName, $receipient, $subject, $body, $isHtml = false, $cc = NULL, $bcc = NULL, $attachmentPath = NULL) {
		return true;
	}
	//TODO: Use
	public function getGeneratorTag() {
		return (isset($GLOBALS['document']['metatags']['generator'])) ? $GLOBALS['document']['metatags']['generator'] : '';
	}
	//TODO: Use
	public function setGeneratorTag($string) {
		$GLOBALS['document']['metatags']['generator'] = $string;
	}

	public function getUrlBase() {
		return rtrim(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB), '/');
	}

	public function getDocumentBase() {
		return $this->getUrlBase();
	}
	//TODO: Implement
	public function setDocumentBase($string) {
		return true;
	}
	//TODO: Implement
	public function setDocumentMimeType($mime) {
		return $GLOBALS['document']['mimetype'] = $mime;
	}
	//TODO: Implement
	public function getDocumentTitle() {
		//Mage::app()->getLayout()->getBlock('head')->getTitle();
	}

	//TODO: Implement
	public function setDocumentTitle($string) {
		//Mage::app()->getLayout()->getBlock('head')->setTitle($string);
	}
	//TODO: Use
	public function setMetaTag($tag,$content) {
		$GLOBALS['document']['metatags'][$tag] = $content;
	}

	public function isLoggedIn() {
		return KSession::get('logged_in',false);
	}
	//TODO: Test
	public function getUserId() {
		if(Mage::getSingleton('customer/session')->isLoggedIn()) {
			$customerData = Mage::getSingleton('customer/session')->getCustomer();
			return $customerData->getId();
		}
		else {
			return 0;
		}
	}

	//TODO: Test
	public function getUserName($userId = NULL) {

		if ($userId == NULL) {
			if(Mage::getSingleton('customer/session')->isLoggedIn()) {
				$user = Mage::getSingleton('admin/session');
				/** @noinspection PhpUndefinedMethodInspection */
				$username = $user->getUser()->getUsername();
			}
			else {
				$username = NULL;
			}
		}
		else {
			/** @noinspection PhpUndefinedMethodInspection */
			$customerData = Mage::getModel('customer/customer')->load($userId)->getData();
			$username = $customerData['email'];
		}

		return $username;
	}

	//TODO: Test
	public function getUserFullName($userId = NULL) {

		if ($userId == NULL) {
			$userId = $this->getUserId();
		}

		if ($userId) {
			/** @noinspection PhpUndefinedMethodInspection */
			$customerData = Mage::getModel('customer/customer')->load($userId)->getData();
			$name = $customerData['firstname'].  ' '. $customerData['lastname'];
		}
		else {
			$name = '';
		}

		return $name;

	}
	public function getUserPasswordEncoded($userId = NULL) {
		return KSession::get('user_passwordencoded',false);
	}
	//TODO: Test
	public function getUserIdByUsername($username) {

		$customer = Mage::getModel('customer/customer');
		/** @noinspection PhpUndefinedMethodInspection */
		$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
		$customer->loadByEmail($username);

		try{
			if ($customer->getId()) {
				return $customer->getId();
			}
		}
		catch(Exception $e) {
			return NULL;
		}
		return NULL;
	}
	//TODO: Implement
	public function getUserGroupId($userId = NULL) {
		return KSession::get('user_group_ids',false);
	}
	//TODO: Test
	public function getUserTimezoneName($userId = NULL) {
		return $this->getConfigOffset();
	}
	//TODO: Test
	public function registerUser($data, $groupIds = array()) {

	}

	protected function unsetErrors() {
		$this->errors = array();
	}

	protected function setError($error) {
		$this->errors[] = $error;
	}

	protected function setErrors($errors) {
		if (is_array($errors) && count($errors)) {
			$this->errors = array_merge((array)$this->errors,$errors);
		}
	}

	public function getErrors() {
		return $this->errors;
	}

	public function getError() {
		if (is_array($this->errors) && count($this->errors)) {
			return end($this->errors);
		}
		else {
			return '';
		}
	}

	public function isAuthorized($task,$userId = NULL, $minGroupId = NULL) {

		$admin = Mage::getModel('admin/session')->getUser();

		if(!$admin || $admin->getId() == '') {
			return false;
		}
		else {
			return true;
		}

	}
	//TODO: Test
	public function passwordsMatch($passwordClear, $passwordEncrypted) {

		$hash = Mage::helper('core')->getHash($passwordClear, 2);

		return Mage::helper('core')->validateHash($passwordEncrypted, $hash);

	}

	//TODO: Check actual standards
	public function passwordMeetsStandards($password) {
		if (mb_strlen($password) < 8) {
			return false;
		}
		if ( preg_match("/[0-9]/", $password) == 0 || preg_match("/[a-zA-Z]/", $password) == 0) {
			return false;
		}

		return true;
	}

	//TODO: Check actual standards
	public function getPasswordStandardsText() {
		return KText::_('Your password should contain at least 8 characters and should contain numbers and letters.');
	}

	//TODO: Implement
	public function changeUserPassword($userId, $passwordClear) {
		return true;
	}

	//TODO: Test
	public function getRootDirectory() {
		return Mage::getBaseDir();
	}
	//TODO: Implement
	public function getAppParameters() {
		$params = new KStorage();
		return $params;
	}

	public function renderOutput(&$output) {
		echo $output;
		foreach ($this->scriptDeclarations as $js) {
			echo $js."\n";
		}
	}

	public function startSession() {
		session_start();
		return true;
	}

	//TODO: Implement
	public function getPasswordResetLink() {
		return Mage::getUrl('*/*/forgotpassword');
	}

	public function getPlatformLoginLink() {
		return Mage::helper('customer')->getLoginUrl();
	}

	public function getRoute($url, $encode = true, $secure = NULL) {

		if (strpos($url,'http') === 0) {
			return $url;
		}

		$parsed = parse_url($url);

		if (isset($parsed['query'])) {
			$params = array();
			parse_str($parsed['query'],$params);
		}

		$params['form_key'] = Mage::getSingleton('core/session')->getFormKey();

		if ($secure !== NULL) {
			$params['_secure'] = $secure;
		}

		if ($this->isAdminArea()) {
			$url = Mage::helper('adminhtml')->getUrl('*/*/index', $params);
		}
		else {
			if (isset($_REQUEST['key'])) {
				$params['key'] = $_REQUEST['key'];
			}
			$url = Mage::getUrl('configbox/index/index',$params);
		}

		return $url;

	}
	
	public function getActiveMenuItemId() {
		return 0;
	}

	public function getLanguages() {

		$languages = Mage::app()->getLocale()->getOptionLocales();

		$return = array();
		foreach ($languages as $language) {
			$tag = str_replace('_', '-', $language['value']);
			$label = $language['label'];
			$return[$tag] = new KenedoObject(array('tag'=>$tag, 'label'=>$label, 'urlCode'=>$tag));
		}
		return $return;

	}
	
	//TODO: Implement
	public function platformUserEditFormIsReachable() {
		return false;
	}
	
	//TODO: Implement
	public function userCanEditPlatformUsers() {
		return false;
	}
	
	//TODO: Implement
	public function getPlatformUserEditUrl($platformUserId) {
		return '';
	}

	public function getComponentDir($componentName) {
		return $this->getRootDirectory().DS.'app'.DS.'code'.DS.'local'.DS.'Elovaris'.DS.'Configbox'.DS.'application'.DS.'components'.DS.strtolower($componentName);
	}

	public function getUrlAssets() {
		$path = Mage::getBaseUrl('skin').'frontend/base/default/css/elovaris/configbox/assets/';
		return $path;
	}

	public function getDirAssets() {
		$path = Mage::getBaseDir('skin').'frontend/base/default/css/elovaris/configbox/assets/';
		return $path;
	}

	public function getDirCache() {
		$path = Mage::getBaseDir('cache');
		return $path;
	}

	public function getDirCustomization() {
		$path = Mage::getBaseDir('media').DS.'elovaris'.DS.'configbox'.DS.'customization';
		return $path;
	}

	public function getDirCustomizationAssets() {
		$path = Mage::getBaseDir('media').DS.'elovaris'.DS.'configbox'.DS.'customization'.DS.'assets';
		return $path;
	}

	public function getUrlCustomizationAssets() {
		$path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).DS.'elovaris'.DS.'configbox'.DS.'customization'.DS.'assets';
		return $path;
	}

	public function getDirCustomizationSettings() {
		$path = Mage::getBaseUrl('media').DS.'elovaris'.DS.'configbox'.DS.'settings';
		return $path;
	}

	public function getDirDataCustomer() {
		$path = Mage::getBaseDir('media').DS.'elovaris'.DS.'configbox'.DS.'customer_data';
		return $path;
	}

	public function getUrlDataCustomer() {
		$path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) .'elovaris/configbox/customer_data';
		return $path;
	}

	public function getDirDataStore() {
		$path = Mage::getBaseDir('media').DS.'elovaris'.DS.'configbox'.DS.'store_data';
		return $path;
	}

	public function getUrlDataStore() {
		$path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) .'elovaris/configbox/store_data';
		return $path;
	}

	public function getTemplateOverridePath($component, $viewName, $templateName) {
		$path = '';
		return $path;
	}

}