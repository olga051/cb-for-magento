<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCustomerform */
?>
<div class="customer-field customer-field-platform_user_id">
	<label for="platform_user_id" class="field-label"><?php echo KText::_('Platform User ID');?></label>
	<div class="form-field">
		<input class="textfield" type="text" id="platform_user_id" name="platform_user_id" value="<?php echo intval($this->customerData->platform_user_id);?>" />
		<div class="validation-tooltip"></div>
	</div>
</div>
<div class="customer-field customer-field-group_id">
	<label for="group_id" class="field-label"><?php echo KText::_('Customer group');?></label>
	<div class="form-field">
		<?php echo $this->groupDropDownHtml;?>
		<div class="validation-tooltip"></div>
	</div>
</div>
<div class="customer-field customer-field-custom_1">
	<label for="custom_1" class="field-label"><?php echo KText::_('Custom field 1');?></label>
	<div class="form-field">
		<input class="textfield" type="text" id="custom_1" name="custom_1" value="<?php echo hsc($this->customerData->custom_1);?>" />
		<div class="validation-tooltip"></div>
	</div>
</div>
<div class="customer-field customer-field-custom_2">
	<label for="custom_2" class="field-label"><?php echo KText::_('Custom field 2');?></label>
	<div class="form-field">
		<input class="textfield" type="text" id="custom_2" name="custom_2" value="<?php echo hsc($this->customerData->custom_2);?>" />
		<div class="validation-tooltip"></div>
	</div>
</div>
<div class="customer-field customer-field-custom_3">
	<label for="custom_3" class="field-label"><?php echo KText::_('Custom field 3');?></label>
	<div class="form-field">
		<input class="textfield" type="text" id="custom_3" name="custom_3" value="<?php echo hsc($this->customerData->custom_3);?>" />
		<div class="validation-tooltip"></div>
	</div>
</div>
<div class="customer-field customer-field-custom_4">
	<label for="custom_4" class="field-label"><?php echo KText::_('Custom field 4');?></label>
	<div class="form-field">
		<input class="textfield" type="text" id="custom_4" name="custom_4" value="<?php echo hsc($this->customerData->custom_4);?>" />
		<div class="validation-tooltip"></div>
	</div>
</div>
