<?php
defined('CB_VALID_ENTRY') or die();

class ObserverOrders {
	
	public $checkedOutStatus = 1;
	public $savedStatus = 8;
	public $quoteRequestedStatus = 11;
	public $orderConfirmedStatus = 4;
	public $assistanceRequestedStatus = 12;
	
	function onConfigBoxGetStatusCodes() {
		
		return array(
			KText::_('Not ordered'),					// 0
			KText::_('In Checkout'),					// 1
			KText::_('Ordered'),						// 2
			KText::_('Paid'),							// 3
			KText::_('Confirmed'),						// 4
			KText::_('Shipped'),						// 5
			KText::_('Cancelled'),						// 6
			KText::_('Refunded'),						// 7
			KText::_('Saved'),							// 8
			KText::_('Incorrect amount paid'), 			// 9
			KText::_('Payed in unsupported currency'), 	// 10
			KText::_('Quotation sent'), 				// 11
			KText::_('Assistance requested'), 			// 12
			KText::_('Deposit paid'), 					// 13
			KText::_('Quotation requested'), 			// 14
			);	
	}
	
	function onConfigBoxGetStatusCodeForType($type) {
		
		switch ($type) {
			
			case 'refunded': 						return 7;
			case 'ordered': 						return 2;
			case 'paid': 							return 3;
			case 'shipped': 						return 5;
			case 'checked out': 					return $this->checkedOutStatus;
			case 'incorrect amount paid': 			return 9;
			case 'payed in unsupported currency': 	return 10;
			case 'confirmed': 						return $this->orderConfirmedStatus;
			case 'quote requested': 				return $this->quoteRequestedStatus;
			case 'assistance requested': 			return $this->assistanceRequestedStatus;
			case 'deposit paid': 					return 13;
			
			default: return null;
		}
		
	}
	/*
	 * Checks permissions for certain actions with carts or order records
	 * 
	 * @param 	string	  	action to check permission for
	 * @param	object		cart details
	 * 
	 */
	function onConfigBoxGetActionPermission($action,$cartDetails) {

		// Legacy, remove in 2.7
		if ($action == 'removeGrandorder') {
			KLog::logLegacyCall('Change the action permission from removeGrandorder to removeCart.');
			$action = 'removeCart';
		}
		
		switch ($action) {
			
			case 'editOrder': 			$codes = array(0,8,11,12,14)	; break;
			case 'removeOrderRecord':	$codes = array(0,1,8,11,12,14)	; break;
			case 'saveOrder': 			$codes = array(0,1,8,11,12,14)	; break;
			case 'checkoutOrder': 		$codes = array(0,1,8,11,12,14)	; break;
			case 'cancelOrder': 		$codes = array(2)				; break;
			case 'removeCart': 			$codes = array(0,8,11,12,14)	; break;
			case 'goBackToCart': 		$codes = array(0,1, 8)			; break;
			case 'addToOrder': 			$codes = array(0,1,8,11,12,14)	; break;
			case 'cleanOrder': 			$codes = array(0,1)				; break;
			case 'placeOrder': 			$codes = array(0,1,8,11,14)		; break;
			case 'getInvoice': 			$codes = array(3,4,5)			; break;
			case 'requestQuote': 		$codes = array(0,1,8,11,12,14)	; break;
			case 'requestAssistance': 	$codes = array(0,1,8,11,12,14)	; break;
			case 'downloadProducts': 	$codes = array(3)				; break;
			
			default:
				KLog::log('No permissions set for action "'.$action.'".','error',KText::_('A system error occured.'));
				return false;
				break;
			
		}
		
		// Get the order status
		if (!isset($cartDetails->status) || $cartDetails->status === null) {
			KenedoObserver::triggerEvent('onConfigBoxGetStatus',array(&$cartDetails));
		}
		
		$status = (isset($cartDetails->status)) ? intval($cartDetails->status) : 0;
		
		// If it's a status unknown to this connector, don't get involved.
		// Other statuses can be added by connectors and have their own way of checking permissions
		$allStati = $this->onConfigBoxGetStatusCodes();
		
		if (!isset($allStati[$status])) {
			return NULL;
		}
		
		if (in_array($status, $codes)) {
			$result =  true;
		}
		else {
			$result = false;
		}
		
		KLog::log('Permission for action "'.$action.'" and status "'.$cartDetails->status.'" requested. Was '.(($result)?'GRANTED':'DENIED'),'debug');
		return $result;
		
	}
	
	function onConfigBoxSetStatus( $orderId, $status ) {
		
		$query = "UPDATE `#__cbcheckout_order_records` SET `status` = ".(int)$status." WHERE `id` = ".(int)$orderId." LIMIT 1";
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$succ = $db->query();
		
		if ($succ) {
			KLog::log('Set status of order record with ID "'.$orderId.'" to "'.$status.'" ','debug');
			return true;
		}
		else {
			KLog::log('Could not set status of record with ID "'.$orderId.'" to "'.$status.'". Error was "'.$db->getErrorMsg().'"','error',KText::_('A system error occured.'));
			return false;	
		}
		
	}
	
	function onConfigBoxGetStatus(&$cartDetails) {
		
		// Legacy, on 2.7 have the id coming from ->id only
		if (!empty($cartDetails->grandorder_id)) {
			KLog::logLegacyCall('onConfigBoxGetStatus called and it provided a grandorder_id, check the source of the call and make sure ->id gets the cart id.');
			$cartId = $cartDetails->grandorder_id;
		}
		elseif (!empty($cartDetails->cart_id)) {
			KLog::logLegacyCall('onConfigBoxGetStatus called. Looks like an orderrecord was provided instead of a cart details record.');
			$cartId = $cartDetails->cart_id;
		}
		else {
			$cartId = $cartDetails->id;
		}
		
		$query = "SELECT `status` FROM `#__cbcheckout_order_records` WHERE `cart_id` = ".intval($cartId)." LIMIT 1";
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		
		$codeStrings = $this->onConfigBoxGetStatusCodes();
				
		$cartDetails->status = (int) $db->loadResult();
		$cartDetails->statusString = (!empty($codeStrings[$cartDetails->status])) ? $codeStrings[$cartDetails->status] : $cartDetails->status;
		
		return $cartDetails->status;
		
	}
	
	function onConfigBoxAddToCart(&$cartDetails) {

		foreach ($cartDetails->positions as $position) {
			$positionId = $position->id;
			$configuration = ConfigboxConfiguration::getInstance($positionId);
			$configuration->storeSelectionsInDb();
		}

		$cartDetails->redirectURL = 'index.php?option=com_configbox&view=cart';
	
		return true;
	}

	function onConfigBoxGetAssistanceLink($cartId) {
	
		$db = KenedoPlatform::getDb();
	
		$query = "SELECT `user_id` FROM `#__configbox_carts` WHERE `id` = ".(int) $cartId;
		$db->setQuery($query);
		$userId = $db->loadResult();
	
		$query = "SELECT `id` FROM `#__cbcheckout_order_records` WHERE `cart_id` = ".(int) $cartId." LIMIT 1";
		$db->setQuery($query);

		$userData = ConfigboxUserHelper::getUser($userId, false, false);
		
		if ($userData) {
			$model = KenedoModel::getModel('ConfigboxModelAdmincustomers');
			$userDataOk = $model->validateData($userData, 'assistance');
		}
		else {
			$userDataOk = false;
		}
	
		if ($userDataOk) {
			$return = new StdClass();
			$return->type = 'link';
			$return->newTab = false;
			$return->url = KLink::getRoute('index.php?option=com_configbox&controller=order&task=requestAssistance&cart_id='.intval($cartId), true);
			return $return;
		}
		else {
			$return = new StdClass();
			$return->type = 'modal';
			$return->width = 450;
			$return->height = 700;
			$return->url = KLink::getRoute('index.php?option=com_configbox&view=inforequest&tmpl=component&type=assistance&from_cart=1&cart_id='.intval($cartId), true);
			return $return;
		}
	
	}
	
	function onConfigBoxGetSaveOrderLink($cartId) {
	
		$db = KenedoPlatform::getDb();
	
		$query = "SELECT `user_id` FROM `#__configbox_carts` WHERE `id` = ".(int) $cartId;
		$db->setQuery($query);
		$userId = $db->loadResult();
	
		$query = "SELECT `id` FROM `#__cbcheckout_order_records` WHERE `cart_id` = ".(int) $cartId." LIMIT 1";
		$db->setQuery($query);
		$orderRecordId = $db->loadResult();
	
		if ($orderRecordId) {
			$userData = ConfigboxUserHelper::getOrderAddress($orderRecordId);
		}
		else {
			$userData = ConfigboxUserHelper::getUser($userId, false, false);
		}
	
		if ($userData) {
			$model = KenedoModel::getModel('ConfigboxModelAdmincustomers');
			$userDataOk = $model->validateData($userData, 'saveorder');
		}
		else {
			$userDataOk = false;
		}
	
		if ($userDataOk) {
			$return = new StdClass();
			$return->type = 'link';
			$return->newTab = false;
			$return->url = KLink::getRoute('index.php?option=com_configbox&controller=order&task=saveOrder&cart_id='.intval($cartId), true);
			return $return;
		}
		else {
			$return = new StdClass();
			$return->type = 'modal';
			$return->width = 450;
			$return->height = 700;
			$return->url = KLink::getRoute('index.php?option=com_configbox&view=inforequest&tmpl=component&type=saveorder&from_cart=1&cart_id='.intval($cartId), true);
			return $return;
		}
	
	}

	function onConfigBoxRequestAssistance(&$cartDetails) {
	
		KLog::log('Quote request for cart ID "'.$cartDetails->id.'"','debug');

		$status = $this->assistanceRequestedStatus;
		
		// Create the order record
		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
		$checkoutRecordId = $orderModel->createOrderRecord($cartDetails, $status);
		
		// Set the status
		KenedoObserver::triggerEvent('onConfigboxSetStatus', array($checkoutRecordId, $status));
		
		// Set the order_id in session
		$orderModel->setId($checkoutRecordId);

		// Add the order address record
		ConfigboxUserHelper::setOrderAddress($checkoutRecordId);
		
		// Set the redirectURL
		$cartDetails->redirectURL = 'index.php?option=com_configbox&view=cart&cart_id='.intval($cartDetails->id);
		return true;
		
	}
	
	function onConfigBoxSaveCart(&$cartDetails) {
	
		KLog::log('Saving cart for cart ID "'.$cartDetails->id.'"','debug');
		
		$status = $this->savedStatus;
		
		// Create the order record
		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
		$checkoutRecordId = $orderModel->createOrderRecord($cartDetails, $status);
		
		// Set the status
		KenedoObserver::triggerEvent('onConfigboxSetStatus', array($checkoutRecordId,$status));
		
		// Set the order_id in session
		$orderModel->setId($checkoutRecordId);

		// Add the order address record
		ConfigboxUserHelper::setOrderAddress($checkoutRecordId);
		
		// Set the redirectURL
		$cartDetails->redirectURL = 'index.php?option=com_configbox&view=user';
		return true;
	
	}
	
	function onConfigBoxCheckout(&$cartDetails) {
	
		KLog::log('Checkout cart for cart ID "'.$cartDetails->id.'"','debug');
		
		$status = $this->checkedOutStatus;
		
		// Create the order record
		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
		$checkoutRecordId = $orderModel->createOrderRecord($cartDetails, $status);
		
		// Set the status
		KenedoObserver::triggerEvent('onConfigboxSetStatus', array($checkoutRecordId,$status));
		
		// Set the order_id in session
		$orderModel->setId($checkoutRecordId);

		if (defined('CONFIGBOX_CUSTOM_RESET_CART_ON_CHECKOUT') == false || CONFIGBOX_CUSTOM_RESET_CART_ON_CHECKOUT == true) {
			// Unset the cart data
			$cartModel = KenedoModel::getModel('ConfigboxModelCart');
			$cartModel->resetCart();
		}

		// Add the order address record
		ConfigboxUserHelper::setOrderAddress($checkoutRecordId);

		$cartDetails->redirectURL = 'index.php?option=com_configbox&view=checkout';
		
		return true;
	
	}

	/**
	 * @param object $cartDetails Cart details from ConfigboxModelCart->getCartDetails();
	 */
	function onConfigBoxGetCbOrderId(&$cartDetails) {
		
		$query = "SELECT `id` FROM `#__cbcheckout_order_records` WHERE `cart_id` = ".intval($cartDetails->id)." LIMIT 1";
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$id = $db->loadResult();
		if ($id !== NULL) $cartDetails->cbOrderId = $id;
		else $cartDetails->cbOrderId = null;
		
	}

	/**
	 * @param object $cartDetails Cart details (use it to get the order address in case we got a connected order record)
	 * @param int $userId ConfigBox user id
	 * @return object $userData (see ConfigboxUserHelper::getUser()
	 */
	function onConfigBoxGetUserData($cartDetails = NULL, $userId = NULL) {

		// If we got cart details, take the order address in case we got a matching order record
		if ($cartDetails) {
			$db = KenedoPlatform::getDb();
			$query = "SELECT `id` FROM `#__cbcheckout_order_records` WHERE `cart_id` = ".intval($cartDetails->id)." LIMIT 1";
			$db->setQuery($query);
			$orderRecordId = $db->loadResult();
			
			if ($orderRecordId) {
				$user = ConfigboxUserHelper::getOrderAddress($orderRecordId);
			}
			else {
				$user = ConfigboxUserHelper::getUser($cartDetails->user_id);
			}

			return $user;

		}
		elseif ($userId) {
			$user = ConfigboxUserHelper::getUser($userId);
			return $user;
		}
		else {
			$userId = ConfigboxUserHelper::getUserId();
			if (!$userId) {
				return false;
			}
			else {
				$user = ConfigboxUserHelper::getUser($userId);
			}
			
			return $user;
		}

	}

	/**
	 * @param int $userId ConfigBox user id
	 * @param int $groupId Configbox group id;
	 * @return object group data (see ConfigboxUserHelper::getGroupData()
	 */
	function &onConfigboxGetGroupData($userId = NULL, $groupId = NULL) {
		
		if ($groupId) {
			$groupData = ConfigboxUserHelper::getGroupData($groupId);
			return $groupData;
		}
		
		if (!$userId) {
			$userId = ConfigboxUserHelper::getUserId();
		}
		
		if ($userId) {
			$user = ConfigboxUserHelper::getUser($userId,false);
			$groupData = ConfigboxUserHelper::getGroupData($user->group_id);
			if ($groupData) {
				return $groupData;
			}
			else {
				$groupData = ConfigboxUserHelper::getGroupData( CONFIGBOX_DEFAULT_CUSTOMER_GROUP_ID );
				return $groupData;
			}
			
		}
		else {
			$groupData = ConfigboxUserHelper::getGroupData( CONFIGBOX_DEFAULT_CUSTOMER_GROUP_ID );
			return $groupData;
		}
		
	}

	/**
	 * @param object $cartDetails Cart details from ConfigboxModelCart->getCartDetails();
	 * @return bool $success
	 */
	function onConfigBoxRemoveCart(&$cartDetails) {
		
		KLog::log('Deleting order record. Cart ID is "'.$cartDetails->id.'".');
		
		$db = KenedoPlatform::getDb();
		$query = "SELECT `id` FROM `#__cbcheckout_order_records` WHERE `cart_id` = ".intval($cartDetails->id);
		$db->setQuery($query);
		$orderRecordId = $db->loadResult();
		
		if (!$orderRecordId) {
			KLog::log('No order record id found for cart ID "'.$cartDetails->id.'".');
			return true;
		}
		
		// Get the model and try to remove them
		$model = KenedoModel::getModel('ConfigboxModelAdminorders');
		$success = $model->delete(array($orderRecordId));
		
		return $success;
		
	}
	
	function onConfigboxGetTaxRate( &$taxRate, $taxRateId, $userId = NULL ) {
		
		if (!$userId) {
			$userId = ConfigboxUserHelper::getUserId();
		}
		$userData = ConfigboxUserHelper::getUser($userId);
		
		$newRate = ConfigboxUserHelper::getTaxRate($taxRateId, $userData);
		
		if ($newRate !== NULL) {
			$taxRate = $newRate;
		}
		
		return $newRate;
	}

	function onConfigBoxIsVatFree( $userRecord ) {
		return ConfigboxUserHelper::isVatFree($userRecord);
	}
	
	function onConfigboxGetDeliveryOptions($cartId, $customerData, $weight, $maxDimensions, $cheapestOnly = false) {
		$deliveryModel = KenedoModel::getModel('ConfigboxModelDelivery');
		return $deliveryModel->getDeliveryOptions($cartId, $customerData, $weight, $maxDimensions, $cheapestOnly);
	}
	
	function onConfigboxGetPaymentOptions($customerData, $baseTotal) {
		$paymentModel = KenedoModel::getModel('ConfigboxModelPayment');
		return $paymentModel->getPaymentOptions($customerData, $baseTotal);
	}
	
}