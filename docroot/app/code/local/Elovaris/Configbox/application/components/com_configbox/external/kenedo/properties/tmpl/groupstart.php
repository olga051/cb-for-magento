<?php 
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this KenedoPropertyGroupstart
 */

if ($this->getPropertyDefinition('toggle') == true) {

	$sessionKey = $this->getSessionKey();
	$defaultState = $this->getPropertyDefinition('default-state', 'closed');
	$state = KSession::get($sessionKey, $defaultState, 'kenedo');

	$fieldsetClasses = 'property-group property-group-'.$this->propertyName.' property-group-using-toggles';
	
	if ($state == 'opened') {
		$fieldsetClasses .= ' property-group-opened';
	}
	else {
		$fieldsetClasses .= ' property-group-closed';
	}
}
else {
	$fieldsetClasses = 'property-group property-group-'.$this->propertyName.' property-group-opened';
	$state = 'opened';
}

$html = '<div class="'.hsc($fieldsetClasses).'"><h2 class="property-group-legend">'.hsc($this->getPropertyDefinition('title')).'</h2>';

if ($this->getPropertyDefinition('toggle') == true) {
	$html .= '<div><input class="property-group-toggle-state" type="hidden" name="toggle-state-'.hsc($this->propertyName).'" value="'.hsc($state).'" /></div>';
}

$html .= '<div class="property-group-content">';

if ($this->getPropertyDefinition('notes', '') != '') {
	$html .= '<div class="property-group-notes">';
	$html .= '<h3 class="property-group-notes-title">'.( ($this->getPropertyDefinition('noteHeading', '')) ? KText::_('Notes') : hsc($this->getPropertyDefinition('noteHeading', ''))).'</h3>';
	$html .= $this->getPropertyDefinition('notes', '').'</div>';
}

$html .= '<div class="property-group-properties">';

echo $html;