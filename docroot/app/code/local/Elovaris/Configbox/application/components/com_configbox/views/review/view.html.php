<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewReview extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var object[] Reviews for the item (can be a product or option)
	 */
	public $reviews;

	/**
	 * @var string Kind of item (product or xref - but xref means option)
	 */
	public $kind;

	/**
	 * @var object Product or Option data
	 */
	public $item;

	/**
	 * @var int ID of the item (not a review but the product or option)
	 */
	public $id;

	/**
	 * @var int Number of reviews to be shown initially (rest shows up on click on 'more')
	 */
	public $listCount;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {
		
		$model = KenedoModel::getModel('ConfigboxModelReviews');
		$kind = KRequest::getKeyword('kind','xref');
		$id = KRequest::getInt('id');
		
		$this->assignRef('id',$id);
		$this->assignRef('kind',$kind);
		
		$reviews = $model->getReviews($id, $kind);
		$this->assignRef('reviews',$reviews);
		
		if ($kind == 'product') {
			$productModel = KenedoModel::getModel('ConfigboxModelProduct');
			$item = $productModel->getProduct($id);
			$item->image = ($item->prod_image) ? CONFIGBOX_URL_PRODUCT_IMAGES.'/'.$item->prod_image : NULL;
		}
		else {
			$optionModel = KenedoModel::getModel('ConfigboxModelOption');
			if ($kind == 'xref') {
				$item = $optionModel->getOption(NULL,$id);
				$item->image = ($item->option_image) ? CONFIGBOX_URL_OPTION_IMAGES.'/'.$item->option_image : NULL;
			}
			else {
				$item = $optionModel->getOption($id,NULL);
			}
		}
		
		$this->assignRef('item', $item);
		
		if ($item->description) {
			$this->assign('showDescription',true);
		}
		
		$this->assign('listCount',3);

	}
	
}