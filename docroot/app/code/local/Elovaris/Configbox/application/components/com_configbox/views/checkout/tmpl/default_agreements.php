<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCheckout */
?>
<div class="agreements">
	<?php if ($this->confirmTerms) { ?>
		<div class="agreement-terms">
			<input type="checkbox" id="agreement-terms" name="agreement-terms" value="1" />			
			<label for="agreement-terms"><?php echo KText::_('I have read and agree to the terms and conditions.');?></label>
			<span class="wrapper-link-terms"><?php echo $this->linkTerms;?></span>
		</div>
	<?php } ?>
	
	<?php if ($this->confirmRefundPolicy) { ?>
		<div class="agreement-refund-policy">
			<input type="checkbox" id="agreement-refund-policy" name="agreement-refund-policy" value="1" />			
			<label for="agreement-refund-policy"><?php echo KText::_('I have read and agree to the refund policy.');?></label>
			<span class="wrapper-link-refund-policy"><?php echo $this->linkRefundPolicy;?></span>
		</div>
	<?php } ?>
</div>