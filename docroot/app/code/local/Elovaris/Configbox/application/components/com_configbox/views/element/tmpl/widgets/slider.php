<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */
?>
<div>
	<span id="configbox-widget-display-<?php echo $this->element->id;?>" class="configbox-widget-slider-display"><?php echo hsc($this->element->getRawValue());?></span>
	<span class="element-unit"><?php echo hsc($this->element->unit);?></span>
</div>
<div id="element-widget-<?php echo $this->element->id;?>" class="configbox-widget configbox-widget-slider"></div>