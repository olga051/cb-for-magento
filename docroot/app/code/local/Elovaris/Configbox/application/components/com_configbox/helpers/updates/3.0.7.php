<?php
defined('CB_VALID_ENTRY') or die();

$db = KenedoPlatform::getDb();

// Rename the formula field to 'code' in calculation_codes
if (ConfigboxUpdateHelper::tableExists('#__configbox_calculation_codes') == true && ConfigboxUpdateHelper::tableFieldExists('#__configbox_calculation_codes', 'formula') == true) {
	$query = "ALTER TABLE  `#__configbox_calculation_codes` CHANGE  `formula`  `code` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL";
	$db->setQuery($query);
	$db->query();
}

if (KenedoPlatform::getName() == 'magento') {

	$mediaDir = Mage::getBaseDir('media');

	$files = array();
	$files[] = $mediaDir.'/elovaris/configbox/customization/assets/javascript/extra_functionality.js';
	$files[] = $mediaDir.'/elovaris/configbox/customization/assets/css/style_overrides.css';

	foreach ($files as $file) {
		if (!is_file($file)) {

			if (!is_dir(dirname($file))) {
				mkdir(dirname($file), 0777, true);
			}

			file_put_contents($file, '');
		}
	}

}