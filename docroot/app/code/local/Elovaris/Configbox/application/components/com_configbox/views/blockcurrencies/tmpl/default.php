<?php
defined('CB_VALID_ENTRY') or die('Restricted access');
/** @var $this ConfigboxViewBlockCurrencies */
?>

<div class="configbox-block block-currencies <?php echo hsc($this->params->get('moduleclass_sfx',''));?> mod_configboxcurrencies <?php echo hsc($this->params->get('moduleclass_sfx',''));?>">

	<?php if (CONFIGBOX_BLOCKTITLE_CURRENCIES) { ?><h2 class="block-title"><?php echo hsc(CONFIGBOX_BLOCKTITLE_CURRENCIES);?></h2><?php } ?>
	
	<form method="post" action="">
		<div>
			<label class="hidden-label" for="curr_id"><?php echo KText::_('Currency');?></label>
			<input class="hidden-button" type="submit" name="submits" value="<?php echo KText::_('Change');?>" />
			<?php echo $this->dropdown;?>
			<noscript>
				<div>
					<input type="submit" name="submits" value="<?php echo KText::_('Change');?>" />
				</div>
			</noscript>
			
		</div>
	</form>
	
	<?php if ($this->showConversionTable == 1) { ?>
	
	
	<div class="conversion-table">
		<?php
		foreach ($this->currencies as $currency) {
			if ($currency->base) continue;
			?>
			<div>1 <?php echo hsc($this->baseCurrency->title);?> = <?php echo cbprice($currency->exchangeRate, false); ?> <?php echo hsc($currency->title);?></div>
			<?php 
		}
		?>
	</div>
	
	<?php } ?>

</div>