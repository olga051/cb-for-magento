<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewAdmindashboard */
?>

<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>">

	<div class="left-part">

		<?php if (count($this->criticalIssues)) { ?>

			<div class="box issues critical-issues">
				<h2><?php echo KText::_('Critical Issues');?></h2>
				<ul class="issue-list">
					<?php foreach ($this->criticalIssues as $issue) { ?>
						<li class="issue-item toggle-wrapper">
							<h3 class="issue-title toggle-handle"><span></span><?php echo hsc($issue->title);?></h3>
							<div class="issue-details toggle-content">
								<h4><?php echo KText::_('Problem');?></h4>
								<div class="issue-problem"><?php echo hsc($issue->problem);?></div>
								<h4><?php echo KText::_('Solution');?></h4>
								<div class="issue-solution"><?php echo $issue->solution;?></div>
								<h4><?php echo KText::_('Who can fix it');?></h4>
								<div class="issue-access"><?php echo hsc($issue->access);?></div>
							</div>
						</li>
					<?php } ?>
				</ul>
			</div>

		<?php } ?>

		<div class="issues box">
			<h2><?php echo KText::_('Health Check');?></h2>
			<?php if (count($this->issues)) { ?>
				<ul class="issue-list">
					<?php foreach ($this->issues as $issue) { ?>
						<li class="issue-item toggle-wrapper">
							<h3 class="issue-title toggle-handle"><span></span><?php echo hsc($issue->title);?></h3>
							<div class="issue-details toggle-content">
								<h4><?php echo KText::_('Problem');?></h4>
								<div class="issue-problem"><?php echo hsc($issue->problem);?></div>
								<h4><?php echo KText::_('Solution');?></h4>
								<div class="issue-solution"><?php echo hsc($issue->solution);?></div>
								<h4><?php echo KText::_('Who can fix it');?></h4>
								<div class="issue-access"><?php echo hsc($issue->access);?></div>
							</div>
						</li>	
					<?php } ?>
				</ul>
			<?php } else { ?>
				<div class="no-issues"><span class="fa fa-check-square fa-lg"></span><?php echo KText::_('No issues found.')?></div>
			<?php } ?>
		</div>
		
		<div class="tips box">
			<h2><?php echo KText::_('Suggestions to improve performance');?></h2>
			<?php if (count($this->performanceTips)) { ?>
				<ul class="tip-list">
					<?php foreach ($this->performanceTips as $tip) { ?>
						<li class="tip-item toggle-wrapper">
							<h3 class="k-tip-title toggle-handle"><span></span><?php echo hsc($tip->title);?></h3>
							<div class="tip-details toggle-content">
								<h4><?php echo KText::_('Prospect');?></h4>
								<div class="tip-prospect"><?php echo hsc($tip->prospect);?></div>
								<h4><?php echo KText::_('Solution');?></h4>
								<div class="tip-solution"><?php echo hsc($tip->solution);?></div>
								<h4><?php echo KText::_('Who can set it up');?></h4>
								<div class="tip-access"><?php echo hsc($tip->access);?></div>
							</div>
						</li>	
					<?php } ?>
				</ul>
			<?php } else { ?>
				<div class="no-tips"><span class="fa fa-check-circle fa-lg"></span><?php echo KText::_('The automated test found you use all suggested performance tips.')?></div>
			<?php } ?>
		</div>
		
		<div class="stats box">
			<h2><?php echo KText::_('Current server status');?></h2>
			<?php if (count($this->currentStats)) { ?>
				<ul class="stat-list">
					<?php foreach ($this->currentStats as $stat) { ?>
						<li class="stat-item toggle-wrapper">
							<h3 class="stat-title toggle-handle"><span></span><?php echo KText::sprintf('%s uses %s%%', $stat->title, $stat->percentageUsed);?></h3>
							<div class="stat-values toggle-content">
								<span class="stat-used"><?php echo KText::sprintf('Using %s%s', $stat->used, $stat->unit);?></span> 
								<span class="stat-total"><?php echo KText::sprintf('of %s%s', $stat->total, $stat->unit);?></span> 
								<span class="stat-free"><?php echo KText::sprintf('- Free space %s%s (%s%%)', $stat->free, $stat->unit, $stat->percentageFree);?></span>
							</div>
						</li>	
					<?php } ?>
				</ul>
			<?php } else { ?>
				<div class="no-stats"><?php echo KText::_('There were no relevant status values found.')?></div>
			<?php } ?>
		</div>
		
		
		<div class="software-update box">
			<h2><?php echo KText::_('Software Update');?></h2>
			<p class="checking-for-update"><i class="fa fa-spinner fa-spin"></i><span class="text"><?php echo KText::_('Checking for updates');?></span></p>
			<p class="patchlevel-update-available"><span class="fa fa-exclamation-triangle"></span><?php echo KText::sprintf('A minor update to version %s was released. Currently installed version is %s.','<span class="latest-version-patchlevel"></span>',CONFIGBOX_VERSION);?> <a class="kenedo-new-tab software-update-link"><?php echo KText::_('Update');?></a></p>
			<p class="mayor-update-available"><span class="fa fa-exclamation-triangle"></span><?php echo KText::sprintf('A mayor update to version %s was released. Currently installed version is %s.','<span class="latest-version-mayor"></span>',CONFIGBOX_VERSION)?> <a class="kenedo-new-tab software-update-link"><?php echo KText::_('Update');?></a></p>
			<p class="no-update-available"><span class="fa fa-check-square fa-lg"></span><?php echo KText::sprintf('You are using the latest version of ConfigBox');?> (<?php echo CONFIGBOX_VERSION;?>).</p>
		</div>
		
		<div class="manual box">
			<h2><?php echo KText::_('ConfigBox Manual');?></h2>
			<a class="manual-link kenedo-new-tab" href="http://www.configbox.at/en/manual"><i class="fa fa-download"></i><?php echo KText::_('Download Manual');?></a>
		</div>
		
	</div>
	
	<div class="right-part">
		<div class="news">
			<h2><?php echo KText::_('News');?></h2>
			<div class="news-target"><i class="fa fa-spinner fa-spin" style="margin-right:5px"></i><?php echo KText::_('Loading');?></div>
		</div>
		<div class="overflow-gradient"></div>
	</div>
	
	<div class="clear"></div>
</div>
