<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxOption {

	public $id;
	public $sku;
	public $price;
	public $price_recurring;
	public $weight;
	public $checked_out;
	public $checked_out_time;
	public $option_custom_1;
	public $option_custom_2;
	public $option_custom_3;
	public $option_custom_4;
	public $external_reviews_id;
	public $option_image;
	public $available;
	public $availibility_date;
	public $was_price;
	public $was_price_recurring;
	public $disable_non_available;
	public $element_id;
	public $option_id;
	public $dependencies;
	public $default;
	public $visualization_image;
	public $visualization_stacking;
	public $visualization_view;
	public $hidenonapplying;
	public $confirm_deselect;
	public $calcmodel;
	public $calcmodel_recurring;
	public $ordering;
	public $published;
	public $assignment_custom_1;
	public $assignment_custom_2;
	public $assignment_custom_3;
	public $assignment_custom_4;
	public $rules;
	public $option_picker_image;
	public $calcmodel_weight;

	public $applies;
	public $cssClasses;
	public $extraAttributes;
	public $checked;
	/** @var  string $disabled Careful, this is actually the whole html attribute disabled */
	public $disabled;
	public $controlClasses;

	// Do not expect these to be all option data, this list is not kept up to date
	public $title;
	public $description;

	public $pickerPreloadCssClasses;
	public $pickerImageSrc;
	public $pickerPreloadAttributes;
	public $isSelected;

	/**
	 * @var int $enable_reviews Setting in GUI.
	 */
	public $enable_reviews;

	/**
	 * @var boolean $showReviews Final decision on showing reviews. Depends on backend global settings and option's
	 * setting (see above).
	 */
	public $showReviews;

	/**
	 * @var string $optionImageSrc Full URL to the option image (don't confuse it with visualization image or picker
	 * image, see GUI on what the option image is). Set in ConfigboxViewConfiguratorpage::display()
	 */
	public $optionImageSrc;
	/**
	 * @var string $optionImagePopupContent HTML for the popup. Set in ConfigboxViewConfiguratorpage::display()
	 */
	public $optionImagePopupContent;
	/**
	 * @var int $optionImagePopupWidth - Width of the popup
	 */
	public $optionImagePopupWidth;


	function __construct(&$data) {
		foreach ($data as $k => $v) {
			$this->$k = $v;
		}
	}
	
	function getPrice($getNet = NULL, $inBaseCurrency = false) {
		return ConfigboxPrices::getXrefPrice($this->id, $this->element_id, $getNet, $inBaseCurrency);
	}
	
	function getPriceRecurring($getNet = NULL, $inBaseCurrency = false) {
		return ConfigboxPrices::getXrefPriceRecurring($this->id, $this->element_id, $getNet, $inBaseCurrency);
	}
	
	function getWasPrice($getNet = NULL, $inBaseCurrency = false) {
		return ConfigboxPrices::getXrefWasPrice($this->id, $this->element_id, $getNet, $inBaseCurrency);
	}
	
	function getWasPriceRecurring($getNet = NULL, $inBaseCurrency = false) {
		return ConfigboxPrices::getXrefWasPriceRecurring($this->id, $this->element_id, $getNet, $inBaseCurrency);
	}
	
	function applies() {
		
		$this->applies = ConfigboxRulesHelper::ruleIsFollowed($this->rules,'option_assignment',$this->id);
		
		return $this->applies;
		
	}
	
	function getWeight() {
		if ($this->calcmodel_weight) {
			return ConfigboxPrices::getXrefWeight($this->id);
		}
		else {
			return $this->weight;
		}
	}
	
	function addCssClass($class) {
		$class = str_replace('  ', ' ', $class);
		$classes = explode(' ',$class);
		foreach ($classes as $class) {
			$this->cssClasses[$class] = hsc($class);
		}
	
	}
	
	function removeCssClass($class) {
		if (isset($this->cssClasses[$class])) {
			unset($this->cssClasses[$class]);
		}
	}
	
	function getCssClasses() {
		return implode(' ',$this->cssClasses);
	}
	
}