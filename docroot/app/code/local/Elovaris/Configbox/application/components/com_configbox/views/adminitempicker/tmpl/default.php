<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewAdminlicense */
?>
<div class="view-item-picker">
	<ul class="product-list">
		<?php foreach($this->tree as $product) { ?>
			<li class="product-item product-<?php echo intval($product['id']);?>" data-type="product" data-id="<?php echo intval($product['id']);?>">
				<span class="sub-list-trigger product-title<?php echo (count($product['pages'])) ? ' has-sub-list':' no-sub-list';?><?php echo (in_array($product['id'],$this->openIds['products'])) ? ' trigger-opened':'';?>"></span>
				<a class="picker-link"><?php echo hsc($product['title']);?></a>
				<ul class="sub-list page-list<?php echo (in_array($product['id'],$this->openIds['products'])) ? ' list-opened':'';?>">
					<?php foreach ($product['pages'] as $page) { ?>
						<li class="page-item page-<?php echo intval($page['id']);?>"  data-type="page" data-id="<?php echo intval($page['id']);?>">
							<span class="sub-list-trigger configurator-page-title<?php echo (count($page['elements'])) ? ' has-sub-list':' no-sub-list';?><?php echo (in_array($page['id'],$this->openIds['pages'])) ? ' trigger-opened':'';?>"></span>
							<a class="picker-link"><?php echo hsc($page['title'])?></a>
							<ul class="sub-list element-list<?php echo (in_array($page['id'],$this->openIds['pages'])) ? ' list-opened':'';?>">
								<?php foreach ($page['elements'] as $element) { ?>
									<li class="element-item element-<?php echo intval($element['id']);?>"  data-type="element" data-id="<?php echo intval($element['id']);?>">
										<span class="sub-list-trigger element-title<?php echo (count($element['options'])) ? ' has-sub-list':' no-sub-list';?><?php echo (in_array($element['id'],$this->openIds['elements'])) ? ' trigger-opened':'';?>"></span>
										<a class="picker-link"><?php echo (CONFIGBOX_USE_INTERNAL_ELEMENT_NAMES) ? hsc($element['internal_name']) : hsc($element['title']);?></a>
										<ul class="sub-list option-list<?php echo (in_array($element['id'],$this->openIds['elements'])) ? ' list-opened':'';?>">
											<?php foreach ($element['options'] as $option) { ?>
												<li class="option-item option-<?php echo intval($option['id']);?>" data-type="xref" data-id="<?php echo intval($option['id']);?>">
													<a class="picker-link"><?php echo hsc($option['title']);?></a>
												</li>
											<?php } ?>
										</ul>
									</li>
								<?php } ?>
							</ul>
						</li>
					<?php } ?>
				</ul>
			</li>
		<?php } ?>
	</ul>
</div>