<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerAdminoptionassignments extends KenedoController {

	/**
	 * @return ConfigboxModelAdminoptionassignments
	 */
	protected function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdminoptionassignments');
	}

	/**
	 * @return ConfigboxViewAdminoptionassignments
	 */
	protected function getDefaultView() {
		return $this->getDefaultViewList();
	}

	/**
	 * @return ConfigboxViewAdminoptionassignments
	 */
	protected function getDefaultViewList() {
		return KenedoView::getView('ConfigboxViewAdminoptionassignments');
	}

	/**
	 * @return ConfigboxViewAdminoptionassignment
	 */
	protected function getDefaultViewForm() {
		return KenedoView::getView('ConfigboxViewAdminoptionassignment');
	}

}
