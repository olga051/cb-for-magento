<?php
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this KenedoPropertyMultiselect
 */

$db = KenedoPlatform::getDb();

if ($this->getPropertyDefinition('modelClass')) {
	$selectModel = KenedoModel::getModel($this->getPropertyDefinition('modelClass'));
	$method = $this->getPropertyDefinition('modelMethod');
	$selectData = $selectModel->$method();
}
else {
//	$db = KenedoPlatform::getDb();
//	$query = "SELECT `".$this->getPropertyDefinition('selectOptionId')."` AS id,`".$this->getPropertyDefinition('selectOptionTitle')."` AS `title` FROM `".$this->getPropertyDefinition('selectTable')."`";
//	$db->setQuery($query);
//	$selectData = $db->loadObjectList();

	$query = "
	SELECT `".$this->getPropertyDefinition('keyOther')."` AS id, `".$this->getPropertyDefinition('display_other')."` AS `title`
	FROM `".$this->getPropertyDefinition('tableOther')."`";
	$db->setQuery($query);
	$selectData = $db->loadObjectList();

}

$query = "
SELECT `".$this->getPropertyDefinition('fkOther')."`
FROM `".$this->getPropertyDefinition('xrefTable')."`";

if ($this->getPropertyDefinition('fkOwn')) {
	$query .= "WHERE `".$this->getPropertyDefinition('fkOwn')."` = ".intval($this->data->{$this->getPropertyDefinition('keyOwn')});
}

$db->setQuery($query);
$savedValues = $db->loadResultList();

$options = array();
if (is_array($selectData) && count($selectData) > 0) {
	foreach ($selectData as $selectOption) {
		$options[$selectOption->{$this->getPropertyDefinition('keyOther')}] = $selectOption->{$this->getPropertyDefinition('displayOther')};
	}
}

if ($this->getPropertyDefinition('asCheckboxes')) {
	echo KenedoHtml::getCheckboxField($this->propertyName, $options, $savedValues, NULL, true, '');
}
else {
	echo KenedoHtml::getSelectField($this->propertyName, $options, $savedValues, NULL, true, 'extended-multiselect');
}
?>

<?php if ($this->getPropertyDefinition('asCheckboxes')) { ?>
	<span class="multiselect-toggles">
		<span onclick="Kenedo.selectAllOptions('<?php echo $this->propertyName;?>')"><?php echo KText::_('Select All');?></span>
		<span onclick="Kenedo.deselectAllOptions('<?php echo $this->propertyName;?>')"><?php echo KText::_('Deselect All');?></span>
	</span>
<?php } ?>