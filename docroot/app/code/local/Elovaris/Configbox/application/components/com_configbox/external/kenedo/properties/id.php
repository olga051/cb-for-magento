<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyId extends KenedoProperty {
	
	function renderListingField($item, $items) {				
		?>
		<input type="checkbox" name="cid[]" class="kenedo-item-checkbox" value="<?php echo intval($item->id);?>" />
		<span><?php echo intval($item->id); ?></span>
		<?php
	}
	
	function isInListing() {
		return true;	
	}
	
	function usesWrapper() {
		return false;
	}
	
	function getBodyAdmin() {
		return '';
	}

}