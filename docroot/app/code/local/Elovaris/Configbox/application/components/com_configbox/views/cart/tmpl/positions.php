<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCart */
?>

<div class="cart-positions">

	<?php foreach ($this->cart->positions as $positionId=>$position) { ?>

		<div class="cart-position-item cart-position-item-id-<?php echo intval($position->productData->id);?>">

			<div class="cart-position-product-image">
				<?php echo $this->positionImages[$positionId]; ?>
			</div>

			<h3 class="cart-position-product-title">
				<?php echo intval($position->quantity);?> x <?php echo hsc($position->productTitle);?>
			</h3>

			<table class="cart-position-configuration cart-position-configuration-product-<?php echo intval($position->productData->id);?>">

				<?php if ($position->usesRecurring) { ?>
					<tr>
						<th class="element-list-item"><?php echo KText::_('Your Selections');?></th>
						<?php if ($this->displayPricing) { ?>

							<?php if ($position->usesRecurring) { ?>
								<th class="element-list-price-recurring"><?php echo hsc($position->productData->priceLabelRecurring);?></th>
							<?php } ?>

							<th class="element-list-price"><?php echo hsc($position->productData->priceLabel);?></th>

						<?php } ?>
					</tr>
				<?php } ?>

				<?php if ($this->displayPricing) { ?>
					<?php if ($position->productBasePriceNet or $position->productBasePriceRecurringNet) { ?>
						<tr>
							<td class="element-list-item"><?php echo (count($position->elements)) ? KText::_('Base Price') : KText::_('Price');?></td>
							<?php if ($position->totalUnreducedRecurringNet) { ?>
								<td class="element-list-price-recurring">
									<?php echo cbprice( ($this->isB2b) ? $position->productBasePriceRecurringNet : $position->productBasePriceRecurringGross, true, true); ?>
								</td>
							<?php } ?>
							<td class="element-list-price">
								<?php echo cbprice( ($this->isB2b) ? $position->productBasePriceNet : $position->productBasePriceGross, true, true); ?>
							</td>

						</tr>
					<?php } ?>
				<?php } ?>

				<?php foreach ($position->elements as $element) { ?>

					<?php if ( $element->displaySettings->showInCart == false ) continue; ?>

					<tr>
						<td class="element-list-item">
							<span class="element-title"><?php echo hsc($element->title);?><span class="element-option-seperator">:</span></span>
							<span class="element-output-value"><?php echo $element->selection->outputValue;?></span>
						</td>

						<?php if ($this->displayPricing) { ?>

							<?php if ($position->usesRecurring) { ?>
								<td class="element-list-price-recurring">
									<?php echo cbprice( ($this->isB2b) ? $element->selection->priceRecurringNet : $element->selection->priceRecurringGross, true, true); ?>
								</td>
							<?php } ?>

							<td class="element-list-price">
								<?php echo cbprice( ($this->isB2b) ? $element->selection->priceNet : $element->selection->priceGross, true, true); ?>
							</td>

						<?php } ?>
					</tr>

				<?php } ?>


				<?php if ($this->displayPricing && $position->quantity > 1) { ?>

					<tr class="element-list-total-unit-price">
						<td class="element-list-item"><?php echo KText::_('Price per item');?></td>

						<?php if ($position->usesRecurring) { ?>
							<td class="element-list-price-recurring">
								<?php echo cbprice(  ( ($this->isB2b) ? $position->totalUnreducedRecurringNet : $position->totalUnreducedRecurringGross ) / $position->quantity, true, true);?>
							</td>
						<?php } ?>

						<td class="element-list-price"><?php echo cbprice( ( ($this->isB2b) ? $position->totalUnreducedNet : $position->totalUnreducedGross ) / $position->quantity);?></td>
					</tr>

				<?php } ?>


				<?php if ($this->displayPricing && count($position->elements) != 0) { ?>

					<tr class="element-list-total">
						<td class="element-list-item"><?php echo KText::_('Total');?></td>

						<?php if ($position->usesRecurring) { ?>
							<td class="element-list-price-recurring">
								<?php echo cbprice( ($this->isB2b) ? $position->totalUnreducedRecurringNet : $position->totalUnreducedRecurringGross, true, true );?>
							</td>
						<?php } ?>

						<td class="element-list-price"><?php echo cbprice( ($this->isB2b) ? $position->totalUnreducedNet : $position->totalUnreducedGross );?></td>
					</tr>

				<?php } ?>

			</table>

			<div class="clear"></div>

			<?php
			// These are the buttons and drop-downs for editing, removing etc.
			$this->position = $position;
			$this->renderView('positioncontrols');
			?>

			<div class="clear"></div>
		</div>

	<?php } ?>

</div>
