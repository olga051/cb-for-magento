<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCart */
?>
<div class="cart-buttons">

	<a rel="nofollow" class="btn button-continue-shopping" href="<?php echo $this->urlContinueShopping;?>"><?php echo KText::_('Continue Shopping')?></a>

	<?php if ($this->canSaveOrder) { ?>
		<a rel="nofollow" class="btn button-save-order" href="<?php echo $this->urlSaveOrder;?>"><?php echo KText::_('Save Cart')?></a>
	<?php } ?>

	<?php if ($this->canRequestQuote) { ?>
		<a rel="nofollow" class="btn button-get-quotation" href="<?php echo $this->urlGetQuotation;?>"><?php echo KText::_('Request Quote')?></a>
	<?php } ?>

	<?php if ($this->canCheckout) { ?>
		<a rel="nofollow" class="btn btn-primary button-checkout" href="<?php echo $this->urlCheckout;?>"><?php echo KText::_('Checkout')?></a>
	<?php } ?>

	<div class="clear"></div>
</div>