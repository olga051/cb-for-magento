<?php 
class KenedoViewHelper {
	
	protected static $canSort = array();
	public static $messages = array();

	/**
	 * @param KenedoProperty[] $properties
	 * @return null
	 */
	static function getGroupingKey($properties) {
		$groupKey = NULL;
		foreach ($properties as $property) {
			if ($property->getPropertyDefinition('type') == 'ordering') {
				$groupKey = $property->getPropertyDefinition('group');
				break;
			}
		}
		return $groupKey;
	}

	/**
	 * @param object[] $records
	 * @param KenedoProperty[] $properties
	 * @param string[] $orderingInfo
	 * @return bool
	 */
	static function canSortItems($records, $properties, $orderingInfo) {

		foreach ($properties as $property) {
			if ($property->getPropertyDefinition('type') == 'ordering') {

				if ($property->getPropertyDefinition('disableSortable', false)) {
					return false;
				}

				if ($property->propertyName == $orderingInfo['propertyName']) {
					return true;
				}
				else {
					return false;
				}

			}
		}

		return false;

	}
	
	static function clearMessages() {
		self::$messages = array();
	}
	
	static function addMessage($text, $type = 'notice') {
		self::$messages[$type][] = $text;
	}
	
	static function getMessages($type = NULL) {
		
		if (!$type) {
			return self::$messages;
		}
		elseif(isset(self::$messages[$type])) {
			return self::$messages[$type];
		}
		else {
			return array();
		}
	}
	
	static function getState($path, $default = NULL) {
		return KSession::get('KenedoView.'.$path,$default);
	}
	
	static function setState($path, $value) {
		KSession::set('KenedoView.'.$path,$value);
	}

	static function unsetState($path) {
		KSession::delete('KenedoView.'.$path);
	}
	
	static function getUpdatedState($path, $requestKey, $defaultValue = NULL, $sanitationType = 'int') {
		$requestState = KRequest::getVar($requestKey, null, 'METHOD', $sanitationType);
		if ($requestState !== NULL) {
			self::setState($path, $requestState);
		}
		
		$state = self::getState($path, $defaultValue);
		return $state;
		
	}
	
	// Legacy method, remove in 2.6 or 3.0
	static function getTabItems() {
		return array();
	}
	
	static function renderTabItems($tabItems) {
		
		if (!$tabItems) {
			return '';
		}
		$currentViewName = KRequest::getKeyword('view');
		ob_start();
		?>
		<div class="kenedo-tabs">
			<ul class="kenedo-tab-list">
				<?php foreach ($tabItems as $viewName=>$tab) {
					
					$isActive = ($viewName == $currentViewName or (isset($tab['activeOn']) && in_array($currentViewName,$tab['activeOn'])) );
					
					if ($isActive && isset($tab['subviews'])) {
						$subviews = $tab['subviews'];
					}
					?>
					<li<?php echo ($isActive) ? ' class="active"' : '';?>><a<?php echo (!empty($tab['tooltip'])) ? ' title="'.hsc($tab['tooltip']).'" ':' ';?>href="<?php echo $tab['link'];?>"><?php echo hsc($tab['title']);?></a></li>
					
				<?php } ?>
			</ul>
			<div class="clear"></div>
			
		</div>
		
		<?php if (isset($subviews)) { ?>
			<div class="kenedo-tabs">
				<ul class="kenedo-tab-list">
					<?php foreach ($subviews as $viewName=>$tab) {
						$currentViewName = KRequest::getKeyword('view');
						
						$isActive = ($viewName == $currentViewName or (isset($tab['activeOn']) && in_array($currentViewName,$tab['activeOn'])) );
						
						?>
						<li><a href="<?php echo $tab['link'];?>" <?php echo ($isActive) ? 'class="active"' : '';?>><?php echo hsc($tab['title']);?></a></li>
					<?php } ?>
				</ul>
				<div class="clear"></div>
			</div>
		<?php } ?>
			
		<?php 
		$tabs = ob_get_clean();

		return $tabs;
	}
	
	static function renderTaskItems($taskItems) {
		
		if (!$taskItems) {
			return '';
		}
		ob_start();
		?>
		<div class="kenedo-tasks">
			<ul class="kenedo-task-list">
				<?php foreach ($taskItems as $task) { ?>
					<?php if (!empty($task['link'])) { ?>
						<li class="link"><a href="<?php echo $task['link'];?>" class="backend-button-small kenedo-modal"><?php echo hsc($task['title']);?></a></li>
					<?php } else { ?>
						<li class="task task-<?php echo hsc($task['task'])?><?php echo (!empty($task['non-ajax'])) ? ' non-ajax':'';?>"><a class="backend-button-small"><?php echo hsc($task['title']);?></a></li>
					<?php } ?>
				<?php } ?>
			</ul>
			<div class="clear"></div>
		</div>
		<?php 
		$tabs = ob_get_clean();
		
		return $tabs;
	}
	
	static function getListingPagination ($itemCount, $listingInfo) {
		
		if ($listingInfo['limit']) {
			$numPages = ceil( $itemCount / $listingInfo['limit'] );
		}
		else {
			$numPages = 1;
		}
		
		$pagination = '';
		
		if ($itemCount > 5) {
		
			$items = array(5,10,15,20,25,30,50,100,0);
			$selected = $listingInfo['limit'];
			
			ob_start();
			?>
			<div class="kenedo-limit">
				<label><?php echo KText::_('Items per page');?></label>
				<select class="kenedo-limit-select">
					<?php foreach ($items as $item) { ?>
						<option value="<?php echo (int)$item;?>" <?php echo ($item == $selected) ? 'selected="selected"' : '';?>><?php echo ($item == 0) ? KText::_('All') : (int)$item;?></option>
					<?php } ?>
				</select>
			</div>
			<?php
			$pagination = ob_get_clean();
		}
		
		$pageLinks = array();
		for ($i = 0; $i < $numPages; $i++) {
			$pageLinks[] = array('start'=>$i * $listingInfo['limit'], 'page'=>$i + 1);
		}
		
		if (count($pageLinks) > 1) {
			ob_start();
			?>
			<div class="kenedo-pagination-list">
				<div class="pagination-label"><?php echo KText::_('Go to page');?></div>
				<ul>
					<?php foreach ($pageLinks as $pageLink) { ?>
						<li <?php echo ($pageLink['start'] == $listingInfo['start']) ? 'class="active"':'';?>><a class="page-start-<?php echo (int)$pageLink['start'];?>"><?php echo (int)$pageLink['page'];?></a></li>
					<?php } ?>
				</ul>
			</div>
			<?php
			$pagination .= ob_get_clean();
		}
		
		return $pagination;
		
		
	}

	static function loadKenedoAssets() {

		$extensions = array('com_configbox', 'com_swatches');
		$extensionLoaded = in_array(KRequest::getKeyword('option', ''), $extensions);
		$loadAnyways = KRequest::getInt('load_any');
		$isMagento = KenedoPlatform::getName() == 'magento';

		if ($isMagento || KenedoPlatform::p()->isAdminArea() || $extensionLoaded || $loadAnyways) {

			if (KenedoPlatform::p()->getDebug()) {
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jquery/jquery-1.11.1.min.js' );
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jquery.colorbox/jquery.colorbox-1.6.3.src.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jqueryui/jquery-ui-1.10.3.min.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jqueryui.touch-punch/jquery.ui.touch-punch-0.2.3.src.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jquery.chosen/jquery.chosen-1.1.0.src.js');
			} else {
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jquery/jquery-1.11.1.min.js' );
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jquery.colorbox/jquery.colorbox-1.6.3.min.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jqueryui/jquery-ui-1.10.3.min.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jqueryui.touch-punch/jquery.ui.touch-punch-0.2.3.min.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jquery.chosen/jquery.chosen-1.1.0.min.js');
			}

			if ($isMagento || strpos(KRequest::getKeyword('controller'), 'admin') === 0) {

				if (KenedoPlatform::p()->getDebug()) {
					KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jquery.address/jquery.address-master.src.js');
				} else {
					KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/jquery.address/jquery.address-master.min.js');
				}

				KenedoPlatform::p()->addStyleSheet(KPATH_URL_ASSETS.'/kenedo/external/codemirror/lib/codemirror.css');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/codemirror/lib/codemirror.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/codemirror/mode/javascript/javascript.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/codemirror/mode/xml/xml.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/codemirror/mode/css/css.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/codemirror/mode/clike/clike.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/codemirror/mode/php/php.js');
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/codemirror/mode/htmlmixed/htmlmixed.js');

			}

			if (KenedoPlatform::p()->getDebug()) {
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/assets/javascript/kenedo.js?version='.CONFIGBOX_VERSION);
			}
			else {
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/assets/javascript/kenedo.min.js?version='.CONFIGBOX_VERSION);
			}

			KenedoPlatform::p()->addStylesheet(KPATH_URL_ASSETS.'/kenedo/external/jqueryui/ui-lightness/jquery-ui-1.10.3.custom.min.css');
			KenedoPlatform::p()->addStyleSheet(KPATH_URL_ASSETS.'/kenedo/assets/css/kenedo.css?version='.CONFIGBOX_VERSION);

			if ($isMagento || strpos(KRequest::getKeyword('controller'), 'admin') === 0) {
				KenedoPlatform::p()->addScript(KPATH_URL_ASSETS.'/kenedo/external/tinymce/tinymce.min.js?version='.CONFIGBOX_VERSION);
			}

			// Load IE stylesheet
			if (!empty($_SERVER['HTTP_USER_AGENT'])) {

				$agent = $_SERVER['HTTP_USER_AGENT'];

				if (strstr($agent,'MSIE 7') && strstr($agent,'Opera') == '') {
					KenedoPlatform::p()->addStyleSheet( KPATH_URL_ASSETS.'/kenedo/assets/css/ie7.css');
				}
				if (strstr($agent,'MSIE 8') && strstr($agent,'Opera') == '') {
					KenedoPlatform::p()->addStyleSheet( KPATH_URL_ASSETS.'/kenedo/assets/css/ie8.css');
				}
				if (strstr($agent,'MSIE 9') && strstr($agent,'Opera') == '') {
					KenedoPlatform::p()->addStyleSheet( KPATH_URL_ASSETS.'/kenedo/assets/css/ie9.css');
				}
				if (strstr($agent,'MSIE 10') && strstr($agent,'Opera') == '') {
					KenedoPlatform::p()->addStyleSheet( KPATH_URL_ASSETS.'/kenedo/assets/css/ie10.css');
				}

			}

		}

	}
	
}