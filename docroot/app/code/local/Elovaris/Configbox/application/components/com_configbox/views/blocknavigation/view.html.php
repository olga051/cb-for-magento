<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewBlocknavigation extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * Comes from ConfigboxModelProduct::getPages() and is a bit modified (CSS classes added)
	 * @var object[]
	 * @see ConfigboxModelProduct::getPages()
	 */
	public $pages;

	/**
	 * @var boolean $showAsTabs As per global or product settings tells if nav should display as tabs
	 */
	public $showAsTabs;

	/**
	 * @var KStorage $params Joomla module parameters
	 */
	public $params;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {
		
		if (empty($this->params)) {
			$params = new KStorage();
			$this->assignRef('params', $params);
		}
		
		$model = KenedoModel::getModel('ConfigboxModelProduct');
		$productId = KRequest::getInt('prod_id');
		$pageId = KRequest::getInt('page_id');

		$pages = $model->getPages($productId);
		$this->assignRef('pages', $pages);

		if (count($pages) > 1) {
			
			foreach ($pages as $page) {
				$page->listItemClasses = 'page page-'.$page->id . ( ($page->id == $pageId ) ? ' active current':'');
				$page->linkClasses = 'onAjaxCompleteOnly';

				if (KenedoPlatform::getName() == 'magento') {
					$currentUrl = Mage::helper('core/url')->getCurrentUrl();
					$currentUrl = preg_replace('/page_id=(\d+)/i', '', $currentUrl);
					$currentUrl = rtrim($currentUrl,'&?');
					$page->url = $currentUrl . ( (strstr($currentUrl, '?')) ? '&':'?') . 'page_id='.$page->id;
				}
				else {
					$page->url = KLink::getRoute('index.php?view=configuratorpage&prod_id='.intval($page->product_id).'&page_id='.intval($page->id));
				}
			}

			$product = $model->getProduct($productId);
			$this->assignRef('product', $product);
			
			// Set some variables
			$this->assign('currentPageId', 	$pageId);
			$this->assign('showAsTabs', 	($product->show_nav_as_tabs == 1 or ($product->show_nav_as_tabs == 2 && CONFIGBOX_SHOW_NAV_AS_TABS) ) );

		}
		
	}

}
