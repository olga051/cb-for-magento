<?php
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyMultiselect extends KenedoProperty {

	function getDataFromRequest( &$data) {
		// Will get dropped during store
		$data->{$this->propertyName} = KRequest::getArray($this->propertyName);
	}

	function store(&$data) {

		// Don't act on ajaxStore, multiselects don't add the xrefs during KenedoModel::getRecord
		if (KRequest::getKeyword('task','') == 'ajaxStore') {
			return true;
		}

		// Get the xref values for convenience
		$fieldValues = $data->{$this->propertyName};

		// Remove the xrefs from the data object, they won't get stored by the model's method
		unset($data->{$this->propertyName});

		$db = KenedoPlatform::getDb();

		// No fkOwn means that we don't have a true xref table, just a table that stores a bunch of values just for that record
		// See #__configbox_active_languages as example.
		// Simple delete all and insert is enough.
		if ($this->getPropertyDefinition('fkOwn', '') == '') {
			$db = KenedoPlatform::getDb();
			$query = "DELETE FROM `".$this->getPropertyDefinition('xrefTable')."`";
			$db->setQuery($query);
			$db->query();

			foreach ($fieldValues as $fieldValue) {
				$query = "INSERT INTO `".$this->getPropertyDefinition('xrefTable')."` SET `".$this->getPropertyDefinition('fkOther')."` = '".$db->getEscaped($fieldValue)."'";
				$db->setQuery($query);
				$db->query();
			}
			return true;
		}

		// Make variables with relevant field data for simplicity
		$xrefTable = $this->getPropertyDefinition('xrefTable');
		$fkOwn = $this->getPropertyDefinition('fkOwn');
		$fkOther = $this->getPropertyDefinition('fkOther');
		$keyOwn = $this->getPropertyDefinition('keyOwn');

		// We can't just drop all regarding xref records, since some contain additional data like ordering, which
		// would get lost. So we load the existing ones and put them in $newXrefRecords if they are to stay. Later
		// we delete all regarding ones and insert those records.

		// Get the existing xref records
		$query = "SELECT * FROM `".$xrefTable."` WHERE `".$fkOwn."` = ".intval($data->$keyOwn);
		$db->setQuery($query);
		$existingXrefRecords = $db->loadObjectList($fkOther);

		// Prepare an array with new xref records (bound to replace the existing ones)
		$newXrefRecords = array();
		foreach ($fieldValues as $fieldValue) {

			// If an xref record for that value exists, use it's existing data
			if (!empty($existingXrefRecords[$fieldValue])) {
				$newXrefRecord = $existingXrefRecords[$fieldValue];
			}
			// Otherwise create a new one (add the ordering field in case the xref table uses ordering)
			else {
				$newXrefRecord = new stdClass();
				$newXrefRecord->$fkOwn = intval($data->$keyOwn);
				$newXrefRecord->$fkOther = $fieldValue;

				if (!empty($this->propertyDefinition['usesOrdering'])) {
					$newXrefRecord->ordering = 0;
				}

			}

			$newXrefRecords[] = $newXrefRecord;
			unset($newXrefRecord);

		}

		// Remove all existing xref records
		$query = "DELETE FROM `".$xrefTable."` WHERE `".$fkOwn."` = ".intval($data->$keyOwn);
		$db->setQuery($query);
		$db->query();

		// Add the new set of xref records
		foreach ($newXrefRecords as $newXrefRecord) {
			$db->insertObject($xrefTable, $newXrefRecord);
		}

		// Set sorting numbers for all xref records that are involved in the second table values
		if (!empty($this->propertyDefinition['usesOrdering'])) {

			foreach ($fieldValues as $fieldValue) {
				$query = "SELECT MAX(ordering) AS `highest_ordering` FROM `".$xrefTable."` WHERE `".$fkOther."` = ".intval($fieldValue);
				$db->setQuery($query);
				$currentHighestOrderNumber = $db->loadResult();

				$query = "SELECT * FROM `".$xrefTable."` WHERE `ordering` = 0 AND `".$fkOther."` = ".intval($fieldValue);
				$db->setQuery($query);
				$xrefsToUpdate = $db->loadObjectList();

				foreach ($xrefsToUpdate as $xrefToUpdate) {
					$currentHighestOrderNumber++;
					$query = "UPDATE `".$xrefTable."` SET `ordering` = ".intvaL($currentHighestOrderNumber)." WHERE `".$fkOther."` = ".intval($fieldValue)." AND `".$fkOwn."` = ".intval($xrefToUpdate->$fkOwn);
					$db->setQuery($query);
					$db->query();
				}

			}
		}

		return true;

	}

	function check($data) {
		return true;
	}

	public function getSelectsForGetRecord($selectAliasPrefix = '', $selectAliasOverride = '') {
		return array();
	}

	public function getJoinsForGetRecord() {

		$joins = array();

		if ($this->getPropertyDefinition('filter') && $this->getPropertyDefinition('fkOwn')) {
			$joins[$this->getTableAlias()] = "
		LEFT JOIN `".$this->getPropertyDefinition('xrefTable')."` AS ".$this->getTableAlias()."
		ON ".$this->getTableAlias().".".$this->getPropertyDefinition('fkOwn')." = `".$this->model->getModelName()."`.`".$this->model->getTableKey()."`
		";
		}

		return $joins;
	}

	public function getTableAlias() {
		$parentModelClass = $this->getPropertyDefinition('modelClass');
		$joinedModel = KenedoModel::getModel($parentModelClass);
		return 'xref_'.$this->model->getModelName().'_'.$joinedModel->getModelName();
	}

	public function getTableColumnName() {
		return $this->getPropertyDefinition('fkOther');
	}

	public function getFilterName() {

		if ($this->getPropertyDefinition('filter', false) == false && $this->getPropertyDefinition('search', false) == false) {
			return '';
		}

		return $this->getTableAlias().'.'.$this->getTableColumnName();

	}

	function getPossibleFilterValues() {

		if ($this->getPropertyDefinition('modelClass')) {
			$selectModel = KenedoModel::getModel($this->getPropertyDefinition('modelClass'));
			$method = $this->getPropertyDefinition('modelMethod');
			$selectData = $selectModel->$method();
		}
		else {
			$db = KenedoPlatform::getDb();
			$query = "
			SELECT
				`".$this->getPropertyDefinition('keyOther')."` AS `id`,
				`".$this->getPropertyDefinition('displayOther')."` AS `title`
			FROM `".$this->getPropertyDefinition('tableOther')."`
			";
			$db->setQuery($query);
			$selectData = $db->loadObjectList();
		}

		$options = array();
		$options['all'] = KText::sprintf('No %s filter', $this->getPropertyDefinition('label'));

		if (is_array($selectData) && count($selectData) > 0) {
			foreach ($selectData as $selectOption) {
				$options[$selectOption->{$this->getPropertyDefinition('keyOther')}] = $selectOption->{$this->getPropertyDefinition('displayOther')};
			}
		}

		return $options;
	}

}