<?php
class KText {

	protected static $strings = array();
	
	public static $languageId = 0;
	/**
	 * @var string Form is de-DE, en-GB etc
	 */
	public static $languageTag = NULL;
	/**
	 * @var string Comes from the language files, falls back on .
	 */
	public static $decimalSymbol = NULL;
	/**
	 * @var string Comes from the language files, falls back on ,
	 */
	public static $thousandsSeparator = NULL;
	/**
	 * @var string Form is de, en etc
	 */
	public static $languageCode = NULL;
	/**
	 * @var string Form is DE, AT, GB etc
	 */
	public static $countryCode = NULL;

	/**
	 * @param string|NULL $languageTag Language tag (format example de-DE)
	 */
	static function setLanguage($languageTag = NULL) {
		
		if ($languageTag == NULL) {
			$languageTag = KenedoPlatform::p()->getLanguageTag();
		}

		self::$languageTag = $languageTag;
		
		$exp = explode('-',self::$languageTag);
		
		self::$decimalSymbol 		= self::_('DECIMAL_MARK','.');
		self::$thousandsSeparator 	= self::_('DIGIT_GROUPING_SYMBOL','.');
		self::$languageCode			= $exp[0];
		self::$countryCode			= $exp[1];
		
	}

	/**
	 * @param string $path Full path to the language ini file
	 * @param string|null $languageTag Language tag (or NULL to use currently set language)
	 * @return bool
	 */
	static function load($path, $languageTag = NULL) {
		
		if (is_file($path) == false) {
			KLog::log('Could not find language file in "'.$path.'".','debug');
			return false;
		}
		
		$strings = parse_ini_file($path);
		
		if ($strings === false) {
			KLog::log('Language file in "'.$path.'" could not be processed. Please check if the syntax of the file is correct', 'error');
			return false;
		}
		
		if ($languageTag == NULL) {
			$languageTag = self::$languageTag;
		}
		
		foreach ($strings as $key=>$string) {
			self::$strings[$languageTag][$key] = $string;
		}

		return true;
		
	}

	/**
	 * @param string $key Key of translation (can be in any case)
	 * @param string|null $fallback Fallback text (in case translation is not found)
	 * @return string Localized text or fallback text
	 */
	static function _($key, $fallback = NULL) {

		if ($key == 'CONFIGBOX_DATEFORMAT_PHP_DATE') {
			$key = 'KENEDO_DATEFORMAT_PHP_DATE';
		}
		if ($key == 'CB_STATE') {
			$key = 'STATE';
		}

		$key = str_replace('(','__POPEN__',$key);
		$key = str_replace(')','__PCLOSE__',$key);

		$keyWithoutColon = rtrim($key, ':');
		$hasTrailingColon = (strrpos($key, ':') === mb_strlen($key) -1);

		if (isset(self::$strings[self::$languageTag][strtoupper($key)])) {
			return self::$strings[self::$languageTag][strtoupper($key)];
		}
		elseif ($hasTrailingColon && isset(self::$strings[self::$languageTag][strtoupper($keyWithoutColon)])) {
			return rtrim(self::$strings[self::$languageTag][strtoupper($keyWithoutColon)], ':').':';
		}
		elseif ($fallback !== NULL && is_string($fallback)) {
			return $fallback;
		}
		else {
			return $key;
		}

	}
	
	static function sprintf($key) {
		
		$funcArgs = func_get_args();
		
		if (count($funcArgs) > 0) {
			$funcArgs[0] = self::_($key);
			return call_user_func_array('sprintf', $funcArgs);
		}
		else {
			return self::_($key);
		}
		
	}
	
}