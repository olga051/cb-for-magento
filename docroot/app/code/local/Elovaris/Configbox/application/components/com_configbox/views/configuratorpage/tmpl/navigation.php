<?php defined('CB_VALID_ENTRY') or die();?>
<div class="navigation">

	<div class="leftpart">
		<?php if (!empty($this->nextLink) && $this->showFinish) { ?>
			<a rel="nofollow" class="navbutton-medium add-to-cart-button <?php echo $this->nextButtonClasses;?>" href="<?php echo $this->finishLink;?>">
				<span class="nav-title"><?php echo KText::_('Finish');?></span>
				<span class="nav-text"><?php echo KText::_('Add to cart');?></span>
			</a>
		<?php } ?>

		<?php if ($this->showRecommendations) { ?>
			<a rel="nofollow" class="navbutton-medium save-bundle-button <?php echo $this->saveBundleButtonClasses;?>" href="<?php echo $this->saveBundleLink;?>">
				<span class="nav-title"><?php echo KText::_('Save');?></span>
				<span class="nav-text"><?php echo KText::_('and recommend');?></span>
			</a>
		<?php } ?>

	</div>

	<div class="rightpart">
		<?php if (!empty($this->nextLink)) { ?>
			<a rel="next" class="navbutton-medium next next-button <?php echo $this->nextButtonClasses;?>" href="<?php echo $this->nextLink;?>">
				<span class="nav-title"><?php echo KText::_('Next')?></span>
				<span class="nav-text"><?php echo hsc($this->nextTitle);?></span>
			</a>
		<?php } elseif($this->showFinish) { ?>
			<a rel="nofollow" class="navbutton-medium next add-to-cart-button <?php echo $this->nextButtonClasses;?>" href="<?php echo $this->finishLink;?>">
				<span class="nav-title"><?php echo KText::_('Finish');?></span>
				<span class="nav-text"><?php echo KText::_('Add to cart');?></span>
			</a>
		<?php } ?>

		<?php if (!empty($this->prevLink)) { ?>
			<a rel="prev" class="navbutton-medium back back-button <?php echo $this->prevButtonClasses;?>" href="<?php echo $this->prevLink;?>">
				<span class="nav-title"><?php echo KText::_('Back')?></span>
				<span class="nav-text"><?php echo hsc($this->prevTitle);?></span>
			</a>
		<?php } ?>
	</div>

	<div class="clear"></div>

</div>