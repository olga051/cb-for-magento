<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewRecord extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	/**
	 * @var object $orderRecord Order record holding everything about an order
	 * @see ConfigboxModelOrderrecord::getOrderRecord
	 */
	public $orderRecord;

	function prepareTemplateVars() {
		
		if (empty($this->orderRecord)) {
			$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
			$orderId = $orderModel->getId();
			$orderRecord = $orderModel->getOrderRecord($orderId);
			$this->assignRef('orderRecord', $orderRecord);
			$this->assign('showProductDetails', true);
			$this->assign('hideSkus', true);
		}

	}
	
}