<?php
interface InterfaceKenedoPlatform {

	public function &getDb();
	public function getDbConnectionData();
	public function initialize();
	public function redirect($url);
	public function logout();
	public function authenticate($username, $passwordClear);
	public function login($username);
	public function getTemplateName();
	public function sendSystemMessage($text, $type = NULL);
	public function getVersionShort();
	public function getDebug();
	public function getDefaultListlimit();
	public function getConfigOffset();
	public function getMailerFromName();
	public function getMailerFromEmail();
	public function getTmpPath();
	public function getLogPath();
	public function getLanguageTag();
	public function getLanguageUrlCode($languageTag = NULL);
	public function getDocumentType();
	public function addScript($path, $type = "text/javascript", $defer = false, $async = false);
	public function addScriptDeclaration($js, $newTag = false, $toBody = false);
	public function addStylesheet($path, $type="text/css", $media = 'all');
	public function addStyleDeclaration($css);
	public function isAdminArea();
	public function isSiteArea();
	public function autoload($className, $classPath);
	public function processContentModifiers($text);
	public function triggerEvent($eventName, $data);
	public function raiseError($errorCode, $errorMessage);
	public function renderHtmlEditor($dataFieldKey, $content, $width, $height, $cols, $rows);
	public function sendEmail($from, $fromName, $receipient, $subject, $body, $isHtml = false, $cc = NULL, $bcc = NULL, $attachmentPath = NULL);
	public function getGeneratorTag();
	public function setGeneratorTag($string);
	public function getUrlBase();
	public function getDocumentBase();
	public function setDocumentBase($string);
	public function setDocumentMimeType($mime);
	public function getDocumentTitle();
	public function setDocumentTitle($string);
	public function setMetaTag($tag,$content);
	public function isLoggedIn();
	public function getUserId();
	public function getUserName($userId = NULL);
	public function getUserFullName($userId = NULL);
	public function getUserPasswordEncoded($userId = NULL);
	public function getUserIdByUsername($username);
	public function getUserGroupId($userId = NULL);
	public function getUserTimezoneName($userId = NULL);
	public function registerUser($data,$groupIds = array());
	public function isAuthorized($task,$userId = NULL, $minGroupId = NULL);
	public function passwordsMatch($passwordClear, $passwordEncrypted);
	public function getRootDirectory();
	public function getAppParameters();
	public function renderOutput(&$output);
	public function startSession();
	public function getPasswordResetLink();
	public function getPlatformLoginLink();
	public function getRoute($url, $encode = true, $secure = NULL);
	public function getActiveMenuItemId();

	/**
	 * @return object[]
	 */
	public function getLanguages();
	public function platformUserEditFormIsReachable();
	public function userCanEditPlatformUsers();
	public function getPlatformUserEditUrl($platformUserId);
	public function getComponentDir($componentName);

	public function getUrlAssets();
	public function getDirAssets();

	public function getDirCache();

	public function getDirCustomization();
	public function getDirCustomizationSettings();

	public function getDirCustomizationAssets();
	public function getUrlCustomizationAssets();

	public function getDirDataCustomer();
	public function getUrlDataCustomer();

	public function getDirDataStore();
	public function getUrlDataStore();

	/**
	 * @param string $component
	 * @param string $viewName
	 * @param string $templateName Name of the view's template, not the platform's main template
	 * @return string $path Absolute path to the file or empty the platform does not support template overrides
	 */
	public function getTemplateOverridePath($component, $viewName, $templateName);

}