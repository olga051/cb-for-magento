<?php
defined('CB_VALID_ENTRY') or die();

class ObserverInvoices {
	
	/**
	 * Generates the invoice and flags the order record as invoice_released
	 * @param int $orderId
	 * @param int $status
	 */
	function onConfigBoxSetStatus( $orderId, $status) {
		
		if (CONFIGBOX_ENABLE_INVOICING && (CONFIGBOX_INVOICE_GENERATION == 0 || CONFIGBOX_INVOICE_GENERATION == 1)) {
			
			$paidStatus = KenedoObserver::triggerEvent('onConfigBoxGetStatusCodeForType',array('paid'),true);
			
			if ($status && $status == $paidStatus && CONFIGBOX_INVOICE_GENERATION == 0) {
				
				// On fully automatic generation, release the invoice
				$db = KenedoPlatform::getDb();
				$query = "UPDATE `#__cbcheckout_order_records` SET `invoice_released` = '1' WHERE `id` = ".(int)$orderId;
				$db->setQuery($query);
				$db->query();
				
				$invoiceModel = KenedoModel::getModel('ConfigboxModelInvoice');
				$invoiceModel->generateInvoice($orderId, 0);
				
			}
			
		}
				
	}
	
}