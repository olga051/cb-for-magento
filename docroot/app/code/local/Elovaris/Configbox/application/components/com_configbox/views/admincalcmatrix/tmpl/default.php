<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewAdmincalcmatrix */
?>

<?php 
ob_start();
?>
<div class="input-picker">
	<div class="tabs">
		<div class="tab tab-open" id="tab-elements"><?php echo KText::_('Elements');?></div>
		<div class="tab tab-closed" id="tab-calculations"><?php echo KText::_('Calculations');?></div>
	</div>
	<div class="panes">
		<div class="pane pane-open" id="pane-elements"><?php KenedoView::getView('ConfigboxViewAdminitempicker')->display();?></div>
		<div class="pane pane" id="pane-calculations"><?php echo KenedoHtml::getSelectField('calculation_id', $this->calculations, NULL, NULL, false, 'calculation-picker','');?></div>
	</div>
</div>
<?php 
$pickerContent = ob_get_clean();
?>

<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>">
	<div id="matrix-editor">
		
		<div class="kenedo-properties">
			
			<div id="property-name-matrix" class="property-name-matrix kenedo-property property-type-matrix">
				<div class="property-label"><?php echo KText::_('Matrix');?></div>
				<div class="property-body">
					
					<table class="matrix-wrapper-table">
						
						<tr>
							<td></td>
							<td class="cell-axis-parameter cell-column-parameter">
								<span class="axis-label label-textfield" style="<?php echo ($this->record->column_type == 'element' && !$this->columnHasOptions) ? 'display:inline;':'display:none;';?>"><?php echo KText::sprintf('Entry in %s','<span class="parameter-title">'.(($this->record->column_element_id) ? hsc($this->elements[$this->record->column_element_id]) : '').'</span>');?></span>
								<span class="axis-label label-options" style="<?php echo ($this->record->column_type == 'element' && $this->columnHasOptions) ? 'display:inline;':'display:none;';?>"><?php echo KText::sprintf('Selection for %s','<span class="parameter-title">'.(($this->record->column_element_id) ? hsc($this->elements[$this->record->column_element_id]) : '').'</span>');?></span>
								<span class="axis-label label-calculation" style="<?php echo ($this->record->column_type == 'calculation') ? 'display:inline;':'display:none;';?>"><?php echo KText::sprintf('Result of calculation %s','<span class="parameter-title">'.(($this->record->column_calc_id) ? $this->calculations[$this->record->column_calc_id] : '').'</span>');?></span>
								<span class="axis-label label-none" style="<?php echo ($this->record->column_type == '') ? 'display:inline;':'display:none;';?>"><?php echo KText::_('No input parameter set');?></span>
								<?php echo KenedoHtml::getTooltip(KText::_('change'), $pickerContent, 'top', 400, 400, NULL, NULL, 'column-parameter-picker');?>
							</td>
						</tr>
						
						<tr>
							<td class="cell-axis-parameter cell-row-parameter">
								<span class="axis-label label-textfield" style="<?php echo ($this->record->row_type == 'element' && !$this->rowHasOptions) ? 'display:inline;':'display:none;';?>"><?php echo KText::sprintf('Entry in %s','<span class="parameter-title">'.(($this->record->row_element_id) ? hsc($this->elements[$this->record->row_element_id]) : '').'</span>');?></span>
								<span class="axis-label label-options" style="<?php echo ($this->record->row_type == 'element' && $this->rowHasOptions) ? 'display:inline;':'display:none;';?>"><?php echo KText::sprintf('Selection for %s','<span class="parameter-title">'.(($this->record->row_element_id) ? hsc($this->elements[$this->record->row_element_id]) : '').'</span>');?></span>
								<span class="axis-label label-calculation" style="<?php echo ($this->record->row_type == 'calculation') ? 'display:inline;':'display:none;';?>"><?php echo KText::sprintf('Result of calculation %s','<span class="parameter-title">'.(($this->record->row_calc_id) ? $this->calculations[$this->record->row_calc_id] : '').'</span>');?></span>
								<span class="axis-label label-none" style="<?php echo ($this->record->row_type == '') ? 'display:inline;':'display:none;';?>"><?php echo KText::_('No input parameter set');?></span>
								<br />
								<?php echo KenedoHtml::getTooltip(KText::_('change'), $pickerContent, 'top', 400, 400, NULL, NULL, 'row-parameter-picker');?>
							</td>
							<td>
								<table class="calc-matrix">
									<thead>
										<tr class="column-parameters">
											<th width="20px" class="dragtable-drag-boundary">
												<a class="toggle-matrix-tools" title="<?php echo KText::_('Show edit icons');?>"></a>
											</th>
											<?php
											$second = false;
											foreach ($this->matrixValues as $y=>$xValues) {
												if ($second == false) {
													$second = true;
													continue;
												}
												foreach ($xValues as $x=>$value) {
													?>
													<th width="20px" class="column-parameter">
														<i class="dragtable-drag-handle column-sort-handle fa fa-reorder"></i>
														<?php if ($this->record->column_type == 'element' && $this->columnHasOptions) {?>
															<?php echo KenedoHtml::getSelectField('option-picker', $this->columnOptions, $x, NULL, false, 'no-chosen');?>
														<?php } else { ?>
															<input class="input-value" type="text" value="<?php echo floatval($x);?>" />
														<?php } ?>
														<span class="trigger-remove"></span>
													</th>
													<?php
												}
												break;
											}
											?>
										</tr>
									</thead>
									<?php foreach ($this->matrixValues as $y=>$xValues) { ?>
										<tr>
											<th width="20px"  class="row-parameter">
												<i class="row-sort-handle fa fa-reorder"></i>
												<?php if ($this->record->row_type == 'element' && $this->rowHasOptions) {?>
													<?php echo KenedoHtml::getSelectField('option-picker', $this->rowOptions, $y, NULL, false, 'no-chosen');?>
												<?php } else { ?>
													<input class="input-value" type="text" value="<?php echo floatval($y);?>" />
												<?php } ?>
												<span class="trigger-remove"></span>
											</th>
											<?php foreach ($xValues as $x=>$value) { ?>
												<td width="20px" ><input class="price" type="text" value="<?php echo floatval($value);?>" /></td>
											<?php } ?>
										</tr>
									<?php } ?>

								</table>
							</td>
							
							<td class="cell-trigger-add-column">
								<a class="trigger-add-column" title="<?php echo KText::_('Add a column');?>"></a>
							</td>
						</tr>
						
						<tr>
							<td>&nbsp;</td>
							<td class="cell-trigger-add-row">
								<a class="trigger-add-row" title="<?php echo KText::_('Add a row');?>"></a>
							</td>
						</tr>
						
					</table>
					
					<input type="hidden" id="matrix" name="matrix" value="" />
					
				</div>
			</div>
			
			
			<?php 
			foreach($this->properties as $property) {
				$property->setData($this->record);
				if ($property->usesWrapper()) {
					?>
				 	<div id="<?php echo $property->getCssId();?>" class="<?php echo $property->renderCssClasses();?>">
				 		<?php if ($property->doesShowAdminLabel()) { ?>
				 			<div class="property-label"><?php echo $property->getLabelAdmin();?></div>
				 		<?php } ?>
				 		<div class="property-body"><?php echo $property->getBodyAdmin();?></div>
				 	</div>
					<?php
				} 
				else {
					echo $property->getBodyAdmin();
				}
			}
			?>

		</div>
	</div>
</div>