<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxCalcTermElementAttribute extends ConfigboxCalcTerm {

	function containsElementId($termData, $elementId) {
		return ($elementId == $termData['elementId']);
	}

	function containsXrefId($termData, $xrefId) {
		return false;
	}

	/**
	 * Called by ConfigboxRulesHelper::getTermCode to get the term result
	 *
	 * @param string[] $termData
	 * @param array[] $selections
	 * @param int|NULL $regardingElementId The Element ID to which the calculation is assigned to
	 * @param int|NULL $regardingXrefId The option assignment ID to which the calculation is assigned to
	 * @param boolean $allowNonNumeric If the result can be non-numeric
	 * @return float The calculated result
	 *
	 * @see ConfigboxCalculation::getTermsCode, ConfigboxCalculation::getTerms, ConfigboxCalculation::getSelections
	 */
	function getTermResult($termData, $selections, $regardingElementId = NULL, $regardingXrefId = NULL, $allowNonNumeric = false) {

		if ($termData['elementId'] == 'regarding') {
			$termData['elementId'] = $regardingElementId;
			$termData['fieldPath'] = str_replace('selectedOption.', 'regardingOption.', $termData['fieldPath']);
		}

		switch ($termData['fieldPath']) {

			case 'price':
				$isValue = ConfigboxPrices::getElementPrice($termData['elementId']);
				break;

			case 'priceRecurring':
				$isValue = ConfigboxPrices::getElementPriceRecurring($termData['elementId']);
				break;

			case 'selected':
				$isValue = (isset($selections[$termData['elementId']])) ? $selections[$termData['elementId']]['text'] : '';
				break;

			default:
				$element = ConfigboxElementHelper::getElement($termData['elementId']);
				//TODO: See if $regardingXrefId makes sense here. Shouldn't it be the selected xref id?
				$isValue = $element->getField( $termData['fieldPath'], $regardingXrefId, $termData['fallbackValue']);
				break;

		}

		// Get the found value, the fallback value or 0
		if (!empty($isValue)) {
			$return = $isValue;
		}
		elseif(!empty($termData['fallbackValue'])) {
			$return = $termData['fallbackValue'];
		}
		else {
			$return = 0;
		}

		// Sanitize the value
		if ($allowNonNumeric == false && is_numeric($return) == false) {
			$return = floatval($return);
		}

		return $return;

	}

	function getTermsPanelHtml() {
		$view = KenedoView::getView('ConfigboxViewAdmincalcformula_elementattribute');
		$view->prepareTemplateVars();
		return $view->getViewOutput('default');
	}

	/**
	 * Called by ConfigboxCalculation::getTermHtml to display the condition (either for editing or display)
	 *
	 * @param string[] $termData
	 * @param bool $forEditing If edit controls or plain display should come out
	 * @return string HTML for that term
	 * @see ConfigboxCalculation::getTermHtml
	 */
	function getTermHtml($termData, $forEditing = true) {
		$attributes = $this->getElementAttributes();
		if (strtolower($termData['elementId']) == 'regarding') {
			$termName = KText::sprintf($attributes[$termData['fieldPath']]['text'], KText::_('Regarding Element'));
		}
		else {
			$termName = KText::sprintf($attributes[$termData['fieldPath']]['text'], hsc(ConfigboxRulesHelper::getElementTitle($termData['elementId'])));
		}

		ob_start();
		?>
		<span class="item term element-field-value"
			  data-type="ElementAttribute"
			  data-element-id="<?php echo hsc($termData['elementId']);?>"
			  data-field-path="<?php echo hsc($termData['fieldPath']);?>">

			<?php echo hsc($termName); ?>

			<?php if ($forEditing) { ?>
				<?php echo KText::_('or');?>
				<input type="text" class="input fallback-value" data-data-key="fallbackValue" value="<?php echo hsc($termData['fallbackValue']);?>" />
			<?php } else { ?>
				<?php echo ($termData['fallbackValue']) ? KText::_('or') : '';?>
				<span class="term-value"><?php echo hsc($termData['fallbackValue']);?></span>
			<?php } ?>

		</span>
		<?php
		return ob_get_clean();
	}

	function getTypeTitle() {
		return KText::_('Elements');
	}

	/**
	 * Returns the possible element subjects for elementattribute conditions
	 * @return array[]
	 */
	function getElementAttributes() {

		$assignmentCustom1 = (trim(CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_1)) ? CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_1 : KText::sprintf('Assignment Custom %s',1);
		$assignmentCustom2 = (trim(CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_2)) ? CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_2 : KText::sprintf('Assignment Custom %s',2);
		$assignmentCustom3 = (trim(CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_3)) ? CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_3 : KText::sprintf('Assignment Custom %s',3);
		$assignmentCustom4 = (trim(CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_4)) ? CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_4 : KText::sprintf('Assignment Custom %s',4);

		$optionCustom1 = (trim(CONFIGBOX_LABEL_OPTION_CUSTOM_1)) ? CONFIGBOX_LABEL_OPTION_CUSTOM_1 : KText::sprintf('Option Custom %s',1);
		$optionCustom2 = (trim(CONFIGBOX_LABEL_OPTION_CUSTOM_2)) ? CONFIGBOX_LABEL_OPTION_CUSTOM_2 : KText::sprintf('Option Custom %s',2);
		$optionCustom3 = (trim(CONFIGBOX_LABEL_OPTION_CUSTOM_3)) ? CONFIGBOX_LABEL_OPTION_CUSTOM_3 : KText::sprintf('Option Custom %s',3);
		$optionCustom4 = (trim(CONFIGBOX_LABEL_OPTION_CUSTOM_4)) ? CONFIGBOX_LABEL_OPTION_CUSTOM_4 : KText::sprintf('Option Custom %s',4);

		$elementCustom1 = (trim(CONFIGBOX_LABEL_ELEMENT_CUSTOM_1)) ? CONFIGBOX_LABEL_ELEMENT_CUSTOM_1 : KText::sprintf('Element Custom %s',1);
		$elementCustom2 = (trim(CONFIGBOX_LABEL_ELEMENT_CUSTOM_2)) ? CONFIGBOX_LABEL_ELEMENT_CUSTOM_2 : KText::sprintf('Element Custom %s',2);
		$elementCustom3 = (trim(CONFIGBOX_LABEL_ELEMENT_CUSTOM_3)) ? CONFIGBOX_LABEL_ELEMENT_CUSTOM_3 : KText::sprintf('Element Custom %s',3);
		$elementCustom4 = (trim(CONFIGBOX_LABEL_ELEMENT_CUSTOM_4)) ? CONFIGBOX_LABEL_ELEMENT_CUSTOM_4 : KText::sprintf('Element Custom %s',4);

		return array(

			'selected' => array ('text'=>KText::_('Entry in %s') ) ,
			'price' => array ('text'=>KText::_('Price of %s') ) ,
			'priceRecurring' => array ('text'=>KText::_('Recurring Price of %s') ) ,

			'element_custom_1' => array ('text'=>KText::_("$elementCustom1 in %s") ) ,
			'element_custom_2' => array ('text'=>KText::_("$elementCustom2 in %s") ) ,
			'element_custom_3' => array ('text'=>KText::_("$elementCustom3 in %s") ) ,
			'element_custom_4' => array ('text'=>KText::_("$elementCustom4 in %s") ) ,

			'selectedOption.assignment_custom_1' => array ('text'=>KText::_("$assignmentCustom1 in %s") ) ,
			'selectedOption.assignment_custom_2' => array ('text'=>KText::_("$assignmentCustom2 in %s") ) ,
			'selectedOption.assignment_custom_3' => array ('text'=>KText::_("$assignmentCustom3 in %s") ) ,
			'selectedOption.assignment_custom_4' => array ('text'=>KText::_("$assignmentCustom4 in %s") ) ,

			'selectedOption.option_custom_1' => array ('text'=>KText::_("$optionCustom1 in %s") ) ,
			'selectedOption.option_custom_2' => array ('text'=>KText::_("$optionCustom2 in %s") ) ,
			'selectedOption.option_custom_3' => array ('text'=>KText::_("$optionCustom3 in %s") ) ,
			'selectedOption.option_custom_4' => array ('text'=>KText::_("$optionCustom4 in %s") ) ,

		);

	}

}