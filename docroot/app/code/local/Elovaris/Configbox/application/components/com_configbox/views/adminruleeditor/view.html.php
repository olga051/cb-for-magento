<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdminRuleeditor extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var bool $ruleIsSet Indicates if the editor already got a rule loaded or if the rule is empty
	 */
	public $ruleIsSet;

	/**
	 * @var string The HTML of the editable rule
	 */
	public $ruleHtml;

	/**
	 * @var string The ID of the parent HTML element holding the value for storing the rule (along with the element or option)
	 */
	public $returnFieldId;

	/**
	 * @var string[] Tab info for the condition panels (key is condition type, value is the condition type's title)
	 * @see ConfigboxCondition::getTypeName, ConfigboxCondition::getTypeTitle
	 */
	public $conditionTabs;

	/**
	 * @var string[] HTML of the condition type's panel, will show up when the user clicks on the type's tab
	 * @see ConfigboxCondition::getConditionsPanelHtml
	 */
	public $conditionPanels;

	/**
	 * @var string The selected panel (fixed)
	 */
	public $selectedTypeName;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {

		$rule = KRequest::getVar('rule','');

		// Get the rule HTML and assign it
		if ($rule) {
			$ruleHtml = ConfigboxRulesHelper::getRuleHtml($rule);
			$this->assign('ruleHtml', $ruleHtml);
			$this->assign('ruleIsSet', true);
		}
		else {
			$this->assign('ruleIsSet', false);
		}

		// Get and set the return field id, this is the id of the input element in the form holding item's data
		$returnFieldId = KRequest::getString('return_field_id', '');
		$this->assignRef('returnFieldId', $returnFieldId);

		// Get all available condition type names
		$conditionTypeNames = ConfigboxCondition::getConditionTypeNames();

		// The intended ordering for tabs (other types will be appended after those)
		$ordering = array(
			'ElementAttribute',
			'Calculations',
			'CustomerGroups',
		);

		// Go through that list and add the real
		$orderedTypeNames = array();
		foreach ($ordering as $typeName) {
			$key = array_search($typeName, $conditionTypeNames);
			if ($key) {
				$orderedTypeNames[] = $conditionTypeNames[$key];
				unset($conditionTypeNames[$key]);
			}
		}
		foreach ($conditionTypeNames as $typeName) {
			$orderedTypeNames[] = $typeName;
		}
		unset($conditionTypeNames);

		// Set up all panels for available conditions
		$tabs = array();
		$panels = array();
		foreach ($orderedTypeNames as $typeName) {
			$condition = ConfigboxCondition::getCondition($typeName);
			$tabs[$typeName] = $condition->getTypeTitle();
			$panels[$typeName] = $condition->getConditionsPanelHtml();
		}

		$this->assignRef('selectedTypeName', $ordering[0]);
		$this->assignRef('conditionTabs', $tabs);
		$this->assignRef('conditionPanels', $panels);

		$this->addViewCssClasses();

	}
	
}
