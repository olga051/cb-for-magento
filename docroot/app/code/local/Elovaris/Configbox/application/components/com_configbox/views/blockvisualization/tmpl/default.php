<?php
defined('CB_VALID_ENTRY') or die('Restricted access');
/** @var $this ConfigboxViewBlockVisualization */
$widthAndHeight = "width: ". intval($this->imageWidth)."px; height: ".intval($this->imageHeight)."px;";

if (count($this->images) > 0 || $this->productInfo->baseimage) {
	?>
	<div class="configbox-block block-visualization <?php echo ($this->params->get('moduleclass_sfx',''));?> mod_configboximages <?php echo ($this->params->get('moduleclass_sfx',''));?> product-id-<?php echo $this->productId;?> page-id-<?php echo $this->pageId;?>">
		
		<?php if (CONFIGBOX_BLOCKTITLE_VISUALIZATION) { ?><h2 class="block-title"><?php echo hsc(CONFIGBOX_BLOCKTITLE_VISUALIZATION);?></h2><?php } ?>
		
		<div class="visualization-frame" style="<?php echo $widthAndHeight;?>">
			<div class="visualization-handle" style="<?php echo $widthAndHeight;?>">
				<?php
				if ($this->productInfo->baseimage) {
					?>
					<div class="base-image">
						<img src="<?php echo $this->baseDir.'/'.$this->productInfo->baseimage;?>" alt="<?php echo hsc(KText::_('Base image'));?>" />
					</div>
					<?php
				}
				
				foreach ($this->images as $image) {
					
					// Skip the pre-loading if the image is on another page and not used yet - except if it can get selected by auto-select
					// Page ID may not be defined, in that case pre-load any image
					if ($image->selected == false && $this->pageId && $image->page_id != $this->pageId ) {
						if ($image->xref_default == 0 or $image->autoselect_default == 0) {						
							continue;
						}
					}
					
					?>
					<div class="<?php echo $image->css_classes;?>" id="<?php echo $image->css_id;?>" style="<?php echo $widthAndHeight;?> <?php echo ($image->selected) ? 'display:block;':'display:none';?>">
						<?php if ($image->visualization_image) { ?>
							<img src="<?php echo ($image->selected) ? $image->visualization_image : $this->blankImage;?>" width="<?php echo intval($this->imageWidth);?>px" height="<?php echo intval($this->imageHeight);?>px" alt="<?php echo 'xref-'.$image->xref_id;?>" data-src="<?php echo $image->visualization_image;?>"<?php echo (!$image->selected) ? ' class="preload-image"':'';?> />
						<?php } ?>
					</div>				
					<?php
				}
				?>
			</div>
		</div>
	</div>
	<?php 
}
?>