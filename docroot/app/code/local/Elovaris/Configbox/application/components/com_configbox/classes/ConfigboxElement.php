<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxElement {

	/**
	 * @var ConfigboxOption[] $options
	 */
	public $options = array();

	public $id;
	public $cat_id;
	public $el_image;
	public $layoutname;
	public $required;
	public $dependencies;
	public $hidenonapplying;
	public $confirm_deselect;
	public $autoselect_default;
	public $validate;
	public $minval;
	public $maxval;
	public $integer_only;
	public $calcmodel;
	public $calcmodel_recurring;
	public $multiplicator;
	public $published;
	public $ordering;
	public $checked_out;
	public $checked_out_time;
	public $class;
	public $classparams;
	public $asproducttitle;
	public $default_value;
	public $show_in_overview;
	public $text_calcmodel;
	public $element_custom_1;
	public $element_custom_2;
	public $element_custom_3;
	public $element_custom_4;
	public $element_custom_translatable_1;
	public $element_custom_translatable_2;
	public $rules;
	public $internal_name;
	public $element_css_classes;
	public $calcmodel_id_min_val;
	public $calcmodel_id_max_val;
	public $autoselect_any;
	public $as_textarea;
	public $as_dropdown;
	public $widget;
	public $upload_extensions;
	public $upload_mime_types;
	public $upload_size_mb;
	public $slider_steps;
	public $unit;
	public $upload_visualize;
	public $upload_visualization_view;
	public $upload_visualization_stacking;
	public $calcmodel_weight;
	public $choices;

	public $applies;
	public $cssClasses;
	public $extraAttributes;
	public $type;
	public $option;
	public $title;
	public $description;
	public $picker_table;

	/**
	 * @var int $desc_display_method See setting in element for description.
	 */
	public $desc_display_method;

	/**
	 * @var string $elementImageCssClasses CSS classes to be set to the element image (see GUI for what that is).
	 * Set in ConfigboxViewConfiguratorpage::display()
	 * @see ConfigboxViewConfiguratorpage::display
	 */
	public $elementImageCssClasses;

	/**
	 * @var string $elementImagePreloadAttributes HTML attributes for the element image (used for smart preloading/delayed loading)
	 * ConfigboxViewConfiguratorpage::display()
	 * @see ConfigboxViewConfiguratorpage::display
	 */
	public $elementImagePreloadAttributes;

	/**
	 * @var string $elementImageSrc Full URL of the element image. Set in ConfigboxViewConfiguratorpage::display()
	 * @see ConfigboxViewConfiguratorpage::display
	 */
	public $elementImageSrc;

	/**
	 * @var string $disabled HTML attribute, used in input elements. Set in ConfigboxViewConfiguratorpage::prepareTemplateVars()
	 * @see ConfigboxViewConfiguratorpage::prepareTemplateVars
	 */
	public $disabled;

	/**
	 * @var int $selectedXrefId ID of currently selected option assignment. Set in ConfigboxViewConfiguratorpage::display()
	 * @see ConfigboxViewConfiguratorpage::display
	 */
	public $selectedXrefId;

	/**
	 * @var string $templateFile Full path to the template file. Depends on element's settings and existing overrides.
	 * Figured out in ConfigboxViewConfiguratorpage::display()
	 * @see ConfigboxViewConfiguratorpage::display
	 */
	public $templateFile;

	function __construct($id, $bare = false) {
				
		KLog::count('element_constructions');
		
		$this->setId($id);
		$this->loadData();
		
		$this->loadOptions();		
		$this->determineType();
		
		$this->cssId = 'elementwrapper-'.$this->id;
		$this->addCssClass('element type-'.$this->type.' template-'.$this->layoutname.' class-'. ( ($this->class) ? $this->class : 'default'). ' '.filter_var($this->element_css_classes,FILTER_SANITIZE_SPECIAL_CHARS));
		
		if ($this->type == 'text') {
			$this->addCssClass('widget-'.$this->widget);
		}
		
		if ($this->hidenonapplying == 1) {
			$this->addCssClass(' hide-not-applying');
		}
		
	}
	
	function getPrice($getNet = NULL, $inBaseCurrency = false) {
		if (!$this->applies()) {
			return 0;
		}
		return ConfigboxPrices::getElementPrice($this->id, $getNet, $inBaseCurrency);
	}
	
	function getPriceRecurring($getNet = NULL, $inBaseCurrency = false) {
		if (!$this->applies()) {
			return 0;
		}
		return ConfigboxPrices::getElementPriceRecurring($this->id, $getNet, $inBaseCurrency);
	}
	
	function addCssClass($class) {
		$class = str_replace('  ', ' ', $class);
		$classes = explode(' ',$class);
		foreach ($classes as $class) {
			$this->cssClasses[$class] = hsc($class);
		}
		
	}
	
	function removeCssClass($class) {
		if (isset($this->cssClasses[$class])) {
			unset($this->cssClasses[$class]);
		}
	}
	
	function getCssClasses() {
		return implode(' ',$this->cssClasses);
	}
	
	function setId($id) {
		$this->id = $id;
	}
	
	function getWeight() {
		
		return ConfigboxPrices::getElementWeight($this->id);
	}
	
	function getRawValue() {
		$configuration = ConfigboxConfiguration::getInstance();
		$rawValue = $configuration->getSelection($this->id);
		return $rawValue;
	}
	
	/**
	 * 
	 * This method outputs the selection text for the element as it should be displayed in overviews Can be HTML
	 * This can be the selected option's title, a localized and formatted date, the link to the file upload etc.
	 * 
	 * @param int $xrefId
	 * @param string $text
	 * @return string Output value
	 */
	function getOutputValue( $xrefId = NULL, $text = NULL ) {
		
		if ($xrefId === NULL && $text === NULL) {
			$configuration = ConfigboxConfiguration::getInstance();
			$xrefId = $configuration->getElementXrefId($this->id);
			$text = $configuration->getElementTextEntry($this->id);
		}
		
		if ($xrefId) {
			return !empty($this->options[$xrefId]->title) ? $this->options[$xrefId]->title : '';
		}
		
		if ($text !== '') {
			return $this->parseTextEntry($text);
		}
		
		return '';
	}

	/**
	 * @param string $text
	 *
	 * @return string
	 */
	function parseTextEntry( $text ) {
		if ($this->widget == 'calendar') {
			if ($text) {
				$value = KenedoTimeHelper::getFormattedOnly($text,KText::_('CALENDAR_DATE_FORMAT_PHP'));
				return $value;
			}
			else {
				return KText::_('No date selected');
			}
		}
		
		if ($this->widget == 'fileupload') {
			
			if ($text != '' && $text != 0) {
				$link = '<a href="'.CONFIGBOX_URL_CONFIGURATOR_FILEUPLOADS.'/'.$text.'" target="_blank">'.KText::_('File Upload').'</a>';
				return $link;
			}
			else {
				return '';
			}
		}
		
		if (is_numeric($text)) {
			if ($this->unit) {
				$value = '<span>'.str_replace('.', KText::_('DECIMAL_MARK','.'), $text) . ' <span class="element-unit">'.hsc($this->unit).'</span></span>';
			}
			else {
				$value = str_replace('.', KText::_('DECIMAL_MARK','.'), $text);
			}
			
			return $value;
		}
		return $text;
		
	}

	/**
	 * @return array
	 */
	function getInitialValue() {
		
		$initialRecord = array('text'=>'','xref_id'=>0);
		
		if (function_exists('overrideElementGetInitialValue')) {
			$override = overrideElementGetInitialValue($this);
			if ($override !== false) {
				return $override;
			}
		}
		
		// Get date from the value
		if ($this->widget == 'calendar' && $this->default_value !== '') {
			$timestamp = $this->dateEntryToTimestamp($this->default_value);
			$initialRecord['text'] = KenedoTimeHelper::getFormattedOnly($timestamp,'date');
			return $initialRecord;
			
		}
		
		// Get the default xref
		if (count($this->options)) {
			foreach ($this->options as $option) {
				if ($option->default == 1 && $option->published == 1) {
					$initialRecord['xref_id'] = $option->id;
					return $initialRecord;
				}
			}
		}
		else {
			// Get the default text
			if ($this->default_value !== '') {
				$initialRecord['text'] = $this->default_value;
				return $initialRecord;
			}
		}

		return $initialRecord;
		
	}
	
	function dateEntryToTimestamp($val) {
		
		if (intval($val) == $val) {
			$string = ( ($val >= 0) ? '+':'-' ). abs($val) .' days';
			$timestamp = strtotime($string);
		}
		else {
			$timestamp = strtotime($val);
		}
		
		return $timestamp;
	}
	
	function getMinimumValue() {
		
		if ($this->calcmodel_id_min_val) {
			$minVal = ConfigboxCalculation::calculate($this->calcmodel_id_min_val, $this->id);
			if ($minVal === NULL or $minVal === '') {
				$minVal = NULL;
			}
		}
		else {
			$minVal = $this->minval;
			if ($minVal === NULL or $minVal === '') {
				$minVal = NULL;
			}
		}
		
		if ($minVal !== NULL && $this->widget == 'calendar') {
			$timestamp = $this->dateEntryToTimestamp($minVal);			
			$minVal = KenedoTimeHelper::getFormattedOnly($timestamp,'date');
		}
		
		return $minVal;
		
	}
	
	function getMaximumValue() {
	
		if ($this->calcmodel_id_max_val) {
			$maxVal = ConfigboxCalculation::calculate($this->calcmodel_id_max_val, $this->id);
			if ($maxVal === NULL or $maxVal === '') {
				$maxVal = NULL;
			}
		}
		else {
			$maxVal = $this->maxval;
			if ($maxVal === NULL or $maxVal === '') {
				$maxVal = NULL;
			}
		}
		
		if ($maxVal !== NULL && $this->widget == 'calendar') {
			$timestamp = $this->dateEntryToTimestamp($maxVal);
			$maxVal = KenedoTimeHelper::getFormattedOnly($timestamp,'date');
		}
		
		return $maxVal;
	
	}
	
	function isValueToLow($value) {
		
		if ($this->validate) {

			$refValue = $this->getMinimumValue();
			
			if ($refValue === NULL) {
				return false;
			}
			
			if ($this->widget == 'calendar') {

				$tsRef = strtotime($refValue);
				$tsVal = strtotime($value);
				
				if ($tsVal < $tsRef) {
					return true;
				}
				else {
					return false;
				}
	
			}
				
			if ($this->widget == 'text' || $this->widget == 'slider' || $this->widget == 'choices') {
		
				if ($value < $refValue) {
					return true;
				}
				else {
					return false;
				}
	
			}
		}

		return false;

	}
	
	function isValueToHigh($value) {
		
		if ($this->validate) {

			$refValue = $this->getMaximumValue();
			
			if ($refValue === NULL) {
				return false;
			}
			
			if ($this->widget == 'calendar') {

				$tsRef = strtotime($refValue);
				$tsVal = strtotime($value);
				
				if ($tsVal > $tsRef) {
					return true;
				}
				else {
					return false;
				}
	
			}
				
			if ($this->widget == 'text' || $this->widget == 'slider' || $this->widget == 'choices') {
		
				if ($value > $refValue) {
					return true;
				}
				else {
					return false;
				}
	
			}
		}

		return false;
	}
	
	/**
	 * Returns if the provided value can be set for the element
	 * On file uploads, the info is taken from $_FILES
	 * 
	 * @param mixed $value Raw value as sent via XHR
	 * @return boolean|string true or a localized user message containing the reason.
	 */
	function isValidValue($value) {
		
		if ($this->type != 'text') {
			return true;
		}
		
		if ($this->widget == 'fileupload') {
				
			$extensions = strtolower($this->upload_extensions);
			$extensions = str_replace(',', ' ', $extensions);
			$extensions = str_replace('.', '', $extensions);
			$extensions = str_replace('  ', ' ', $extensions);
			$extensions = explode(' ',$extensions);
			$validExtensions = array_map('trim',$extensions);
				
			if ($key = array_search('php',$validExtensions)) {
				unset($validExtensions[$key]);
			}
				
			$mimeTypes = strtolower($this->upload_mime_types);
			$mimeTypes = str_replace(',', ' ', $mimeTypes);
			$mimeTypes = str_replace('.', '', $mimeTypes);
			$mimeTypes = str_replace('  ', ' ', $mimeTypes);
			$mimeTypes = explode(' ',$mimeTypes);
			$validMimeTypes = array_map('trim',$mimeTypes);
				
			$file = $_FILES['configbox-widget-fileupload-filefield'];
				
			$fileExtension = substr(strrchr($file['name'],'.'),1);
			if (!in_array($fileExtension,$validExtensions)) {
				$response = KText::sprintf('Files with extension %s are not allowed.',$fileExtension);
				return $response;
			}
				
			$fileMimeType = KenedoFileHelper::getMimeType($file['tmp_name']);
			if ($fileMimeType) {
				if (!in_array($fileMimeType,$validMimeTypes)) {
					$response = KText::sprintf('Files with MIME type %s are not allowed.',$fileMimeType);
					return $response;
				}
			}
				
			if ($this->upload_size_mb != 0) {
				$validFilesizeBytes = $this->upload_size_mb * 1024 * 1024;
				if ( filesize($file['tmp_name']) > $validFilesizeBytes ) {
					$response = KText::sprintf('File size is over the maximum of %s MB.', $this->upload_size_mb);
					return $response;
				}
			}

			return true;
		}
		else {
			
			if ($this->isValueToLow($value)) {
				$minimumValue = $this->getMinimumValue();
				if ($this->widget == 'calendar') {
					$response = KText::sprintf('The earliest date is %s.',$this->parseTextEntry($minimumValue));
				}
				else {
					$response = KText::sprintf('Value must be at least %s.',$this->parseTextEntry($minimumValue));
				}
				return $response;
			}
			
			if ($this->isValueToHigh($value)) {
				$maximumValue = $this->getMaximumValue();
				if ($this->widget == 'calendar') {
					$response = KText::sprintf('The latest date is %s.',$this->parseTextEntry($maximumValue));
				}
				else {
					$response = KText::sprintf('Value must be %s or less.',$this->parseTextEntry($maximumValue));
				}
				return $response;
			}
			
			return true;
			
		}

	}
	
	function loadOptions() {
		
		$assignments = ConfigboxCacheHelper::getAssignments();
		$xrefs = isset($assignments['element_to_xref'][$this->id]) ? $assignments['element_to_xref'][$this->id] : array();
		
		$options = array();
		foreach ($xrefs as $xrefId) {
			$options[] = ConfigboxCacheHelper::getXrefData($xrefId);
		}
		
		$this->options = array();
		if (count($options) == 0) return true;
		
		foreach ($options as &$option) {
			unset($option->price,$option->price_recurring);
			$this->options[$option->id] = new ConfigboxOption($option);
		}
		unset($options);

		foreach ($this->options as $option) {

			$option->checked = '';
			$option->disabled = '';
			$option->controlClasses = ' configbox_control ';
			
			$option->addCssClass('xref xrefwrapper');
			
			if ($option->option_picker_image) {
				$option->addCssClass('image-picker');
			}
			
			if ($option->available == 0) {
				if ($option->disable_non_available) {
					$option->addCssClass('not-available');
					$option->disabled = 'disabled = "disabled"';
				}
			}
			else {
				$option->addCssClass('available');
			}

		}

		return true;

	}
	
	function getType() {
		if (!$this->type) {
			$this->determineType();
		}
		return $this->type;		
	}
	
	function determineType() {
		
		if ( $this->text_calcmodel == 0 && (!empty($this->calcmodel) || !empty($this->calcmodel_recurring))) {
			$this->type = 'calcmodel';
			return;
		}
		
		switch (count($this->options)) {
			case 0:
				$this->type = 'text';
			break;
			case 1:
				$this->type = 'checkbox';
			break;
			default:
				if ($this->as_dropdown) {
					$this->type = 'dropdown';
				}
				else {
					$this->type = 'radio';
				}
			break;
		}
	}
	
	function applies() {
		return ConfigboxRulesHelper::ruleIsFollowed($this->rules,'element',$this->id);
	}
	
	function loadData() {
		
		$data = ConfigboxCacheHelper::getElementData($this->id);

		if (!$data) {
			KLog::log('Element with ID "'.$this->id.'" (Type of ID: '.var_export($this->id, true).') not found. Backtrace: '.var_export(debug_backtrace(false), true), 'error');
			return false;
		}

		foreach ($data as $k => &$v) {
			$this->$k = $v;
		}
		return true;
	
	}
	
	function getField($path, $regardingXrefId = NULL, $default = NULL) {
		
		KLog::log('Getting element field for element "'.$this->id.'". Path is "'.$path.'"');
		
		if ($default === NULL) {
			$default = 0;
		}
		
		if (strstr($path,'.')) {
			$attributePath = explode('.',$path);
			$obj = $this;
			foreach ($attributePath as $attributeCrumb) {
				
				if (strtolower($attributeCrumb) == 'regardingoption' && $regardingXrefId) {
					$obj = $this->options[$regardingXrefId];
					continue;
				}
				
				if (strtolower($attributeCrumb) == 'selectedoption') {
					
					$configuration = ConfigboxConfiguration::getInstance();
					$selectedXrefId = $configuration->getElementXrefId($this->id);
					
					if ($selectedXrefId) {
						$obj = $this->options[$selectedXrefId];
						continue;
					}
					
				}
				
				if (!empty($obj->$attributeCrumb) && (is_string($obj->$attributeCrumb) || $obj->$attributeCrumb != 0)) {
					// Replace the object (if the looping is ongoing, this will be replaced again until we reached the last one).
					$obj = $obj->$attributeCrumb;
				}
				else {
					KLog::log('Attribute "'.$attributeCrumb.'" not found in element ID "'.$this->id.'" or is empty. Attribute path was "'.$path.'". Using "'.$default.'" as value.','debug');
					$obj = $default;
					break;
				}
			}
			return $obj;
		}
		else {
			
			if (strtolower($path) == 'entry') {
				$configuration = ConfigboxConfiguration::getInstance();
				$entry = $configuration->getElementTextEntry($this->id);
				return $entry;
			}
			
			if (isset($this->$path)) $value = $this->$path;
			else $value = $default;
			
			return $value;
		}
		
	}
	
}
