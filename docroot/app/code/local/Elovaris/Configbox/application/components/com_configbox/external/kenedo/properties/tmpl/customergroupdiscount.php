<?php
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this KenedoPropertyCustomergroupdiscount
 */
$keyDiscountFactor = 'discount_factor_'.$this->getPropertyDefinition('discountlevel');
$keyDiscountStart = 'discount_start_'.$this->getPropertyDefinition('discountlevel');

?>
<div class="price">
	<label for="<?php echo $this->propertyName; ?>_start"><?php echo KText::_('For net order totals starting at:'); ?></label>
	<input type="text" name="<?php echo $this->propertyName; ?>_start" id="<?php echo $this->propertyName; ?>_start" value="<?php echo $this->data->$keyDiscountStart != 0 ? cbprice($this->data->$keyDiscountStart, false) : ''; ?>" /> <?php echo CONFIGBOX_BASE_CURRENCY_SYMBOL;?>
</div>
<div class="discount">
	<label for="<?php echo $this->propertyName; ?>_factor"><?php echo KText::_('Discount Rate:'); ?></label>
	<input type="text" name="<?php echo $this->propertyName; ?>_factor" id="<?php echo $this->propertyName; ?>_factor" value="<?php echo $this->data->$keyDiscountFactor != 0 ? ConfigboxCustomerGroupHelper::getFormattedDiscount($this->data->$keyDiscountFactor) : ''; ?>" /> %
	<label class="discount-label"><?php echo KText::_('Discount Label');?></label>
</div>
