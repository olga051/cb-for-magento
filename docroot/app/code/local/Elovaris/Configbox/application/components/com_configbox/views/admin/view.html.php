<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdmin extends KenedoView {

	/**
	 * @var string $contentHtml Supposed to be set from outside, otherwise Admindashboard view output will be inserted.
	 */
	public $contentHtml;

	function prepareTemplateVars() {

		$this->addViewCssClasses();
		
		if (empty($this->contentHtml)) {
			ob_start();
			KenedoView::getView('ConfigboxViewAdmindashboard')->display();
			$this->assign('contentHtml', ob_get_clean());
		}
		
	}
	
}
