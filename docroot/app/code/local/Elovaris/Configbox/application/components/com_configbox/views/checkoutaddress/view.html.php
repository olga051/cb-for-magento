<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewCheckoutaddress extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'checkoutaddress';

	/**
	 * Holds all information about the order
	 * @var object $orderRecord
	 * @see ConfigboxModelOrderrecord::getOrderREcord
	 */
	public $orderRecord;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {
		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
		$orderId = $orderModel->getId();
		$orderRecord = $orderModel->getOrderRecord($orderId);
		$this->assignRef('orderRecord', $orderRecord);
	}

	function display() {
		$this->prepareTemplateVars();
		if (!$this->orderRecord) {
			$this->renderView('notfound');
		}
		else {
			$this->renderView('default');
		}
	}
}