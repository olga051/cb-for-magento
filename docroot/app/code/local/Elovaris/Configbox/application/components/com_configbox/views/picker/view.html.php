<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewPicker extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'picker';

	/**
	 * @var object Element data
	 * @see ConfigboxElementHelper::getElement
	 */
	public $element;

	/**
	 * @var string ID of the HTML element that holds the user-friendly name of the entry made in the picker.
	 */
	public $displayTarget;

	/**
	 * @var string ID of the HTML element that holds the value that shall be saved as selection.
	 */
	public $target;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {
				
		$id = KRequest::getInt('id');		
		$element = ConfigboxElementHelper::getElement($id);
		
		$this->assignRef('element', $element);
		$this->assign('target', KRequest::getString('target'));
		$this->assign('displayTarget', KRequest::getString('display_target'));

	}

}