<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerInforequest extends KenedoController {

	/**
	 * @return NULL
	 */
	protected function getDefaultModel() {
		return NULL;
	}

	/**
	 * @return ConfigboxViewInforequest
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewInforequest');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

}
