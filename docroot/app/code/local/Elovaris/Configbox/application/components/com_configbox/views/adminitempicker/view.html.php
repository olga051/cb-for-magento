<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdminitempicker extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @return ConfigboxModelAdminproducttree
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdminproducttree');
	}

	function prepareTemplateVars() {

		$model = $this->getDefaultModel();
		$tree = $model->getTree(false);
		$this->assignRef('tree',$tree);

		if (KRequest::getString('open_tree_ids')) {
			$openIds = json_decode(KRequest::getString('open_tree_ids'), true);
		}
		else {
			$openIds = array('products'=>array(), 'pages'=>array(), 'elements'=>array());
		}
		
		$this->assign('openIds', $openIds);
		
		$this->addViewCssClasses();

	}
	
}