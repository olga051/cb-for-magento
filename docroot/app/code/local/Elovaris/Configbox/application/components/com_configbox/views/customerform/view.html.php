<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewCustomerform extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var object $customerData
	 * @see ConfigboxUserHelper::getUser
	 */
	public $customerData;

	/**
	 * @var object[] $customerFields Info about which fields should be visible
	 * @see ConfigboxUserHelper::getUserFields
	 */
	public $customerFields;

	/**
	 * @var string $customerFields in JSON
	 * @see customerFields
	 */
	public $customerFieldsJson;

	/**
	 * @var string[] $fieldCssClasses Array of strings containing CSS classes for each form field. Keys of array are field names
	 */
	public $fieldCssClasses;

	/**
	 * @var string $formType (quotation|checkout|profile)
	 */
	public $formType = '';

	/**
	 * @var bool $useCityLists If dropdowns for cities shall be used (depends on if there are cities defined)
	 */
	public $useCityLists;

	/**
	 * @var boolean $allowDeliveryAddress If the delivery address toggle should be shown. Depends on if samedelivery is active in customer fields
	 */
	public $allowDeliveryAddress;

	/**
	 * @var boolean $useLoginForm If the login form shall be shown
	 */
	public $useLoginForm;

	/**
	 * @var string $viewDataAttributes HTML-data attributes for the wrapping view div
	 */
	public $viewDataAttributes;

	/**
	 * Depends on registrationOptional and if the customer is logged in already
	 * @var boolean $useOptionalRegistration Indictes if the optional register checkbox shall be shown.
	 */
	public $useOptionalRegistration;

	/**
	 * @var boolean $userIsAdmin Indicates if an admin uses the form (adds fields for custom fields, platform user id)
	 */
	public $userIsAdmin;

	/**
	 * @var string $groupDropDownHtml HTML for the select to choose the customer group.
	 */
	public $groupDropDownHtml;

	/**
	 * @return ConfigboxModelCustomerForm
	 */
	public function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelCustomerForm');
	}

	function prepareTemplateVars() {

		if (empty($this->formType)) {
			$this->assignRef('formType', KRequest::getKeyword('form_type', 'profile'));
		}

		if (empty($this->customerData)) {
			$customerData = ConfigboxUserHelper::getUser(NULL, false, true);
			$this->assignRef('customerData', $customerData);
		}

		if (empty($this->customerFields)) {
			$customerFields = ConfigboxUserHelper::getUserFields($this->customerData->group_id);
			$this->assignRef('customerFields', $customerFields);
		}

		if (empty($this->fieldCssClasses)) {
			$cssClasses = $this->getFieldCssClasses($this->formType, $this->customerData);
			$this->assign('fieldCssClasses', $cssClasses);
		}

		if (empty($this->useCityLists)) {
			$useCityLists = ConfigboxCountryHelper::systemUsesCities();
			$this->assign('useCityLists', $useCityLists);
		}

		if ($this->useLoginForm === NULL) {
			$this->useLoginForm = true;
		}

		$isLoggedIn = KenedoPlatform::p()->isLoggedIn();
		if ($isLoggedIn) {
			$this->useLoginForm = false;
		}

		// Find the default value for optional registration
		if ($this->useOptionalRegistration === NULL) {

			// In case the customer is logged in, no registration obviously
			if ($isLoggedIn) {
				$this->useOptionalRegistration = false;
			}
			// For RFQ, registration is optional..
			elseif (in_array($this->formType, array('quotation'))) {
				$this->useOptionalRegistration = true;
			}
			// ..otherwise registration is mandatory
			else {
				$this->useOptionalRegistration = false;
			}

		}

		$isAdmin = ConfigboxPermissionHelper::canEditOrders();
		$this->assign('userIsAdmin', $isAdmin);

		$groupModel = KenedoModel::getModel('ConfigboxModelAdmincustomergroups');
		$groups = $groupModel->getRecords();
		$dropDown = KenedoHtml::getSelectField('group_id', $groups, $this->customerData->group_id);
		$this->assign('groupDropDownHtml', $dropDown);

		$this->assign('allowDeliveryAddress', $this->customerFields['samedelivery']->{'show_'.$this->formType});

		$this->assign('customerFieldsJson', json_encode($this->customerFields));

		$viewData = array(
			'customer-fields' => json_encode($this->customerFields),
			'view-url' => KLink::getRoute('index.php?option=com_configbox&view=customerform&formType='.$this->formType.'&format=raw', false),
		);
		$dataAttributes = '';
		foreach ($viewData as $key=>$value) {
			$dataAttributes .= ' data-'.$key.'="'.hsc($value).'"';
		}
		$this->assign('viewDataAttributes', $dataAttributes);

		$this->addViewCssClasses();

		return $this;

	}

	protected function getFieldCssClasses($formType, $customerData) {

		$model = $this->getDefaultModel();

		$customerFields = ConfigboxUserHelper::getUserFields($customerData->group_id);

		$cssClasses = array();

		foreach ($customerFields as $customerField) {
			// For convenience
			$fieldName = $customerField->field_name;

			// Add general CSS classes
			$classes = array();
			$classes[] = 'customer-field';
			$classes[] = 'customer-field-'.$fieldName;

			// Mark required fields
			if ($customerField->{'require_'.$formType}) {
				$classes[] = 'required-field';
			}

			// Make unused fields (fields that are not shown as configured in customer field settings)
			if (($customerField->{'show_'.$formType}) == false) {
				$classes[] = 'unused-field';
			}

			// Mark if delivery state dropdown got no states
			if ($fieldName == 'state' && $customerData->country) {
				$counties = $model->getStates($customerData->country);
				if (!$counties) {
					$classes[] = 'has-no-data';
				}
			}

			// Mark if billing state dropdown got no states
			if ($fieldName == 'billingstate' && $customerData->billingcountry) {
				$counties = $model->getStates($customerData->billingcountry);
				if (!$counties) {
					$classes[] = 'has-no-data';
				}
			}

			// Mark if billing county dropdown got no counties
			if ($fieldName == 'county_id') {

				// No state means we can't have any counties
				if (!$customerData->state) {
					$classes[] = 'has-no-data';
				}
				// Check if state got countes. If not, add the has-no-data flag
				else {
					$counties = $model->getCounties($customerData->state);

					if (!$counties) {
						$classes[] = 'has-no-data';
					}
				}

			}

			// Mark if billing county dropdown got no counties
			if ($fieldName == 'billingcounty_id') {

				// No state means we can't have any counties
				if (!$customerData->billingstate) {
					$classes[] = 'has-no-data';
				}
				// Check if state got countes. If not, add the has-no-data flag
				else {
					$counties = $model->getCounties($customerData->billingstate);

					if (!$counties) {
						$classes[] = 'has-no-data';
					}
				}

			}

			// Mark if delivery city dropdown got no cities
			if ($fieldName == 'city_id') {

				if ($customerData->county_id) {
					$cities = $model->getCities($customerData->county_id);
					if (!$cities) {
						$classes[] = 'has-no-data';
					}
				}
				else {
					$classes[] = 'uses-textfield-instead';
				}

			}

			// Mark if billing city dropdown got no cities
			if ($fieldName == 'billingcity_id') {

				if ($customerData->billingcounty_id) {
					$cities = $model->getCities($customerData->billingcounty_id);
					if (!$cities) {
						$classes[] = 'has-no-data';
					}
				}
				else {
					$classes[] = 'uses-textfield-instead';
				}

			}

			if ($fieldName == 'city' && $customerData->county_id) {
				$cities = $model->getCities($customerData->county_id);
				if ($cities) {
					$classes[] = 'uses-dropdown-instead';
				}
			}

			if ($fieldName == 'billingcity' && $customerData->billingcounty_id) {
				$cities = $model->getCities($customerData->billingcounty_id);
				if ($cities) {
					$classes[] = 'uses-dropdown-instead';
				}
			}

			// Finally add imploded array to cssClasses
			$cssClasses[$fieldName] = implode(' ', $classes);
		}

		return $cssClasses;

	}
	
}