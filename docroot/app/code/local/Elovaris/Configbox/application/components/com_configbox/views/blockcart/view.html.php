<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewBlockcart extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var KStorage Joomla module parameters if applicable
	 */
	public $params;

	/**
	 * @var object Cart data
	 * @see ConfigboxModelCart::getCartDetails
	 */
	public $cartDetails;

	/**
	 * @var string The property name for the cart total in cartDetails. Depends on B2B/B2C mode 'totalNet', 'totalGross'
	 */
	public $totalKey;

	/**
	 * @var string Same as totalKey for recurring price. Depends on B2B/B2C mode 'totalRecurringNet', 'totalRecurringGross'
	 * @see totalKey
	 */
	public $totalKeyRecurring;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars($tpl = null) {
			
		if (empty($this->params)) {
			$params = new KStorage();
			$this->assignRef('params',$params);
		}
		
		$model = KenedoModel::getModel('ConfigboxModelCart');
		if ($model->getId()) {
			$cartDetails = $model->getCartDetails();
		}
		else {
			$cartDetails = NULL;
		}

		
		$this->assignRef('cartDetails', $cartDetails);
		
		// Legacy var, remove in 2.7 or 3.0
		$this->assignRef('grandOrderDetails', $cartDetails);
		
		if ( ConfigboxPrices::showNetPrices() ) {
			$this->assign('totalKey', 'totalNet');
			$this->assign('totalKeyRecurring', 'totalRecurringNet');
		}
		else {
			$this->assign('totalKey', 'totalGross');
			$this->assign('totalKeyRecurring', 'totalRecurringGross');
		}

	}

}
