<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerUser extends KenedoController {

	/**
	 * @return NULL
	 */
	protected function getDefaultModel() {
		return NULL;
	}

	/**
	 * @return ConfigboxViewUser
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewUser');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

	function removeOrder() {
		
		// Get and sanitize record ids
		$cids = KRequest::getArray('cid');
		foreach ($cids as &$cid) {
			$cid = (int)$cid;
		}
		
		// Bounce if no ids where found
		if (count($cids) == 0) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('No orders chosen for removal.'));
			return false;
		}
		
		// Get the store ID
		$storeId = ConfigboxStoreHelper::getStoreId();
		
		// Find the records that the user should be able to reach
		$db = KenedoPlatform::getDb();
		$query = "SELECT `id`, `user_id`, `status` FROM `#__cbcheckout_order_records` WHERE `id` IN (".implode(',',$cids).")";
		if ($storeId != 1) {
			$query .= " AND store_id = ".(int)$storeId;
		}
		$db->setQuery($query);
		$removalItems = $db->loadObjectList('id');
		
		// Bounce if nothing is there
		if (!$removalItems) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('No orders chosen for removal.'));
			return false;
		}
		
		// Get the user's id
		$userId = ConfigboxUserHelper::getUserId();
		
		// Loop through the orders and see if the user can actually remove them
		foreach ($removalItems as $item) {
			if ($item->user_id != $userId) {
				$platformUserId = KenedoPlatform::p()->getUserId();
				if (ConfigboxPermissionHelper::canEditOrders($platformUserId) == false) {
					KenedoPlatform::p()->sendSystemMessage(KText::_('You cannot remove orders of other customers.'));
					return false;
				}
			}
			if (ConfigboxOrderHelper::isPermittedAction('removeOrderRecord', $item) == false) {
				$platformUserId = KenedoPlatform::p()->getUserId();
				if (ConfigboxPermissionHelper::canEditOrders($platformUserId) == false) {
					KenedoPlatform::p()->sendSystemMessage(KText::sprintf('You cannot remove order %s because if its status.',$item->id));
					return false;
				}
			}
				
		}
		
		// Get the ids of the order records
		$orderIds = array_keys($removalItems);
		
		// Get the model and try to remove them
		$model = KenedoModel::getModel('ConfigboxModelAdminorders');
		$success = $model->delete($orderIds);
		
		// Send a message if removal did not work
		if ($success == false) {
			$errors = $model->getErrors();
			foreach ($errors as $error) {
				KenedoPlatform::p()->sendSystemMessage($error);
			}
		}
		else {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Order removed.'));
		}
		
		// Redirect to the customer account page
		$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=user',false));
		$this->redirect();

		return true;
	}

	function store() {

		// Get the default model
		$model = KenedoModel::getModel('ConfigboxModelAdmincustomers');

		// Make a normalized data object from HTTP request data
		$data = $model->getDataFromRequest();

		// Prepare the data
		$model->prepareForStorage($data);

		// See what kind of customer form we deal with
		$formType = KRequest::getKeyword('form_type', 'profile');

		// Check if the data validates
		$checkResult = $model->validateData($data, $formType);

		// Abort and send feedback if validation fails
		if ($checkResult === false) {

			$response = new stdClass();
			$response->success = false;
			$response->errors = $model->getErrors();
			$response->validationIssues = $model->getValidationIsssues();
			echo json_encode($response);
			return;

		}

		// Get the data stored
		$success = $model->store($data);

		// Abort and send feedback if storage fails
		if ($success === false) {
			$response = new stdClass();
			$response->success = false;
			$response->errors = $model->getErrors();
			echo json_encode($response);
			return;
		}
		else {
			$response = new stdClass();
			$response->success = true;
			$response->message = KText::_('Record saved.');
			$response->errors = array();
			echo json_encode($response);
			return;
		}

	}
	
	function saveUser() {

		$context = KRequest::getKeyword('type');

		// Get the default model
		$model = KenedoModel::getModel('ConfigboxModelAdmincustomers');

		// Make a normalized data object from HTTP request data
		$data = $model->getDataFromRequest();

		// Prepare the data (auto-fill data like empty URL segment fields and similar)
		$model->prepareForStorage($data);

		// Check if the data validates
		$checkResult = $model->validateData($data, $context);

		// Abort and send feedback if validation fails
		if ($checkResult === false) {
			$errors = $model->getErrors();
			foreach ($errors as $error) {
				KenedoPlatform::p()->sendSystemMessage($error);
			}
			// Get the failure redirection URL
			$url = urldecode(KRequest::getString('return_failure'));
			$this->setRedirect( $url );
			$this->redirect();
			return;
		}

		// Get the data stored
		$success = $model->store($data);

		// Abort and send feedback if storage fails
		if ($success === false) {
			$errors = $model->getErrors();
			foreach ($errors as $error) {
				KenedoPlatform::p()->sendSystemMessage($error);
			}
			// Get the failure redirection URL
			$url = urldecode(KRequest::getString('return_failure'));
			$this->setRedirect( $url );
			$this->redirect();
			return;
		}

		// Store the order address and check if delivery and payment method are still valid
		if ($context != 'profile') {

			$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
			$orderId = $orderModel->getId();

			ConfigboxUserHelper::resetUserCache();
			$user = ConfigboxUserHelper::getUser();

			// Store the order address
			$success = ConfigboxUserHelper::setOrderAddress($orderId, $user);

			// Abort if a problem occurred
			if ($success == false) {
				KLog::log('Could not set order address.', 'error', KText::_('System error: Could not set order address.'));
				return;
			}

			// Get the order and check if delivery and payment is still valid
			$orderModel->unsetOrderRecord($orderId);
			$orderRecord = $orderModel->getOrderRecord($orderId);

			$noneSelected = ($orderRecord->delivery_id == 0);
			$nowInvalid = ($orderRecord->delivery_id && $orderModel->isValidDeliveryOption($orderRecord, $orderRecord->delivery_id) == false);

			if ($noneSelected || $nowInvalid) {

				$possibleOptions = $orderModel->getOrderRecordDeliveryOptions($orderRecord);
				if ($possibleOptions) {
					$orderModel->storeOrderRecordDeliveryOption($orderId, $possibleOptions[0]->id);
				} else {
					$orderModel->storeOrderRecordDeliveryOption($orderId, 0);
				}

			}

			// Now the payment
			$noneSelected = ($orderRecord->payment_id == 0);
			$nowInvalid = ($orderRecord->payment_id && $orderModel->isValidPaymentOption($orderRecord, $orderRecord->payment_id) == false);

			if ($noneSelected || $nowInvalid) {

				$orderModel->storeOrderRecordPaymentOption($orderId, 0);

				$possibleOptions = $orderModel->getOrderRecordPaymentOptions($orderRecord);
				if ($possibleOptions) {
					$orderModel->storeOrderRecordPaymentOption($orderId, $possibleOptions[0]->id);
				} else {
					$orderModel->storeOrderRecordPaymentOption($orderId, 0);
				}

			}
		}

		// Register the user if it wasn't done already
		if ($context != 'profile' && $context != 'quotation') {
			ConfigboxUserHelper::resetUserCache();
			$user = ConfigboxUserHelper::getUser();
			if ($user->platform_user_id == 0) {
				$model = KenedoModel::getModel('ConfigboxModelAdmincustomers');
				$model->registerPlatformUser($user->id);
			}
		}

		// Log the user in for checkout/quote/
		if ($context == 'checkout' or $context == 'saveorder' or $context = 'assistance') {
			ConfigboxUserHelper::resetUserCache();
			$user = ConfigboxUserHelper::getUser();
			//TODO: Handle login failures
			if (KenedoPlatform::p()->isLoggedIn() == false) {
				KenedoPlatform::p()->login($user->billingemail);
			}
		}

		// Send some feedback if requested by the info request form
		if (KRequest::getInt('success_message')) {
			KenedoPlatform::p()->sendSystemMessage( KText::_('Your data has been saved.') );
		}

		// Get the success redirection URL
		$url = urldecode(KRequest::getString('return_success'));
		$this->setRedirect($url);
		$this->redirect();
		
	}
	
	function loginUser() {
		
		// Store the old user id for later moving of orders and carts
		$oldUserId = ConfigboxUserHelper::getUserId();
		
		$email = KRequest::getString('username','');
		$password = KRequest::getString('password','');
		
		// Either email or username can be used
		if (!$email) {
			$email = KRequest::getString('email','');
		}

		$userId = ConfigboxUserHelper::getUserIdByEmail($email);

		// Authenticate the user
		$authenticated = ConfigboxUserHelper::authenticateUser($userId, $password);

		if ($authenticated == true) {
			// Login the user
			$response = ConfigboxUserHelper::loginUser($userId);
		}
		else {
			// Or put response to false
			$response = false;
		}

		if ($response == true) {
			
			// Set the order address as well
			$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
			$orderId = $orderModel->getId();
			if ($orderId) {
				ConfigboxUserHelper::setOrderAddress($orderId);
			}

			// Get the new user id
			$newUserId = ConfigboxUserHelper::getUserId();
			
			// Move the orders
			ConfigboxUserHelper::moveUserOrders($oldUserId, $newUserId);
			
			// Get the success redirection URL
			$url = urldecode(KRequest::getString('return_success'));
				
		}
		else {
			
			// Get the failure redirection URL
			$url = urldecode(KRequest::getString('return_failure'));
			
		}
		
		if (KRequest::getString('format') == 'json') {
			$jsonResponse = new stdClass();
			$jsonResponse->success = $response;
			if ($response == false) {
				$jsonResponse->errorMessage = KText::_('Login failed. Please check your email address and password.');
			}
			echo json_encode($jsonResponse);
		}
		else {

			// Send feedback
			if ($response == true) {
				$message = 'You have been successfully logged in.';
			}
			else {
				$message = 'Login failed. Please check your email address and password.';
			}

			KenedoPlatform::p()->sendSystemMessage(KText::_($message));

			// Redirect
			$this->setRedirect($url);
			$this->redirect();
			
		}
	
	
	}
	
	function registerUser() {
		
		$userId = ConfigboxUserHelper::getUserId();
		if ($userId == 0) {
			$userId = ConfigboxUserHelper::createNewUser();
			ConfigboxUserHelper::setUserId($userId);
		}
		
		$userData = ConfigboxUserHelper::getUser($userId);
		
		$userData->billingemail = KRequest::getString('email','');
		$userData->billingfirstname = KRequest::getString('firstname','');
		$userData->billinglastname = KRequest::getString('lastname','');
		
		$password 	= KRequest::getString('password');
		$password2 	= KRequest::getString('passwordconf');
		
		if ($password != $password2) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('The password confirmation does not match the password.'));
			$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=user&layout=register',false));
			$this->redirect();
		}
		
		$response = ConfigboxUserHelper::registerPlatformUser($userData, $password);
		
		if ($response == false) {
			$errorMessage = ConfigboxUserHelper::$error;
			KenedoPlatform::p()->sendSystemMessage($errorMessage);
			$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=user&layout=register',false));
			$this->redirect();
			return false;
		}
		else {
			
			$loginResponse = KenedoPlatform::p()->login($response->username);

			// Login the user
			if ($loginResponse == true) {
				KenedoPlatform::p()->sendSystemMessage(KText::_('Thank you for registering. You are now registered and logged in.'));
				$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=user', false, CONFIGBOX_SECURECHECKOUT));
				$this->redirect();
				
			}
			else {
				KenedoPlatform::p()->sendSystemMessage(KText::_('Could not login after registration.'));
				$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=user&layout=login', false, CONFIGBOX_SECURECHECKOUT));
				$this->redirect();
			}
			
		}

		return true;
		
	}
	
	function sendPasswordChangeVerificationCode() {
		
		$emailAddress = KRequest::getString('email','');
		$verificationCode = ConfigboxUserHelper::getPassword(16);
		
		$userId = ConfigboxUserHelper::getUserIdByEmail($emailAddress);
		
		if (!$userId) {
			$jsonResponse = new stdClass();
			$jsonResponse->success = false;
			$jsonResponse->errorMessage = KText::_('There is no customer account with that email address.');
			echo json_encode($jsonResponse);
			return;
		}
		
		KSession::set('passwordChangeUserId', $userId);
		KSession::set('passwordChangeVerificationCode', $verificationCode);
		
		$shopData = ConfigboxStoreHelper::getStoreRecord();
		
		$emailSubject = KText::sprintf('PASSWORD_CHANGE_VERIFICATION_CODE_EMAIL_SUBJECT', $shopData->shopname);
		$emailBody = KText::sprintf('PASSWORD_CHANGE_VERIFICATION_CODE_EMAIL_TEXT', $verificationCode);
		
		$emailView = KenedoView::getView('ConfigboxViewEmailtemplate');
		$emailView->prepareTemplateVars();
		$emailView->assign('emailContent', $emailBody);
		$emailBody = $emailView->getViewOutput('default');
		
		$email = new stdClass();
		$email->toEmail		= $emailAddress;
		$email->fromEmail	= $shopData->shopemailsales;
		$email->fromName	= $shopData->shopname;
		$email->subject		= $emailSubject;
		$email->body 		= $emailBody;
		$email->attachments	= array();
		$email->cc			= NULL;
		$email->bcc			= NULL;
		
		$dispatchResponse = KenedoPlatform::p()->sendEmail($email->fromEmail, $email->fromName, $email->toEmail, $email->subject, $email->body, true, $email->cc, $email->bcc, $email->attachments);
		
		$jsonResponse = new stdClass();
		
		if ($dispatchResponse !== true) {
			
			$jsonResponse->success = false;
			$jsonResponse->errorMessage = KText::_('We could not send you a verification code because the email dispatch failed. Please contact us to solve this issue.');
			
			// Log error message to errors
			KLog::log('Password change verification code email could not be sent. Recipient was "'.$email->toEmail.'". Sender email was "'.$email->fromEmail.'". Email body char count was "'.mb_strlen($email->body).'".', 'error');
		}
		else {
			$jsonResponse->success = true;
		}
		
		echo json_encode($jsonResponse);
		
	}
	
	function changePasswordWithCode() {
		
		// Prepare the response object
		$jsonResponse = new stdClass();
		$jsonResponse->errors = array();
		
		// Get the input
		$verificationCode = trim(KRequest::getString('code'));
		$password = trim(KRequest::getVar('password'));

		// Check if the verification code was supplied
		if ($verificationCode == '') {
			$jsonResponse->success = false;
			$jsonResponse->errors[] = array(
				'fieldName'=>'verification_code',
				'code'=>'no_code',
				'message'=>KText::_('Please enter the verification code you received by email.'),
			);
		}
		// Check if the codes match
		elseif ($verificationCode != KSession::get('passwordChangeVerificationCode')) {
			$jsonResponse->success = false;

			$jsonResponse->errors[] = array(
				'fieldName'=>'verification_code',
				'code'=>'code_mismatch',
				'message'=>KText::_('The verification code is not correct. Please check and try again. You can also request a new code.'),
			);

		}

		// Check if the new password was supplied
		if ($password == '') {
			$jsonResponse->success = false;
			$jsonResponse->errors[] = array(
				'fieldName'=>'new_password',
				'code'=>'no_password',
				'message'=>KText::_('Please enter a new password.'),
			);
		}
		// Check if the new password is ok
		elseif( KenedoPlatform::p()->passwordMeetsStandards($password) == false ) {
			$jsonResponse->success = false;
			$jsonResponse->errors[] = array(
					'fieldName'=>'new_password',
					'code'=>'bad_password',
					'message'=>KenedoPlatform::p()->getPasswordStandardsText(),
			);
		}

		// Stop if we got problems so far
		if (count($jsonResponse->errors)) {
			echo json_encode($jsonResponse);
			return;
		}
		
		// Get the user ID from the verification info
		$userId = KSession::get('passwordChangeUserId');
		
		// Change the password
		$success = ConfigboxUserHelper::changeUserPassword($userId, $password);
		
		// Do the final setup of the response
		if ($success == true) {

			// Log the user in if requested
			if (KRequest::getInt('login')) {				
				$user = ConfigboxUserHelper::getUser($userId, false, false);

				$isAuthenticated = ConfigboxUserHelper::authenticateUser($user->id, $password);

				if ($isAuthenticated) {
					ConfigboxUserHelper::loginUser($user->id);
				}
			}
			
			$jsonResponse->success = true;
			
			KSession::delete('passwordChangeUserId');
			KSession::delete('passwordChangeVerificationCode');
			
		}
		else {
			$jsonResponse->success = false;
			$jsonResponse->errors[] = array(
					'fieldName'=>'',
					'code'=>'change_password_failure',
					'message'=>ConfigboxUserHelper::$error,
			);
		}
		
		// Do the response
		echo json_encode($jsonResponse);
		return;
		
	}

}
