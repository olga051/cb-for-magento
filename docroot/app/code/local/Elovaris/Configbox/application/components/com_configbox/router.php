<?php
defined('_JEXEC') or die();

function ConfigboxBuildRoute( &$query ) {

	if (!is_file(dirname(__FILE__).'/external/kenedo/init.php')) {
		return array();
	}

	require_once(dirname(__FILE__).'/external/kenedo/init.php');

	// Init segments
	$segments = array();

	if (!empty($query['lang'])) {
		$langTag = $query['lang'];
	}
	else {
		$langTag = KenedoPlatform::p()->getLanguageTag();
	}

	// For edit order routes
	if (isset($query['controller']) && $query['controller'] == 'adminorders') {

		if (!empty($query['cid'])) {
			$segments[] = $query['cid'][0];
			unset($query['cid'],$query['task'],$query['controller']);
			return $segments;
		}
		elseif(!empty($query['Itemid'])) {
			unset($query['controller']);
		}
	}

	// For IPN controller tasks
	if (!empty($query['controller']) && $query['controller'] == 'ipn') {

		$segments[] = 'ipn';

		if ($query['task'] == 'processipn') {
			$segments[] = 'processipn';
			$segments[] = $query['connector_name'];
			unset($query['connector_name']);
		}
		else {
			KLog::log('Unknown task for IPN controller detected. Task was "'.$query['task'].'"','error');
			return array();
		}

		unset($query['controller'], $query['task'], $query['system'], $query['Itemid']);
		return $segments;

	}


	// Nothing goes without view
	if (!isset($query['view'])) return $segments;

	// Legacy, remove with 4.0
	if ($query['view'] == 'grandorder') {
		$query['view'] = 'cart';
	}

	// Legacy, remove with 4.0
	if ($query['view'] == 'category') {
		$query['view'] = 'configuratorpage';
	}

	// Legacy, remove with 4.0
	if ($query['view'] == 'products') {
		$query['view'] = 'productlisting';
	}

	// Legacy, remove with 4.0 - Change cat_id to page_id
	if ($query['view'] == 'configuratorpage' && !empty($query['cat_id'])) {

		$ref = (!empty($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
		$msg = 'A link with an outdated URL for configurator pages was found.';
		if ($ref) {
			$msg .= ' The link was found on page "'.$ref.'". Most likely an article, module or custom Configbox template. cat_id should be replaced by page_id. The link you see on the page may be processed. Check the source of the link, where you will see the URL parameters.';
		}
		$msg .= ' We keep supporting the old link until version 4 only, please change the link as soon as you can.';
		KLog::log($msg, 'deprecated');

		$query['page_id'] = $query['cat_id'];
		unset($query['cat_id']);

	}

	switch ($query['view']) {

		case 'checkout':

			$newQuery = array('option'=>'com_configbox');

			// Fallback, remove in 2.7
			if (!empty($query['cborder_id'])) {
				$newQuery['order_id'] = $query['order_id'];
			}

			if (!empty($query['order_id'])) {
				$newQuery['order_id'] = $query['order_id'];
			}
			$query = $newQuery;

			return array('checkout');
			break;

		case 'terms':

			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=terms', $langTag);
			if ($id) {
				$query['Itemid'] = $id;
				unset($query['view']);
				return array();
			}
			else {
				unset($query['view']);
				return array('terms');
			}
			break;

		case 'refundpolicy':
			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=refundpolicy', $langTag);
			if ($id) {
				$query['Itemid'] = $id;
				unset($query['view']);
				return array();
			}
			else {
				unset($query['view']);
				return array('refundpolicy');
			}
			break;

		case 'user':

			if (!empty($query['layout']) && $query['layout'] == 'editprofile') {
				$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=user&layout=editprofile', $langTag);
				if ($id) {
					unset($query['layout']);
					return array();
				}
				else {
					return array();
				}
			}

			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=user', $langTag);

			if ($id) {
				$query['Itemid'] = $id;
				unset($query['view']);
				return array();
			}
			else {
				unset($query['view']);
				return array('user');
			}
			break;

		case 'userorder':
			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=user', $langTag);
			if ($id) {
				$query['Itemid'] = $id;
				unset($query['view']);
				if (!empty($query['order_id'])) {
					$segments[] = 'orders';
					$segments[] = $query['order_id'];
					unset($query['order_id']);
					return $segments;
				}
				return array();
			}
			else {
				unset($query['view'], $query['Itemid']);
				return array('userorder');
			}
			break;

		case 'configuratorpage':

			// Get prod and page id
			$prodId = (!empty($query['prod_id'])) ? intval($query['prod_id']) : 0;
			$pageId = (!empty($query['page_id'])) ? intval($query['page_id']) : 0;

			// Case product id and page id is set
			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=configuratorpage&prod_id='.$prodId.'&page_id='.$pageId, $langTag);
			if ($id) {
				$query['option'] = 'com_configbox';
				$query['Itemid'] = $id;
				unset($query['view'], $query['page_id'], $query['prod_id']);
				return array();
			}

			// Case product id is set and page id is not
			// Missing page id is fine, it will be figured out by finding the first page of the product
			if ($pageId == 0) {
				$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=configuratorpage&prod_id='.$prodId.'&page_id=0', $langTag);
				if ($id) {
					$query['option'] = 'com_configbox';
					$query['Itemid'] = $id;
					unset($query['view'], $query['page_id'], $query['prod_id']);
					return array();
				}
			}

			// Ok, from now on we may need the labels for product and page
			$prodLabel = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 17, $prodId, $langTag);
			$pageLabel = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 18, $pageId, $langTag);

			// Find a matching product view menu item that is a parent menu item of the current menu item
			$id = KenedoRouterHelper::getParentProductMenuItemId($prodId, $langTag);
			if ($id) {
				$query['option'] = 'com_configbox';
				$query['Itemid'] = $id;
				unset($query['view'], $query['page_id'], $query['prod_id']);
				return array($pageLabel);
			}

			// Find a any matching product menu item
			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=product&prod_id='.$prodId, $langTag);
			if ($id) {
				$query['option'] = 'com_configbox';
				$query['Itemid'] = $id;
				unset($query['view'], $query['page_id'], $query['prod_id']);
				return array($pageLabel);
			}

			// Get product listings the product belongs to
			$listingIds = KenedoRouterHelper::getListingIds($prodId);

			// Check if there are any parent product listing menu items in the current path - and use the item id in case
			if ($listingIds) {

				$itemId = KenedoRouterHelper::getParentListingMenuItemId($listingIds, $langTag);

				if ($itemId) {
					$query['option'] = 'com_configbox';
					$query['Itemid'] = $id;
					unset($query['view'], $query['page_id'], $query['prod_id']);
					return array($prodLabel,$pageLabel);
				}

			}

			// Check for product listing menu items where the product belongs to, no matter if in path or not
			foreach ($listingIds as $listingId) {
				$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=productlisting&listing_id='.$listingId, $langTag);
				if ($id) {
					$query['option'] = 'com_configbox';
					$query['Itemid'] = $id;
					unset($query['view'], $query['page_id'], $query['prod_id']);
					return array($prodLabel, $pageLabel);
				}
			}

			// Find a product listing menu item with no specific product listing id in path
			$id = KenedoRouterHelper::getParentListingMenuItemId(array('0'));
			if ($id) {
				$query['option'] = 'com_configbox';
				$query['Itemid'] = $id;
				unset($query['view'], $query['page_id'], $query['prod_id']);
				return array($prodLabel, $pageLabel);
			}

			// Try with general product listings no matter if in path or not
			$searchLink = 'index.php?option=com_configbox&view=productlisting&listing_id=0';

			// Check for a matching menu item
			$id = KenedoRouterHelper::getItemIdByLink($searchLink);

			if ($id) {
				$query['option'] = 'com_configbox';
				$query['Itemid'] = $id;
				unset($query['view'], $query['page_id'], $query['prod_id']);
				return array($prodLabel, $pageLabel);
			}

			// If no product listing link is found, try pages
			$searchLinks = array();
			// Item with both params set
			$searchLinks[1] = 'index.php?option=com_configbox&view=configuratorpage&prod_id='.$prodId.'&page_id='.$pageId;
			// Item with only prod_id param set
			$searchLinks[2] = 'index.php?option=com_configbox&view=configuratorpage&prod_id='.$prodId.'&page_id=0';
			// Item with both params set to zero
			$searchLinks[3] = 'index.php?option=com_configbox&view=configuratorpage&prod_id=0&page_id=0';
			// Item with no params, empty params were not set in earlier Joomla versions
			$searchLinks[4] = 'index.php?option=com_configbox&view=configuratorpage';

			foreach ($searchLinks as $key => $searchLink) {

				$id = KenedoRouterHelper::getItemIdByLink($searchLink, $langTag);

				if ($id) {
					$query['option'] = 'com_configbox';
					$query['Itemid'] = $id;
					unset($query['view'], $query['page_id'], $query['prod_id']);

					switch ($key) {

						case 1: return array();
						case 2:
							// Special case: if go for the first page in a prod/nopage menu item, do not add the page label (double content)
							// The missing page label is dealt with in the configurator page view
							if (KenedoRouterHelper::isFirstPage($prodId,$pageId)) {
								return array();
							}
							else {
								return array($pageLabel);
							}
						default: return array($prodLabel,$pageLabel);
					}

				}
			}

			// This should only happen, if no matching menu items are found anywhere
			$segments = array($prodLabel,$pageLabel);
			unset ($query['view'],$query['prod_id'],$query['page_id'],$query['Itemid']);
			return $segments;


			break;

		case 'product':

			$prodId = (!empty($query['prod_id'])) ? intval($query['prod_id']) : 0;

			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=product&prod_id='.$prodId);

			// Perfect match, thank you very much. Return the option and menu item id and good bye.
			if ($id) {
				$query['option'] = 'com_configbox';
				$query['Itemid'] = $id;
				unset($query['view'], $query['page_id'], $query['prod_id']);
				return array();
			}

			// Get product listings the product belongs to
			$listingIds = KenedoRouterHelper::getListingIds($prodId);

			// See if we currently are on a product listing page
			$currentListingId = KRequest::getInt('listing_id');

			// If so use that listing if the product belongs to it
			if ($currentListingId && in_array($currentListingId, $listingIds)) {
				$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=productlisting&listing_id='.$currentListingId);
				if ($id) {
					$query['option'] = 'com_configbox';
					$query['Itemid'] = $id;
					unset($query['view'], $query['page_id'], $query['prod_id'], $query['listing_id']);
					$prodLabel = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 17, $prodId, $langTag);
					return array($prodLabel);
				}
			}

			// Check for product listing menu items where the product belongs to
			if ($listingIds) {
				foreach ($listingIds as $listingId) {
					$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=productlisting&listing_id='.$listingId);
					if ($id) {
						$query['option'] = 'com_configbox';
						$query['Itemid'] = $id;
						unset($query['view'], $query['page_id'], $query['prod_id'], $query['listing_id']);
						$prodLabel = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 17, $prodId, $langTag);
						return array($prodLabel);
					}
				}
			}

			// We're getting desperate to find something good here, let's try product listing links without a set listing id
			$searchLinks = array(
				'index.php?option=com_configbox&view=productlisting&listing_id=0',
				'index.php?option=com_configbox&view=productlisting',
			);

			foreach ($searchLinks as $searchLink) {
				// Check for a matching menu item
				$id = KenedoRouterHelper::getItemIdByLink($searchLink);
				if ($id) {
					$query['option'] = 'com_configbox';
					$query['Itemid'] = $id;
					unset($query['view'], $query['page_id'], $query['prod_id'], $query['listing_id']);
					$prodLabel = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 17, $prodId, $langTag);
					return array($prodLabel);
				}
			}

			$prodLabel = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 17, $prodId, $langTag);

			if ($prodLabel) {
				// This is sad, that poor little product with nowhere to go. We wish it all the best and return the product label.
				unset ($query['view'], $query['prod_id']);
				return array($prodLabel);
			}
			else {
				// I'm hearth-broken now. Not even a product label to send the little product off! Where has the world come to?
				return array();
			}

			break;

		case 'cart':

			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=cart');

			if ($id) {
				unset($query['view']);
				$query['Itemid'] = $id;
			}
			return array();

			break;

		case 'productlisting':

			$listingId = ( empty($query['listing_id']) ) ? 0 : (int)$query['listing_id'];

			$id = KenedoRouterHelper::getItemIdByLink('index.php?option=com_configbox&view=productlisting&listing_id='.$listingId);

			if ($id) {
				unset($query['view'],$query['listing_id']);
				$query['Itemid'] = $id;
			}

			return array();

			break;

		default:
			return array();
			break;
	}
}

function ConfigboxParseRoute( $segments ) {

	if (!is_file(dirname(__FILE__).'/external/kenedo/init.php')) {
		return array();
	}

	require_once(dirname(__FILE__).'/external/kenedo/init.php');

	// For IPN controller routes
	if ($segments[0] == 'ipn') {
		$vars = array(
			'controller' => 'ipn',
			'task' => $segments[1],
			'connector_name' => $segments[2],
		);
		return $vars;
	}

	// See if Joomla already found a match in the menu for the incoming URI request
	$activeItemId = KenedoPlatform::p()->getActiveMenuItemId();
	if ($activeItemId) {

		// Get the menu item record
		$query = "SELECT `link` FROM `#__menu` WHERE `id` = ".(int)$activeItemId;
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$activeItem = $db->loadObject();

		// Parse the menu item's link and put it into the record (this way
		$queryString = parse_url($activeItem->link,PHP_URL_QUERY);
		$query = array();
		parse_str($queryString,$query);
		$activeItem->query = $query;

	}
	else {
		$activeItem = NULL;
	}


	// Get the number of segments we're dealing with
	$segmentCount 	= count($segments);

	// Prepare the vars array we are supposed to send back
	$vars 			= array();

	// J1.5 somehow replaces the first dash with a colon, we change it back to a dash
	foreach ($segments as $key=>$segment) {
		$segments[$key] = str_ireplace(':', '-', $segments[$key]);
	}

	// Let's deal with admin order edit views right away
	if ($activeItem && !empty($activeItem->query['view']) && $activeItem->query['view'] == 'adminorders') {
		if (is_numeric($segments[0])) {
			$vars['controller'] = 'adminorders';
			$vars['view'] = 'adminorders';
			$vars['task'] = 'edit';
			$vars['cid'][0] = $segments[0];
			return $vars;
		}
	}

	// Let's deal with admin order edit views right away
	if ($activeItem && !empty($activeItem->query['view']) && $activeItem->query['view'] == 'user') {
		if ($segments[0] == 'orders') {
			$vars['view'] = 'userorder';
			$vars['order_id'] = $segments[1];
			return $vars;
		}
	}

	if ($activeItem == null && $segmentCount == 1 && $segments[0] == 'checkout') {
		$vars['view'] = $segments[0];
		return $vars;
	}

	if ($activeItem == null && $segmentCount == 1 && $segments[0] == 'terms') {
		$vars['view'] = $segments[0];
		return $vars;
	}

	if ($activeItem == null && $segmentCount == 1 && $segments[0] == 'refundpolicy') {
		$vars['view'] = $segments[0];
		return $vars;
	}

	if ($activeItem == null && $segmentCount == 1 && $segments[0] == 'user') {
		$vars['view'] = $segments[0];
		return $vars;
	}

	if ($activeItem == null && $segmentCount == 1 && $segments[0] == 'userorder') {
		$vars['view'] = $segments[0];
		return $vars;
	}



	// Check if we should orient ourselves by the active menu item
	if ($activeItem && $activeItem->query['option'] == 'com_configbox') {
		$orientationByMenuItem = true;
	}
	else {
		$orientationByMenuItem = false;
	}

	$segmentMatching = array();

	// Now we figure out what segments stand for which actual var
	// A segment matching like array('prod_id','page_id') means that the first segment stands
	// for the param prod_id, the second for page_id

	// Here we may also override the view if the menu item is wrong (or was mislead on purpose)

	if ($orientationByMenuItem) {

		if ($activeItem->query['view'] == 'grandorder') {
			$activeItem->query['view'] = 'cart';
		}
		if ($activeItem->query['view'] == 'category') {
			$activeItem->query['view'] = 'configuratorpage';
		}
		if ($activeItem->query['view'] == 'products') {
			$activeItem->query['view'] = 'productlisting';
		}

		switch ($activeItem->query['view']) {

			case 'productlisting':

				if ($segmentCount == 1) {
					$view = 'product';
					$segmentMatching = array('prod_id');
				}
				elseif ($segmentCount == 2) {
					$view = 'configuratorpage';
					$segmentMatching = array('prod_id','page_id');
				}
				else {
					$view = 'productlisting';
				}
				break;

			case 'product':

				// If we just got 1 segment, Joomla must think we deal with a product view
				// We're all good with that, we change the view and know that the segment stands for the page id
				if ($segmentCount == 1) {
					$view = 'configuratorpage';
					$vars['prod_id'] = $activeItem->query['prod_id'];
					$segmentMatching = array('page_id');
				}
				// If there are more segments (on 0 segments the whole function would not have been called)
				// We got no idea what we're dealing here and leave the segments alone
				else {
					$view = 'product';
					$segmentMatching = array();
				}
				break;

			case 'configuratorpage':

				$view = 'configuratorpage';
				// If we got no prod_id from the menu item, we assume that the first segment stands for the prod_id
				if (empty($activeItem->query['prod_id'])) {
					$segmentMatching[] = 'prod_id';
				}
				// Ok, so the menu item provided a prod_id, so let's see what else is going on
				else {
					$vars['prod_id'] = $activeItem->query['prod_id'];
				}
				// If we got no page_id from the menu item, we assume that the next segment stands for the prod_id (see above about the prod_id)
				if (empty($activeItem->query['page_id'])) {
					$segmentMatching[] = 'page_id';
				}
				// Ok, so the menu item provided a page_id, so no more lookups in the segments (a situation where the page id is set 
				// in the menu params, but no prod_id just does not happen
				else {
					$vars['page_id'] = $activeItem->query['page_id'];
				}
				break;

			case 'cart':

				$view = 'cart';
				$segmentMatching = array();
				break;

			default:

				// No matching view, contemplate about going to panic
				$view = NULL;
				$segmentMatching = array();
				break;

		}

	}
	else {

		switch (count($segments)) {

			case 1:

				$view = 'product';
				$segmentMatching = array('prod_id');
				break;

			case 2:

				$view = 'configuratorpage';
				$segmentMatching = array('prod_id','page_id');
				break;

			default:

				// No matching view, contemplate about going to panic
				$view = NULL;
				$segmentMatching = array();
				break;
		}

	}

	// Now we got all our segment matching done, let's fill those vars
	// We don't actually have a reason to do this per view, but it might just be necessary in the future
	switch ($view) {

		case 'configuratorpage':

			foreach ($segmentMatching as $segmentKey=>$varKey) {

				if ($varKey == 'prod_id') {
					// Get the prod id by segment
					$vars[$varKey] = KenedoRouterHelper::getProdId($segments[$segmentKey]);
					// Here we carry over the segments because we do some old label redirects in the page view.html file
					$_GET['prodlabel'] = $_POST['prodlabel'] = $_REQUEST['prodlabel'] = $segments[$segmentKey];
				}
				if ($varKey == 'page_id') {
					// Get the page id by segment
					$vars[$varKey] = KenedoRouterHelper::getPageId($vars['prod_id'],$segments[$segmentKey]);
					// Here we carry over the segments because we do some old label redirects in the page view.html file
					$_GET['pagelabel'] = $_POST['pagelabel'] = $_REQUEST['pagelabel'] = $segments[$segmentKey];
				}

			}

			break;

		default:

			foreach ($segmentMatching as $segmentKey=>$varKey) {
				if ($varKey == 'prod_id') {
					$vars[$varKey] = KenedoRouterHelper::getProdId($segments[$segmentKey]);
					$_GET['prodlabel'] = $_POST['prodlabel'] = $_REQUEST['prodlabel'] = $segments[$segmentKey];
				}
			}
			break;

	}

	// Deal with the module assignment trick
	if ($view == 'configuratorpage') {
		$id = KenedoRouterHelper::getFakedConfigurationItemId($vars['prod_id'],$vars['page_id']);
		if ($id) {
			/** @noinspection PhpDeprecationInspection */
			JRequest::setVar('Itemid',$id);
			KRequest::setVar('Itemid',$id);
		}
	}

	// Last but not least, add the view name
	$vars['view'] = $view;

	return $vars;

}
