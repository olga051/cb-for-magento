<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */
?>
<div id="<?php echo $this->element->cssId;?>" class="<?php echo $this->element->getCssClasses();?>">

	<?php if (ConfigboxPermissionHelper::canQuickEdit()) echo ConfigboxQuickeditHelper::renderElementButtons($this->element);?>
	
	<div class="heading">
					
		<h2 class="element-title"><?php echo hsc($this->element->title);?></h2>
		
		<?php if (!empty($this->element->description) && $this->element->desc_display_method == 1) { ?>
			<div class="element-description"><?php echo $this->element->description;?></div>
		<?php } ?>
		
		<?php if (!empty($this->element->description) && $this->element->desc_display_method == 2) { ?>
			<div class="element-description-tooltip"><?php echo KenedoHtml::getTooltip('<a class="fa fa-info-circle"></a>', $this->element->description);?></div>
		<?php } ?>
		
	</div>
	<div class="inputfield">
	
		<?php if ($this->element->el_image) { ?>
			<img class="<?php echo hsc($this->element->elementImageCssClasses);?>" src="<?php echo $this->element->elementImageSrc;?>" alt="<?php echo hsc($this->element->title);?>"<?php echo $this->element->elementImagePreloadAttributes;?> />
		<?php } ?>
		
		<input class="configbox-control configbox-control-textfield" id="element-<?php echo (int)$this->element->id;?>" value="<?php echo hsc($this->element->getRawValue());?>" type="hidden" name="element-<?php echo (int)$this->element->id;?>" />
		
		<div class="configbox-widget-wrapper configbox-widget-wrapper-<?php echo hsc($this->element->widget);?>">
			<?php require( KPATH_DIR_CB.'/views/element/tmpl/widgets/'. $this->element->widget. '.php'); ?>
		</div>
		
		<?php if (ConfigboxPermissionHelper::canSeePricing()) { ?>
			<span class="element-price-display">
				<span class="element-price-wrapper" <?php echo ($this->element->getPrice() == 0) ? 'style="display:none"':'';?>>
					<span class="element-price element-price-<?php echo $this->element->id;?>"> <?php echo cbprice($this->element->getPrice() );?></span> 
					<span class="element-price-label element-price-label-<?php echo $this->element->id;?>"><?php echo hsc($this->product->priceLabel);?></span>
				</span>
				<span class="element-price-recurring-wrapper" <?php echo ($this->element->getPriceRecurring() == 0) ? 'style="display:none"':'';?>>
					<span class="element-price-recurring element-price-recurring-<?php echo (int)$this->element->id;?>"><?php echo cbprice($this->element->getPriceRecurring() );?></span>
					<span class="element-price-recurring-label element-price-recurring-label-<?php echo (int)$this->element->id;?>"><?php echo hsc($this->product->priceLabelRecurring);?></span>
				</span>
			</span>
		<?php } ?>
		
		<span class="position-loading-symbol"><span class="wheel"></span></span>
		<span class="validation-feedback"></span>
		
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
