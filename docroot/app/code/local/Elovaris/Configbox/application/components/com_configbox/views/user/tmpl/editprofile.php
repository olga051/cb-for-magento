<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewUser */
?>
<div id="com_configbox">
<div id="view-user">
<div id="layout-editprofile">

	<h1 class="componentheading"><?php echo KText::_('Your delivery and billing information')?></h1>

	<?php echo $this->customerFormHtml;?>

	<div id="buttons">

		<a rel="nofollow" href="<?php echo $this->urlCustomerAccount;?>" class="back navbutton-medium">
			<span class="nav-center"><?php echo KText::_('Close');?></span>
		</a>

		<a rel="nofollow" class="navbutton-medium next trigger-store-customer-form" data-back-url="<?php echo $this->urlCustomerAccount;?>">
			<span class="nav-center"><?php echo KText::_('Save');?></span>
		</a>

	</div>

</div>
</div>
</div>
