<?php
class ConfigboxProductImageHelper {
	
	protected static $productImages = array();

	static function getVisualizationData($cartPositionId) {

		// Get the selections
		$configuration = ConfigboxConfiguration::getInstance($cartPositionId);
		$selections = $configuration->getSelections();

		// Extract the assignment IDs
		$xrefIds = array();
		foreach ($selections as $selection) {
			if ($selection['element_option_xref_id']) {
				$xrefIds[] = $selection['element_option_xref_id'];
			}
		}

		// Stop if there aren't any
		if (count($xrefIds) == 0) {
			return array();
		}

		$signature = serialize($xrefIds);

		if (empty(self::$productImages[$signature])) {
			// Get the visualization infos out
			$db = KenedoPlatform::getDb();
			$query = "
			SELECT xref.element_id, xref.option_id, xref.visualization_image
			FROM `#__configbox_xref_element_option` AS xref
			WHERE xref.id IN (".implode(',', $xrefIds).") AND xref.visualization_image != ''
			ORDER BY xref.visualization_stacking
			";
			$db->setQuery($query);
			self::$productImages[$signature] = $db->loadObjectList('element_id');
		}

		return self::$productImages[$signature];

	}
	
	static function getProductImageHtml($cartPositionDetails, $width = NULL, $height = NULL) {
		
		if ($width === NULL && $height === NULL) {
			$width = $cartPositionDetails->productData->opt_image_x;
			$height = $cartPositionDetails->productData->opt_image_y;
		}
		elseif($width !== NULL && $height === NULL) {
			$ratio = $cartPositionDetails->productData->opt_image_x / $width;
			if ($ratio == 0) {
				$height = 0;
			}
			else {
				$height = intval($cartPositionDetails->productData->opt_image_y / $ratio);
			}
		}
		elseif($width === NULL && $height !== NULL) {
			$ratio = $cartPositionDetails->productData->opt_image_y / $height;
			if ($ratio == 0) {
				$width = 0;
			}
			else {
				$width = intval($cartPositionDetails->productData->opt_image_x / $ratio);
			}
		}
		else {
			
			$newWidth = $origWidth = $cartPositionDetails->productData->opt_image_x;
			$newHeight = $origHeight = $cartPositionDetails->productData->opt_image_y;
			
			$maxWidth = $width;
			$maxHeight = $height;
			
			if ($origWidth > $maxWidth) {
				$newWidth = $maxWidth;
				
				$ratio = $origHeight / $origWidth;
				$newHeight = $newWidth * $ratio;
				
			}
			
			if ($newHeight > $maxHeight) {
				$newHeight = $maxHeight;
				
				$ratio = $origWidth / $origHeight;
				$newWidth = $newHeight * $ratio;
				
			}
			
			$width = $newWidth;
			$height = $newHeight;
			
		}
		
		$images = self::getVisualizationData($cartPositionDetails->id);

		ob_start();
	
		if (self::hasProductImage($cartPositionDetails)) { ?>
			
			<div class="cb-product-visualization" style="position:relative; width: <?php echo intval($width);?>px; height: <?php echo intval($height);?>px">
				<?php if (!empty($cartPositionDetails->productData->baseimage)) { ?>
					<div style="display:block;position:absolute;top:0px;left:0px; width: <?php echo intval($width); ?>px; height: <?php echo intval($height);?>px">
						<img src="<?php echo CONFIGBOX_URL_VIS_PRODUCT_BASE_IMAGES.'/'.$cartPositionDetails->productData->baseimage;?>" width="<?php echo intval($width);?>px" height="<?php echo intval($height);?>px" alt="Visualization" style="display:block" />
					</div>
				<?php } ?>
				
				<?php foreach ($images as $image) { ?>
					<div style="display:block;position:absolute;top:0px;left:0px; width: <?php echo intval($width);?>px; height: <?php echo intval($height);?>px">
						<img src="<?php echo CONFIGBOX_URL_VIS_OPTION_IMAGES.'/'.$image->visualization_image;?>" width="<?php echo intval($width);?>px" height="<?php echo intval($height);?>px" alt="Visualization" />
					</div>
				<?php } ?>
			</div>
			<?php 
		}
		
			
		$output = ob_get_clean();
		
		return $output;
		
	}

	/**
	 * @param $cartPositionDetails
	 * @param string $destination
	 * @return bool|string|NULL false on failure, NULL if no product image, path to image on success
	 */
	static function getMergedProductImage($cartPositionDetails, $destination = '') {
		
		if (empty($destination)) {
			$destinationFile = KenedoPlatform::p()->getTmpPath().DS.uniqid().'.png';
		}
		elseif (is_dir($destination)) {
			$destinationFile = $destination.DS.uniqid().'.png';
		}
		elseif (is_dir(dirname($destination))) {
			$destinationFile = $destination;
		}
		else {
			$destinationFile = KenedoPlatform::p()->getTmpPath().DS.uniqid().'.png';
		}
				
		if (self::hasProductImage($cartPositionDetails) == false) {
			return NULL;
		}
		else {
				
			if (!empty($cartPositionDetails->productData->baseimage)) {
				
				$path = CONFIGBOX_DIR_VIS_PRODUCT_BASE_IMAGES .DS. $cartPositionDetails->productData->baseimage;
				if (KenedoFileHelper::getExtension($path) == 'png') {
					$base = imagecreatefrompng($path);
				}
				elseif (KenedoFileHelper::getExtension($path) == 'jpg' or KenedoFileHelper::getExtension($path) == 'jpeg') {
					$base = imagecreatefromjpeg($path);
				}
				else {
					$base = imagecreate($cartPositionDetails->productData->opt_image_x, $cartPositionDetails->productData->opt_image_y);
				}
				
				imagealphablending($base, true);
				imagesavealpha($base, true);
			}
			
			$images = self::getVisualizationData($cartPositionDetails->id);
			
			// Shortcut for the case if just one image is there (and no base image) - this helps because imagealphablending and imagesavealpha goes wrong otherwise
			if (count($images) == 1 && !isset($base)) {
				$sourceFile = CONFIGBOX_DIR_VIS_OPTION_IMAGES .DS. $images[key($images)]->visualization_image;
				$success = copy($sourceFile, $destinationFile);
				if ($success) {
					return $destinationFile;
				}
				else {
					return false;
				}
			}
			
			foreach ($images as $image) {
				
				if (!isset($base)) {
					
					$path = CONFIGBOX_DIR_VIS_OPTION_IMAGES .DS. $image->visualization_image;
					if (KenedoFileHelper::getExtension($path) == 'png') {
						$base = imagecreatefrompng($path);
					}
					elseif (KenedoFileHelper::getExtension($path) == 'jpg' or KenedoFileHelper::getExtension($path) == 'jpeg') {
						$base = imagecreatefromjpeg($path);
					}
					else {
						$base = imagecreate($cartPositionDetails->productData->opt_image_x, $cartPositionDetails->productData->opt_image_y);
					}
					
					if ($base === false) {
						KLog::log('PHP error during processing of visualization image "'.$image->visualization_image.'"','error','Error processing visualization image. Only PNG images are fully supported, please transcode the image to PNG 24 format.');
						return false;
					}
					
				}
				else {

					$path = CONFIGBOX_DIR_VIS_OPTION_IMAGES .DS. $image->visualization_image;
					if (KenedoFileHelper::getExtension($path) == 'png') {
						$visImage = imagecreatefrompng($path);
					}
					elseif (KenedoFileHelper::getExtension($path) == 'jpg' or KenedoFileHelper::getExtension($path) == 'jpeg') {
						$visImage = imagecreatefromjpeg($path);
					}
					else {
						$visImage = imagecreate($cartPositionDetails->productData->opt_image_x, $cartPositionDetails->productData->opt_image_y);
					}
					
					if ($visImage === false) {
						KLog::log('PHP error during processing of visualization image "'.$image->visualization_image.'"','error','Error processing visualization image. Only PNG images are fully supported, please transcode the image to PNG 24 format.');
						return false;
					}
										
					imagealphablending($base, true);
					imagesavealpha($base, true);
					
					self::mergeImages($base, $visImage, 0, 0, 0, 0, $cartPositionDetails->productData->opt_image_x, $cartPositionDetails->productData->opt_image_y);
					imagedestroy($visImage);
				}
			}
			
			$success = imagepng($base, $destinationFile);
			
			if ($success) {
				return $destinationFile;
			}
			else {
				return false;	
			}
			
		}

	}
	
	static function hasProductImage($cartPositionDetails) {
		return (count(self::getVisualizationData($cartPositionDetails->id)) || $cartPositionDetails->productData->baseimage);
	}
	
	static function getVisualizationImageSlots() {
		
		$configuration = ConfigboxConfiguration::getInstance();
		$prodId = $configuration->getProductId();
		if ($prodId == 0) return array();
	
		$db = KenedoPlatform::getDb();
		$pageId = KRequest::getInt('page_id',0);
		$query = "SELECT `visualization_view` FROM `#__configbox_pages` WHERE `id` = ".intval($pageId);
		$db->setQuery($query);
		$visualization_view = $db->loadResult();
	
		$query = "
		SELECT e.id AS element_id, xref.id AS xref_id, c.id as page_id, xref.visualization_image, e.autoselect_default, xref.default AS xref_default,
		CASE WHEN CHAR_LENGTH(xref.visualization_image) THEN xref.visualization_view ELSE e.upload_visualization_view END as visualization_view,
		CASE WHEN CHAR_LENGTH(xref.visualization_image) THEN xref.visualization_stacking ELSE e.upload_visualization_stacking END as visualization_stacking
			
		FROM `#__configbox_elements` AS e
		LEFT JOIN `#__configbox_xref_element_option` AS xref ON e.id = xref.element_id
		LEFT JOIN `#__configbox_pages` AS c ON c.id = e.page_id
		LEFT JOIN `#__configbox_products` AS p ON p.id = c.product_id
			
		WHERE
			
		p.id = ".intval($prodId)." AND p.published = 1 AND c.published = 1
			
		AND
	
		(
		xref.visualization_view = '".$db->getEscaped($visualization_view)."' AND xref.visualization_image != ''
			
		OR
			
		e.upload_visualize = 1 AND e.widget = 'fileupload' AND e.upload_visualization_view = '".$db->getEscaped($visualization_view)."'
			
		)
			
		ORDER BY visualization_stacking";

		$db->setQuery($query);
		$slots = $db->loadObjectList();
	
		foreach ($slots as &$slot) {
				
			$slot->type = ($slot->xref_id) ? 'xref-image':'element-image';
			$slot->css_classes = 'visualization-image';
			$slot->css_classes .= ' image-element-id-'.$slot->element_id;
			$slot->css_classes .= ' image-type-'.$slot->type;
				
				
			if ($slot->type == 'xref-image') {
	
				$slot->css_id = 'image-xref-id-'.$slot->xref_id;
				$slot->css_classes .= ' image-xref-id-'.$slot->xref_id;
	
				$slot->visualization_image = CONFIGBOX_URL_VIS_OPTION_IMAGES .'/'. $slot->visualization_image;
	
				if ($configuration->getElementXrefId($slot->element_id) == $slot->xref_id) {
					$slot->selected = true;
				}
				else {
					$slot->selected = false;
				}
	
			}

		}
	
		return $slots;
	}


	static function getProductInfo($productId) {
		return self::getProductVisualizationInfo($productId);
	}

	static function getProductVisualizationInfo($productId) {
			
		$query = "SELECT `id`, `baseimage`, `opt_image_x`, `opt_image_y` FROM `#__configbox_products` WHERE `id` = ".intval($productId)." LIMIT 1";
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$info = $db->loadObject();
	
		return $info;
	
	}
	
	/**
	 * merge two true colour images with variable opacity while maintaining alpha
	 * transparency of both images.
	 *
	 * @param  resource $dst  Destination image link resource
	 * @param  resource $src  Source image link resource
	 * @param  int      $dstX x-coordinate of destination point
	 * @param  int      $dstY y-coordinate of destination point
	 * @param  int      $srcX x-coordinate of source point
	 * @param  int      $srcY y-coordinate of source point
	 * @param  int      $w    Source width
	 * @param  int      $h    Source height
	 * @param  int      $pct  Opacity of source image (0-100)
	 **/
	static function mergeImages($dst, $src, $dstX, $dstY, $srcX, $srcY, $w, $h, $pct = 100) {
		$pct /= 100;
	
		/* make sure opacity level is within range before going any further */
		$pct  = max(min(1, $pct), 0);
	
		if ($pct == 0) {
			/* 0% opacity? then we have nothing to do */
			return;
		}
	
		/* work out if we need to bother correcting for opacity */
		if ($pct < 1) {
			/* we need a copy of the original to work from, only copy the cropped */
			/* area of src                                                        */
			$srccopy  = imagecreatetruecolor($w, $h);
	
			/* attempt to maintain alpha levels, alpha blending must be *off* */
			imagealphablending($srccopy, false);
			imagesavealpha($srccopy, true);
	
			imagecopyresized($srccopy, $src, 0, 0, $srcX, $srcY, $w, $h, imagesx($src), imagesy($src));
				
			/* we need to know the max transaprency of the image */
			$max_t = 0;
	
			for ($y = 0; $y < $h; $y++) {
				for ($x = 0; $x < $w; $x++) {
					$src_c = imagecolorat($srccopy, $x, $y);
					$src_a = ($src_c >> 24) & 0xFF;
	
					$max_t = $src_a > $max_t ? $src_a : $max_t;
				}
			}
			/* src has no transparency? set it to use full alpha range */
			$max_t = $max_t == 0 ? 127 : $max_t;
	
			/* $max_t is now being reused as the correction factor to apply based */
			/* on the original transparency range of  src                         */
			$max_t /= 127;
	
			/* go back through the image adjusting alpha channel as required */
			for ($y = 0; $y < $h; $y++) {
				for ($x = 0; $x < $w; $x++) {
					$src_c  = imagecolorat($src, $srcX + $x, $srcY + $y);
					$src_a  = ($src_c >> 24) & 0xFF;
					$src_r  = ($src_c >> 16) & 0xFF;
					$src_g  = ($src_c >>  8) & 0xFF;
					$src_b  = ($src_c)       & 0xFF;
	
					/* alpha channel compensation */
					$src_a = ($src_a + 127 - (127 * $pct)) * $max_t;
					$src_a = ($src_a > 127) ? 127 : (int)$src_a;
	
					/* get and set this pixel's adjusted RGBA colour index */
					$rgba  = ImageColorAllocateAlpha($srccopy, $src_r, $src_g, $src_b, $src_a);
	
					/* ImageColorAllocateAlpha returns -1 for PHP versions prior  */
					/* to 5.1.3 when allocation failed                               */
					if ($rgba === false || $rgba == -1) {
						$rgba = ImageColorClosestAlpha($srccopy, $src_r, $src_g, $src_b, $src_a);
					}
	
					imagesetpixel($srccopy, $x, $y, $rgba);
				}
			}
	
			/* call imagecopy passing our alpha adjusted image as src */
			imagecopyresized($dst, $srccopy, $dstX, $dstY, 0, 0, $w, $h, imagesx($src), imagesy($src));
	
			/* cleanup, free memory */
			imagedestroy($srccopy);
			return;
		}
		
		/* still here? no opacity adjustment required so pass straight through to */
		/* imagecopy rather than imagecopymerge to retain alpha channels          */
		imagecopyresized($dst, $src, $dstX, $dstY, $srcX, $srcY, $w, $h, imagesx($src), imagesy($src));
		return;
	}
	
}

