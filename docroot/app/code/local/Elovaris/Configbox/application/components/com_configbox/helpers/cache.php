<?php
class ConfigboxCacheHelper {
	
	static $cache;
	static $hasFixedArray;
	static $keyPrefix;
	static $empty = '';
	
	public static function getKeyPrefix() {
		if (empty(self::$keyPrefix)) {
			self::$keyPrefix = md5(__FILE__);
		}
		return self::$keyPrefix;
	}

	public static function &getCalculationModel($id) {
		
		if (!isset(self::$cache['calculationModels'])) {
			self::$cache['calculationModels'] = self::getFromCache('calculationModels');
			if (self::$cache['calculationModels'] == NULL) {
				self::writeCalculationModelCache();
				self::$cache['calculationModels'] = self::getFromCache('calculationModels');
			}
		}
		
		return self::$cache['calculationModels'][$id];
	}
	
	public static function writeCalculationModelCache() {
		
		$query = "
		SELECT m.name, m.type, t.*, f.*, m.id, e.calc
		FROM `#__configbox_calculations` AS m
		LEFT JOIN `#__configbox_calculation_matrices` AS t ON t.id = m.id
		LEFT JOIN `#__configbox_calculation_codes` AS f ON f.id = m.id
		LEFT JOIN `#__configbox_calculation_formulas` AS e ON e.id = m.id";
		
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);		
		$list = $db->loadObjectList('id');
		
		return self::writeToCache('calculationModels', $list);
	}
	
	/**
	 * 
	 * Copies translations of a text to the order records translation table
	 * 
	 * @param int $orderId the id of the order record
	 * @param string $translationTable translation table, where the string originally came from (with suffix)
	 * @param int $type type of the data (see langType in fields in KenedoEntity)
	 * @param int $key key of the original item
	 * @return bool success
	 * 
	 */
	public static function copyTranslationToOrder($orderId, $translationTable,$type,$key) {
		
		$db = KenedoPlatform::getDb(); 
		
		$tableValue = str_replace('#__','',$translationTable);

		$values = array();
		$languages = KenedoLanguageHelper::getActiveLanguages();
		foreach ($languages as $language) {
			$text = self::getTranslation($translationTable, $type, $key, $language->tag);
			if ($text == '') {
				continue;
			}
			$values[] = "( ".intval($orderId).", '".$db->getEscaped($tableValue)."', ".intval($type).", ".intval($key).", '".$db->getEscaped($language->tag)."','".$db->getEscaped($text)."')";
		}

		if (count($values)) {

			$query = "REPLACE INTO `#__cbcheckout_order_strings` (`order_id`, `table`, `type`, `key`, `language_tag`, `text`) VALUES ".implode(",\n",$values);
			$db->setQuery($query);
			$succ = $db->query();

			if (!$succ) {
				$errorMsg = $db->getErrorMsg();
				$query = "ROLLBACK";
				$db->setQuery($query);
				$db->query();
				KLog::log('Error inserting translation record for type "'.$type.'". SQL error is "'.$errorMsg.'"','error','Error inserting translation record record. See log for more info');
			}
		}
		else {
			$succ = true;
		}	
		return $succ;
	}
	
	/**
	 *
	 * Write translations for an item to the order records translation table
	 *
	 * @param int $orderId ID of the order record
	 * @param string $translationTable Name of the translation table, where the original translations (without table prefix placeholder)
	 * @param int $type Language type (see langType in fields in KenedoEntity)
	 * @param int $key Key of the original item
	 * @param string[] $translations Array of translations (array keys are the language tag)
	 * @return bool $success
	 *
	 */
	public static function addTranslationsToOrder($orderId, $translationTable, $type, $key, $translations) {

		$db = KenedoPlatform::getDb();

		$tableValue = str_replace('#__','',$translationTable);

		$query = "REPLACE INTO `#__cbcheckout_order_strings` (`order_id`, `table`, `type`, `key`, `language_tag`, `text`) VALUES ";
		$values = array();

		foreach ($translations as $languageTag => $text) {

			if ($text == '') {
				continue;
			}

			$values[] = "( ".intval($orderId).", '".$db->getEscaped($tableValue)."', ".intval($type).", ".intval($key).", '".$db->getEscaped($languageTag)."','".$db->getEscaped($text)."')";

		}

		if (count($values)) {
			$query .= implode(",\n",$values);
			$db->setQuery($query);
			$succ = $db->query();

			if (!$succ) {
				$errorMsg = $db->getErrorMsg();
				$query = "ROLLBACK";
				$db->setQuery($query);
				$db->query();
				KLog::log('Error inserting translation record for type "'.$type.'". SQL error is "'.$errorMsg.'"','error','Error inserting translation record record. See log for more info');
			}
		}
		else {
			$succ = true;
		}
		return $succ;

	}

	/**
	 * Gets a translated text that belongs to an order record
	 * @param int $orderId Order record id
	 * @param string $translationTable The table name, where the data comes from with no suffix (e.g. configbox_strings)
	 * @param int $type Type of the data (see langType in entities)
	 * @param int $key Key of the data (e.g. product id etc)
	 * @param string $languageTag The language you want the translation. NULL for current language
	 * @return string The translated text or empty if not found
	 */
	public static function &getOrderTranslation( $orderId, $translationTable, $type, $key, $languageTag = NULL) {

		if ($languageTag === NULL) {
			$languageTag = KText::$languageTag;
		}
		
		if (!isset(self::$cache['orderTranslations'][$orderId][$languageTag][$translationTable][$type])) {
			self::writeOrderTranslations($orderId, $languageTag);
		}
		
		if (isset(self::$cache['orderTranslations'][$orderId][$languageTag][$translationTable][$type][$key])) {
			return self::$cache['orderTranslations'][$orderId][$languageTag][$translationTable][$type][$key];
		}
		else {
			return self::$empty;
		}
		
	}
	
	public static function writeOrderTranslations($orderId, $languageTag) {

		if (!isset(self::$cache['orderTranslations'][$orderId][$languageTag])) {
			$db = KenedoPlatform::getDb();
			$query = "SELECT * FROM `#__cbcheckout_order_strings` WHERE `order_id` = ".intval($orderId)." AND `language_tag` = '".$db->getEscaped($languageTag)."'";
			$db->setQuery($query);
			$items = $db->loadAssocList();
			foreach ($items as $item) {
				self::$cache['orderTranslations'][$orderId][$languageTag][$item['table']][$item['type']][$item['key']] = $item['text'];
			}
			
			unset($items);
			
		}

	}

	public static function &getTranslation($translationTable,$type,$key,$languageTag = NULL) {
	
		if ($languageTag === NULL) {

			if (empty(KText::$languageTag)) {
				$languageTag = KenedoPlatform::p()->getLanguageTag();
			}

		}

		$tags = KenedoLanguageHelper::getActiveLanguageTags();

		if (empty($tags[$languageTag])) {
			$languageTag = CONFIGBOX_LANGUAGE_TAG;

			KText::setLanguage($languageTag);
		}
		else {
			KText::setLanguage($languageTag);
		}

		$tags = KenedoLanguageHelper::getActiveLanguageTags();

		if (empty($tags[$languageTag])) {
			$languageTag = CONFIGBOX_LANGUAGE_TAG;

			KText::setLanguage($languageTag);
		}
		else {
			KText::setLanguage($languageTag);
		}

		// Legacy, remove in 2.7
		if ($translationTable == '#__cbcheckout_strings') {
			KLog::logLegacyCall('Do not get translations from cbcheckout_strings, use configbox_strings instead. Some type ids have changed, see update notes on 2.6.18');
		}
		
		$translationTables['#__configbox_strings'] = 0;
		
		$tableId = 0;
		
		if (!isset(self::$cache['translations'][$languageTag])) {
			
			$cacheKey = 'translations.'.$languageTag;
			
			// Get from cache
			self::$cache['translations'][$languageTag] = self::getFromCache($cacheKey);
	
			// If NULL in cache
			if (self::$cache['translations'][$languageTag] === NULL) {
				self::writeTranslationCache($languageTag);
				self::$cache['translations'][$languageTag] = self::getFromCache($cacheKey);
			}
	
		}

//		if ($type == 1) {
//			var_dump($languageTag);
//			var_dump(self::$cache['translations'][$languageTag][$tableId][$type]);
//			die();
//		}

		
		if ( isset(self::$cache['translations'][$languageTag][$tableId][$type][$key]) ) {
			return self::$cache['translations'][$languageTag][$tableId][$type][$key];
		}
		else {
			$empty = '';
			return $empty;
		}
		
	}
	
	protected static function writeTranslationCache($languageTag) {
		
		$cacheKey = 'translations.'.$languageTag;
		
		$db = KenedoPlatform::getDb();
		
		$translationTables['#__configbox_strings'] = 0;
		
		$translations = array();
		
		foreach ($translationTables as $translationTable=>$tableId) {
			
			$query = "SELECT * FROM `".$translationTable."` WHERE `language_tag` = '".$db->getEscaped($languageTag)."'";
			$db->setQuery($query);
			$items = $db->loadAssocList();
			if ($items) {
				foreach ($items as &$t) {
					if (!empty($t['text']))	$translations[$tableId][$t['type']][$t['key']] = $t['text'];
				}
			}
		}
				
		return self::writeToCache($cacheKey, $translations);
		
	}
	
	static public function &getAssignments() {
		
		if (!isset(self::$cache['assignments'])) {
			
			// Get from cache
			self::$cache['assignments'] = self::getFromCache('assignments');
				
			// If NULL in cache
			if (self::$cache['assignments'] === NULL) {
				self::writeAssignments();
				self::$cache['assignments'] = self::getFromCache('assignments');
			}
			
		}
		return self::$cache['assignments'];
	}
	
	static protected function writeAssignments() {
		
		$db = KenedoPlatform::getDb();
		
		$query = "SELECT `id` AS `rate_id`, `zone` AS `zone_id` FROM `#__configbox_shipping_methods` WHERE `published` = 1 ORDER BY `price` ASC";
		$db->setQuery($query);
		$items = $db->loadAssocList();
		
		foreach ($items as $item) {
			$cache['zone_to_shippingmethod'][$item['zone_id']][$item['rate_id']] = $item['rate_id'];
			$cache['shippingmethod_to_zone'][$item['rate_id']][$item['zone_id']] = $item['zone_id'];
		}
		
		$query = "
		SELECT a.`payment_id`, a.`country_id` 
		FROM `#__cbcheckout_xref_country_payment_option` AS a 
		LEFT JOIN `#__configbox_countries` AS c ON a.country_id = c.id
		LEFT JOIN `#__configbox_payment_methods` AS p ON a.payment_id = p.id
		WHERE p.`published` = 1 && c.`published` = 1";
		$db->setQuery($query);
		$items = $db->loadAssocList();
		
		foreach ($items as $item) {
			$cache['payment_to_country'][$item['payment_id']][$item['country_id']] = $item['country_id'];
			$cache['country_to_payment'][$item['country_id']][$item['payment_id']] = $item['payment_id'];
		}
		
		
		$query = "
		SELECT a.`zone_id`, a.`country_id`
		FROM `#__cbcheckout_xref_country_zone` AS a
		LEFT JOIN `#__configbox_countries` AS c ON a.country_id = c.id
		LEFT JOIN `#__configbox_zones` AS z ON a.zone_id = z.id
		WHERE c.`published` = 1";
		$db->setQuery($query);
		$items = $db->loadAssocList();
		
		foreach ($items as $item) {
			$cache['zone_to_country'][$item['zone_id']][$item['country_id']] = $item['country_id'];
			$cache['country_to_zone'][$item['country_id']][$item['zone_id']] = $item['zone_id'];
		}
		
		
		$query = "
		SELECT x.`listing_id`, x.`product_id` 
		FROM `#__configbox_xref_listing_product` AS x
		LEFT JOIN `#__configbox_listings` AS l ON l.id = x.listing_id
		LEFT JOIN `#__configbox_products`  AS p ON p.id = x.product_id
		WHERE p.published = 1 AND l.published = 1
		ORDER BY x.`ordering`";
		$db->setQuery($query);
		$items = $db->loadAssocList();
		foreach ($items as $item) {
			$cache['listing_to_product'][$item['listing_id']][$item['product_id']] = $item['product_id'];
			$cache['product_to_listing'][$item['product_id']][$item['listing_id']] = $item['listing_id'];
		}
		
		$query = "
		SELECT e.id AS element_id, c.id AS page_id, c.product_id AS product_id
		FROM `#__configbox_elements` AS e
		LEFT JOIN `#__configbox_pages` AS c ON c.id = e.page_id
		LEFT JOIN `#__configbox_products` AS p ON p.id = c.product_id
		WHERE e.published = 1 AND c.published = 1 AND p.published = 1
		ORDER BY p.id, c.ordering, e.ordering";
		
		$db->setQuery($query);
		$items = $db->loadAssocList();
		
		foreach ($items as $item) {
			$cache['element_to_product'][$item['element_id']] = $item['product_id'];
			$cache['element_to_page'][$item['element_id']] = $item['page_id'];
			$cache['product_to_element'][$item['product_id']][$item['element_id']] = $item['element_id'];
			$cache['page_to_element'][$item['page_id']][$item['element_id']] = $item['element_id'];			
		}
		
		$query = "
		SELECT c.id AS page_id, c.product_id AS product_id
		FROM `#__configbox_pages` AS c
		WHERE c.published = 1
		ORDER BY c.ordering";
		$db->setQuery($query);
		$items = $db->loadAssocList();
		foreach ($items as $item) {
			$cache['product_to_page'][$item['product_id']][$item['page_id']] = $item['page_id'];
			$cache['page_to_product'][$item['page_id']] = $item['product_id'];
		}
		
		$query = "	
		SELECT xref.id AS xref_id, o.id AS option_id, xref.element_id AS element_id, c.id AS page_id, c.product_id AS product_id
		FROM `#__configbox_xref_element_option` AS xref
		LEFT JOIN `#__configbox_options` AS o ON o.id = xref.option_id
		LEFT JOIN `#__configbox_elements` AS e ON e.id = xref.element_id
		LEFT JOIN `#__configbox_pages` AS c ON c.id = e.page_id
		LEFT JOIN `#__configbox_products` AS p ON p.id = c.product_id
		WHERE xref.published = 1 AND e.published = 1 AND c.published = 1 AND p.published = 1
		ORDER BY p.id, c.ordering, e.ordering, xref.ordering";
		
		$db->setQuery($query);
		$items = $db->loadAssocList();
		
		foreach ($items as $item) {
			$cache['xref_to_element'][$item['xref_id']] = $item['element_id'];
			$cache['element_to_xref'] [$item['element_id']] [$item['xref_id']] = $item['xref_id'];
				
			$cache['xref_to_product'][$item['xref_id']] = $item['product_id'];			
			$cache['product_to_xref'][$item['product_id']][$item['xref_id']] = $item['xref_id'];
			
			$cache['xref_to_page'][$item['xref_id']] = $item['page_id'];
			$cache['page_to_xref'][$item['page_id']][$item['xref_id']] = $item['xref_id'];
		}
		
		return self::writeToCache('assignments', $cache);
		
	}
	
	/**
	 * This method adds info to a product data object (like localized fields, prices, price module settings etc)
	 * to make it ready for easy use in templates
	 * 
	 * @param object $product
	 */
	public static function augmentProduct(&$product) {
		
		// Prevent augmenting product data multiple times (yes, in a very lousy way)
		if (isset($product->title)) return;

		if ($product) {
			$product->productDetailPanes = self::getProductDetailPanes($product->id);
		}

		// Append translatable strings
		$model = KenedoModel::getModel('ConfigboxModelAdminproducts');
		$props = $model->getProperties();
		foreach ($props as $prop) {
			if ($prop->getType() == 'translatable') {
				$product->{$prop->getName()} = self::getTranslation($prop->getPropertyDefinition('stringTable'), $prop->getPropertyDefinition('langType'), $product->id);
			}
		}
		
		// Add listing title and description
		$product->listing_title					= self::getTranslation('#__configbox_strings', 20, $product->listing_id);
		$product->listing_description			= self::getTranslation('#__configbox_strings', 40, $product->listing_id);
		
		// Add path to product image (either the image or the global product default image)
		if ($product->prod_image) {
			$product->imagesrc = CONFIGBOX_URL_PRODUCT_IMAGES.'/'.$product->prod_image;
		}
		else {
			$product->imagesrc = CONFIGBOX_URL_DEFAULT_IMAGES.'/'.CONFIGBOX_DEFAULTPRODIMAGE;
		}

		// Set the price label (mind the camel case notation)
		$product->priceLabel = ($product->pricelabel) ? $product->pricelabel : KText::_('Price');
		$product->priceLabelRecurring = ($product->pricelabel_recurring) ? $product->pricelabel_recurring : KText::_('Recurring Price');
		
		// Deal with tax rates and prices - START
		$product->taxRate = ConfigboxPrices::getProductTaxRate($product->id);
		$product->taxRateRecurring = ConfigboxPrices::getProductTaxRateRecurring($product->id);
		
		$product->basePriceNet = ConfigboxPrices::getProductPrice($product->id,true,true);
		$product->basePriceGross = ConfigboxPrices::getProductPrice($product->id,false,true);
		$product->basePriceTax = $product->basePriceGross - $product->basePriceNet;
		
		$product->basePriceRecurringNet = ConfigboxPrices::getProductPriceRecurring($product->id,true,true);
		$product->basePriceRecurringGross = ConfigboxPrices::getProductPriceRecurring($product->id,false,true);
		$product->basePriceRecurringTax = $product->basePriceRecurringGross - $product->basePriceRecurringNet;
		
		$product->priceNet = ConfigboxPrices::getProductPrice($product->id,true,false);
		$product->priceGross = ConfigboxPrices::getProductPrice($product->id,false,false);
		$product->priceTax = $product->priceGross - $product->priceNet;
		
		$product->priceRecurringNet = ConfigboxPrices::getProductPriceRecurring($product->id,true,false);
		$product->priceRecurringGross = ConfigboxPrices::getProductPriceRecurring($product->id,false,false);
		$product->priceRecurringTax = $product->priceRecurringGross - $product->priceRecurringNet;
		
		// These two are always in the current tax mode and the selected currency
		$product->price = ConfigboxPrices::getProductPrice($product->id, ConfigboxPrices::showNetPrices(), false);
		$product->priceRecurring = ConfigboxPrices::getProductPriceRecurring($product->id, ConfigboxPrices::showNetPrices(), false);
		
		
		$product->wasPriceNet = ConfigboxPrices::getProductWasPrice($product->id,true,false);
		$product->wasPriceGross = ConfigboxPrices::getProductWasPrice($product->id,false,false);
		$product->wasPriceTax = $product->wasPriceGross - $product->wasPriceNet;
		
		$product->wasPriceRecurringNet = ConfigboxPrices::getProductWasPriceRecurring($product->id,true,false);
		$product->wasPriceRecurringGross = ConfigboxPrices::getProductWasPriceRecurring($product->id,false,false);
		$product->wasPriceRecurringTax = $product->wasPriceRecurringGross - $product->wasPriceRecurringNet;
		
		// These two are always in the current tax mode and the selected currency
		$product->wasPrice = ConfigboxPrices::getProductWasPrice($product->id, ConfigboxPrices::showNetPrices(), false);
		$product->wasPriceRecurring = ConfigboxPrices::getProductWasPriceRecurring($product->id, ConfigboxPrices::showNetPrices(), false);
		
		// Unset price variables that could be misleading in templates
		unset($product->baseprice);
		unset($product->baseprice_recurring);
		
		// Deal with taxRate and prices - END
		
		// Set the custom price text for the regular price
		if ($product->custom_price_text) {
			$matches = array();
			$regEx = "/\[(.*?)\]/";
			preg_match_all($regEx, $product->custom_price_text, $matches);
			if (isset($matches[1][0])) {
				$search = $matches[0][0];
				$price = (float)$matches[1][0] * CONFIGBOX_CURRENCY_MULTI;
				$price = $price + ($price / 100 * $product->taxRate);
				$output = cbprice($price);
				$product->custom_price_text = str_replace($search,$output,$product->custom_price_text);
			}
		}
		
		// Set the custom price text for the recurring price
		if ($product->custom_price_text_recurring) {
			$matches = array();
			$regEx = "/\[(.*?)\]/";
			preg_match_all($regEx, $product->custom_price_text_recurring, $matches);
			if (isset($matches[1][0])) {
				$search = $matches[0][0];
				$price = (float)$matches[1][0] * CONFIGBOX_CURRENCY_MULTI;
				$price = $price + ($price / 100 * $product->taxRate);
				$output = cbprice($price);
				$product->custom_price_text_recurring = str_replace($search,$output,$product->custom_price_text_recurring);
			}
		}
			
		// Determine if reviews shall be shown
		$product->showReviews = ( $product->enable_reviews == 1 or ( $product->enable_reviews == 2 and CONFIGBOX_ENABLE_REVIEWS_PRODUCTS == 1) );

		// Determine the first page id (mainly used for the 'configure product' link), 
		$assignments = self::getAssignments();
		$pages = isset($assignments['product_to_page'][$product->id]) ? $assignments['product_to_page'][$product->id] : array();

		$product->isConfigurable = (count($pages)) ? true : false;
		$product->firstPageId = (count($pages)) ? array_shift($pages) : NULL;

		// Deal with price module settings - START
		$ps = array (
				'pm_show_regular_first' 			=> $product->pm_show_regular_first,
				'pm_show_delivery_options' 			=> $product->pm_show_delivery_options,
				'pm_show_payment_options'			=> $product->pm_show_payment_options,
				'pm_show_net_in_b2c' 				=> $product->pm_show_net_in_b2c,
				'pm_regular_show_prices' 			=> $product->pm_regular_show_prices,
				'pm_regular_show_categories' 		=> $product->pm_regular_show_categories,
				'pm_regular_show_elements' 			=> $product->pm_regular_show_elements,
				'pm_regular_show_elementprices' 	=> $product->pm_regular_show_elementprices,
				'pm_regular_expand_categories' 		=> $product->pm_regular_expand_categories,
				'pm_regular_show_taxes'				=> $product->pm_regular_show_taxes,
				'pm_regular_show_cart_button'		=> $product->pm_regular_show_cart_button,
				'pm_recurring_show_overview'		=> $product->pm_recurring_show_overview,
				'pm_recurring_show_prices' 			=> $product->pm_recurring_show_prices,
				'pm_recurring_show_categories' 		=> $product->pm_recurring_show_categories,
				'pm_recurring_show_elements' 		=> $product->pm_recurring_show_elements,
				'pm_recurring_show_elementprices' 	=> $product->pm_recurring_show_elementprices,
				'pm_recurring_expand_categories' 	=> $product->pm_recurring_expand_categories,
				'pm_recurring_show_taxes'			=> $product->pm_recurring_show_taxes,
				'pm_recurring_show_cart_button'		=> $product->pm_recurring_show_cart_button,
		);
		
		foreach ($ps as $key=>&$value) {
			if (($value == 2 && $key != 'pm_regular_expand_categories' && $key != 'pm_recurring_expand_categories') or ($value == 3 && $key == 'pm_regular_expand_categories')) {
				$product->$key = constant('CONFIGBOX_'.strtoupper($key));
			}
		
			if (($value == 2 && $key != 'pm_regular_expand_categories' && $key != 'pm_recurring_expand_categories') or ($value == 3 && $key == 'pm_recurring_expand_categories')) {
				$product->$key = constant('CONFIGBOX_'.strtoupper($key));
			}
		
		}
		
		// Deal with price module settings - END
		
	}
	
	/**
	 * Returns product data of either all or a single product
	 * @param int $productId
	 * @return object|object[] $productData Object with product data or array of objects all products
	 */
	public static function &getProductData($productId = NULL) {
		
		if (!isset( self::$cache['products'] )) {
			// Get from cache
			self::$cache['products'] = self::getFromCache('products');
			
			// If NULL in cache
			if (self::$cache['products'] == NULL) {
				$db = KenedoPlatform::getDb();
				$query = "
				SELECT p.*, xref.listing_id AS listing_id
				FROM `#__configbox_products` p
				LEFT JOIN `#__configbox_xref_listing_product` AS xref ON xref.product_id = p.id
				WHERE `published` = 1 
				ORDER BY xref.`ordering`";
				$db->setQuery($query);
				$products = $db->loadObjectList('id');

				self::$cache['products'] = $products;
				self::writeToCache('products', self::$cache['products']);
			}
		}
		
		if ($productId) {
			self::augmentProduct(self::$cache['products'][$productId]);
			return self::$cache['products'][$productId];
		}
		else {
			foreach (self::$cache['products'] as $productId=>&$product) {
				self::augmentProduct($product);
			}
			return self::$cache['products'];
		}
	
	}

	protected static function getProductDetailPanes($productId) {
	
		$db = KenedoPlatform::getDb();
		$query = "SELECT * FROM `#__configbox_product_detail_panes` WHERE `product_id` = ".intval($productId)."  ORDER BY `ordering`";
		$db->setQuery($query);
		$panes = $db->loadObjectList();
		
		if ($panes == NULL) {
			$panes = array();
		}
	
		foreach ($panes as $pane) {
			$pane->heading 			= self::getTranslation('#__configbox_strings',  93, $pane->id);
			$pane->content			= self::getTranslation('#__configbox_strings',  94, $pane->id);
			$pane->usesHeadingIcon	= !empty($pane->heading_icon_filename);
			$pane->headingIconSrc 	= CONFIGBOX_URL_PRODUCT_DETAIL_PANE_ICONS . '/'. $pane->heading_icon_filename;
		}
	
		return $panes;
	
	}
	
	protected static function writeElementDataCache() {
	
		$db = KenedoPlatform::getDb();
		$query = "
		SELECT e.*, c.product_id AS product_id, c.id AS page_id
		FROM `#__configbox_elements` AS e
		LEFT JOIN `#__configbox_pages` AS c ON c.id = e.page_id
		LEFT JOIN `#__configbox_products` AS p ON p.id = c.product_id
		WHERE e.published = 1 AND c.published = 1 AND p.published = 1";
		$db->setQuery($query);
		$elements = $db->loadObjectList();
	
		$data = array();
		
		foreach ($elements as $element) {
			$data[$element->product_id][$element->id] = $element;
		}
		
		foreach ($data as $productId=>&$dataItem) {
			self::writeToCache('elements.product_'.$productId, $dataItem);
		}
	
		return true;
	}

	/**
	 * @param $elementId
	 * @return ConfigboxElement|NULL
	 * @throws Exception
	 */
	public static function &getElementData($elementId) {
	
		$assignments = self::getAssignments();
		
		if (!isset($assignments['element_to_product'][$elementId])) {
			$return = NULL;
			return $return;
		}
		
		$productId = $assignments['element_to_product'][$elementId];
		
		if (!$productId) {
			$return = NULL;
			return $return;
		}
		
		if (!isset( self::$cache['elements'][$productId][$elementId] )) {
	
			// Get from cache
			self::$cache['elements'][$productId] = self::getFromCache('elements.product_'.$productId);
	
			// If NULL in cache
			if (self::$cache['elements'][$productId] === NULL) {
				self::writeElementDataCache();
				self::$cache['elements'][$productId] = self::getFromCache('elements.product_'.$productId);
			}
			
			if (empty(self::$cache['elements'][$productId])) {
				$return = NULL;
				return $return; 
			}
			
			// Append translatable strings
			$model = KenedoModel::getModel('ConfigboxModelAdminelements');
			$props = $model->getProperties();
			
			foreach (self::$cache['elements'][$productId] as $element) {

				foreach ($props as $prop) {
					if ($prop->getType() == 'translatable') {
						$element->{$prop->getName()} = self::getTranslation($prop->getPropertyDefinition('stringTable'), $prop->getPropertyDefinition('langType'), $element->id);
					}
				}
				
			}
	
		}
	
		return self::$cache['elements'][$productId][$elementId];
	
	}
	
	public static function &getXrefData($xrefId) {
		
		$assignments = self::getAssignments();
		$productId = $assignments['xref_to_product'][$xrefId];

		if (!isset( self::$cache['xrefs'][$productId][$xrefId] )) {
			
			// Get from cache
			self::$cache['xrefs'][$productId] = self::getFromCache('xrefs.product_'.$productId);
			
			if (self::$cache['xrefs'][$productId] === NULL) {
				self::writeXrefDataCache($productId);
				self::$cache['xrefs'][$productId] = self::getFromCache('xrefs.product_'.$productId);
			}
			
			if (empty(self::$cache['xrefs'][$productId])) {
				$return = NULL;
				return $return;
			}
			
			// Append translatable strings
			$optionModel = KenedoModel::getModel('ConfigboxModelAdminoptions');
			$optionProps = $optionModel->getProperties();

			$xrefModel = KenedoModel::getModel('ConfigboxModelAdminxrefelementoptions');
			$xrefProps = $xrefModel->getProperties();

			foreach (self::$cache['xrefs'][$productId] as $id=>$xref) {

				foreach ($optionProps as $prop) {
					if ($prop->getType() == 'translatable') {
						$xref->{$prop->getName()} = self::getTranslation($prop->getPropertyDefinition('stringTable'), $prop->getPropertyDefinition('langType'), $xref->option_id);
					}
				}
				unset($prop);

				foreach ($xrefProps as $prop) {
					if ($prop->getType() == 'translatable') {
						$xref->{$prop->getName()} = self::getTranslation($prop->getPropertyDefinition('stringTable'), $prop->getPropertyDefinition('langType'), $xref->id);
					}
				}
				
			}

		}
		
		return self::$cache['xrefs'][$productId][$xrefId];
	
	}
	
	protected static function writeXrefDataCache($productId) {
	
		$db = KenedoPlatform::getDb();
		
		$query = "	
		SELECT  
		o.*,
		o.price AS basePriceStatic,
		o.price_recurring AS basePriceRecurringStatic,
		o.was_price,
		o.was_price_recurring,
		xref.*,
		xref.id AS id,
		xref.option_id AS option_id,
		c.id AS page_id,
		p.id AS product_id
	
		FROM `#__configbox_xref_element_option` AS xref
		LEFT JOIN `#__configbox_options` AS o ON o.id = xref.option_id
		LEFT JOIN `#__configbox_elements` AS e ON e.id = xref.element_id
		LEFT JOIN `#__configbox_pages` AS c ON c.id = e.page_id
		LEFT JOIN `#__configbox_products` AS p ON p.id = c.product_id
		
		WHERE xref.published = 1 AND e.published = 1 AND c.published = 1 AND p.id = ".(int)$productId."
		ORDER BY xref.element_id, xref.ordering";
		
		$db->setQuery($query);
		$items = $db->loadObjectList('id');
		
		return self::writeToCache('xrefs.product_'.$productId, $items);
		
	}
	
	public static function purgeCache() {

		self::$cache = NULL;

		if (extension_loaded('apc') && ini_get('apc.enabled')) {
			
			$prefix = self::getKeyPrefix();

			apc_clear_cache('user');

			// If we got APCIterator, removing our entries is much faster
			if (class_exists('APCIterator')) {
				$iterator = new APCIterator('user', '/^'.$prefix.'/');

				while($iterator->valid()) {
					apc_delete($iterator->key());
					$iterator->next();
				}

			}
			// Otherwise use the keys cached alongside the actual cache entries to figure out what to remove
			else {
			
				$cacheKeys = apc_fetch($prefix.'.cacheKeys');
				
				if ($cacheKeys) {
					foreach ($cacheKeys as $key) {
						apc_delete($key);
					}
					apc_delete($prefix.'.cacheKeys');
				}
				
			}
			
		}
		else {
			if (is_dir(CONFIGBOX_DIR_CACHE)) {
				if (is_writable(CONFIGBOX_DIR_CACHE)) {
					KenedoFileHelper::deleteFolder(CONFIGBOX_DIR_CACHE);
				}
				else {
					return false;
				}
			}
			
		}

		self::$cache = NULL;

		return true;
	}
	
	public static function writeToCache($key, &$data) {
		
		if (extension_loaded('apc') && ini_get('apc.enabled')) {
			$prefix = self::getKeyPrefix();
			KLog::count('cache_writes');
			
			$res = apc_store($prefix.'.'.$key,$data);
			
			// If we got no APCIterator, include the key in the cacheKeys entry for purging
			if ($res && class_exists('APCIterator') == false) {
				$cacheKeys = apc_fetch($prefix.'.cacheKeys');
				if (!$cacheKeys) $cacheKeys = array();
				if (!in_array($key,$cacheKeys)) {
					$cacheKeys[] = $key;
					$res = apc_store($prefix.'.cacheKeys',$cacheKeys);
				}
			}
			
			return $res;
		}
		else {
			$key = str_replace('.', DS, $key);
			
			$filename = CONFIGBOX_DIR_CACHE.DS.$key.'.php';
			if (!is_dir(dirname($filename))) {
				mkdir(dirname($filename), 0777, true);
			}
			$content = "<?php\ndefined('CB_VALID_ENTRY') or die();\n\$var = ".var_export($data,true).";";
			return file_put_contents($filename, $content);
		}
		
		
	}
	
	public static function &getFromCache($key) {
		
		if (extension_loaded('apc') && ini_get('apc.enabled')) {
			$prefix = self::getKeyPrefix();
			$result = apc_fetch($prefix.'.'.$key);
			if ($result === false) {
				$result = NULL;
			}
			return $result;
		}
		else {
			$key = str_replace('.', DS, $key);
			
			$filename = CONFIGBOX_DIR_CACHE.DS.$key.'.php';
			if (is_file($filename)) {
				include($filename);
				/** @noinspection PhpUndefinedVariableInspection */
				return $var;
			}
			else {
				$var = NULL;
				return $var;
			}
		}
	}

}