<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewTerms */
?>

<div id="com_configbox">
<div id="view-terms">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

<?php if (KRequest::getKeyword('tmpl') == 'component') { ?>
	<div style="margin:10px">
<?php } ?>

<h2 class="componentheading"><?php echo hsc(KText::_('Terms and Conditions'));?></h2>

<?php echo $this->terms;?>

<?php if (KRequest::getKeyword('tmpl') == 'component') { ?>
	</div>
<?php } ?>

</div>
</div>
</div>