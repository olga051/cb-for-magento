<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewUserorder */
?>
<div class="buttons">

	<a rel="prev" class="navbutton-medium back floatleft leftmost" href="<?php echo $this->urlBackToAccount;?>">
		<span class="nav-title"><?php echo KText::_('Back');?></span>
		<span class="nav-text"><?php echo KText::_('To Customer Account');?></span>
	</a>

	<?php if ($this->canCheckout) { ?>
		<a rel="next" class="navbutton-medium next floatright rightmost" href="<?php echo $this->urlCheckoutOrder;?>">
			<span class="nav-title"><?php echo KText::_('Purchase');?></span>
			<span class="nav-text"><?php echo KText::_('To Checkout');?></span>
		</a>
	<?php } ?>

	<div class="clear"></div>
</div>