<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewProduct extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'product';

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	/**
	 * @var boolean $showPageHeading Setting from platform (in case of Joomla it's the menu item's setting)
	 */
	public $showPageHeading;

	/**
	 * @var string $pageHeading The page heading text (either the heading set via the platform or if empty the product title)
	 */
	public $pageHeading;

	/**
	 * @var bool $showProductDetailPanes Tells if product detail panes shall be shown (depends on settings in product form)
	 */
	public $showProductDetailPanes;

	/**
	 * @var string $productDetailPanes Ready-to-use HTML with product detail panes
	 * @see ConfigboxViewProductdetailpanes
	 */
	public $productDetailPanes;

	/**
	 * @var boolean $useLongdescTemplate Indicates if the product description shall be used instead of the template
	 * Setting is found in the product settings.
	 */
	public $useLongdescTemplate;

	/**
	 * @var string $linkconfigure The URL to the product's first configurator page
	 */
	public $linkconfigure;

	/**
	 * @var string $linkbuy The URL that triggers a placing the product in the cart
	 */
	public $linkbuy;

	/**
	 * @var string $priceKey Makes the prices show net or gross, depending on B2B/B2C setting. Is either priceNet or priceGross
	 */
	public $priceKey;

	/**
	 * @var string $priceKeyRecurring Same as priceKey for the recurring price
	 */
	public $priceKeyRecurring;

	/**
	 * Looks into the listing's specified template name and uses it (or overrides of it)
	 * @param NULL|string $template IGNORED!
	 */
	function renderView($template = NULL) {

		if (empty($this->product)) {
			header('', true, 410);
			parent::renderView('notfound');
			return;
		}

		// List the template paths sorted by how specific they are
		$templates['templateOverride'] = KenedoPlatform::p()->getTemplateOverridePath('com_configbox', 'product', $this->template);
		$templates['customTemplate'] = CONFIGBOX_DIR_CUSTOMIZATION.DS.'templates'.DS.'product'.DS.$this->template.'.php';
		$templates['defaultTemplate'] = KPATH_DIR_CB.DS.'views'.DS.'product'.DS.'tmpl'.DS.'default.php';

		// Try which template to use
		foreach ($templates as $template) {
			if (is_file($template)) {
				require($template);
				break;
			}
		}

	}

	function prepareTemplateVars() {

		// Fix labels (in case of language change with JoomFish or when labels have changed)
		if ((KRequest::getInt('prod_id',0) == 0 && KRequest::getString('prodlabel','')) != '' || (KRequest::getInt('page_id',0) == 0 && KRequest::getString('catlabel','') != '')) {
			KLog::log('No cat or prod id, but label found. Trying to fix label (prodlabel: "'.KRequest::getString('prodlabel','').'", catlabel: "'.KRequest::getString('catlabel','').'", prod_id:"'.KRequest::getInt('prod_id',0).'", page_id:"'.KRequest::getInt('page_id',0).'")','debug');
			ConfigboxLabelsHelper::fixLabels();
		}

		$model = KenedoModel::getModel('ConfigboxModelProduct');
		$product = $model->getProduct();

		if (!$product || $product->published != '1') {
			return;
		}

		// Set price key (net or gross)
		if ( ConfigboxPrices::showNetPrices() ) {
			$priceKey = 'priceNet';
			$priceKeyRecurring = 'priceRecurringNet';
		}
		else {
			$priceKey = 'priceGross';
			$priceKeyRecurring = 'priceRecurringGross';
		}
		$this->assign('priceKey', $priceKey);
		$this->assign('priceKeyRecurring', $priceKeyRecurring);

		// Prepare some variables that aren't practical to set up in the model
		$linkbuy = KLink::getRoute('index.php?option=com_configbox&view=cart&task=addProductToCart&prod_id='.$product->id);
		$this->assign('linkbuy', $linkbuy);

		if ($product->isConfigurable) {
			$linkconfigure = KLink::getRoute('index.php?option=com_configbox&view=configuratorpage&prod_id='.$product->id.'&page_id='.$product->firstPageId);
		} else {
			$linkconfigure = $linkbuy;
		}
		$this->assign('linkconfigure', $linkconfigure);

		// Replace placeholders with data
		$product->longdescription = str_replace('{linkbuy}',$linkbuy,$product->longdescription);
		$product->longdescription = str_replace('{linkimage}',$product->imagesrc,$product->longdescription);
		$product->longdescription = str_replace('{linkconfigure}',$linkconfigure,$product->longdescription);

		$product->longdescription = str_replace('#linkbuy',$linkbuy,$product->longdescription);
		$product->longdescription = str_replace('#linkconfigure',$linkconfigure,$product->longdescription);

		if (ConfigboxPermissionHelper::canSeePricing()) {
			$product->longdescription = str_replace('{productprice}','<span class="product_price">'.cbprice($product->{$priceKey}).'</span>',$product->longdescription);
			$product->longdescription = str_replace('{productpricerecurring}','<span class="product_price_recurring">'.cbprice($product->{$priceKeyRecurring}).'</span>',$product->longdescription);
		}
		else {
			$product->longdescription = str_replace('{productprice}','',$product->longdescription);
			$product->longdescription = str_replace('{productpricerecurring}','',$product->longdescription);
		}

		$product->description = KenedoPlatform::p()->processContentModifiers($product->description);
		$product->description = trim($product->description);
		$product->longdescription = KenedoPlatform::p()->processContentModifiers($product->longdescription);
		$product->longdescription = trim($product->longdescription);

		// Deal with product detail panes
		if (count($product->productDetailPanes) && $product->product_detail_panes_in_product_pages) {
			$view = KenedoView::getView('ConfigboxViewProductdetailpanes');
			$view->assignRef('product',$product);
			$view->assignRef('parentView','productPage');
			$productDetailsHtml = $view->getViewOutput($product->product_detail_panes_method);

			$productDetailsHtml = KenedoPlatform::p()->processContentModifiers($productDetailsHtml);
			$productDetailsHtml = trim($productDetailsHtml);

			$this->assignRef('productDetailPanes',$productDetailsHtml);
			$this->assign('showProductDetailPanes',true);
		}
		else {
			$this->assign('showProductDetailPanes',false);
		}

		$params = KenedoPlatform::p()->getAppParameters();
		$this->assignRef('params',$params);

		$this->assign('showPageHeading', ($params->get('show_page_heading', 1) && $params->get('page_title','') != ''));
		$this->assign('pageHeading', ($params->get('page_title')) ? $params->get('page_title') : $product->title );

		$this->assign('product', $product);

		$this->assign('linkimage', $product->imagesrc);
		$this->assign('useLongdescTemplate', (!empty($product->longdescription) && $product->longdesctemplate == 1));

		// Get the layout name of the product
		$templateName = (!empty($product->layoutname)) ? $product->layoutname : 'default';
		$this->assign('template', $templateName);

	}

}
