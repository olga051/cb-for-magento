<?php
class ConfigboxControllerReview extends KenedoController {

	/**
	 * @return ConfigboxModelReviews $model
	 */
	protected function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelReviews');
	}

	/**
	 * @return ConfigboxViewReview
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewReview');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

	/**
	 * Stores a review. Form data normally comes from a form in ConfigboxViewReview and sent via an XHR.
	 * See CSS class .trigger-send-review for event handler that does the request.
	 *
	 * @throws Exception
	 */
	public function storeReview() {

		$model = KenedoModel::getModel('ConfigboxModelReviews');

		$id = KRequest::getInt('id');
		$kind = KRequest::getKeyword('kind');
		$name = KRequest::getString('name');
		$comment = KRequest::getString('comment');
		$rating = KRequest::getFloat('rating');

		$storeSuccess = $model->storeReview($id, $kind, $name, $comment, $rating);

		if ($storeSuccess) {

			$model->notifyOnReview($id, $kind, $name, $comment, $rating);

			$json = array('data'=>array('name'=>$name, 'comment'=>$comment), 'success'=>true, 'errors'=>array() );
		}
		else {
			$json = array('data'=>array('name'=>$name, 'comment'=>$comment), 'success'=>false, 'errors'=>$model->getErrors());
		}

		echo json_encode($json);

	}

}