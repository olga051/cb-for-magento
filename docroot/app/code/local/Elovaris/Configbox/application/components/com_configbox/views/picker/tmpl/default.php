<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewPicker */
?>
<div id="modalbox" data-target="<?php echo $this->target;?>" data-display-target="<?php echo $this->displayTarget;?>">
<?php echo $this->element->picker_table; ?>
</div>