<?php
defined('CB_VALID_ENTRY') or die();

require_once(dirname(__FILE__).DS.'common.php');

define('KPATH_DIR_CB', KenedoPlatform::p()->getComponentDir('com_configbox') );
define('KPATH_URL_ASSETS', KenedoPlatform::p()->getUrlAssets());
define('KPATH_DIR_ASSETS', KenedoPlatform::p()->getDirAssets());

KenedoAutoload::registerClass('ConfigboxCacheHelper',		KPATH_DIR_CB.DS.'helpers'.DS.'cache.php');
KenedoAutoload::registerClass('ConfigboxUpdateHelper',		KPATH_DIR_CB.DS.'helpers'.DS.'update.php');
KenedoAutoload::registerClass('ConfigboxLabelsHelper',		KPATH_DIR_CB.DS.'helpers'.DS.'labels.php');
KenedoAutoload::registerClass('ConfigboxRulesHelper',		KPATH_DIR_CB.DS.'helpers'.DS.'rules.php');
KenedoAutoload::registerClass('ConfigboxProductImageHelper',KPATH_DIR_CB.DS.'helpers'.DS.'productimage.php');
KenedoAutoload::registerClass('ConfigboxPrices',			KPATH_DIR_CB.DS.'helpers'.DS.'prices.php');
KenedoAutoload::registerClass('ConfigboxLocationHelper',	KPATH_DIR_CB.DS.'helpers'.DS.'location.php');
KenedoAutoload::registerClass('ConfigboxQuickeditHelper',	KPATH_DIR_CB.DS.'helpers'.DS.'quickedit.php');
KenedoAutoload::registerClass('ConfigboxRatingsHelper',		KPATH_DIR_CB.DS.'helpers'.DS.'ratings.php');
KenedoAutoload::registerClass('ConfigboxCacheHelper',		KPATH_DIR_CB.DS.'helpers'.DS.'cache.php');
KenedoAutoload::registerClass('ConfigboxCalculation',		KPATH_DIR_CB.DS.'helpers'.DS.'calculation.php');
KenedoAutoload::registerClass('ConfigboxUserHelper',		KPATH_DIR_CB.DS.'helpers'.DS.'user.php');
KenedoAutoload::registerClass('ConfigboxCalcTerm',			KPATH_DIR_CB.DS.'classes'.DS.'ConfigboxCalcTerm.php');
KenedoAutoload::registerClass('ConfigboxCondition',			KPATH_DIR_CB.DS.'classes'.DS.'ConfigboxCondition.php');
KenedoAutoload::registerClass('ConfigboxElement',			KPATH_DIR_CB.DS.'classes'.DS.'ConfigboxElement.php');
KenedoAutoload::registerClass('ConfigboxOption',			KPATH_DIR_CB.DS.'classes'.DS.'ConfigboxOption.php');
KenedoAutoload::registerClass('ConfigboxConfiguration',		KPATH_DIR_CB.DS.'classes'.DS.'ConfigboxConfiguration.php');

if (function_exists('lcfirst') == false) {
	function lcfirst( $str ) {
		return (string)(strtolower(substr($str,0,1)).substr($str,1));
	}
}

if (function_exists('hsc') == false) {
	function hsc($string) {
		return htmlspecialchars($string,ENT_QUOTES);
	}
}

if (function_exists('cbprice') == false) {
	function cbprice($price, $symbol = true, $emptyOnZero = false, $decimals = 2) {
		if ($emptyOnZero && $price == 0) {
			return '';
		}
		else {
			return ConfigboxCurrencyHelper::getFormatted($price, $symbol, $decimals);
		}
	}
}

if (function_exists('cbtaxrate') == false) {
	function cbtaxrate($rate, $symbol = true) {
		return ConfigboxCurrencyHelper::getFormattedTaxRate($rate, $symbol);
	}
}

// In case JDump isn't installed create a dummy dump function
if (!function_exists('dump')) {
	function dump() {
		return true;
	}
}

// Set all path constants
ConfigboxConfigHelper::setPaths();

// Apply software updates
ConfigboxUpdateHelper::applyUpdates();

// Apply customization software updates
ConfigboxUpdateHelper::applyCustomizationUpdates();

// Load the override files
ConfigboxOverridesHelper::loadOverrideFiles();

// Load the configuration constants
ConfigboxConfigHelper::setConfig();

// Get Language Information
ConfigboxLanguageHelper::setLanguage();

// Get Currency Information
ConfigboxCurrencyHelper::setCurrency();

// Register the observers
ConfigboxObserverHelper::registerObservers();

// Run the data cleanup processes
KenedoModel::getModel('ConfigboxModelCleanup')->cleanUp();

// Add assets to page if HTML
if (KenedoPlatform::p()->getDocumentType() == 'html') {
	KenedoViewHelper::loadKenedoAssets();
	ConfigboxViewHelper::loadGeneralAssets();
}
