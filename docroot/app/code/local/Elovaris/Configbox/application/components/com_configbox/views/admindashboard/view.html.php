<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdmindashboard extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'admindashboard';

	/**
	 * @var object[] List of issues that absolutely need to be resolved
	 * @see ConfigboxModelAdmindashboard::getCriticalIssues
	 */
	public $criticalIssues;

	/**
	 * @var object[] List of issues that may cause problems
	 * @see ConfigboxModelAdmindashboard::getIssues
	 */
	public $issues;

	/**
	 * @var object[] List of performance tips
	 * @see ConfigboxModelAdmindashboard::getPerformanceTips
	 */
	public $performanceTips;

	/**
	 * @var object[] List of server stats
	 * @see ConfigboxModelAdmindashboard::getCurrentStats
	 */
	public $currentStats;

	/**
	 * @return ConfigboxModelAdmindashboard
	 * @throws Exception
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdmindashboard');
	}

	function prepareTemplateVars() {
				
		$dashboard = KenedoModel::getModel('ConfigboxModelAdmindashboard');

		$this->assign('criticalIssues', 	$dashboard->getCriticalIssues() );
		$this->assign('issues', 			$dashboard->getIssues() );
		$this->assign('performanceTips', 	$dashboard->getPerformanceTips() );
		$this->assign('currentStats', 		$dashboard->getCurrentStats() );
		
		$this->addViewCssClasses();

	}
	
}
