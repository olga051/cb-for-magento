<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $xref ConfigboxOption */
/** @var $this ConfigboxViewConfiguratorpage */
$xref = reset($this->element->options);
?>
<div id="<?php echo $this->element->cssId;?>" class="<?php echo $this->element->getCssClasses();?>">
	
	<?php if (ConfigboxPermissionHelper::canQuickEdit()) echo ConfigboxQuickeditHelper::renderElementButtons($this->element);?>
	
	<div class="heading">
		
		<h2 class="element-title"><?php echo hsc($this->element->title);?></h2>
		
		<?php if (!empty($this->element->description) && $this->element->desc_display_method == 1) { ?>
			<div class="element-description"><?php echo $this->element->description;?></div>
		<?php } ?>
		
		<?php if (!empty($this->element->description) && $this->element->desc_display_method == 2) { ?>
			<div class="element-description-tooltip"><?php echo KenedoHtml::getTooltip('<a class="fa fa-info-circle"></a>', $this->element->description);?></div>
		<?php } ?>
		
	</div>
	<div class="inputfield">
		<?php if ($this->element->el_image) { ?>
			<img class="<?php echo hsc($this->element->elementImageCssClasses);?>" src="<?php echo $this->element->elementImageSrc;?>" alt="<?php echo hsc($this->element->title);?>"<?php echo $this->element->elementImagePreloadAttributes;?> />
		<?php } ?>
		
		<div id="xrefwrapper-<?php echo (int)$xref->id;?>" class="<?php echo $xref->getCssClasses();?>">
		
			<?php if (ConfigboxPermissionHelper::canQuickEdit()) echo ConfigboxQuickeditHelper::renderXrefButtons($xref);?>
			
			<?php if ($xref->option_picker_image) { ?>
				<div class="configbox-image-button-wrapper">
					<img class="<?php echo $xref->pickerPreloadCssClasses;?>" src="<?php echo $xref->pickerImageSrc;?>" alt="<?php echo hsc($xref->title);?>"<?php echo $xref->pickerPreloadAttributes;?> />
				</div>
			<?php } ?>
			
			<input class="configbox-control configbox-control-checkbox" type="checkbox" id="xref-<?php echo (int)$xref->id;?>" name="element-<?php echo (int)$this->element->id;?>" value="<?php echo (int)$xref->id;?>" <?php echo $xref->disabled;?> <?php echo ($xref->isSelected) ? 'checked="checked"':'';?> />
			<label class="configbox-label configbox-label-checkbox" for="xref-<?php echo (int)$xref->id;?>">
				<span class="xref-title"><?php echo hsc($xref->title);?></span>
				
				<?php if (ConfigboxPermissionHelper::canSeePricing()) { ?>
					<span class="xref-price-display">
						<span class="xref-price-wrapper" <?php echo (!$xref->getPrice()) ? 'style="display:none"':'';?>>
							<?php if ($xref->getWasPrice() != 0) { ?>
								<span class="xref-was-price xref-was-price-<?php echo (int)$xref->id;?>"> <?php echo cbprice($xref->getWasPrice() );?></span> 
							<?php } ?>
							<span class="xref-price xref-price-<?php echo (int)$xref->id;?>"> <?php echo cbprice($xref->getPrice() );?></span> 
							<span class="xref-price-label xref-price-label-<?php echo (int)$xref->id;?>"><?php echo hsc($this->product->priceLabel);?></span>
						</span>
						<span class="xref-price-recurring-wrapper" <?php echo (!$xref->getPriceRecurring() ) ? 'style="display:none"':'';?>>
							<?php if ($xref->getWasPriceRecurring() != 0) { ?>
								<span class="xref-was-price-recurring xref-was-price-recurring-<?php echo (int)$xref->id;?>"> <?php echo cbprice($xref->getWasPriceRecurring() );?></span> 
							<?php } ?>
							<span class="xref-price-recurring xref-price-recurring-<?php echo (int)$xref->id;?>"><?php echo cbprice($xref->getPriceRecurring() );?></span>
							<span class="xref-price-recurring-label xref-price-recurring-label-<?php echo (int)$xref->id;?>"><?php echo hsc($this->product->priceLabelRecurring);?></span>
						</span>
					</span>							
				<?php } ?>
				
				<?php if ($xref->description) { ?>
					<span class="xref-description"><?php echo KenedoHtml::getTooltip(KText::_('Details'), $xref->description, 'top');?></span>
				<?php } ?>
				
				<?php if ($xref->available == 0) { ?>
					<span class="xref-available"><?php echo ( $xref->availibility_date ) ? KText::sprintf('Available on %s',$xref->availibility_date) : KText::_('Not available');?></span>
				<?php } ?>
				
				<?php if ( $xref->showReviews ) { ?>
					<div class="xref-reviews"><?php echo ConfigboxRatingsHelper::renderOptionRatingsPopup(KText::_('Reviews'), $this->element, $xref);?></div>
				<?php } ?>
				
			</label>
			<span class="position-loading-symbol"><span class="wheel"></span></span>
			<span class="validation-feedback"></span>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>