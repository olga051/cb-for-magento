<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewReview */
?>
<div class="view-reviews view-reviews-popup view-reviews-option">
	
	<div class="review-page<?php echo (count($this->reviews)) ? ' has-reviews':' has-no-reviews';?> review-type-<?php echo $this->kind;?> review-item-id-<?php echo $this->item->id;?>">
			
		<div class="review-page-with-description" style="display:<?php echo ($this->item->description) ? 'block':'none';?>">
			
			
			<div class="review-form" style="display:<?php echo (count($this->reviews)) ? 'none':'block';?>">
				<div class="form-part">
					<div class="headline"><?php echo KText::_('Add your Review');?></div>
					<div class="name"><input type="text" placeholder="<?php echo KText::_('Name');?>" /></div>
					<div class="comment"><textarea placeholder="<?php echo KText::_('Review');?>"></textarea></div>
					<div class="rating">
						<div class="rating-title"><?php echo KText::_('Your Rating');?></div>
						<div class="rating-stars-16"><?php echo ConfigboxRatingsHelper::renderRatingStars(3.5);?></div>
					</div>
					<div class="send-button"><a class="trigger-send-review button-frontend-small" data-kind="<?php echo hsc($this->kind);?>" data-itemid="<?php echo (int)$this->id;?>"><?php echo KText::_('Send');?></a></div>
				</div>
				<div class="feedback-part" style="display:none">
					<div class="feedback-message"><?php echo KText::_('Thank you for your review. Please allow some time for us to process your review.');?></div>
				</div>
			</div>
			
			<div class="item-information">
				
				<?php if ($this->item->image) { ?>
					<div class="item-image">
						<img src="<?php echo $this->item->image;?>" alt="<?php echo hsc($this->item->title);?>" />
					</div> 
				<?php } ?>
				
				<div class="item-title"><?php echo hsc($this->item->title);?></div>
				<div class="item-description"><?php echo $this->item->description;?></div>
				
				<?php if( count($this->reviews) == 0) { ?>
					<div class="no-reviews-yet"><?php echo hsc(KText::sprintf('There are no customer reviews for %s yet. Be the first to write one.',$this->item->title));?></div>
				<?php } ?>
					
			</div>
			
			<div class="short-review-list" style="display:<?php echo (count($this->reviews)) ? 'block':'none';?>">
				<h1><?php echo KText::_('Reviews');?></h1>
				
				<ul class="review-list">
					<?php
					$i = 1;
					foreach ($this->reviews as $review) {
						if ($i > $this->listCount) break;
						$i++;
						?>
						<li class="review">	
							<div class="comment">&quot;<?php echo hsc(mb_substr($review->comment,0,80));?>&quot;</div>
							<div class="name">- <?php echo hsc($review->name);?></div>
							<div class="rating"><?php echo ConfigboxRatingsHelper::renderRatingStars($review->rating);?></div>
						</li>
					<?php } ?>
				</ul>
				<?php if (count($this->reviews) > $this->listCount or true) { ?>
					<div class="more-reviews"><a class="trigger-read-reviews"><?php echo KText::sprintf('Read all %s customer reviews',count($this->reviews));?></a></div>
				<?php } ?>
				<div class="add-review"><a class="trigger-read-reviews"><?php echo KText::_('Add your review');?></a></div>
			</div>
			
			<div class="clear"></div>
		</div>
		
		<div class="review-page-long-list" style="display:<?php echo ($this->item->description) ? 'none':'block';?>">
		
			<div class="left-part">
				
				<div class="review-form">
					<div class="form-part">
						<div class="headline"><?php echo KText::_('Add your Review');?></div>
						<div class="name"><input type="text" placeholder="<?php echo KText::_('Name');?>" /></div>
						<div class="comment"><textarea placeholder="<?php echo KText::_('Review');?>"></textarea></div>
						<div class="rating">
							<div class="rating-title"><?php echo KText::_('Your Rating');?></div>
							<div class="rating-stars-16"><?php echo ConfigboxRatingsHelper::renderRatingStars(3.5);?></div>
						</div>
						<div class="send-button"><a class="button-send-review trigger-send-review" data-kind="<?php echo hsc($this->kind);?>" data-itemid="<?php echo (int)$this->id;?>"><?php echo KText::_('Send');?></a></div>
						<?php if ($this->item->description) { ?>
						<div class="back-button"><a class="trigger-back-to-description"><?php echo KText::_('‹ Back to Overview');?></a></div>
						<?php } ?>
					</div>
					<div class="feedback-part" style="display:none">
						<div class="feedback-message"><?php echo KText::_('Thank you for your review. Please allow some time for us to process your review.');?></div>
						
						<?php if ($this->item->description) { ?>
						<div class="back-button"><a class="trigger-back-to-description"><?php echo KText::_('‹ Back to Overview');?></a></div>
						<?php } ?>
						
					</div>
				</div>
				
			</div>
			
			<div class="right-part">
				
				<div class="long-review-list">
				
					<?php if (count($this->reviews) > 0) { ?>
					
						<h1><?php echo hsc( KText::sprintf('%s Reviews of %s',count($this->reviews), $this->item->title) );?></h1>
						<div class="height-limiter">
							<?php foreach ($this->reviews as $review) { ?>
								<li class="review">	
									<div class="comment">&quot;<?php echo hsc($review->comment);?>&quot;</div>
									<div class="name">- <?php echo hsc($review->name);?></div>
									<div class="rating"><?php echo ConfigboxRatingsHelper::renderRatingStars($review->rating);?></div>
									<div class="clear"></div>
								</li>
							<?php } ?>
						</div>
					<?php } else { ?>
						<div class="item-title"><?php echo hsc($this->item->title);?></div>
						<div class="no-reviews-yet"><?php echo hsc(KText::sprintf('There are no customer reviews for %s yet. Be the first to write one.',$this->item->title));?></div>
					<?php } ?>
				</div>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
	
</div>