<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewCart extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'cart';

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	/**
	 * @var object $cart Object holding cart data (using printr, JDump or similar is recommended)
	 * @see ConfigboxModelCart::getCartDetails()
	 */
	public $cart;

	/**
	 * @var boolean $displayPricing Indicates if pricing shall be shown. Depends on customer group settings (Show pricing)
	 */
	public $displayPricing;

	/**
	 * @var object $position Helper object to make life easier in template 'positioncontrols'. Is set during looping
	 * through the cart's positions
	 */
	public $position;

	/**
	 * @var array[] $positionUrls Array holding URLs for various actions like removing/copying/editing a position.
	 * Array is grouped per position.
	 */
	public $positionUrls;

	/**
	 * @var string[] $quantityFields Holds HTML for each position's quantity input (visibility and type of input depends
	 * on the products settings for quantity, see backend GUI for reference)
	 */
	public $quantityFields;

	/**
	 * @var boolean $canEditOrder The system looks if the cart is already connected to an order and tells if it can be edited
	 */
	public $canEditOrder;

	/**
	 * @var string $urlPositionFormAction The URL to use for form submissions on the cart page
	 */
	public $urlPositionFormAction;

	/**
	 * @var string $urlContinueShopping URL for 'Continue Shopping' links. Depends on ConfigBox setting in section 'Checkout'
	 */
	public $urlContinueShopping;

	/**
	 * @var string[] $positionImages The HTML for the product images or visualizations, listed by position ID.
	 */
	public $positionImages;

	/**
	 * @var string $urlPrintCart The URL for the print view
	 */
	public $urlPrintCart;

	/**
	 * @var bool $showPageHeading If the page heading should be shown (In Joomla that is controlled by the menu item parameters)
	 */
	public $showPageHeading;

	/**
	 * @var string $pageHeading The page heading
	 */
	public $pageHeading;

	/**
	 * @var boolean $canSaveOrder Indicates if the cart can be saved
	 */
	public $canSaveOrder;

	/**
	 * @var boolean $canCheckout Indicates if the user can checkout this cart
	 */
	public $canCheckout;

	/**
	 * @var boolean $canRequestQuote Indicates if the cart be used for a quote request
	 */
	public $canRequestQuote;

	/**
	 * @var string $urlSaveOrder URL to save an order
	 */
	public $urlSaveOrder;

	/**
	 * @var string $urlCheckout URL to checkout a cart
	 */
	public $urlCheckout;

	/**
	 * @var string $urlGetQuotation URL to request a quote
	 */
	public $urlGetQuotation;

	/**
	 * @var boolean $isB2b Indicates if B2B mode is on. Simply depends on group settings, it's read from the cart data.
	 */
	public $isB2b;

	function display() {

		$this->prepareTemplateVars();

		// Do the empty cart template if we got an empty cart
		if (empty($this->cart) || empty($this->cart->positions)) {
			$this->renderView('emptycart');
			return;
		}
		else {
			$this->renderView();
		}

	}

	function prepareTemplateVars($tpl = null){

		// Get the cart model
		$cartModel = KenedoModel::getModel('ConfigboxModelCart');

		// Check if we got a cart_id parameter in (if not, it's fine, we use the session var)
		if (KRequest::getInt('cart_id')) {
			$cartId = KRequest::getInt('cart_id');
			if ($cartModel->cartBelongsToUser($cartId)) {
				$cartModel->setId($cartId);
			}
		}

		// Set robots meta tag
		KenedoPlatform::p()->setMetaTag('robots','noindex');

		// Get page heading stuff
		$params = KenedoPlatform::p()->getAppParameters();
		$this->assignRef('params',$params);
		$this->assign('showPageHeading', ($params->get('show_page_heading', 1) && $params->get('page_title','') != ''));
		$this->assign('pageHeading', $params->get('page_title',''));

		// Prepare urls
		$this->assign('urlPrintCart', KLink::getRoute('index.php?option=com_configbox&view=cart&tmpl=component&print=1'));
		$this->assign('urlPositionFormAction', KLink::getRoute('index.php?option=com_configbox&view=cart'));
		$this->assign('urlContinueShopping', KLink::getRoute('index.php?option=com_configbox&view=productlisting&listing_id='.CONFIGBOX_CONTINUE_LISTING_ID));

		// Get the cart ID
		$cartId = $cartModel->getId();

		// Do the empty cart template if nothing was found
		if (!$cartId) {
			return;
		}
		else {
			$this->assign('cartId', $cartId);
		}

		// Check if the cart belongs, just in case
		if ($cartModel->cartBelongsToUser($cartId) == false) {
			$cartModel->resetCart();
			$this->assign('cartId', 0);
			return;
		}
		
		$cartDetails = $cartModel->getCartDetails();

		// Do the empty cart template if we got an empty cart
		if (empty($cartDetails) || empty($cartDetails->positions)) {
			return;
		}

		// For convenience, store if we deal with B2B mode
		$this->assign('isB2b', ($cartDetails->groupData->b2b_mode == 1));

		// Assign the cart details
		$this->assignRef('cart', $cartDetails);

		// Prepare all permissions
		$canCheckout 			= ConfigboxOrderHelper::isPermittedAction('checkoutOrder',$cartDetails);
		$canEdit 				= ConfigboxOrderHelper::isPermittedAction('editOrder',$cartDetails);
		$canSave 				= ConfigboxOrderHelper::isPermittedAction('saveOrder',$cartDetails);
		$canRemoveCart 			= ConfigboxOrderHelper::isPermittedAction('removeCart',$cartDetails);
		$canRequestQuote 		= ConfigboxOrderHelper::isPermittedAction('requestQuote',$cartDetails);

		// Assign them
		$this->assignRef('canCheckout', $canCheckout);
		$this->assignRef('canEditOrder', $canEdit);
		$this->assignRef('canSaveOrder', $canSave);
		$this->assignRef('canRemoveCart', $canRemoveCart);
		$this->assignRef('canRequestQuote', $canRequestQuote);

		// Prepare cart button URLs
		$urlGetQuotation = KLink::getRoute('index.php?option=com_configbox&view=rfq&cart_id='.$cartDetails->id);
		$urlSaveOrder = KLink::getRoute('index.php?option=com_configbox&view=saveorder&cart_id='.$cartDetails->id);
		$urlCheckout = KLink::getRoute('index.php?option=com_configbox&controller=cart&task=checkoutCart&cart_id='.$cartDetails->id);

		// Assign them
		$this->assignRef('urlGetQuotation', $urlGetQuotation);
		$this->assignRef('urlSaveOrder', $urlSaveOrder);
		$this->assignRef('urlCheckout', $urlCheckout);

		// Deal with the quantity field settings for the product
		$quantityFields = array();
		foreach ($cartDetails->positions as $positionId=>$position) {
			$configuration = ConfigboxConfiguration::getInstance($positionId);

			if ($position->productData->quantity_element_id && $configuration->getSelection($position->productData->quantity_element_id)) {
				$element = ConfigboxElementHelper::getElement( $position->productData->quantity_element_id );

				if (count($element->options)) {
					$selectedValue = NULL;
					$selectOptions = array();
					foreach ($element->options as $option) {
						if (!empty($option->assignment_custom_1)) {
							$selectOptions[$option->id] = $option->title;
						}
						if ($selectedValue === NULL && $option->assignment_custom_1 == $position->quantity) {
							$selectedValue = $option->id;
						}
					}
					$quantityFields[$position->id] = KenedoHtml::getSelectField('quantity', $selectOptions, $selectedValue, NULL, false, 'input-product-quantity', 'quantity-'.$position->id);
					continue;
				}

			}
			elseif ($position->productData->alt_quantity_element_id && $configuration->getSelection($position->productData->alt_quantity_element_id)) {
				$element = ConfigboxElementHelper::getElement( $position->productData->alt_quantity_element_id );

				if (count($element->options)) {
					$selectedValue = NULL;
					$selectOptions = array();
					foreach ($element->options as $option) {
						if (!empty($option->assignment_custom_1)) {
							$selectOptions[$option->id] = $option->title;
						}
						if ($selectedValue === NULL && $option->assignment_custom_1 == $position->quantity) {
							$selectedValue = $option->id;
						}
					}
					$quantityFields[$position->id] = KenedoHtml::getSelectField('quantity', $selectOptions, $selectedValue, NULL, false, 'input-product-quantity', 'quantity-'.$position->id);
					continue;
				}
			}

			$quantityFields[$position->id] = KenedoHtml::getTextField('quantity', intval($position->quantity), 'quantity-'.$position->id, 'input-product-quantity');

		}
		unset($position);
		$this->assignRef('quantityFields',$quantityFields);

		// Prepare position button urls
		$positionUrls = array();
		foreach ($cartDetails->positions as $positionId=>$position) {
			$positionUrls[$positionId]['urlRemove'] = KLink::getRoute('index.php?option=com_configbox&controller=cart&task=removeCartPosition&cart_position_id='.intval($position->id));
			$positionUrls[$positionId]['urlEdit'] = KLink::getRoute('index.php?option=com_configbox&controller=cart&task=editCartPosition&cart_position_id='.intval($position->id));
			$positionUrls[$positionId]['urlCopy'] = KLink::getRoute('index.php?option=com_configbox&controller=cart&task=copyCartPosition&cart_position_id='.intval($position->id));
		}
		$this->assignRef('positionUrls',$positionUrls);
		unset($position);

		$positionImages = array();
		foreach($cartDetails->positions as $position) {
			$positionImages[$position->id] = ConfigboxProductImageHelper::getProductImageHtml($position, 300, 250);
		}
		$this->assignRef('positionImages', $positionImages);

		// Add some styling if on print
		if (KRequest::getInt('print')) {
			$css = "body{background:#fff!important} #view-cart { width:19cm} #view-cart .cart-buttons, #view-cart .controls, #view-cart .print-link { display:none!important}";
			KenedoPlatform::p()->addStyleDeclaration($css);
		}

		// Prepare displayPricing
		$this->assign('displayPricing', ConfigboxPermissionHelper::canSeePricing());

	}
}
