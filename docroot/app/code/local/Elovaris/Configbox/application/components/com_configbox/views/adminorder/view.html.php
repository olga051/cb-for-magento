<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdminorder extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'adminorders';

	/**
	 * @var object $orderRecord as from ConfigboxModelOrderrecord::getOrderRecord()
	 * @see ConfigboxModelOrderrecord::getOrderRecord()
	 */
	public $orderRecord;

	/**
	 * @var string HTML for the order record
	 * @see ConfigboxViewRecord
	 */
	public $orderRecordHtml;

	/**
	 * @var string HTML for the order status dropdown
	 */
	public $statusSelect;

	/**
	 * @var object Data about the invoice.
	 * @see ConfigboxModelInvoice::getInvoiceData
	 */
	public $invoiceData;

	/**
	 * @var string Invoice prefix
	 * @see ConfigboxModelInvoice::getInvoicePrefix
	 */
	public $invoicePrefix;

	/**
	 * @var string The next possible invoice serial
	 * @see ConfigboxModelInvoice::getNextInvoiceSerial
	 */
	public $nextInvoiceSerial;

	/**
	 * @var object[] Array holding the order's files
	 * @see ConfigboxModelOrderfiles::getOrderFiles
	 */
	public $orderFiles;

	/**
	 * @var boolean Indicates if the link to the platform user page should be shown. Depends on if customers get linked on the platform and if it's possible to link there directly.
	 */
	public $showPlatformUserEditLink;

	/**
	 * @var string Complete URL to the platform user's edit page.
	 */
	public $urlPlatformUserEditForm;

	/**
	 * @return ConfigboxModelOrderrecord
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelOrderrecord');
	}

	function prepareTemplateVars() {

		// Get the requested record record ID
		$cid = KRequest::getArray('cid');
		if ($cid) {
			$orderRecordId = $cid[0];
		}
		else {
			$orderRecordId = KRequest::getInt('id');
		}

		// If order files uploads are enables, get data about uploaded files into the template
		if (CONFIGBOX_ENABLE_FILE_UPLOADS) {
			$filesModel = KenedoModel::getModel('ConfigboxModelOrderfiles');
			$files = $filesModel->getOrderFiles($orderRecordId);
			$this->assignRef('orderFiles', $files);
		}
		
		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');

		// Get invoice data in there
		$invoiceModel = KenedoModel::getModel('ConfigboxModelInvoice');
		$invoiceData = $invoiceModel->getInvoiceData($orderRecordId);
		$this->assignRef('invoiceData', $invoiceData);

		// Info about the next invoice serial
		$nextInvoiceSerial = $invoiceModel->getNextInvoiceSerial();
		$this->assignRef('nextInvoiceSerial', $nextInvoiceSerial);

		// The invoice prefix
		$invoicePrefix = $invoiceModel->getInvoicePrefix();
		$this->assignRef('invoicePrefix', $invoicePrefix);

		// Get the order record
		$orderRecord = $orderModel->getOrderRecord($orderRecordId);
		$this->assignRef('orderRecord', $orderRecord);

		// Get the HTML for the order record
		if ($orderRecord) {
			// Get the order record view HTML
			$orderRecordView = KenedoView::getView('ConfigboxViewRecord');
			$orderRecordView->assignRef('orderRecord', $orderRecord);
			$orderRecordView->assign('showIn', 'shopmanager');
			$orderRecordView->assign('showChangeLinks', false);
			$orderRecordView->assign('showProductDetails', true);
			$orderRecordHtml = $orderRecordView->getViewOutput();
			$this->assignRef('orderRecordHtml', $orderRecordHtml);
		}
		else {
			$this->assign('orderRecordHtml', '');
		}

		// In some platforms, we can bring the user to the customer's platform user account page
		$userEditFormReachable = KenedoPlatform::p()->platformUserEditFormIsReachable();
		$userCanEditUsers = KenedoPlatform::p()->userCanEditPlatformUsers();
		$this->assign('platformUserEditFormIsReachable', $userEditFormReachable);
		$this->assign('userCanEditPlatformUsers', $userCanEditUsers);

		// Get the link in
		if ($userEditFormReachable && $userCanEditUsers && !empty($orderRecord->orderAddress->platform_user_id)) {
			$this->assign('showPlatformUserEditLink', true);			
			$this->assignRef('urlPlatformUserEditForm', KenedoPlatform::p()->getPlatformUserEditUrl($orderRecord->orderAddress->platform_user_id));
		}
		else {
			$this->assign('showPlatformUserEditLink', false);
		}

		// Status drop-down, used for changing order status
		$statusSelect = $orderModel->getStatusDropDown($orderRecord->status);
		$this->assign('statusSelect',$statusSelect);

		// And the view CSS classes
		$this->addViewCssClasses();

	}
	
}