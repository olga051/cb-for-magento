<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdmincalcformula_elementattribute extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var array[] Array of array of xref data objects, grouped by element ID then xref ID
	 * @see ConfigboxRulesHelper::getXrefs
	 */
	public $elementXrefs;

	/**
	 * @var object[] Array of xref data objects, grouped by element ID
	 * @see ConfigboxRulesHelper::getElements
	 */
	public $elements;

	/**
	 * @var array[] Infos about usable element attributes (like selected option, custom fields etc)
	 * @see ConfigboxConditionElementAttribute::getElementAttributes
	 */
	public $elementAttributes;

	/**
	 * @var int Product ID used for filtering the element conditions
	 */
	public $selectedProductId;

	/**
	 * @var int Page ID used for filtering the element conditions
	 */
	public $selectedPageId;

	/**
	 * @var string Dropdown HTML for the product filter
	 */
	public $productFilterHtml;

	/**
	 * @var string[] Array of HTML with dropdowns for page filtering, grouped by product ID
	 */
	public $pageFilterDropdowns;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {

		$selectedPageId = KRequest::getInt('filter_page_id', 0);

		// Get the page id for filtering
		$this->assign('selectedPageId', $selectedPageId);

		$products = ConfigboxRulesHelper::getProductsForFilter();

		// Get the product id for filtering
		$selectedProductId = 0;
		if ($selectedPageId) {
			// With a page filter in place, get the corresponding product id
			$selectedProductId = ConfigboxRulesHelper::getProductIdForPage($selectedPageId);
		}
		else {
			// Otherwise get the first product id
			foreach ($products as $item) {
				$selectedProductId = $item->id;
				break;
			}
		}
		$this->assign('selectedProductId', $selectedProductId);


		$productFilterOptions = array();
		foreach ($products as $item) {
			$productFilterOptions[$item->id] = $item->title;
		}
		$productFilterHtml = KenedoHtml::getSelectField('product-filter', $productFilterOptions, $selectedProductId);
		$this->assign('productFilterHtml', $productFilterHtml);


		// Prepare the filter drop-downs for pages
		$pages = ConfigboxRulesHelper::getPagesForFilter();

		$pageFilterDropdowns = array();
		foreach ($pages as $productId => $pageCollection) {
			$filterOptions = array();
			$filterOptions[0] = KText::_('All pages');
			
			foreach ($pageCollection as $page) {
				$filterOptions[$page->page_id] = $page->page_title;
			}
			$cssClasses = 'page-filter page-filter-'. $productId;
			$pageFilterDropdowns[$productId] = KenedoHtml::getSelectField('page-filter-'. $productId, $filterOptions, $selectedPageId, 0, false, $cssClasses);
		}
		
		$this->assign('pageFilterDropdowns', $pageFilterDropdowns);


		// Prepare the element list
		$elements = ConfigboxRulesHelper::getElements();
		$this->assignRef('elements', $elements);
		
		// Prepare the element attribute list
		$elementAttributes = ConfigboxCalcTerm::getTerm('ElementAttribute')->getElementAttributes();
		$this->assignRef('elementAttributes', $elementAttributes);

		// Prepare the xref list for element conditions
		$xrefs = ConfigboxRulesHelper::getXrefs();

		// Group the xrefs in elements
		$elementXrefs = array();
		foreach ($xrefs as $xref) {
			$elementXrefs[$xref->element_id][$xref->xref_id] = $xref;
		}
		$this->assignRef('elementXrefs', $elementXrefs);

		$this->addViewCssClasses();

	}
	
}
