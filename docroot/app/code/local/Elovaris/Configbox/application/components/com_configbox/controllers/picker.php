<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerPicker extends KenedoController {

	/**
	 * @return NULL
	 */
	protected function getDefaultModel() {
		return NULL;
	}

	/**
	 * @return ConfigboxViewPicker
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewPicker');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

}