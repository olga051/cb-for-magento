<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCart */
?>
<div id="com_configbox">
	<div id="view-cart">
		<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

			<?php if ($this->showPageHeading) { ?>
				<h1 class="componentheading cart-page-title"><?php echo hsc($this->pageHeading);?></h1>
			<?php } ?>

			<div class="empty-cart-notice">
				<p><?php echo KText::_('You have no products in your cart.');?></p>
			</div>

			<div class="cart-buttons">
				<a rel="nofollow" class="floatleft navbutton-medium back continue-shopping" href="<?php echo $this->urlContinueShopping;?>">
					<span class="nav-center"><?php echo KText::_('Continue Shopping')?></span>
				</a>
				<div class="clear"></div>
			</div>

		</div>
	</div>
</div>