<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyTranslatable extends KenedoProperty {
	
	function __construct($propertyDefinition, $model) {
		parent::__construct($propertyDefinition, $model);
		
		if (isset($this->propertyDefinition['optionTags']['USE_TEXTAREA']) || isset($this->propertyDefinition['optionTags']['USE_HTMLEDITOR'])) {
			$this->cssClasses[] = 'using-editors';
		}
	}
	
	function getDataFromRequest( &$data ) {
		
		$languages = KenedoLanguageHelper::getActiveLanguages();
		
		foreach ($languages as $language) {
				
			$key = $this->propertyName .'-'.$language->tag;
			
			if (KRequest::getVar($key,NULL) === NULL) {
				$data->$key = NULL;
			}
			else {
				if (isset($this->propertyDefinition['optionTags']['ALLOW_RAW'])) {
					$data->$key = KRequest::getVar($key,'','METHOD');
				}
				elseif (isset($this->propertyDefinition['optionTags']['ALLOW_HTML'])) {
					$data->$key = KRequest::getHtml($key,'');
				}
				else {
					$data->$key = KRequest::getString($key,'');
				}
			}
			
		}
		
		if (isset($data->{$this->propertyName})) unset( $data->{$this->propertyName} );
		
	}

	function check($data) {

		$this->resetErrors();

		// If field is required, go through all languages, check if values are set
		if ($this->isRequired()) {

			$languages = KenedoLanguageHelper::getActiveLanguages();

			foreach ($languages as $language) {
				$dataFieldKey = $this->propertyName .'-'.$language->tag;
				if (empty($data->$dataFieldKey) && $data->$dataFieldKey !== '0') {
					$this->setError(KText::sprintf('Field %s cannot be empty.',$this->propertyDefinition['label']));
					return false;
				}
			}

		}

		return true;

	}

	function store(&$data) {
		
		$db = KenedoPlatform::getDb();
		
		$languages = KenedoLanguageHelper::getActiveLanguages();
		
		foreach ($languages as $language) {
			$type = $this->propertyDefinition['langType'];
			$key = $data->id;
			$dataFieldKey = $this->propertyName .'-'.$language->tag;
			$text = $data->$dataFieldKey;
			
			$query = "REPLACE INTO `".$this->propertyDefinition['stringTable']."` SET `type` = ".(int)$type.", `key` = ".(int)$key.", `lang_id` = 0, `language_tag` = '".$db->getEscaped($language->tag)."', `text` = '".$db->getEscaped($text)."'";
			$db->setQuery($query);
			$db->query();
			
			unset($data->$dataFieldKey);
		}
		
		return true;
		
	}
	
	function delete($id, $tableName) {
		
		$db = KenedoPlatform::getDb();
		$query = "DELETE FROM `".$this->propertyDefinition['stringTable']."` WHERE `type` = ".(int) $this->propertyDefinition['langType']." AND `key` = ".(int)$id;
		$db->setQuery($query);
		$succ = $db->query();
		return $succ;
		
	}

	function getJoinQuerySelectsAndJoins($selectAlias = '', $joinField = '') {
		
		$db = KenedoPlatform::getDb();
		
		if (!$selectAlias) {
			$selectAlias = $this->propertyDefinition['name'];
		}
	
		if (!$joinField) {
			$joinField = 'a.id';
		}
	
		$joinAlias = 'str_'.$selectAlias;
	
		$response['selects'][] = "`".$joinAlias."`.`text` AS `".$selectAlias."`";
		$response['joins'][] = 'LEFT JOIN `'.$this->propertyDefinition['stringTable'].'` AS `'.$joinAlias.'` ON `'.$joinAlias.'`.`key` = '.$joinField.' AND `'.$joinAlias.'`.`type` = '.(int)$this->propertyDefinition['langType'].' AND `'.$joinAlias.'`.`language_tag` = \''.$db->getEscaped(KText::$languageTag).'\'';
	
		return $response;
	}

	public function getSelectsForGetRecord($selectAliasPrefix = '', $selectAliasOverride = '') {

		$selectAlias = ($selectAliasOverride) ? $selectAliasPrefix.$selectAliasOverride : $selectAliasPrefix.$this->getSelectAlias();

		$select = $this->getTableAlias().".".$this->getTableColumnName()." AS `".$selectAlias."`";

		return array($select);
	}

	public function getJoinsForGetRecord() {
		$db = KenedoPlatform::getDb();
		$tableAlias = $this->getTableAlias();

		$joins = array();
		$joins[$tableAlias] = "LEFT JOIN `".$this->getPropertyDefinition('stringTable')."` AS `".$tableAlias."` ON
		`".$tableAlias."`.key = `".$this->model->getModelName()."`.`".$this->model->getTableKey()."` AND
		`".$tableAlias."`.type = '".$this->getPropertyDefinition('langType')."' AND
		`".$tableAlias."`.language_tag = '".$db->getEscaped(KText::$languageTag)."'";

		return $joins;
	}

	/**
	 * Adds translations since queries have a limit on how many joins can be done
	 * @param $data
	 */
	public function appendDataForGetRecord(&$data) {

		$tags = KenedoLanguageHelper::getActiveLanguageTags();
		foreach ($tags as $tag) {
			$fieldName = $this->propertyName.'-'.$tag;
			$data->$fieldName = ConfigboxCacheHelper::getTranslation($this->propertyDefinition['stringTable'], $this->propertyDefinition['langType'], $data->id, $tag);
		}

	}

	/**
	 * @return string|string[]
	 */
	public function getFilterName() {

		if ($this->getPropertyDefinition('filter', false) == false && $this->getPropertyDefinition('search', false) == false) {
			return '';
		}

		return $this->getTableAlias().'.'.$this->getTableColumnName();

	}

	/**
	 * @return string
	 */
	public function getSelectAlias() {
		return $this->propertyName;
	}

	/**
	 * @return string
	 */
	public function getTableColumnName() {
		return 'text';
	}

	/**
	 * @return string
	 */
	public function getTableAlias() {
		return 'translation_'.$this->model->getModelName().'_'.$this->propertyName;
	}

}