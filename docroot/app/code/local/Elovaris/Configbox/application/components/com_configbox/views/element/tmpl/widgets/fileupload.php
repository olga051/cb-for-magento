<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */
?>
<div id="configbox-widget-display-<?php echo (int)$this->element->id;?>" class="configbox-widget-fileupload-display">
	<?php echo $this->element->getOutputValue();?>
</div>
<div id="element-widget-<?php echo (int)$this->element->id;?>" class="configbox-widget configbox-widget-fileupload">
	<form target="element-widget-fileupload-iframe-<?php echo (int)$this->element->id;?>" id="configbox-widget-fileupload-form-<?php echo (int)$this->element->id;?>" class="configbox-widget-fileupload-form" enctype="multipart/form-data" action="<?php KLink::getRoute('index.php');?>" method="post">
		<div>
			<input type="file" class="configbox-widget-fileupload-filefield" name="configbox-widget-fileupload-filefield" <?php echo (!$this->element->applies()) ? 'disabled="disabled"':'';?> />
			<input type="submit" class="configbox-widget-fileupload-submit" value="<?php echo KText::_('Upload');?>" <?php echo (!$this->element->applies()) ? 'disabled="disabled"':'';?> />
			<input type="hidden" name="lang" value="<?php echo hsc(KText::$languageCode);?>" />
			<input type="hidden" name="option" value="com_configbox" />
			<input type="hidden" name="controller" value="json" />
			<input type="hidden" name="task" value="updateElement" />
			<input type="hidden" name="format" value="json" />
			<input type="hidden" name="element_id" value="<?php echo (int)$this->element->id;?>" />
			<input type="hidden" name="value" value="file" />
			<input type="hidden" name="confirmed" value="0" />
			<input type="hidden" name="file-upload" value="1" />
			<input type="hidden" name="prod_id" value="<?php echo KRequest::getInt('prod_id');?>" />
			<input type="hidden" name="page_id" value="<?php echo KRequest::getInt('page_id');?>" />
		</div>
	</form>
	<iframe style="display:none" src="about:blank" class="element-widget-fileupload-iframe" id="element-widget-fileupload-iframe-<?php echo (int)$this->element->id;?>" name="element-widget-fileupload-iframe-<?php echo (int)$this->element->id;?>"></iframe>
</div>