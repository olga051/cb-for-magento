<?php
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this ConfigboxViewAdminRuleeditor
 */
?>

<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>" data-return-field-id="<?php echo hsc($this->returnFieldId);?>">
	
	<div id="buttons">
	
		<div class="floatright">
			<a class="backend-button-small button-limit-condition-width"><?php echo KText::_('Limit condition width');?></a>
			<a class="backend-button-small button-put-in-brackets"><?php echo KText::_('Put in brackets');?></a>
			<a class="backend-button-small button-remove-selected-items"><?php echo KText::_('Remove selected');?></a>
			<a class="backend-button-small button-store"><?php echo KText::_('Save');?></a>
			<a class="backend-button-small button-cancel"><?php echo KText::_('Cancel');?></a>	
		</div>
		
		<div id="combinator-blueprints">
			<span class="item combinator" data-type="combinator" data-kind="AND"><?php echo KText::_('AND');?></span>
			<span class="item combinator" data-type="combinator" data-kind="OR"><?php echo KText::_('OR');?></span>		
		</div>
		
	</div>

	<fieldset id="compability-rule">
		<legend><?php echo KText::_('Compatibility Rule');?></legend>
		<div id="rule">
			<?php if ($this->ruleIsSet) { ?>
				<?php echo $this->ruleHtml;?>
			<?php } else { ?>
				<span class="drop-area initial"><?php echo KText::_('Pick an element below and drag conditions in this area.');?></span>
			<?php } ?>
		</div>
	</fieldset>
	
	<fieldset id="item-picker">
		<legend><?php echo KText::_('Add a new condition');?></legend>
		
		<div class="picker-tabs">
			<ul>
				<?php foreach ($this->conditionTabs as $typeName=>$tabTitle) { ?>
					<li class="picker-tab panel-<?php echo strtolower($typeName);?><?php echo ($typeName == $this->selectedTypeName) ? ' selected-tab':'';?>" id="panel-<?php echo strtolower($typeName);?>">
						<?php echo hsc($tabTitle);?>
					</li>
				<?php } ?>
			</ul>
		</div>

		<div class="picker-panels">
			<?php foreach ($this->conditionPanels as $typeName=>$content) { ?>
				<div class="panel panel-<?php echo strtolower($typeName);?><?php echo ($typeName == $this->selectedTypeName) ? ' selected-panel':'';?>">
					<?php echo $content;?>
				</div>
			<?php } ?>
			<div class="clear"></div>
		</div>

	</fieldset>
	
	<div id="operator-picker-blueprint">
		
		<div class="operator-picker">
			<div data-operator="<?php echo htmlentities('<');?>" class="operator operator-less-than"><?php echo KText::_('is below');?></div>
			<div data-operator="<?php echo htmlentities('<=');?>" class="operator operator-less-than-equals"><?php echo KText::_('is or below');?></div>
			<div data-operator="<?php echo htmlentities('==');?>" class="operator operator-is"><?php echo KText::_('is');?></div>
			<div data-operator="<?php echo htmlentities('!=');?>" class="operator operator-is-not"><?php echo KText::_('is not');?></div>
			<div data-operator="<?php echo htmlentities('>=');?>" class="operator operator-greater-than-equals"><?php echo KText::_('is or above');?></div>
			<div data-operator="<?php echo htmlentities('>');?>" class="operator operator-greater-than"><?php echo KText::_('is above');?></div>
		</div>
		
	</div>

	<div class="hidden-helpers">
		<div class="textfield-autosize"><span id="width-tester"></span></div>
	</div>

</div>

