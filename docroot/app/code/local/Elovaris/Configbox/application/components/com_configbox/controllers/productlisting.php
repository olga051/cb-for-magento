<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerProductlisting extends KenedoController {

	/**
	 * Returns the model to be used for standard tasks
	 *
	 * @return ConfigboxModelProductlisting
	 */
	protected function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelProductlisting');
	}

	/**
	 * Returns the KenedoView subclass for displaying arbitrary content
	 *
	 * @return ConfigboxViewProductlisting
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewProductlisting');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

}