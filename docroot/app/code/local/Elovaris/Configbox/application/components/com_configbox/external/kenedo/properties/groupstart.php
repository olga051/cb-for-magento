<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyGroupstart extends KenedoProperty {
	
	function usesWrapper() {
		return false;
	}

	function getDataFromRequest(&$data) {
		if ($this->getPropertyDefinition('toggle')) {
			$sessionKey = $this->getSessionKey();
			$defaultState = $this->getPropertyDefinition('default-state', 'closed');
			$value = KRequest::getKeyword('toggle-state-'.$this->propertyName, $defaultState);
			KSession::set($sessionKey, $value, 'kenedo');
		}
	}

	public function getSelectsForGetRecord($selectAliasPrefix = '', $selectAliasOverride = '') {
		return array();
	}

	public function getJoinsForGetRecord() {
		return array();
	}

	protected function getSessionKey() {
		return 'toggles.'. $this->model->getModelName() .'.'. $this->propertyName;
	}

}