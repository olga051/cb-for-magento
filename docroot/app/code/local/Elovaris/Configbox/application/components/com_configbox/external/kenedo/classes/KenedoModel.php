<?php
class KenedoModel {

	/**
	 * @var string $componentName Component that handles tasks on the data of the model. Set it unless the model uses a KenedoEntity
	 * @see KenedoModel::getModel(), KenedoModel::__construct()
	 */
	public $componentName = '';

	/**
	 * @var KenedoModel[] $instances Array holding all singleton instances of the KenedoModels
	 * @see KenedoModel::getModel()
	 */
	static $instances;

	/**
	 * @var string[] $errors Array holding strings of error messages.
	 * @see KenedoModel::getErrors(), KenedoModel::setErrors() and similar
	 */
	protected $errors;

	/**
	 * Memo-cached values for getPropertyObject
	 * @see KenedoModel::getPropertyObject()
	 * @var string[]
	 */
	protected static $memoPropertyClassNames = array();

	protected $memoGetProperties = array();

	/**
	 * Memoized results of getRecord
	 * @see KenedoModel::getRecord
	 * @var object[]
	 */
	protected $memoGetRecord = array();

	/**
	 * @return string Table used for storage
	 */
	function getTableName() {
		return '';
	}

	/**
	 * @return string Name of the table's primary key
	 */
	function getTableKey() {
		return '';
	}

	/**
	 * Returns a nested array with infos about where records of that type are used
	 *
	 * Example:
	 *
	 * $usageInfo = array(
	 *		'componentName' => array(
	 *			'modelName'=> array(
	 *				array(
	 *					'titleField'	=> 'Name of the property that names the record',
	 *					'fkField'		=> 'Name of the property that holds the foreign key value',
	 *					'controller'	=> 'Name of the controller dealing with the type of record',
	 *					'name'			=> 'Description of the field using the entry. E.g. Element weight calculation',
	 *				),
	 *			),
	 *		),
	 *	);
	 *
	 * @return array
	 */
	function getRecordUsageInfo() {
		return array();
	}

	/**
	 *
	 * Get's you a singleton instance of a model. $className follows a convention that indicates the location of the
	 * model's class file (location can be specified with $path if necessary).
	 *
	 * Convention:
	 * ConfigboxModelName - Configbox brings you in the right component's model folder, Model does nothing, Name get's
	 * the filename. Looks into the custom model folder when a system model is not found.
	 *
	 * @param string $className
	 * @param string $path
	 *
	 * @throws Exception if $className is empty or just containing whitespace
	 * @throws Exception if the requested model cannot be found
	 *
	 * @return KenedoModel $model A KenedoModel subclass object
	 *
	 * @see InterfaceKenedoPlatform::getComponentDir(), CONFIGBOX_DIR_CUSTOMIZATION
	 *
	 */
	static function getModel($className, $path = '') {

		// Legacy, remove with 2.7 for old client method (frontend made it load from components frontend folder, backend from backend - ignored in 2.6.0)
		if ($path == 'frontend' || $path == 'backend') {
			KLog::logLegacyCall('Method no longer uses frontend and backend as $path parameter. Use path to model file instead if necessary.');
			$path = '';
		}

		// Legacy, remove with 2.7
		if ($className == 'CbcheckoutModelOrder') {
			KLog::logLegacyCall('Model CbcheckoutModelOrder is now called ConfigboxModelOrderrecord');
			$className = 'ConfigboxModelOrderrecord';
		}
		// Legacy, remove with 2.7
		if ($className == 'ConfigboxModelGrandorder') {
			KLog::logLegacyCall('Model ConfigboxModelGrandorder is now called ConfigboxModelCart');
			$className = 'ConfigboxModelCart';
		}
		// Legacy, remove with 2.7
		if ($className == 'ConfigboxModelOrder') {
			KLog::logLegacyCall('Model ConfigboxModelOrder is now called ConfigboxModelCartposition');
			$className = 'ConfigboxModelCartposition';
		}

		// Abort in case $className is empty
		if (trim($className) == '') {
			$logMessage = 'KenedoModel::getModel called with empty className parameter';
			$identifier = KLog::log($logMessage, 'error');
			$publicMessage = 'KenedoModel::getModel called with empty className parameter. See error log file (Identifier: '.$identifier.').';
			throw new Exception($publicMessage);
		}
		
		if (!isset(self::$instances[$className])) {

			// Figure out component name and file name
			$component = 'com_' . strtolower( substr($className, 0, strpos($className, 'Model') ) );
			$modelFileName = strtolower( substr($className, strpos($className, 'Model') + 5 ) ) . '.php';
			
			// MERGELECACY - any com_cbcheckout is overwritten to com_configbox - logging is done later in this method
			if ($component == 'com_cbcheckout') {
				$component = 'com_configbox';
			}

			// Get the complete path to the regular and custom model
			$regularPath = KenedoPlatform::p()->getComponentDir($component) .DS. 'models' .DS. $modelFileName;
			$customPath = CONFIGBOX_DIR_CUSTOMIZATION .DS. 'models' .DS. $modelFileName;

			// Overwrite $path to get the file from either customization or system
			if ($path == '') {

				if (is_file($regularPath)) {
					$path = $regularPath;
				}
				elseif (is_file($customPath)) {
					$path = $customPath;
				}

			}

			// Abort if the model file cannot be found anywhere
			if (!is_file($path)) {
				$logMessage = 'Model file for class "'.$className.'" not found in path "'.$path.'".';
				$identifier = KLog::log($logMessage, 'error');
				$publicMessage = 'Model file for class "'.$className.'" not found. See error log file (Identifier: '.$identifier.').';
				throw new Exception($publicMessage);
			}

			// Load the model file
			require_once($path);

			// MERGELECACY - The model's class name may still use CbcheckoutModel, offer temporary backwards compatibility and log
			if (class_exists($className) == false) {
				$legacyClassName = str_replace('ConfigboxModel','CbcheckoutModel',$className);
				if (class_exists($legacyClassName)) {
					$className = $legacyClassName;
					KLog::logLegacyCall('Change custom class names in customization/models from CbcheckoutModel... into ConfigboxModel.. and update any KModel::getModel calls');
				}
			}

			// Finally store the object
			self::$instances[$className] = new $className($component);

		}
		
		return self::$instances[$className];

	}

	/**
	 * @param string $component
	 *
	 * @throws Exception if $component parameter is empty
	 */
	function __construct($component = '') {

		if (empty($component)) {
			$logMessage = 'Model constructor called without an empty $component parameter.';
			$identifier = KLog::log($logMessage, 'error');
			$publicMessage = 'A system error occured. (Identifier: '.$identifier.').';
			throw new Exception($publicMessage);
		}

		// Set the component name
		$this->componentName = $component;

	}

	/**
	 * Returns a normalized object with the relevant record data coming in via a HTTP request. Each model's property
	 * reads from the HTTP request and adds the sanitized value/values to the right object properties.
	 *
	 * @return object Record data collected by KenedoProperties
	 */
	function getDataFromRequest() {

		$data = new stdClass();

		$properties = $this->getProperties();

		foreach ($properties as $property) {
			$property->getDataFromRequest($data);
		}

		return $data;
	}

	/**
	 * Used for appending or auto-filling data before validation and storing
	 * @param object $data
	 * @return boolean if anything reported an issue
	 */
	function prepareForStorage($data) {

		$properties = $this->getProperties();
		foreach ($properties as $property) {
			$response = $property->prepare($data);
			if ($response !== true) {
				$this->setErrors($property->getErrors());
				return false;
			}
		}

		return true;
	}

	/**
	 * Get's you localized error messages via KenedoModel::getErrors()
	 *
	 * @param object $data The normalized data object
	 * @param string $context A string you can use in overrides that you can use to validate differently. OFC only useful if your code calls this method directly.
	 * @return boolean
	 */
	function validateData($data, $context = '') {

		$properties = $this->getProperties();
		$finalResponse = true;
		foreach ($properties as $property) {
			$response = $property->check($data);
			if ($response !== true) {
				$this->setErrors($property->getErrors());
				$finalResponse = false;
			}
		}

		return $finalResponse;

	}

	function isInsert($data) {
		return ( empty($data->{$this->getTableKey()}) );
	}

	function store($data) {

		$db = KenedoPlatform::getDb();

		// Figure out if we're dealing with an insert or an update
		$isInsert = $this->isInsert($data);

		// Do a pre-store to get a primary key id on new records
		// We do this because properties may require the id of the main record to have a foreign key for their own records
		if ($isInsert) {

			$query = "INSERT INTO `".$this->getTableName()."` SET `".$this->getTableKey()."` = NULL";
			$db->setQuery($query);
			$success = $db->query();

			if ($success) {
				$data->{$this->getTableKey()} = $db->insertid();
			}
			else {
				$this->setError('Entity pre-store failed in class "' . get_class( $this ).'" because of a database error: '.$db->getErrorMsg());
				return false;
			}
		}

		// Loop through the entity fields and call their store method
		$properties = $this->getProperties();
		foreach ($properties as $property) {
			$response = $property->store($data);
			if ($response === false) {
				$this->setErrors($property->getErrors());
				return false;
			}
		}

		// Finally write the record
		$success = $db->updateObject( $this->getTableName(), $data, $this->getTableKey(), false );

		$id = $data->{$this->getTableKey()};

		$this->forgetRecord($id);

		if ($success == false) {

			$this->setError('Storing record in class "'. get_class( $this ).'" failed because of a database error. DB error message was "'.$db->getErrorMsg().'".');

			// Revert insertion on base record and any properties
			if ($isInsert) {
				$this->delete($id);
			}

			return false;
		}

		$success = $this->afterStore($id, $isInsert);

		if ($success === false) {

			if ($isInsert) {
				$this->delete($id);
			}

			return false;
		}

		return true;

	}

	/**
	 * "Soft abstract" function that you can override to do further processing after regular storage completed.
	 * Has to return true or false. False will make the store method revert the insert (but not an update).
	 * You can rely on that the "base" method does no processing, so no need for any parent::afterStore() stuff.
	 *
	 * Use ->setError() to send user feedback if you need it
	 *
	 * @param int $id ID of the record that was updated
	 * @param boolean $wasInsert Whether the storage was an update or insert
	 * @return bool true if all good, false on errors
	 */
	function afterStore($id, $wasInsert) {
		return true;
	}

	/**
	 * Remove memo-cache entries of records for the given id
	 * @param $id
	 */
	public function forgetRecord($id) {
		if (!empty($this->memoGetRecord[$id])) {
			unset($this->memoGetRecord[$id]);
		}
	}

	/**
	 * Remove any memo-cached entries of records
	 */
	public function forgetRecords() {
		$this->memoGetRecord = array();
	}

	/**
	 * In case the model implements CRUD method itself, just return an array.
	 * @return array[]|array Array of the model's property definitions.
	 */
	function getPropertyDefinitions() {
		return array();
	}

	/**
	 * Gets custom property definitions from the customization folder to be merged with the regular ones.
	 * @return array[]|array Array of the model's property definitions.
	 */
	function getCustomPropertyDefinitions() {

		$customPropDefs = array();

		$dir = CONFIGBOX_DIR_MODEL_PROPERTY_CUSTOMIZATION;
		$fileBase = strtolower( substr(get_class($this), strpos(get_class($this), 'Model') + 5 ) );
		$file = $fileBase . '.php';
		$path = $dir.DS.$file;
		if (is_file($path) == false) {
			return $customPropDefs;
		}

		include_once($path);

		$function = 'customPropertyDefinitions'.ucfirst($fileBase);

		if (function_exists($function) == false) {
			return $customPropDefs;
		}

		return $function();

	}

	/**
	 * @return KenedoProperty[]
	 */
	function getProperties() {
		if (empty($this->memoGetProperties)) {
			// Get the regular defs
			$propDefs = $this->getPropertyDefinitions();
			// Get the custom defs
			$customPropDefs = $this->getCustomPropertyDefinitions();
			// Merge them together
			$propDefs = array_merge($propDefs, $customPropDefs);
			// Sort them by positionForm
			uasort($propDefs, array('KenedoModel', 'sortProperties'));
			// Loop through and make property objects
			$this->memoGetProperties = array();
			foreach ($propDefs as $propDef) {
				$this->memoGetProperties[$propDef['name']] = $this->getPropertyObject($propDef['type'], $propDef);
			}
		}

		return $this->memoGetProperties;
	}

	/**
	 * Sorting callable, used in KenedoModel::getProperties()
	 * @see KenedoModel::getProperties
	 * @param $a
	 * @param $b
	 * @return int
	 */
	function sortProperties($a, $b) {
		if ($a['positionForm'] < $b['positionForm']) {
			return -1;
		}
		elseif ($a['positionForm'] == $b['positionForm']) {
			return 0;
		}
		else {
			return 1;
		}
	}

	/**
	 * @param string $propertyType
	 * @param array $propertyDefinition
	 * @return KenedoProperty (Sub-class of it)
	 * @throws Exception if property type is unknown
	 */
	protected function getPropertyObject($propertyType, $propertyDefinition) {

		if (empty(self::$memoPropertyClassNames[$propertyType])) {

			$regularFolder = CONFIGBOX_DIR_PROPERTIES_DEFAULT;
			$customFolder = CONFIGBOX_DIR_PROPERTIES_CUSTOM;

			$fileName = strtolower($propertyType).'.php';

			// Check in customization folder
			if (file_exists($customFolder.DS.$fileName)) {
				$classFile = $customFolder.DS.$fileName;
			}
			// Check in system folder
			elseif (file_exists($regularFolder.DS.$fileName)) {
				$classFile = $regularFolder.DS.$fileName;
			}
			// If needed, deal with templates not found (currently fields like checked_out_time have no display)
			else {
				KLog::log('Property type "'.$propertyType.'" not found. Model was "'.$this->getModelName().'", prop defs were "'.var_export($propertyDefinition, true).'"', 'error');
				throw new Exception('Property type "'.$propertyType.'" not found.');
			}

			$className = 'KenedoProperty'.ucfirst(strtolower($propertyType));

			require_once($classFile);

			self::$memoPropertyClassNames[$propertyType] = $className;

		}

		$className = self::$memoPropertyClassNames[$propertyType];

		return new $className($propertyDefinition, $this);

	}

	/**
	 * @return KenedoProperty[]
	 */
	function getPropertiesForListing() {

		$properties = $this->getProperties();
		$returnProperties = array();
		foreach ($properties as $property) {
			if ($property->isInListing()) {
				$returnProperties[$property->getListingPosition()] = $property;
			}
		}
		ksort($returnProperties);
		return $returnProperties;

	}

	/**
	 * Holds the definition of tasks available for listings of that type. Data is used by KenedoViewHelper::renderTaskItems in Admin templates
	 *
	 * @see KenedoViewHelper::renderTaskItems
	 * @return array
	 */
	function getListingTasks() {
		$tasks = array(
				array('title'=>KText::_('Add'), 	'task'=>'add'),
				array('title'=>KText::_('Remove'), 	'task'=>'remove'),
		);
		return $tasks;
	}

	/**
	 * Holds the definition of tasks available for edit forms of that type. Data is used by KenedoViewHelper::renderTaskItems in Admin templates
	 *
	 * @see KenedoViewHelper::renderTaskItems
	 * @return array
	 */
	function getDetailsTasks() {
		$tasks = array(
				array('title'=>KText::_('Save and Close'), 	'task'=>'store'),
				array('title'=>KText::_('Save'), 			'task'=>'apply'),
				array('title'=>KText::_('Cancel'), 			'task'=>'cancel'),
		);
		return $tasks;
	}

	/**
	 * Returns the model's name (e.g. 'ConfigboxModelName' becomes 'name')
	 * @param KenedoModel|NULL $model Model object or use NULL to use $this
	 * @return string
	 */
	function getModelName($model = NULL) {
		if ($model === NULL) {
			$model = $this;
		}
		$className = get_class($model);
		$modelName = substr($className, strpos($className, 'Model') + 5 );
		return strtolower($modelName);
	}


	/**
	 * @return object A record with all properties set to default values or empty
	 */
	function initData() {

		$data = $this->getDataFromRequest();

		$props = $this->getProperties();
		foreach ($data as $key=>$value) {
			if (empty($value) && !empty($props[$key])) {
				if ($props[$key]->getPropertyDefinition('default')) {
					$data->$key = $props[$key]->getPropertyDefinition('default');
				}
			}
		}
		return $data;
	}

	/**
	 * @param int $id Record ID
	 * @return object
	 */
	function getRecord($id) {

		if (empty($this->memoGetRecord[$id])) {
			$props = $this->getProperties();
			$selects = array();
			$joins = array();
			foreach ($props as $prop) {
				$selects = array_merge($selects, $prop->getSelectsForGetRecord());
				$joins = array_merge($joins, $prop->getJoinsForGetRecord());
			}

			$db = KenedoPlatform::getDb();

			$query = "SELECT
				".implode(", \n", $selects)."

				FROM ".$db->getQuoted($this->getTableName())." AS ".$this->getModelName()."

				".implode("\n\n", $joins);

			$query .= "\n\nWHERE ".$db->getQuoted($this->getModelName()).".".$db->getQuoted($this->getTableKey())." = ".intval($id);

			$db->setQuery($query);
			$record = $db->loadObject();

			if ($record) {
				foreach ($props as $prop) {
					$prop->appendDataForGetRecord($record);
				}
			}

			$this->memoGetRecord[$id] = $record;
		}

		return $this->memoGetRecord[$id];

	}

	/**
	 * Returns records of the model's type.
	 * @see KenedoView::getFiltersFromUpdatedState(), KenedoView::getPaginationFromUpdatedState(), KenedoView::getOrderingFromUpdatedState()
	 *
	 * @param string[] $filters See KenedoView::getFiltersFromUpdatedState()
	 * @param string[] $pagination See KenedoView::getPaginationFromUpdatedState()
	 * @param string[] $ordering See See KenedoView::getOrderingFromUpdatedState()
	 * @return object[]
	 * @throws Exception
	 */
	function getRecords($filters = array(), $pagination = array(), $ordering = array()) {

		$props = $this->getProperties();
		$selects = array();
		$joins = array();
		$wheres = array();

		foreach ($props as $prop) {
			$selects = array_merge($selects, $prop->getSelectsForGetRecord());
			$joins = array_merge($joins, $prop->getJoinsForGetRecord());
		}

		$db = KenedoPlatform::getDb();

		if (count($filters)) {

			foreach ($filters as $filterName=>$value) {
				if (is_numeric($value)) {
					$wheres[] = $db->getEscaped($filterName)." = '".$db->getEscaped($value)."'";
				}
				else {
					$wheres[] = $db->getEscaped($filterName)." LIKE '%".$db->getEscaped($value)."%'";
				}
			}

		}

		$query = "SELECT \n".implode(", \n", $selects)."\n\n";

		$query .= "FROM ".$db->getQuoted($this->getTableName())." AS ".$this->getModelName();

		if (count($joins)) {
			$query .= "\n\n".implode("\n\n", $joins);
		}

		if (count($wheres)) {
			$query .= "\n\nWHERE ".implode(' AND ', $wheres);
		}

		if (!empty($ordering)) {
			if (!empty($props[$ordering['propertyName']])) {

				// Get the property to order by
				$prop = $props[$ordering['propertyName']];

				// Join props are great fun. We don't show the foreign table row's key value, but the prop defined
				// in propNameDisplay. So we go all the way to that property and get table alias and column name
				if ($prop->getType() == 'join') {
					/** @var KenedoModel $parentModel */
					$parentModelName = $prop->getPropertyDefinition('modelClass');
					$parentModel = KenedoModel::getModel($parentModelName);
					$parentProps = $parentModel->getProperties();
					$displayProp = $parentProps[$prop->getPropertyDefinition('propNameDisplay')];
					$filterName = $displayProp->getTableAlias().'.'.$displayProp->getTableColumnName();
				}
				else {
					$filterName = $prop->getTableAlias().'.'.$prop->getTableColumnName();
				}

				$query .= "\n\nORDER BY ".$filterName." ".strtoupper($db->getEscaped($ordering['direction']));
			}
		}

		if (!empty($pagination) && $pagination['limit'] != 0) {
			$query .= "\n\n LIMIT ".intval($pagination['start']).", ".intval($pagination['limit']);
		}

		$db->setQuery($query);
		$records = $db->loadObjectList();

		if (!$records) {
			$records = array();
		}

		foreach ($records as $record) {
			foreach ($props as $prop) {
				$prop->appendDataForGetRecord($record);
			}
		}

		return $records;

	}

	/**
	 * Returns an array with applicable filter names
	 * @return string[]
	 */
	function getFilterNames() {
		$filterNames = array();
		$props = $this->getProperties();
		foreach ($props as $prop) {
			$propFilterName = $prop->getFilterName();
			if (is_array($propFilterName)) {
				$filterNames = array_merge($filterNames, $propFilterName);
			}
			elseif(!empty($propFilterName)) {
				$filterNames[] = $propFilterName;
			}
		}
		return $filterNames;
	}

	/**
	 * Makes the property 'published' (called Active in the GUI nowadays) 1 or 0
	 * Looks for a GET/POST parameter 'ids', it is supposed to be a comma separated list of record ids
	 *
	 * @param int[] $ids Ids of records to active/deactivate
	 * @param boolean $publish True for publish, False for unpublish
	 * @return boolean
	 */
	function publish($ids, $publish = true) {

		// Check if $ids is an array
		if (is_array($ids) == false) {
			KLog::log('Non array passed to method. No changes made. Got this: '.var_export($ids,true), 'error');
			$this->setError('Activation/Deactivation of records failed. See error log file for details.');
			return false;
		}

		// Done
		if (count($ids) == 0) {
			return true;
		}

		// Sanitize ids
		foreach ($ids as &$id) {
			$id = intval($id);
		}

		$db = KenedoPlatform::getDb();
		$query = "UPDATE `".$this->getTableName()."` SET `published` = '".intval($publish)."' WHERE `".$this->getTableKey()."` IN (".implode(',', $ids).")";
		$db->setQuery($query);
		$success = $db->query();

		$this->forgetRecords();

		return ($success === true) ? true : false;

	}

	/**
	 * @param int|int[] $ids ID or array of IDs
	 * @return bool
	 */
	function delete($ids) {

		if (is_numeric($ids)) {
			$ids = array($ids);
		}

		// First check if all records can be removed
		// Otherwise, error messages will be set and false returned
		$allGood = true;
		foreach ($ids as $id) {
			if ($this->canDelete($id) == false) {
				$allGood = false;
			}
		}
		// That is because KenedoModel::canDelete() sets error messages as feedback. We want to see all messages, not
		// just the first.
		if ($allGood == false) {
			return false;
		}

		$db = KenedoPlatform::getDb();

		foreach ($ids as $id) {

			$query = 'DELETE FROM `'.$this->getTableName().'` WHERE `'.$this->getTableKey().'` = '.intval($id);
			$db->setQuery($query);
			$success = $db->query();

			if ($success == false) {
				$this->setError($db->getErrorMsg());
				return false;
			}
			else {

				// Go trough properties and run their delete method
				$properties = $this->getProperties();
				foreach ($properties as $property) {
					$property->delete($id, $this->getTableName());
				}

				$this->afterDelete($id);
			}

		}

		return true;

	}

	/**
	 * Soft-abstract method to use for some post-delete processing
	 * @param int $id ID of the record that was deleted
	 */
	function afterDelete($id) {

	}

	function canDelete($id) {
		return true;
	}

	/**
	 * Does essentially the same as KenedoModel::store, but you can change individual fields of the record with no need
	 * of having all fields present in GET/POST.
	 *
	 * @param int $id ID of record to update
	 * @return bool true if all good, false on errors
	 */
	function ajaxStore($id) {
	
		// Load the item that will be used for storing (in case you wonder, the constructor calls _setId() to figure out what record to load
		$record = $this->getRecord($id);

		// Prepare an object holding all record data (To be filled with KenedoEntity's method for getting GET/POST data)
		$newData = new stdClass();

		// Cry out in case there's no item
		if (!$record) {
			$this->setError(KText::_('Item with ID "'.$id.'" not found.'));
			return false;
		}

		// Let each fields populate the record object with GET/POST data
		$properties = $this->getProperties();
		foreach ($properties as $property) {
			$property->getDataFromRequest($newData);
		}

		// Overwrite all non-empty fields from before in the item
		foreach ($newData as $key=>$val) {
			if ($val !== NULL) {
				$record->$key = $val;
			}
		}

		$this->prepareForStorage($record);

		// Check if the data validates
		$checkResult = $this->validateData($record);
		if ($checkResult == false) {
			return false;
		}

		$isInsert = $this->isInsert($record);

		// Get the data stored
		$success = $this->store($record);

		// Abort and send feedback if storage fails
		if ($success === false) {
			return false;
		}

		$id = $record->{$this->getTableKey()};

		$success = $this->afterStore($id, $isInsert);

		if ($success === false) {

			if ($isInsert) {
				$this->delete($id);
			}

			return false;

		}
		else {
			return true;
		}

	}

	/**
	 * @param int[] $ordering Key/value pairs (key is record id, value is position)
	 * @return bool
	 */
	function storeOrdering($ordering) {
		
		if (count($ordering) == 0) {
			return true;
		}
		
		$db = KenedoPlatform::getDb();

		foreach ($ordering as $id=>$position) {
			$query = "UPDATE `".$this->getTableName()."` SET `ordering` = ".intval($position)." WHERE `".$this->getTableKey()."` = ".intval($id);
			$db->setQuery($query);
			$db->query();
		}
	
		return true;
	}

	/**
	 * Returns an array with all info regarding where the record is being used (determined by KenedoEntity join fields)
	 * Used for displaying item usage in most edit forms.
	 *
	 * @param int $recordId ID of record to check
	 * @see KenedoEntity::getItemUsage
	 * @return array
	 */
	function getRecordUsage($recordId) {

		$return = array();

		$usageInfo = $this->getRecordUsageInfo();

		if ($recordId == 0 || !isset($this->children) || !count($usageInfo)) {
			return $return;
		}

		$db = KenedoPlatform::getDb();
		foreach ($usageInfo as $component=>$items) {

			foreach ($items as $modelName=>$propertyInfos) {
				foreach ($propertyInfos as $propertyInfo) {
					$model = KenedoModel::getModel($modelName);
					$properties = $model->getProperties();

					$titleField = $properties->{$propertyInfo['titleField']};
					if ($titleField['type'] == 'translatable') {
						$select = 'a.id, str_title.text AS title';
						$join = "LEFT JOIN `".$titleField['stringTable']."` AS str_title ON str_title.key = a.id AND str_title.type = ".(int)$titleField['langType']." AND str_title.language_tag = '".$db->getEscaped(KText::$languageTag)."'";
					}
					else {
						$select = 'a.id, a.'.$propertyInfo['titleField'].' AS title';
						$join = '';
					}

					$query = "
					SELECT ".$select."
					FROM `".$model->getTableName()."` AS a
					".$join."
					WHERE `".$propertyInfo['fkField']."` = ".(int)$recordId;

					if (!empty($propertyInfo['filterField'])) {
						$query .= " AND `".$propertyInfo['filterField']."` = '".$db->getEscaped($propertyInfo['filterValue'])."'";
					}

					$db->setQuery($query);
					$usages = $db->loadObjectList();

					if ($usages) {
						foreach ($usages as $usage) {
							$usage->link = KLink::getRoute('index.php?option='.$component.'&controller='.$propertyInfo['controller'].'&id='.$usage->id.'&task=edit');
						}

						$return[$propertyInfo['name']] = $usages;
					}
				}
			}

		}

		return $return;

	}

	function resetErrors() {
		$this->errors = array();
	}

	function setError($error) {
		$this->errors[] = $error;
	}
	
	function setErrors($errors) {
		if (is_array($errors) && count($errors)) {
			$this->errors = array_merge((array)$this->errors,$errors);
		}
	}
	
	function getErrors() {
		return $this->errors;
	}
	
	function getError() {
		if (is_array($this->errors) && count($this->errors)) {
			return end($this->errors);
		}
		else {
			return '';
		}
	}
	
}
