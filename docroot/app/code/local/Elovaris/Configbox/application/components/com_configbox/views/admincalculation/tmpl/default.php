<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewAdmincalculation */
?>
<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>">

<form class="kenedo-details-form" method="post" enctype="multipart/form-data" action="<?php echo KLink::getRoute('index.php');?>" data-view="<?php echo hsc($this->view);?>">

	<?php echo (count($this->pageTasks)) ? KenedoViewHelper::renderTaskItems($this->pageTasks) : ''; ?>

	<?php if (!empty($this->pageTitle)) { ?><h1 class="kenedo-page-title"><?php echo hsc($this->pageTitle);?></h1><?php } ?>
	
	<div class="kenedo-messages">
		<div class="kenedo-messages-error"></div>
		<div class="kenedo-messages-notice"></div>
	</div>
	
	<div class="kenedo-properties name-type">
		
		<div id="property-name-name" class="property-name-name kenedo-property property-type-string">
			<div class="property-label"><?php echo KText::_('Name');?></div>
			<div class="property-body"><div class="string-type-number">
				<input type="text" name="name" id="name" value="<?php echo !empty($this->record->name) ? hsc($this->record->name) : '';?>">
			</div></div>
		</div>
	
		<div id="form-property-type" class="property-name-type kenedo-property property-type-radio">
			<div class="property-label"><?php echo KText::_('Type');?></div>
			<div class="property-body">
				
				<input type="radio" name="type" id="typematrix" value="matrix" <?php if (empty($this->record->type) || $this->record->type == 'matrix') { ?> checked="checked" <?php } ?>>
				<label for="typematrix"><?php echo KText::_('Matrix');?></label>

				<input type="radio" name="type" id="typeformula" value="formula" <?php if (!empty($this->record->type) && $this->record->type == 'formula') { ?> checked="checked" <?php } ?>>
				<label for="typeformula"><?php echo KText::_('Formula');?></label>

				<input type="radio" name="type" id="typecode" value="code" <?php if (!empty($this->record->type) && $this->record->type == 'code') { ?> checked="checked" <?php } ?>>
				<label for="typecode"><?php echo KText::_('Code');?></label>

			</div>
		</div>
		
	</div>

	<div class="calc-type-subview"
		 data-url-matrix="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=admincalcmatrices&task=edit&ajax_sub_view=1&tmpl=component&format=raw&id='.intval($this->record->id));?>"
		 data-url-formula="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=admincalcformulas&task=edit&ajax_sub_view=1&tmpl=component&format=raw&id='.intval($this->record->id));?>"
		 data-url-code="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=admincalccodes&task=edit&ajax_sub_view=1&tmpl=component&format=raw&id='.intval($this->record->id));?>"
		>
		<?php
		switch ($this->record->type) {
			case 'matrix':
				KenedoView::getView('ConfigboxViewAdmincalcmatrix')->display();
				break;

			case 'formula':
				KenedoView::getView('ConfigboxViewAdmincalcformula')->display();
				break;

			case 'code':
				KenedoView::getView('ConfigboxViewAdmincalccode')->display();
				break;
		}
		?>
	</div>

	<?php if (!empty($this->itemUsage)) { ?>
		<div class="kenedo-item-usage">
			<div class="kenedo-usage-message"><?php echo KText::_('The item is in use in the following entries:');?></div>
			<?php foreach ($this->itemUsage as $fieldName=>$items) { ?>
				<?php foreach ($items as $item) { ?>
				<div>
					<span class="kenedo-candelete-usage-entry-name"><?php echo hsc($fieldName);?></span> - <a class="kenedo-new-tab kenedo-candelete-usage-entry-link" href="<?php echo $item->link;?>" class="new-tab"><?php echo hsc($item->title);?></a>
				</div>
				<?php } ?>
			<?php } ?>
		</div>
	<?php } ?>
	
	<div class="kenedo-hidden-fields">
		<input type="hidden" id="option" 		name="option" 				value="<?php echo hsc($this->component);?>" />
		<input type="hidden" id="controller"	name="controller" 			value="<?php echo hsc($this->controllerName);?>" />
		<input type="hidden" id="task" 			name="task" 				value="" />
		<input type="hidden" id="ajax_sub_view" name="ajax_sub_view" 		value="<?php echo ($this->isAjaxSubview()) ? '1':'0';?>" />		
		<input type="hidden" id="in_modal"		name="in_modal" 			value="<?php echo ($this->isInModal()) ? '1':'0';?>" />
		<input type="hidden" id="tmpl"			name="tmpl" 				value="component" />
		<input type="hidden" id="format"		name="format" 				value="raw" />
		<input type="hidden" id="id"			name="id" 					value="<?php echo !empty($this->record->id) ? intval($this->record->id) : 0; ?>" />
		<input type="hidden" id="lang"			name="lang" 				value="<?php echo hsc(KenedoPlatform::p()->getLanguageUrlCode());?>" />
		<input type="hidden" id="return" 		name="return" 				value="<?php echo KLink::base64UrlEncode($this->returnUrl);?>" />
		<input type="hidden" id="form_custom_1" name="form_custom_1" 		value="<?php echo hsc(KRequest::getString('form_custom_1'));?>" />
		<input type="hidden" id="form_custom_2" name="form_custom_2" 		value="<?php echo hsc(KRequest::getString('form_custom_2'));?>" />
		<input type="hidden" id="form_custom_3" name="form_custom_3" 		value="<?php echo hsc(KRequest::getString('form_custom_3'));?>" />
		<input type="hidden" id="form_custom_4" name="form_custom_4" 		value="<?php echo hsc(KRequest::getString('form_custom_4'));?>" />
		<?php if (KenedoPlatform::getName() == 'magento') { ?>		
		<input type="hidden" id="form_key" 		name="form_key" 			value="<?php echo Mage::getSingleton('core/session')->getFormKey();?>" />
		<?php } ?>
	</div>
	
</form>
</div>

