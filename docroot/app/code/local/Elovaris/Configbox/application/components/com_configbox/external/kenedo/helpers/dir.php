<?php
class KenedoDirHelper {

	static function getRootUrlPath() {

		$dir = dirname($_SERVER['SCRIPT_NAME']);
		$ex = explode('/',$dir);
		$last = array_pop($ex);

		if ($last == 'administrator') {
			$dir = implode('/',$ex).'/';
		}
		elseif ($last == 'com_configbox' or $last == 'com_cbcheckout') {
			array_pop($ex);
			$dir = implode('/',$ex).'/';
		}
		else {
			$dir = dirname($_SERVER['SCRIPT_NAME']).'/';
		}
		if (empty($dir)) {
			$dir = '/';
		}

		$dir = str_replace('//', '/', $dir);
		return $dir;
	}


}