<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyRadio extends KenedoProperty {
	
	function renderListingField($item, $items) {
		
		$value = $item->{$this->propertyName};
		$outputValue = $this->propertyDefinition['radios'][$value];

		if ($this->getPropertyDefinition('listinglink')) {

			$option = $this->getPropertyDefinition('component');
			$controller = $this->getPropertyDefinition('controller');

			$returnUrl = KLink::base64UrlEncode(KLink::getRoute( 'index.php?option='.hsc($option).'&controller='.hsc($controller), false ));
			$href = KLink::getRoute('index.php?option='.hsc($option).'&controller='.hsc($controller).'&task=edit&id='.intval($item->id).'&return='.$returnUrl);

			?>
			<a class="listing-link" href="<?php echo $href;?>"><?php echo hsc($outputValue);?></a>
		<?php
		}
		else {
			echo hsc($outputValue);
		}
		
	}
		
}