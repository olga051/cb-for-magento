<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxConditionElementAttribute extends ConfigboxCondition {

	function containsElementId($conditionData, $elementId) {
		return ($elementId == $conditionData['elementId']);
	}

	function containsXrefId($conditionData, $xrefId) {

		if ($conditionData['field'] == 'selectedOption.id') {

			$db = KenedoPlatform::getDb();
			$query = "
			SELECT `id`
			FROM `#__configbox_xref_element_option`
			WHERE `id` = ".intval($conditionData['value'])." AND `element_id` = ".intval($conditionData['elementId']);
			$db->setQuery($query);
			$isActualXref = intval($db->loadResult());

			if ($isActualXref) {
				return ($xrefId == $conditionData['value']);
			}

		}

		return false;

	}

	/**
	 * Called by ConfigboxRulesHelper::getConditionCode to compare condition with provided selections
	 *
	 * @param string[] $conditionData
	 * @param array[] $selections
	 * @return bool true if selections meet the condition
	 *
	 * @see ConfigboxRulesHelper::getConditionsCode, ConfigboxRulesHelper::getConditions, ConfigboxRulesHelper::getSelections
	 */
	function getEvaluationResult($conditionData, $selections) {

		$elementId = $conditionData['elementId'];
		$operator = $conditionData['operator'];
		$shouldValue = $conditionData['value'];

		switch ($conditionData['field']) {

			case 'price':
				$isValue = ConfigboxPrices::getElementPrice($elementId);
				break;

			case 'priceRecurring':
				$isValue = ConfigboxPrices::getElementPriceRecurring($elementId);
				break;

			case 'selectedOption.id':
				if (isset($selections[$elementId])) {

					// Special processing if element is text field - we crank this into selectedOption.id for now
					if (empty($selections[$elementId]['element_option_xref_id']) && !empty($selections[$elementId]['text'])) {

						$isValue = $selections[$elementId]['text'];

						if ((!empty($isValue) or $isValue === '0') && $operator == '!=') {
							return true;
						}
						if ((!empty($isValue) or $isValue === '0') && $operator == '==') {
							return false;
						}

					}
					else {
						$isValue = $selections[$elementId]['element_option_xref_id'];
						if ($isValue == 0) {
							$isValue = '';
						}
					}
				}
				else {
					$isValue = '';
				}
				break;

			case 'selected':
				$isValue = (isset($selections[$elementId])) ? $selections[$elementId]['text'] : '';
				break;

			default:
				$element = ConfigboxElementHelper::getElement($elementId);
				$isValue = $element->getField($conditionData['field']);
				break;

		}

		if (is_numeric($isValue)) {
			$return = version_compare( (float)$isValue,$shouldValue,$operator);

			return $return;
		}
		else {
			if ($operator == "==") {
				$return = (strcmp($isValue,$shouldValue) == 0);

				return $return;
			}
			if ($operator == "!=") {
				$return = (strcmp($isValue,$shouldValue) != 0);

				return $return;
			}
			else {
				$isValue = floatval($isValue);
				$return = version_compare( $isValue,$shouldValue,$operator);
				return $return;
			}
		}

	}

	/**
	 * Called by ConfigboxRulesHelper::getConditionHtml to display the condition (either for editing or display)
	 *
	 * @param string[] $conditionData
	 * @param bool $forEditing If edit controls or plain display should come out
	 * @return string HTML for that condition
	 * @see ConfigboxRulesHelper::getConditionsHtml
	 */
	function getConditionHtml($conditionData, $forEditing = true) {

		$isXrefCondition = ($conditionData['field'] == 'selectedOption.id');

		$optionTitle = '';
		if ($isXrefCondition) {
			if ($conditionData['value'] == 0) {
				$optionTitle = KText::_('without selection');
			}
			else {
				$optionTitle = ConfigboxRulesHelper::getXrefTitle($conditionData['value']);
			}
		}

		$elementTitle = ConfigboxRulesHelper::getElementTitle($conditionData['elementId']);
		$attributes = $this->getElementAttributes();

		$conditionName = KText::sprintf($attributes[$conditionData['field']]['text'], $elementTitle);

		$operatorHtml = $this->getOperatorText($conditionData['operator']);

		$conditionValue = $conditionData['value'];
		if (is_numeric($conditionValue)) {
			$conditionValue = str_replace('.', KText::_('DECIMAL_MARK','.'), $conditionValue);
		}

		ob_start();

		?>
		<span
			class="item condition elementattribute"
			data-type="<?php echo $conditionData['type'];?>"
			data-element-id="<?php echo $conditionData['elementId'];?>"
			data-field="<?php echo $conditionData['field'];?>"
			data-operator="<?php echo $conditionData['operator'];?>"
			data-value="<?php echo $conditionData['value'];?>"
			>

			<span class="condition-name"><?php echo hsc($conditionName);?></span>

			<span class="condition-operator"><?php echo $operatorHtml;?></span>

			<?php if ($isXrefCondition) { ?>
				<span class="condition-value"><?php echo hsc($optionTitle);?></span>
			<?php } else { ?>

				<?php if ($forEditing) { ?>
					<input class="input" data-data-key="value" type="text" value="<?php echo hsc($conditionValue);?>" />
				<?php } else { ?>
					<span class="condition-value"><?php echo hsc($conditionValue);?></span>
				<?php } ?>

			<?php } ?>

		</span>
		<?php

		return ob_get_clean();

	}

	function getTypeTitle() {
		return KText::_('Elements');
	}

	function getConditionsPanelHtml() {
		$view = KenedoView::getView('ConfigboxViewAdminruleeditor_elementattribute');
		$view->prepareTemplateVars();
		return $view->getViewOutput('default');
	}

	/**
	 * Returns the possible element subjects for elementattribute conditions
	 * @return array[]
	 */
	function getElementAttributes() {

		$assignmentCustom1 = (trim(CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_1)) ? CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_1 : KText::sprintf('Assignment Custom %s',1);
		$assignmentCustom2 = (trim(CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_2)) ? CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_2 : KText::sprintf('Assignment Custom %s',2);
		$assignmentCustom3 = (trim(CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_3)) ? CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_3 : KText::sprintf('Assignment Custom %s',3);
		$assignmentCustom4 = (trim(CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_4)) ? CONFIGBOX_LABEL_ASSIGNMENT_CUSTOM_4 : KText::sprintf('Assignment Custom %s',4);

		$optionCustom1 = (trim(CONFIGBOX_LABEL_OPTION_CUSTOM_1)) ? CONFIGBOX_LABEL_OPTION_CUSTOM_1 : KText::sprintf('Option Custom %s',1);
		$optionCustom2 = (trim(CONFIGBOX_LABEL_OPTION_CUSTOM_2)) ? CONFIGBOX_LABEL_OPTION_CUSTOM_2 : KText::sprintf('Option Custom %s',2);
		$optionCustom3 = (trim(CONFIGBOX_LABEL_OPTION_CUSTOM_3)) ? CONFIGBOX_LABEL_OPTION_CUSTOM_3 : KText::sprintf('Option Custom %s',3);
		$optionCustom4 = (trim(CONFIGBOX_LABEL_OPTION_CUSTOM_4)) ? CONFIGBOX_LABEL_OPTION_CUSTOM_4 : KText::sprintf('Option Custom %s',4);

		$attributes = array(

			'selectedOption.id' => array ('text'=>KText::_('Selection in %s') ) ,
			'selected' => array ('text'=>KText::_('Entry in %s') ) ,
			'price' => array ('text'=>KText::_('Price of %s') ) ,
			'priceRecurring' => array ('text'=>KText::_('Recurring Price of %s') ) ,

			'selectedOption.assignment_custom_1' => array ('text'=>KText::_("$assignmentCustom1 in %s") ) ,
			'selectedOption.assignment_custom_2' => array ('text'=>KText::_("$assignmentCustom2 in %s") ) ,
			'selectedOption.assignment_custom_3' => array ('text'=>KText::_("$assignmentCustom3 in %s") ) ,
			'selectedOption.assignment_custom_4' => array ('text'=>KText::_("$assignmentCustom4 in %s") ) ,

			'selectedOption.option_custom_1' => array ('text'=>KText::_("$optionCustom1 in %s") ) ,
			'selectedOption.option_custom_2' => array ('text'=>KText::_("$optionCustom2 in %s") ) ,
			'selectedOption.option_custom_3' => array ('text'=>KText::_("$optionCustom3 in %s") ) ,
			'selectedOption.option_custom_4' => array ('text'=>KText::_("$optionCustom4 in %s") ) ,

		);

		return $attributes;

	}

}