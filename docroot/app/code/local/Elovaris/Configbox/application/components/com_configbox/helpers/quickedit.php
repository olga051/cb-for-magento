<?php
class ConfigboxQuickeditHelper {
	
	static function renderElementButtons($element) {
		ob_start();
		?>
		<div class="quick-edit-buttons quick-edit-buttons-element">
			<a onclick="return confirm(window.com_configbox.lang.areyousure)" title="<?php echo KText::_('Delete Element');?>" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminelements&task=delete&quickedit=1&ids='.$element->id);?>">
				<span class="fa fa-times"></span>
			</a>			
			<a class="kenedo-modal" data-modal-width="1040" data-modal-height="700" title="<?php echo KText::_('Edit Element');?>" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminelements&task=edit&id='.$element->id.'&in_modal=1');?>">
				<span class="fa fa-pencil"></span>
			</a>
		</div>
		<?php
		$output = ob_get_clean();
		return $output;
	}
	
	static function renderXrefButtons($option) {		
		ob_start();
		?>
		<div class="quick-edit-buttons quick-edit-buttons-option">
			<a class="quick-edit-button-edit" onclick="return confirm(window.com_configbox.lang.areyousure)" title="<?php echo KText::_('Delete Option');?>" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminoptionassignments&task=delete&quickedit=1&tmpl=component&ids='.$option->id);?>">
				<span class="fa fa-times"></span>
			</a>			
			<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" title="<?php echo KText::_('Edit Option');?>" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminoptionassignments&task=edit&in_modal=1&id='.$option->id);?>">
				<span class="fa fa-pencil"></span>
			</a>
		</div>
		<?php
		$output = ob_get_clean();
		return $output;
	}

	/**
	 * @param object $page Configurator page data (as from ConfigboxModelConfiguratorpage::getPage)
	 * @param object $product Product data (as from ConfigboxModelProduct::getProduct)
	 * @return string
	 * @see ConfigboxModelConfiguratorpage::getPage, ConfigboxModelProduct::getProduct
	 */
	static function renderConfigurationPageButtons($page, $product = NULL) {
		ob_start();
		?>
		<div class="quick-edit-buttons quick-edit-buttons-configuration-page">
			
			<div class="quick-edit-buttons-fader">
				<a class="toolbarimage show-edit-buttons" title="<?php echo KText::_('Edit Configurator Page');?>">
					<span class="fa fa-pencil"></span>
				</a>
			</div>
			
			<div class="quick-edit-buttons-content">
				
				<div class="toolbarbutton hide-edit-buttons">
					<a class="toolbarimage">
						<span class="fa fa-share"><?php echo KText::_('Close');?></span>
					</a>
				</div>
				<div class="toolbarbutton">
					<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminelements&in_modal=1&page_id='.$page->id.'&task=edit&id=0');?>">
						<span class="fa fa-plus-circle"><?php echo KText::_('Add Element');?></span>
					</a>
					
				</div>
				<div class="toolbarbutton">
					<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminpages&task=edit&id=0&in_modal=1&product_id='.(int)$page->product_id);?>">
						<span class="fa fa-plus-circle"><?php echo KText::_('Add Page');?></span>
					</a>
				</div>
				<div class="toolbarbutton">
					<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminpages&task=edit&in_modal=1&id='.$page->id);?>">
						<span class="fa fa-pencil"><?php echo KText::_('Edit Page');?></span>
					</a>
				</div>
				<div class="toolbarbutton">
					<a onclick="return confirm(window.com_configbox.lang.areyousure)" class="toolbarimage" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminpages&task=delete&quickedit=1&ids='.$page->id);?>">
						<span class="fa fa-times"><?php echo KText::_('Delete Page');?></span>
					</a>
				</div>
				<?php if ($product) { ?>
				<div class="toolbarbutton">
					<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminproducts&task=edit&in_modal=1&id='.$product->id);?>">
						<span class="fa fa-pencil"><?php echo KText::_('Edit Product');?></span>
					</a>
				</div>
				<?php } ?>
				
				<div class="toolbarbutton">
					<a class="toolbarimage" onclick="cbj('.hide-not-applying').removeClass('hide-not-applying');">
						<span class="fa fa-eye"><?php echo KText::_('Show hidden');?></span>
					</a>
				</div>
				
				
				<div class="clear"></div>
			</div>
			
		</div>
		<?php
		$output = ob_get_clean();
		return $output;
	}

	/**
	 * @param object $product Product data (as from ConfigboxModelProduct::getProduct)
	 * @return string
	 * @see ConfigboxModelProduct::getProduct
	 */
	static function renderProductPageButtons($product = NULL) {
		ob_start();
		?>
		<div class="quick-edit-buttons quick-edit-buttons-product-page">
			
			<div class="toolbarbutton">
				<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminpages&in_modal=1&prod_id='.$product->id.'&task=edit&id=0');?>">
					<span class="fa fa-plus-circle"><?php echo KText::_('Add Page');?></span>
			 	</a>
			</div>
			
			<div class="toolbarbutton">
				<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminproducts&in_modal=1&task=edit&id='.$product->id);?>">
					<span class="fa fa-pencil"><?php echo KText::_('Edit Product');?></span>
				</a>
			</div>
			
			<div class="clear"></div>
		</div>
		<?php
		$output = ob_get_clean();
		return $output;
	}

	/**
	 * @param object $listing Listing data (as from ConfigboxModelProductlisting::getProductListing)
	 * @return string
	 * @see ConfigboxModelProductlisting::getProductListing
	 */
	static function renderProductListingButtons($listing = NULL) {
		ob_start();
		?>

		<div class="quick-edit-buttons quick-edit-buttons-product-listing-page">
			<div class="toolbarbutton">
				<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminproducts&in_modal=1&task=edit&id=0');?>">
					<span class="fa fa-plus-circle"><?php echo KText::_('Add Product');?></span>
				</a>
			</div>
			<div class="toolbarbutton">
				<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminlistings&in_modal=1&task=edit&id=0');?>">
					<span class="fa fa-plus-circle"><?php echo KText::_('Add Listing');?></span>
				</a>
			</div>

			<div class="quick-edit-buttons quick-edit-buttons-product-listing">
				<div class="toolbarbutton">
					<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminlistings&in_modal=1&id='.$listing->id.'&task=edit');?>">
						<span class="fa fa-pencil"><?php echo KText::_('Edit Listing');?></span>
					</a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>

		<?php
		$output = ob_get_clean();
		return $output;
	}

	/**
	 * @param object $product Product data (as from ConfigboxModelProduct::getProduct)
	 * @return string
	 * @see ConfigboxModelProduct::getProduct
	 */
	static function renderProductButtons($product = NULL) {
		ob_start();
		?>
		<div class="quick-edit-buttons quick-edit-buttons-product">
			
			<a onclick="return confirm(window.com_configbox.lang.areyousure)" title="<?php echo KText::_('Delete Product');?>" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminproducts&task=delete&quickedit=1&ids='.$product->id);?>">
				<span class="fa fa-times"><?php echo KText::_('Delete Product');?></span>
			</a>
			
			<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminproducts&in_modal=1&task=edit&id='.$product->id);?>">
				<span class="fa fa-pencil"><?php echo KText::_('Edit Product');?></span>
			</a>
			<?php if (!$product->isConfigurable) { ?>
				<a class="kenedo-modal" data-modal-width="1000" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminpages&in_modal=1&task=edit&id=0&product_id='.$product->id);?>">
					<span class="fa fa-plus-circle"><?php echo KText::_('Add Page');?></span>
				</a>
			<?php } ?>
			
			<div class="clear"></div>
		</div>
		<?php
		$output = ob_get_clean();
		return $output;
	}
	
}