<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyBoolean extends KenedoProperty {
	
	function renderListingField($item, $items) {
	
		$value = $item->{$this->propertyName};
	
		echo ($value == 1) ? KText::_('CBYES') : KText::_('CBNO');
	
	}
	
}