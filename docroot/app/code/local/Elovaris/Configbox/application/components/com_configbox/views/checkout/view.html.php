<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewCheckout extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'checkout';

	/**
	 * @see ConfigboxUserHelper::orderAddressComplete()
	 * @var boolean $orderAddressComplete Indicates if customer data is complete enough for checkout
	 */
	public $orderAddressComplete;

	/**
	 * Holds all information about the order
	 * @var object $orderRecord
	 * @see ConfigboxModelOrderrecord::getOrderREcord
	 */
	public $orderRecord;

	/**
	 * @var string $orderAddressFormHtml HTML for the order address form
	 * @see ConfigboxViewCustomerform
	 */
	public $orderAddressFormHtml;

	/**
	 * @var string $orderAddressHtml HTML for the order address display
	 * @see ConfigboxViewCheckoutaddress
	 */
	public $orderAddressHtml;

	/**
	 * @var boolean $useDelivery Indicates if delivery methods are used (Can be disabled in settings -> checkout)
	 */
	public $useDelivery;

	/**
	 * @var string $deliveryHtml HTML for picking the delivery method
	 * @see ConfigboxViewCheckoutdelivery
	 */
	public $deliveryHtml;

	/**
	 * @var string $paymentHtml HTML for picking the payment method
	 * @see ConfigboxViewCheckoutpayment
	 */
	public $paymentHtml;

	/**
	 * @var string $orderRecordHtml HTML for showing the positions, pricing on the bottom of the checkout page
	 * @see ConfigboxViewRecord
	 */
	public $orderRecordHtml;

	/**
	 * @var boolean $confirmTerms Indicates if the customer needs to confirm the T&C (settings -> checkout)
	 */
	public $confirmTerms;

	/**
	 * @var boolean $confirmRefundPolicy Indicates if the customer needs to confirm the refund policy (settings -> checkout)
	 */
	public $confirmRefundPolicy;

	/**
	 * @var string $linkTerms Ready-made HTML link for the T&C with modal window functionality
	 */
	public $linkTerms;

	/**
	 * @var string $linkRefundPolicy Ready-made HTML link for the refund policy with modal window functionality
	 */
	public $linkRefundPolicy;

	/**
	 * @var boolean $canGoBackToCart Indicates if the customer can go back to cart (and discard the order)
	 */
	public $canGoBackToCart;

	/**
	 * @var string $backToCartUrl URL to the cart page
	 */
	public $backToCartUrl;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function display() {

		$this->prepareTemplateVars();

		if (empty($this->orderRecord)) {
			echo KText::_('Order not found.');
			return;
		}

		$this->renderView();

	}

	function prepareTemplateVars() {

		// Get the order id (if a customer comes into this view with a set ID, the controller's display method handles setting)
		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
		$orderId = $orderModel->getId();
		$orderRecord = $orderModel->getOrderRecord($orderId);

		// Bounce if we got no order record
		if (!$orderRecord) {
			return;
		}

		$view = KenedoView::getView('ConfigboxViewCustomerform');
		$view->customerFields = ConfigboxUserHelper::getUserFields($orderRecord->orderAddress->group_id);
		$view->customerData = $orderRecord->orderAddress;
		$view->formType = 'checkout';
		$view->prepareTemplateVars();
		$formHtml = $view->getViewOutput();

		$this->assign('orderAddressFormHtml', $formHtml);

		// Assign the order record data
		$this->assignRef('orderRecord', $orderRecord);

		$this->assign('confirmTerms', CONFIGBOX_EXPLICIT_AGREEMENT_TERMS);
		$this->assign('confirmRefundPolicy', CONFIGBOX_EXPLICIT_AGREEMENT_RP);

		// Links to some pages
		$termsLink = '<a class="rovedo-modal modal-link-terms" data-modal-width="900" data-modal-height="700" href="'.KLink::getRoute('index.php?option=com_configbox&view=terms&tmpl=component').'">'.KText::_('Terms and Conditions').'</a>';
		$refundLink = '<a class="rovedo-modal modal-link-refund-policy" data-modal-width="900" data-modal-height="700" href="'.KLink::getRoute('index.php?option=com_configbox&view=refundpolicy&tmpl=component').'">'.KText::_('Refund Policy').'</a>';
		$this->assign('linkTerms',$termsLink);
		$this->assign('linkRefundPolicy',$refundLink);
		
		// Back to cart URL
		$backToCartUrl = KLink::getRoute('index.php?option=com_configbox&controller=checkout&task=backToCart');
		$this->assign('backToCartUrl',$backToCartUrl);
		
		// Check if the customer can actually go back
		$cartModel = KenedoModel::getModel('ConfigboxModelCart');
		$cartModel->setId($orderRecord->cart_id);
		$cartDetails = $cartModel->getCartDetails();
		$canGoBack 	= ConfigboxOrderHelper::isPermittedAction('goBackToCart',$cartDetails);
		$this->assign('canGoBackToCart',$canGoBack);
		
		// Pass over whether to use delivery or not
		$this->assign('useDelivery', (CONFIGBOX_DISABLE_DELIVERY == 0));

		// Figure out if address is complete for checkout
		$orderAddressComplete = ConfigboxUserHelper::orderAddressComplete($orderRecord->orderAddress);
		$this->assignRef('orderAddressComplete',$orderAddressComplete);
		
		// Assign the order address subview HTML
		if ($orderAddressComplete) {
			ob_start();
			$view = KenedoView::getView('ConfigboxViewCheckoutaddress');
			$view->display();
			$html = ob_get_clean();
		}
		else {
			$html = '';
		}
		$this->assignRef('orderAddressHtml', $html);
		
		// Assign the delivery subview HTML
		ob_start();
		$view = KenedoView::getView('ConfigboxViewCheckoutdelivery');
		$view->display();
		$html = ob_get_clean();
		$this->assignRef('deliveryHtml', $html);
		
		// Assign the payment subview HTML
		ob_start();
		$view = KenedoView::getView('ConfigboxViewCheckoutpayment');
		$view->display();
		$html = ob_get_clean();
		$this->assignRef('paymentHtml', $html);
		
		// Assign the checkout record subview HTML
		ob_start();
		$view = KenedoView::getView('ConfigboxViewRecord');
		$view->assignRef('orderRecord', $orderRecord);
		$view->assign('showProductDetails', true);
		$view->assign('hideSkus', true);
		$view->display();
		$html = ob_get_clean();
		$this->assignRef('orderRecordHtml', $html);

	}
	
	function getFieldClasses($fields,$field) {
	
		$fieldClasses = array();
		$fieldClasses[] = 'order-address-field field-'.$field;
	
		if (!empty($fields[$field]->require_checkout)) {
			$fieldClasses[] = 'required';
		}
	
		return implode(' ', $fieldClasses);
	
	}
	
}