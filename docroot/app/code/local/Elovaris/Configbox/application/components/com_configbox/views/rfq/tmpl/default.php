<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewRfq */
?>
<div id="com_configbox">
<div id="view-rfq" class="<?php $this->renderViewCssClasses();?>" data-cart-id="<?php echo intval($this->cartId);?>">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

	<h1 class="page-title page-title-rfq"><?php echo KText::_('Get a quotation');?></h1>

	<div class="wrapper-customer-form">
		<h2 class="step-title"><?php echo KText::_('Your address');?></h2>
		<?php echo $this->customerFormHtml; ?>
	</div>

	<div class="field-comment">
		<label for="comment" class="field-label"><?php echo KText::_('Comment');?></label>
		<div class="form-field">
			<textarea id="comment" name="comment" cols="20" rows="5"></textarea>
		</div>
	</div>

	<div class="buttons">
		<a class="btn button-back" href="<?php echo $this->urlCart;?>"><?php echo KText::_('Back to cart');?></a>
		<a class="btn btn-primary button-get-quote trigger-request-quotation"><?php echo KText::_('Get Quote');?></a>
	</div>

</div>
</div>
</div>