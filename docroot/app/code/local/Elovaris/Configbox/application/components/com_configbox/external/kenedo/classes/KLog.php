<?php
class KLog {

	static $starttime;

	static $takes = array();
	static $timings = array();

	static $counts = array();

	static $ignoredErrorNumbers = array(2048);

	static $errorCodes = array(
		1	=> 'E_ERROR',
		2	=> 'E_WARNING',
		4	=> 'E_PARSE',
		8	=> 'E_NOTICE',
		2048=> 'E_STRICT',
		5096=> 'E_RECOVERABLE_ERROR',
		8192=> 'E_DEPRECATED',
	);

	static function setTiming($task, $milliSeconds) {
		self::$timings[$task] = $milliSeconds;
	}

	/**
	 *
	 * Start or reset the timer for time profiling
	 * @param string $task task keyword
	 */
	static function start($task = 'default') {
		self::$starttime[$task] = microtime(true)*1000;
	}

	/**
	 *
	 * Get the time since start was called in ms
	 *
	 * @param string $task task keyword
	 * @return int Time in ms
	 */
	static function time($task = 'default') {
		$time =  (int) ((microtime(true)*1000) - self::$starttime[$task]);
		self::$takes[$task][] = $time;
		return $time;
	}

	/**
	 *
	 * Get the time since start was called in ms
	 *
	 * @param string $task task keyword
	 * @return int Time in ms
	 */
	static function stop($task = 'default') {
		$time =  (int) ((microtime(true)*1000) - self::$starttime[$task]);
		self::$timings[$task] = $time;
		return $time;
	}

	static function getTakes() {
		return self::$takes;
	}

	static function getTimings() {
		return self::$timings;
	}

	static function count($task) {
		if (!isset(self::$counts[$task])) {
			self::$counts[$task] = 0;
		}
		self::$counts[$task]++;
	}

	static function getCount($task) {
		if (!isset(self::$counts[$task])) {
			self::$counts[$task] = 0;
		}
		return self::$counts[$task];
	}

	static function getCounts() {
		return self::$counts;
	}

	static function logLegacyCall($info = '', $stackLevelsBack = 0) {

		$caller = self::getCallerInfo(1 + $stackLevelsBack);

		$logMessage = 'Deprecated call in file "'.$caller['file'].'", line "'.$caller['line'].'".';
		if ($info) {
			$logMessage .= 'Info: '.$info;
		}

		self::log($logMessage, 'deprecated');
	}

	/**
	 *
	 * Logs a message to the log file and optionally triggers a platform exception
	 *
	 * @param string $messageInternal The internal log message, only shown in the log file (or on the website if debug mode is on and a JError is triggered)
	 * @param string $logLevel The log level of the message (debug, notice, warning, error etc). Controls which file is used for logging, debug is ignored unless debug mode is on.
	 * @param string $messagePublic This triggers a platform exception and uses the text to display
	 *
	 * @return string|bool|null $identifier String that identifies the log message, false on error, null if ignored because of debug setting
	 */
	static function log($messageInternal, $logLevel = 'debug', $messagePublic = '') {

		// Sanitize log level string
		$logLevel = strtolower($logLevel);

		// Ignore debug messages if not in debug mode
		if (($logLevel == 'debug' || $logLevel == 'inconsistencies') && KenedoPlatform::p()->getDebug() == 0) {
			return NULL;
		}

		// Determine which log file to use
		switch ($logLevel) {

			case 'debug':
				$logFile = 'configbox_debug.php';
				break;

			case 'warning':
				$logFile = 'configbox_warnings.php';
				break;

			case 'error':
				$logFile = 'configbox_errors.php';
				break;

			case 'critical':
				$logFile = 'configbox_errors.php';
				break;

			case 'payment':
				$logFile = 'configbox_payment.php';
				break;

			case 'payment_tracking':
				$logFile = 'configbox_payment_tracking.php';
				break;

			case 'calculation_code_error':
				$logFile = 'configbox_calculation_code_errors.php';
				break;

			case 'permissions':
				$logFile = 'configbox_permissions.php';
				break;

			case 'php_error':
				$logFile = 'configbox_php_messages.php';
				break;

			case 'php_platform_errors':
				$logFile = 'configbox_php_platform_errors.php';
				break;

			case 'deprecated':
				$logFile = 'configbox_deprecated.php';
				break;

			case 'external_apis':
				$logFile = 'configbox_external_apis.php';
				break;

			case 'db_error':
				$logFile = 'configbox_db_errors.php';
				break;

			case 'upgrade_errors':
				$logFile = 'configbox_upgrade_errors.php';
				break;

			case 'authorization':
				$logFile = 'configbox_authorization.php';
				break;

			case 'inconsistencies':
				$logFile = 'configbox_inconsistencies.php';
				break;

			case 'cleanup':
				$logFile = 'configbox_cleanup.php';
				break;

			default:

				// Custom log files (log level starts with custom_)
				if (strpos($logLevel, 'custom_') === 0) {
					// Clear out any nastiness
					$logFile = str_replace(array('/', '\\', '.'), '', $logLevel);
					$logFile = strtolower($logFile).'.php';

				}
				else {
					$logFile = 'configbox_general.php';
				}

		}

		$logPath = KenedoPlatform::p()->getLogPath().DS.'configbox';
		if (!is_dir($logPath)) {
			mkdir($logPath,0777,true);
		}

		$backTrace = self::getCallerInfo();
		$timeString = KenedoTimeHelper::getFormattedOnly('NOW', 'datetime');
		$identifier = self::getRandomString();

		$line = "\n". $timeString ."\t". $logLevel ."\t". $identifier ."\t".  $backTrace['class'].'->'.$backTrace['method'].' (Line '.$backTrace['line'].')' ."\t". $messageInternal;

		if (!is_file($logPath.DS.$logFile)) {
			touch($logPath.DS.$logFile);
			file_put_contents($logPath.DS.$logFile, "<?php\ndie('No browser access, use FTP.');\n?>\n\n", FILE_APPEND);
		}

		$response = file_put_contents($logPath.DS.$logFile, $line, FILE_APPEND);

		if ($response !== false) {
			$response = true;
		}

		if ( KenedoPlatform::p()->getDebug() ) {
			if (!empty($messagePublic)) {
				KenedoPlatform::p()->raiseError($identifier,$messagePublic . ' '. $messageInternal);
			}
		}
		else {
			if (!empty($messagePublic)) {
				KenedoPlatform::p()->raiseError($identifier,$messagePublic);
			}
		}

		if ($response == true) {
			return $identifier;
		}
		else {
			return false;
		}

	}

	static protected function getCallerInfo($offset = 0) {
		$stack = debug_backtrace(false);
		foreach ($stack as $index=>$level) {
			if (empty($level['class']) || $level['class'] != 'KLog') {
				$info = array();
				$info['class'] = isset($stack[($offset+$index)]['class']) ? $stack[($offset+$index)]['class'] : NULL;
				$info['method'] = isset($stack[($offset+$index)]['function']) ? $stack[($offset+$index)]['function'] : NULL;
				$info['line'] = isset($stack[($offset+$index-1)]['line']) ? $stack[($offset+$index-1)]['line'] : NULL;
				$info['file'] = isset($stack[($offset+$index-1)]['file']) ? $stack[($offset+$index-1)]['file'] : NULL;
				return $info;
			}
		}
		// This is for weird situations
		$info = array();
		$info['class'] = isset($stack[2]['class']) ? $stack[2]['class'] : NULL;
		$info['method'] = isset($stack[2]['function']) ? $stack[2]['function'] : NULL;
		$info['line'] = isset($stack[1]['line']) ? $stack[1]['line'] : NULL;
		$info['file'] = isset($stack[1]['file']) ? $stack[1]['file'] : NULL;
		return $info;
	}

	static protected function getRandomString($length = 10) {

		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$string = '';

		for ($p = 0; $p < $length; $p++) {
			$string .= $characters{mt_rand(0, strlen($characters) - 1)};
		}

		return $string;
	}

	static public function handleError($number, $message, $file, $line) {

		if (!in_array($number,self::$ignoredErrorNumbers)) {

			// Log errors (except notices) that don't come from ConfigBox separately
			if ( !strstr($file,'com_configbox') && !strstr($file,'mod_configbox') && !strstr($file,'configbox.php') ) {
				$logLevel = 'php_platform_errors';
				if ($number != E_NOTICE) { // We better let PHP notices from non-CB code alone
					self::log('PHP error (Type: '.self::getErrorType($number).', message: '.$message.', file: '.$file.', line: '.$line, $logLevel );
				}
			}
			else {
				$logLevel = 'php_error';
				self::log('PHP error (Type: '.self::getErrorType($number).', message: '.$message.', file: '.$file.', line: '.$line, $logLevel );
			}

			// Log a backtrace in case system runs in debug mode or if error is an E_ERROR
			if (KenedoPlatform::p()->getDebug() || $number == E_ERROR) {
				$trace = debug_backtrace(false);
				array_shift($trace);
				self::log("Backtrace:\n". var_export($trace, true)."\n\n", $logLevel);
			}

			// Joomla somehow feels that displaying errors and reporting errors is the same, so let's make the error get logged when error_reporting is on
			if (ini_get('error_reporting') != 0) {
				// We'll mention the CB log file with more details
				$cbLogfile = ($logLevel == 'php_platform_errors') ? 'configbox_php_platform_errors.php' : 'configbox_php_messages.php';
				error_log(self::getErrorType($number) . ': '.$message.' in file '.$file.' on line:'.$line.'. Depending on the type of error you find detailed info in the log file "'.$cbLogfile.'", located in the Joomla log folder, subfolder configbox.');
			}

		}

	}

	/**
	 * Logs uncaught exceptions
	 * @param Exception $exception
	 * @throws Exception
	 */
	static public function handleException($exception) {
		$logMessage = 'PHP exception from exception handler (Message: "'.$exception->getMessage().'", file: "'.$exception->getFile().'", line: "'.$exception->getLine().'")';
		self::log($logMessage, 'php_error');
		error_log($logMessage.'. Also see Configbox php_errors.php in Joomla log folder (subfolder configbox).');
		throw $exception;
	}

	static public function handleShutdown() {

		$error = error_get_last();
		if (!$error) {
			return;
		}

		// Log errors from non-CB files separately
		if (!strstr($error['file'], 'com_configbox') && !strstr($error['file'], 'mod_configbox') && !strstr($error['file'], 'configbox.php') ) {
			$logLevel = 'php_platform_errors';
		}
		else {
			$logLevel = 'php_error';
		}

		if($error && $error['type'] != E_WARNING && $error['type'] != 8 && $error['type'] != E_STRICT && $error['type'] != E_DEPRECATED) {
			if (!in_array($error['type'],self::$ignoredErrorNumbers)) {
				self::log('PHP error (Type: '.self::getErrorType($error['type']).', message: '.$error['message'].', file: '.$error['file'].', line: '.$error['line'], $logLevel );
			}
		}

		// Pointless since backtrace doesn't get the right info during shutdown
//		if ($error['type'] == E_ERROR) {
//			$trace = debug_backtrace(false);
//			array_shift($trace);
//			self::log("Backtrace:\n". var_export($trace, true)."\n\n", $logLevel);
//		}

	}

	protected static function getErrorType($errorNumber) {
		return (isset(self::$errorCodes[$errorNumber])) ? self::$errorCodes[$errorNumber] : $errorNumber;
	}

}
if (function_exists('class_alias')) {
	class_alias('KLog', 'ConfigboxDebugger');
}
