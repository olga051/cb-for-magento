<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCheckout */
?>

<?php if ($this->canGoBackToCart) { ?>
	<a class="button-back-to-cart navbutton-medium back leftmost floatleft" href="<?php echo $this->backToCartUrl;?>"><span class="nav-center"><?php echo KText::_('Back to cart');?></span></a>
<?php } ?>

<a class="button-order trigger-place-order navbutton-medium next rightmost floatright"><span class="nav-center"><i class="fa fa-spin fa-spinner loading-indicator"></i><?php echo KText::_('TEXT_ORDER_BUTTON');?></span></a>	