<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewUserorder */
?>
<div id="com_configbox">
	<div id="view-userorder">
		<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

			<div class="order-not-found-notice">
				<p><?php echo KText::_('This order does not exist.'); ?></p>
			</div>

		</div>
	</div>
</div>