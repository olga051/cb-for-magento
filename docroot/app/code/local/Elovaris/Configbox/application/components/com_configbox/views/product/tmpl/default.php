<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewProduct */
?>
<div id="com_configbox">
<div id="view-product">
<div id="view-product-id-<?php echo KRequest::getInt('prod_id',0);?>">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">
<?php
if (empty($this->product) || $this->product->discontinued == 1) {
	echo KText::_('This product does not exist.');
}
elseif ($this->useLongdescTemplate) {
	echo $this->product->longdescription;
}
else {
	?>

	<?php if (ConfigboxPermissionHelper::canQuickEdit()) echo ConfigboxQuickeditHelper::renderProductPageButtons($this->product);?>

	<?php if ($this->showPageHeading) { ?>
		<h1 class="componentheading"><?php echo hsc($this->pageHeading); ?></h1>
	<?php } ?>

	<a class="product-image" href="<?php echo $this->linkconfigure;?>">
		<img class="product_image" src="<?php echo $this->product->imagesrc;?>" alt="<?php echo hsc($this->product->title);?>" />
	</a>

	<?php if (ConfigboxPermissionHelper::canSeePricing()) { ?>
		<?php if ($this->product->custom_price_text) { ?>
			<span class="product-price"><?php echo $this->product->custom_price_text;?></span>
		<?php } elseif ($this->product->{$this->priceKey} != 0|| ConfigboxPermissionHelper::canQuickEdit()) { ?>
			<?php if ($this->product->wasPrice != 0) { ?>
				<span class="product-was-price product-was-price-<?php echo (int)$this->product->id;?>"> <?php echo cbprice($this->product->wasPrice);?></span>
			<?php } ?>
			<span class="product-price"><?php echo cbprice($this->product->{$this->priceKey}); ?> <?php echo hsc($this->product->priceLabel);?></span>
		<?php } ?>

		<?php if ($this->product->custom_price_text_recurring) { ?>
			<span class="product-price-recurring"><?php echo $this->product->custom_price_text_recurring;?></span>
		<?php } elseif ($this->product->{$this->priceKeyRecurring} != 0) { ?>
			<?php if ($this->product->wasPriceRecurring != 0) { ?>
				<span class="product-was-price-recurring product-was-price-recurring-<?php echo (int)$this->product->id;?>"> <?php echo cbprice($this->product->wasPriceRecurring);?></span>
			<?php } ?>
			<span class="product-price-recurring"><?php echo cbprice($this->product->{$this->priceKeyRecurring}). ' '.hsc($this->product->priceLabelRecurring);?></span>
		<?php } ?>
	<?php } ?>

	<h3 class="product-title"><?php echo hsc($this->product->title);?></h3>

	<?php if ($this->product->longdescription) { ?>
		<div class="product-description"><?php echo $this->product->longdescription;?></div>
	<?php } elseif ($this->product->description) { ?>
		<div class="product-description"><?php echo $this->product->description;?></div>
	<?php } ?>

	<?php if ($this->product->isConfigurable) { ?>
		<a class="button-frontend-small link-configure" href="<?php echo $this->linkconfigure;?>"><?php echo KText::sprintf('Configure %s',hsc($this->product->title));?></a>
	<?php } ?>

	<?php if ($this->product->show_buy_button) { ?>
		<a rel="nofollow" class="button-frontend-small link-buy" href="<?php echo $this->linkbuy;?>"><?php echo KText::_('Buy');?></a>
	<?php } ?>

	<div class="clear"></div>

	<?php if ($this->showProductDetailPanes) { ?>
		<div class="product-detail-panes-wrapper"><?php echo $this->productDetailPanes; ?></div>
	<?php } ?>

	<?php
}
?>
</div>
</div>
</div>
</div>