<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewBundle */
?>
<div id="com_configbox">
<div id="view-bundle" class="view-bundle-<?php echo (int)$this->bundle->id;?>">
<div id="template-<?php echo $this->template;?>">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">
<?php 
if (!$this->bundle || empty($this->bundleOrder)) {
	?>
	<div><?php echo KText::_('Bundle not found.');?></div>
	<?php
}
else {
	?>
	<div class="product-<?php echo (int)$this->bundleOrder->id;?>">
		<h1 class="componentheading bundle-title"><?php echo hsc($this->bundleProduct->title);?></h1>
		
		<?php
		$position = $this->bundleOrder;
		?>
		<div class="order-product order-product-id-<?php echo $position->productData->id;?>">


			<div class="bundle-description">
				<?php echo $position->productData->description;?>
			</div>


			<?php if (ConfigboxProductImageHelper::hasProductImage($position)) { ?>
				<div class="bundle-visualization">
					<?php echo ConfigboxProductImageHelper::getProductImageHtml($position); ?>
				</div>
			<?php } ?>

			<div class="bundle-configuration-quick">
				<?php foreach ($position->elements as $element) {
					
					if ($element->displaySettings->showInCart == false || !$element->selection->outputValue) continue;
					?>
					<div class="element-list-item">
						<span class="element-title"><?php echo hsc($element->title);?>:</span>
						<span class="option-title"><?php echo $element->selection->outputValue;?></span>
					</div>
				<?php } ?>
				
				<?php if ($position->productData->recurring_interval) { ?>
					<div class="element-list-item">
						<span class="element-title"><?php echo KText::_('Payment Interval');?>:</span>
						<span class="option-title"><?php echo hsc($position->productData->recurring_interval);?></span>
					</div>
				<?php } ?>
			</div>
			
			<?php if (ConfigboxPermissionHelper::canSeePricing()) { ?>
			
			<div class="bundle-configuration">
			
				<table class="elementlist elementlist-product-<?php echo (int) $position->productData->id;?>">
					
					<tr>
						<th class="element-list-item">
							<h3><?php echo hsc($this->bundleProduct->title);?> <?php echo ($position->quantity > 1) ? KText::_('Quantity').': '.intval($position->quantity) : '';?></h3>
						</th>
						
						<?php if ($position->usesRecurring) { ?>
							<th class="element-list-price-recurring"><?php echo hsc($position->productData->priceLabelRecurring);?></th>
						<?php } ?>
						
						<th class="element-list-price"><?php echo hsc($position->productData->priceLabel);?></th>
						
					</tr>
					
					<?php if ($position->productData->priceNet != 0) { ?>
						<tr>
							<td class="element-list-item"><?php echo KText::_('Base Price');?></td>
							
							<?php if ($position->usesRecurring) { ?>
								<td class="element-list-price-recurring item-price">
									<?php echo cbprice( ($this->mode == 'b2b') ? $position->productData->priceRecurringNet : $position->productData->priceRecurringGross, true, true ); ?>
								</td>
							<?php } ?>
							
							<td class="element-list-price item-price">
								<?php echo cbprice( ($this->mode == 'b2b') ? $position->productData->priceNet : $position->productData->priceGross, true, true );?>
							</td>
							
						</tr>
					<?php } ?>
					
					<?php
					$taxIncEx = ($this->mode == 'b2b') ? 'excl.':'incl.';
					$taxPlusInc = ($this->mode == 'b2b') ? 'Plus':'Incl.';
					foreach ($position->elements as $element) {
						if ( $element->displaySettings->showInCart == false ) {
							continue;
						}
						?>
						<tr>
							<td class="element-list-item">
								<span class="elementtitle"><?php echo hsc($element->title);?><span class="element-option-seperator">:</span></span>
								<span class="option-title"><?php echo $element->selection->outputValue;?></span>
							</td>
							
							<?php if ($position->usesRecurring) { ?>
								<td class="element-list-price-recurring item-price">
									<?php echo cbprice( ($this->mode == 'b2b') ? $element->selection->priceRecurringGross : $element->selection->priceRecurringGross, true, true);?>
								</td>
							<?php } ?>
							
							<td class="element-list-price item-price">
								<?php echo cbprice( ($this->mode == 'b2b') ? $element->selection->priceNet : $element->selection->priceGross, true, true);?>
							</td>
							
						</tr>
					<?php } ?>
					
					
					<?php if ($position->quantity > 1) { ?>
						<tr class="subtotal-net">
							<td class="element-list-item"><?php echo KText::_('Price per item');?></td>
							
							<?php if ($position->usesRecurring) { ?>
								<td class="element-list-price-recurring item-price">
									<?php echo cbprice(( ($this->mode == 'b2b') ? $position->totalUnreducedRecurringNet : $position->totalUnreducedRecurringGross) / $position->quantity);?>
								</td>
							<?php } ?>
							
							<td class="element-list-price item-price">
								<?php echo cbprice(( ($this->mode == 'b2b') ? $position->totalUnreducedNet : $position->totalUnreducedGross) / $position->quantity);?>
							</td>
							
						</tr>
					<?php } ?>
					
					<?php if ($this->bundle->discountNet or $this->bundle->discountRecurringNet) { ?>
						<tr class="discount">
							<td class="element-list-item"><?php echo hsc($this->bundle->discountTitle);?></td>

							<?php if ($position->usesRecurring) { ?>
								<td class="element-list-price-recurring item-price">
									<?php echo cbprice( ($this->mode == 'b2b') ? $this->bundle->discountRecurringNet : $this->bundle->discountRecurringGross, true, true);?>
								</td>
							<?php } ?>
							
							<td class="element-list-price item-price"><?php echo cbprice( ($this->mode == 'b2b') ? $this->bundle->discountNet : $this->bundle->discountGross);?></td>
						
						</tr>
					<?php } ?>
					
					<tr class="total-tax">
						<td class="element-list-item">
							<?php $text = ($position->totalUnreducedRecurringNet != 0 && $position->productData->taxRateRecurring != $position->productData->taxRate) ? $taxPlusInc.' %s regular, %s tax recurring' : $taxPlusInc.' %s tax';?>
							<?php echo KText::sprintf($text, cbtaxrate($position->productData->taxRate), cbtaxrate($position->productData->taxRateRecurring));?>
						</td>
						
						<?php if ($position->usesRecurring) { ?>
							<td class="element-list-price-recurring item-price">
								<?php echo cbprice($position->totalUnreducedRecurringTax + $this->bundle->discountRecurringTax);?>
							</td>
						<?php } ?>
						
						<td class="element-list-price item-price">
							<?php echo cbprice($position->totalUnreducedTax + $this->bundle->discountTax);?>
						</td>
					</tr>
					
					<tr class="total">
						<td class="element-list-item"><?php echo KText::_('Total');?></td>				
						
						<?php if ($position->usesRecurring) { ?>
							<td class="element-list-price item-price"><?php echo cbprice($position->totalUnreducedRecurringGross + $this->bundle->discountRecurringGross);?></td>
						<?php } ?>
						
						<td class="element-list-price item-price"><?php echo cbprice($position->totalUnreducedGross + $this->bundle->discountGross);?></td>
					</tr>
					
				</table>
			
			</div> <!-- configuration -->
			<?php } ?>
		</div>
		
		<div class="bundle-recommendation">
			
			<fieldset class="recommendation-form">
				
				<legend><?php echo KText::_('Send a message');?></legend>
				
				<div class="sender-name">
					<label for="recommend-sender-name"><?php echo KText::_('Your name');?></label>
					<input type="text" id="recommend-sender-name" name="recommend-sender-name" />
				</div>
				
				<div class="sender-email">
					<label for="recommend-sender-email"><?php echo KText::_('Your email address');?></label>
					<input type="text" id="recommend-sender-email" name="recommend-sender-email" />
				</div>
				
				<div class="email">
					<label for="recommend-email"><?php echo KText::_('Email address of receipient');?></label>
					<input type="text" id="recommend-email" name="recommend-email" />
				</div>
				
				<div class="field-message">
					<label for="recommend-message"><?php echo KText::_('Message');?></label>
					<textarea id="recommend-message" name="recommend-message"><?php echo $this->defaultMessage;?></textarea>
				</div>
				
				<?php if ($this->useCaptcha) { ?>
					<div class="field-captcha">
						<?php echo $this->recaptchaHtml;?>
					</div>
				<?php } ?>
				
				<div class="hidden-fields">
					<input type="hidden" name="recommend-bundle-id" id="recommend-bundle-id" value="<?php echo (int)$this->bundle->id;?>" />
				</div>
				
				<div class="buttons">
					<a class="button-frontend-small recommendation-close"><?php echo KText::_('Close');?></a>
					<a class="button-frontend-small recommendation-send"><?php echo KText::_('Send');?></a>
					<span class="feedback">
						<span class="fields-empty"><?php echo KText::_('Please fill all fields.');?></span>
					</span>
				</div>
				
			</fieldset>
			<div class="clear"></div>
		</div>
		
		
		<div class="bundle-controls">
			<a rel="nofollow" class="navbutton-medium next add-to-cart-button" href="<?php echo $this->addBundleToCartLink;?>">
				<span class="nav-title"><?php echo KText::_('Buy');?></span>
				<span class="nav-text"><?php echo KText::_('Add to cart');?></span>
			</a>
			<a rel="nofollow" class="navbutton-medium edit-order-button" href="<?php echo $this->editBundleLink;?>">
				<span class="nav-title"><?php echo KText::_('Change');?></span>
				<span class="nav-text"><?php echo KText::_('the configuration');?></span>
			</a>
			<a rel="nofollow" class="navbutton-medium recommend-bundle-button">
				<span class="nav-title"><?php echo KText::_('Recommend');?></span>
				<span class="nav-text"><?php echo KText::_('via email');?></span>
			</a>
			
			<div class="clear"></div>
		</div>
	</div>
	<?php
}
?>

</div></div></div></div>