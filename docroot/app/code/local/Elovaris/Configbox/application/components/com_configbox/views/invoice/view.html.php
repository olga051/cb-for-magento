<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewInvoice extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var string Complete URL to the default CSS file (there is a separate CSS file for quote PDFs)
	 */
	public $hrefCssSystem;

	/**
	 * @var string Complete URL to the custom CSS file.
	 */
	public $hrefCssCustom;

	/**
	 * @var boolean Indicates if the shop logo should be displayed (depends on if one is uploaded in shop data)
	 */
	public $useShopLogo;

	/**
	 * @var int Pixel height of the logo as it should appear.
	 */
	public $shopLogoHeight;

	/**
	 * @var int Pixel width of the logo as it should appear.
	 */
	public $shopLogoWidth;

	/**
	 * @var string Complete URL to the shop logo.
	 */
	public $shopLogoUrl;

	/**
	 * @var object $shopData Object holding all store information
	 */
	public $shopData;

	/**
	 * @var int $orderId ID of the order record used in the quote - NEEDS TO BE SET BEFORE CALLING DISPLAY()
	 */
	public $orderId;

	/**
	 * @var object $orderRecord Order record holding everything about the order
	 * @see ConfigboxModelOrderrecord::getOrderRecord
	 */
	public $orderRecord;

	/**
	 * @var string $orderRecordHtml HTML with the info about all positions of the order record
	 * @see ConfigboxViewRecord
	 */
	public $orderRecordHtml;

	/**
	 * @var string $invoiceNumber Serial of the invoice. See Settings/Invoice for serial generation.
	 */
	public $invoiceNumber;

	/**
	 * @var string $paymentMethodHtml HTML that comes from the invoice.php template of the selected payment method PSP.
	 */
	public $paymentMethodHtml;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {

		// Order ID has to be passed via ->assign()
		$orderId = $this->orderId;
		
		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
		$orderModel->unsetOrderRecord($orderId);
		$orderRecord = $orderModel->getOrderRecord($orderId);

		if (!$orderRecord) {
			return;
		}

		$this->assignRef('orderRecord', $orderRecord);
		
		$view = KenedoView::getView('ConfigboxViewRecord');
		$view->assignRef('orderRecord', $orderRecord);
		$view->assign('showIn','quotation');
		$view->assign('showChangeLinks',false);
		$view->assign('showProductDetails',false);
		$html = $view->getViewOutput();
		$this->assignRef('orderRecordHtml',$html);

		$connectorName = $orderRecord->payment->connector_name;
		$folder = ConfigboxPspHelper::getPspConnectorFolder($connectorName);

		if (is_file($folder.DS.'invoice.php')) {
			ob_start();
			include($folder.DS.'invoice.php');
			$html = ob_get_clean();
			$this->assign('paymentMethodHtml', $html);
		}
		else {
			$this->assign('paymentMethodHtml', '');
		}

		$shopData = ConfigboxStoreHelper::getStoreRecord($orderRecord->store_id);
		$this->assignRef('shopData', $shopData);

		$this->assign('hrefCssSystem', KPATH_DIR_ASSETS.DS.'css'.DS.'pdf-invoice.css');
		$this->assign('hrefCssCustom', CONFIGBOX_DIR_CUSTOMIZATION_ASSETS_CSS.DS.'style_overrides.css');

		$filePath = CONFIGBOX_DIR_SHOP_LOGO .DS. $shopData->shoplogo;

		$this->assign('useShopLogo', false);
		$this->assign('shopLogoWidth',	0);
		$this->assign('shopLogoHeight',	0);

		if (is_file($filePath)) {

			$this->assign('useShopLogo', true);
			$this->assign('shopLogoUrl', $filePath);

			$image = new ConfigboxImageResizer($filePath);

			$maxWidth = 1000;
			$maxHeight = 60;

			if ($image->width > $maxWidth || $image->height > $maxHeight) {
				$dimensions = $image->getDimensions($maxWidth, $maxHeight, 'containment');
				$this->assign('shopLogoWidth',	intval($dimensions['optimalWidth']));
				$this->assign('shopLogoHeight',	intval($dimensions['optimalHeight']));
			}

		}

	}
}