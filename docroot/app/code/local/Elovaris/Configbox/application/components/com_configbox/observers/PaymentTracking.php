<?php
defined('CB_VALID_ENTRY') or die();

class ObserverPaymentTracking {

	/**
	 * Schedules a payment tracking
	 * @param int $orderId
	 * @param int $status
	 */
	function onConfigBoxSetStatus($orderId, $status) {

		$paidStatus = KenedoObserver::triggerEvent('onConfigBoxGetStatusCodeForType',array('paid'),true);

		if ($status != $paidStatus) {
			return;
		}

		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
		$orderRecord = $orderModel->getOrderRecord($orderId);
		$db = KenedoPlatform::getDb();

		$query = "SELECT `id` FROM `#__cbcheckout_order_payment_trackings` WHERE `user_id` = ".intval($orderRecord->user_id)." AND `order_id` = ".intval($orderRecord->id);
		$db->setQuery($query);
		$existsAlready = $db->loadResult();

		if ($existsAlready) {
			return;
		}
		KLog::log('Tracking scheduled for user id "'.$orderRecord->user_id.'" and order id "'.$orderId.'"','payment_tracking');
		$query = "INSERT INTO `#__cbcheckout_order_payment_trackings` (`user_id`, `order_id`) VALUES ('".$orderRecord->user_id."','".$orderRecord->id."')";
		$db->setQuery($query);
		$db->query();

	}

	/**
	 * Inserts tracking code for the user's past purchase in the app's HTML output when the document type is html.
	 * @param string $content
	 */
	public function onAfterRender(&$content) {

		$documentType = KenedoPlatform::p()->getDocumentType();

		if ($documentType != 'html') {
			return;
		}

		$userId = ConfigboxUserHelper::getUserId();

		$db = KenedoPlatform::getDb();
		$query = "SELECT * FROM `#__cbcheckout_order_payment_trackings` WHERE `user_id` = ".intval($userId)." AND `got_tracked` = '0'";
		$db->setQuery($query);
		$scheduledTrackings = $db->loadObjectList();

		if ($scheduledTrackings) {

			foreach ($scheduledTrackings as $scheduledTracking) {
				$trackingInstructions = KenedoObserver::triggerEvent('onPaymentTrackingRequested',array(&$scheduledTracking));
				foreach ($trackingInstructions as $observerName=>$trackingInstruction) {

					$message = 'Tracking for id "'.$scheduledTracking->id.'" from "'.$observerName.'" placed for user id "'.$scheduledTracking->user_id.'" and order id "'.$scheduledTracking->order_id.'"';

					if (!empty( $trackingInstruction['head_js'] ) ) {
						KLog::log($message.'. Using head_js','payment_tracking');
						KenedoPlatform::p()->addScriptDeclaration($trackingInstruction['head_js'], true);
					}

					if (!empty( $trackingInstruction['body'] ) ) {
						KLog::log($message.'. Using body','payment_tracking');
						$content .= $trackingInstruction['body'];
					}

				}
			}

			$query = "UPDATE `#__cbcheckout_order_payment_trackings` SET `got_tracked` = '1' WHERE `user_id` = ".intval($userId);
			$db->setQuery($query);
			$db->query();

		}

	}

	/**
	 * Gets the GA ecommerce tracking script tag
	 * @param stdClass $scheduledTracking Holding tracking schedule data
	 * @return array $response Array structure holding tracking id as key and sub array with key 'body' or 'head_js' holding the HTML to be injected
	 */
	public function onPaymentTrackingRequested($scheduledTracking) {

		$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
		$orderRecord = $orderModel->getOrderRecord($scheduledTracking->order_id);

		$response = array();
		$response['body'] = $this->getGAEcommerceTrackingJs($orderRecord);

		return $response;

	}

	/**
	 * Returns the GA Ecommerce tracking JS tag (Event Tracking, Asynchronous Tracking and Univeral Tracking).
	 * @param object $orderRecord
	 * @return string GA Ecommerce tracking JS tag
	 */
	protected function getGAEcommerceTrackingJs($orderRecord) {
		ob_start();
		?>
		<script type="text/javascript">
			cbj(document).ready(function(){

				var _gaq = window._gaq || [];

				// Track event to be used for goal definition
				_gaq.push(['_trackEvent', 'configbox_goals', 'order_payment', undefined, <?php echo number_format($orderRecord->basePayableAmount, 2, '.', '');?>]);

				_gaq.push(['_addTrans',
					'<?php echo intval($orderRecord->id);?>', // transaction ID - required
					'',  // affiliation or store name
					'<?php echo number_format($orderRecord->totalNet + $orderRecord->payment->priceNet, 2, '.', '');?>', // total - required
					'<?php echo number_format($orderRecord->totalTax + $orderRecord->delivery->priceTax + $orderRecord->payment->priceTax, 2, '.', '');?>', // tax
					'<?php echo number_format($orderRecord->delivery->priceNet, 2, '.', '');?>', // shipping
					'<?php echo $orderRecord->orderAddress->billingcity;?>', // city
					'<?php echo $orderRecord->orderAddress->billingstatecode;?>', // state or province
					'<?php echo $orderRecord->orderAddress->billingcountry_2_code;?>' // country
				]);

				<?php foreach ($orderRecord->positions as $position) { ?>

				_gaq.push(['_addItem',
					'<?php echo intval($orderRecord->id);?>', // transaction ID - required
					'<?php echo ($position->product_sku) ? hsc($position->product_sku) : hsc($position->product_id);?>', // SKU/code - required
					'<?php echo hsc($position->productTitle);?>', // product name
					'',   // listing or variation
					'<?php echo number_format($position->totalReducedNet / $position->quantity, 2, '.', '');?>', // unit price - required
					'<?php echo intval($position->quantity);?>' // quantity - required
				]);

				<?php } ?>

				_gaq.push(['_set', 'currencyCode', '<?php echo hsc($orderRecord->currency->code);?>']);
				_gaq.push(['_trackTrans']); //submits transaction to the Analytics servers


				// Now for Universal Analytics

				// Prepare the ga object just in case
				window.ga = window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;

				// Put in the ready callback
				ga(function() {

					// Prepare the transaction data
					var transactionData = {
						'id': '<?php echo intval($orderRecord->id);?>',
						'affiliation': '',
						'revenue': '<?php echo number_format($orderRecord->totalNet + $orderRecord->payment->priceNet, 2, '.', '');?>',
						'shipping': '<?php echo number_format($orderRecord->delivery->priceNet, 2, '.', '');?>',
						'tax': '<?php echo number_format($orderRecord->totalTax + $orderRecord->delivery->priceTax + $orderRecord->payment->priceTax, 2, '.', '');?>',
						'currency': '<?php echo hsc($orderRecord->currency->code);?>'
					};

					// Prepare the items array
					var itemData = [];

					<?php foreach ($orderRecord->positions as $position) { ?>
					// Collect all positions as items
					itemData.push({
						'id': '<?php echo intval($orderRecord->id);?>',
						'name': '<?php echo hsc($position->productTitle);?>',
						'sku': '<?php echo ($position->product_sku) ? hsc($position->product_sku) : hsc($position->product_id);?>',
						'category': '',
						'price': '<?php echo number_format($position->totalReducedNet / $position->quantity, 2, '.', '');?>',
						'quantity': '<?php echo intval($position->quantity);?>'
					});
					<?php } ?>

					// Get all trackers, init trackerName for the loop
					var trackers = ga.getAll();
					var trackerName;

					// Loop through trackers and add transation/items and send
					for (var i in trackers) {
						if (trackers.hasOwnProperty(i) === true) {
							// Get the tracker name
							trackerName = trackers[i].get('name');

							ga(trackerName + '.send', {
								hitType: 'event',
								eventCategory: 'configbox_goals',
								eventAction: 'order_payment',
								eventLabel: '',
								eventValue: <?php echo number_format($orderRecord->basePayableAmount, 2, '.', '');?>
							});

							// Require the ecommerce plugin
							ga(trackerName + '.require', 'ecommerce');
							// Add the transaction
							ga(trackerName + '.ecommerce:addTransaction', transactionData);
							// Loop through the prepared position data and add the items
							for (var itemDataKey in itemData) {
								if (itemData.hasOwnProperty(itemDataKey) === true) {
									ga(trackerName + '.ecommerce:addItem', itemData[itemDataKey]);
								}
							}
							// Off they go..
							ga(trackerName + '.ecommerce:send');

						}
					}
				});

			});
		</script>
		<?php
		return ob_get_clean();

	}

}