<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */

$choices = explode("\n",$this->element->choices);
foreach ($choices as &$choice) {
	$choice = trim($choice);
}
unset($choice);

$choiceMade = trim($this->element->getRawValue());
// If there is nothing, see if there is a default value
if (!$choiceMade) {
	$initial    = $this->element->getInitialValue();
	$choiceMade = !empty($initial['text']) ? $initial['text'] : '';
}

$customValue = '';

if ($choiceMade != '') {
	// If the entered text isn't one of the choices, but there is a custom field, make sure it's filled in right
	if (array_search($choiceMade, $choices) === false && array_search('custom', $choices)) {
		$customValue = $choiceMade;
		$choiceMade  = 'custom';
	}
}
?>
<ul class="choice-list">
	<?php foreach ($choices as $key=>$choice) { ?>
		<li class="choice-item">
			<input class="configbox-choice-field" <?php echo ($this->element->applies()) ? '':'disabled="disabled"';?> <?php echo ($choiceMade == $choice) ? ' checked="checked" ':' ';?>type="radio" id="element-widget-<?php echo (int)$this->element->id.'-'.$key;?>" name="element-widget-<?php echo (int)$this->element->id;?>" value="<?php echo hsc($choice);?>" />
			<?php if (trim($choice) == 'custom') { ?>
				<input class="configbox-choice-free-field" type="text" name="element-widget-<?php echo (int)$this->element->id;?>" value="<?php echo hsc($customValue);?>" <?php echo ($this->element->applies()) ? '':'disabled="disabled"';?> />
			<?php } else { ?>
				<label class="configbox-label" for="element-widget-<?php echo (int)$this->element->id.'-'.$key;?>"><?php echo hsc($choice);?> 
			<?php } ?>
			<span class="element-unit"><?php echo hsc($this->element->unit);?></span></label>
		</li>
	<?php } ?>
</ul>
