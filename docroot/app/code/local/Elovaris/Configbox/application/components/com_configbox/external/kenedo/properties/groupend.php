<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyGroupend extends KenedoProperty {
	
	function usesWrapper() {
		return false;
	}
	
	function getDataFromRequest(&$data) {
		return true;
	}

	public function getSelectsForGetRecord($selectAliasPrefix = '', $selectAliasOverride = '') {
		return array();
	}

	public function getJoinsForGetRecord() {
		return array();
	}
	
}