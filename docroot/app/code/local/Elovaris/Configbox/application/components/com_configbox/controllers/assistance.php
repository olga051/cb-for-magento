<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerAssistance extends KenedoController {

	/**
	 * @return NULL
	 */
	protected function getDefaultModel() {
		return NULL;
	}

	/**
	 * @return ConfigboxViewInforequest
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewInforequest');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

	function display() {
		
		$userId = ConfigboxUserHelper::getUserId();
		$user = ConfigboxUserHelper::getUser($userId);
		$group = ConfigboxUserHelper::getGroupData($user->group_id);
		
		if ($group->enable_request_assistance == false) {
			$platformUserId = KenedoPlatform::p()->getUserId();
			if (ConfigboxPermissionHelper::canRequestAssistance($platformUserId) == false) {
				KLog::log('Platform user ID "'.$platformUserId.'" tried to request assistance, although his group permissions do not allow it.','permissions',KText::_('You cannot request assistance.'));
				return false;
			}
		}
		
		if (KRequest::getInt('cart_id')) {
				
			$cartId = KRequest::getInt('cart_id');
			
			$db = KenedoPlatform::getDb();
			$query = "SELECT `id`,`status`,`user_id` FROM `#__cbcheckout_order_records` WHERE `cart_id` = ".(int)$cartId;
			$db->setQuery($query);
			$orderRow = $db->loadObject();
				
			if ($orderRow && $orderRow->user_id != ConfigboxUserHelper::getUserId()) {
				$platformUserId = KenedoPlatform::p()->getUserId();
				if (ConfigboxPermissionHelper::canRequestAssistance($platformUserId) == false) {
					KLog::log('Platform user ID "'.$platformUserId.'" tried to request assistance for another customer\'s (customer with ID "'.$orderRow->userId.'") order.','permissions',KText::_('Order not found.'));
					return false;
				}
			}
				
			if ($orderRow && $orderRow->status == 12) {
				$checkoutRecordId = $orderRow->id;
			}
			else {
				// Get the cart object
				$cartModel = KenedoModel::getModel('ConfigboxModelCart');
				$cartModel->setId($cartId);
				$cartDetails = $cartModel->getCartDetails();
		
				$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
				$checkoutRecordId = $orderModel->createOrderRecord($cartDetails, 12);
			}
			
			KenedoObserver::triggerEvent('onConfigBoxSetStatus', array($checkoutRecordId, 12));
			
			KenedoPlatform::p()->sendSystemMessage(KText::_('Your request has been sent. We will get back to you as soon as possible.'));		
			$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=cart&cart_id='.(int)$cartId,false));
			$this->redirect();
			
		}

		return true;

	}
	
}