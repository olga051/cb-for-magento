<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdmincalcmatrix extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'admincalcmatrices';

	/**
	 * @var string[] Names of all calculations (IDs as key), suitable for dropdowns
	 */
	public $calculations;

	/**
	 * @var string[] Titles of all elements (IDs as key), suitable for dropdowns
	 */
	public $elements;

	/**
	 * @var boolean Indicates if the rows use an element with options (as opposed to text elements)
	 */
	public $rowHasOptions;

	/**
	 * @var boolean Indicates if the columns use an element with options (as opposed to text elements)
	 */
	public $columnHasOptions;

	/**
	 * @var object[] Options for the row's element (if row element has options)
	 */
	public $rowOptions;

	/**
	 * @var object[] Options for the columns's element (if column element has options)
	 */
	public $columnOptions;

	/**
	 * @var string[][] Holds matrix data. First array keys are column value, second are row values
	 */
	public $matrixValues;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {

		// Get and prepare the id
		$matrixId = KRequest::getInt('id',0);
		$this->assign('matrixId', $matrixId);

		// Get the calculations for the calculation axis picker
		$calcModel = KenedoModel::getModel('ConfigboxModelAdmincalculations');
		$calculations = $calcModel->getRecords();

		// Prepare the options for the matrix drop-down
		$calcItems = array();
		$calcItems[0] = KText::_('Select a calculation');
		foreach ($calculations as $calculation) {
			$calcItems[$calculation->id] = $calculation->name;
		}
		$this->assignRef('calculations', $calcItems);
		
		// Get the elements for the element axis picker
		$elements = ConfigboxRulesHelper::getElements();
		$elementItems = array();
		foreach ($elements as $id => $element) {
			$elementItems[$id] = $element->element_title;
		}
		unset($element);
		$this->assignRef('elements',$elementItems);

		// Prepare the matrix values
		$matrixValues = ConfigboxCalculation::getMatrixValues($matrixId);
		
		if (count($matrixValues) == 0) {
			$matrixValues[0] = array('','','','');
			$matrixValues[1] = array('','','','');
			$matrixValues[2] = array('','','','');
		}
		
		$this->assign('matrixValues', $matrixValues);

		// Prepare the item data
		$model = KenedoModel::getModel('ConfigboxModelAdmincalcmatrices');
		$record = $model->getRecord($matrixId);
		if (!$record) {
			$record = $model->initData();
		}
		$this->assignRef('record', $record);

		// Prepare a helper var to check if column uses elements with options
		$columnHasOptions = (isset($elements[$record->column_element_id]) && $elements[$record->column_element_id]->xref_count) ? '1':'0';
		$this->assign('columnHasOptions', $columnHasOptions);

		// Same as above for rows
		$rowHasOptions = (isset($elements[$record->row_element_id]) && $elements[$record->row_element_id]->xref_count) ? '1':'0';
		$this->assign('rowHasOptions', $rowHasOptions);

		$optionsModel = KenedoModel::getModel('ConfigboxModelAdminoptions');

		// Prepare option data for columns
		if ($record->column_type == 'element' && $columnHasOptions) {
			$columnOptions = $optionsModel->getOptionsByElementId($record->column_element_id);
			$this->assign('columnOptions', $columnOptions);
		}

		// Prepare option data for rows
		if ($record->row_type == 'element' && $rowHasOptions) {
			$rowOptions = $optionsModel->getOptionsByElementId($record->row_element_id);
			$this->assign('rowOptions', $rowOptions);
		}

		// Assign the entity fields
		$properties = $model->getProperties();
		$this->assignRef('properties', $properties);

		$this->addViewCssClasses();

	}
	
}
