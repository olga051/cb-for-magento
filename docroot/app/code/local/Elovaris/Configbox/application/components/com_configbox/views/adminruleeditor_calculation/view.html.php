<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdminRuleeditor_calculation extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var object[] Array of calculation data objects
	 */
	public $calculations;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {

		// Prepare the calculations for conditions
		$calcModel = KenedoModel::getModel('ConfigboxModelAdmincalculations');
		$calculations = $calcModel->getRecords();
		$this->assignRef('calculations',$calculations);

		$this->addViewCssClasses();

	}
	
}
