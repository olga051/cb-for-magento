<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerProduct extends KenedoController {

	/**
	 * @return ConfigboxModelProduct
	 */
	protected function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelProduct');
	}

	/**
	 * @return ConfigboxViewProduct
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewProduct');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

}