<?php
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this KenedoPropertyRule
 */

// Check for a page id (the rule editor will pre-select the right page)
// This property will be there when the option assignment is loaded
if (isset($this->data->parent_pages_id)) {
	$pageId = (int)$this->data->parent_pages_id;
}
// This one if the element is loaded
elseif (isset($this->data->page_id)) {
	$pageId = (int)$this->data->page_id;
}
else {
	$pageId = 0;
}

$ruleText = trim(ConfigboxRulesHelper::getRuleHtml($this->data->{$this->propertyName}, false));
$url = KLink::getRoute('index.php?option=com_configbox&controller=adminruleeditor&format=raw&in_modal=1', false);
?>

<input class="data-field" data-return-field-id="<?php echo $this->propertyName;?>" data-page-id="<?php echo intval($pageId);?>" type="hidden" name="<?php echo $this->propertyName;?>" id="<?php echo $this->propertyName;?>" value="<?php echo hsc($this->data->{$this->propertyName}); ?>" />

<a id="rule-text-<?php echo $this->propertyName;?>" class="rule-text trigger-edit-rule" data-editor-url="<?php echo $url;?>"><?php echo $ruleText;?></a>

<a id="edit-button-<?php echo $this->propertyName;?>" class="backend-button-small trigger-edit-rule" data-editor-url="<?php echo $url;?>" <?php echo ($ruleText != '' && trim($this->data->{$this->propertyName}) != '[]') ? 'style="display:none"':'' ?>><?php echo KText::_('Open rule editor')?></a>
