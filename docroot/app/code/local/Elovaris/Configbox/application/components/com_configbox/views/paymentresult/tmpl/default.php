<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewPaymentresult */
?>
<div id="com_configbox">
<div id="view-paymentresult">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">
	
	<p><?php echo KText::_('Thank you for your order.');?>
	<?php  
	if (!empty($this->shopData->shopemailsales)) {
		echo KText::_('If you have any further questions please send an email to'). ' ';
		?>
		<a href="mailto:<?php echo $this->shopData->shopemailsales;?>"><?php echo $this->shopData->shopemailsales;?></a>
		<?php
		
		if (!empty($this->shopData->shopphonesales)) {
			echo KText::sprintf('or call us at %s',$this->shopData->shopphonesales);
		}
		?>.
		
		<?php
	}
	?>
	</p>
	
	<ul>		
		<li><a href="<?php echo $this->linkToOrder;?>"><?php echo KText::_('See your order status');?></a></li>
		<li><a href="<?php echo $this->linkToCustomerProfile;?>"><?php echo KText::_('Go to your customer account');?></a></li>
		<li><a href="<?php echo $this->linkToDefaultProductListing;?>"><?php echo KText::_('Continue shopping');?></a></li>
	</ul>

</div>
</div>
</div>