<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdminoptionassignment extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'adminoptionassignments';

	/**
	 * @var object $option Holds the option's data object
	 * @see ConfigboxModelAdminoptions::getRecord()
	 */
	public $option;

	/**
	 * @var KenedoProperty[] Properties of the Option Type
	 * @see ConfigboxModelAdminoptions::getProperties
	 */
	public $optionProperties;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function getPageTitle() {
		return KText::_('Option Assignment');
	}

	function prepareTemplateVars() {

		$id = KRequest::getInt('id');

		$model = KenedoModel::getModel('ConfigboxModelAdminoptionassignments');
		
		$this->assignRef('pageTitle', $this->getPageTitle());
		$this->assignRef('pageTasks', $model->getDetailsTasks());
		$this->assignRef('itemUsage', $model->getRecordUsage($id) );

		$xrefModel = KenedoModel::getModel('ConfigboxModelAdminxrefelementoptions');
		$optionModel = KenedoModel::getModel('ConfigboxModelAdminoptions');

		// Get the xref data
		if ($id) {
			$xref = $xrefModel->getRecord($id);
		}
		else {
			$xref = $xrefModel->initData();
		}

		if ($xref->option_id) {
			$option = $optionModel->getRecord($xref->option_id);
		}
		else {
			$option = $optionModel->initData();
		}

		$this->assignRef('record', $xref);
		$this->assignRef('option', $option);
		
		$this->assignRef('properties', $xrefModel->getProperties() );
		$this->assignRef('optionProperties', $optionModel->getProperties() );

		$this->addViewCssClasses();

	}
}