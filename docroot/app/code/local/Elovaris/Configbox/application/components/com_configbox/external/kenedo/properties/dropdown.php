<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyDropdown extends KenedoProperty {
		
	function renderListingField($item, $items) {
	
		$value = $item->{$this->propertyName};
		$outputValue = $this->propertyDefinition['items'][$value];

		if ($this->getPropertyDefinition('listinglink')) {

			$option = $this->getPropertyDefinition('component');
			$controller = $this->getPropertyDefinition('controller');

			$returnUrl = KLink::base64UrlEncode(KLink::getRoute( 'index.php?option='.hsc($option).'&controller='.hsc($controller), false ));
			$href = KLink::getRoute('index.php?option='.hsc($option).'&controller='.hsc($controller).'&task=edit&id='.intval($item->id).'&return='.$returnUrl);

			$linkText = (!empty($outputValue)) ? $outputValue : KText::_('No Name');
			?>
			<a class="listing-link" href="<?php echo $href;?>"><?php echo hsc($linkText);?></a>
		<?php
		}
		else {
			echo hsc($outputValue);
		}

	}

	public function getFilterInput(KenedoView $view, $filters) {

		if (!$this->getPropertyDefinition('search') && !$this->getPropertyDefinition('filter')) {
			return '';
		}

		$filterName = $this->getFilterName();
		$filterNameRequest = $this->getFilterNameRequest();
		$filterHtmlName = str_replace('.', '_', $filterName);

		$chosenValue = !empty($filters[$filterName]) ? $filters[$filterName] : NULL;

		$options = $this->getPossibleFilterValues($this->model);

		$html = KenedoHtml::getSelectField($filterNameRequest, $options, $chosenValue, '', false, 'listing-filter', $filterHtmlName);

		return $html;

	}

	protected function getPossibleFilterValues() {

		$db = KenedoPlatform::getDb();
		$query = "SELECT DISTINCT `".$this->propertyName."` AS `id`, `".$this->propertyName."` AS `title` FROM `".$this->model->getTableName()."`";
		$db->setQuery($query);
		$values = $db->loadObjectList('id');
		if ($values) {
			if (in_array($this->getType(), array('boolean', 'published', 'checkbox') ) ) {
				foreach ($values as $value) {
					$value->title = ($value->id) ? KText::_('CBYES') : KText::_('CBNO');
				}
			}
		}
		return $values;

	}
		
}