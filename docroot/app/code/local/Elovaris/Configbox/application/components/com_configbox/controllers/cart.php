<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerCart extends KenedoController {

	/**
	 * @return ConfigboxModelCart
	 */
	protected function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelCart');
	}

	/**
	 * @return ConfigboxViewCart
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewCart');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

	/**
	 * Typically requested from a configurator page with a configuration that needs to be finished.
	 * Task is used to set the underlying position of the config to finished making the position 'visible' in the cart.
	 * Redirects the visitor to the cart.
	 * @throws Exception
	 */
	function finishConfiguration() {

		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');

		if (!$positionModel->getId()) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Configuration not found. Your session probably timed out.'));
			return;
		}

		$position = $positionModel->getPosition();
		if (!$position) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Configuration not found. Your session probably timed out.'));
			return;
		}

		// Check for missing elements
		$missingElements = $positionModel->getMissingElements();

		if ($missingElements) {
			$this->reportMissingElementsAndRedirect($missingElements, $positionModel->getId());
			return;
		}

		// Set position's flag to finished
		$positionModel->editPosition(array('finished'=>1));

		// Fire the 'add to cart' event
		$cartModel = KenedoModel::getModel('ConfigboxModelCart');
		$cartDetails = $cartModel->getCartDetails();

		KenedoObserver::triggerEvent('onConfigBoxAddToCart', array(&$cartDetails));

		// The 'stay' GET/POST parameter makes the customer stay on the configurator page
		if (KRequest::getInt('stay')) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Product added to the cart.'),'notice');
			$this->copyCartPosition();
		}
		else {
			$url = KLink::getRoute($cartDetails->redirectURL, false);
			$this->setRedirect($url);
		}

	}

	protected function reportMissingElementsAndRedirect($missingElements, $cartPositionId) {

		// Prepare the text for the feedback
		$text = '<div>'.KText::_('Before finishing the configuration you have to make a choice for these required elements:').'</div>';
		$text .= '<ul>';
		foreach ($missingElements as $missingElement) {
			$text .= '<li>'.$missingElement->elementTitle.'</li>';
		}
		$text .= '</ul>';

		// Send the feedback
		KenedoPlatform::p()->sendSystemMessage($text);

		// Get the info for the first missing element
		$firstMissing = $missingElements[key($missingElements)];

		// Create the URL and redirect
		$url = KLink::getRoute('index.php?option=com_configbox&view=configuratorpage&task=editCartPosition&cart_position_id='.intval($cartPositionId).'&prod_id='.$firstMissing->prodId.'&page_id='.$firstMissing->pageId, false);
		$this->setRedirect($url);

	}

	/**
	 * Brings cart contents to checkout.
	 * On issues, it redirects to the cart page with feedback via system messages
	 * Otherwise redirects the visitor to the checkout page URL (as defined by the connector)
	 *
	 * @see ObserverOrders::onConfigBoxCheckout
	 * @throws Exception
	 */
	function checkoutCart() {

		// Get the cart details
		$cartModel = KenedoModel::getModel('ConfigboxModelCart');
		$cartId = KRequest::getInt('cart_id');

		// Check if the user owns the cart
		if ($cartModel->cartBelongsToUser($cartId) == false) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('This cart does not belong to your customer account.'));
			$this->setRedirect( KLink::getRoute('index.php?option=com_configbox&view=cart', false) );
			return;
		}

		// Set the id
		$cartModel->setId($cartId);

		// Get the cart details
		$cartDetails = $cartModel->getCartDetails();

		// See if checkout is permitted
		if (ConfigboxOrderHelper::isPermittedAction('checkoutOrder', $cartDetails) == false) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('You cannot checkout this cart anymore.'));
			$this->setRedirect( KLink::getRoute('index.php?option=com_configbox&view=cart', false) );
			return;
		}

		KenedoObserver::triggerEvent('onConfigBoxCheckout', array(&$cartDetails));

		$cartModel->resetCartDataCache();

		$this->setRedirect(KLink::getRoute($cartDetails->redirectURL, false, CONFIGBOX_SECURECHECKOUT));

		return;

	}

	function removeCartPosition() {

		// Get the position id from request
		$positionId = KRequest::getInt('cart_position_id');

		// Get the position model
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');

		// Check if the user owns this position
		if ($positionModel->userOwnsPosition($positionId) == false) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('You cannot remove this position.'));
			$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=cart', false));
			return;
		}

		// Try removing the position
		$success = $positionModel->removePosition();

		// Get the right feedback text
		$message = ($success) ? KText::_('Product removed from order') : KText::_('Could not remove product from order');

		KenedoPlatform::p()->sendSystemMessage($message);
		$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=cart', false));

	}

	function editCartPosition() {

		// Get the position id from request
		$positionId = KRequest::getInt('cart_position_id');

		// Get the position model
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');

		// Check if the user owns this position
		if ($positionModel->userOwnsPosition($positionId) == false) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Order not found. Your session probably timed out.'));
			$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=cart', false));
			return;
		}

		// Set the id, get position data
		$positionModel->setId($positionId);
		$positionData = $positionModel->getPosition();

		// Edit the position
		$positionModel->editPosition(array('finished' => 0));

		// The user can request a specific configurator page to get to, if not, go to the first one

		$productId = $positionData->prod_id;
		$pageId = KRequest::getInt('page_id', 0);

		if (!$pageId) {
			$productModel = KenedoModel::getModel('ConfigboxModelProduct');
			$product = $productModel->getProduct($positionData->prod_id);
			$pageId = $product->firstPageId;
		}

		$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=configuratorpage&prod_id=' . intval($productId) . '&page_id=' . intval($pageId), false));

	}

	function setCartPositionQuantity() {

		// Get the position id and quantity from request
		$positionId = KRequest::getInt('cart_position_id');
		$quantity = KRequest::getInt('quantity',0);

		// Keep the redirect url for nicer code (is the same for any outcome)
		$redirectUrl = KLink::getRoute('index.php?option=com_configbox&view=cart', false);

		// Get the position model
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');

		// Check if the user owns this position
		if ($positionModel->userOwnsPosition($positionId) == false) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Invalid position id'));
			$this->setRedirect($redirectUrl);
			return;
		}

		// Check if the position is valid
		if ($quantity < 1) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Invalid quantity'));
			$this->setRedirect($redirectUrl);
			return;
		}

		// Set the position id, get basic position data
		$positionModel->setId($positionId);
		$positionData = $positionModel->getPosition();

		// Get the cart details for permission check
		$cartModel = KenedoModel::getModel('ConfigboxModelCart');
		$cartModel->setId($positionData->cart_id);
		$cartDetails = $cartModel->getCartDetails();

		// Check if editing the cart is permitted
		if (ConfigboxOrderHelper::isPermittedAction('editOrder',$cartDetails) == false) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('You cannot edit this order anymore.'));
			$this->setRedirect($redirectUrl);
			return;
		}

		// Update the quantity
		$success = $positionModel->updateQuantity();

		// Send feedback and redirect
		if ($success == false) {
			KenedoPlatform::p()->sendSystemMessage($positionModel->getError());
			$this->setRedirect($redirectUrl);
		}
		else {
			$this->setRedirect($redirectUrl);
		}

	}

	function copyCartPosition() {

		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
		$positionId = $positionModel->copyPosition();
		$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&controller=cart&task=editCartPosition&cart_position_id='.intval($positionId),false));

	}

	function addProductToCart() {

		if (ConfigboxUserHelper::getUserId() == 0) {
			$userId = ConfigboxUserHelper::createNewUser();
			ConfigboxUserHelper::setUserId($userId);
		}

		$productId = KRequest::getInt('prod_id',0);
		if (!$productId) {
			return;
		}

		$cartModel = KenedoModel::getModel('ConfigboxModelCart');
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');

		// Create a cart if we got none yet
		if (!$cartModel->getId()) {
			$cartId = $cartModel->createCart();
			$cartModel->setId($cartId, true);
		}

		$cartId = $cartModel->getId();
		$positionId = $positionModel->getId();

		// Set up the new position
		$positionModel->createPosition($cartId, $productId);

		// Check for missing elements
		$missingElements = $positionModel->getMissingElements();
		if ($missingElements) {
			$this->reportMissingElementsAndRedirect($missingElements,$positionModel->getId());
			return;
		}

		$positionModel->editPosition(array('finished'=>1));
		$cartDetails = $cartModel->getCartDetails();
		KenedoObserver::triggerEvent('onConfigBoxAddToCart',array(&$cartDetails));

		if (KRequest::getKeyword('format','html') == 'json') {
			// Set the old position id
			if ($positionId) {
				$positionModel->setId($positionId);
			}
			KRequest::setVar('layout','component');
			$response = new stdClass();
			$response->success = 1;
			$response->message = KText::_('Product added to the cart.');
			echo json_encode($response);
		}
		else {
			if (KRequest::getInt('stay')) {
				// Set the old position id
				if ($positionId) {
					$positionModel->setId($positionId);
				}
				KenedoPlatform::p()->sendSystemMessage(KText::_('Product added to the cart.'),'notice');
				$this->setRedirect($_SERVER['HTTP_REFERER']);
			}
			else {
				$this->setRedirect( KLink::getRoute($cartDetails->redirectURL,false) );
			}
		}
		return;
	}

}