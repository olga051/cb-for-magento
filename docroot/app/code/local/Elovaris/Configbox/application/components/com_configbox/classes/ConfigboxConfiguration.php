<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxConfiguration {

	protected static $instances;

	protected $positionId;
	protected $productId;
	protected $selections = array();
	protected $simSelections = array();

	/**
	 *
	 * @param int $positionId
	 * @return ConfigboxConfiguration
	 */
	static function &getInstance($positionId = NULL) {

		if ($positionId === NULL) {
			$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
			$positionId = $positionModel->getId();
		}

		if (!isset(self::$instances[$positionId]) ) {
			self::$instances[$positionId] = new self($positionId);
		}

		return self::$instances[$positionId];

	}

	public function getPositionId() {
		return $this->positionId;
	}

	public function getProductId() {
		return $this->productId;
	}

	protected function __construct($positionId) {

		$this->positionId = intval($positionId);

		$selections = $this->loadSelectionsFromSession();

		if (!$selections) {
			$selections = $this->loadSelectionsFromDb();
		}
		$this->selections = $selections;

		// Get the product id cheap by looking into the selections
		if (count($this->selections)) {
			foreach ($this->selections as $elementId=>&$item) {
				$this->productId = $item['prod_id'];
				$item['element_id'] = $elementId;
				unset($item['prod_id']);
			}
		}
		// If we got no selections yet, get it from the cart position
		else {
			$db = KenedoPlatform::getDb();
			$query = "SELECT `prod_id` FROM `#__configbox_cart_positions` WHERE `id` = ".intval($positionId);
			$db->setQuery($query);
			$this->productId = $db->loadResult();
		}

	}

	public function setSelection($elementId, $xrefId, $textEntry = '', $isCalcElement = false) {

		if (!$elementId) {
			return false;
		}

		if (!$this->productId) {
			return false;
		}

		$textEntry = strval($textEntry);

		// Removal
		if ($xrefId == 0 && $textEntry === '' && $isCalcElement == false) {

			// Remember old text value for possible file upload removal
			$oldText = isset($this->selections[$elementId]) ? $this->selections[$elementId]['text'] : '';

			// Remove the entry from the orderItems member
			unset($this->selections[$elementId]);

			$this->storeSelectionsInSession();

			// Remove file if it is a file upload
			$element = ConfigboxElementHelper::getElement($elementId);
			if ($element->widget == 'fileupload') {

				$fileName = $oldText;
				// Be paranoid
				if (strstr($fileName, '..') != false) {
					KLog::log('A potential attack was detected by slipping in a fabricated filename into an elements value. File name is "'.$fileName.'"','warning');
				}
				else {
					$basePath = realpath(CONFIGBOX_DIR_CONFIGURATOR_FILEUPLOADS);
					$filePath = realpath($basePath . DS . $oldText);
					// Be even more paranoid
					if (dirname($filePath) != $basePath) {
						KLog::log('A potential attack was detected by slipping in a fabricated filename into an elements value. File name is "'.$fileName.'"','warning');
					}
					elseif (is_file($filePath) && is_writable($filePath)) {
						unlink($filePath);
						KLog::log($filePath.' removed.','info');
					}
				}
			}

			return true;
		}
		// Insert/update
		else {

			$product = ConfigboxCacheHelper::getProductData($this->productId);
			if ($product->quantity_element_id == $elementId || $product->alt_quantity_element_id == $elementId) {
				$qElement = ConfigboxElementHelper::getElement($elementId);
				if (count($qElement->options)) {
					$qValue = $qElement->options[$xrefId]->assignment_custom_1;
					$textEntry = '';
				}
				else {
					$xrefId = 0;
					$qValue = $textEntry;
				}

				$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
				KLog::log('Setting quantity "'.$qValue.'"');
				$positionModel->editPosition(array('quantity'=>$qValue));
			}

			// Do the object
			$this->selections[$elementId]['element_option_xref_id'] = $xrefId;
			$this->selections[$elementId]['text'] = $textEntry;

			$this->storeSelectionsInSession();

			return true;

		}

	}

	/**
	 * Makes selections permanent by storing them in the DB
	 */
	public function storeSelectionsInDb() {

		$db = KenedoPlatform::getDb();

		// Remove any existing selections for that position id
		$query = "DELETE FROM `#__configbox_cart_position_configurations` WHERE `cart_position_id` = ".intval($this->getPositionId());
		$db->setQuery($query);
		$success = $db->query();

		if (!$success) {
			throw new Exception('An error occured during storing selections.');
		}

		$selections = $this->getSelections(false);
		foreach ($selections as $elementId=>$selection) {
			// Collect all selection data
			$record = new stdClass();
			$record->cart_position_id = $this->getPositionId();
			$record->prod_id = $this->getProductId();
			$record->element_id = $elementId;
			$record->element_option_xref_id = $selection['element_option_xref_id'];
			$record->text = $selection['text'];

			// Insert a row
			$success = $db->insertObject('#__configbox_cart_position_configurations', $record);

			if (!$success) {
				throw new Exception('An error occured during storing selections.');
			}

		}

	}

	/**
	 * Stores all selections in session
	 */
	public function storeSelectionsInSession() {
		$selections = $this->getSelections(false);
		foreach ($selections as &$selection) {
			$selection['prod_id'] = $this->getProductId();
		}
		KSession::set('selections_'.$this->getPositionId(), json_encode($selections));
	}

	public function deleteSelectionsFromSession() {
		KSession::delete('selections_'.$this->getPositionId());
	}

	public function loadSelectionsFromDb() {
		$db = KenedoPlatform::getDb();
		$query = "	SELECT o.`element_id`, o.`element_option_xref_id`, o.`text`, o.`prod_id`
					FROM `#__configbox_cart_position_configurations` AS o
					LEFT JOIN `#__configbox_elements` AS e ON e.id = o.element_id
					WHERE o.`cart_position_id` = ".intval($this->getPositionId())." AND e.published = 1";
		$db->setQuery($query);
		$data = $db->loadAssocList('element_id');
		return $data;
	}

	public function loadSelectionsFromSession() {
		$data = KSession::get('selections_'.$this->getPositionId(), '');
		return json_decode($data, true);
	}

	/**
	 * Tells if selection for $elementId is currently a sim selection
	 * @param int $elementId
	 * @return bool
	 */
	public function isSimSelection($elementId) {
		return (!empty($this->simSelections[$elementId]));
	}

	/**
	 * Tells if selection for $elementid currently has an actual selection (no matter if a simSelection covers it)
	 * @param int $elementId
	 * @return bool
	 */
	public function hasRealSelection($elementId) {
		return (!empty($this->selections[$elementId]));
	}

	/**
	 * @param mixed[] $simSelections See setSimSelection
	 * @see ConfigboxConfiguration::setSimSelection
	 */
	public function addSimSelections($simSelections) {
		foreach ($simSelections as $elementId=>$simSelection) {
			$this->setSimSelection($elementId, $simSelection['element_option_xref_id'], $simSelection['text'], $simSelection['is_calc_element']);
		}

	}

	/**
	 * Let's you set a 'faked' selection (used for simulating a configuration during rule processing).
	 * All functions returning configuration data will have simulated selections merged over the real ones.
	 *
	 * @see ConfigboxConfiguration::unsetSimSelections(), ConfigboxConfiguration::unsetSimSelection()
	 * @throws Exception if $elementId isn't right (int higher than 0)
	 * @param int $elementId
	 * @param int $xrefId
	 * @param string $textEntry
	 * @param bool|false $isCalcElement
	 */
	public function setSimSelection( $elementId, $xrefId, $textEntry = '', $isCalcElement = false ) {

		if ($elementId == 0) {
			throw new Exception('Tried to set a simSelection with no element ID. Value was '.var_export($elementId, true));
		}

		$this->simSelections[$elementId]['is_calc_element'] = $isCalcElement;
		$this->simSelections[$elementId]['element_id'] = $elementId;

		if ($xrefId == 0 && $textEntry == '' && $isCalcElement == false) {
			$this->simSelections[$elementId]['element_option_xref_id'] = NULL;
			$this->simSelections[$elementId]['text'] = NULL;
		}
		else {
			$this->simSelections[$elementId]['element_option_xref_id'] = $xrefId;
			$this->simSelections[$elementId]['text'] = $textEntry;
		}
	}

	public function getSimSelections() {
		return $this->simSelections;
	}

	public function getSimSelection($elementId) {
		return (!empty($this->simSelections[$elementId])) ? $this->simSelections[$elementId] : NULL;
	}


	/**
	 * Remove any simulated selections
	 */
	public function unsetSimSelections() {
		$this->simSelections = array();
	}

	/**
	 * Removed any simulated selection for the given element ID
	 * @param $elementId
	 */
	public function unsetSimSelection($elementId) {
		unset($this->simSelections[$elementId]);
	}

	/**
	 * Use this to get the current selections in a cart position
	 *
	 * - Got the simulated selections merged in
	 * - Expects the instance to be set to the desired cart position id (see getInstance).
	 *
	 * @see ConfigboxConfiguration::getInstance for setting the right cart position ID
	 * @param bool $includeSimSelections If simulated selections should be merged in
	 * @return array[] Array of selections, key is the element id, value is an array (keys text, element_option_xref_id)
	 */
	public function getSelections($includeSimSelections = true) {

		$selections = $this->selections;

		if ($includeSimSelections == true) {
			foreach ($this->simSelections as $elementId=>$simItem) {
				if ($simItem['text'] === NULL && $simItem['element_option_xref_id'] === NULL) {
					// Mind that this unsets it in the copy, not the object member
					unset($selections[$elementId]);
				}
				else {
					$selections[$elementId]['element_option_xref_id'] = $simItem['element_option_xref_id'];
					$selections[$elementId]['text'] = $simItem['text'];
				}
			}
		}

		return $selections;

	}

	/**
	 * @param $elementId
	 * @return null|int|string Text entry or selected option assignment ID
	 */
	public function getSelection($elementId) {

		$value = NULL;

		// Try simulated items first
		if (isset($this->simSelections[$elementId])) {

			if ($this->simSelections[$elementId]['text'] !== '') {
				$value = $this->simSelections[$elementId]['text'];
			}
			elseif ($this->simSelections[$elementId]['element_option_xref_id'] != 0) {
				$value = $this->simSelections[$elementId]['element_option_xref_id'];
			}
			else {
				$value = NULL;
			}

		}
		// Then try actual configuration items
		elseif (isset($this->selections[$elementId])) {

			if ($this->selections[$elementId]['text'] !== '') {
				$value = $this->selections[$elementId]['text'];
			}
			elseif ($this->selections[$elementId]['element_option_xref_id'] != 0) {
				$value = $this->selections[$elementId]['element_option_xref_id'];
			}
			else {
				$value = NULL;
			}

		}
		else {
			$value = NULL;
		}

		return $value;

	}

	/**
	 * @param $elementId
	 * @return null|int Selected option assignment ID for that element
	 */
	public function getElementXrefId($elementId) {

		if (isset($this->simSelections[$elementId])) {
			return $this->simSelections[$elementId]['element_option_xref_id'];
		}
		elseif (isset($this->selections[$elementId])) {
			return $this->selections[$elementId]['element_option_xref_id'];
		}
		else {
			return NULL;
		}

	}

	/**
	 * @param $elementId
	 * @return null|string The text entry for that element
	 */
	public function getElementTextEntry($elementId) {

		if (isset($this->simSelections[$elementId])) {
			return $this->simSelections[$elementId]['text'];
		}
		elseif (isset($this->selections[$elementId])) {
			return $this->selections[$elementId]['text'];
		}
		else {
			return NULL;
		}

	}

	/**
	 * Use this to get the element ids of current selections in a configured product in a cart position (including simulated selections)
	 * Expects the instance to be set to the desired cart position id (see getInstance).
	 * @param bool $includeSimSelections If simulated selections should be merged in
	 * @return int[] Array of element ids
	 */
	public function getSelectionElementIds($includeSimSelections = true) {

		$selections = $this->getSelections($includeSimSelections);
		if (is_array($selections)) {
			return array_keys($selections);
		}
		else {
			return array();
		}

	}

}