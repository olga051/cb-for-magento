<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewConfiguratorpage extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'configuratorpage';

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	/**
	 * @var bool $showProductDetailPanes Tells if product detail panes shall be shown (depends on settings in product form)
	 */
	public $showProductDetailPanes;

	/**
	 * @var string $productDetailPanes Ready-to-use HTML with product detail panes
	 * @see ConfigboxViewProductdetailpanes
	 */
	public $productDetailPanes;

	/**
	 * @var object $product Typical product data (as in product listing and product details view)
	 */
	public $product;

	/**
	 * @var object $configuratorPage Object holding all configurator page data (augmented in display method)
	 * @see ConfigboxModelConfiguratorpage::getPage()
	 */
	public $configuratorPage;

	/**
	 * @var ConfigboxElement[] $elements Helper property set during looping through the page elements (makes life easier in element
	 * templates).
	 */
	public $elements;

	/**
	 * @var ConfigboxElement $element Helper property set during looping through the page elements (makes life easier in element
	 * templates).
	 */
	public $element;

	/**
	 * @var boolean $showPageHeading Setting from platform (in case of Joomla it's the menu item's setting)
	 */
	public $showPageHeading;

	/**
	 * @var string $pageCssClasses String holding all CSS classes to be used in one of the wrapping divs (element
	 * setting CSS classes comes into play here) - Set in ConfigboxViewConfiguratorpage::display
	 * @see ConfigboxViewConfiguratorpage::prepareTemplateVars
	 */
	public $pageCssClasses;

	/**
	 * @var object[] Array of objects with currently missing elements (elementTitle, elementId, pageId and prodId)
	 * @see ConfigboxModelCartposition::getMissingElements
	 */
	public $missingSelections;

	/**
	 * @var string $finishLink URL to call to finish the configuration
	 */
	public $finishLink;
	
	function display() {

		$this->prepareTemplateVars();

		if (empty($this->configuratorPage)) {
			header('', true, 410);
			$this->renderView('pagenotfound');
		}
		else {

			$layoutName = ($this->product->layoutname) ? $this->product->layoutname : 'default';

			// Figure out which element template to use
			$templates['templateOverride'] 	= KenedoPlatform::p()->getTemplateOverridePath('com_configbox', 'configuratorpage', $layoutName);
			$templates['customTemplate'] 	= CONFIGBOX_DIR_CUSTOMIZATION.DS.'templates'.DS.'configuratorpage'.DS. $layoutName .'.php';
			$templates['defaultTemplate'] 	= KPATH_DIR_CB.DS.'views'.DS.'configuratorpage'.DS.'tmpl'.DS.'default.php';

			foreach ($templates as $template) {
				if (file_exists($template)) {
					require($template);
					break;
				}
			}

		}

	}

	function prepareTemplateVars() {

		$pageId = KRequest::getInt('page_id',0);
		$productId = KRequest::getInt('prod_id',0);

		// Fix labels (in case of language change with JoomFish or when labels have changed)
		if ((KRequest::getInt('prod_id',0) == 0 && KRequest::getString('prodlabel','')) != '' || (KRequest::getInt('page_id',0) == 0 && KRequest::getString('catlabel','') != '')) {
			KLog::log('No cat or prod id, but label found. Trying to fix label (prodlabel: "'.KRequest::getString('prodlabel','').'", catlabel: "'.KRequest::getString('catlabel','').'", prod_id:"'.KRequest::getInt('prod_id',0).'", page_id:"'.KRequest::getInt('page_id',0).'")','debug');
			ConfigboxLabelsHelper::fixLabels();
			$pageId = KRequest::getInt('page_id',0);
			$productId = KRequest::getInt('prod_id',0);
		}
		
		if (ConfigboxUserHelper::getUserId() == 0) {
			$userId = ConfigboxUserHelper::createNewUser();
			ConfigboxUserHelper::setUserId($userId);
		}

		// Get the models
		$productModel = KenedoModel::getModel('ConfigboxModelProduct');
		$pageModel = KenedoModel::getModel('ConfigboxModelConfiguratorpage');

		// Get the product
		$product = $productModel->getProduct($productId);

		// If the page does not exists or isn't published, then stop
		if (!$product || $product->published != '1') {
			return;
		}

		// Get the page
		$page = $pageModel->getPage($pageId);

		// If the page does not exists or isn't published, then stop
		if (!$page || $page->published != '1') {
			return;
		}

		// Make sure we have a cart id
		$cartModel = KenedoModel::getModel('ConfigboxModelCart');
		if (!$cartModel->getId()) {
			KLog::log('No cart is set, creating new one.');
			$cartId = $cartModel->createCart();
			$cartModel->setId($cartId, true);
		}
		$cart = $cartModel->getCartData();

		// Create a new one if customer cannot add to this cart anymore (checked out or similar)
		if (ConfigboxOrderHelper::isPermittedAction('addToOrder', $cart) == false) {
			KLog::log('Adding to this cart not allowed, creating new one.');
			$cartId = $cartModel->createCart();
			$cartModel->setId($cartId, true);
		}

		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');

		// If there is no position id, create a new position
		if (!$positionModel->getId()) {
			KLog::log('No position is set, creating new one.');
			$positionModel->createPosition($cartModel->getId(), $productId);
		}
		$position = $positionModel->getPosition();

		// If the position concerns another product, do a new one
		if ($position->prod_id != $productId or $position->finished) {
			if ($position->finished) {
				KLog::log('Currently active position is finished, creating new one.');
			}
			else {
				KLog::log('Currently active position has a product diffent from the requested product. Creating new position.');
			}
			$positionModel->createPosition($cartModel->getId(), $productId);
		}

		$elements = $pageModel->getElements($pageId);

		// Process element and option descriptions through content plugins
		foreach ($elements as $element) {

			// Load element class language files
			if ($element->class) {
				$tag = KenedoPlatform::p()->getLanguageTag();
				$file = CONFIGBOX_DIR_CUSTOMIZATION.DS.'element_classes'.DS.$element->class.DS.'language'.DS.$tag.DS.'element.ini';
				if (is_file($file)) {
					KText::load($file);
				}
			}

			// Set the CSS classes indicating compliance with rules
			if ($element->applies()) {
				$element->addCssClass('element-applying');
				$element->disabled = 'disabled="disabled"';
			}
			else {
				$element->addCssClass('element-not-applying');
				$element->disabled = '';
			}

			// Set pre-loading and delayed loading classes and attributes
			if ($element->el_image) {

				if ($element->applies() == false && $element->hidenonapplying) {
					$element->elementImageCssClasses = 'element-image preload-image';
					$element->elementImagePreloadAttributes = ' data-src="'.CONFIGBOX_URL_ELEMENT_IMAGES.'/'.$element->el_image.'"';
					$element->elementImageSrc = KPATH_URL_ASSETS.'/assets/images/blank.gif';
				}
				else {
					$element->elementImageCssClasses = 'element-image';
					$element->elementImagePreloadAttributes = '';
					$element->elementImageSrc = ($element->el_image) ? CONFIGBOX_URL_ELEMENT_IMAGES.'/'.$element->el_image : '';
				}

			}

			if (!empty($element->options)) {

				// Prepare the currently selected xref id
				$selectedXrefId = ($element->options) ? intval($element->getRawValue()) : NULL;

				foreach ($element->options as $option) {

					// Set the CSS class to the selected option
					if ($selectedXrefId == $option->id) {
						$option->addCssClass('selected');
						$option->isSelected = true;
					}
					else {
						$option->isSelected = false;
					}

					// Set the CSS class indicating if the option is compliant
					if ($option->applies()) {
						$option->addCssClass('xref-applying');
					}
					else {
						$option->addCssClass('xref-not-applying');
					}

					// CSS class indicates if an uncomplient option shall be hidden
					if ($option->hidenonapplying == 1) {
						$option->addCssClass('hide-not-applying');
					}

					// Disable option if element or option does not apply
					if (!$option->applies() || !$element->applies()) {
						$option->disabled = 'disabled = "disabled"';
					}

					// Make the picker image src a complete URL
					if ($option->option_picker_image) {
						$option->pickerImageSrc = CONFIGBOX_URL_OPTION_PICKER_IMAGES.'/'.$option->option_picker_image;
					}
					else {
						$option->pickerImageSrc = KPATH_URL_ASSETS.'/images/blank.gif';
					}

					// Set preloading/delayed loading CSS classes and attributes
					if ($option->pickerImageSrc && (!$option->applies || !$element->applies()) && ($element->hidenonapplying || $option->hidenonapplying)) {
						$option->pickerPreloadAttributes = ' data-src="'.$option->pickerImageSrc.'"';
						$option->pickerImageSrc = '';
						$option->pickerPreloadCssClasses = 'configbox-image-button-image preload-image';
					}
					else {
						$option->pickerPreloadAttributes = '';
						$option->pickerPreloadCssClasses = 'configbox-image-button-image';
					}

					// Make the option image src a complete URL
					if ($option->option_image) {
						$option->optionImageSrc = CONFIGBOX_URL_OPTION_IMAGES.'/'.$option->option_image;
						$option->optionImagePopupContent = '<img src="'.$option->optionImageSrc.'" alt="'.hsc($option->title).'" />';
						$dim = KenedoFileHelper::getImageDimensions(CONFIGBOX_DIR_OPTION_IMAGES.DS.$option->option_image);
						$option->optionImagePopupWidth = $dim['width'] + 20;
					}
					else {
						$option->optionImageSrc = '';
						$option->optionImagePopupContent = '';
					}

				}
			}

			// Figure out which element template to use (element class templates are done in the page template later)
			$templates['customTemplate'] = CONFIGBOX_DIR_CUSTOMIZATION.DS.'templates'.DS.'element'.DS. $element->layoutname.DS.$element->type.'.php';
			$templates['defaultTemplate'] = KPATH_DIR_CB.DS.'views'.DS.'element'.DS.'tmpl'.DS.$element->type.'.php';

			foreach ($templates as $template) {
				if (file_exists($template)) {
					$element->templateFile = $template;
					break;
				}
			}

		}

		// Overwrite min and max values with possibly calculated ones
		foreach ($elements as $element) {
			$element->minval = $element->getMinimumValue();
			$element->maxval = $element->getMaximumValue();
		}

		// Process element and option descriptions with content plugins, also strip out the dreaded <p><br mce_bogus="1"></p> output
		foreach ($elements as $element) {

			if ($element->description) {
				$element->description = KenedoPlatform::p()->processContentModifiers($element->description);
				$element->description = trim($element->description);
			}

			foreach ($element->options as $eloption) {

				if ($eloption->description) {
					$eloption->description = KenedoPlatform::p()->processContentModifiers($eloption->description);
					$eloption->description = trim($eloption->description);
				}

				if ( $eloption->enable_reviews == 1 or ( $eloption->enable_reviews == 2 and CONFIGBOX_ENABLE_REVIEWS_OPTIONS == 1) ) {
					$eloption->showReviews = true;
				}
				else {
					$eloption->showReviews = false;
				}

			}
		}

		// Put info on missing selections to the template
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
		$this->missingSelections = $positionModel->getMissingElements();

		// Get all CSS classes for the wrapping div together for nicer usage in template
		$pageCssClasses = array();
		$pageCssClasses[] = 'view-configurator-page-'.$page->id;
		if (trim($page->css_classes)) {
			$pageCssClasses = array_merge($pageCssClasses, explode(' ', trim($page->css_classes)));
		}
		$this->assign('pageCssClasses', implode(' ', $pageCssClasses));

		// Set CSS classes for buttons, indicated what shall be hidden
		if (count($this->missingSelections) && $page->lock_on_required) {
			$this->assign('nextButtonsHidden',true);
			$this->assign('nextButtonClasses', "configbox-disabled onAjaxCompleteOnly");
		}
		else {
			$this->assign('nextButtonsHidden',false);
			$this->assign('nextButtonClasses', "onAjaxCompleteOnly");
		}

		$this->assign('prevButtonClasses', "onAjaxCompleteOnly");
		$this->assign('saveBundleButtonClasses', "onAjaxCompleteOnly");

		// Get the info about the prev and next page button
		$sequence = $pageModel->getNextAndPrevPageId($productId, $pageId);

		// No next page means we show the finish button
		if (!$sequence->nextPageId) {
			$this->assign('showFinish', true);
		}
		// If we don't deal with the last page, see page settings (or global settings) for showing finish button
		elseif($page->finish_last_page_only == 0 or ($page->finish_last_page_only == 2 && CONFIGBOX_FINISH_LAST_PAGE_ONLY == 0)) {
			$this->assign('showFinish', true);
		}
		else {
			$this->assign('showFinish', false);
		}

		// Indicate if the recommendation should show
		$this->assign('showRecommendations', CONFIGBOX_ALLOW_RECOMMENDATIONS);

		// Override recommendation and show finish for Magento
		if (KenedoPlatform::getName() == 'magento') {
			$this->assign('showFinish', false);
			$this->assign('showRecommendations', false);
		}

		// Prepare titles and hrefs for the page navigation buttons
		$nextLink = NULL;
		$nextTitle = NULL;

		$prevLink = NULL;
		$prevTitle = NULL;

		if ($sequence->nextPageId) {
			$nextLink = KLink::getRoute('index.php?option=com_configbox&view=configuratorpage&prod_id='.$product->id.'&page_id='.$sequence->nextPageId);
			$nextTitle = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 3, $sequence->nextPageId);
		}

		if ($sequence->prevPageId) {
			$prevLink = KLink::getRoute('index.php?option=com_configbox&view=configuratorpage&prod_id='.$product->id.'&page_id='.$sequence->prevPageId);
			$prevTitle = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 3, $sequence->prevPageId);
		}

		// Change next and prev links for Magento
		if (KenedoPlatform::getName() == 'magento') {
			if ($sequence->nextPageId) {
				$currentUrl = Mage::helper('core/url')->getCurrentUrl();
				$currentUrl = str_replace('page_id='.$page->id, '', $currentUrl);
				$currentUrl = rtrim($currentUrl,'&?');
				$nextLink = $currentUrl . ( (strstr($currentUrl, '?')) ? '&':'?') . 'page_id='.$sequence->nextPageId;
			}
			if ($sequence->prevPageId) {
				$currentUrl = Mage::helper('core/url')->getCurrentUrl();
				$currentUrl = str_replace('page_id='.$page->id, '', $currentUrl);
				$currentUrl = rtrim($currentUrl,'&?');
				$prevLink = $currentUrl . ( (strstr($currentUrl, '?')) ? '&':'?') . 'page_id='.$sequence->prevPageId;
			}
		}

		$this->assignRef('prevLink', $prevLink);
		$this->assignRef('prevTitle', $prevTitle);
		$this->assignRef('nextLink', $nextLink);
		$this->assignRef('nextTitle', $nextTitle);

		// Set page title
		if ($nextLink == NULL && $prevLink == NULL) {
			$documentTitle = KText::sprintf('Configure %s', $product->title);
		}
		else {
			$documentTitle = $product->title.' - '.$page->title;
		}
		KenedoPlatform::p()->setDocumentTitle($documentTitle);

		// Add JS metadata
		$js  = "cbj(document).ready(function(){\n";
		$js .= "if (typeof(com_configbox) == 'undefined') {\ncom_configbox = {};\n}\n";
		$js .= "com_configbox.elements = ".json_encode($elements).";\n";
		$js .= "com_configbox.product = ".json_encode($product).";\n";
		$js .= "com_configbox.view = 'configuratorpage'".";\n";
		$js .= "com_configbox.prod_id = ".intval($productId).";\n";
		$js .= "com_configbox.page_id = ".intval($page->id).";\n";
		$js .= "com_configbox.lockPageOnRequired = ".intval($page->lock_on_required).";\n";
		$js .= "com_configbox.cartPositionId = ".intval($positionModel->getId()).";\n";
		$js .= "com_configbox.pricing = ".json_encode($positionModel->getPricing()).";\n";
		$js .= "com_configbox.cartPositionDetails = ".json_encode( $positionModel->getPositionDetails() ).";\n";
		$js .= "});\n";
		KenedoPlatform::p()->addScriptDeclaration($js, true, true);

		// Process content plugins for description
		if (!empty($page->description)) {
			$page->description = KenedoPlatform::p()->processContentModifiers($page->description);
			$page->description = trim($page->description);
		}

		// Set the template name
		$templateName = ($page->layoutname) ? $page->layoutname : 'default';
		$this->assignRef('template', $templateName);

		// Set finish and save bundle links
		$this->assignRef('finishLink', KLink::getRoute('index.php?option=com_configbox&controller=cart&task=finishConfiguration&cart_position_id='.intval($positionModel->getId()) ));
		$this->assignRef('saveBundleLink', KLink::getRoute('index.php?option=com_configbox&controller=bundle&task=saveBundle&cart_position_id='.intval($positionModel->getId()) ));

		// These are menu item parameters from Joomla
		$params = KenedoPlatform::p()->getAppParameters();
		$this->assignRef('params', $params);
		$this->assign('showPageHeading', ($params->get('show_page_heading', 1) && $params->get('page_title','') != ''));

		// Deal with product detail panes
		if (count($product->productDetailPanes) && $product->product_detail_panes_in_configurator_steps) {
			$view = KenedoView::getView('ConfigboxViewProductdetailpanes');
			$view->assignRef('product',$product);
			$view->assignRef('parentView','configuratorPage');
			$productDetailsHtml = $view->getViewOutput($product->product_detail_panes_method);

			// Run them through content modifiers
			$productDetailsHtml = KenedoPlatform::p()->processContentModifiers($productDetailsHtml);
			$productDetailsHtml = trim($productDetailsHtml);
			
			$this->assignRef('productDetailPanes',$productDetailsHtml);
			$this->assign('showProductDetailPanes', true);
		}
		else {
			$this->assign('showProductDetailPanes', false);
		}

		$this->assignRef('product', $product);
		$this->assignRef('configuratorPage', $page);
		$this->assignRef('elements', $elements);

	}

}
