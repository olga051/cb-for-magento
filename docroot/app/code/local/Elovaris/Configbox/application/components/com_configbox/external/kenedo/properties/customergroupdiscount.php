<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyCustomergroupdiscount extends KenedoProperty {
	
	function getDataFromRequest( &$data ) {
		
		$fieldName = $this->propertyName.'_start';
		
		if (KRequest::getVar($fieldName,NULL) === NULL) {
			$data->$fieldName = NULL;
		}
		else {
			$data->$fieldName = KRequest::getString($fieldName,'');
		}

		$fieldName = $this->propertyName.'_factor';
		
		if (KRequest::getVar($fieldName,NULL) === NULL) {
			$data->$fieldName = NULL;
		}
		else {
			$data->$fieldName = KRequest::getString($fieldName,'');
		}
		
	}
	
	function store(&$data) {
		
		$db = KenedoPlatform::getDb();
		
		$fieldNameStart = $this->propertyName.'_start';
		$fieldNameFactor = $this->propertyName.'_factor';
		
		$value_factor = (float)str_replace(',', '.', $data->$fieldNameFactor);
		$value_start = $value_factor != 0 ? (float)str_replace(',', '.', $data->$fieldNameStart) : 0;
		
		$query = "UPDATE `#__cbcheckout_user_groups` SET `discount_start_".(int)$this->propertyDefinition['discountlevel']."` = ".$value_start.", `discount_factor_".(int)$this->propertyDefinition['discountlevel']."` = ".$value_factor." WHERE `id` = ".(int)$data->id;
		$db->setQuery($query);
		$db->query();

		unset($data->$fieldNameStart);
		unset($data->$fieldNameFactor);
		
		return true;
	}
	
}