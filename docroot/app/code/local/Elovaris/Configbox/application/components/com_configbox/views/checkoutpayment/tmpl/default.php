<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCheckoutpayment */
?>
<?php if (count($this->options) > 1) { ?>
<div id="subview-payment">
	<h2 class="step-title"><?php echo KText::_('Payment options');?></h2>
	<ul class="list-options">
		<?php foreach ($this->options as $option) { ?>
			<li>
				<input class="option-control" id="payment-option-<?php echo (int)$option->id;?>" type="radio" name="payment_id" value="<?php echo (int)$option->id;?>" <?php echo ($option->id == $this->selected) ? 'checked="checked"':'';?> />
				<label class="option-label" for="payment-option-<?php echo $option->id;?>">
					<span class="option-title"><?php echo hsc($option->title);?></span>
					<?php if ($option->priceNet != 0) { ?>
					<span class="option-price"><?php echo cbprice( ($this->mode == 'b2b') ? $option->priceNet : $option->priceGross, true, true); ?></span>
					<?php } ?>
					
					<?php 
					if ($option->description) {
						echo KenedoHtml::getTooltip('<span class="icon-option-description"></span>', $option->description, NULL, 400);
					}
					?>
				</label>
				<span class="processing-symbol">&nbsp;</span>
			</li>
		<?php } ?>
	</ul>
</div>
<?php } ?>