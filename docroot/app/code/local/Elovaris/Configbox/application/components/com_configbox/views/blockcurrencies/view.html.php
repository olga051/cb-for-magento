<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewBlockcurrencies extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var KStorage $params
	 */
	public $params;

	/**
	 * @var object[]
	 * @see ConfigboxCurrencyHelper::getCurrencies
	 */
	public $currencies;

	/**
	 * @var object $baseCurrency The base currency data (Base currency is the backend's currency. It is set in the currency record).
	 */
	public $baseCurrency;

	/**
	 * @var string $dropDown HTML for the whole <select>
	 */
	public $dropdown;

	/**
	 * @var bool $showConversionTable Depends on setting show_conversion_table in CB settings
	 */
	public $showConversionTable;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars($tpl = null) {
		
		if (empty($this->params)) {
			$params = new KStorage();
			$this->assignRef('params', $params);
		}

		$currencies = ConfigboxCurrencyHelper::getCurrencies();

		$baseCurrency = reset($currencies);

		foreach ($currencies as $currency) {
			if ($currency->base) {
				$baseCurrency = $currency;
				break;
			}
		}

		foreach ($currencies as $currency) {
			$currency->exchangeRate = round(floatval($currency->multiplicator) / floatval($baseCurrency->multiplicator), 4);
		}

		$this->assign('baseCurrency', $baseCurrency);
		$this->assign('currencies', ConfigboxCurrencyHelper::getCurrencies());
		$this->assign('dropdown', ConfigboxCurrencyHelper::getCurrencyDropdown());
		$this->assign('showConversionTable', (bool) CONFIGBOX_SHOW_CONVERSION_TABLE);

	}

}
