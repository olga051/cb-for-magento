<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyOrdering extends KenedoProperty {
	
	function renderListingField($item, $items) {
		?>
		<span class="sort-handle fa fa-bars"></span>
		<input type="text" class="ordering-text-field" name="ordering-item[<?php echo (int)$item->id;?>]" value="<?php echo (int)$item->{$this->getName()};?>" />
		<?php
	}
	
	function usesWrapper() {
		return false;
	}
	
	function getBodyAdmin() {
		?>
		<div><input type="hidden" id="ordering" name="ordering" value="<?php echo (int)$this->data->ordering;?>" /></div>
		<?php
	}

	function getDataFromRequest(&$data) {

		parent::getDataFromRequest($data);

		// In case of an insert, we bump up the ordering number
		if ($data->id == 0) {

			$db = KenedoPlatform::getDb();

			$tableName = $this->getPropertyDefinition('tableName');
			$groupKey = $this->getPropertyDefinition('group');
			$where = (!$groupKey) ? '' : "WHERE `".$groupKey."` = '".$db->getEscaped($data->{$groupKey})."'";

			$query = "SELECT `".$this->propertyName."` FROM `".$tableName."` ".$where." ORDER BY `".$this->propertyName."` DESC LIMIT 1";
			$db->setQuery($query);

			$data->{$this->propertyName} = intval($db->loadResult()) + 10;

		}

	}

}