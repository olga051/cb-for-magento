<?php
class ConfigboxPrices {
	
	protected static $cache = NULL;
	protected static $taxRateCache = NULL;
	protected static $calculationCache = NULL;
	
	protected static $customFunctionsLoaded = false;
	protected static $taxClasses = NULL;

	/**
	 * Memoizes the return value for ConfigboxPrices::showNetPrices
	 *
	 * @var boolean|NULL
	 */
	protected static $showNetPrices = array();

	/**
	 * Memoizes the return value for ConfigboxPrices::pricesEnteredNet
	 *
	 * @var boolean|NULL
	 */
	protected static $pricesEnteredNet = NULL;

	/**
	 * Tells if prices in the system are entered as net prices
	 *
	 * @return bool
	 */
	public static function pricesEnteredNet() {

		if (self::$pricesEnteredNet === NULL) {

			if (KenedoPlatform::getName() == 'magento') {
				self::$pricesEnteredNet = (Mage::getStoreConfig('tax/calculation/price_includes_tax')) ? false : true;
			}
			else {
				self::$pricesEnteredNet = true;
			}

		}

		return self::$pricesEnteredNet;

	}

	/**
	 * Tells if prices shall be shown net (wherever the system does not show net/tax/gross)
	 *
	 * @param int|null $userId User ID (or NULL to have it auto-determined)
	 * @return bool
	 */
	public static function showNetPrices($userId = NULL) {

		if (KenedoPlatform::getName() == 'magento') {
			$taxDisplayType = Mage::getStoreConfig('tax/display/type');
			$showNet = ($taxDisplayType == 1);
			return $showNet;
		}

		if ($userId === NULL) {
			$userId = ConfigboxUserHelper::getUserId();
		}

		if (!isset(self::$showNetPrices[$userId])) {
			$groupData = KenedoObserver::triggerEvent('onConfigboxGetGroupData',array($userId), true);
			self::$showNetPrices[$userId] = (boolean)$groupData->b2b_mode;
		}

		return self::$showNetPrices[$userId];
	}

	/**
	 * @param int $taxClassId See Backend -> Settings -> Tax Classes
	 *
	 * @return float $taxRate
	 */
	public static function getTaxRate($taxClassId) {

		if (KenedoPlatform::getName() == 'magento') {
			return $_SESSION['cbtaxrate'];
		}

		if (!isset(self::$cache['taxRates'])) {
			self::populateTaxRateCache();
		}
		
		return self::$cache['taxRates'][$taxClassId];
	}

	/**
	 * @param float   $price            Price of the item as entered in the backend (may be gross or net, depends on setting)
	 * @param boolean $getNet           If the price should come out net or gross
	 * @param int     $itemId           ID of either the product or element to look up the tax class ID
	 * @param string  $taxClassCacheKey See ConfigboxPrices::populateCache
	 *
	 * @return float
	 * @throws Exception
	 */
	protected static function getNormalizedPrice($price, $getNet, $itemId, $taxClassCacheKey) {
		
		if (self::pricesEnteredNet() == true && $getNet == true) {
			return $price;
		}
		elseif (self::pricesEnteredNet() == false && $getNet == false) {
			return $price;
		}
		elseif(self::pricesEnteredNet() == true && $getNet == false) {
			$taxRate = self::getTaxRate(self::$cache[$taxClassCacheKey][$itemId]);
			$taxAmount = round($price / 100 * $taxRate, 2);
			return $price + $taxAmount;
		}
		elseif(self::pricesEnteredNet() == false && $getNet == true) {
			$taxRate = self::getTaxRate(self::$cache[$taxClassCacheKey][$itemId]);
			// Tax amount is not rounded, because that will get us the right net from which you will
			// be able to get the right rounded tax amount and with that the matching and original gross amount.
			$taxAmount = $price / (100 + $taxRate) * $taxRate;
			$netAmount = round($price - $taxAmount, 3);
			return $netAmount;
		}
		else {
			throw new Exception('Wrong setting in pricing', 500);
		}
	
	}

	/**
	 * @param int  $productId      ID of the product
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Product base price
	 */
	public static function getProductPrice($productId, $getNet = NULL, $inBaseCurrency = true) {

		if (!$productId) {
			return 0;
		}

		if (KenedoPlatform::getName() == 'magento') {
			$sessionKey = 'cbproduct_base_price_'.$productId;
			if (isset($_SESSION[$sessionKey])) {
				$price = $_SESSION[$sessionKey];
				if (!$inBaseCurrency) {
					$price *= CONFIGBOX_CURRENCY_MULTI;
				}
				return $price;
			}
			else {
				return 0;
			}
		}


		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
		
		if (self::$cache === NULL) {
			self::populateCache();
		}
		
		$price = self::$cache['priceByProduct'][$productId];
		
		if (!$inBaseCurrency) {
			$price *= CONFIGBOX_CURRENCY_MULTI;
		}
		
		return self::getNormalizedPrice($price, $getNet, $productId, 'taxClassIdByProduct');
		
	}

	/**
	 * @param int  $productId      ID of the product
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Product recurring base price
	 */
	public static function getProductPriceRecurring($productId, $getNet = NULL, $inBaseCurrency = true) {

		if (!$productId) {
			return 0;
		}
		
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
		
		if (self::$cache === NULL) {
			self::populateCache();
		}
		
		$price = self::$cache['priceRecurringByProduct'][$productId];
		
		if (!$inBaseCurrency) {
			$price *= CONFIGBOX_CURRENCY_MULTI;
		}
		
		return self::getNormalizedPrice($price, $getNet, $productId, 'taxClassIdByProductRecurring');
		
	}

	/**
	 * @param float $baseTotalNet
	 * @param int $productId
	 * @param bool $inBaseCurrency
	 * @return float Gross position total
	 */
	public static function getPositionPriceGross($baseTotalNet, $productId, $inBaseCurrency = true) {
	
		if (self::$cache === NULL) {
			self::populateCache();
		}

		if (!$inBaseCurrency) {
			$baseTotalNet *= CONFIGBOX_CURRENCY_MULTI;
		}

		$taxRate = self::getTaxRate(self::$cache['taxClassIdByProduct'][$productId]);
		$taxAmount = round($baseTotalNet / 100 * $taxRate, 2);
		$grossOrderPrice = $baseTotalNet + $taxAmount;
		return $grossOrderPrice;

	}

	/**
	 * @param float $baseTotalNet
	 * @param int $productId
	 * @param bool $inBaseCurrency
	 * @return float Gross position total recurring
	 */
	public static function getPositionPriceRecurringGross($baseTotalNet, $productId, $inBaseCurrency = true) {
		
		if (self::$cache === NULL) {
			self::populateCache();
		}

		if (!$inBaseCurrency) {
			$baseTotalNet *= CONFIGBOX_CURRENCY_MULTI;
		}

		$taxRate = self::getTaxRate(self::$cache['taxClassIdByProduct'][$productId]);
		$grossOrderPrice = $baseTotalNet + round($baseTotalNet / 100 * $taxRate, 2);
		return $grossOrderPrice;

	}

	/**
	 * @param int  $productId      ID of the product
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Product base was price
	 */
	public static function getProductWasPrice($productId, $getNet = NULL, $inBaseCurrency = true) {
	
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		$price = self::$cache['wasPriceByProduct'][$productId];
	
		if (!$inBaseCurrency) {
			$price *= CONFIGBOX_CURRENCY_MULTI;
		}
	
		return self::getNormalizedPrice($price, $getNet, $productId, 'taxClassIdByProduct');
	
	}

	/**
	 * @param int  $productId      ID of the product
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Product recurring base was price
	 */
	public static function getProductWasPriceRecurring($productId, $getNet = NULL, $inBaseCurrency = true) {
	
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		$price = self::$cache['wasPriceRecurringByProduct'][$productId];
	
		if (!$inBaseCurrency) {
			$price *= CONFIGBOX_CURRENCY_MULTI;
		}
	
		return self::getNormalizedPrice($price, $getNet, $productId, 'taxClassIdByProductRecurring');
	
	}

	/**
	 * @param int  $elementId      ID of the element
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Element price in the current configuration
	 */
	public static function getElementPrice($elementId, $getNet = NULL, $inBaseCurrency = true) {
		
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
		
		if (self::$cache === NULL) {
			self::populateCache();
		}
		
		if (isset(self::$cache['calcModelByElement'][$elementId])) {
			$price = ConfigboxCalculation::calculate( self::$cache['calcModelByElement'][$elementId] , $elementId, 0);
			
			if (!$inBaseCurrency) {
				$price *= CONFIGBOX_CURRENCY_MULTI;
			}
			
			return self::getNormalizedPrice($price, $getNet, $elementId, 'taxClassIdByElement');
			
		}
		else {
			$configuration = ConfigboxConfiguration::getInstance();
			$elementXrefId = $configuration->getElementXrefId($elementId);
			
			if ($elementXrefId) {
				$price = self::getXrefPrice($elementXrefId, $elementId, $getNet, $inBaseCurrency);
				return $price;
			}
			else {
				$price = 0;
				return $price;
			}
		}
		
	}

	/**
	 * @param int  $elementId      ID of the element
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Element recurring price in the current configuration
	 */
	public static function getElementPriceRecurring($elementId, $getNet = NULL, $inBaseCurrency = true) {
		
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
		
		if (self::$cache === NULL) {
			self::populateCache();
		}
		
		if (isset(self::$cache['calcModelRecurringByElement'][$elementId])) {
			$price = ConfigboxCalculation::calculate( self::$cache['calcModelRecurringByElement'][$elementId] , $elementId, 0);
			if (!$inBaseCurrency) {
				$price *= CONFIGBOX_CURRENCY_MULTI;
			}
			
			return self::getNormalizedPrice($price, $getNet, $elementId, 'taxClassIdByElementRecurring');
			
		}
		else {
			$configuration = ConfigboxConfiguration::getInstance();
			$elementXrefId = $configuration->getElementXrefId($elementId);
			
			if ($elementXrefId) {
				$price = self::getXrefPriceRecurring($elementXrefId, $elementId, $getNet, $inBaseCurrency);
				return $price;
			}
			else {
				$price = 0;
				return $price;
			}
		}
		
	}

	/**
	 * @param int  $xrefId         ID of the option assignment
	 * @param int  $elementId      Element ID of the option assignment (redundant but speeds things up a bit)
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Option assignment price in the current configuration
	 */
	public static function getXrefPrice($xrefId, $elementId, $getNet = NULL, $inBaseCurrency = true) {
		
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
		
		if (self::$cache === NULL) {
			self::populateCache();
		}
		
		if (isset(self::$cache['calcModelByXref'][$xrefId])) {
			$price = ConfigboxCalculation::calculate( self::$cache['calcModelByXref'][$xrefId]['calcmodel'] , self::$cache['calcModelByXref'][$xrefId]['regardingElement'], $xrefId);
		}
		else {
			$price = self::$cache['priceByXref'][$xrefId];
		}
		
		if (!$inBaseCurrency) {
			$price *= CONFIGBOX_CURRENCY_MULTI;
		}
		
		return self::getNormalizedPrice($price, $getNet, $elementId, 'taxClassIdByElement');
		
	}

	/**
	 * @param int  $xrefId         ID of the option assignment
	 * @param int  $elementId      Element ID of the option assignment (redundant but speeds things up a bit)
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Option assignment recurring price in the current configuration
	 */
	public static function getXrefPriceRecurring($xrefId, $elementId, $getNet = NULL, $inBaseCurrency = true) {
	
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
		
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		if (isset(self::$cache['calcModelRecurringByXref'][$xrefId])) {
			$price = ConfigboxCalculation::calculate( self::$cache['calcModelRecurringByXref'][$xrefId]['calcmodelRecurring'] , self::$cache['calcModelRecurringByXref'][$xrefId]['regardingElement'], $xrefId);
		}
		else {
			$price = self::$cache['priceRecurringByXref'][$xrefId];
		}
		
		if (!$inBaseCurrency) {
			$price *= CONFIGBOX_CURRENCY_MULTI;
		}
		
		return self::getNormalizedPrice($price, $getNet, $elementId, 'taxClassIdByElement');
		
	}

	/**
	 * @param int  $xrefId         ID of the option assignment
	 * @param int  $elementId      Element ID of the option assignment (redundant but speeds things up a bit)
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Option assignment was price in the current configuration
	 */
	public static function getXrefWasPrice($xrefId, $elementId, $getNet = NULL, $inBaseCurrency = true) {
		
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		$price = self::$cache['wasPriceByXref'][$xrefId];
		
		if (!$inBaseCurrency) {
			$price *= CONFIGBOX_CURRENCY_MULTI;
		}
	
		return self::getNormalizedPrice($price, $getNet, $elementId, 'taxClassIdByElement');
	
	}

	/**
	 * @param int  $xrefId         ID of the option assignment
	 * @param int  $elementId      Element ID of the option assignment (redundant but speeds things up a bit)
	 * @param null|boolean $getNet True for net, false for gross, NULL to auto-determine (will depend on customer group
	 *                             setting B2B mode.
	 * @param bool $inBaseCurrency If the price should be in base currency or in current currency
	 *
	 * @return float Option assignment recurring was price in the current configuration
	 */
	public static function getXrefWasPriceRecurring($xrefId, $elementId, $getNet = NULL, $inBaseCurrency = true) {
	
		if ($getNet === NULL) {
			$getNet = ConfigboxPrices::showNetPrices();
		}
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		$price = self::$cache['wasPriceRecurringByXref'][$xrefId];
	
		if (!$inBaseCurrency) {
			$price *= CONFIGBOX_CURRENCY_MULTI;
		}
	
		return self::getNormalizedPrice($price, $getNet, $elementId, 'taxClassIdByElement');
	
	}

	/**
	 * @param int $elementId
	 *
	 * @return float
	 */
	public static function getElementWeight($elementId) {
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		if (isset(self::$cache['calcModelWeightByElement'][$elementId])) {
			$weight = ConfigboxCalculation::calculate( self::$cache['calcModelWeightByElement'][$elementId] , $elementId, 0);
			return $weight;
		}
		else {
			$configuration = ConfigboxConfiguration::getInstance();
			$elementXrefId = $configuration->getElementXrefId($elementId);
	
			if ($elementXrefId) {
				$weight = self::getXrefWeight($elementXrefId);
				return $weight;
			}
			else {
				$weight = 0;
				return $weight;
			}
		}
	
	}

	/**
	 * @param int $xrefId
	 *
	 * @return object
	 */
	public static function getXrefWeight($xrefId) {
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		if (isset(self::$cache['calcModelWeightByXref'][$xrefId])) {
			$weight = ConfigboxCalculation::calculate( self::$cache['calcModelWeightByXref'][$xrefId]['calcmodelWeight'] , self::$cache['calcModelWeightByXref'][$xrefId]['regardingElement'], $xrefId);
		}
		else {
			$weight = self::$cache['weightByXref'][$xrefId];
		}
	
		return $weight;
	
	}

	/**
	 * @param int $productId
	 *
	 * @return float
	 */
	public static function getProductTaxRate($productId) {
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		$taxClassId = self::getProductTaxClassId($productId);
		$taxRate = self::getTaxRate($taxClassId);
		return $taxRate;
	}

	/**
	 * @param int $productId
	 *
	 * @return float
	 */
	public static function getProductTaxClassId($productId) {
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		$taxClassId = self::$cache['taxClassIdByProduct'][$productId];
		return $taxClassId;
	}

	/**
	 * @param int $productId
	 *
	 * @return float
	 */
	public static function getProductTaxRateRecurring($productId) {
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		$taxClassId = self::getProductTaxClassIdRecurring($productId);
		$taxRate = self::getTaxRate($taxClassId);
		return $taxRate;
	}

	/**
	 * @param int $productId
	 *
	 * @return int
	 */
	public static function getProductTaxClassIdRecurring($productId) {
	
		if (self::$cache === NULL) {
			self::populateCache();
		}
	
		$taxClassId = self::$cache['taxClassIdByProductRecurring'][$productId];
		return $taxClassId;
	}

	/**
	 * Populates the cache
	 * @see ConfigboxPrices::$cache
	 */
	protected static function populateTaxRateCache() {

		$db = KenedoPlatform::getDb();
		$query = "SELECT `id`, `default_tax_rate` as `taxRate` FROM `#__configbox_tax_classes`";
		$db->setQuery($query);
		$taxRates = $db->loadAssocList();
		
		foreach ($taxRates as $taxRate) {
			KenedoObserver::triggerEvent('onConfigboxGetTaxRate', array(&$taxRate['taxRate'],$taxRate['id'],NULL));
			self::$cache['taxRates'][$taxRate['id']] = (float)$taxRate['taxRate'];
		}
	
	}

	/**
	 * Populates the cache
	 * @see ConfigboxPrices::$cache
	 */
	protected static function populateCache() {
		
		KLog::start('populatePriceCache');
		
		self::$cache = ConfigboxCacheHelper::getFromCache('rovedoPrices');
		
		if (self::$cache !== NULL) {
			KLog::stop('populatePriceCache');
			return;
		}

		$db = KenedoPlatform::getDb();
		
		$query = "
			SELECT 	xref.element_id AS element_id, xref.id AS xref_id, 
					xref.calcmodel AS xref_calcmodel, xref.calcmodel_recurring AS xref_calcmodel_recurring, 
					xref.calcmodel_weight AS xref_calcmodel_weight, 
					o.price AS xref_price, o.price_recurring AS xref_price_recurring, o.weight AS xref_weight,
					o.was_price AS xref_was_price, o.was_price_recurring AS xref_was_price_recurring
					
			FROM `#__configbox_xref_element_option` AS xref
			LEFT JOIN `#__configbox_options` AS o ON o.id = xref.option_id
			LEFT JOIN `#__configbox_elements` AS e ON e.id = xref.element_id
		";
		
		
		$db->setQuery($query);
		$items = $db->loadAssocList();
		
		foreach ($items as &$item) {
			
			if ($item['xref_calcmodel']) {
				self::$cache['calcModelByXref'][$item['xref_id']]['calcmodel'] = (int)$item['xref_calcmodel'];
				self::$cache['calcModelByXref'][$item['xref_id']]['regardingElement'] = (int)$item['element_id'];
			}
			if ($item['xref_calcmodel_recurring']) {
				self::$cache['calcModelRecurringByXref'][$item['xref_id']]['calcmodelRecurring'] = (int)$item['xref_calcmodel_recurring'];
				self::$cache['calcModelRecurringByXref'][$item['xref_id']]['regardingElement'] = (int)$item['element_id'];
			}
			if ($item['xref_calcmodel_weight']) {
				self::$cache['calcModelWeightByXref'][$item['xref_id']]['calcmodelWeight'] = (int)$item['xref_calcmodel_weight'];
				self::$cache['calcModelWeightByXref'][$item['xref_id']]['regardingElement'] = (int)$item['element_id'];
			}
			
			self::$cache['weightByXref'][$item['xref_id']] = (float)$item['xref_weight'];
			self::$cache['priceByXref'][$item['xref_id']] = (float)$item['xref_price'];
			self::$cache['priceRecurringByXref'][$item['xref_id']] = (float)$item['xref_price_recurring'];
			self::$cache['wasPriceByXref'][$item['xref_id']] = (float)$item['xref_was_price'];
			self::$cache['wasPriceRecurringByXref'][$item['xref_id']] = (float)$item['xref_was_price_recurring'];

		}
		
		$query = "
		SELECT 	
			e.id AS element_id, e.calcmodel, e.calcmodel_recurring, e.calcmodel_weight, 
			p.id AS product_id, 
			p.taxclass_id, p.taxclass_recurring_id,
			p.baseprice AS product_price, p.baseprice_recurring AS product_price_recurring
			
		FROM `#__configbox_elements` AS e
		LEFT JOIN `#__configbox_pages` AS c ON c.id =  e.page_id
		LEFT JOIN `#__configbox_products` AS p ON p.id = c.product_id
		"; 
		$db->setQuery($query);
		$mixInElements = $db->loadAssocList();
		
		foreach ($mixInElements as $item) {
			if ($item['calcmodel']) {
				self::$cache['calcModelByElement'][$item['element_id']] = (int)$item['calcmodel'];
			}
			if ($item['calcmodel_recurring']) {
				self::$cache['calcModelRecurringByElement'][$item['element_id']] = (int)$item['calcmodel_recurring'];
			}
			if ($item['calcmodel_weight']) {
				self::$cache['calcModelWeightByElement'][$item['element_id']] = (int)$item['calcmodel_weight'];
			}
			if ($item['calcmodel_weight']) {
				self::$cache['calcModelWeightByElement'][$item['element_id']] = (int)$item['calcmodel_weight'];
			}
			self::$cache['taxClassIdByElement'][$item['element_id']] = $item['taxclass_id'];
			self::$cache['taxClassIdByElementRecurring'][$item['element_id']] = $item['taxclass_recurring_id'];
			
		}
				
		$query = "
		SELECT 
		p.id AS product_id, 
		p.baseprice AS product_price, p.baseprice_recurring AS product_price_recurring,
		p.was_price AS product_was_price, p.was_price_recurring AS product_was_price_recurring, 
		p.taxclass_id, p.taxclass_recurring_id
		FROM `#__configbox_products` AS p
		";
		$db->setQuery($query);
		$mixInProducts = (array)$db->loadAssocList();
		foreach ($mixInProducts as $item) {
			self::$cache['taxClassIdByProduct'][$item['product_id']] = $item['taxclass_id'];
			self::$cache['taxClassIdByProductRecurring'][$item['product_id']] = $item['taxclass_recurring_id'];
			self::$cache['priceByProduct'][$item['product_id']] = (float)$item['product_price'];
			self::$cache['priceRecurringByProduct'][$item['product_id']] = (float)$item['product_price_recurring'];
			self::$cache['wasPriceByProduct'][$item['product_id']] = (float)$item['product_was_price'];
			self::$cache['wasPriceRecurringByProduct'][$item['product_id']] = (float)$item['product_was_price_recurring'];
		}

		ConfigboxCacheHelper::writeToCache('rovedoPrices', self::$cache);
		
		KLog::stop('populatePriceCache');

	}

	/**
	 * All data of the tax classes table
	 * @return array[]
	 */
	public static function getTaxClasses() {
		
		if (self::$taxClasses === NULL) {
			$query = "SELECT * FROM `#__configbox_tax_classes`";
			$db = KenedoPlatform::getDb();
			$db->setQuery($query);
			self::$taxClasses = $db->loadAssocList();
		}
		
		return self::$taxClasses;
	}
}