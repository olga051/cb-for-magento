<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewInforequest extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'inforequest';

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	/**
	 * @var object $customer Object holding customer data
	 * @see ConfigboxUserHelper::getUser
	 */
	public $customer;

	/**
	 * @var object[] $userFields Array of objects holding info about what fields to show and which are required (see Backend GUI -> Customer fields)
	 * @see ConfigboxUserHelper::getUserFields
	 */
	public $userFields;

	/**
	 * @var string $type Shows what type of info request it is (quotation, assistance, save_order)
	 */
	public $type;

	/**
	 * @var string $failureUrl URL to redirect to when storing went wrong
	 */
	public $failureUrl;

	/**
	 * @var string $failureUrl URL to redirect to when storing went well
	 */
	public $successUrl;

	/**
	 * @var string $urlPasswordReset The URL comes from the platform method
	 * @see InterfaceKenedoPlatform::getPasswordResetLink
	 */
	public $urlPasswordReset;

	/**
	 * @var int $fromCart Indicates if the customer came from the cart page
	 */
	public $fromCart;

	/**
	 * @var int $fromConfigurator Indicates if the customer came from a configurator page
	 */
	public $fromConfigurator;

	/**
	 * @var int $cartId ID of the cart
	 */
	public $cartId;

	/**
	 * @var int $cartPositionId ID of the cart position
	 */
	public $cartPositionId;

	function display() {

		$this->assign('urlPasswordReset', KenedoPlatform::p()->getPasswordResetLink());

		if (KRequest::getInt('success')) {
			
			if (KRequest::getKeyword('type') == 'quotation') {
				
				$cartId = KRequest::getInt('cart_id');
				
				if (KRequest::getInt('from_cart')) {
					$link = KenedoObserver::triggerEvent('onConfigBoxGetQuotationLink',array($cartId),true);
				}
				else {
					$positionId = KRequest::getInt('cart_position_id',0);
					$link = KenedoObserver::triggerEvent('onConfigBoxGetQuotationLink',array($cartId,$positionId),true);
				}
				
				ob_start();
				
				// CONFIG QUOTATION NEW TAB
				if ($link->newTab) {
					?>
					window.parent.cbj('.request-quote').removeClass('rovedo-modal').addClass('new-tab').attr('href','<?php echo $link->url;?>');
					window.parent.cbj.colorbox.close();
					window.open('<?php echo str_replace('&amp;','&',$link->url);?>');
					<?php
				}
				elseif($link->type == 'modal') {
					?>
					window.location.href = '<?php echo $link->url;?>';
					<?php
				}
				else {
					?>
					window.parent.cbj('.request-quote').removeClass('rovedo-modal').removeClass('new-tab').attr('href','<?php echo $link->url;?>');
					window.parent.cbj.colorbox.close();
					window.parent.location.href = '<?php echo str_replace('&amp;','&',$link->url);?>';
					<?php
				}
				$js = ob_get_clean();
				KenedoPlatform::p()->addScriptDeclaration($js,true);
			}
			
			if (KRequest::getKeyword('type') == 'assistance') {
			
				$id = KRequest::getInt('cart_id');
				$link = KenedoObserver::triggerEvent('onConfigBoxGetAssistanceLink',array($id),true);
				ob_start();
				?>
				window.parent.cbj.colorbox.close();
				window.parent.location.href = '<?php echo $link->url;?>';
				<?php
				$js = ob_get_clean();
				KenedoPlatform::p()->addScriptDeclaration($js,true);
			}
			
			if (KRequest::getKeyword('type') == 'saveorder') {
					
				$id = KRequest::getInt('cart_id');
				$link = KenedoObserver::triggerEvent('onConfigBoxGetSaveOrderLink',array($id),true);
				ob_start();
				?>
				window.parent.cbj.colorbox.close();
				window.parent.location.href = '<?php echo $link->url;?>';
				<?php
				$js = ob_get_clean();
				KenedoPlatform::p()->addScriptDeclaration($js,true);
			}
			
		}
		else {
			
			$userId = ConfigboxUserHelper::getUserId();
			
			$userData = ConfigboxUserHelper::getUser($userId, false, true);
			$this->assignRef('customer',$userData);
			
			$user = ConfigboxUserHelper::getUser();
			$userFields = ConfigboxUserHelper::getUserFields($user->group_id);
			
			$this->assignRef('userFields',$userFields);

			$js = "
			cbj(document).ready(function(){
			if (typeof(window.com_configbox) == 'undefined') window.com_configbox = {};\n
			com_configbox.customerFieldDefinitions = ".json_encode($userFields).";\n
			var userFields = com_configbox.customerFieldDefinitions\n;
			});
			";

			KenedoPlatform::p()->addScriptDeclaration($js, true);
			
			$type = KRequest::getKeyword('type','quotation');
			$this->assignRef('type',$type);
			
			$fromCart = KRequest::getInt('from_cart',0);
			$this->assignRef('fromCart',$fromCart);
			
			// Legacy call, remove with 2.7
			$this->assignRef('fromGrandOrder',$fromCart);
			
			$fromConfigurator = KRequest::getInt('from_configurator',0);
			$this->assignRef('fromConfigurator',$fromConfigurator);
			
			$cartId = KRequest::getInt('cart_id',0);
			$this->assignRef('cartId',$cartId);
			
			// Legacy call, remove with 2.7
			$this->assignRef('grandOrderId',$cartId);
			
			$positionId = KRequest::getInt('cart_position_id',0);
			$this->assignRef('cartPositionId',$positionId);
			
				
			if ($fromCart) {
				$this->assign('successUrl', urlencode( KLink::getRoute('index.php?option=com_configbox&view=inforequest&tmpl=component&type='.hsc($this->type).'&from_cart=1&cart_id='.intval($this->cartId).'&success=1', false) ));
				$this->assign('failureUrl', urlencode( KLink::getRoute('index.php?option=com_configbox&view=inforequest&tmpl=component&type='.hsc($this->type).'&from_cart=1&cart_id='.intval($this->cartId).'&success=0', false) ));
			}
			elseif($fromConfigurator) {
				$this->assign('successUrl', urlencode( KLink::getRoute('index.php?option=com_configbox&view=inforequest&tmpl=component&type='.hsc($this->type).'&from_configurator=1&cart_position_id='.intval($positionId).'&cart_id='.intval($this->cartId).'&success=1', false) ));
				$this->assign('failureUrl', urlencode( KLink::getRoute('index.php?option=com_configbox&view=inforequest&tmpl=component&type='.hsc($this->type).'&from_configurator=1&cart_position_id='.intval($positionId).'&cart_id='.intval($this->cartId).'&success=0', false) ));
			}
			else {
				return;	
			}
			
			
			$this->renderView();
		}
		
	}
}