<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewAdminproducttree */
?>
<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>">
<a class="toggle-tree-edit"></a>
<ul class="product-list" data-update-url="<?php echo $this->treeUpdateUrl;?>">
	<?php foreach($this->tree as $product) { ?>
		<li class="product-item<?php echo ($product['published']) ? '':' inactive';?>" id="product-<?php echo intval($product['id']);?>">
			<div>
				<span id="product-trigger-<?php echo intval($product['id']);?>" class="sub-list-trigger product-title<?php echo (count($product['pages'])) ? ' has-sub-list':' no-sub-list';?><?php echo (in_array($product['id'],$this->openIds['products'])) ? ' trigger-opened':'';?>"></span>
				<a class="edit-link ajax-target-link" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminproducts&task=edit&id='.$product['id']);?>" title="<?php echo hsc($product['title']);?>"><?php echo hsc($product['title']);?></a>
				<a class="trigger-remove" data-url="<?php echo $product['url_delete'];?>"></a>
				<a class="trigger-copy"></a>
				<ul class="sub-list page-list<?php echo (in_array($product['id'],$this->openIds['products'])) ? ' list-opened':'';?>">
					<?php foreach ($product['pages'] as $page) { ?>
						<li class="page-item<?php echo ($page['published']) ? '':' inactive';?>" id="page-<?php echo intval($page['id']);?>">
							<div>
								<span id="page-trigger-<?php echo intval($page['id']);?>" class="sub-list-trigger configurator-page-title<?php echo (count($page['elements'])) ? ' has-sub-list':' no-sub-list';?><?php echo (in_array($page['id'],$this->openIds['pages'])) ? ' trigger-opened':'';?>"></span>
								<a class="edit-link ajax-target-link" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminpages&task=edit&id='.$page['id']);?>" title="<?php echo hsc($page['title'])?>"><?php echo hsc($page['title'])?></a>
								<a class="trigger-remove" data-url="<?php echo $page['url_delete'];?>"></a>
								<a class="trigger-copy"></a>
								<ul class="sub-list element-list<?php echo (in_array($page['id'],$this->openIds['pages'])) ? ' list-opened':'';?>">
									<?php foreach ($page['elements'] as $element) { ?>
										<li class="element-item<?php echo ($element['published']) ? '':' inactive';?>" id="element-<?php echo intval($element['id']);?>">
											<div>
												<span id="element-trigger-<?php echo intval($element['id']);?>" class="sub-list-trigger element-title<?php echo (count($element['options'])) ? ' has-sub-list':' no-sub-list';?><?php echo (in_array($element['id'],$this->openIds['elements'])) ? ' trigger-opened':'';?>"></span>
												<a class="edit-link ajax-target-link" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminelements&task=edit&id='.$element['id']);?>" title="<?php echo (CONFIGBOX_USE_INTERNAL_ELEMENT_NAMES) ? hsc($element['internal_name']) : hsc($element['title']);?>"><?php echo (CONFIGBOX_USE_INTERNAL_ELEMENT_NAMES) ? hsc($element['internal_name']) : hsc($element['title']);?></a>
												<a class="trigger-remove" data-url="<?php echo $element['url_delete'];?>"></a>
												<a class="trigger-copy"></a>
												<ul class="sub-list option-list<?php echo (in_array($element['id'],$this->openIds['elements'])) ? ' list-opened':'';?>">
													<?php foreach ($element['options'] as $option) { ?>
														<li class="option-item<?php echo ($option['published']) ? '':' inactive';?>" id="option-<?php echo intval($option['id']);?>">
															<div>
																<a class="edit-link ajax-target-link" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminoptionassignments&task=edit&id='.$option['id']);?>" title="<?php echo hsc($option['title']);?>"><?php echo hsc($option['title']);?></a>
																<a class="trigger-remove"></a>
																<a class="trigger-copy"></a>
															</div>
														</li>
													<?php } ?>
												</ul>
											</div>
										</li>
									<?php } ?>
									<li class="element-item add-item">
										<a class="add-link ajax-target-link" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminelements&task=edit&id=0&page_id='.$page['id']);?>"><?php echo KText::_('Add element');?></a>
									</li>
								</ul>
							</div>
						</li>
					<?php } ?>
					<li class="page-item add-item">
						<a class="add-link ajax-target-link" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminpages&task=edit&id=0&product_id='.$product['id']);?>"><?php echo KText::_('Add page');?></a>
					</li>
				</ul>
			</div>
		</li>
	<?php } ?>
	<li class="product-item add-item">
		<a class="add-link ajax-target-link" href="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminproducts&task=edit&id=0');?>"><?php echo KText::_('Add product');?></a>
	</li>
</ul>
</div>