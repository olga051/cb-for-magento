<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerAjaxapi extends KenedoController {

	/**
	 * @return NULL
	 */
	protected function getDefaultModel() {
		return NULL;
	}

	/**
	 * @return ConfigboxViewAjaxapi
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewAjaxapi');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

	function getStateSelectOptions() {
		$this->display();
	}
	
	function getCountySelectOptions() {
		$this->display();
	}
	
	function getCityInput() {
		$this->display();
	}
	
	function validateRegex() {
		$this->display();
	}

	function getElementOptions() {
		$this->display();
	}

	function getNotificationUrl() {
		
//		$langUrlCode = KenedoPlatform::p()->getLanguageUrlCode( CONFIGBOX_LANGUAGE_TAG );
		$paymentClassName = KRequest::getKeyword('payment_class');
		
		$url = 'index.php?option=com_configbox&controller=ipn&task=processipn&connector_name='.$paymentClassName;
		
		$route = KLink::getRoute($url, false);
		
		$prefix = (CONFIGBOX_SECURECHECKOUT) ? 'https://'.KPATH_HOST : 'http://'.KPATH_HOST;
		
		$url = $prefix . $route;
		
		echo $url;
		
	}
	

}
