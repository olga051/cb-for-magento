<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewUserorder */
?>

<div class="order-files">

	<ul class="order-file-list">
		<?php foreach ($this->orderFiles as $upload) { ?>
			<li id="file-<?php echo (int)$upload->id;?>" class="order-file"><a title="<?php echo hsc($upload->comment);?>" class="file-name"><?php echo hsc($upload->file);?></a> <a class="file-delete fa fa-times-circle"><?php echo KText::_('Delete');?></a></li>
		<?php } ?>
		<li id="order-file-blueprint" class="order-file"><a class="file-name"></a> <a class="file-delete fa fa-times-circle"><?php echo KText::_('Delete');?></a></li>
	</ul>


	<div class="file-upload">

		<a class="upload-file-toggle button-frontend-small"><?php echo KText::_('Upload a new file');?></a>

		<div class="upload-file-content">

			<form enctype="multipart/form-data" method="post" id="file-upload-form" target="file-upload-iframe">
				<div>

					<div class="form-item form-item-file">
						<label for="orderFile"><?php echo KText::_('File');?></label>
						<input name="orderFile" id="orderFile" type="file" />
					</div>

					<div class="form-item form-item-comment">
						<label for="comment"><?php echo KText::_('Comment');?></label>
						<textarea name="comment" id="comment" rows="4" cols="20" class="comment-field"></textarea>
					</div>

					<div class="form-controls">
						<a class="form-cancel button-frontend-small"><?php echo KText::_('Close');?></a>
						<a class="form-send button-frontend-small"><?php echo KText::_('Send');?></a>
					</div>

					<div class="hidden-fields">
						<input type="submit" name="submit-buttom" value="submit" />
						<input type="hidden" name="option" value="com_configbox" />
						<input type="hidden" name="controller" value="userorder" />
						<input type="hidden" name="task" value="addOrderFile" />
						<input type="hidden" name="format" value="raw" />
						<input type="hidden" name="orderId" value="<?php echo (int)$this->orderRecord->id;?>" />
					</div>
				</div>
			</form>

		</div>
	</div>

	<iframe style="display:none" src="about:blank" class="file-upload-iframe" id="file-upload-iframe" name="file-upload-iframe"></iframe>

	<div class="clear"></div>

</div>
