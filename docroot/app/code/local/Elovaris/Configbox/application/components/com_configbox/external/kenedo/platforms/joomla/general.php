<?php
defined('CB_VALID_ENTRY') or die();

class KenedoPlatformJoomla implements InterfaceKenedoPlatform {
	
	protected $db;
	protected $languages = NULL;
	protected $cache;
	protected $errors;
	public $scriptDeclarations= array();
	
	public function initialize() {

		// Joomla's direct execution safeguard
		if (!defined('_JEXEC')) {
			define('_JEXEC',1);
		}

		// Set the document base in frontend (since many templates neglect to do so)
		if (JFactory::getApplication('Cms')->isSite()) {
			// Set the base URL
			KenedoPlatform::p()->setDocumentBase( $this->getUrlBase().'/' );
		}

		if (JFactory::getApplication()->isAdmin()) {
			if (class_exists('JToolBarHelper')) {
				JToolBarHelper::title('ConfigBox');
			}
		}
		
		// Make Joomla's SEO rewriting behave on non html pages
		if (!empty($_SERVER['REQUEST_URI']) && substr($_SERVER['REQUEST_URI'],-4) == '.raw') {
			KRequest::setVar('format','raw');
		}
		
		// In case option is not set, do it
		if (empty($_REQUEST['option'])) {
			$_POST['option'] = $_GET['option'] = $_REQUEST['option'] = 'com_configbox';
		}
		
		// Set modal settings if requested
		if (!empty($_REQUEST['in_modal']) && $_REQUEST['in_modal'] != 0) {
			$_POST['tmpl'] = $_GET['tmpl'] = $_REQUEST['tmpl'] = 'component';
		}
		
		// Set ajax subview settings if requested
		if (!empty($_REQUEST['ajax_sub_view']) && $_REQUEST['ajax_sub_view'] != 0) {
			$_POST['tmpl'] = $_GET['tmpl'] = $_REQUEST['tmpl'] = 'component';
		}
		
		/* LEGACY VIEW NAMES - START */
		if (isset($_REQUEST['view'])) {
			if ($_REQUEST['view'] == 'category') {
				$_POST['view'] = $_GET['view'] = $_REQUEST['view'] = 'configuratorpage';
			}
			if ($_REQUEST['view'] == 'products') {
				$_POST['view'] = $_GET['view'] = $_REQUEST['view'] = 'productlisting';
			}
			if ($_REQUEST['view'] == 'grandorder') {
				$_POST['view'] = $_GET['view'] = $_REQUEST['view'] = 'cart';
			}
		}
		/* LEGACY VIEW NAMES - END */
		
		// Legacy, remove with 2.7
		if (isset($_REQUEST['grandorder_id'])) {
			$_POST['cart_id'] = $_GET['cart_id'] = $_REQUEST['cart_id'] = $_REQUEST['grandorder_id'];
		}
		if (isset($_REQUEST['from_grandorder'])) {
			$_POST['from_cart'] = $_GET['from_cart'] = $_REQUEST['from_cart'] = $_REQUEST['from_grandorder'];
		}
		
		// Legacy, remove with 2.7
		if (!empty($_REQUEST['option']) && $_REQUEST['option'] == 'com_configbox' && !empty($_REQUEST['cat_id']) && empty($_REQUEST['page_id']) ) {
			$ref = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';
			$log = 'A link with an outdated URL for configurator pages was found.';
			if ($ref) $log .= ' The link was found on page "'.$ref.'". Most likely an article, module or custom Configbox template. cat_id should be replaced by page_id. The link you see on the page may be processed. Check the source of the link, where you will see the URL parameters.';
			$log .= ' We keep supporting the old link until version 2.7 only, please change the link as soon as you can.';
			KLog::log($log,'deprecated');
			$_REQUEST['page_id'] = $_REQUEST['cat_id'];
		}

		// Legacy, remove with 2.7
		if (!empty($_REQUEST['option']) && $_REQUEST['option'] == 'com_configbox' && !empty($_REQUEST['pcat_id']) && empty($_REQUEST['listing_id']) ) {
			$ref = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';
			$log = 'A link with an outdated URL for product listing pages was found.';
			if ($ref) $log .= ' The link was found on page "'.$ref.'". Most likely an article, module or custom Configbox template. pcat_id should be replaced by listing_id. The link you see on the page may be processed. Check the source of the link, where you will see the URL parameters.';
			$log .= ' We keep supporting the old link until version 2.7 only, please change the link as soon as you can.';
			KLog::log($log,'deprecated');
			$_REQUEST['listing_id'] = $_REQUEST['pcat_id'];
		}

		// Legacy, remove with 2.7
		if (!empty($_REQUEST['option']) && $_REQUEST['option'] == 'com_cbcheckout') {
			$ref = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';
			$log = 'A link with an outdated URL for product a order management page was found.';
			if ($ref) $log .= ' The link was found on page "'.$ref.'". Most likely an article, module or custom Configbox template. The value parameter option should be replaced by com_cbcheckout (option=com_cbcheckout is now option=com_configbox). The link you see on the page may be processed. Check the source of the link, where you will see the URL parameters.';
			$log .= ' We keep supporting the old link until version 2.7 only, please change the link as soon as you can.';
			KLog::log($log,'deprecated');
			$_REQUEST['option'] = 'com_configbox';
		}
		
	}
	
	public function getDbConnectionData() {
		
		$connection = new StdClass();
		
		$connection->hostname 	= JFactory::getApplication()->getCfg('host');
		$connection->username 	= JFactory::getApplication()->getCfg('user');
		$connection->password 	= JFactory::getApplication()->getCfg('password');
		$connection->database 	= JFactory::getApplication()->getCfg('db');
		$connection->prefix 	= JFactory::getApplication()->getCfg('dbprefix');
		
		return $connection;
		
	}
	
	public function &getDb() {
		if (!$this->db) {
			require_once(dirname(__FILE__).DS.'database.php');
			$this->db = new KenedoDatabaseJoomla();
		}
		
		return $this->db;
	}
	
	public function redirect($url) {
		$url = str_replace('&amp;','&',$url);
		JFactory::getApplication()->redirect($url);
	}
	
	public function logout() {
		JFactory::getApplication()->logout();
	}

	/**
	 *
	 * Authenticate a user (in other words, figure out if the provided credentials match a user)
	 * This does not login a user.
	 *
	 * @param $username
	 * @param $password
	 * @return bool
	 */
	public function authenticate($username, $password) {

		if ($this->getVersionShort() == '1.5') {

			$credentials = array(
				'username'=>$username,
				'password'=>$password,
			);

			$options = array();

			jimport( 'joomla.user.authentication');

			$authenticate = JAuthentication::getInstance();
			$response	  = $authenticate->authenticate($credentials, $options);

			/** @noinspection PhpDeprecationInspection */
			if ($response->status === JAUTHENTICATE_STATUS_SUCCESS) {
				return true;
			}
			else {
				return false;
			}

		}
		// For Joomla version starting with 2.5
		else {

			$credentials = array(
				'username'=>$username,
				'password'=>$password,
			);

			$options = array();

			jimport('joomla.user.authentication');

			$authenticate = JAuthentication::getInstance();
			$response = $authenticate->authenticate($credentials, $options);

			if ($response->status === JAuthentication::STATUS_SUCCESS) {
				return true;
			}
			else {
				return false;
			}

		}

	}
	
	public function login($username) {

		JPluginHelper::importPlugin('user');

		$platformUserId = $this->getUserIdByUsername($username);

		if (!$platformUserId) {
			KLog::log('Platform login for username "'.$username.'" requested, but that user does not exist.');
			//TODO: Make this a proper user feedback message
			$this->setError('Cannot login user. User with username "'.$username.'" was not found.');
			return false;
		}

		$fakeAuthResponse = array(
			'username' => $username,
			'language' => $this->getLanguageTag(),
			'fullname' => $this->getUserFullName($platformUserId),
		);

		$options = array(
			'action' => 'core.login.site',
		);

		if ($this->getVersionShort() == '1.5') {
			$results = JFactory::getApplication()->triggerEvent('onLoginUser', array($fakeAuthResponse, $options));
		}
		else {
			$results = JFactory::getApplication()->triggerEvent('onUserLogin', array($fakeAuthResponse, $options));
		}

		if (in_array(true, $results)) {
			return true;
		}
		else {
			return false;
		}

		/*$credentials['username'] = $username;
		$credentials['password'] = $password;
		
		return JFactory::getApplication()->login($credentials, $options);*/

	}

	public function getTemplateName() {
		JFactory::getApplication()->getTemplate();
	}
	
	public function sendSystemMessage($text, $type = NULL) {
		JFactory::getApplication()->enqueueMessage($text, $type);
	}
	
	public function getVersionShort() {
		jimport('joomla.version');
		$version = new JVersion();
		return substr($version->getShortVersion(),0,3);
	}
	
	public function getDebug() {
		if (!isset($this->cache['debug'])) {
			$app = JFactory::getApplication();
			if (method_exists($app, 'getCfg')) {
				$this->cache['debug'] = JFactory::getApplication()->getCfg('debug');
			}
			else {
				$this->cache['debug'] = JFactory::getApplication()->get('debug');
			}

		}
		return $this->cache['debug'];
	}
	
	public function getDefaultListlimit() {
		return JFactory::getApplication()->getCfg('list_limit');
	}
	
	public function getConfigOffset() {
		return JFactory::getApplication()->getCfg('offset');
	}
	
	public function getMailerFromName() {
		return JFactory::getApplication()->getCfg('fromname');
	}
	
	public function getMailerFromEmail() {
		return JFactory::getApplication()->getCfg('mailfrom');
	}
	
	public function getTmpPath() {
		return JFactory::getApplication()->getCfg('tmp_path');
	}
	
	public function getLogPath() {
		return JFactory::getApplication()->getCfg('log_path');
	}
	
	public function getLanguageTag() {
		return JFactory::getLanguage()->get('tag');	
	}
	
	public function getLanguageUrlCode($languageTag = NULL) {
		
		if ($languageTag == NULL) {
			$languageTag = $this->getLanguageTag();
		}
		
		$languages = $this->getLanguages();
		
		if (!empty($languages[$languageTag])) {
			return $languages[$languageTag]->urlCode;
		}
		else {
			return NULL;
		}
		
	}
	
	public function getDocumentType() {
		return JFactory::getDocument()->getType();
	}
	
	public function addScript($path, $type = "text/javascript", $defer = false, $async = false) {
		if ($this->getDocumentType() == 'html') {
			JFactory::getDocument()->addScript($path, $type, $defer , $async);
		}
	}

	public function addScriptDeclaration($js, $newTag = false, $toBody = false) {

		if ($this->getDocumentType() != 'html') {
			return;
		}

		if ($toBody) {
			$this->scriptDeclarations[] = $js;
			return;
		}

		if ($newTag) {
			$js = trim($js);
			if (substr($js,0,7) != '<script') {
				$tag = '<script type="text/javascript">'."\n//<![CDATA[\n";
				$tag.= $js;
				$tag.= "\n//]]>\n".'</script>';
				$js = $tag;
			}

			/** @noinspection PhpUndefinedMethodInspection */
			JFactory::getDocument()->addCustomTag($js);
		}
		else {
			JFactory::getDocument()->addScriptDeclaration($js,'text/javascript');
		}

	}
	
	public function addStylesheet($path, $type = 'text/css', $media = 'all') {
		if ($this->getDocumentType() == 'html') {
			JFactory::getDocument()->addStyleSheet($path, $type, $media);
		}
	}
	
	public function addStyleDeclaration($css) {
		if ($this->getDocumentType() == 'html') {
			JFactory::getDocument()->addStyleDeclaration($css);
		}
	}
	
	public function isAdminArea() {
		return JFactory::getApplication()->isAdmin();	
	}
	
	public function isSiteArea() {
		return JFactory::getApplication()->isSite();
	}
	
	public function autoload($className, $classPath) {
		
		return KenedoAutoload::registerClass($className, $classPath);
		
	}
	
	public function processContentModifiers($text) {
		
		$item = new StdClass();
		
		$item->text = $text;
		$item->introtext = $text;
		$item->fulltext = $text;
		
		$params = array();
		
		$dispatcher = JDispatcher::getInstance();
		if ($this->getVersionShort() == 1.5) {
			$results = $dispatcher->trigger('onBeforeDisplayContent', array(&$item, &$params) );
			$item->beforeDisplayContent = trim(implode("\n", $results));
			$dispatcher->trigger('onPrepareContent', array(&$item, &$params) );
		}
		else {
			JPluginHelper::importPlugin('content');
			$page = 0;
			$dispatcher->trigger('onContentPrepare', array('com_configbox.content', &$item, &$params, &$page) );
		}
		
		return $item->text;
		
	}
	
	public function triggerEvent($eventName, $data) {
		
		// Import ConfigBox Plugins
		JPluginHelper::importPlugin('configbox');
		
		$dispatcher = JDispatcher::getInstance();
		return $dispatcher->trigger($eventName, $data);
	}
	
	public function raiseError($errorCode, $errorMessage) {

		if ($this->getVersionShort() == '1.5') {
			/** @noinspection PhpDeprecationInspection */
			JError::raiseError($errorCode,$errorMessage);
		}
		else {
			throw new Exception($errorMessage, intval($errorCode));
		}

	}
	
	public function renderHtmlEditor($dataFieldKey, $content, $width, $height, $cols, $rows) {

		return '<textarea name="'.hsc($dataFieldKey).'" id="'.hsc($dataFieldKey).'" class="kenedo-html-editor not-initialized" style="width:'.(int)$width.'px; height:'.$height.'px" rows="'.(int)$rows.'" cols="'.(int)$cols.'">'.hsc($content).'</textarea>';

		/*if (KRequest::getKeyword('format') != 'raw') {
			return JFactory::getEditor()->display( $dataFieldKey,  $content ,  $width, $height, $cols, $rows );
		}
		else {
			return '<textarea name="'.hsc($dataFieldKey).'" id="'.hsc($dataFieldKey).'" class="kenedo-html-editor not-initialized" style="width:'.(int)$width.'px; height:'.$height.'px" rows="'.(int)$rows.'" cols="'.(int)$cols.'">'.hsc($content).'</textarea>';
		}*/
	}
	
	public function sendEmail($fromEmail, $fromName, $receipient, $subject, $body, $isHtml = false, $cc = NULL, $bcc = NULL, $attachment = NULL) {
		
		$mailer = JFactory::getMailer();
		$mailer->setSender(array($fromEmail, $fromName));
		$mailer->addRecipient($receipient);
		$mailer->setSubject($subject);
		$mailer->setBody($body);
		$mailer->setFrom($fromEmail, $fromName);
		$mailer->IsHTML( $isHtml );
		
		$mailer->addCC($cc);
		$mailer->addBCC($bcc);
		
		$reflect = new ReflectionObject($mailer);
		
		if ($reflect->hasProperty('Sender') && $reflect->getProperty('Sender')->isPublic() ) {
			$mailer->Sender = $fromEmail;
		}
		
		if (!empty($attachment)) {
			if (is_string($attachment)) {
				$mailer->addAttachment($attachment);
			}
			elseif (is_array($attachment)) {
				foreach ($attachment as $attachmentItem) {
					$mailer->addAttachment($attachmentItem);
				}
			}
		}
		
		$response = $mailer->Send();
		
		return $response;
		
	}
	
	public function getGeneratorTag() {
		return JFactory::getDocument()->getGenerator();
	}
	
	public function setGeneratorTag($string) {
		return JFactory::getDocument()->setGenerator($string);
	}
	
	public function getUrlBase() {

		if (!empty($_SERVER['HTTP_HOST'])) {
			$base = JUri::base();
		}
		else {
			$base = '';
		}

		if (JFactory::getApplication('Cms')->isAdmin()) {
			$base = rtrim($base, '/');
			$ex = explode('/', $base);
			if (array_pop($ex) == 'administrator') {
				$base = implode('/',$ex);
			}
		}

		$base = rtrim($base, '/');

		return $base;

	}
	
	public function getDocumentBase() {
		return JFactory::getDocument()->getBase();
	}
	public function setDocumentBase($string) {
		return JFactory::getDocument()->setBase($string);
	}
	
	public function setDocumentMimeType($mime) {
		return JFactory::getDocument()->setMimeEncoding($mime);
	}
	
	public function getDocumentTitle() {
		return JFactory::getDocument()->getTitle();
	}
	
	public function setDocumentTitle($string) {
		return JFactory::getDocument()->setTitle($string);
	}
	
	public function setMetaTag($tag,$content) {
		JFactory::getDocument()->setMetaData($tag, $content);
	}
	
	public function isLoggedIn() {
		return (JFactory::getUser()->get('id') != 0);
	}
	
	public function getUserId() {
		return JFactory::getUser()->get('id');
	}
	
	public function getUserName($userId = NULL) {
		return JFactory::getUser($userId)->get('username');
	}
	
	public function getUserFullName($userId = NULL) {
		return JFactory::getUser($userId)->get('name');
	}
	
	public function getUserPasswordEncoded($userId = NULL) {
		return JFactory::getUser($userId)->get('password');
	}
	
	public function getUserIdByUsername($username) {
		jimport('joomla.user.helper');
		return JUserHelper::getUserId($username);
	}
	
	public function getUserGroupId($userId = NULL) {
		return JFactory::getUser($userId)->get('gid');
	}
	
	public function getUserTimezoneName($userId = NULL) {
		
		$version = $this->getVersionShort();
		
		if ($version == 1.5) {
			$user = JFactory::getUser($userId);
			$offset = (int)$user->getParam('timezone', $this->getConfigOffset() );
			
			$oldOffsets = array(
					'-12' => 'Etc/GMT-12', '-11' => 'Pacific/Midway', '-10' => 'Pacific/Honolulu','-9.5' => 'Pacific/Marquesas',
					'-9' => 'US/Alaska','-8' => 'US/Pacific','-7' => 'US/Mountain',
					'-6' => 'US/Central','-5' => 'US/Eastern','-4.5' => 'America/Caracas',
					'-4' => 'America/Barbados','-3.5' => 'Canada/Newfoundland',
					'-3' => 'America/Buenos_Aires','-2' => 'Atlantic/South_Georgia',
					'-1' => 'Atlantic/Azores','0' => 'Europe/London',
					'1' => 'Europe/Amsterdam','2' => 'Europe/Istanbul',
					'3' => 'Asia/Riyadh','3.5' => 'Asia/Tehran',
					'4' => 'Asia/Muscat','4.5' => 'Asia/Kabul',
					'5' => 'Asia/Karachi','5.5' => 'Asia/Calcutta',
					'5.75' => 'Asia/Katmandu','6' => 'Asia/Dhaka',
					'6.5' => 'Indian/Cocos','7' => 'Asia/Bangkok',
					'8' => 'Australia/Perth','8.75' => 'Australia/West',
					'9' => 'Asia/Tokyo','9.5' => 'Australia/Adelaide',
					'10' => 'Australia/Brisbane','10.5' => 'Australia/Lord_Howe',
					'11' => 'Pacific/Kosrae','11.5' => 'Pacific/Norfolk',
					'12' => 'Pacific/Auckland','12.75' => 'Pacific/Chatham',
					'13' => 'Pacific/Tongatapu','14' => 'Pacific/Kiritimati'
			);
			
			$tzName = timezone_open($oldOffsets[$offset])->getName();
		}
		else {
			
			$user = JFactory::getUser($userId);
			
			$tzName = $user->getParam('timezone', $this->getConfigOffset() );
		}
		
		return $tzName;
		
	}
	
	public function registerUser($data, $groupIds = array()) {

		$user['email'] = $data->email;
		$user['name'] = $data->name;
		$user['username'] = $data->username;
		$user['password'] = $data->password;
		$user['password2'] = $data->password2;
		
		if ($this->getVersionShort() == 1.5) {
			$user['gid'] = 18;
			$user['usertype'] = 'Registered';
		}
		else {
			$user['groups'] = $groupIds;
		}
		
		$this->unsetErrors();
		
		$juser = JUser::getInstance(0);
		if (!$juser->bind($user)) {
			/** @noinspection PhpDeprecationInspection */
			foreach ($juser->getErrors() as $error) {
				$this->setError($error);
			}
			return false;
		}

		// Suppress Joomla's account email dispatch
		if ($this->getVersionShort() != '1.5') {
			if (method_exists('JPluginHelper','getPlugin')) {
				$plugin = JPluginHelper::getPlugin('user', 'joomla');
				if ($plugin) {
					$data = new KStorage($plugin->params);
					$data->set('mail_to_user', 0);
					$plugin->params = $data->toString('json');
				}
			}
		}

		if (!$juser->save()) {
			/** @noinspection PhpDeprecationInspection */
			foreach ($juser->getErrors() as $error) {
				$this->setError($error);
			}
			return false;
		}

		$userObject = new stdClass();
		
		$userObject->id 		= $juser->get('id');
		$userObject->name 		= $juser->get('name');
		$userObject->username 	= $juser->get('username');
		$userObject->password 	= $juser->get('password');
		
		return $userObject;
		
	}
	
	protected function unsetErrors() {
		$this->errors = array();
	}
	
	protected function setError($error) {
		$this->errors[] = $error;
	}
	
	protected function setErrors($errors) {
		if (is_array($errors) && count($errors)) {
			$this->errors = array_merge((array)$this->errors,$errors);
		}
	}
	
	public function getErrors() {
		return $this->errors;
	}
	
	public function getError() {
		if (is_array($this->errors) && count($this->errors)) {
			return end($this->errors);
		}
		else {
			return '';
		}
	}
	
	public function isAuthorized($task, $userId = NULL, $minGroupId = NULL) {
		
		if ($this->getVersionShort() == 1.5) {
			
			if ($minGroupId) {
				return (KenedoPlatform::p()->getUserGroupId($userId) >= $minGroupId);
			}
			else {
				return false;
			}
		}
		
		$exp = explode('.',$task,2);
		$component = $exp[0];
		$task = $exp[1];
		
		if ($this->getVersionShort() <= 1.7) {
			/** @noinspection PhpDeprecationInspection */
			return (JFactory::getUser($userId)->authorize($task,$component) === true) ? true : false;
		}
		else {
			return (JFactory::getUser($userId)->authorise($task,$component) === true) ? true : false;
		}
		
	}
	
	public function passwordsMatch($passwordClear, $passwordEncrypted) {
		
		// We got an old Joomla version switch here
		// It's complicated..at some point within 2.5 Joomla added ::verifyPassword and the old way of password check was gone
		// So we go weird to check it
		if ($this->getVersionShort() < 2 || method_exists('JUserHelper', 'verifyPassword') == false) {
			jimport('joomla.user.helper');
			$exploded	= explode( ':', $passwordEncrypted );
			if (isset($exploded[1])) {
				$salt = $exploded[1];
			}
			else {
				$salt = '';
			}
			$crypt	= $exploded[0];
			/** @noinspection PhpDeprecationInspection */
			$test =  JUserHelper::getCryptedPassword($passwordClear, $salt);
			
			if ($test == $crypt) return true;
			else return false;
		}
		else {
			$match = JUserHelper::verifyPassword($passwordClear, $passwordEncrypted);
			return $match;
		}
		
	}
	
	public function passwordMeetsStandards($password) {
		
		if (mb_strlen($password) < 6) {
			return false;
		}
		
		/*
		if (mb_strlen($password) < 8) {
			return false;
		}
		if ( preg_match("/[0-9]/", $password) == 0 || preg_match("/[a-zA-Z]/", $password) == 0) {
			return false;
		}
		*/
		return true;
	}
	
	public function getPasswordStandardsText() {
		
		return KText::_('Your password should contain at least 6 characters.');
		
		//return KText::_('Your password should contain at least 8 characters and should contain numbers and letters.');
	}
	
	public function changeUserPassword($userId, $passwordClear) {
		
		$user = JUser::getInstance($userId);
		
		$data = $user->getProperties();
		$data['password'] = $passwordClear;
		$data['password2'] = $passwordClear;
		
		$user->bind($data);
		$success = $user->save(true);
		
		if ($success == false) {
			/** @noinspection PhpDeprecationInspection */
			KLog::log('Could not change user password for user ID "'.$userId.'". Error messages from platform are "'.var_export($user->getErrors(),true),'error');
			return false;
		}
		else {
			return true;
		}
		
	}
	
	public function getRootDirectory() {
		return JPATH_SITE;
	}
	
	public function getAppParameters() {
		
		$option = KRequest::getKeyword('option');
		$params = JComponentHelper::getParams($option);
		
		if ($this->getVersionShort() != 1.5) {
			/** @noinspection PhpUndefinedMethodInspection */
			$appParams = JFactory::getApplication()->getParams();
			$params->merge($appParams);
		}
		
		$obj = $params->toObject();
		
		$params = new KStorage();
		foreach ($obj as $key=>$value) {
			if ($key == 'show_page_title' or $key == 'show_page_heading') $key = 'show_page_heading';
			if ($key == 'page_title') $key = 'page_title';
			$params->set($key,$value);
		}
		return $params;
	}
	
	public function renderOutput(&$output) {
		echo $output;
		if (count($this->scriptDeclarations)) {
			?>
			<script type="text/javascript">
				<?php
				foreach ($this->scriptDeclarations as $js) {
					echo $js."\n";
				}
				?>
			</script>
			<?php
		}

	}
	
	public function startSession() {
		
		return true;
	}
	
	public function getPasswordResetLink() {
		if ($this->getVersionShort() == 1.5) {
			return KLink::getRoute('index.php?option=com_user&view=reset');
		}
		else {
			return KLink::getRoute('index.php?option=com_users&view=reset');
		}
	}
	
	public function getPlatformLoginLink() {
		if ($this->getVersionShort() == 1.5) {
			return KLink::getRoute('index.php?option=com_user&view=login');
		}
		else {
			return KLink::getRoute('index.php?option=com_users&view=login');
		}
	}
	
	
	public function getRoute($url, $encode = true, $secure = NULL) {
		if (strstr($url, 'option=com_cbcheckout')) {

			$log = 'Found a getRoute call for "'.$url.'". Most likely in a custom Configbox template or other customization. The value for parameter option should be replaced with com_cbcheckout (option=com_cbcheckout is now option=com_configbox).';
			$log .= ' We keep supporting the old link until version 2.7 only, please change the link as soon as you can.';
			KLog::logLegacyCall($log, 1);

			$url = str_replace('option=com_cbcheckout', 'option=com_configbox', $url);
		}
		return JRoute::_($url,$encode,$secure);
	}
	
	public function getActiveMenuItemId() {
		$activeItem	= JFactory::getApplication()->getMenu()->getActive();
		if ($activeItem) {
			return $activeItem->id;
		}
		else {
			return NULL;
		}
	}
	
	public function getLanguages() {
		
		if ($this->languages === NULL) {
			
			if ($this->getVersionShort() == 1.5) {
				
				$db = $this->getDb();
				
				$query = "SHOW TABLES LIKE '#__languages'";
				$db->setQuery($query);
				$hasJoomfish = $db->loadResult();
				
				if ($hasJoomfish) {
					$query = "SELECT `code` AS `tag`, `shortcode` AS `urlCode`, `name` AS `label` FROM `#__languages`";
					$db->setQuery($query);
					$this->languages = $db->loadObjectList('tag');
				}
				else {
					$tag = $this->getLanguageTag();
					$this->languages[$tag] = new KenedoObject();
					$this->languages[$tag]->tag = $tag;
					$this->languages[$tag]->urlCode = substr($tag,0,2);
					$this->languages[$tag]->label = JFactory::getLanguage()->getName();
				}
			}
			else {
				$db = $this->getDb();
				$query = "SELECT `lang_code` AS `tag`, `sef` AS `urlCode`, `title` AS `label` FROM `#__languages`";
				$db->setQuery($query);
				$this->languages = $db->loadObjectList('tag');
			}
			
		}
		
		return $this->languages;
	}
	
	public function platformUserEditFormIsReachable() {
		
		if ($this->getVersionShort() == 1.5) {
			return $this->isAdminArea();
		}
		else {
			return false;
		}
		
	}
	
	public function userCanEditPlatformUsers() {
		$user = JFactory::getUser();
		
		if ($this->getVersionShort() == 1.5) {
			return ($user->get('gid') > 18);
		}
		else {
			return $user->authorise('core.manage', 'com_users');
		}
		
	}
	
	public function getPlatformUserEditUrl($platformUserId) {
		
		if ($this->getVersionShort() == 1.5) {
			$link = KPATH_URL_BASE.'/administrator/index.php?option=com_users&view=user&task=edit&cid[]='.$platformUserId;
		}
		else {
			$link = KPATH_URL_BASE.'/administrator/index.php?option=com_users&view=user&layout=edit&id=106'.$platformUserId;
		}
		
		return $link;
		
	}

	public function getComponentDir($componentName) {
		return JPATH_SITE.DS.'components'.DS.strtolower($componentName);
	}

	public function getUrlAssets() {
		$path = $this->getUrlBase().'/components/com_configbox/assets';
		return $path;
	}

	public function getDirAssets() {
		$path = $this->getComponentDir('com_configbox').DS.'assets';
		return $path;
	}

	public function getDirCache() {
		// Not using JPATH_CACHE on purpose to avoid writing into the admin cache
		return JPATH_SITE.DS.'cache';
	}

	public function getDirCustomization() {
		$path = $this->getComponentDir('com_configbox').DS.'data'.DS.'customization';
		return $path;
	}

	public function getDirCustomizationAssets() {
		$path = $this->getComponentDir('com_configbox').DS.'data'.DS.'customization'.DS.'style_overrides';
		return $path;
	}

	public function getUrlCustomizationAssets() {
		$path = $this->getUrlBase().'/components/com_configbox/data/customization/style_overrides';
		return $path;
	}

	public function getDirCustomizationSettings() {
		$path = $this->getComponentDir('com_configbox').DS.'data'.DS.'settings';
		return $path;
	}

	public function getDirDataCustomer() {
		$path = $this->getComponentDir('com_configbox').DS.'data';
		return $path;
	}

	public function getUrlDataCustomer() {
		$path = $this->getUrlBase().'/components/com_configbox/data';
		return $path;
	}

	public function getDirDataStore() {
		$path = $this->getComponentDir('com_configbox').DS.'data';
		return $path;
	}

	public function getUrlDataStore() {
		$path = $this->getUrlBase().'/components/com_configbox/data';
		return $path;
	}

	public function getTemplateOverridePath($component, $viewName, $templateName) {
		$path = JPATH_SITE .DS. 'templates' .DS. $this->getTemplateName() .DS. 'html' .DS. $component .DS. $viewName .DS. $templateName.'.php';
		return $path;
	}

}