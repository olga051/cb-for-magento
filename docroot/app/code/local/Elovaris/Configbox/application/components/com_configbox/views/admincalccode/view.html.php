<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdmincalccode extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'admincalccodes';

	/**
	 * @return ConfigboxModelAdmincalccodes
	 * @throws Exception
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdmincalccodes');
	}

	function prepareTemplateVars() {
		
		$model 	= KenedoModel::getModel('ConfigboxModelAdmincalccodes');
		$id = KRequest::getInt('id');
		$record = $model->getRecord($id);
		$properties = $model->getProperties();

		$this->assignRef('record', $record);
		$this->assignRef('properties', $properties);
		
		$this->addViewCssClasses();
		
	}
	
}
