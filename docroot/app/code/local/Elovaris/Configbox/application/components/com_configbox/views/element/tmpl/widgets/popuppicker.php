<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewConfiguratorpage */
?>

<?php if ($this->element->picker_table) { ?>
	<span class="picked_value" id="picked-value-<?php echo (int)$this->element->id;?>"><?php echo hsc($this->element->getOutputValue());?></span>
	<a class="value-picker" href="<?php echo KLink::getRoute('index.php?option=com_configbox&view=picker&id='.$this->element->id.'&tmpl=component&target=element-'.$this->element->id.'&display_target=picked-value-'.$this->element->id);?>"><?php echo KText::_('Choose');?></a>
<?php } else { ?>
	<span><?php echo KText::_('No table set.');?></span>
<?php } ?>