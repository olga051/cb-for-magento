<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCart */
?>
<div id="com_configbox">
	<div id="view-cart">
		<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

			<?php if ($this->showPageHeading) { ?>
				<h1 class="page-title page-title-cart"><?php echo hsc($this->pageHeading);?></h1>
			<?php } ?>

			<?php $this->renderView('positions'); ?>

			<?php $this->renderView('summary'); ?>

			<?php $this->renderView('cartbuttons'); ?>

		</div>
	</div>
</div>
