<?php
class KenedoProperty {

	/**
	 * @var string $propertyName Name of the property as part of the type it is currently used
	 */
	public $propertyName;

	/**
	 * An array of arrays holding all information about a property. Written down in each Type.
	 * @see ConfigboxTypeProduct
	 * @var array $propertyDefinition
	 */
	protected $propertyDefinition = array();

	/**
	 * Data set by the Model (holds the data of all properties of the type)
	 * @var object $data
	 */
	protected $data;

	/**
	 * @var KenedoModel $model A reference to the model the property belongs to
	 */
	protected $model;

	/**
	 * @var string[] $cssClasses CSS classes used in the property's wrapping div when shown as form item
	 */
	protected $cssClasses = array();

	/**
	 * @var string $cssId CSS ID used in the property's wrapping div when shown as form item
	 */
	protected $cssId = '';

	/**
	 * @see KenedoProperty::getErrors, KenedoProperty::getError, KenedoProperty::setErrors, KenedoProperty::setError
	 * @var string[] $errors Array of strings with error messages
	 * @var KenedoModel $model The model the property belongs to
	 */
	protected $errors = array();

	function __construct($propertyDefinition, KenedoModel $model) {

		$this->propertyName = $propertyDefinition['name'];
		$this->propertyDefinition = $propertyDefinition;
		$this->model = $model;

		if (!isset($this->propertyDefinition['required'])) {
			$this->propertyDefinition['required'] = 0;
		}
		
		if (!isset($this->propertyDefinition['invisible'])) {
			$this->propertyDefinition['invisible'] = false;
		}
		
		$this->cssId = 'property-name-'.$this->propertyDefinition['name'];

		$this->cssClasses[] = 'property-name-'.$this->propertyDefinition['name'];
		$this->cssClasses[] = 'kenedo-property';
		$this->cssClasses[] = 'property-type-'.$this->propertyDefinition['type'];
		
		if ($this->isRequired()) {
			$this->cssClasses[] = 'required';
		}
		
		if ($this->isVisible() == false) {
			$this->cssClasses[] = 'invisible-field';
		}
		
		if (isset($this->propertyDefinition['options'])) {
			$optionsArray = explode(' ',$this->propertyDefinition['options']);
			foreach ($optionsArray as $fieldOption) {
				$this->propertyDefinition['optionTags'][$fieldOption] = true;
			}
		}
		else {
			$this->propertyDefinition['optionTags'] = array();
		}
		
	}

	function getType() {
		return $this->getPropertyDefinition('type', '');
	}
	
	function setData( $data ) {
		$this->data = $data;
	}
	
	function getDataFromRequest( &$data ) {

		if (KRequest::getVar($this->propertyName, NULL) === NULL) {
			$data->{$this->propertyName} = NULL;
		}
		else {
			
			if (isset($this->propertyDefinition['optionTags']['ALLOW_RAW'])) {
				$data->{$this->propertyName} = KRequest::getVar($this->propertyName, '', 'METHOD');
				$data->{$this->propertyName} = stripslashes($data->{$this->propertyName});
			}
			elseif (isset($this->propertyDefinition['optionTags']['ALLOW_HTML'])) {
				$data->{$this->propertyName} = KRequest::getHtml($this->propertyName, '');
			}
			else {
				$data->{$this->propertyName} = KRequest::getString($this->propertyName, '');
			}
		}
		
	}

	function prepare( &$data ) {
		return true;
	}

	function check( $data ) {

		$this->resetErrors();

		if ($this->isRequired()) {

			if (empty($data->{$this->propertyName}) && $data->{$this->propertyName} !== '0') {
				$this->setError(KText::sprintf('Field %s cannot be empty.', $this->propertyDefinition['label']));
				return false;
			}
		}
		
		return true;
	}
	
	function store(&$data) {
		return true;
	}
	
	function delete ( $id, $tableName ) {
		return true;
	}
	
	function getLabelAdmin() {
		
		if (isset($this->propertyDefinition['tooltip'])) {
			return KenedoHtml::getTooltip(hsc($this->propertyDefinition['label']), $this->propertyDefinition['tooltip']);
		}
		else {
			return hsc(isset($this->propertyDefinition['label']) ? $this->propertyDefinition['label'] : '');
		}
	}
	
	function renderCssClasses() {
		return implode(' ',$this->cssClasses);
	}
	
	function getCssId() {
		return $this->cssId;
	}
	
	function getName() {
		return $this->propertyName;
	}
	
	function getPropertyDefinition($key = NULL, $default = NULL) {

		if ($key) {
			if (isset($this->propertyDefinition[$key])) {
				return $this->propertyDefinition[$key];
			}
			else {
				return $default;
			}
		}
		else {
			return $this->propertyDefinition;
		}
	}
	
	function getListingWidth() {
		if (!empty($this->propertyDefinition['listingwidth'])) {
			return $this->propertyDefinition['listingwidth'];
		}
		else {
			return 'auto';
		}
	}
	
	function renderListingField($item, $items) {
		
		if ($this->getPropertyDefinition('listinglink')) {

			$option = $this->getPropertyDefinition('component');
			$controller = $this->getPropertyDefinition('controller');

			$returnUrl = KLink::base64UrlEncode(KLink::getRoute( 'index.php?option='.hsc($option).'&controller='.hsc($controller), false ));
			$href = KLink::getRoute('index.php?option='.hsc($option).'&controller='.hsc($controller).'&task=edit&id='.intval($item->id).'return='.$returnUrl);

			$linkText = (!empty($item->{$this->propertyName})) ? $item->{$this->propertyName} : KText::_('No Name');
			?>
			<a class="listing-link" href="<?php echo $href;?>"><?php echo hsc($linkText);?></a>
			<?php
		}
		else {
			echo hsc($item->{$this->propertyName});
		}
		
	}
	
	function renderListingHeading($ordering) {

		// Props without an order def simply have their label shown
		if ($this->getPropertyDefinition('order', '') == '') {
			echo hsc($this->getPropertyDefinition('label'));
			return;
		}

		// The key prop get's a checkbox that makes all records selected
		if ($this->model->getTableKey() == $this->propertyName) {
			?>
			<input type="checkbox" name="checkall" class="kenedo-check-all-items" />
			<?php
		}

		echo KenedoHtml::getListingOrderHeading($this->propertyName, $this->getPropertyDefinition('label'), $ordering);

		// Ordering props get a special link-button that triggers storing the user's ordering
		if ($this->getType() == 'ordering') {
			?>
			<a class="link-save-item-ordering" title="<?php echo KText::_('Save ordering');?>">
				<span class="fa fa-floppy-o"></span>
			</a>
			<?php
		}
			
	}
	
	function getBodyAdmin() {
		
		$templateFileAdmin = $this->getAdminTemplateFile();

		if ($templateFileAdmin) {
			ob_start();
			include($templateFileAdmin);
			return ob_get_clean();
		}
		else {
			return NULL;
		}
		
	}
	
	function getLabelDisplay() {	
		return '';
	}
	
	function getBodyDisplay() {
		return '';
	}
	
	function isRequired() {
		return (!empty($this->propertyDefinition['required']) && $this->propertyDefinition['required'] == 1);
	}
	
	function isVisible() {
		return ($this->propertyDefinition['invisible'] == 0);
	}
	
	function isInListing() {
		return ( !empty($this->propertyDefinition['listing']));
	}
	
	function getListingPosition() {
		return isset($this->propertyDefinition['listing']) ? $this->propertyDefinition['listing'] : 0;
	}
	
	function usesWrapper() {
		return true;
	}
	
	function doesShowAdminLabel() {
		$return = ($this->getPropertyDefinition('hideAdminLabel',0) == false);
		return $return;
	}

	protected function getAdminTemplateFile() {

		$regularFolder = CONFIGBOX_DIR_PROPERTIES_DEFAULT.DS.'tmpl';
		$customFolder = CONFIGBOX_DIR_PROPERTIES_CUSTOM.DS.'tmpl';
	
		$filename = strtolower( $this->propertyDefinition['type'] ).'.php';

		// Custom folder
		if ( file_exists($customFolder .DS. $filename) ) {
			$templateFile = $customFolder .DS. $filename;
		}
		// Regular folder
		elseif ( file_exists($regularFolder .DS. $filename) ) {
			$templateFile = $regularFolder .DS. $filename;
		}
		// If needed, deal with templates not found (currently fields like checked_out_time have no display)
		else {
			return NULL;
		}
		
		return $templateFile;
	
	}

	/**
	 * Returns an array of strings with SQL query text for the field list (e.g. table.column as alias).
	 * Used for KenedoModel::getRecord and getRecords.
	 *
	 * @see KenedoModel::getRecord(), KenedoModel::getRecords()
	 *
	 * @param string $selectAliasPrefix Prefix to use on the select alias of the selected column
	 * @param string $selectAliasOverride Used to force the supplied select alias (prefix will be taken into account)
	 * @return string[]
	 */
	public function getSelectsForGetRecord($selectAliasPrefix = '', $selectAliasOverride = '') {
		$selectAlias = ($selectAliasOverride) ? $selectAliasPrefix.$selectAliasOverride : $selectAliasPrefix.$this->getSelectAlias();
		$select = $this->getTableAlias().'.'.$this->getTableColumnName().' AS `'.$selectAlias.'`';
		return array($select);
	}

	/**
	 * Returns an array of strings with SQL for JOIN statements to be added to the KenedoModel::getRecord() or
	 * KenedoModel::getRecords() SQL query.
	 * @see KenedoModel::getRecord(), KenedoModel::getRecords()
	 * @return string[]
	 */
	public function getJoinsForGetRecord() {
		return array();
	}

	/**
	 * Used to append any kind of data after KenedoModel::getRecord or ::getRecords is done fetching from the DB.
	 *
	 * @see KenedoModel::getRecord(), KenedoModel::getRecords()
	 * @param object $data
	 */
	public function appendDataForGetRecord(&$data) {
		return;
	}

	/**
	 * Returns the SQL field alias for that property. This will affect how the property will be named in the record
	 * object.
	 * @return string
	 */
	public function getSelectAlias() {
		return $this->propertyName;
	}

	/**
	 * Returns the column name that this property refers to.
	 * @return string
	 */
	public function getTableColumnName() {
		return $this->propertyName;
	}

	/**
	 * Returns the table alias to be used in possible JOIN statements for that property.
	 * @return string
	 */
	public function getTableAlias() {
		return $this->model->getModelName();
	}

	/**
	 * Returns the filter name (or names) of that property. A filter name is simply the table and column name
	 * (table.col), exactly as you would reference to that field in the getRecord query.
	 *
	 * @return string|string[]
	 */
	public function getFilterName() {

		if ($this->getPropertyDefinition('filter', false) == false && $this->getPropertyDefinition('search', false) == false) {
			return '';
		}

		return $this->getTableAlias().'.'.$this->getTableColumnName();

	}

	/**
	 * Returns the filter name (or names) as it needs to be for GET/POST data (PHP changes dots to underscores, so
	 * $ is used as placeholder for the dot.
	 * @return string|string[] String or array of strings, depending on what KenedoProperty::getFilterName does
	 */
	public function getFilterNameRequest() {

		$filterNames = $this->getFilterName();
		if (is_array($filterNames) == false) {
			$filterNames = array($filterNames);
		}
		$requestFilterNames = array();
		foreach ($filterNames as $filterName) {
			$requestFilterNames[] = 'filter_'.str_replace('.', '$', $filterName);
		}
		if (count($requestFilterNames) == 1) {
			$requestFilterNames = $requestFilterNames[0];
		}
		return $requestFilterNames;

	}
	
	public function resetErrors() {
		$this->errors = array();
	}
	
	protected function setError($msg) {
		$this->errors[] = $msg;
	}
	
	public function getErrors() {
		return $this->errors;
	}
	
	public function getError() {
		if (count($this->errors)) {
			return end($this->errors);
		}
		else {
			return NULL;
		}
	}

	public function getFilterInput(KenedoView $view, $filters) {

		if (!$this->getPropertyDefinition('search') && !$this->getPropertyDefinition('filter')) {
			return '';
		}

		$filterName = $this->getFilterName();
		$filterNameRequest = $this->getFilterNameRequest();
		$filterNameHtml = str_replace('.', '_', $filterName);
		$chosenValue = !empty($filters[$filterName]) ? $filters[$filterName] : NULL;

		$html = '';

		if ($this->getPropertyDefinition('search')) {
			ob_start();
			?>
			<input
				class="listing-filter"
				placeholder="<?php echo KText::sprintf('Filter by %s', $this->getPropertyDefinition('label'));?>"
				type="text" value="<?php echo hsc($chosenValue); ?>"
				name="<?php echo hsc($filterNameRequest);?>"
				id="<?php echo hsc($filterNameHtml);?>" />

			<a class="kenedo-search backend-button-small"><?php echo KText::_('Filter');?></a>
			<?php
			$html = ob_get_clean();
		}
		elseif ($this->getPropertyDefinition('filter')) {
			$options = $this->getPossibleFilterValues();
			$html = KenedoHtml::getSelectField($filterNameRequest, $options, $chosenValue, 'all', false, 'listing-filter', $filterNameHtml);
		}

		return $html;

	}

	protected function getPossibleFilterValues() {

		// Get all records for that model
		$records = $this->model->getRecords();

		// Prepare the options array, set default value
		$options = array();
		$options['all'] = KText::sprintf('No %s filter', $this->getPropertyDefinition('label'));

		$groupKey = $this->getPropertyDefinition('groupby');

		// Group if necessary
		if ($this->getPropertyDefinition('groupby')) {

			$groupedRecords = array();
			foreach ($records as $record) {
				$groupedRecords[$record->{$groupKey}][] = $record;
			}
			$records = $groupedRecords;
		}

		// Extract the infos for the select field
		foreach ($records as $record) {
			if (!is_array($record)) {

				$value = $record->{$this->getSelectAlias()};

				// Make values nice for yes/no fields
				if (in_array($this->getType(), array('boolean', 'published', 'checkbox') ) ) {
					$value = ($value) ? KText::_('CBYES') : KText::_('CBNO');
				}
				// Get the items/radios for radio and dropdown types
				if (in_array($this->getType(), array('radio', 'dropdown') ) ) {
					$map = array_merge($this->getPropertyDefinition('items', array()), $this->getPropertyDefinition('radios', array()));
					$value = $map[$value];
				}
				// Joins do their funny business
				if (in_array($this->getType(), array('join') ) ) {
					$value = $record->{$this->propertyName.'_display_value'};
				}

				$options[$record->{$this->model->getTableKey()}] = $value;
			}
			else {

				foreach ($record as $groupedRecord) {

					$value = $groupedRecord->{$this->getSelectAlias()};

					// Make values nice for yes/no fields
					if (in_array($this->getType(), array('boolean', 'published', 'checkbox') ) ) {
						$value = ($value) ? KText::_('CBYES') : KText::_('CBNO');
					}
					// Get the items/radios for radio and dropdown types
					if (in_array($this->getType(), array('radio', 'dropdown') ) ) {
						$map = array_merge($this->getPropertyDefinition('items', array()), $this->getPropertyDefinition('radios', array()));
						$value = $map[$value];
					}
					// Joins do their funny business
					if (in_array($this->getType(), array('join') ) ) {
						$value = $record->{$this->propertyName.'_display_value'};
					}

					$options[$groupedRecord->{$groupKey}][$groupedRecord->{$this->model->getTableKey()}] = $value;

				}
			}
		}

		return $options;

	}
}