<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyJoin extends KenedoProperty {
	
	function check( $data ) {
		$this->resetErrors();
		if ($this->isRequired()) {
			if (empty($data->{$this->propertyName})) {
				$this->setError(KText::sprintf('Field %s cannot be empty.', $this->propertyDefinition['label']));
				return false;
			}
		}

		return true;
	}
	
	function getParentModelRecords() {
		
		$joinModel = KenedoModel::getModel($this->propertyDefinition['modelClass']);

		// Prepare the method to get the joined data
		$method = $this->propertyDefinition['modelMethod'];
		
		// Check if the method specified exists then call it
		if (method_exists($joinModel, $method)) {
			$items = $joinModel->$method();
		}
		else {
			return false;
		}

		return $items;
		
	}
	
	function groupItems(&$items, $groupKey) {
		
		$groupedItems = array();
		
		foreach ($items as &$item) {
			$groupedItems[$item->{$groupKey}][] = $item;			
		}
		
		return $groupedItems;
		
	}

	function renderListingField($item, $items) {
		
		if (isset($item->{$this->propertyName.'_display_value'})) {
			$value = $item->{$this->propertyName.'_display_value'};
		}
		else {
			$value = $item->{$this->propertyName};
		}

		if ($this->getPropertyDefinition('listinglink')) {

			$option = $this->getPropertyDefinition('component');
			$controller = $this->getPropertyDefinition('controller');

			$returnUrl = KLink::base64UrlEncode(KLink::getRoute( 'index.php?option='.hsc($option).'&controller='.hsc($controller), false ));
			$href = KLink::getRoute('index.php?option='.hsc($option).'&controller='.hsc($controller).'&task=edit&id='.intval($item->id).'&return='.$returnUrl);

			?>
			<a class="listing-link" href="<?php echo $href;?>"><?php echo hsc($value);?></a>
		<?php
		}
		else {
			echo hsc($value);
		}

	}

	/**
	 * The join property adds its column to the selects and in case the 'parent' def is 1, it does more:
	 *
	 * 1) Any join has a def called 'propNameDisplay' which is the joined table's property that names the record
	 * (e.g. a join for the product_id has the product's title prop name as 'propNameDisplay'). It adds this to the
	 * field list with a special alias, so we can show the joined record's title more easily in listings.
	 *
	 * 2) With the def 'joinAdditionalProps', more props of the joined table will be added. 'joinAdditionalProps' is
	 * an array of arrays with 2 key/value pairs. 'propertyName' and an optional 'selectAliasOverride'. First is the
	 * prop's name, the second forces the name of the select alias (prefix will still be in the name).
	 *
	 * 3) It will add all join props of the parents tables recursively, but only goes deeper if the 'parent' def is on.
	 *
	 * @param string $selectAliasPrefix
	 * @param string $selectAliasOverride
	 * @return array|string[]
	 * @throws Exception
	 */
	public function getSelectsForGetRecord($selectAliasPrefix = '', $selectAliasOverride = '') {

		$selects = parent::getSelectsForGetRecord($selectAliasPrefix, $selectAliasOverride);

		if ($this->getPropertyDefinition('parent', 0) == 0) {
			return $selects;
		}

		$parentModel = KenedoModel::getModel($this->getPropertyDefinition('modelClass'));

		$parentProps = $parentModel->getProperties();

		if ($this->getPropertyDefinition('propNameDisplay')) {
			$propName = $this->getPropertyDefinition('propNameDisplay');
			$joinDisplayProp = $parentProps[$propName];
			if ($selectAliasOverride) {
				$override = $selectAliasPrefix.$selectAliasOverride.'_display_value';
			}
			else {
				$override = $selectAliasPrefix.$this->propertyName.'_display_value';
			}
			$joinDisplayPropSelects = $joinDisplayProp->getSelectsForGetRecord('', $override);
			$selects = array_merge($selects, $joinDisplayPropSelects);
		}

		if ($this->getPropertyDefinition('joinAdditionalProps')) {
			$addProps = $this->getPropertyDefinition('joinAdditionalProps');
			foreach ($addProps as $addProp) {
				// If there is an alias defined
				if (!empty($addProp['selectAliasOverride'])) {
					$prefix = $selectAliasPrefix;
					$override = $addProp['selectAliasOverride'];
				}
				else {
					$prefix = $selectAliasPrefix .'joinedby_'.$this->propertyName.'_to_'.$parentModel->getModelName().'_';
					$override = '';
				}
				$additionalSelects = $parentProps[$addProp['propertyName']]->getSelectsForGetRecord($prefix, $override);
				$selects = array_merge($selects, $additionalSelects);
			}
		}

		foreach ($parentProps as $parentProp) {

			if ($parentProp->getPropertyDefinition('type') == 'join') {
				$prefix = $selectAliasPrefix . 'joinedby_'.$this->propertyName.'_to_'.$parentModel->getModelName().'_';
				$parentSelects = $parentProp->getSelectsForGetRecord($prefix);
				$selects = array_merge($selects, $parentSelects);
			}
		}

		return $selects;


	}

	/**
	 * If the 'parent' def is on, it recurses into parent models and adds joins for certain props of that model. See
	 * infos for KenedoPropertyJoin::getSelectsForGetRecord() for details.
	 * @see KenedoPropertyJoin::getSelectsForGetRecord()
	 * @return string[]
	 */
	public function getJoinsForGetRecord() {

		if ($this->getPropertyDefinition('parent', 0) == 0) {
			return parent::getJoinsForGetRecord();
		}

		$joins = array();

		// Get some info about the parent model
		$parentModel = KenedoModel::getModel($this->getPropertyDefinition('modelClass'));
		$parentKeyField = $this->getPropertyDefinition('propNameKey');
		$parentProps = $parentModel->getProperties();
		$parentTableAlias = $parentProps[$parentKeyField]->getTableAlias();

		// Do the join statement for the parent model's table
		$joins[$parentTableAlias] = "LEFT JOIN `".$parentModel->getTableName()."` AS `".$parentTableAlias."`
					ON `".$parentTableAlias."`.`".$this->getPropertyDefinition('propNameKey')."` = `".$this->getTableAlias()."`.`".$this->getTableColumnName()."`";

		// Get join statements for the prop that deals with displaying the prop's value
		$parentDisplayPropName = $this->getPropertyDefinition('propNameDisplay');
		$parentPropJoins = $parentProps[$parentDisplayPropName]->getJoinsForGetRecord();
		$joins = array_merge($joins, $parentPropJoins);

		// Go through the additional props that shall be in the record
		if ($this->getPropertyDefinition('joinAdditionalProps')) {
			$addProps = $this->getPropertyDefinition('joinAdditionalProps');
			foreach ($addProps as $addProp) {
				$additionalJoins = $parentProps[$addProp['propertyName']]->getJoinsForGetRecord();
				$joins = array_merge($joins, $additionalJoins);
			}
		}

		// Join any parent props that are joins (This will go all the way up recursively, the check for the parent def
		// happens at the start of the method.
		foreach ($parentProps as $parentProp) {
			if ($parentProp->getPropertyDefinition('type') == 'join') {
				$parentJoins = $parentProp->getJoinsForGetRecord();
				$joins = array_merge($joins, $parentJoins);
			}

			if ($parentProp->getPropertyDefinition('type') == 'multiselect' && $parentProp->getPropertyDefinition('filter')) {
				$parentJoins = $parentProp->getJoinsForGetRecord();
				$joins = array_merge($joins, $parentJoins);
			}
		}

		return $joins;

	}

	/**
	 * @return string|string[]
	 */
	public function getFilterName() {

		if ($this->getPropertyDefinition('filter', false) == false && $this->getPropertyDefinition('search', false) == false) {
			return '';
		}

		$filterNames = array();

		if ($this->getPropertyDefinition('filter')) {
			$filterNames[] = $this->model->getModelName().'.'.$this->propertyName;
		}

		if ($this->getPropertyDefinition('filterparents')) {
			$parentModel = KenedoModel::getModel($this->getPropertyDefinition('modelClass'));
			$parentProps = $parentModel->getProperties();
			foreach ($parentProps as $parentProp) {
				$parentFilterName = $parentProp->getFilterName();
				if (is_array($parentFilterName)) {
					$filterNames = array_merge($filterNames, $parentFilterName);
				}
				elseif(!empty($parentFilterName)) {
					$filterNames[] = $parentFilterName;
				}

			}
		}

		return $filterNames;

	}

	public function getFilterInput(KenedoView $view, $filters) {

		if (!$this->getPropertyDefinition('search') && !$this->getPropertyDefinition('filter')) {
			return '';
		}

		$filterInputs = array();

		$filterNames = $this->getFilterName();
		$filterName = $filterNames[0];
		$filterNameRequest = $this->getFilterNameRequest();
		$filterHtmlName = str_replace('.', '_', $filterName);

		if (is_array($filterNameRequest)) {
			$filterNameRequest = $filterNameRequest[0];
		}

		$options = $this->getPossibleFilterValues();
		$chosenValue = !empty($filters[$filterName]) ? $filters[$filterName] : NULL;

		$html = KenedoHtml::getSelectField($filterNameRequest, $options, $chosenValue, 'all', false, 'listing-filter', $filterHtmlName);

		$filterInputs[$this->getTableAlias().'.'.$this->getTableColumnName()] = $html;

		if ($this->getPropertyDefinition('filterparents')) {

			$parentModel = KenedoModel::getModel($this->getPropertyDefinition('modelClass'));
			$parentProps = $parentModel->getProperties();
			foreach ($parentProps as $parentProp) {
				$parentFilterInputs = $parentProp->getFilterInput($view, $filters);
				if (is_array($parentFilterInputs)) {
					$filterInputs = array_merge($filterInputs, $parentFilterInputs);
				}
				elseif(!empty($parentFilterInputs)) {
					$filterInputs[$parentProp->getTableAlias().'.'.$parentProp->getTableColumnName()] = $parentFilterInputs;
				}

			}

		}

		return $filterInputs;

	}

	protected function getPossibleFilterValues() {

		$joinModel = KenedoModel::getModel($this->getPropertyDefinition('modelClass'));
		$method = $this->getPropertyDefinition('modelMethod');
		$records = $joinModel->$method();

		// Group if necessary
		if ($this->getPropertyDefinition('groupby')) {
			$groupKey = $this->getPropertyDefinition('groupby');
			$groupedRecords = array();
			foreach ($records as $record) {
				$groupedRecords[$record->{$groupKey}][] = $record;
			}
			$records = $groupedRecords;
		}

		// Make an array of select options
		$options = array();
		$options['all'] = KText::sprintf('No %s filter', $this->getPropertyDefinition('label'));

		// For convenience
		$keyNameParent = $this->getPropertyDefinition('propNameKey');
		$displayKey = $this->getPropertyDefinition('propNameDisplay');

		// Loop through the records and populate the options array
		foreach ($records as $record) {
			// Grouped records
			if (is_array($record)) {
				foreach ($record as $groupedRecord) {
					$options[$groupedRecord->{$groupKey}][$groupedRecord->{$keyNameParent}] = $groupedRecord->{$displayKey};
				}
			}
			// Normal records
			else {
				$options[$record->{$keyNameParent}] = $record->{$displayKey};
			}
		}

		return $options;

	}

}