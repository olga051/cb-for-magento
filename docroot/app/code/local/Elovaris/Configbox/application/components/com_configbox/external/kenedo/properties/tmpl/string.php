<?php 
defined('CB_VALID_ENTRY') or die();
/**
 * @var $this KenedoPropertyString
 */
$value = (isset($this->data->{$this->propertyName})) ? $this->data->{$this->propertyName} : '';

$style = $this->getPropertyDefinition('style');
$size = $this->getPropertyDefinition('size');

$styleAttribute = ($style) ? 'style="'.$style.'"' : '';
$sizeAttribute = ($size) ? 'maxlength="'.intval($size).'"' : '';

$stringType = $this->getPropertyDefinition('stringType', 'string');

if ($stringType == 'number' || $stringType == 'price') {
	
	if ($value !== '') {
		$value = floatval($value);
	}
	
	if ( (empty($value) && $value !== 0) && $this->getPropertyDefinition('default')) {
		$value = $this->getPropertyDefinition('default');
	}
}
else {
	if (empty($value) && $this->getPropertyDefinition('default')) {
		$value = $this->getPropertyDefinition('default');
	}
}

// For numeric kinds of strings, replace the dot decimal symbol with the localized one
if ($stringType == 'number' or $stringType == 'price' or $stringType == 'time') {
	$value = str_replace('.', KText::$decimalSymbol, $value);
}

if ($stringType == 'time') {

	if ($this->getPropertyDefinition('displayin')) {

		switch($this->getPropertyDefinition('displayin')) {
			case 'days':
				$value /= 86400;
				break;
			case 'hours':
				$value /= 3600;
				break;
			case 'minutes':
				$value /= 60;
				break;
		}
	}
		
}

?>
<div class="string-type-<?php echo hsc($stringType);?>">
	<?php
	$tags = $this->getPropertyDefinition('optionTags');
	if (isset($tags['USE_TEXTAREA'])) {
		?>
		<textarea class="text_area" name="<?php echo $this->propertyName;?>" id="<?php echo $this->propertyName;?>" <?php echo $styleAttribute;?>><?php echo hsc($value);?></textarea>
		<?php
	}
	else {
		?>
		<input type="text" name="<?php echo $this->propertyName;?>" id="<?php echo $this->propertyName;?>" value="<?php echo hsc($value);?>" <?php echo $styleAttribute. ' '.$sizeAttribute;?> />
		<?php
	}

	if ($this->getPropertyDefinition('unit')) {
		?>
		<span class="unit"><?php echo hsc($this->getPropertyDefinition('unit'));?></span>
		<?php
	}
	?>
</div>
