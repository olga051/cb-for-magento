<?php 
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewUser */
?>
<div id="com_configbox">
<div id="view-user">
<div id="layout-default">
		
<h1 class="componentheading"><?php echo KText::_('Your Account')?></h1>

<?php if ($this->isTemporaryAccount) { ?>
	<div class="account-temporary-notice">
		<p><?php echo KText::_('Your account is temporary, when you provide your address, we set you up with a permanent customer account and send your password via email.');?></p>
		<p><?php echo Ktext::_('If you already have an account, please login to see your account information and order history.');?></p>
	</div>
<?php } ?>

<div id="addressinfo">

	<fieldset id="billinginfo">
		<legend><?php echo KText::_('Billing Address');?></legend>
		
		<a rel="nofollow" href="<?php echo KLink::getRoute('index.php?view=user&layout=editprofile', true, CONFIGBOX_SECURECHECKOUT);?>" class="edit-profile trigger-change-address"><?php echo KText::_('Change');?></a>
		
		<table>
			<tr>
				<td class="key"><?php echo KText::_('Company');?>:</td>
				<td><?php echo hsc($this->customer->billingcompanyname);?></td>
			</tr>
			
			<tr>
				<td class="key"><?php echo KText::_('First Name');?>:</td>
				<td><?php echo hsc($this->customer->billingfirstname);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Last Name');?>:</td>
				<td><?php echo hsc($this->customer->billinglastname);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Address 1');?>:</td>
				<td><?php echo hsc($this->customer->billingaddress1);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Address 2');?>:</td>
				<td><?php echo hsc($this->customer->billingaddress2);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('ZIP Code');?>:</td>
				<td><?php echo hsc($this->customer->billingzipcode);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('City');?>:</td>
				<td><?php echo hsc($this->customer->billingcity);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Country');?>:</td>
				<td><?php echo hsc($this->customer->billingcountryname);?></td>
			</tr>
			<?php if ($this->customer->billingstate) { ?>
			<tr>
				<td class="key"><?php echo KText::_('State');?>:</td>
				<td><?php echo hsc($this->customer->billingstatename);?></td>
			</tr>
			<?php } ?>
			<tr>
				<td class="key"><?php echo KText::_('Email');?>:</td>
				<td><?php echo hsc($this->customer->billingemail);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Phone');?>:</td>
				<td><?php echo hsc($this->customer->billingphone);?></td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset id="deliveryinfo">
		<legend><?php echo KText::_('Shipping Address');?></legend>
		
		<a rel="nofollow" href="<?php echo KLink::getRoute('index.php?view=user&layout=editprofile', true, CONFIGBOX_SECURECHECKOUT);?>" class="edit-profile trigger-change-address"><?php echo KText::_('Change');?></a>
		
		<table>
			<tr>
				<td class="key"><?php echo KText::_('Company');?>:</td>
				<td><?php echo hsc($this->customer->companyname);?></td>
			</tr>
			
			<tr>
				<td class="key"><?php echo KText::_('First Name');?>:</td>
				<td><?php echo hsc($this->customer->firstname);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Last Name');?>:</td>
				<td><?php echo hsc($this->customer->lastname);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Address 1');?>:</td>
				<td><?php echo hsc($this->customer->address1);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Address 2');?>:</td>
				<td><?php echo hsc($this->customer->address2);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('ZIP Code');?>:</td>
				<td><?php echo hsc($this->customer->zipcode);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('City');?>:</td>
				<td><?php echo hsc($this->customer->city);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Country');?>:</td>
				<td><?php echo hsc($this->customer->countryname);?></td>
			</tr>
			<?php if ($this->customer->state) { ?>
			<tr>
				<td class="key"><?php echo KText::_('State');?>:</td>
				<td><?php echo hsc($this->customer->statename);?></td>
			</tr>
			<?php } ?>
			<tr>
				<td class="key"><?php echo KText::_('Email');?>:</td>
				<td><?php echo hsc($this->customer->email);?></td>
			</tr>
			<tr>
				<td class="key"><?php echo KText::_('Phone');?>:</td>
				<td><?php echo hsc($this->customer->phone);?></td>
			</tr>
		</table>
	</fieldset>
	
	<div class="clear"></div>
	
</div>

<?php if (count($this->orderRecords)) { ?>
	<fieldset id="orderhistory">
		<legend><?php echo KText::_('Order History');?></legend>
		
		<table class="table-orders">
			<tr>
				<th class="orders-id"><?php echo KText::_('Order ID');?></th>
				<th class="orders-date"><?php echo KText::_('Date');?></th>
				<?php if (ConfigboxPermissionHelper::canSeePricing()) { ?>
					<th class="orders-total"><?php echo KText::_('Total');?></th>
				<?php } ?>
				<th class="orders-status"><?php echo KText::_('Status');?></th>
				<th class="orders-actions"><?php echo KText::_('Actions');?></th>
			</tr>
			
			<?php foreach ($this->orderRecords as $orderRecord) { ?>
				<tr class="order-status-<?php echo intval($orderRecord->status);?>">
					<td class="orders-id"><?php echo (int)$orderRecord->id;?></td>
					<td class="orders-date"><?php echo hsc( KenedoTimeHelper::getFormatted($orderRecord->created_on) );?></td>
					<?php if (ConfigboxPermissionHelper::canSeePricing()) { ?>
						<td class="orders-total"><?php echo cbprice($orderRecord->payableAmount);?></td>
					<?php } ?>
					<td class="orders-status"><?php echo hsc($orderRecord->statusString);?></td>
					<td class="orders-actions">
						
						<?php if ($orderRecord->toUserOrders) { ?>
							<a href="<?php echo KLink::getRoute('index.php?option=com_configbox&view=userorder&order_id='.(int)$orderRecord->id);?>"><?php echo KText::_('Display')?></a>
						<?php } else { ?>
							<a href="<?php echo KLink::getRoute('index.php?option=com_configbox&view=cart&cart_id='.(int)$orderRecord->cart_id);?>"><?php echo KText::_('Display')?></a>
						<?php } ?>
						
						<?php if (ConfigboxOrderHelper::isPermittedAction('removeOrderRecord', $orderRecord)) { ?>
							<a href="<?php echo KLink::getRoute('index.php?option=com_configbox&view=user&task=removeOrder&cid[]='.(int)$orderRecord->id,true);?>"><?php echo KText::_('Remove')?></a>
						<?php } ?>
						
						<?php if (CONFIGBOX_ENABLE_INVOICING && $orderRecord->invoice_released) { ?>
							<span class="download-invoice">
								<a href="<?php echo KLink::getRoute('index.php?controller=invoice&format=raw&order_id='.(int)$orderRecord->id,true,CONFIGBOX_SECURECHECKOUT);?>"><?php echo KText::_('Download Invoice')?></a>
							</span>
						<?php } ?>
					</td>
				</tr>
			<?php } ?>
		</table>
	</fieldset>
<?php } ?>

</div>
</div>
</div>