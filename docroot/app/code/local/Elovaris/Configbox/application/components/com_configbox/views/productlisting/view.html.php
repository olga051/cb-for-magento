<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewProductlisting extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'productlisting';

	/**
	 * @var object $listing Product listing information
	 */
	public $listing;

	/**
	 * @var object[] $products Array holding objects with product data
	 */
	public $products;

	/**
	 * @var boolean $showPageHeading Whether to show the page heading or not
	 */
	public $showPageHeading;

	/**
	 * @var string $pageHeading The text of the page heading
	 */
	public $pageHeading;

	/**
	 * @var KStorage $params Object holding Joomla parameters for that page
	 */
	public $params;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	/**
	 * Looks into the listing's specified template name and uses it (or overrides of it)
	 * @param NULL|string $template IGNORED!
	 */
	function renderView($template = NULL) {

		if (empty($this->listing)) {
			header('', true, 410);
			parent::renderView('notfound');
			return;
		}

		// List the template paths sorted by how specific they are
		$templates['templateOverride'] = KenedoPlatform::p()->getTemplateOverridePath('com_configbox', 'productlisting', $this->template);
		$templates['customTemplate'] = CONFIGBOX_DIR_CUSTOMIZATION.DS.'templates'.DS.'productlisting'.DS.$this->template.'.php';
		$templates['defaultTemplate'] = KPATH_DIR_CB.DS.'views'.DS.'productlisting'.DS.'tmpl'.DS.'default.php';

		// Try which template to use
		foreach ($templates as $template) {
			if (file_exists($template)) {
				require($template);
				break;
			}
		}

	}

	function prepareTemplateVars() {

		// Get the requested product listing id
		$listingId = KRequest::getInt('listing_id');

		$model = KenedoModel::getModel('ConfigboxModelProductlisting');

		// This is the listing data itself, title, description etc.
		$listing = $model->getProductListing($listingId);

		// Get the layout name of the listing
		$templateName = (!empty($listing->layoutname)) ? $listing->layoutname : 'default';
		$this->assign('template', $templateName);


		// Params is stuff like show headings etc.
		$params = KenedoPlatform::p()->getAppParameters();
		$this->assignRef('params', $params);

		$this->assign('showPageHeading', ($params->get('show_page_heading', 1) && $params->get('page_title','') != ''));
		$this->assign('pageHeading', ($params->get('page_heading','') != '') ? $params->get('page_heading','') : '');

		if (empty($listing) || $listing->published == 0) {
			$this->listing = NULL;
			return;
		}

		$this->assign('pageHeading', ($params->get('page_heading','') != '') ? $params->get('page_heading','') : $listing->title);

		$this->assignRef('listing', $listing);

		// Get the products of that listing
		$products = $model->getProductsForListing($listingId);

		if ($listing->product_sorting == 0) {
			usort($products, array('ConfigboxModelProductlisting', 'sortProductsByTitle'));
		}

		// Augment product data
		foreach ($products as $key=>$product) {

			// Remove from array if discontinued
			if ($product->discontinued == 1) {
				unset($products[$key]);
				continue;
			}

			// Process description with content plugins
			$product->description = KenedoPlatform::p()->processContentModifiers($product->description);
			$product->description = trim($product->description);

			// Set links
			$product->linkconfigure = KLink::getRoute('index.php?option=com_configbox&view=configuratorpage&prod_id='.$product->id.'&page_id='.$product->firstPageId);
			$product->linkbuy 		= KLink::getRoute('index.php?option=com_configbox&view=cart&task=addProductToCart&prod_id='.$product->id);
			$product->linkdetails 	= KLink::getRoute('index.php?option=com_configbox&view=product&prod_id='.$product->id);

		}

		$this->assignRef('products', $products);

	}

}
