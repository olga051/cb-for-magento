<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxUserHelper {

	static $viesWsdl = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';
	static $error;
	static $errors = array();
	static $userFields;
	static $taxRates;
	static $memoizedPlatformGroupIds;
	static $orderAddresses;
	static $memoizedGroupData;
	static $users;
	static $salutations;

	static function getUserId() {
		return KSession::get('user_id', 0, 'com_configbox');
	}

	static function setUserId($id) {
		KSession::set('user_id', intval($id), 'com_configbox');
	}

	static function getSalutationDropdown($name = 'salutation_id', $selected = NULL) {

		if (!isset(self::$salutations[KText::$languageTag])) {
			self::getSalutations();
		}

		$options = array();
		foreach (self::$salutations[KText::$languageTag] as $id=>$item) {
			$options[$id] = $item->title;
		}

		$selectBox = KenedoHtml::getSelectField($name, $options, $selected);
		return $selectBox;
	}

	/**
	 * @param $salutationId
	 * @return object $salutation Object holding salutation info (see EntitySalutations)
	 */
	static function getSalutation($salutationId) {
		if (!$salutationId) {
			return NULL;
		}

		if (!isset(self::$salutations[KText::$languageTag])) {
			self::getSalutations();
		}

		return self::$salutations[KText::$languageTag][$salutationId];

	}

	static function getOrderSalutation($salutationId, $orderId) {
		if (!$salutationId || !$orderId) {
			return NULL;
		}

		$query = "SELECT * FROM `#__cbcheckout_order_salutations` WHERE `id` = ".intval($salutationId)." AND `order_id` = ".intval($orderId);
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$salutation = $db->loadObject();
		if (!$salutation) {
			return NULL;
		}
		else {
			$salutation->title = ConfigboxCacheHelper::getOrderTranslation($orderId, 'configbox_strings', 55, $salutationId);
		}

		return $salutation;

	}

	static function getSalutations() {

		if (!isset(self::$salutations[KText::$languageTag])) {
			$query = "SELECT * FROM `#__configbox_salutations`";
			$db = KenedoPlatform::getDb();
			$db->setQuery($query);
			self::$salutations[KText::$languageTag] = $db->loadObjectList('id');
			foreach ( self::$salutations[KText::$languageTag] AS $id=>$salutation) {
				$salutation->title = ConfigboxCacheHelper::getTranslation('#__configbox_strings',55,$id);
			}
		}

	}

	/**
	 * @param int $customerGroupId
	 * @return int $platformGroupId
	 */
	static function getPlatformGroupId($customerGroupId = NULL) {

		if (!$customerGroupId) {
			$customerGroupId = CONFIGBOX_DEFAULT_CUSTOMER_GROUP_ID;
		}

		if (empty(self::$memoizedPlatformGroupIds[$customerGroupId])) {
			$query = "SELECT `joomla_user_group_id` FROM `#__cbcheckout_user_groups` WHERE `id` = ".(int)$customerGroupId;
			$db = KenedoPlatform::getDb();
			$db->setQuery($query);
			self::$memoizedPlatformGroupIds[$customerGroupId] = $db->loadResult();
		}

		return self::$memoizedPlatformGroupIds[$customerGroupId];

	}

	/**
	 * Returns the customer field definition array. Mind that the group id is ignored (definition per group are on ice)
	 * @param int $groupId
	 * @return array of user fields
	 */
	static function getUserFields($groupId = NULL) {

		// Since this is can only be 1 currently, we hard-code the group id
		$groupId = 1;

		if (!isset(self::$userFields[$groupId])) {

			$db = KenedoPlatform::getDb();
			$query = 'SELECT * FROM `#__cbcheckout_user_field_definitions` WHERE `group_id` = '.(int)$groupId;
			$db->setQuery($query);
			self::$userFields[$groupId] = $db->loadObjectList('field_name');

			$copy = (array)self::$userFields[$groupId]['billingcity'];
			self::$userFields[$groupId]['billingcity_id'] = $copy;
			self::$userFields[$groupId]['billingcity_id']['field_name'] = 'billingcity_id';
			self::$userFields[$groupId]['billingcity_id'] = (object)self::$userFields[$groupId]['billingcity_id'];

			$copy = (array)self::$userFields[$groupId]['city'];
			self::$userFields[$groupId]['city_id'] = $copy;
			self::$userFields[$groupId]['city_id']['field_name'] = 'city_id';
			self::$userFields[$groupId]['city_id'] = (object)self::$userFields[$groupId]['city_id'];
		}
		return self::$userFields[$groupId];
	}

	static function getUserIdByEmail($email) {
		if (trim($email) == '') {
			return null;
		}
		$db = KenedoPlatform::getDb();
		$query = "SELECT `id` FROM `#__configbox_users` WHERE `is_temporary` = 0 AND `billingemail` = '".$db->getEscaped($email)."'";
		$db->setQuery($query);
		$id = $db->loadResult();
		return $id;
	}

	static function getUserByJoomlaUserId($jUserId) {

		$db = KenedoPlatform::getDb();

		$query = "SELECT * FROM `#__configbox_users` WHERE `platform_user_id` = '".(int)$jUserId."' LIMIT 1";
		$db->setQuery($query);
		$user = $db->loadObject();

		if ($user) {
			return $user;
		}

		$userId = self::getUserId();

		if ($userId == 0) {
			$user = self::initUserRecord();
		}
		else {
			$query = "SELECT * FROM `#__configbox_users` WHERE `id` = ".(int)$userId;
			$db->setQuery($query);
			$user = $db->loadObject();
		}

		return $user;
	}

	static function resetUserCache($userId = NULL) {
		if ($userId) {
			if (isset(self::$users[$userId])) {
				unset(self::$users[$userId]);
			}
		}
		else {
			self::$users = array();
		}
	}

	static function resetOrderAddressCache($orderId = NULL) {
		if ($orderId) {
			if (isset(self::$orderAddresses[$orderId])) {
				unset(self::$orderAddresses[$orderId]);
			}
		}
		else {
			self::$orderAddresses = array();
		}
	}

	/**
	 * @param int $userId ConfigBox user id (leave empty for the current user)
	 * @param bool $augment If the user record should get augmented (add stuff like country name etc.)
	 * @param bool $init If you want an "inited" record if not found (holds all fields and goes through Geo IP)
	 * @return object User data, NULL if not found and $init is false
	 */
	static function &getUser($userId = NULL, $augment = true, $init = true) {

		if ($userId == NULL) {
			$userId = self::getUserId();
		}

		if (!isset(self::$users[$userId])) {

			$db = KenedoPlatform::getDb();
			$query = "SELECT *  FROM `#__configbox_users` WHERE `id` = ".intval($userId);
			$db->setQuery($query);
			$user = $db->loadObject();

			if (!$user && $init == false) {
				$return = NULL;
				return $return;
			}

			if (!$user) {
				$user = self::initUserRecord();
			}

			if ($user) {
				self::$users[$userId] = $user;
			}

		}

		if ($augment) {
			self::augmentUserRecord( self::$users[$userId] );
		}

		return self::$users[$userId];

	}

	static function orderAddressComplete($orderAddress, $context = 'checkout') {

		$model = KenedoModel::getModel('ConfigboxModelAdmincustomers');
		$response = $model->validateData($orderAddress, $context);

		return $response;

	}

	/**
	 * Change the customer and platform user password
	 * The method is very complicated given the task. That is because we cannot rely on the platform to provide a
	 * direct way to get a password encrypted. So we need to store the platform user first, then retrieve the password.
	 *
	 * @param int $cbUserId
	 * @param string $newPassword
	 * @return bool $success
	 */
	static function changeUserPassword($cbUserId, $newPassword) {

		$db = KenedoPlatform::getDb();
		$query = "SELECT `platform_user_id` FROM `#__configbox_users` WHERE `id` = ".intval($cbUserId);
		$db->setQuery($query);
		$platformId = $db->loadResult();

		if (!$platformId) {
			KLog::log('Could not change the password for customer with ID "'.$cbUserId.'". The customer account is not connected to a platform account.','error');
			self::$error = KText::_('We could not change your password because of a system error. Please contact us to solve this issue.');
			return false;
		}

		// Keep a copy of the old password for later
		$oldPassword = KenedoPlatform::p()->getUserPasswordEncoded($platformId);

		// Change the platform user password
		$success = KenedoPlatform::p()->changeUserPassword($platformId, $newPassword);

		if ($success == false) {
			self::$error = KText::_('We could not change your password because of a system error. Please contact us to solve this issue.');
			return false;
		}

		// Now get the password
		$password = KenedoPlatform::p()->getUserPasswordEncoded($platformId);

		// Update the customer password
		$query = "UPDATE `#__configbox_users` SET `password` = '".$db->getEscaped($password)."' WHERE `id` = ".intval($cbUserId);
		$db->setQuery($query);
		$success = $db->query();

		if ($success) {
			return true;
		}
		else {
			// Change back the password to avoid out-of-sync issues
			KenedoPlatform::p()->changeUserPassword($platformId, $oldPassword);
			self::$error = KText::_('We could not change your password because of a system error. Please contact us to solve this issue.');
			return false;
		}
	}

	/**
	 * @param int $orderId
	 * @param bool $augment
	 * @param bool $init
	 * @return object
	 */
	static function &getOrderAddress($orderId, $augment = true, $init = true) {

		if ($orderId === NULL) {
			$return = NULL;
			return $return;
		}

		if (!isset(self::$orderAddresses[$orderId])) {
			$db = KenedoPlatform::getDb();
			$query = "SELECT * FROM `#__cbcheckout_order_users` WHERE `order_id` = ".intval($orderId)." LIMIT 1";
			$db->setQuery($query);
			$orderAddress = $db->loadObject();

			if (!$orderAddress && $init == false) {
				$return = NULL;
				return $return;
			}

			if (!$orderAddress && $init == true) {
				$orderAddress = self::initUserRecord();
			}

			self::$orderAddresses[$orderId] = $orderAddress;
		}

		if ($augment) {
			self::augmentUserRecord( self::$orderAddresses[$orderId], $orderId );
		}

		return self::$orderAddresses[$orderId];
	}

	static function setOrderAddress($orderId, $userData = NULL, $wasUpdated = true) {

		if ($userData === NULL) {
			$userData = self::getUser();
		}

		$record = new stdClass();

		if ($userData) {

			$record->id 			= $userData->id;

			$record->platform_user_id = $userData->platform_user_id;
			$record->language_tag 	= $userData->language_tag;
			$record->vatin 			= $userData->vatin;
			$record->samedelivery 	= $userData->samedelivery;
			$record->group_id 		= $userData->group_id;
			$record->newsletter 	= $userData->newsletter;

			if (!isset($userData->custom_1)) {
				$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
				KLog::log('Missing custom_1 in user data. This is the backtrace: '.var_export($trace, true), 'warning');
			}

			$record->custom_1 = $userData->custom_1;
			$record->custom_2 = $userData->custom_2;
			$record->custom_3 = $userData->custom_3;
			$record->custom_4 = $userData->custom_4;

			$record->billingcompanyname 	= $userData->billingcompanyname;
			$record->billingfirstname 		= $userData->billingfirstname;
			$record->billinglastname 		= $userData->billinglastname;
			$record->billinggender 			= $userData->billinggender;
			$record->billingsalutation_id 	= $userData->billingsalutation_id;
			$record->billingaddress1 		= $userData->billingaddress1;
			$record->billingaddress2 		= $userData->billingaddress2;
			$record->billingzipcode 		= $userData->billingzipcode;
			$record->billingcity 			= $userData->billingcity;
			$record->billingcountry 		= $userData->billingcountry;
			$record->billingstate 			= $userData->billingstate;
			$record->billingcounty_id		= $userData->billingcounty_id;
			$record->billingcity_id			= $userData->billingcity_id;
			$record->billingemail 			= $userData->billingemail;
			$record->billingphone 			= $userData->billingphone;


			// Duplicate billing to delivery info if same delivery
			if ($userData->samedelivery == 1) {
				$record->companyname 	= $userData->billingcompanyname;
				$record->firstname 		= $userData->billingfirstname;
				$record->lastname 		= $userData->billinglastname;
				$record->gender 		= $userData->billinggender;
				$record->salutation_id 	= $userData->billingsalutation_id;
				$record->address1 		= $userData->billingaddress1;
				$record->address2 		= $userData->billingaddress2;
				$record->zipcode 		= $userData->billingzipcode;
				$record->city 			= $userData->billingcity;
				$record->country 		= $userData->billingcountry;
				$record->state 			= $userData->billingstate;
				$record->county_id		= $userData->billingcounty_id;
				$record->city_id		= $userData->billingcity_id;
				$record->email 			= $userData->billingemail;
				$record->phone 			= $userData->billingphone;
			}
			else {
				$record->companyname 	= $userData->companyname;
				$record->firstname 		= $userData->firstname;
				$record->lastname 		= $userData->lastname;
				$record->gender 		= $userData->gender;
				$record->salutation_id 	= $userData->salutation_id;
				$record->address1 		= $userData->address1;
				$record->address2 		= $userData->address2;
				$record->zipcode 		= $userData->zipcode;
				$record->city 			= $userData->city;
				$record->country 		= $userData->country;
				$record->state 			= $userData->state;
				$record->county_id		= $userData->county_id;
				$record->city_id		= $userData->city_id;
				$record->email 			= $userData->email;
				$record->phone 			= $userData->phone;
			}

		}

		// Set the order id for storing in the order_ table
		$record->order_id = $orderId;

		// Set created time in case something went foul
		if (empty($record->created) || $record->created == '0000-00-00 00:00:00') {
			$record->created = KenedoTimeHelper::getNormalizedTime('NOW', 'datetime');
		}

		// Check if there is an entry already
		$db = KenedoPlatform::getDb();
		$baseDataStored = $db->replaceObject('#__cbcheckout_order_users', $record);

		// Bounce on failure
		if ($baseDataStored == false) {
			$internalMessage = 'Could not set order user record. SQL error is "'.$db->getErrorMsg().'". Record is '.var_export($record,true);
			KLog::log($internalMessage, 'error', KText::_('A system error occured during storing your order\'s address information'));
			return false;
		}

		// Store all related user data in the order_ tables (will be used when augmenting order addresses)
		if ($baseDataStored == true) {

			if ($record->billingcountry) {
				self::storeOrderCountryData($record->billingcountry, $record->order_id);
			}

			if ($record->country && $record->country != $record->billingcountry) {
				self::storeOrderCountryData($record->country, $record->order_id);
			}

			if ($record->billingstate) {
				self::storeOrderStateData($record->billingstate, $record->order_id);
			}
			if ($record->state && $record->state != $record->billingstate) {
				self::storeOrderStateData($record->state, $record->order_id);
			}

			if ($record->billingcounty_id) {
				self::storeOrderCountyData($record->billingcounty_id, $record->order_id);
			}
			if ($record->county_id && $record->county_id != $record->billingcounty_id) {
				self::storeOrderCountyData($record->county_id, $record->order_id);
			}

			if ($record->billingcounty_id) {
				self::storeOrderCityData($record->billingcity_id, $record->order_id);
			}
			if ($record->city_id && $record->city_id != $record->billingcounty_id) {
				self::storeOrderCityData($record->city_id, $record->order_id);
			}

			if ($record->billingsalutation_id) {
				self::storeOrderSalutationData($record->billingsalutation_id, $record->order_id);
			}
			if ($record->salutation_id && $record->salutation_id != $record->billingsalutation_id) {
				self::storeOrderSalutationData($record->salutation_id, $record->order_id);
			}

		}

		if ($baseDataStored) {

			if (KRequest::getVar('comment',NULL) !== NULL) {
				$comment = KRequest::getString('comment');
				$db = KenedoPlatform::getDb();
				$query = "UPDATE `#__cbcheckout_order_records` SET `comment` = '".$db->getEscaped($comment)."' WHERE `id` = ".(int)$orderId;
				$db->setQuery($query);
				$db->query();
			}

			// Augment and cache the order record

			self::$orderAddresses[$orderId] = $record;

			if ($wasUpdated) {
				self::augmentUserRecord($record, $orderId);
				KenedoObserver::triggerEvent( 'onConfigBoxUpdateOrderAddress' , array(self::$orderAddresses[$orderId], $orderId) );
			}


		}

		return $baseDataStored;

	}


	static function storeOrderCountryData($countryId, $orderId) {
		$data = CbcheckoutCountryHelper::getCountry($countryId);
		if (!$data) {
			return false;
		}

		$db = KenedoPlatform::getDb();
		$record = clone $data;
		$record->order_id = $orderId;
		unset($record->custom_translatable_1, $record->custom_translatable_2);
		$success = $db->replaceObject('#__cbcheckout_order_countries', $record);

		if ($success) {
			ConfigboxCacheHelper::copyTranslationToOrder($orderId, '#__configbox_strings', 42, $countryId);
			ConfigboxCacheHelper::copyTranslationToOrder($orderId, '#__configbox_strings', 43, $countryId);
		}

		return $success;

	}

	static function storeOrderStateData($stateId, $orderId) {
		$data = CbcheckoutCountryHelper::getState($stateId);
		if (!$data) {
			return false;
		}

		$db = KenedoPlatform::getDb();
		$record = clone $data;
		$record->order_id = $orderId;
		$success = $db->replaceObject('#__cbcheckout_order_states', $record);

		return $success;

	}

	static function storeOrderCountyData($countyId, $orderId) {
		$data = CbcheckoutCountryHelper::getCounty($countyId);
		if (!$data) {
			return false;
		}

		$db = KenedoPlatform::getDb();
		$record = clone $data;
		$record->order_id = $orderId;
		$success = $db->replaceObject('#__cbcheckout_order_counties', $record);

		return $success;

	}

	static function storeOrderCityData($cityId, $orderId) {
		$data = CbcheckoutCountryHelper::getCity($cityId);
		if (!$data) {
			return false;
		}

		$db = KenedoPlatform::getDb();
		$record = clone $data;
		$record->order_id = $orderId;
		$success = $db->replaceObject('#__cbcheckout_order_cities', $record);

		return $success;

	}

	static function storeOrderSalutationData($salutationId, $orderId) {
		$data = self::getSalutation($salutationId);
		if (!$data) {
			return false;
		}

		$db = KenedoPlatform::getDb();
		$record = clone $data;
		$record->order_id = $orderId;
		unset($record->title);
		$success = $db->replaceObject('#__cbcheckout_order_salutations', $record);

		if ($success) {
			ConfigboxCacheHelper::copyTranslationToOrder($orderId, '#__configbox_strings', 55, $salutationId);
		}

		return $success;

	}

	/**
	 * @param null|int $groupId Group ID or NULL to use the current user's group ID (or group of user defaults)
	 * @return object|null Group data or NULL if group doesn't exist
	 */
	static function &getGroupData($groupId = NULL) {

		if ($groupId === NULL) {
			$user = self::getUser(NULL, false, true);
			$groupId = $user->group_id;
		}

		if (!isset(self::$memoizedGroupData[$groupId])) {

			$db = KenedoPlatform::getDb();
			$query = "SELECT * FROM `#__cbcheckout_user_groups` WHERE `id` = ".(int)$groupId;
			$db->setQuery($query);
			$groupData = $db->loadObject();

			if ($groupData) {
				$groupData->title_discount_1 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 80, $groupId);
				$groupData->title_discount_2 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 81, $groupId);
				$groupData->title_discount_3 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 82, $groupId);
				$groupData->title_discount_4 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 83, $groupId);
				$groupData->title_discount_5 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 84, $groupId);

				$groupData->title_recurring_discount_1 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 85, $groupId);
				$groupData->title_recurring_discount_2 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 86, $groupId);
				$groupData->title_recurring_discount_3 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 87, $groupId);
				$groupData->title_recurring_discount_4 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 88, $groupId);
				$groupData->title_recurring_discount_5 = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 89, $groupId);

				self::$memoizedGroupData[$groupId] = $groupData;

			}
			else {
				self::$memoizedGroupData[$groupId] = NULL;
			}

		}

		return self::$memoizedGroupData[$groupId];
	}

	/**
	 * Adds country, state, county, city, gender and salutation info to the bare user record.
	 * BE AWARE: Alters the provided $user parameter var, does not return it
	 * Augments user records as well as order address (use $orderId param)
	 *
	 * @param object $user Bare user record (augmenting twice is fine), comes in referenced
	 * @param int $orderId Order ID, when provided, data is taken from the order_ tables instead
	 *
	 * @return bool false bare record isn't provided, true otherwise
	 */
	static function augmentUserRecord(&$user, $orderId = NULL) {

		if (!$user) {
			return false;
		}

		// Name of the customer's preferred language
		if ($user->language_tag) {
			$language = KenedoLanguageHelper::getLanguageByTag($user->language_tag);
			if ($language) {
				$user->language_name = $language->label;
			}
			else {
				$user->language_name = '';
			}
		}
		else {
			$user->language_name = '';
		}

		// Country data for delivery
		if ($user->country) {

			if ($orderId) {
				$country = ConfigboxCountryHelper::getOrderCountry($user->country, $orderId);
			}
			else {
				$country = ConfigboxCountryHelper::getCountry($user->country);
			}

			$user->country_2_code 	= $country->country_2_code;
			$user->country_3_code 	= $country->country_3_code;
			$user->countryname  	= $country->country_name;
		}
		else {
			$user->country_2_code = '';
			$user->country_3_code = '';
			$user->countryname   = '';
		}

		// Country data for billing
		if ($user->billingcountry) {

			if ($orderId) {
				$country = ConfigboxCountryHelper::getOrderCountry($user->billingcountry, $orderId);
			}
			else {
				$country = ConfigboxCountryHelper::getCountry($user->country);
			}

			$user->billingcountry_2_code 	= $country->country_2_code;
			$user->billingcountry_3_code 	= $country->country_3_code;
			$user->billingcountryname   	= $country->country_name;
		}
		else {
			$user->billingcountry_2_code 	= '';
			$user->billingcountry_3_code 	= '';
			$user->billingcountryname   	= '';
		}

		// State data for delivery
		$user->statecode = '';
		$user->statefips = '';
		$user->statename = '';

		if ($user->state) {

			if ($orderId) {
				$state = ConfigboxCountryHelper::getOrderState($user->state, $orderId);
				if (!$state) {
					$state = ConfigboxCountryHelper::getState($user->state);
				}
			}
			else {
				$state = ConfigboxCountryHelper::getState($user->state);
			}
			
			if ($state) {
				$user->statecode = $state->iso_code;
				$user->statefips = $state->fips_number;
				$user->statename = $state->name;
			}
			else {
				KLog::log('State data not found for delivery state ID "'.$user->state.'". var_export on state ID gives us '.var_export($user->state).'. Whole user data is '.var_export($user, true), 'error');
			}

		}


		// State data for billing
		$user->billingstatecode = '';
		$user->billingstatefips = '';
		$user->billingstatename = '';

		if ($user->billingstate) {

			if ($orderId) {
				$state = ConfigboxCountryHelper::getOrderState($user->billingstate, $orderId);
				if (!$state) {
					$state = ConfigboxCountryHelper::getState($user->billingstate);
				}
			}
			else {
				$state = ConfigboxCountryHelper::getState($user->billingstate);
			}

			if ($state) {
				$user->billingstatecode = $state->iso_code;
				$user->billingstatefips = $state->fips_number;
				$user->billingstatename = $state->name;
			}
			else {
				KLog::log('State data not found for billing state ID "'.$user->state.'". var_export on state ID gives us '.var_export($user->billingstate).'. Whole user data is '.var_export($user, true), 'error');
			}

		}

		// County data for delivery
		if ($user->county_id) {
			if ($orderId) {
				$county = ConfigboxCountryHelper::getOrderCounty($user->county_id, $orderId);
			}
			else {
				$county = ConfigboxCountryHelper::getCounty($user->county_id);
			}

			$user->county = $county->county_name;
		}
		else {
			$user->county = '';
		}

		// County data for billing
		if ($user->billingcounty_id) {

			if ($orderId) {
				$county = ConfigboxCountryHelper::getOrderCounty($user->billingcounty_id, $orderId);
			}
			else {
				$county = ConfigboxCountryHelper::getCounty($user->billingcounty_id);
			}

			$user->billingcounty = $county->county_name;
		}
		else {
			$user->billingcounty = '';
		}

		// City data for delivery
		if ($user->city_id) {
			if ($orderId) {
				$city = ConfigboxCountryHelper::getOrderCity($user->city_id, $orderId);
			}
			else {
				$city = ConfigboxCountryHelper::getCity($user->city_id);
			}

			$user->city = $city->city_name;
		}

		// City data for billing
		if ($user->billingcity_id) {
			if ($orderId) {
				$city = ConfigboxCountryHelper::getOrderCity($user->billingcity_id, $orderId);
			}
			else {
				$city = ConfigboxCountryHelper::getCity($user->billingcity_id);
			}

			$user->billingcity = $city->city_name;
		}

		// Salutation data for delivery
		if ($user->salutation_id) {

			if ($orderId) {
				$salutation = self::getOrderSalutation($user->salutation_id, $orderId);
			} else {
				$salutation = self::getSalutation($user->salutation_id);
			}

			$user->salutation = $salutation->title;
			// There's some nasty stuff going on (during data preparing in entity, that field is actually figured out and also stored in the DB)
			// At some point that entity prepare thing should go away, this here will be the replacement
			$user->gender = $salutation->gender;

		}
		else {
			$user->salutation = '';
			// There's some nasty stuff going on (during data preparing in entity, that field is actually figured out and also stored in the DB)
			// At some point that entity prepare thing should go away, this here will be the replacement
			$user->gender = (!empty($user->gender)) ? $user->gender : 1;
		}

		// Salutation data for billing
		if ($user->billingsalutation_id) {

			if ($orderId) {
				$salutation = self::getOrderSalutation($user->billingsalutation_id, $orderId);
			} else {
				$salutation = self::getSalutation($user->billingsalutation_id);
			}

			$user->billingsalutation = $salutation->title;
			// There's some nasty stuff going on (during data preparing in entity, that field is actually figured out and also stored in the DB)
			// At some point that entity prepare thing should go away, this here will be the replacement
			$user->billinggender = $salutation->gender;

		}
		else {
			$user->billingsalutation = '';
			// There's some nasty stuff going on (during data preparing in entity, that field is actually figured out and also stored in the DB)
			// At some point that entity prepare thing should go away, this here will be the replacement
			$user->billinggender = (!empty($user->billinggender)) ? $user->billinggender : 1;
		}

		return true;

	}

	static function getPassword($length) {

		$salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$len = strlen($salt);
		$makepass = '';

		$stat = @stat(__FILE__);
		if(empty($stat) || !is_array($stat)) $stat = array(php_uname());

		mt_srand(crc32(microtime() . implode('|', $stat)));

		for ($i = 0; $i < $length; $i ++) {
			$makepass .= $salt[mt_rand(0, $len -1)];
		}

		return $makepass;

	}

	/**
	 * @param object $cbUser ConfigBox user record
	 * @param string $passwordClear Password in clear text (leave empty to auto-generate)
	 * @return bool|StdClass false on error, object holding registration data
	 */
	static function registerPlatformUser($cbUser, $passwordClear = NULL) {

		self::$error = null;

		if (!$passwordClear) {
			$passwordClear = self::getPassword(8);
		}

		$user = new stdClass();
		$user->email 		= $cbUser->billingemail;
		$user->username 	= $cbUser->billingemail;
		$user->password 	= $passwordClear;
		$user->password2 	= $passwordClear;
		$user->name 		= $cbUser->billingfirstname . ' ' . $cbUser->billinglastname;

		$platformGroupIds				= array(self::getPlatformGroupId($cbUser->group_id));

		// The session value notifies the onAfterStoreUser method not to create a new customer record
		// That is because the method looks for a joomla id field to decide. This cannot be set before actually having a user registered. So..

		KSession::set('noUserSetup',true);
		$newUser = KenedoPlatform::p()->registerUser($user, $platformGroupIds);
		KSession::delete('noUserSetup');

		if ($newUser == false) {
			self::addError( KenedoPlatform::p()->getError() );
			return false;
		}

		$registrationData = new stdClass();

		$registrationData->platformUserId = $newUser->id;
		$registrationData->username = $newUser->username;
		$registrationData->passwordClear = $passwordClear;
		$registrationData->passwordEncrypted = $newUser->password;

		return $registrationData;

	}

	/**
	 * @param object $user Configbox user record
	 * @param string $passwordClear
	 * @return bool $success
	 */
	static function sendRegistrationEmail( $user, $passwordClear ) {

		// Get the store information for email data
		$shopData = ConfigboxStoreHelper::getStoreRecord();

		// Load the registration view content
		$registrationView = KenedoView::getView('ConfigboxViewEmailcustomerregistration');
		$registrationView->assign('shopData', $shopData);
		$registrationView->assign('customer', $user);
		$registrationView->assign('passwordClear', $passwordClear);

		$registrationHtml = $registrationView->getViewOutput('default');

		// Load the general email template and put the registration view content in it
		$emailView = KenedoView::getView('ConfigboxViewEmailtemplate');
		$emailView->prepareTemplateVars();
		$emailView->assign('emailContent', $registrationHtml);
		$registrationHtml = $emailView->getViewOutput('default');

		// Prepare the email data
		$email = new stdClass();
		$email->senderEmail = $shopData->shopemailsupport;
		$email->senderName	= $shopData->shopname;
		$email->receipient = $user->billingemail;
		$email->subject = KText::sprintf('EMAIL_CUSTOMER_REGISTRATION_SUBJECT',$shopData->shopname);
		$email->body = $registrationHtml;
		$email->mode = 1;

		// Send the email
		$sendSuccess = KenedoPlatform::p()->sendEmail($email->senderEmail, $email->senderName, $email->receipient, $email->subject, $email->body, $email->mode);

		if (!$sendSuccess) {
			self::$error = KText::_('Registration email could not get sent.');
			KLog::log('Registration email could not get sent to "'.$email->senderEmail.'".','warning');
			return false;
		}
		else {
			return true;
		}
	}

	/**
	 * @param int $userId ConfigBox user id
	 * @param string $password Clear text password
	 * @return bool $success
	 */
	static function authenticateUser($userId, $password) {
		$user = self::getUser($userId, false, false);
		if (!$user) {
			return false;
		}
		$response = KenedoPlatform::p()->authenticate($user->billingemail, $password, array());
		return $response;
	}

	/**
	 * @param int $userId ConfigBox user id
	 * @return bool $success
	 */
	static function loginUser($userId) {
		$user = self::getUser($userId, false, false);
		if (!$user) {
			return false;
		}
		$response = KenedoPlatform::p()->login($user->billingemail);
		return $response;
	}

	/**
	 * This method is used by the user plugin for Joomla. It is an event handler, not a method to login someone
	 * @param array $platformUser
	 * @return bool
	 */
	static function onLoginUser($platformUser) {

		$isAdminArea = KenedoPlatform::p()->isAdminArea();

		if ($isAdminArea) {
			return true;
		}

		// Remember old user id
		$oldUserId = KSession::get('user_id', 0, 'com_configbox');

		$platformUserId = KenedoPlatform::p()->getUserIdByUsername($platformUser['username']);

		// Search for existing customers
		$db = KenedoPlatform::getDb();
		$query = "SELECT `id` FROM `#__configbox_users` WHERE `platform_user_id` = ".(int)$platformUserId ." LIMIT 1";
		$db->setQuery($query);
		$userId = $db->loadResult();

		// Auto-create a non customer in joomla
		if (!$userId) {

			$nameParts = explode(' ',$platformUser['fullname']);

			if (count($nameParts) > 1) {
				$lastName = array_pop($nameParts);
				$firstName = implode(' ',$nameParts);
			}
			else {
				$firstName = 'No first name';
				$lastName = $platformUser['fullname'];
			}

			$user = new stdClass();

			$user->billingfirstname = $firstName;
			$user->firstname = $firstName;

			$user->billinglastname = $lastName;
			$user->lastname = $lastName;

			// Get the platform user id
			$user->platform_user_id = $platformUserId;

			// Get the Joomla user email address
			$user->billingemail = $platformUser['email'];
			$user->email = $platformUser['email'];
			$user->password = KenedoPlatform::p()->getUserPasswordEncoded($platformUserId);

			// Set the user to non temporary since he is a registered platform user
			$user->is_temporary = 0;

			// Create user
			$userId = self::createNewUser($user);

		}

		// Set session var
		KSession::set('user_id',$userId,'com_configbox');

		// If user id changed, update old user's records
		if ($oldUserId != 0) {
			self::moveUserOrders($oldUserId,$userId);
		}

		return true;
	}

	static function getUserFieldTranslations() {
		return array(
			'companyname'=>KText::_('Delivery Company Name'),
			'salutation_id'=>KText::_('Delivery Salutation'),
			'firstname'=>KText::_('Delivery First Name'),
			'lastname'=>KText::_('Delivery Last Name'),
			'address1'=>KText::_('Delivery Address 1'),
			'address2'=>KText::_('Delivery Address 2'),
			'zipcode'=>KText::_('Delivery ZIP Code'),
			'city'=>KText::_('Delivery City'),
			'city_id'=>KText::_('Delivery City'),
			'country'=>KText::_('Delivery Country'),
			'state'=>KText::_('Delivery State'),
			'email'=>KText::_('Delivery Email'),
			'phone'=>KText::_('Delivery Phone'),
			'language_tag'=>KText::_('Preferred Language'),

			'vatin'=>KText::_('VAT IN'),

			'billingcompanyname'=>KText::_('Billing Company Name'),
			'billingsalutation_id'=>KText::_('Billing Salutation'),
			'billingfirstname'=>KText::_('Billing First Name'),
			'billinglastname'=>KText::_('Billing Last Name'),
			'billingaddress1'=>KText::_('Billing Address'),
			'billingaddress2'=>KText::_('Billing Address 2'),
			'billingzipcode'=>KText::_('Billing ZIP Code'),
			'billingcity'=>KText::_('Billing City'),
			'billingcity_id'=>KText::_('Billing City'),
			'billingcountry'=>KText::_('Billing Country'),
			'billingstate'=>KText::_('Billing State'),
			'billingemail'=>KText::_('Billing Email'),
			'billingphone'=>KText::_('Billing Phone'),

			'samedelivery'=>KText::_('Use billing address for shipping'),
			'newsletter'=>KText::_('Newsletter'),

			'billingcounty_id'=>KText::_('Billing County'),
			'county_id'=>KText::_('Delivery County'),

		);
	}

	static function initUserRecord() {

		$info = new stdClass();

		$db = KenedoPlatform::getDb();

		$userColumns = $db->getTableFields('#__configbox_users',false);
		$userColumns = $userColumns['#__configbox_users'];

		foreach ($userColumns as $colName=>$column) {
			$type = strtolower($column->Type);

			if (strstr($type,'int(') || strstr($type,'boolean') || strstr($type,'float') || strstr($type,'decimal')) {
				$info->$colName = 0;
			}
			else {
				$info->$colName = '';
			}
		}

		$info->group_id = CONFIGBOX_DEFAULT_CUSTOMER_GROUP_ID;
		$info->language_tag = KText::$languageTag;
		$info->newsletter = CONFIGBOX_NEWSLETTER_PRESET;

		// Yes, it is vice versa
		$info->samedelivery = (CONFIGBOX_ALTERNATE_SHIPPING_PRESET) ? 0 : 1;

		if (CONFIGBOX_ENABLE_GEOLOCATION) {

			$locationData = ConfigboxLocationHelper::getLocationByIp();

			if ($locationData !== false) {

				if ($locationData->city) {
					$info->city = $locationData->city;
					$info->billingcity = $locationData->city;
				}

				if ($locationData->zipcode) {
					$info->zipcode = $locationData->zipcode;
					$info->billingzipcode = $locationData->zipcode;
				}

				if ($locationData->countryCode) {
					$id = ConfigboxCountryHelper::getCountryIdByCountry2Code( $locationData->countryCode );
					$info->country = $id;
					$info->billingcountry = $id;
				}

				if ($locationData->stateFips) {
					$id = ConfigboxCountryHelper::getStateIdByFipsNumber( $info->country, $locationData->stateFips );
					$info->state = $id;
					$info->billingstate = $id;
				}

			}

		}

		if (!$info->country) {
			$info->country = CONFIGBOX_DEFAULT_COUNTRY_ID;
		}

		if (!$info->billingcountry) {
			$info->billingcountry = CONFIGBOX_DEFAULT_COUNTRY_ID;
		}

		return $info;

	}

	/**
	 * @param object $user Existing user information (Same structure like any user record ), or nothing if you start new
	 * @return int $userId
	 */
	static function createNewUser($user = NULL) {

		if ($user == NULL) {
			$user = new stdClass();
			$user->is_temporary = 1;
		}

		$userBlueprint = self::initUserRecord();

		foreach ($userBlueprint as $key=>$value) {
			if (!isset($user->$key)) {
				$user->$key = $value;
			}
		}

		if (empty($user->created)) {
			$user->created = KenedoTimeHelper::getFormattedOnly('NOW','datetime');
		}

		$db = KenedoPlatform::getDb();

		$success = $db->insertObject('#__configbox_users', $user,'id');

		if ($success == false) {
			if (class_exists('KLog')) {
				KLog::log('Error when creating new user. SQL error: "'.$db->getErrorMsg().'".','error',KText::_("A system error occured."));
			}
			return false;
		}
		else {
			return $user->id;
		}

	}

	static function moveUserOrders($oldUserId, $newUserId) {

		if (!$oldUserId || !$newUserId) {
			return false;
		}

		if ($oldUserId == $newUserId) {
			return true;
		}

		$db = KenedoPlatform::getDb();

		$query = "UPDATE `#__cbcheckout_order_records` SET `user_id` = ".(int)$newUserId." WHERE `user_id` = ".(int)$oldUserId;
		$db->setQuery($query);
		$db->query();

		// Deal with the redundancy in order_users
		$query = "UPDATE `#__cbcheckout_order_users` SET `id` = ".(int)$newUserId." WHERE `id` = ".(int)$oldUserId;
		$db->setQuery($query);
		$db->query();

		$query = "UPDATE `#__configbox_carts` SET `user_id` = ".(int)$newUserId." WHERE `user_id` = ".(int)$oldUserId;
		$db->setQuery($query);
		$db->query();

		return true;

	}

	static function logoutUser($platformUser) {

		$isAdminArea = KenedoPlatform::p()->isAdminArea();

		if ($isAdminArea) {
			return;
		}

		KSession::delete('user_id','com_configbox');
		KSession::delete('order_id','com_configbox');
		KSession::delete('cart_id','com_configbox');
		KSession::terminateSession();

	}

	static function deleteUser($platformUser) {
		$platformUser = (array)$platformUser;
		$query = "DELETE FROM `#__configbox_users` WHERE `platform_user_id` = ".intval($platformUser['id'])." LIMIT 1";
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$response = $db->query();
		return (bool) $response;
	}

	/**
	 * Checks against order address data tables if order id is provided
	 * @param object $userRecord Object holding ConfigBox user record
	 * @param int $orderId, the order id (optional)
	 * @return boolean
	 */
	static function isVatFree($userRecord, $orderId = NULL) {

		if ($orderId) {
			$deliveryCountry = ConfigboxCountryHelper::getOrderCountry($userRecord->country, $orderId);
			$billingCountry = ConfigboxCountryHelper::getOrderCountry($userRecord->billingcountry, $orderId);
		}
		else {
			$deliveryCountry = ConfigboxCountryHelper::getCountry($userRecord->country);
			$billingCountry = ConfigboxCountryHelper::getCountry($userRecord->billingcountry);
		}
		$isVatFree = false;

		if ($billingCountry && $deliveryCountry) {

			// If both countries are vat free - OK
			if ($billingCountry->vat_free && $deliveryCountry->vat_free) {
				$isVatFree = true;
			}

			// If both countries are vat free with VAT IN and VAT IN is present - OK
			if ($billingCountry->vat_free_with_vatin && $deliveryCountry->vat_free_with_vatin && $userRecord->vatin) {
				$isVatFree = true;
			}
			// If billing country is vat free with VAT IN and delivery country vat free and VAT IN - OK
			if ($billingCountry->vat_free_with_vatin && $deliveryCountry->vat_free && $userRecord->vatin) {
				$isVatFree = true;
			}

		}
		elseif ($billingCountry) {

			if ($billingCountry->vat_free) {
				$isVatFree = true;
			}

			if ($billingCountry->vat_free_with_vatin && $userRecord->vatin) {
				$isVatFree = true;
			}

		}

		return $isVatFree;
	}

	static function getTaxRate($taxClassId, &$orderAddress, $checkVatFree = true) {

		// If this is VAT free, tax rate is zero in any case
		if ($checkVatFree && self::isVatFree($orderAddress)) {
			return 0;
		}


		if (!isset(self::$taxRates[$taxClassId][$orderAddress->country][$orderAddress->state][$orderAddress->county_id][$orderAddress->city_id])) {

			$db = KenedoPlatform::getDb();
			$query = "
			SELECT *
			FROM `#__configbox_tax_classes` AS tcr
			LEFT JOIN `#__cbcheckout_tax_class_rates` AS tc ON tc.tax_class_id = ".(int)$taxClassId." AND ( (tc.state_id != 0 AND tc.state_id = ".(int)$orderAddress->state.") OR (tc.country_id != 0 AND tc.country_id = ".(int)$orderAddress->country."))
			WHERE tcr.id = ".(int)$taxClassId."
			ORDER BY tc.state_id DESC LIMIT 1
			";

			$db->setQuery($query);
			$taxRate = $db->loadAssoc();

			// If we got one, return it, else continue where we get to the default configbox taxrate
			if ($taxRate['tax_rate'] !== NULL) {
				$taxRate =  (float)$taxRate['tax_rate'];
			}
			else if ($taxRate['tax_rate'] === NULL && $taxRate['default_tax_rate'] !== NULL) {
				$taxRate = (float)$taxRate['default_tax_rate'];
			}
			else {
				KLog::log('Could not find tax rate for tax class id: '.$taxClassId, 'error', KText::_('Could not find tax rate for tax class id: "'.$taxClassId.'"'));
				return false;
			}

			self::$taxRates[$taxClassId][$orderAddress->country][$orderAddress->state][$orderAddress->county_id][$orderAddress->city_id] = $taxRate;

			// Add county and city tax
			$db = KenedoPlatform::getDb();
			$query = "
			SELECT tcr.*
			FROM `#__configbox_tax_classes` AS tc
			LEFT JOIN `#__cbcheckout_tax_class_rates` AS tcr ON tcr.tax_class_id = tc.id AND ( (tcr.county_id != 0 AND tcr.county_id = ".(int)$orderAddress->county_id.") OR (tcr.city_id != 0 AND tcr.city_id = ".(int)$orderAddress->city_id.") )
			WHERE tc.id = ".intval($taxClassId)."
			ORDER BY tcr.county_id DESC, tcr.city_id DESC
			";
			$db->setQuery($query);
			$taxRates = $db->loadAssocList();
			if ($taxRates) {
				foreach ($taxRates as $taxRate) {
					self::$taxRates[$taxClassId][$orderAddress->country][$orderAddress->state][$orderAddress->county_id][$orderAddress->city_id] += floatval($taxRate['tax_rate']);
				}
			}

		}

		return self::$taxRates[$taxClassId][$orderAddress->country][$orderAddress->state][$orderAddress->county_id][$orderAddress->city_id];

	}

	static function onAfterStoreUser($platformUser, $isNew, $success, $msg) {

		// This session variable is set in the registerPlatformUser method to prevent this method to set up a second user account.
		if (KSession::get('noUserSetup',false) == true) {
			return true;
		}

		$db = KenedoPlatform::getDb();

		// Search for existing customers
		$query = "SELECT `id` FROM `#__configbox_users` WHERE `platform_user_id` = ".intval($platformUser['id']) ." LIMIT 1";
		$db->setQuery($query);
		$userId = $db->loadResult();

		// Create the customer account if there is none yet
		if (!$userId) {

			$user = new stdClass();

			$nameParts = explode(' ', $platformUser['name']);

			if (count($nameParts) > 1) {
				$lastName = array_pop($nameParts);
				$firstName = implode(' ',$nameParts);
			}
			else {
				$firstName = 'No first name';
				$lastName = $platformUser['name'];
			}

			$user->billingfirstname = $firstName;
			$user->firstname = $firstName;

			$user->billinglastname = $lastName;
			$user->lastname = $lastName;

			// Get the platform user id
			$user->platform_user_id = $platformUser['id'];

			// Get the Joomla user email address
			$user->billingemail = $platformUser['email'];
			$user->email = $platformUser['email'];

			// Set the user to non temporary since he is a registered platform user
			$user->is_temporary = 0;

			$user->password = $platformUser['password'];

			// Create user
			$userId = self::createNewUser($user);

		}
		else {

			$nameParts = explode(' ', $platformUser['name']);

			if (count($nameParts) > 1) {
				$lastName = array_pop($nameParts);
				$firstName = implode(' ',$nameParts);
			}
			else {
				$firstName = 'No first name';
				$lastName = $platformUser['name'];
			}

			$query = "
			UPDATE `#__configbox_users` SET
			`billingfirstname` = '".$db->getEscaped($firstName)."',
			`billinglastname` = '".$db->getEscaped($lastName)."',
			`billingemail` = '".$db->getEscaped($platformUser['email'])."',
			`password` = '".$db->getEscaped($platformUser['password'])."'
			WHERE `id` = ".intval($userId);
			$db->setQuery($query);
			$db->query();

		}

		if ($userId) {
			self::resetUserCache($userId);
			$customer = self::getUser($userId);
			KenedoObserver::triggerEvent('onConfigBoxUpdateUserInfo' , array($customer) );
		}


		return true;

	}

	/**
	 * That method used by the authentication plugin for Joomla
	 * To check for authentication, use ConfigboxUserHelper::authenticate
	 * @param $credentials
	 * @param $options
	 * @param $response
	 * @return bool
	 */
	static function onAuthenticate( $credentials, $options, &$response ) {

		$username = $credentials['username'];
		$passwordClear = $credentials['password'];

		if (empty($username) || empty($passwordClear)) {
			self::addError(KText::_('Username or password incorrect.'));
			/** @noinspection PhpDeprecationInspection */
			$response->status = (defined('JAUTHENTICATE_STATUS_FAILURE')) ? JAUTHENTICATE_STATUS_FAILURE : JAuthentication::STATUS_FAILURE;
			$response->error_message	= '';
			return false;
		}

		$userId = self::getUserIdByEmail($username);
		$user = self::getUser($userId, false, false);

		if (!$user) {
			self::addError(KText::_('Username or password incorrect.'));
			/** @noinspection PhpDeprecationInspection */
			$response->status = (defined('JAUTHENTICATE_STATUS_FAILURE')) ? JAUTHENTICATE_STATUS_FAILURE : JAuthentication::STATUS_FAILURE;
			$response->error_message	= '';
			return false;
		}

		$passwordEncoded = KenedoPlatform::p()->getUserPasswordEncoded($user->platform_user_id);

		if (!$passwordEncoded) {
			self::addError(KText::_('Username or password incorrect.'));
			/** @noinspection PhpDeprecationInspection */
			$response->status = (defined('JAUTHENTICATE_STATUS_FAILURE')) ? JAUTHENTICATE_STATUS_FAILURE : JAuthentication::STATUS_FAILURE;
			$response->error_message	= '';
			return false;
		}

		$passwordsMatch = KenedoPlatform::p()->passwordsMatch($passwordClear, $passwordEncoded);

		if ($passwordsMatch == false) {
			self::addError(KText::_('Username or password incorrect.'));
			/** @noinspection PhpDeprecationInspection */
			$response->status = (defined('JAUTHENTICATE_STATUS_FAILURE')) ? JAUTHENTICATE_STATUS_FAILURE : JAuthentication::STATUS_FAILURE;
			$response->error_message	= '';
			return false;
		}

		/** @noinspection PhpDeprecationInspection */
		$response->status 			= (defined('JAUTHENTICATE_STATUS_SUCCESS')) ? JAUTHENTICATE_STATUS_SUCCESS : JAuthentication::STATUS_FAILURE;
		$response->error_message	= '';

		$response->email = $user->billingemail;
		$response->fullname = $user->billingfirstname . ' ' . $user->billinglastname;
		$response->username = 'dummy';
		$response->language = $user->language_tag;

		if (!empty($user->platform_user_id)) {
			$db = KenedoPlatform::getDb();
			$query = "SELECT `username` FROM `#__users` WHERE `id` = ".intval($user->platform_user_id);
			$db->setQuery($query);
			$username = $db->loadResult();
			if ($username) {
				$response->username = $username;
			}
		}

		return true;

	}

	static function checkVatIn($vatIn, $orderAddress) {

		/*
		 * Store the default socket time out to change it back later (it is lowered to prevent
		 * unnecessary long overall processing timings).
		 */

		$originalSocketTimeout = ini_get("default_socket_timeout");

		// Bounce if the SOAP extension isn't installed on the server
		if (class_exists('SoapClient') == false) {
			KLog::log('The server does not support SOAP. VAT IN check cannot be done.','warning');
			return true;
		}

		self::$error = '';

		try {
			$vies = new SoapClient(self::$viesWsdl, array("connection_timeout" => 3));
		}
		catch (Exception $e) {
			//KenedoPlatform::p()->sendMessage(KText::_('The VAT-IN validation service is currently unavailable. Please try again later.'));
			KLog::log('The VAT-IN validation service was not available.','warning');
			return true;
		}

		if (!is_object($vies)) {
			KLog::log('The VAT-IN validation service was not available.','warning');
			return true;
		}

		// Get the billing country
		$country = ConfigboxCountryHelper::getCountry($orderAddress->billingcountry);

		// Prepare the params for the SOAP request
		$param = new stdClass();
		$param->countryCode = $country->country_2_code;

		// Normalize the provided VAT IN
		$vatIn = str_replace(' ', '', $vatIn);

		if (stripos($vatIn,$country->country_2_code) === 0) {
			$vatIn = substr($vatIn,2);
		}

		if (strpos($vatIn,'-') === 0) {
			$vatIn = substr($vatIn,1);
		}

		$param->vatNumber = $vatIn;

		try {
			// Do the request, set the timeout to 3 seconds to prevent long processing time
			ini_set("default_socket_timeout", 3);
			/** @noinspection PhpUndefinedMethodInspection */
			$response = $vies->checkVat($param);
		}
		catch (SoapFault $e) {
			$ret = $e->getMessage();

			$faults = array (
				'INVALID_INPUT'       => KText::_('The provided country code is invalid or the VAT number is empty'),
				'SERVICE_UNAVAILABLE' => KText::_('The SOAP service is unavailable, try again later'),
				'MS_UNAVAILABLE'      => KText::_('The Member State service is unavailable, try again later or with another Member State'),
				'TIMEOUT'             => KText::_('The Member State service could not be reached in time, try again later or with another Member State'),
				'SERVER_BUSY'         => KText::_('The service cannot process your request. Try again later.'),
			);

			KLog::log('The VAT IN cannot be checked because of this error "'.$ret.'". "'.$faults[$ret].'". VAT IN was "'.$vatIn.'", Country code was "'.$country->country_2_code.'".','warning');
			ini_set("default_socket_timeout", $originalSocketTimeout);
			return true;

		}

		// Reset the socket timeout to the regular value
		ini_set("default_socket_timeout", $originalSocketTimeout);

		return $response->valid;
	}


	static function addError($error) {
		self::$errors[] = $error;

		// Backward compatibility
		self::$error = $error;
	}

	static function addErrors($errors) {

		if (is_array($errors) && count($errors)) {
			self::$errors = array_merge(self::$errors, $errors);

			// Backward compatibility
			self::$error = end($errors);
		}
	}

	static function resetErrors() {
		self::$errors = array();
		// Backward compatibility
		self::$error = '';
	}

	static function getErrors() {
		return self::$errors;
	}

	static function getError() {
		if (is_array(self::$errors) && count(self::$errors)) {
			return end(self::$errors);
		}
		else {
			return '';
		}
	}


}

/**
 * Class CbcheckoutUserHelper
 * @deprecated To be removed in 2.7: Use ConfigboxUserHelper instead
 */
class CbcheckoutUserHelper extends ConfigboxUserHelper {

}