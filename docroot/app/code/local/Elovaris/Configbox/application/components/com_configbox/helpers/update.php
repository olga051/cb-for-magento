<?php
class ConfigboxUpdateHelper {
	
	static $latestUpdateVersion = '';
	static $currentlyProcessedVersion = '';
	static $oldDisplayErrorSetting = '';
	
	static function applyUpdates() {
				
		self::$oldDisplayErrorSetting = ini_set('display_errors',0);
		$db = KenedoPlatform::getDb();

		// Create the system vars table here, we do not do any table creation outside the update process and can't have it within the files
		$query = "
		CREATE TABLE IF NOT EXISTS `#__configbox_system_vars` (
		  `key` varchar(128) NOT NULL,
		  `value` text NOT NULL,
		  PRIMARY KEY (`key`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$db->setQuery($query);
		$db->query();
		
		// Get the latest version that was updated to
		$query = "SELECT `value` FROM `#__configbox_system_vars` WHERE `key` = 'latest_update_version'";
		$db->setQuery($query);
		self::$latestUpdateVersion = $db->loadResult();

		if (!self::$latestUpdateVersion) {
			self::$latestUpdateVersion = '0.1';
		}

		$configboxUpdateFileFolder 	= KPATH_DIR_CB.DS.'helpers'.DS.'updates';
		$configboxUpdateFiles 		= KenedoFileHelper::getFiles($configboxUpdateFileFolder, '.php$', false, false);

		// Sort the files by version
		usort($configboxUpdateFiles, array("ConfigboxUpdateHelper", "sortFiles"));

		foreach ($configboxUpdateFiles as $configboxUpdateFile) {
			
			// Note down the version of that file
			self::$currentlyProcessedVersion = basename($configboxUpdateFile,'.php');
			
			// Figure out if we should run that file
			$doProcessFile = version_compare(self::$currentlyProcessedVersion, self::$latestUpdateVersion,'gt');
			
			if ($doProcessFile) {

				$db->resetFailedQueryCount();

				// Include the file
				require( $configboxUpdateFileFolder.DS.$configboxUpdateFile );

				if ($db->getFailedQueryCount() == 0) {

					// Store the version each time in the loop so that the problems through timeout/break/fatal errors are minimized
					$query = "REPLACE INTO `#__configbox_system_vars` SET `key` = 'latest_update_version', `value` = '".$db->getEscaped(self::$currentlyProcessedVersion)."'";
					$db->setQuery($query);
					$db->query();

				}
				else {

					// Note that upgrade script failed
					$query = "REPLACE INTO `#__configbox_system_vars` SET `key` = 'failed_update_detected', `value` = '1'";
					$db->setQuery($query);
					$db->query();

					// Log the issues
					foreach ($db->getFailedQueryLog() as $data) {
						$logEntry = $data['caller_class'].'::'.$data['caller_function'].'(), File '.$data['caller_file'].' on line: '.$data['caller_line'].', Error num: "'.$data['error_num'].'", error msg: "'.$data['error_msg'].'", query: "'.$data['query'].'"';
						KLog::log($logEntry, 'upgrade_errors');
					}

					// Don't go on
					break;
				}

			}
					
		}
		
		ini_set('display_errors', self::$oldDisplayErrorSetting);
	}

	static function applyCustomizationUpdates() {

		self::$oldDisplayErrorSetting = ini_set('display_errors',0);
		$db = KenedoPlatform::getDb();

		// Create the system vars table here, we do not do any table creation outside the update process and can't have it within the files
		$query = "
		CREATE TABLE IF NOT EXISTS `#__configbox_system_vars` (
		  `key` varchar(128) NOT NULL,
		  `value` text NOT NULL,
		  PRIMARY KEY (`key`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$db->setQuery($query);
		$db->query();

		// Get the latest version that was updated to
		$query = "SELECT `value` FROM `#__configbox_system_vars` WHERE `key` = 'latest_customization_update_version'";
		$db->setQuery($query);
		self::$latestUpdateVersion = $db->loadResult();

		if (!self::$latestUpdateVersion) {
			self::$latestUpdateVersion = '0.1';
		}

		$configboxUpdateFileFolder 	= CONFIGBOX_DIR_CUSTOMIZATION.DS.'updates';
		$configboxUpdateFiles 		= KenedoFileHelper::getFiles($configboxUpdateFileFolder, '.php$', false, false);

		// Sort the files by version
		usort($configboxUpdateFiles, array("ConfigboxUpdateHelper", "sortFiles"));

		foreach ($configboxUpdateFiles as $configboxUpdateFile) {

			// Note down the version of that file
			self::$currentlyProcessedVersion = basename($configboxUpdateFile,'.php');

			// Figure out if we should run that file
			$doProcessFile = version_compare(self::$currentlyProcessedVersion, self::$latestUpdateVersion,'gt');

			if ($doProcessFile) {

				$db->resetFailedQueryCount();

				// Include the file
				require( $configboxUpdateFileFolder.DS.$configboxUpdateFile );

				if ($db->getFailedQueryCount() != 0) {
					// Store the version each time in the loop so that the problems through timeout/break/fatal errors are minimized
					$query = "REPLACE INTO `#__configbox_system_vars` SET `key` = 'failed_update_detected', `value` = '1'";
					$db->setQuery($query);
					$db->query();
				}

				// Store the version each time in the loop so that the problems through timeout/break/fatal errors are minimized
				$query = "REPLACE INTO `#__configbox_system_vars` SET `key` = 'latest_customization_update_version', `value` = '".$db->getEscaped(self::$currentlyProcessedVersion)."'";
				$db->setQuery($query);
				$db->query();

			}

		}

		ini_set('display_errors', self::$oldDisplayErrorSetting);
	}

	static function sortFiles($a,$b) {
		return version_compare($a, $b);
	}
	
	static function getTableFields($tableName) {
		
		$db = KenedoPlatform::getDb();
		$db->setQuery( 'SHOW COLUMNS FROM ' . $tableName );

		$fields = $db->loadObjectList();
		$return = array();
		if (!is_array($fields)) {
			return $return;
		} 
		foreach ($fields as $field) {
			$return[$field->Field] = $field;
		}
		return $return;
	}
	
	static function getTableList() { 
		$db = KenedoPlatform::getDb();
		$db->setQuery( 'SHOW TABLES' );
		$tables = $db->loadAssocList();
		
		$return = array();
		foreach ($tables as $table) {
			$first_key = key($table);
			$tableName = $table[$first_key];
			$return[$tableName] = $tableName;
		}
		
		return $return;
	}
	
	static function tableExists($tableName) {
		
		$db = KenedoPlatform::getDb();
		
		$tableNamePrefixed = str_replace('#__',$db->getPrefix(),$tableName);
		
		if (!$tableNamePrefixed) {
			KLog::log('tableExists called with invalid table name "'.$tableName.'", prefixed name is "'.$tableNamePrefixed.'".','error','A system error occured');
			return false;
		}
		
		$query = "SHOW TABLES LIKE '".$db->getEscaped($tableNamePrefixed)."'";
		$db->setQuery( $query );
		$table = $db->loadResult();
		
		if ($table) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	static function tableFieldExists($tableName, $fieldName) {
		
		$fields = self::getTableFields($tableName);
		if (!isset($fields[$fieldName])) {
			return false;
		}
		else {
			return true;
		}
	}
	
}
