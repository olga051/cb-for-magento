<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewBundle extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'bundle';

	/**
	 * @var string Either 'B2B' or 'B2C' depends on the customer group settings of the current user.
	 */
	public $mode;

	/**
	 * @var object A bundle is similar to a cart
	 * @see ConfigboxModelBundle::getBundle
	 */
	public $bundle;

	/**
	 * @var object A bundle order is similar to a cart position
	 * @see ConfigboxModelCartposition::getCartPosition
	 */
	public $bundleOrder;

	/**
	 * @var object A bundle product is essentially the same as a cart position's product
	 */
	public $bundleProduct;

	/**
	 * @var string The default message text to be sent as recommendation
	 */
	public $defaultMessage;

	/**
	 * @var boolean Indicates if a Captcha should be used. Depends on setting 'use_captcha_recommendation'
	 */
	public $useCaptcha;

	/**
	 * @var string Complete URL for the 'Add to Cart' link
	 */
	public $addBundleToCartLink;

	/**
	 * @var string Complete URL for the 'Edit bundle' link
	 */
	public $editBundleLink;

	/**
	 * @var string All the Recaptcha HTML
	 */
	public $recaptchaHtml;

	/**
	 * @return ConfigboxModelBundle
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelBundle');
	}

	function prepareTemplateVars() {
		
		// Create an order if need be
		
		// Make sure we have a cart id
		$cartModel = KenedoModel::getModel('ConfigboxModelCart');
		if (!$cartModel->getId()) {
			$cartId = $cartModel->createCart();
			$cartModel->setId($cartId, true);
		}
		$cart = $cartModel->getCartData();
		
		// Create a new one if customer cannot add to this order anymore (checked out or similar)
		if (!ConfigboxOrderHelper::isPermittedAction('addToOrder',$cart)) {
			KLog::log('Adding to this cart not allowed, creating new one.');
			$cartId = $cartModel->createCart();
			$cartModel->setId($cartId, true);
		}
		
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
		
		$bundleId = KRequest::getInt('id');
		$cartId = $cartModel->getId();
		
		$positionModel->createPosition($cartId,NULL,$bundleId);
		
		$bundleId = KRequest::getInt('id');

		$bundleModel = KenedoModel::getModel('ConfigboxModelBundle');
		$productModel = KenedoModel::getModel('ConfigboxModelProduct');
		
		$bundle = $bundleModel->getBundle($bundleId);

		$this->assignRef('bundle',$bundle);
		
		if ($bundle) {
			$cartDetails = $cartModel->getCartDetails();
			$position = $positionModel->getPositionDetails();

			if (!$position) {
				return;
			}
			
			$customerGroup = KenedoObserver::triggerEvent('onConfigboxGetGroupData',array($cartDetails->user_id), true);
			
			$discount 			= ConfigboxCustomerGroupHelper::getDiscount($customerGroup, $position->baseTotalUnreducedNet);
			$discountRecurring 	= ConfigboxCustomerGroupHelper::getDiscount($customerGroup, $position->baseTotalUnreducedNet);
			
			$bundle->discountTitle = $discount->title;
			$bundle->discountRecurringTitle = $discountRecurring->title;
			
			// Calculate the amount of discount for net/tax/gross - REGULAR
			$bundle->baseDiscountNet 	= round($position->baseTotalUnreducedNet * ($discount->percentage / 100), 4);
			$bundle->baseDiscountTax 	= round($position->baseTotalUnreducedTax * ($discountRecurring->percentage / 100), 2);
			$bundle->baseDiscountGross 	= $bundle->baseDiscountNet + $bundle->baseDiscountTax;
				
			// Calculate the amount of discount for net/tax/gross - RECURRING
			$bundle->baseDiscountRecurringNet 	= round($position->baseTotalUnreducedRecurringNet * ($discountRecurring->percentage / 100), 4);
			$bundle->baseDiscountRecurringTax 	= round($position->baseTotalUnreducedRecurringTax * ($discountRecurring->percentage / 100), 2);
			$bundle->baseDiscountRecurringGross = $bundle->baseDiscountRecurringNet + $bundle->baseDiscountRecurringTax;

			ConfigboxCurrencyHelper::appendCurrencyPrices($bundle);
						
			$bundleImages 	= $bundleModel->getBundleImages($bundleId);
			$bundleProduct 	= $productModel->getProduct($bundle->product_id);
			
			$this->assignRef('bundleOrder',		$position);
			$this->assignRef('bundleImages',	$bundleImages);
			$this->assignRef('bundleProduct',	$bundleProduct);
			
			$this->assign('editBundleLink', KLink::getRoute('index.php?option=com_configbox&view=cart&task=editCartPosition&cart_position_id='.(int)$position->id));
			$this->assign('addBundleToCartLink', KLink::getRoute('index.php?option=com_configbox&view=cart&task=finishConfiguration&cart_position_id='.(int)$position->id));
				
		}
		
		$defaultMessage = KText::_('BUNDLE_RECOMMENDATION_DEFAULT_MESSAGE');
		
		$link = KPATH_SCHEME.'://' . KPATH_HOST. KLink::getRoute('index.php?option=com_configbox&view=bundle&id='.$bundleId);
		
		if (function_exists('getBundleLink')) {
			getBundleLink($bundleId);
		}
		
		$defaultMessage .= "\n\n".$link;
		
		$this->assignRef('defaultMessage',$defaultMessage);
		
		$useCaptcha = (CONFIGBOX_USE_CAPTCHA_RECOMMENDATION && CONFIGBOX_RECAPTCHA_PUBLIC_KEY && CONFIGBOX_RECAPTCHA_PRIVATE_KEY);
		
		if ($useCaptcha) {

			// Load reCAPTCHA library
			if (class_exists('ReCaptchaResponse') == false) {
				require_once(KPATH_DIR_CB.'/external/recaptcha/recaptchalib.php');
			}

			$recaptchaHtml = recaptcha_get_html(CONFIGBOX_RECAPTCHA_PUBLIC_KEY,NULL,true);
		}
		else {
			$recaptchaHtml = '';
		}
		
		$this->assignRef('useCaptcha',$useCaptcha);
		$this->assignRef('recaptchaHtml',$recaptchaHtml);
		
		$groupData = KenedoObserver::triggerEvent('onConfigboxGetGroupData',array(),true);
		$this->assign('mode', $groupData->b2b_mode ? 'b2b' : 'b2c' );

	}

}
