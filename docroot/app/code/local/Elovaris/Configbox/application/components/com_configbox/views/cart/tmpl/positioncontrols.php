<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewCart */
?>
<div class="position-controls">
	<?php if ($this->canEditOrder) { ?>
		<form action="<?php echo $this->urlPositionFormAction;?>" method="post">
			<div>

				<?php if ($this->position->productData->quantity_in_cart == 1) { ?>

					<label for="quantity-<?php echo $this->position->id;?>" class="label-product-quantity"><?php echo KText::_('Quantity');?></label>
					<?php echo $this->quantityFields[$this->position->id];?>
					<input type="hidden" name="option" value="com_configbox" />
					<input type="hidden" name="task" value="setCartPositionQuantity" />
					<input type="hidden" name="view" value="cart" />
					<input type="hidden" name="cart_id" value="<?php echo intval($this->cart->id);?>" />
					<input type="hidden" name="cart_position_id" value="<?php echo intval($this->position->id);?>" />

					<a rel="nofollow" class="button-frontend-small button-update-quantity"><?php echo KText::_('Change Quantity');?></a>

				<?php } elseif ($this->position->productData->quantity_in_cart == 2) { ?>
					<label for="quantity-<?php echo intval($this->position->id);?>" class="label-product-quantity"><?php echo KText::_('Quantity');?>:</label>
					<span><?php echo intval($this->position->quantity);?></span>
				<?php } ?>

				<a rel="nofollow" class="button-frontend-small button-remove-product" href="<?php echo $this->positionUrls[$this->position->id]['urlRemove'];?>"><?php echo KText::_('Remove');?></a>

				<?php if ($this->position->isConfigurable) { ?>
					<a rel="nofollow" class="button-frontend-small button-edit-product" href="<?php echo $this->positionUrls[$this->position->id]['urlEdit'];?>"><?php echo KText::_('Change');?></a>
					<a rel="nofollow" class="button-frontend-small button-copy-product" href="<?php echo $this->positionUrls[$this->position->id]['urlCopy'];?>"><?php echo KText::_('Copy');?></a>
				<?php } ?>

			</div>
		</form>
	<?php
	} ?>
</div>