<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerAdmincalcmatrices extends KenedoController {

	/**
	 * @return ConfigboxModelAdmincalcmatrices
	 */
	protected function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdmincalcmatrices');
	}

	/**
	 * @return ConfigboxViewAdmincalcmatrix
	 */
	protected function getDefaultView() {
		return $this->getDefaultViewForm();
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return ConfigboxViewAdmincalcmatrix
	 */
	protected function getDefaultViewForm() {
		return KenedoView::getView('ConfigboxViewAdmincalcmatrix');
	}

}
