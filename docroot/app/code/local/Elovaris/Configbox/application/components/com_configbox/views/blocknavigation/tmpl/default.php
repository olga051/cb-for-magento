<?php
defined('CB_VALID_ENTRY') or die('Restricted access');
/** @var $this ConfigboxViewBlockNavigation */
?>

<?php if (count($this->pages) > 1) { ?>

<div class="configbox-block block-navigation <?php echo hsc($this->params->get('moduleclass_sfx'));?> mod_configboxnav <?php echo hsc($this->params->get('moduleclass_sfx'));?><?php echo ($this->showAsTabs) ? ' tab-style':'';?>">
	
	<?php if (CONFIGBOX_BLOCKTITLE_NAVIGATION) { ?><h2 class="block-title"><?php echo hsc(CONFIGBOX_BLOCKTITLE_NAVIGATION);?></h2><?php } ?>
	
	<ul class="<?php echo ($this->showAsTabs) ? 'tabs':'menu';?>">
		<?php foreach ($this->pages as $page) { ?>
			<li class="<?php echo hsc($page->listItemClasses);?>">
				<a class="<?php echo hsc($page->linkClasses);?>" href="<?php echo $page->url;?>">
					<span><?php echo hsc($page->title);?></span>
				</a>
			</li>
		<?php } ?>
	</ul>
</div>

<?php } ?>