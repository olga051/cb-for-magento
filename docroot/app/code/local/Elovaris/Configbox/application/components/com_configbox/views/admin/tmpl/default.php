<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewAdmin*/
?>
<div id="com_configbox">
	<div id="kenedo-page">
		<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>">
		
		<div class="configbox-heading">
			<div class="configbox-logo"></div>
			<div class="right-part"></div>
			<div class="messages-wrapper"></div>
		</div>
		
		<div class="clear"></div>
		
		<div class="configbox-mainmenu">
			<?php KenedoView::getView('ConfigboxViewAdminmainmenu')->display();?>
		</div>
		<div class="configbox-ajax-target">
			<?php echo $this->contentHtml;?>
		</div>
		
		<div class="clear"></div>
		
		</div>
	</div>
</div>