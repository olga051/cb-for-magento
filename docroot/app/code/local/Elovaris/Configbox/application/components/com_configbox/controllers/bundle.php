<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerBundle extends KenedoController {

	/**
	 * @return ConfigboxModelBundle
	 */
	protected function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelBundle');
	}

	/**
	 * @return ConfigboxViewBundle
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewBundle');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

	function saveBundle() {

		if (!CONFIGBOX_ALLOW_RECOMMENDATIONS) {
			return;
		}

		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
		$bundleId = $positionModel->saveBundle();
		$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=bundle&id='.(int)$bundleId,false));

	}

	function recommendBundle() {

		if (!CONFIGBOX_ALLOW_RECOMMENDATIONS) {
			return;
		}

		$bundleId = KRequest::getInt('bundle_id');
		$emailReceipient = KRequest::getString('email');
		$nameSender = KRequest::getString('name');
		$emailSender = KRequest::getString('senderEmail');
		$comment = KRequest::getString('message');
		$recaptcha_challenge_field = KRequest::getString('recaptcha_challenge_field');
		$recaptcha_response_field = KRequest::getString('recaptcha_response_field');

		$useCaptcha = (CONFIGBOX_USE_CAPTCHA_RECOMMENDATION && CONFIGBOX_RECAPTCHA_PUBLIC_KEY && CONFIGBOX_RECAPTCHA_PRIVATE_KEY);

		if ($useCaptcha) {

			// Load reCAPTCHA library
			if (class_exists('ReCaptchaResponse') == false) {
				require_once(KPATH_DIR_CB.'/external/recaptcha/recaptchalib.php');
			}

			$resp = recaptcha_check_answer (
				CONFIGBOX_RECAPTCHA_PRIVATE_KEY,
				ConfigboxLocationHelper::getClientIpV4Address(),
				$recaptcha_challenge_field,
				$recaptcha_response_field);

			if (!$resp->is_valid) {
				$response = new StdClass();
				$response->success = false;
				$response->text = KText::_('The spam protection words are incorrect');
				echo(json_encode($response));
				return;
			}
		}

		$link = KPATH_SCHEME.'://' . KPATH_HOST. KLink::getRoute('index.php?option=com_configbox&view=bundle&id='.(int)$bundleId,false);

		$message  = KText::sprintf('BUNDLE_RECOMMENDATION_MESSAGE_PREPEND', $nameSender, $emailSender, $link );
		$message .= str_replace($link,'',$comment);

		$append = KText::sprintf('BUNDLE_RECOMMENDATION_MESSAGE_APPEND', $nameSender,$emailSender, $link);
		if ($append == 'BUNDLE_RECOMMENDATION_MESSAGE_APPEND') $append = '';
		$message .= $append;

		// Line breaks from the ini files are taken literally, no matter how you do it. So we replace them here
		$message = str_replace('\n',"\n",$message);

		if (function_exists('getBundleLink')) {
			getBundleLink($bundleId);
		}

		if (function_exists('getRecommendationText')) {
			getRecommendationText($message);
		}

		$email = new StdClass();
		$email->fromEmail = $emailSender;
		$email->fromName = $nameSender;
		$email->to = $emailReceipient;
		$email->subject = KText::sprintf('Recommendation from %s',$nameSender);
		$email->body = $message;

		$succ = KenedoPlatform::p()->sendEmail($email->fromEmail, $email->fromName, $email->to, $email->subject, $email->body);

		if ($succ === true) {
			$response = new StdClass();
			$response->success = true;
			$response->text = KText::_('Email sent.');
			echo(json_encode($response));
		}
		else {
			$response = new StdClass();
			$response->success = false;
			$response->text = KText::_('An error occured.');
			echo(json_encode($response));
		}

	}

}