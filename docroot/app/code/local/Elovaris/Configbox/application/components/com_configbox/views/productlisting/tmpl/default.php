<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewProductlisting */
?>
<div id="com_configbox">
<div id="view-productlisting">
<div id="template-<?php echo hsc($this->template);?>">
<div id="view-product-listing-id-<?php echo intval($this->listing->id);?>">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

<?php if ($this->showPageHeading) { ?>
	<h1 class="componentheading"><span class="product-listing-title"><?php echo hsc($this->pageHeading);?></span></h1>
<?php } ?>

<div class="product-listing product-listing-id-<?php echo intval($this->listing->id);?>">

	<?php if ($this->listing->description) { ?>
		<div class="product-listing-description"><?php echo $this->listing->description;?></div>
	<?php } ?>

	<ul class="listing-products">
		<?php foreach ($this->products as $product) { ?>
			<li class="listing-product product-id-<?php echo intval($product->id);?>">

				<?php if (ConfigboxPermissionHelper::canQuickEdit()) echo ConfigboxQuickeditHelper::renderProductButtons($product);?>

				<div class="listing-product-image-wrapper">
					<?php if ($product->longdescription) { ?>
						<a class="listing-product-image-link" href="<?php echo $product->linkdetails;?>" title="<?php echo hsc(KText::sprintf('See details for %s', $product->title));?>">
							<img class="product-image" src="<?php echo $product->imagesrc;?>" alt="<?php echo hsc($product->title);?>" />
						</a>
					<?php } elseif ($product->isConfigurable) { ?>
						<a class="listing-product-image-link" href="<?php echo $product->linkconfigure;?>" title="<?php echo hsc(KText::sprintf('Configure %s', $product->title));?>">
							<img class="product-image" src="<?php echo $product->imagesrc;?>" alt="<?php echo hsc($product->title);?>" />
						</a>
					<?php } else { ?>
						<a class="listing-product-image-link" href="<?php echo $product->linkbuy;?>" title="<?php echo hsc(KText::sprintf('Buy', $product->title));?>">
							<img class="product-image" src="<?php echo $product->imagesrc;?>" alt="<?php echo hsc($product->title);?>" />
						</a>
					<?php } ?>
				</div>

				<?php if (ConfigboxPermissionHelper::canSeePricing()) { ?>

					<?php if ($product->custom_price_text) { ?>
						<span class="listing-product-price"><?php echo hsc($product->custom_price_text);?></span>
					<?php } elseif ($product->price != 0) { ?>
						<?php if ($product->wasPrice != 0) { ?>
							<span class="listing-was-price listing-was-price-<?php echo intval($product->id);?>"> <?php echo cbprice($product->wasPrice);?></span>
						<?php } ?>
						<span class="listing-product-price"><?php echo cbprice($product->price); ?> <?php echo hsc($product->priceLabel);?></span>
					<?php } ?>

					<?php if ($product->custom_price_text_recurring) { ?>
						<span class="listing-product-price-recurring"><?php echo hsc($product->custom_price_text_recurring);?></span>
					<?php } elseif ($product->priceRecurring != 0) { ?>
						<?php if ($product->wasPriceRecurring != 0) { ?>
							<span class="listing-was-price-recurring listing-was-price-recurring-<?php echo intval($product->id);?>"> <?php echo cbprice($product->wasPriceRecurring);?></span>
						<?php } ?>
						<span class="listing-product-price-recurring"><?php echo cbprice($product->priceRecurring); ?> <?php echo hsc($product->priceLabelRecurring);?></span>
					<?php } ?>

				<?php } ?>

				<?php if ($product->longdescription) { ?>
					<h2 class="listing-product-title"><a href="<?php echo $product->linkdetails;?>"><?php echo hsc($product->title);?></a></h2>
				<?php } elseif ($product->isConfigurable) { ?>
					<h2 class="listing-product-title"><a href="<?php echo $product->linkconfigure;?>"><?php echo hsc($product->title);?></a></h2>
				<?php } else { ?>
					<h2 class="listing-product-title"><a href="<?php echo $product->linkbuy;?>"><?php echo hsc($product->title);?></a></h2>
				<?php } ?>

				<?php if ($product->description) { ?>
					<div class="listing-product-description"><?php echo $product->description;?></div>
				<?php } ?>

				<div class="clearright"></div>

				<?php if ($product->isConfigurable) { ?>
					<a class="button-frontend-small link-configure" href="<?php echo $product->linkconfigure;?>"><?php echo hsc(KText::sprintf('Configure %s', $product->title));?></a>
				<?php } ?>

				<?php if ($product->show_buy_button) { ?>
					<a class="button-frontend-small link-buy" rel="nofollow" title="<?php echo hsc(KText::sprintf('Buy %s', $product->title));?>" href="<?php echo $product->linkbuy;?>"><?php echo KText::_('Buy');?></a>
				<?php } ?>

				<?php if ($product->longdescription) { ?>
					<a class="button-frontend-small link-details" title="<?php echo hsc(KText::sprintf('See details for %s', $product->title));?>" href="<?php echo $product->linkdetails;?>"><?php echo KText::_('Details');?></a>
				<?php } ?>

				<?php if ( $product->showReviews ) { ?>
					<div class="button-frontend-small listing-product-reviews" data-product-id="<?php echo intval($product->id);?>"><?php echo ConfigboxRatingsHelper::renderProductRatingsPopup('<a>'.KText::_('Reviews').'</a>', $product);?></div>
				<?php } ?>

				<div class="clear"></div>

			</li>
		<?php } ?>
	</ul>
	<div class="clear"></div>
</div>

</div></div></div></div></div>