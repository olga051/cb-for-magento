<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewEmailinvoice extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'emailinvoice';

	/**
	 * @var object $orderRecord
	 * @see ConfigboxModelOrderrecord::getOrderRecord
	 */
	public $orderRecord;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function prepareTemplateVars() {

		$shopData = ConfigboxStoreHelper::getStoreRecord();
		$this->assignRef('shopData', $shopData);
		
		$customer = ConfigboxUserHelper::getUser();
		$this->assignRef('customer', $customer);
		
		$this->renderView();
	}
	
}