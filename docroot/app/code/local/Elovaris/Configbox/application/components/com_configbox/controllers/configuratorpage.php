<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxControllerConfiguratorpage extends KenedoController {

	/**
	 * @return ConfigboxModelConfiguratorpage $model
	 */
	protected function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelConfiguratorpage');
	}

	/**
	 * @return ConfigboxViewConfiguratorpage
	 */
	protected function getDefaultView() {
		return KenedoView::getView('ConfigboxViewConfiguratorpage');
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewList() {
		return NULL;
	}

	/**
	 * @return NULL
	 */
	protected function getDefaultViewForm() {
		return NULL;
	}

	/**
	 * Renders JSON data about with missing selections for the current cart position.
	 *
	 * @throws Exception
	 */
	function getMissingSelections() {
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
		$missingElements = $positionModel->getMissingElements();
		echo json_encode($missingElements);
	}

	/**
	 * Requested via XHR by JS: com_configbox.sendSelectionToServer()
	 * It handles a make a change in the configuration, and sends a JSON string with comprehensive info and instructions
	 * back. The data is primarily handled by JS com_configbox.processServerResponse(). There is a JS event called
	 * 'configurationUpdated' that is triggered on #com_configbox for which anyone can write handlers for.
	 *
	 * @throws Exception
	 */
	function makeSelection() {

		KLog::start('99 total for ConfigboxControllerConfiguratorpage:makeSelection');

		KLog::start('01 prepare');

		// Set input variables

		// The element that gets a new selection
		$elementId = KRequest::getInt('element_id',false);
		// The value of the new selection
		$value = KRequest::getString('value',false);
		// The page the user is on (for performance reasons, saves a lookup)
		$pageId = KRequest::getInt('page_id',0);
		// Indicates that the user gave permission for automatic changes that need permission (setting in element and
		// option called 'confirm automatic changes')
		$gotConfirmation = (KRequest::getInt('confirmed', 0) != 0);

		// That array will be the JSON object sent back to the client
		$response = array();

		KLog::start('01a get position model');
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
		KLog::stop('01a get position model');

		KLog::start('01b get configuration instance');
		$configuration = ConfigboxConfiguration::getInstance();
		KLog::stop('01b get configuration instance');

		KLog::start('01c get cart position id');
		$response['cart_position_id'] = $positionModel->getId();
		KLog::stop('01c get cart position id');

		KLog::start('01d get element');
		$element = ConfigboxElementHelper::getElement($elementId);
		KLog::start('01d get element');

		// Store the requested change for later reference
		$response['requestedChange']['elementId'] = $element->id;
		$response['requestedChange']['elementTitle'] = $element->title;
		$response['requestedChange']['text'] = (count($element->options) == 0) ? $value : '';
		$response['requestedChange']['xrefId'] = (count($element->options) != 0) ? $value : 0;
		$response['requestedChange']['optionTitle'] = NULL;

		if (!$element) {
			$response['error'] = 'ERROR: Invalid element ID supplied (ID was "'.var_export($elementId, true).'")';
			$this->sendResponse($response);
			exit();
		}

		KLog::start('01e get original value');
		// Get the original value (in case the frontend selection needs restoration)
		$response['originalValue']['text'] = $configuration->getElementTextEntry($elementId);
		$response['originalValue']['xrefId'] = $configuration->getElementXrefId($elementId);
		$response['originalValue']['outputValue'] = $element->getOutputValue($response['originalValue']['xrefId'], $response['originalValue']['text']);
		KLog::stop('01e get original value');

		if (!$response['cart_position_id']) {
			$response['error'] = 'ERROR: No cart position id set';
			$this->sendResponse($response);
			exit();
		}

		// Check if the selection for that element is valid
		$validationResponse = $element->isValidValue($value);

		// Return if invalid
		if ($validationResponse !== true) {
			$response['error'] = $validationResponse;
			$this->sendResponse($response);
			exit();
		}

		KLog::stop('01 prepare');

		$originallyApplyingElementIds = $positionModel->getCurrentlyApplyingElementIds();

		KLog::start('02 inconsistencies');

		// Prepare the proposed changes to check for inconsistencies
		$proposedChanges = array(
			$elementId => array(
				'element_id' => $response['requestedChange']['elementId'],
				'element_option_xref_id' => $response['requestedChange']['xrefId'],
				'text' => $response['requestedChange']['text'],
			),
		);

		// Get inconsistencies
		$inconsistencies = $positionModel->getInconsistencies($proposedChanges, $originallyApplyingElementIds);

		$configuration->unsetSimSelections();

		// See if we need confirmation for automatic changes
		$needConfirmation = false;
		foreach ($inconsistencies as $inconsistency) {
			if ($inconsistency['confirm_deselect']) {
				$needConfirmation = true;
				break;
			}
		}

		// If we got need confirmation for automatic changes, prepare the text for it and send the response
		if ($needConfirmation == true && $gotConfirmation == false ) {
			$response['inconsistencies'] = $inconsistencies;
			$response['confirmationText'] = $this->getConfirmationText($inconsistencies);
			$this->sendResponse($response);
			exit();
		}

		KLog::start('02a resolve_inconsistencies');

		// Init the configurationChanges array
		$response['configurationChanges']['add'] = array();
		$response['configurationChanges']['remove'] = array();

		// Remove (or change) inconsistent selections (for real, not simulated)
		foreach ($inconsistencies as $inconsistency) {

			if (!empty($inconsistency['replacement'])) {

				// Make the selection
				KLog::log('Making real selection for "'.$inconsistency['element_id'].'": xref_id "'.$inconsistency['replacement']['element_option_xref_id'].'", text "'.$inconsistency['replacement']['text'].'"', 'inconsistencies');
				$configuration->setSelection($inconsistency['element_id'], $inconsistency['replacement']['element_option_xref_id'], $inconsistency['replacement']['text']);

				// Add instructions for the frontend to add (or change) the element's selection visually
				$response['configurationChanges']['add'][$inconsistency['element_id']] = array (
					'outputValue'	=> $inconsistency['replacement']['output_value'],
					'xrefId'		=> $inconsistency['replacement']['element_option_xref_id'],
					'text'			=> $inconsistency['replacement']['text'],
				);

			}
			else {

				// Remove the selection
				KLog::log('Making real selection for "'.$inconsistency['element_id'].'". Unsetting', 'inconsistencies');
				$configuration->setSelection($inconsistency['element_id'], 0, '');

				// Add instructions for the frontend to remove the selection visually
				$response['configurationChanges']['remove'][$inconsistency['element_id']] = array ('xrefId'=>$inconsistency['element_option_xref_id']);

			}

		}

		KLog::stop('02a resolve_inconsistencies');

		KLog::stop('02 inconsistencies');


		KLog::start('03 set_selection');

		// Make the requested change
		$saveResponse = $this->setElementValue($elementId, $value);

		// The method adds things to the response, so we merge here
		$response = array_merge($response, $saveResponse);

		// If we got an error, end things here
		if (isset($response['error'])) {
			$this->sendResponse($response);
			exit();
		}

		KLog::stop('03 set_selection');


		// Get autoselects and text prefills
		KLog::start('05 find autoselects');
		$autoSelects = $positionModel->getAutoSelectItems(true, $originallyApplyingElementIds);
		KLog::stop('05 find autoselects');
		$response['autoselect_ignored'] = $originallyApplyingElementIds;
		// Set those autoselects and set the configurationChanges (so that the client's elements are updated too)
		KLog::start('06 set autoselects');
		foreach ($autoSelects as $autoSelect) {
			// Make the selection
			$configuration->setSelection($autoSelect['elementId'], $autoSelect['xrefId'], $autoSelect['text']);
			// Add instructions for the frontend to add (or change) the element's selection visually
			$response['configurationChanges']['add'][$autoSelect['elementId']] = $autoSelect;
			// Unset any conflicting removal instructions
			if (isset($response['configurationChanges']['remove'][$autoSelect['elementId']])) {
				unset($response['configurationChanges']['remove'][$autoSelect['elementId']]);
			}
		}
		KLog::stop('06 set autoselects');

		// Get the dynamic validation values (min/max values that are calculated)
		KLog::start('07 validation values');
		$response['validationValues'] = $positionModel->getDynamicValidationValues($pageId);
		KLog::stop('07 validation values');

		// Deal with item visibility (Instructions for the frontend on what elements/options shall be hidden/shown)
		KLog::start('08 itemvisibility');
		$response['itemVisibility'] = $positionModel->getPageItemVisibility($pageId);
		KLog::stop('08 itemvisibility');

		// Get the updated position details to the browser
		KLog::start('08 position');
		$response['cartPositionDetails'] = $positionModel->getPositionDetails();
		KLog::stop('08 position');

		// Get data for all prices for update in the browser
		if (ConfigboxPermissionHelper::canSeePricing()) {
			KLog::start('09 pricing');
			$response['pricing'] = $positionModel->getPricing();
			KLog::stop('09 pricing');
		}

		// Finally send the response
		$this->sendResponse($response);

	}

	/**
	 * Helper method for self::updateElement to render the JSON data.
	 * @param $response
	 */
	protected function sendResponse($response) {

		if (defined('CONFIGBOX_ENABLE_PERFORMANCE_TRACKING') && CONFIGBOX_ENABLE_PERFORMANCE_TRACKING) {
			KLog::stop('99 total for ConfigboxControllerConfiguratorpage:makeSelection');
			$response['performance']['counts'] = KLog::getCounts();
			$response['performance']['counts']['queries'] = KenedoPlatform::getDb()->getQueryCount();
			$response['performance']['queries'] = KenedoPlatform::getDb()->getQueryList();
			$response['performance']['totalQueryTime'] = KenedoPlatform::getDb()->getTotalQueryTime();
			$response['performance']['timings'] = KLog::getTimings();
		}

		ob_clean();
		header('Cache-Control: no-cache, must-revalidate');

		ob_start();

		// This is for elements that use the widget 'fileupload'.
		// File uploads don't use JSON, but the makeSelection request goes as POST request into a hidden iframe. So
		// We write a script tag that writes the JSON response to a designated place for the callback to pick it up.
		if (KRequest::getInt('file-upload')) {
			?>
			<script type="text/javascript">
				window.parent.com_configbox.configuratorFileUploadResponseData = <?php echo json_encode($response);?>;
			</script>
			<?php
		}
		else {
			header('Content-Type: application/json', true);
			echo json_encode($response);
		}

		$data = ob_get_clean();

		if (defined('CONFIGBOX_CONFIGURATOR_NO_COMPRESSSION')) {
			echo $data;
			exit();
		}

		if (KenedoPlatform::getName() == 'magento') {
			echo $data;
			exit();
		}

		// Here we just deal with encoding
		if (ini_get('zlib.output_compression')) {
			echo $data;
		}
		elseif (!isset($_SERVER['HTTP_ACCEPT_ENCODING'])) {
			echo $data;
		}
		elseif (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false) {
			header('Content-Encoding: gzip');
			echo gzencode($data, 2);
		}
		elseif (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'x-gzip') !== false) {
			header('Content-Encoding: x-gzip');
			echo gzencode($data, 2);
		}
		else {
			echo $data;
		}

		// Do a blunt exit to avoid any after-render processing of the platform
		exit();

	}

	protected function setElementValue($elementId,$value) {

		$element = ConfigboxElementHelper::getElement($elementId);

		if (!$element) {
			$response['error'] = 'element not found';
			return $response;
		}

		if (!$element->applies()) {
			$response['error'] = 'element does not apply';
			return $response;
		}

		$return = $element->isValidValue($value);

		if ($return !== true) {
			$response['error'] = $return;
			return $response;
		}


		if ($element->widget == 'fileupload') {

			$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
			$positionId = $positionModel->getId();

			$file = $_FILES['configbox-widget-fileupload-filefield'];
			$fileExtension = substr(strrchr($file['name'],'.'),1);

			$value = $positionId.'-'.$element->id.'.'.$fileExtension;
			$destination = CONFIGBOX_DIR_CONFIGURATOR_FILEUPLOADS .DS. $value;

			move_uploaded_file($file['tmp_name'],  $destination);
			chmod($destination,0777);

		}

		$configuration = ConfigboxConfiguration::getInstance();

		// For text fields, update the text column instead or option_id
		if ($element->type == 'text') {
			$configuration->setSelection($element->id,0,$value);
		}
		else {
			$configuration->setSelection($element->id,$value);
		}

		$configuration->unsetSimSelections();

		// Include the updated element info in the response for reference
		$response['requestedChange']['elementId'] = $element->id;
		$response['requestedChange']['elementTitle'] = $element->title;
		$response['requestedChange']['text'] = $configuration->getElementTextEntry($element->id);
		$response['requestedChange']['xrefId'] = $configuration->getElementXrefId($element->id);
		$response['requestedChange']['outputValue'] = $element->getOutputValue();

		return $response;

	}

	/**
	 * @param array $inconsistencies
	 * @return string Localized plain text for the confirmation question
	 * @see ConfigboxModelCartposition::getInconsistencies
	 */
	protected function getConfirmationText($inconsistencies) {

		// Those contain infos on element's that got to loose their selection..
		$deselectionLines = array();
		// ..and these contain infos on element's with an alternative selection.
		$replacementLines = array();

		foreach ($inconsistencies as $elementId=>$inconsistency) {
			if ($inconsistency['confirm_deselect']) {
				if (isset($inconsistency['replacement'])) {
					$replacementLines[] = $inconsistency['element_title'] . ': ' .$inconsistency['replacement']['replaced_value'] . ' '.KText::_('to').' ' .$inconsistency['replacement']['output_value'];
				}
				else {
					$deselectionLines[] = $inconsistency['element_title'] . ': '.$inconsistency['output_value'];
				}
			}
		}

		$text = '';

		// Prepare the confirmation text
		if (count($deselectionLines) || count($replacementLines)) {

			if (count($deselectionLines)) {
				$text .= KText::_('With this selection, following elements will be deselected:', true). "\n\n";
				$text .= implode("\n", $deselectionLines);
			}

			if (count($deselectionLines) and count($replacementLines)) {
				$text .= "\n\n";
			}

			if (count($replacementLines)) {
				$text .= KText::_('With this selection, following selections will be changed:',true). "\n\n";
				$text .= implode("\n", $replacementLines);
			}

		}

		return $text;

	}

}