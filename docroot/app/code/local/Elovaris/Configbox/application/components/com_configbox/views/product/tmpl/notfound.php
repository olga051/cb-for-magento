<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewProductlisting */
?>
<div id="com_configbox">
	<div id="view-product">
		<div id="template-notfound">
			<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">
				<?php echo KText::_('Product not found.');?>
			</div>
		</div>
	</div>
</div>