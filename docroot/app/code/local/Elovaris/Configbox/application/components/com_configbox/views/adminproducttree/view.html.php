<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdminproducttree extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'adminproducttree';

	/**
	 * @var array $tree
	 * @see ConfigboxModelAdminproducttree::getTree
	 */
	public $tree;

	/**
	 * @var array[] - Array with data for checking what tree nodes shall be open
	 * @see ConfigboxViewAdminproducttree::prepareTemplateVars
	 */
	public $openIds;

	/**
	 * @var string $treeUpdateUrl URL to use for updates.
	 * @see com_configbox.refreshProductTree
	 */
	public $treeUpdateUrl;

	/**
	 * @return ConfigboxModelAdminproducttree
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdminproducttree');
	}

	function prepareTemplateVars() {
		
		$model = KenedoModel::getModel('ConfigboxModelAdminproducttree');
		
		$tree = $model->getTree(false, KRequest::getInt('only_product_id', NULL));
		$this->assignRef('tree', $tree);

		if (KRequest::getString('open_tree_ids')) {
			$openIds = json_decode(KRequest::getString('open_tree_ids'),true);
		}
		else {
			$openIds = array('products'=>array(),'pages'=>array(),'elements'=>array());
		}

		$treeUpdateUrl = KLink::getRoute('index.php?option=com_configbox&controller=adminproducttree&format=raw', false);
		$this->assignRef('treeUpdateUrl', $treeUpdateUrl);
		
		$this->assign('openIds', $openIds);
		
		$this->addViewCssClasses();

	}
	
}