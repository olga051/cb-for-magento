<?php
class KenedoTimeHelper {
	
	static $outputTimeZoneName = 'Europe/Vienna';
	static $outputTimeZone = NULL;
	
	static $normalizedTimeZoneName = 'UTC';
	static $normalizedTimeZone = NULL;
	
	public static function setOutputTimeZoneName($timeZoneName = '') {
		if ($timeZoneName) {
			self::$outputTimeZoneName = $timeZoneName;
		}
		self::$outputTimeZone = timezone_open(self::$outputTimeZoneName);
	}
	
	public static function setNormalizedTimeZoneName($timeZoneName = '') {
		if ($timeZoneName) {
			self::$normalizedTimeZoneName = $timeZoneName;
		}
		self::$normalizedTimeZone = timezone_open(self::$normalizedTimeZoneName);
	}
	
	public static function getFormatted($timeString = 'NOW', $format = '') {
		
		if (!self::$outputTimeZone) {
			self::setNormalizedTimeZoneName();
			self::setOutputTimeZoneName();
		}
		
		$format = self::getFormat($format);
		$timeString = self::getTimeString($timeString);
		
		try {
			$time = new DateTime($timeString, self::$normalizedTimeZone );
			$time->setTimezone(self::$outputTimeZone);
		}
		catch(Exception $e) {
			return '';
		}
		$formatted = $time->format($format);
		
		return $formatted;
		
	}
	
	public static function getNormalizedTime($timeString = 'NOW', $format = '') {
		
		if (!self::$outputTimeZone) {
			self::setNormalizedTimeZoneName();
			self::setOutputTimeZoneName();
		}
		
		$format = self::getFormat($format);
		$timeString = self::getTimeString($timeString);
		
		try {
			$time = new DateTime($timeString, self::$outputTimeZone );
			$time->setTimezone( self::$normalizedTimeZone );
			$formatted = $time->format($format);
		}
		catch(Exception $e) {
			return '';
		}
		return $formatted;
	}
	
	public static function getFormattedOnly($timeString = 'NOW', $format = '') {
		
		if (!self::$outputTimeZone) {
			self::setNormalizedTimeZoneName();
			self::setOutputTimeZoneName();
		}
		
		$format = self::getFormat($format);
		$timeString = self::getTimeString($timeString);
		
		try {
			$time = new DateTime($timeString, self::$normalizedTimeZone );
		}
		catch(Exception $e) {
			return '';
		}
		
		$formatted = $time->format($format);
		
		return $formatted;
		
	}
	
	protected static function getTimeString($string) {

		if (is_numeric($string)) {
			$string = '@'.$string;
		}
		
		return $string;
	}
	
	protected static function getFormat($format) {
		
		if (strtolower($format) == 'datetime') {
			$format = "Y-m-d H:i:s";
		}
		
		if (strtolower($format) == 'date') {
			$format = KText::_('KENEDO_DATEFORMAT_PHP_DATE', 'Y-m-d');
		}
		
		if (strtolower($format) == 'timestamp') {
			$format = "U";
		}
		
		if (empty($format)) {
			$format = KText::_('KENEDO_DATEFORMAT_PHP_DATE_AND_TIME', 'Y-m-d H:i');
		}

		return $format;
		
	}
	
}