<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewAdminoptionassignment */
?>
<div id="view-<?php echo hsc($this->view);?>" class="<?php $this->renderViewCssClasses();?>">

<form class="kenedo-details-form" method="post" enctype="multipart/form-data" action="<?php echo KLink::getRoute('index.php');?>">
		
	<?php echo (count($this->pageTasks)) ? KenedoViewHelper::renderTaskItems($this->pageTasks) : ''; ?>
	
	<?php if (!empty($this->pageTitle)) { ?><h1 class="kenedo-page-title"><?php echo hsc($this->pageTitle);?></h1><?php } ?>
	
	<div class="clear"></div>
	
	<div class="kenedo-messages">
		<div class="kenedo-messages-error"></div>
		<div class="kenedo-messages-notice"></div>
	</div>
	
	<div class="xref-data">
		<h2><?php echo KText::_('Option Assignment Settings');?></h2>
		<p><?php echo KText::_('These settings only apply to the assignment to this element. You choose an existing option or create a new option along with the assignment.');?></p>
		<div class="kenedo-properties">
			<?php 
			
			foreach($this->properties as $property) {
				$property->setData($this->record);
				if ($property->usesWrapper()) {
					?>
				 	<div id="<?php echo $property->getCssId();?>" class="<?php echo $property->renderCssClasses();?>">
				 		<?php if ($property->doesShowAdminLabel()) { ?>
				 			<div class="property-label"><?php echo $property->getLabelAdmin();?></div>
				 		<?php } ?>
				 		<div class="property-body"><?php echo $property->getBodyAdmin();?></div>
				 	</div>
					<?php
				} 
				else {
					echo $property->getBodyAdmin();
				}
			}
			?>
			<div class="clear"></div>
		</div>
		
	</div>
	
	<div class="option-data">
		<h2><?php echo KText::_('Global Option Settings');?></h2>
		<p><?php echo KText::_('These settings apply to every assignment of the option. Changes to these settings affect all assignments of this option.');?></p>
		<div class="option-fields-target">
			<div class="kenedo-properties">
				<?php 
				foreach($this->optionProperties as $propertyName=>$property) {
					$property->setData($this->option);
					if ($property->usesWrapper()) {
						?>
					 	<div id="<?php echo $property->getCssId();?>" class="<?php echo $property->renderCssClasses();?>">
					 		<?php if ($property->doesShowAdminLabel()) { ?>
					 			<div class="property-label"><?php echo $property->getLabelAdmin();?></div>
					 		<?php } ?>
					 		<div class="property-body"><?php echo $property->getBodyAdmin();?></div>
					 	</div>
						<?php
					} 
					else {
						echo $property->getBodyAdmin();
					}
				}
				?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
	<div class="clear"></div>
	
	<?php if (!empty($this->itemUsage)) { ?>
		<div class="kenedo-item-usage">
			<div class="kenedo-usage-message"><?php echo KText::_('The item is in use in the following entries:');?></div>
			<?php foreach ($this->itemUsage as $propertyName=>$items) { ?>
				<?php foreach ($items as $item) { ?>
				<div>
					<span class="kenedo-candelete-usage-entry-name"><?php echo hsc($propertyName);?></span> - <a class="kenedo-new-tab kenedo-candelete-usage-entry-link" href="<?php echo $item->link;?>" class="new-tab"><?php echo hsc($item->title);?></a>
				</div>
				<?php } ?>
			<?php } ?>
		</div>
	<?php } ?>

	<div class="kenedo-hidden-fields">

		<input type="hidden" id="option_assignment_load_url" name="option_assignment_load_url" value="<?php echo KLink::getRoute('index.php?option=com_configbox&controller=adminoptions&task=edit&id=%');?>" />

		<input type="hidden" id="option" 		name="option" 			value="<?php echo hsc($this->component);?>" />
		<input type="hidden" id="controller"	name="controller" 		value="<?php echo hsc($this->controllerName);?>" />
		<input type="hidden" id="task" 			name="task" 			value="" />
		<input type="hidden" id="ajax_sub_view" name="ajax_sub_view" 	value="<?php echo ($this->isAjaxSubview()) ? '1':'0';?>" />
		<input type="hidden" id="in_modal"		name="in_modal" 		value="<?php echo ($this->isInModal()) ? '1':'0';?>" />
		<input type="hidden" id="tmpl"			name="tmpl" 			value="component" />
		<input type="hidden" id="format"		name="format" 			value="raw" />
		<input type="hidden" id="id"			name="id" 				value="<?php echo intval($this->record->id); ?>" />
		<input type="hidden" id="lang"			name="lang" 			value="<?php echo hsc(KenedoPlatform::p()->getLanguageUrlCode());?>" />
		<!-- unencoded return url "<?php echo $this->returnUrl;?>" -->
		<input type="hidden" id="return" 		name="return" 			value="<?php echo KLink::base64UrlEncode($this->returnUrl);?>" />
		<input type="hidden" id="form_custom_1" name="form_custom_1" 	value="<?php echo hsc(KRequest::getString('form_custom_1'));?>" />
		<input type="hidden" id="form_custom_2" name="form_custom_2" 	value="<?php echo hsc(KRequest::getString('form_custom_2'));?>" />
		<input type="hidden" id="form_custom_3" name="form_custom_3" 	value="<?php echo hsc(KRequest::getString('form_custom_3'));?>" />
		<input type="hidden" id="form_custom_4" name="form_custom_4" 	value="<?php echo hsc(KRequest::getString('form_custom_4'));?>" />
		<?php if (KenedoPlatform::getName() == 'magento') { ?>
			<input type="hidden" id="form_key" 		name="form_key" 		value="<?php echo Mage::getSingleton('core/session')->getFormKey();?>" />
		<?php } ?>
	</div>
	
</form>
</div>
