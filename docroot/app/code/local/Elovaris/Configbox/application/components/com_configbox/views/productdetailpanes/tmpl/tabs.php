<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewProductdetailpanes */
?>
<div class="product-detail-panes product-detail-panes-type-tabs">
	<ul class="product-detail-panes-headings">
	<?php foreach ($this->product->productDetailPanes as $i=>$pane) { ?>
		<li data-pane-id="<?php echo intval($i+1);?>" class="<?php echo ($i == 0) ? 'active-tab ':'';?>product-detail-panes-heading-item product-detail-panes-heading-item-<?php echo $i+1;?><?php echo ($pane->usesHeadingIcon) ? ' uses-icon':''; ?><?php echo (trim($pane->css_classes) != '')  ? ' '.hsc($pane->css_classes):'';?>">
			<a>
				<?php if ($pane->usesHeadingIcon) { ?>
					<img class="product-detail-panes-heading-icon" src="<?php echo $pane->headingIconSrc;?>" />
				<?php } ?>
				<span class="product-detail-panes-heading-text"><?php echo hsc($pane->heading);?></span>
			</a>
		</li>
	<?php } ?>
	</ul>

	<div class="product-detail-panes-contents">
		<?php foreach ($this->product->productDetailPanes as $i=>$pane) { ?>
			<div class="product-detail-panes-content<?php echo ($i == 0) ? ' active-pane':'';?>" data-pane-id="<?php echo intval($i+1);?>">
				<?php echo $pane->content;?>
			</div>
		<?php } ?>
	</div>
</div>