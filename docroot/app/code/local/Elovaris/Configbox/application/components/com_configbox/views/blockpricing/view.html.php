<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewBlockpricing extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var KStorage $params Joomla module parameters
	 */
	public $params;

	/**
	 * @var string $cssClass Either 'pricing-regular' or 'pricing-recurring'
	 */
	public $cssClass;

	/**
	 * @var array $pricing Big array with all prices, quantity, tax, delivery, payment
	 * @see ConfigboxModelCartposition::getPricing()
	 */
	public $pricing;

	/**
	 * @var int $productId
	 */
	public $productId;

	/**
	 * @var int $pageId Page ID the visitor is currently on
	 */
	public $pageId;

	/**
	 * @var bool $showPages Indicates if individual pages should be displayed
	 */
	public $showPages;

	/**
	 * @var bool $showElements Indicates if individual elements should be displayed
	 */
	public $showElements;

	/**
	 * @var bool $showPrices Indicates if pricing should be displayed
	 */
	public $showPrices;

	/**
	 * @var bool $showElementPrices Indicates if individual element prices should be displayed
	 */
	public $showElementPrices;

	/**
	 * @var bool $showTaxes Indicates if tax summary should be displayed
	 */
	public $showTaxes;

	/**
	 * @var bool $showDelivery Indicates if delivery method infos should be displayed
	 */
	public $showDelivery;

	/**
	 * @var bool $showPayment Indicates if payment method infos should be displayed
	 */
	public $showPayment;

	/**
	 * @var bool $showCartButton Indicates if add-to-cart button should be displayed
	 */
	public $showCartButton;

	/**
	 * @var bool $showNetInB2c Indicates if net total should be displayed when in B2C mode
	 */
	public $showNetInB2c;

	/**
	 * @var int $expandPages If pages should be expanded initially 0 for no, 1 for yes, 2 for only the current one.
	 */
	public $expandPages;

	/**
	 * @var bool $isRegular Indicates if the template deals with regular pricing (otherwise recurrig pricing)
	 */
	public $isRegular;

	/**
	 * @var string $mode Either 'b2b' or 'b2c', depending on customer group setting
	 */
	public $mode;

	/**
	 * @var string $labelKey Either 'priceLabel' or 'priceRecurringLabel'
	 */
	public $labelKey;

	/**
	 * @var string $productPriceKey Either 'productPrice' or 'productPriceRecurring'
	 */
	public $productPriceKey;

	/**
	 * @var string $priceKey Either 'price' or 'priceRecurring'
	 */
	public $priceKey;

	/**
	 * @var string $pricePerItemGrossKey Either 'pricePerItemGross' or 'pricePerItemRecurringGross'
	 */
	public $pricePerItemGrossKey;

	/**
	 * @var string $pricePerItemNetKey Either 'pricePerItemNet' or 'pricePerItemRecurringNet'
	 */
	public $pricePerItemNetKey;

	/**
	 * @var string $totalGrossKey Either 'priceGross' or 'priceRecurringGross'
	 */
	public $totalGrossKey;

	/**
	 * @var string $totalNetKey Either 'priceNet' or 'priceRecurringNet'
	 */
	public $totalNetKey;

	/**
	 * @var string $addToCartLinkClasses CSS classes for the add-to-cart button
	 */
	public $addToCartLinkClasses;

	/**
	 * @var string $addToCartLink URL for adding the product to the cart
	 */
	public $addToCartLink;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

	function display() {

		if (empty($this->params)) {
			$params = new KStorage();
			$this->assignRef('params', $params);
		}
		
		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
		
		$positionId = $positionModel->getId();

		if (!$positionId) {
			return;
		}

		$position = $positionModel->getPosition();

		$pricing = $positionModel->getPricing();
		if (!$pricing) {
			return;
		}
		
		$this->assignRef('pricing', $pricing);

		$productModel = KenedoModel::getModel('ConfigboxModelProduct');
		$product = $productModel->getProduct($position->prod_id);

		$pageId = KRequest::getInt('page_id', NULL);
		if ($pageId === NULL) {
			$pageId = $product->firstPageId;
		}
		
		$groupData = KenedoObserver::triggerEvent('onConfigboxGetGroupData', array(), true);
		$b2b = (boolean)$groupData->b2b_mode;
		$this->assign('mode', ($b2b) ? 'b2b':'b2c');

		$this->assign('showDelivery', !CONFIGBOX_DISABLE_DELIVERY && $product->pm_show_delivery_options);
		$this->assign('showPayment', $product->pm_show_payment_options);
		$this->assign('showNetInB2c', $product->pm_show_net_in_b2c);
		
		$this->assign('pageId', $pageId);
		$this->assign('productId', $position->prod_id);

		$link = KLink::getRoute('index.php?option=com_configbox&view=cart&task=finishConfiguration&cart_position_id='.$positionId);
		$this->assignRef('addToCartLink', $link);

		$this->assign('addToCartLinkClasses','add-to-cart onAjaxCompleteOnly');

		$this->assign('deliveryNetKey',		'priceNet');
		$this->assign('shippingNetTaxKey',	'priceTax');
		$this->assign('shippingNetGrossKey','priceGross');
			
		$this->assign('paymentNetKey',		'priceNet');
		$this->assign('paymentTaxKey',		'priceTax');
		$this->assign('paymentGrossKey',	'priceGross');


		if ($product->pm_regular_show_overview) {
			
			$this->assignRef('showPrices', 			$product->pm_regular_show_prices);
			$this->assignRef('showPages', 			$product->pm_regular_show_categories);
			$this->assignRef('showElements', 		$product->pm_regular_show_elements);
			$this->assignRef('showElementPrices', 	$product->pm_regular_show_elementprices);
			$this->assignRef('expandPages', 		$product->pm_regular_expand_categories);
			$this->assign('showTaxes', 				$product->pm_regular_show_taxes);
			$this->assign('showCartButton', 		$product->pm_regular_show_cart_button);
			$this->assign('cssClass', 				'pricing-regular');
			$this->assign('priceKey', 				'price');
			$this->assign('productPriceKey', 		'productPrice');
			$this->assign('labelKey', 				'priceLabel');
			$this->assign('totalKey', 				'price');
			
			$this->assign('pricePerItemNetKey', 	'pricePerItemNet');
			$this->assign('pricePerItemTaxKey', 	'pricePerItemTax');
			$this->assign('pricePerItemGrossKey', 	'pricePerItemGross');
			
			$this->assign('taxRateKey',				'taxRate');
			
			$this->assign('totalNetKey',			'priceNet');
			$this->assign('totalTaxKey',			'priceTax');
			$this->assign('totalGrossKey',			'priceGross');
			
			$this->assign('isRegular',			true);
			
			$regularTree = $this->getViewOutput();
			
		}
		else {
			$regularTree = '';
		}
		
		if ($product->pm_recurring_show_overview) {
			
			$this->assignRef('showPrices', 			$product->pm_recurring_show_prices);
			$this->assignRef('showPages', 			$product->pm_recurring_show_categories);
			$this->assignRef('showElements', 		$product->pm_recurring_show_elements);
			$this->assignRef('showElementPrices', 	$product->pm_recurring_show_elementprices);
			$this->assignRef('expandPages', 		$product->pm_recurring_expand_categories);
			$this->assign('showTaxes', 				$product->pm_recurring_show_taxes);
			$this->assign('showCartButton', 		$product->pm_recurring_show_cart_button);
			$this->assign('cssClass', 				'pricing-recurring');
			$this->assign('priceKey', 				'priceRecurring');
			$this->assign('productPriceKey', 		'productPriceRecurring');
			$this->assign('labelKey', 				'priceRecurringLabel');
			$this->assign('totalKey', 				'priceRecurring');
			
			$this->assign('pricePerItemNetKey', 	'pricePerItemRecurringNet');
			$this->assign('pricePerItemTaxKey', 	'pricePerItemRecurringTax');
			$this->assign('pricePerItemGrossKey', 	'pricePerItemRecurringGross');
			
			$this->assign('taxRateKey',				'taxRateRecurring');
			
			$this->assign('totalNetKey',			'priceRecurringNet');
			$this->assign('totalTaxKey',			'priceRecurringTax');
			$this->assign('totalGrossKey',			'priceRecurringGross');
			
			$this->assign('isRegular',				false);
			
			$recurringTree = $this->getViewOutput();
		
		}
		else {
			$recurringTree = '';
		}
		$this->assign('priceKey', ( ConfigboxPrices::showNetPrices() ) ? 'priceNet':'priceGross');
		?>
		<div class="configbox-block block-pricing <?php echo hsc($this->params->get('moduleclass_sfx',''));?> mod_configboxprices <?php echo hsc($this->params->get('moduleclass_sfx',''));?>">
			
			<?php if (CONFIGBOX_BLOCKTITLE_PRICING) { ?><h2 class="block-title"><?php echo hsc(CONFIGBOX_BLOCKTITLE_PRICING);?></h2><?php } ?>
					
			<?php
			if ($product->pm_show_regular_first) {
				echo $regularTree."\n".$recurringTree;
			}
			else {
				echo $recurringTree."\n".$regularTree;
			}
			$this->renderView('footer');
			
			?>
		</div>
		<?php
		
	}

}
