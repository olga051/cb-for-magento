<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Model_Wishlist extends Mage_Wishlist_Model_Wishlist {

	/**
	 * Adds new product to wishlist.
	 * Returns new item or string on error.
	 *
	 * @param int|Mage_Catalog_Model_Product $product
	 * @param mixed $request
	 * @param bool $forciblySetQty
	 * @return Mage_Wishlist_Model_Item|string
	 */
	public function addNewItem($product, $request = null, $forciblySetQty = false) {

		KLog::log('Method started. param $request is:'. var_export($request, true), 'info');

		if ($product instanceof Mage_Catalog_Model_Product) {
			$productId = $product->getId();
			// Maybe force some store by wishlist internal properties
			$storeId = $product->hasWishlistStoreId() ? $product->getWishlistStoreId() : $product->getStoreId();
		} else {
			$productId = (int) $product;
			if ($request->getStoreId()) {
				$storeId = $request->getStoreId();
			} else {
				$storeId = Mage::app()->getStore()->getId();
			}
		}

		/* @var $product Mage_Catalog_Model_Product */
		$product = Mage::getModel('catalog/product')
			->setStoreId($storeId)
			->load($productId);

		$options = $product->getOptions();
		$requestData = $request->getData();

		foreach($options as $option) {

			if( $option->getType() == 'configbox' ) {

				$model = KenedoModel::getModel('ConfigboxModelCartposition');
				$positionId = $model->getId();

				if (!$positionId) {
					$cartModel = KenedoModel::getModel('ConfigboxModelCart');
					$cartId = $cartModel->getId();
					if (!$cartId) {
						$cartId = $cartModel->createCart();
					}
					$prepareModel = Mage::getModel('configbox/prepare');
					$cbProductId = $prepareModel->getCbProductId($productId);
					$positionId = $model->createPosition($cartId, $cbProductId);
				}

				$configuration = ConfigboxConfiguration::getInstance($positionId);

				// Now store selections in DB
				$configuration->storeSelectionsInDb();

				$configInfo = array(
					'order_id' => $positionId,
					'mage_prod_id' => $product->getId(),
					'prod_id' => $configuration->getProductId(),
					'items' => $configuration->getSelections(),
				);

				$requestData['options'] = $configInfo;

				$request->setData($requestData);

				$model->editPosition(array('finished'=>1));

			}
		}

		return parent::addNewItem ( $product, $request );
	}
}