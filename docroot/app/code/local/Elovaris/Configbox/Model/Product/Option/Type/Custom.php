<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Model_Product_Option_Type_Custom extends Mage_Catalog_Model_Product_Option_Type_Default {

	public function getOptionPrice($serializedConfigInfo, $basePrice) {
			
		KLog::log('Method started. Param values are: '. var_export(func_get_args(),true),'info');

		// Get the info about the configuration
		$configInfo = unserialize($serializedConfigInfo);

		KLog::log('ConfigInfo is: '. var_export($configInfo,true),'info');

		// Get the cart position model
		$model = KenedoModel::getModel('ConfigboxModelCartposition');
		// Get the old id for setting it back after all that
		$oldId = $model->getId();
		// Set the ID of the current configuration
		$model->setId($configInfo['order_id'], false);

		// Get the total net price of the order
		$positionDetails = $model->getPositionDetails();

		$includingTax = Mage::getStoreConfig('tax/calculation/price_includes_tax');

		if ($includingTax) {
			$price = $positionDetails->totalUnreducedGross;
		}
		else {
			$price = $positionDetails->totalUnreducedNet;
		}

		// Subtract the Magento price again (it is added as CB product base price in ConfigboxPrices)
		$price = $price - $basePrice;

		// Reset the cart position id
		$model->setId($oldId, true);
		KLog::log('Price is: '. $price,'info');
		return $price;

	}

	public function getOptionSku($optionValue, $skuDelimiter) {
		$sku = '';
		return $sku;
	}
	
	/**
	 * Validate user input for option
	 *
	 * @throws Mage_Core_Exception
	 * @param array $configInfo configInfo (apparently set in Elovaris_Configbox_Model_Quote)
	 * @return Mage_Catalog_Model_Product_Option_Type_Default
	 */
	public function validateUserValue($configInfo) {
		KLog::log('Method started. Param values are:'. var_export($configInfo,true),'info');

		$this->setData('user_value',serialize($configInfo));
		
		$this->setIsValid(true);
		return $this;
	}


	/**
	 * Prepare the configuration text display in the cart page
	 *
	 * @return mixed Prepared option value
	 */
	public function prepareForCart() {

		KLog::log('Method started, getUserValue gets us: '.var_export($this->getUserValue(),true),'info');

		$userValue = $this->getUserValue();
		KLog::log('Taking user value from getUserValue(), returning: '.var_export($userValue,true),'info');
		return $userValue;

	}

	public function prepareOptionValueForRequest($optionValue)
	{
		KLog::log('prepareOptionValueForRequest called, returning: '.var_export($optionValue, true),'info');
		return $optionValue;
	}

	public function getPrintableOptionValue($value) {
		return $this->getFormattedOptionValue($value);
	}

	/**
	 * Return the configuration text for all kinds of pages
	 *
	 * @param string $value The serialized config array
	 * @return string
	 */
	public function getFormattedOptionValue($value) {

		if (function_exists('overrideGetFormattedOptionValue')) {
			$response = overrideGetFormattedOptionValue($value);
			if ($response !== NULL) {
				return $response;
			}
		}

		KLog::log('Method started, param value is: '.var_export($value,true),'info');
		$configInfo = unserialize($value);
			
		$txt = '';

		foreach ($configInfo['items'] as $elementId=>$item) {
			$element = ConfigboxElementHelper::getElement($elementId);
			if ($element) {
				$txt .= $element->title.': '.$element->getOutputValue($item['element_option_xref_id'], $item['text']).'<br />';
			}
			else {
				KLog::log('Element ID "'.$elementId.'" mentioned in configInfo, but does not exist. Whole item is '.var_export($item, true), 'error');
			}
		}

		KLog::log('Returning: '.var_export($txt, true),'info');
		return $txt;
	}
	
}
