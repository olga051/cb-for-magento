<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Model_Product_Option extends Mage_Catalog_Model_Product_Option {

	/**
	 * This method got to return 'configbox' when parameter $type says 'configbox'. Otherwise whatever the parent
	 * model says. Exciting, no?
	 *
	 * @param string $type
	 *
	 * @return string
	 */
    public function getGroupByType($type = null) {

		if ($type == 'configbox') {
			return 'configbox';
		}
		else {
			return parent::getGroupByType($type);
		}

    }

	/**
	 * In case $type is 'configbox', return the model configbox/product_option_type_custom. Otherwise whatever the
	 * parent says.
	 *
	 * @param string $type
	 *
	 * @return Elovaris_Configbox_Model_Product_Option_Type_Custom|Mage_Catalog_Model_Product_Option_Type_Default
	 */
	public function groupFactory($type) {

		if ($type == 'configbox') {
			return Mage::getModel('configbox/product_option_type_custom');
		}
		else {
			return parent::groupFactory($type);
		}

    }

}