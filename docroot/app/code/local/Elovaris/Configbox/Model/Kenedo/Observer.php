<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Model_Kenedo_Observer extends Mage_Core_Model_Abstract {

	public function init($observer) {
		$this->initKenedo($observer);
	}

	public function initKenedo($observer) {

		// Get request parameters
		$data = $observer->getData();
		$params = $data['front']->getRequest()->getParams();

		// Re-populate $_REQUEST - muahaha
		foreach ($params as $key=>$value) {
			$_REQUEST[$key] = $value;
		}

		// Init Kenedo (now inits CB as well)
		$path = Mage::getBaseDir('app').'/code/local/Elovaris/Configbox/application/components/com_configbox/external/kenedo/helpers/init.php';
		require_once($path);
		initKenedo('com_configbox');

	}

}
