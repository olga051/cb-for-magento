<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Model_Quote extends Mage_Sales_Model_Quote {

	/**
	 * Add product to quote
	 *
	 * return error message if product type instance can't prepare product
	 *
	 * @param mixed $product
	 * @param null|float|Varien_Object $request
	 * @return Mage_Sales_Model_Quote_Item|string
	 */
	public function addProduct(Mage_Catalog_Model_Product $product, $request = null) {

		KLog::log('Method started. param $request is:'. var_export($request, true), 'info');

		$productId = $product->getId();
		$options = $product->getOptions();
		$requestData = $request->getData();
		 
		foreach($options as $option) {

			if( $option->getType() == 'configbox' ) {
				 
				$model = KenedoModel::getModel('ConfigboxModelCartposition');
				$positionId = $model->getId();

				if (!$positionId) {
					$cartModel = KenedoModel::getModel('ConfigboxModelCart');
					$cartId = $cartModel->getId();
					if (!$cartId) {
						$cartId = $cartModel->createCart();
					}
					$prepareModel = Mage::getModel('configbox/prepare');
					$cbProductId = $prepareModel->getCbProductId($productId);
					$positionId = $model->createPosition($cartId, $cbProductId);
				}

				$configuration = ConfigboxConfiguration::getInstance($positionId);

				// Now store selections in DB
				$configuration->storeSelectionsInDb();

				$configInfo = array(
						'order_id' => $positionId,
						'mage_prod_id' => $product->getId(),
						'prod_id' => $configuration->getProductId(),
						'items' => $configuration->getSelections(),
				);

				$requestData['options'] = $configInfo;
				 
				$request->setData($requestData);

				$model->editPosition(array('finished'=>1));

			}
		}
		 
		return parent::addProduct ( $product, $request );
	}

}
