<?php
/**
 * ConfigBox for Magento by Rovexo (formerly Elovaris Applications)
 * http://www.configbox.at
 *
 * @category    elovaris
 * @package     elovaris_configbox
 * @copyright   Copyright 2016 Rovexo (https://www.rovexo.com)
 * @license 	https://www.configbox.at/eula
 **/
class Elovaris_Configbox_Model_Prepare extends Mage_Core_Model_Abstract {

	static $prepared = false;

	function prepareConfigurator() {

		if (self::$prepared == true) {
			return;
		}

		self::$prepared = true;

		// Get the Magento product data
		$mageProduct = Mage::registry('current_product');
		$mageProductId = $mageProduct->getId();
		$data = $mageProduct->getData();

		// Figure out the tax percentage of the product
		$store = Mage::app()->getStore();
		$taxCalculation = Mage::getModel('tax/calculation');
		$request = $taxCalculation->getRateRequest(null, null, null, $store);
		$taxClassId = $mageProduct->getTaxClassId();
		$percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));

		// Drop it in a session, will get picked up by ConfigboxPrices
		$_SESSION['cbtaxrate'] = $percent;
		$sessionKey = 'cbproduct_base_price_'.$this->getCbProductId($mageProductId);
		$_SESSION[$sessionKey] = $data['price'];

		// Elovaris_Configbox_Model_Quote and Elovaris_Configbox_Model_Wishlist puts these
		// in the request data, this info makes ConfigBox set the position ID for editing
		// cart items
		if (!empty($data['options']['order_id']) || !empty($data['preconfigured_values'])) {

			// Data is different in Magento 1.8 and 1.9..
			if (!empty($data['preconfigured_values'])) {
				$configData = $data['preconfigured_values']->getData();
				$positionId  = $configData['options']['order_id'];
			}
			elseif (!empty($data['options']['order_id'])) {
				$positionId = $data['options']['order_id'];
			}

			// Make sure the CB position is unfinished..
			$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');
			$positionModel->setId($positionId);
			$positionModel->editPosition(array('finished'=>0));
			$position = $positionModel->getPosition();

			// ..then get the product ID of that position out
			$productId = $position->prod_id;

			// Get model and product data
			$prodModel = KenedoModel::getModel('ConfigboxModelProduct');
			$product = $prodModel->getProduct($productId);

			// Sneak prod_id into REQUEST
			KRequest::setVar('prod_id', $productId);

			// Unless a GET/POST parameter is set, use the product's first page id
			if (KRequest::getInt('page_id', 0) == 0) {
				KRequest::setVar('page_id', $product->firstPageId);
			}

		}
		// This way we can assume we deal with a new configuration
		// All we got to do is figuring out the right CB product ID and page ID
		else {

			$db = KenedoPlatform::getDb();

			// Get product and page id from the DB by the Mage product ID
			$query = "
			SELECT p.id AS product_id, page.id AS page_id
			FROM `#__configbox_products` AS p
			LEFT JOIN `#__configbox_pages` AS `page` ON page.product_id = p.id
			WHERE p.`magento_product_id` = '".intval($mageProductId)."'
			ORDER BY `page`.published ASC, page.ordering ASC";
			$db->setQuery($query);
			$info = $db->loadAssoc();

			// Sneak prod_id into REQUEST
			KRequest::setVar('prod_id', $info['product_id']);

			// Product ID
			$productId = $info['product_id'];

			// Unless a GET/POST parameter is set, use the product's first page id
			if (KRequest::getInt('page_id', 0) == 0) {
				KRequest::setVar('page_id', $info['page_id']);
			}

		}

		// Make sure we have a cart id
		$cartModel = KenedoModel::getModel('ConfigboxModelCart');
		if (!$cartModel->getId()) {
			KLog::log('No cart is set, creating new one.');
			$cartId = $cartModel->createCart();
			$cartModel->setId($cartId, true);
		}

		$positionModel = KenedoModel::getModel('ConfigboxModelCartposition');

		// If there is no position id, create a new position
		if (!$positionModel->getId()) {
			KLog::log('No position is set, creating new one.');
			$positionModel->createPosition($cartModel->getId(),$productId);
		}
		$position = $positionModel->getPosition();

		// If the position concerns another product, do a new one
		if ($position->prod_id != $productId or $position->finished) {

			if ($position->finished) {
				KLog::log('Position is finished, creating new one.');
			}
			else {
				KLog::log('A different product is selected than current position\'s product, creating new one.');
			}
			$positionModel->createPosition($cartModel->getId(),$productId);
		}

		KRequest::setVar('option','com_configbox');
		KRequest::setVar('view','configuratorpage');

	}

	function getCbProductId($magentoProductId) {

		// Get product and page id from the DB by the Mage product ID
		$db = KenedoPlatform::getDb();
		$query = "
			SELECT p.id AS product_id
			FROM `#__configbox_products` AS p
			LEFT JOIN `#__configbox_pages` AS `page` ON page.product_id = p.id
			WHERE p.`magento_product_id` = '".intval($magentoProductId)."'
			ORDER BY `page`.published ASC, page.ordering ASC";
		$db->setQuery($query);
		$id = $db->loadResult();
		return $id;
	}

}
