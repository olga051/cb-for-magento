/* global jQuery: false */
/* global com_configbox: true */
/* global tinyMCE: true */
/* global CodeMirror: true */
/* global alert: false */
/* global confirm: false */
/* global alert: false */
/* global console: false */
/* jshint -W116 */

// Here we set cbj as reference to jQuery
var cbj = jQuery.noConflict(true);

/* DEAL WITH PUSH STATE LINKS (using jQuery.address) */
cbj(document).ready(function() {
	
	// Only dealt with in backend (kind of switched on by having the plugin loaded)
	if (typeof(cbj.address) !== 'undefined') {

		// Set the base url. The plugin adds hash marks with the next URL otherwise
		cbj.address.state(location.protocol + '//' + location.host);

		// Handler for state changes
		cbj.address.change(function(event) {

			// Don't do anything on the first state change. It is fired when the page is loaded 'in the usual way' without pushState
			if (com_configbox.pageIsInited == false) {
				com_configbox.pageIsInited = true;
				return;
			}

			// Load the requested subview
			Kenedo.loadSubview(event);

		});
	}

});

cbj(document).ready(function() {

	Kenedo.initNewContent(cbj('#kenedo-page'));
	Kenedo.afterInitNewContent(cbj('#kenedo-page'));

	// Kenedo modal
	cbj('body').on('click','.kenedo-modal',function(event){

		event.preventDefault();

		var modalWidth	= cbj(this).data('modal-width');
		var modalHeight	= cbj(this).data('modal-height');

		if (!modalWidth) {
			modalWidth = 900;
		}
		if (!modalHeight) {
			modalHeight = 600;
		}

		cbj.colorbox({
			transition 		: 'fade',
			href 			: cbj(this).attr('href'),
			overlayClose 	: true,
			iframe 			: true,
			fastIframe		: false,
			width			: modalWidth,
			height			: modalHeight
		});

	});

	// New tab links
	cbj('#kenedo-page').on('click','.kenedo-new-tab, .new-tab',function(event){
		window.open(this.href);
		event.preventDefault();
	});

	/* MODAL WINDOW FOR MENU ITEM PARAMETER PICKERS - START */

	// Opening a modal window in Joomla menu item parameter boxes
	cbj('body').on('click','.trigger-picker-select',function(event){
		event.preventDefault();

		cbj.colorbox({
			transition 		: 'fade',
			href 			: cbj(this).attr('href'),
			overlayClose 	: true,
			iframe 			: true,
			fastIframe		: false,
			width			: '1200px',
			height			: '600px'
		});

	});

	// Parameter picker functionality (for menu item parameters)
	cbj('#kenedo-page').on('click','.listing-link', function(event){

		// Do not act if the link is within a param-picker view (these are views used in Joomla parameter
		// picker views, see Joomla -> Menus -> New Menu Item -> Pick a type 'Configbox' and 'Product',
		// see parameters, select one, get a modal with a list)
		if (cbj(this).closest('.param-picker').length == 0) {
			return;
		}

		event.preventDefault();

		var id = cbj(this).closest('.item-row').data('item-id');
		var title = cbj(this).text();

		var pickerObject = cbj(this).closest('.kenedo-listing-form').find('.listing-data-pickerobject').data('value');
		var pickerMethod = cbj(this).closest('.kenedo-listing-form').find('.listing-data-pickermethod').data('value');

		if (pickerMethod && typeof(parent.window[pickerMethod]) != 'undefined') {
			parent.window[pickerMethod](id);
		}

		if (parent.window.document.getElementById(pickerObject + '_id')) {
			parent.window.document.getElementById(pickerObject + '_id').value = id;
			parent.window.document.getElementById(pickerObject + '_name').value = title;
		}

		parent.window.cbj.colorbox.close();

	});

	/* MODAL WINDOW FOR MENU ITEM PARAMETER PICKERS - END */

	// Language switcher for translatables
	cbj('#kenedo-page').on('click','.property-type-translatable .language-switcher', function(){

		cbj(this).siblings().removeClass('active');
		cbj(this).addClass('active');

		var selector = cbj(this).attr('for');
		cbj('#translation-' + selector).show().siblings().hide();

	});

	// Trigger event when form content changes
	cbj('#kenedo-page').on('change','.kenedo-property :input', function(){

		var currentFieldName = cbj(this).closest('.kenedo-property').attr('id').replace('property-name-','');
		var currentFieldValue = cbj(this).val();
		var settings = Kenedo.getFormSettings();

		cbj(this).closest('form').trigger('extendedSettingsChanged',[settings, currentFieldName, currentFieldValue]);

	});

	// Toggle form field groups display
	cbj('#kenedo-page').on('click','.property-group-using-toggles>.property-group-legend', function (){

		if (cbj(this).closest('.property-group').hasClass('property-group-opened')) {
			cbj(this).closest('.property-group').removeClass('property-group-opened').addClass('property-group-closed');
			cbj(this).closest('.property-group').find('.property-group-toggle-state').val('closed');
		}
		else {
			cbj(this).closest('.property-group').removeClass('property-group-closed').addClass('property-group-opened');
			cbj(this).closest('.property-group').find('.property-group-toggle-state').val('opened');
		}

	});

	// JOIN LINKS: Show the right edit link when join selects are changed
	cbj('#kenedo-page').on('change','.join-select', function(){
		cbj(this).closest('.property-body').find('.join-link').hide();
		cbj(this).closest('.property-body').find('.join-link-' + cbj(this).val() ).show();
	});

	// Link to show the file upload input for file upload fields
	cbj('#kenedo-page').on('click','.show-file-uploader', function(event){

		event.preventDefault();

		// Store the empty input field for replacement on cancel (this makes it possible to 'reset'
		// the file upload input element when clicking cancel and preventing unwanted storage)
		var inputField = cbj(this).closest('.property-body').find('.file-upload-field').clone();
		cbj(this).data('inputfield',inputField);

		cbj(this).closest('.property-body').find('.file-uploader').slideDown(300);
		cbj(this).closest('.property-body').find('.file-current-file, .file-delete, .file-upload').slideUp(300);

	});

	// Link to hide it again
	cbj('#kenedo-page').on('click','.file-upload-cancel', function(event){

		event.preventDefault();

		cbj(this).closest('.property-body').find('.file-uploader').slideUp(300);
		cbj(this).closest('.property-body').find('.file-current-file, .file-delete, .file-upload').slideDown(300);

		// Get the saved empty input field and replace it (See handler above for reference)
		var inputField = cbj(this).closest('.property-body').find('.show-file-uploader').data('inputfield');
		cbj(this).closest('.property-body').find('.file-upload-field').replaceWith(inputField);

	});

	// Event handlers for common Kenedo form and listing tasks
	cbj('#kenedo-page').on('click', '.order-property', 										Kenedo.changeListingOrder );
	cbj('#kenedo-page').on('change','.listing-filter', 										Kenedo.changeListingFilters );
	cbj('#kenedo-page').on('click', '.link-save-item-ordering', 							Kenedo.storeOrdering );
	cbj('#kenedo-page').on('click', '.kenedo-check-all-items', 								Kenedo.toggleCheckboxes );
	cbj('#kenedo-page').on('click', '.kenedo-pagination-list a', 							Kenedo.changeListingStart );
	cbj('#kenedo-page').on('change','.kenedo-limit-select', 								Kenedo.changeListingLimit );
	cbj('#kenedo-page').on('click', '.kenedo-trigger-toggle-active', 						Kenedo.toggleActive );
	cbj('#kenedo-page').on('click', '.kenedo-details-form .kenedo-task-list .task', 		Kenedo.executeFormTask );
	cbj('#kenedo-page').on('click', '.kenedo-listing-form .kenedo-task-list .task', 		Kenedo.executeListingTask );
	cbj('#kenedo-page').on('click', '.listing-link', 										Kenedo.openListingLink );

	/* SORTABLE SETUP - START */

	// Show the save button for ordering after a change in ordering
	cbj('#kenedo-page').on('keyup','.ordering-text-field',function(){
		cbj('.link-save-item-ordering').show();
	});

	/* SORTABLE SETUP - END */


	/* EVENT HANDLERS FOR KENEDO POPUPS - START */

	// Open the popup when the trigger was entered
	cbj('body').on('mouseenter', '.kenedo-popup-trigger', KenedoPopup.cancelClosing);
	cbj('body').on('mouseenter', '.kenedo-popup-trigger', KenedoPopup.scheduleOpening);

	// Close when trigger was left
	cbj('body').on('mouseleave', '.kenedo-popup-trigger', KenedoPopup.scheduleClosing);
	cbj('body').on('mouseleave', '.kenedo-popup-trigger', KenedoPopup.cancelOpening);

	// Close when popup was left
	cbj('body').on('mouseleave', '.kenedo-popup', KenedoPopup.scheduleClosing);

	// Cancel closing when popup is entered (so also when re-entered)
	cbj('body').on('mouseenter', '.kenedo-popup', KenedoPopup.cancelClosing);

	// Prevent clicks on trigger making selections (or whatever the click would trigger)
	cbj('.kenedo-popup-trigger').click(function(event){
		event.stopPropagation();
		event.preventDefault();
		cbj(this).trigger('mouseenter');
	});

	/* EVENT HANDLERS FOR KENEDO POPUPS - END */

});

var Kenedo = {

	/**
	 *  @var {{Function[]}} subviewReadyFunctions
	 *  @see Kenedo.registerSubviewReadyFunction
	 *	@see Kenedo.runSubviewReadyFunctions
	 *	@see Kenedo.initNewContent
	 */
	subviewReadyFunctions : {},

	base64UrlEncode : function(input) {
		var encoded = Base64.encode(input);
		return encoded.replace(/\+/g,'-').replace(/\//g,'_').replace(/=/g,',');
	},

	base64UrlDecode : function(input) {
		var unreplaced = input.replace(/-/g,'+').replace(/_/g,'/').replace(/,/g,'=');
		return Base64.decode(unreplaced);
	},

	openListingLink : function(event) {

		// Links within intra-listings are handled differently: They will open in a modal window
		if (cbj(this).closest('.intra-listing').length) {

			event.preventDefault();

			// Prepare a function that resizes the modal (to be used in callbacks later on)
			var maximizeHeight = function() {
				cbj.colorbox.resize( { height:'95%' } );
			};

			var params = {
				transition 		: 'fade',
				href 			: cbj(this).attr('href'),
				overlayClose	: true,
				iframe 			: true,
				fastIframe		: false,
				width			: '1000px',
				height			: '95%',

				onComplete		: function() {

					// Maximize height on window resize
					cbj(window.top).on('resize', maximizeHeight);

					// Prevent scrolling outside the modal
					window.top.cbj('html').css('overflow','hidden');

					// FF/Win positions the modal badly if overflow:hidden is active, this works around it
					cbj(window.top).trigger('resize');

				},

				onClosed		: function() {

					// Turn off the resize handler
					cbj(window.top).off('resize', maximizeHeight);

					// Allow scrolling outside the modal again
					window.top.cbj('html').css('overflow','auto');

				}
			};

			// Add param in_modal to the URL (so that the view is loaded accordingly
			// (see KenedoController::wrapViewAndDisplay() for reference)

			if (com_configbox.platform == 'magento') {
				params.href += 'in_modal/1';
			}
			else {
				if (params.href.indexOf('?') == -1) {
					params.href += '?in_modal=1';
				}
				else {
					params.href += '&in_modal=1';
				}
			}



			cbj.colorbox(params);

			return;
		}

		// Otherwise, links open using pushState (yeah, still doing it a bit loosey goosey, but it works)
		event.preventDefault();

		var value = /address:/.test(cbj(this).attr('rel')) ? cbj(this).attr('rel').split('address:')[1].split(' ')[0] :
			cbj.address.state() !== undefined && !/^\/?$/.test(cbj.address.state()) ?
				cbj(this).attr('href').replace(new RegExp('^(.*' + cbj.address.state() + '|\\.)'), '') :
				cbj(this).attr('href').replace(/^(#!?|\.)/, '');

		cbj.address.value( value );

	},

	/**
	 * This method initializes any JS-dependent stuff on the page (use wrapper to limit the 'area' to run it)
	 * @param {jQuery} wrapper - jQuery object representing the DOM elements with stuff that needs initialization
	 */
	initNewContent : function(wrapper) {

		if (cbj(wrapper).length == 0) {
			return;
		}

		// Move all unit labels for Kenedo field type string into the text box (see fields for price in backend for reference)
		cbj(wrapper).find('.property-type-string .unit').each(function(){
			var offset = parseInt(cbj(this).clone().css('visibility','hidden').appendTo('#kenedo-page').outerWidth()) + 15;
			cbj(this).css({'position':'relative','right':offset});
			var inputWidth = parseInt(cbj(this).prev('input').css('width'));
			var inputPaddingRight = parseInt(cbj(this).prev('input').css('padding-right'));
			cbj(this).prev('input').css({'width':inputWidth - offset + inputPaddingRight, 'padding-right': offset });
			cbj('#kenedo-page>.unit').remove();
		});

		// Make all ajax-target-link links use state change (see jQuery.address)
		if (typeof(cbj.address) != 'undefined') {
			cbj('.ajax-target-link').address();
		}

		// Init the sortables
		cbj(wrapper).find('.sortable-listing tbody').sortable(Kenedo.listingSortableSettings);

		// Turn off auto-complete for ordering fields in listing
		cbj(wrapper).find('.kenedo-listing .ordering-text-field').attr('autocomplete','off');

		// Init the calendar
		cbj(wrapper).find('.datepicker').datepicker();

		// Go through all views and run the ready functions
		cbj(wrapper).find('.kenedo-view').each(function() {
			var viewId = cbj(this).attr('id');
			if (viewId) {
				Kenedo.runSubviewReadyFunctions(viewId);
			}
		});

		// Trigger form content change to fire handlers to hide and show fields on startup
		cbj(wrapper).find('.kenedo-details-form').each(function(){
			var settings = Kenedo.getFormSettings();
			cbj(this).trigger('extendedSettingsChanged',[settings, null, null]);
		});

		// Make selected drop-downs into chosen drop-downs (see jQuery.chosen)
		if (typeof(cbj.fn.chosen) != 'undefined') {

			// Init the chosen select form items
			cbj(wrapper).find('select.listing-filter, select.join-select, .property-type-dropdown select').chosen({
				disable_search_threshold : 10,
				search_contains : true,
				inherit_select_classes : true
			});

			// Deal with the width problem of initially hidden drop-downs
			cbj(wrapper).find('.chosen-container:hidden').each(function(){
				if (cbj(this).is('.join-select')) {
					cbj(this).css('width','250px');
				}
				else {
					if (cbj(this).closest('.kenedo-properties').length) {
						cbj(this).css('width','250px');
					}
					else {
						cbj(this).css('width','auto');
					}

				}
			});
		}

	},

	/**
	 * This method is supposed to run after Kenedo.initNewContent and contains the code that may be heavier. This
	 * is simply for making subview loading appear a bit quicker.
	 *
	 * @see Kenedo.initNewContent
	 * @see Kenedo.loadSubview
	 * @param {jQuery=} wrapper - jQuery object representing the DOM elements with stuff that needs initialization
	 */
	afterInitNewContent : function(wrapper) {

		if (wrapper.find('.kenedo-html-editor').length != 0 && typeof(window.tinyMCE) != 'undefined') {

			// Init HTML editors
			tinyMCE.init({
				convert_urls : false,
				document_base_url : com_configbox.urlBase,
				documentBaseURL : com_configbox.urlBase,
				baseURL : com_configbox.tinyMceBaseUrl,
				suffix : '.min',

				// General options
				mode 		: "textareas",
				selector 	: '.kenedo-html-editor',
				theme 		: "modern",
				plugins		: [
					"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
					"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
					"save table contextmenu directionality emoticons template paste textcolor"
				],
				toolbar		: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
				content_css : "",
				template_external_list_url 	: "js/template_list.js",
				external_link_list_url 		: "js/link_list.js",
				external_image_list_url 	: "js/image_list.js",
				media_external_list_url 	: "js/media_list.js"
			});

		}

	},

	listingSortableSettings : {

		handle 		: '.sort-handle',
		items		: 'tr',
		helper		: 'clone',

		start		: function() {

			// Init the current ordering arrays
			var currentOrdering = [];
			var currentItemIds = [];

			var sortableArray = cbj(this).sortable('toArray');
			sortableArray.pop();

			// Loop through the current sortable ordering and set current values
			for (var i in sortableArray) {
				if (sortableArray.hasOwnProperty(i) === true) {
					currentOrdering.push(cbj('#'+sortableArray[i] + ' .ordering-text-field').val());
					currentItemIds.push(sortableArray[i].replace('item-id-',''));
				}
			}

			// Store them in the its data object
			cbj(this).data('currentOrdering',currentOrdering);
			cbj(this).data('currentItemIds',currentItemIds);

		},

		update: function() {

			// Show the save symbol
			cbj(this).closest('.kenedo-listing-form').find('.link-save-item-ordering').show();

			// Init the arrays for new ordering values
			var updatedOrdering = [];
			var updatedItemIds = [];

			// Get current ordering from sortable's data
			var currentOrdering = cbj(this).data('currentOrdering');

			// Loop through the updated ordering and set the update arrays
			var ordering = cbj(this).sortable('toArray');

			for (var i in ordering) {
				if (ordering.hasOwnProperty(i) === true) {
					updatedItemIds.push( ordering[i].replace('item-id-','') );
					updatedOrdering.push( currentOrdering[i] );
				}
			}

			// Update the ordering text fields
			for (i in updatedOrdering) {
				if (typeof(updatedOrdering[i]) == 'string') {
					cbj(this).find('#item-id-'+updatedItemIds[i] + ' .ordering-text-field').val(currentOrdering[i]);
				}
			}

			// Renew the current arrays for next sort
			currentOrdering = updatedOrdering;
			var currentItemIds = updatedItemIds;

			// Store them in its data object
			cbj(this).data('currentOrdering',currentOrdering);
			cbj(this).data('currentItemIds',currentItemIds);

		}
	},

	requiredFields : [],
	messages : { error : [], notice : [] },
	success : false,

	reloadIntraListings : function() {
		cbj('.intra-listing').each(function(){
			var url = cbj(this).data('listing-url');
			cbj(this).load(url, function(){
				cbj(this).find('.sortable-listing tbody').sortable(Kenedo.listingSortableSettings);
			});
		});
	},

	toggleActive: function() {

		var id = cbj(this).data('id');
		var task = (cbj(this).data('active')) ? 'unpublish' : 'publish';

		cbj(this).find('.fa').addClass('fa-spinner fa-spin');

		cbj(this).closest('.kenedo-listing-form').find('.listing-data-ids').data('value',id);
		cbj(this).closest('.kenedo-listing-form').find('.listing-data-task').data('value',task);

		Kenedo.updateListingForm(cbj(this));
	},

	executeListingTask: function(event) {
		event.stopPropagation();
		var task = cbj(this).attr('class').replace('task-','').replace('task','').replace('non-ajax','').replace(' ','');

		var checkedIds = [];
		cbj(this).closest('.kenedo-listing-form').find('.kenedo-item-checkbox').each(function(){
			if (cbj(this).prop('checked') == true) {
				checkedIds.push(cbj(this).val());
			}
		});

		var ids = checkedIds.join(',');

		cbj(this).closest('.kenedo-listing-form').find('.listing-data-ids').data('value',ids);

		if (task == 'add') {

			// The URL for adding records is always stored in a data attribute, all that is set in the template for listings
			var url = Kenedo.base64UrlDecode(cbj(this).closest('.kenedo-listing-form').find('.listing-data-add-link').data('value'));

			if (cbj(this).closest('.intra-listing').length) {

				var maximizeHeight = function() {
					cbj.colorbox.resize( { height:'95%' } );
				};

				var params = {
					transition 		: 'fade',
					href 			: url,
					overlayClose 	: true,
					iframe 			: true,
					fastIframe		: false,
					width			: 1000,
					height			: '95%',

					onComplete		: function() {

						// Maximize height on window resize
						cbj(window.top).on('resize',maximizeHeight);

						// Prevent scrolling outside the modal
						window.top.cbj('html').css('overflow','hidden');

						// FF/Win positions the modal badly if overflow:hidden is active, this works around it
						cbj(window.top).trigger('resize');

					},
					onClosed		: function() {

						// Turn off the resize handler
						cbj(window.top).off('resize',maximizeHeight);

						// Allow scrolling outside the modal again
						window.top.cbj('html').css('overflow','auto');

					}

				};

				cbj.colorbox(params);
				return;
			}

			if (cbj(this).closest('.kenedo-listing-form').find('.listing-data-ajax_sub_view').data('value') == 1) {
				Kenedo.changeStateUrl(url);
			}
			else {
				window.location.href = url;
			}

		}
		else {

			cbj(this).closest('.kenedo-listing-form').find('.listing-data-task').data('value',task);
			Kenedo.updateListingForm(cbj(this));
		}

	},

	changeStateUrl: function(url) {
		url = url.replace(cbj.address.state(),'');
		cbj.address.value(url);
	},

	/**
	 * Loads a subview into the ajax target div (currently hard-coded as .configbox-ajax-target)
	 * Takes care of things like of executing the subview's loading scripts
	 * @param {(String|Event)} parameter - URL to load or event object (takes the URL from jQuery(this).attr('href') then)
	 * @param {String=} targetSelector - CSS selector for the element to put the view in
	 * @param {Function=} callbackFunction - Optional callback function
	 */
	loadSubview: function(parameter, targetSelector, callbackFunction) {

		if (!targetSelector) {
			targetSelector = '.configbox-ajax-target';
		}
		// The URL we will load eventually
		var url;

		// If used as a click handler, get the URL through cbj(this)
		if (typeof(parameter) == 'object') {

			// This is for listing links in intra-listings (i.e. lists in settings page)
			// These open in modals, we prevent subview loading on the cheap here
			if (cbj(this).closest('.intra-listing').length) {
				return;
			}

			// Get the URL from the link
			url = parameter.value;

		}

		// If used as a regular function call, get the URL from the parameter
		if (typeof(parameter) == 'string' && parameter != '') {
			url = parameter;
		}

		if (!url) {
			throw('loadSubview called, but no URL could be determined.');
		}

		// Add the ajax_sub_view parameter in case it's not there
		if (url.indexOf('ajax_sub_view') == -1) {
			if (com_configbox.platform == 'magento') {
				url += 'ajax_sub_view/1/format/raw';
			}
			else {
				url += (url.indexOf('?') == -1) ? '?ajax_sub_view=1&format=raw' : '&ajax_sub_view=1&format=raw';
			}
		}

		// Store the URL to work around the closure problem below
		Kenedo.subviewUrl = url;

		// Remove all TinyMCE editors, they will be initialized again in Kenedo.afterInitNewContent()
		// This works around a strange issue with tinyMCE when initialized editors are simply removed from the DOM
		if (window.tinyMCE) {

			// Some sites overwrite tinyMCE with a lower version, call remove only if we're on 4 or higher (3 has a
			// different function signature)
			if (window.tinyMCE.majorVersion > 3) {
				window.tinyMCE.remove();
			}

		}

		// Load the sub view with a nice fade effect

		// First fade out the current target area
		cbj(targetSelector).animate( {'opacity':'.0'}, 200, function(){

			// Then load the new content into the target area (look careful - we're injecting only the first
			// .kenedo-view element we find in the response.
			cbj(targetSelector).load(Kenedo.subviewUrl + ' .kenedo-view:first', function(responseText, textStatus, jqXHR){

				if (jqXHR.status !== 200) {

					var text = '<div id="view-error" class="kenedo-view kenedo-ajax-sub-view">';
					text += '<div class="kenedo-listing-form">';
					text += '<p>Encounted a system error: HTTP code is ' + jqXHR.status + '. Text message is: '+jqXHR.statusText+'</p>';
					text += '</div>';
					text += '</div>';
					cbj(targetSelector).html(text);

				}

				// When done, init the the new content
				Kenedo.initNewContent(cbj(targetSelector));

				// Now fade in the content
				cbj(targetSelector).animate( {'opacity':'1'}, 400, function(){

					// When done, do the rest of the initialization
					Kenedo.afterInitNewContent(cbj(targetSelector));

					if (typeof(callbackFunction) == 'function') {
						callbackFunction();
					}

				});

			});

		});

	},

	/**
	 * Registers a function to be executed when a certain subview is loaded via Kenedo.loadSubview().
	 * This is a similar concept to document.ready, but these functions are fired when Kenedo.loadSubview()
	 * injects the sub view of that name into the document.
	 * @param {String} viewId Content of the ID HTML attribute of the subview's wrapping div
	 * @param {Function} fn Ready function
	 *
	 */
	registerSubviewReadyFunction : function(viewId, fn) {

		if (typeof(Kenedo.subviewReadyFunctions[viewId]) == 'undefined') {
			Kenedo.subviewReadyFunctions[viewId] = [];
		}

		Kenedo.subviewReadyFunctions[viewId].push(fn);

	},

	/**
	 * Checks for a registered ready function and executes the function
	 * This is used to run any JS needed for the content of an ajax subview.
	 * @see Kenedo.loadSubview for reference on loading subviews
	 * @see Kenedo.initNewContent for reference on initializing content
	 * @see Kenedo.registerSubviewReadyFunction for reference on registering subview ready functions
	 * @param {String} viewId Content of the ID HTML attribute of the subview's wrapping div
	 */
	runSubviewReadyFunctions : function (viewId) {
		if (typeof(Kenedo.subviewReadyFunctions[viewId]) != 'undefined') {
			cbj.each(Kenedo.subviewReadyFunctions[viewId],function(key, fn){
				if (typeof(fn) == 'function') {
					fn();
				}
			});
		}
	},

	changeListingStart: function() {
		var start = cbj(this).attr('class').replace('page-start-','');
		cbj(this).closest('.kenedo-listing-form').find('.listing-data-limitstart').data('value',start);
		Kenedo.updateListingForm(cbj(this));
	},

	changeListingFilters: function() {
		Kenedo.updateListingForm(cbj(this));
	},

	changeListingLimit: function() {
		var limit = cbj(this).val();
		cbj(this).closest('.kenedo-listing-form').find('.listing-data-limit').data('value',limit);
		Kenedo.updateListingForm(cbj(this));
	},

	/**
	 * Update (Refresh) the listing. Depending on its location within the html doc
	 *
	 * @param {jQuery} item - Simply regard it as some element within the kenedo-listing-form
	 */
	updateListingForm: function(item) {

		if (!item) {
			item = cbj(this);
		}

		// Get the 'base' url dealing with the given type of record (see KenedoView template default-table.php)
		var url = cbj(item).closest('.kenedo-listing-form').find('.listing-data-base-url').data('value');

		// Add ? for the query string in case
		if (url.indexOf('?') == -1) {
			url += '?';
		}

		// Loop through the listing-data fields and append them to the query string
		cbj(item).closest('.kenedo-listing-form').find('.listing-data').each(function(){
			var key = cbj(this).data('key');
			// Leave out base-url, no use for it plus probably trouble
			if (key == 'base-url') {
				return;
			}
			var value = cbj(this).data('value');
			url += '&' + key + '=' + encodeURIComponent(value);
		});

		// Do the same for the filters
		cbj(item).closest('.kenedo-listing-form').find('.listing-filter').each(function(){
			var key = cbj(this).attr('name');
			var value = cbj(this).val();
			url += '&' + key + '=' + encodeURIComponent(value);
		});

		// If the link has the .non-ajax class, do a regular page load
		if (item.hasClass('non-ajax')) {
			window.location.href = url;
			return;
		}

		// Init the target div where the view should be inserted in
		var target = cbj('#kenedo-page');

		// If we're dealing with a link in an intra-listing, target is the parent intra-listing
		if (cbj(item).closest('.intra-listing').length) {
			target = cbj(item).closest('.intra-listing');
		}
		else {
			// In case we got a link within the ConfigBox target area, use that one
			if (cbj(item).closest('.configbox-ajax-target').length) {
				target = cbj(item).closest('.configbox-ajax-target');
			}
		}

		// Load the contents
		target.load(url + ' .kenedo-view:first', function() {

			// Init the new content
			Kenedo.initNewContent(target);
			Kenedo.afterInitNewContent(target);

			// Note: We do not (yet) deal with messages in listings like we do in forms because listing views are still
			// completely reloaded and carry messages with them already (as opposed to populating and showing messages
			// using JS.

			// Show messages
			if (cbj(this).find('.kenedo-messages li').length != 0) {
				cbj(this).find('.kenedo-messages').slideDown(200);
			}

			// Show error messages if any
			if (cbj(this).find('.kenedo-messages-error li').length != 0) {
				cbj(this).find('.kenedo-messages-error').slideDown(200);
			}

			// Show notices if any
			if (cbj(this).find('.kenedo-messages-notice li').length != 0) {
				cbj(this).find('.kenedo-messages-notice').slideDown(200);
			}

		});

	},

	toggleCheckboxes: function() {
		var checked = cbj(".kenedo-listing .kenedo-check-all-items").prop('checked');
		cbj(this).closest('.kenedo-listing').find('.kenedo-item-checkbox').prop('checked',checked);
	},

	storeOrdering: function() {

		if (cbj(this).hasClass('clicked')) {
			return;
		}

		cbj(this).addClass('clicked');
		cbj(this).find('.fa').removeClass('fa-floppy-o').addClass('fa-spinner').addClass('fa-spin');

		cbj(this).closest('.kenedo-listing-form').find('.listing-data-task').data('value', 'storeOrdering');

		var orderingPairs = [];
		cbj(this).closest('.kenedo-listing-form').find('.item-row').each(function(){
			var itemId = cbj(this).data('item-id');
			var order = cbj(this).find('.ordering-text-field').val();
			orderingPairs.push( '"' + itemId + '":' + order);
		});

		var string = '{' + orderingPairs.join(',') + '}';

		cbj(this).closest('.kenedo-listing-form').find('.listing-data-ordering-items').data('value', string);

		Kenedo.updateListingForm(cbj(this));
	},

	changeListingOrder: function() {

		var propertyName = cbj(this).attr('id').replace('order-property-name-','');
		var direction = '';

		if (cbj(this).hasClass('active')) {
			direction = (cbj(this).hasClass('direction-asc')) ? 'desc' : 'asc';
		}
		else {
			direction = 'asc';
		}

		cbj(this).closest('.kenedo-listing-form').find('.listing-data-listing_order_property_name').data('value', propertyName);
		cbj(this).closest('.kenedo-listing-form').find('.listing-data-listing_order_dir').data('value', direction);

		Kenedo.updateListingForm(cbj(this));
	},

	isViewInModal: function() {
		return ( cbj('#in_modal').val() === '1');
	},

	isViewAjaxView: function() {
		return ( cbj('#ajax_sub_view').val() === '1');
	},

	getReturnUrl: function() {
		return Kenedo.base64UrlDecode(cbj('#return').val());
	},

	getTask: function() {
		return cbj('#task').val();
	},

	getViewName: function() {
		return cbj('.kenedo-details-form').data('view');
	},

	/**
	 * @see Kenedo.getFormTaskHandler
	 * @see Kenedo.setFormTaskHandler
	 */
	formTaskHandlers : {},

	getFormTaskHandler: function(viewName) {
		return this.formTaskHandlers[viewName] || Kenedo.defaultFormTaskHandler;
	},

	setFormTaskHandler: function(viewName, fn) {
		this.formTaskHandlers[viewName] = fn;
	},

	/**
	 * @see Kenedo.getFormTaskResponseHandler
	 * @see Kenedo.setFormTaskResponseHandler
	 */
	formTaskResponseHandlers : {},

	getFormTaskResponseHandler: function(viewName) {
		return this.formTaskResponseHandlers[viewName] || Kenedo.defaultFormTaskResponseHandler;
	},

	setFormTaskResponseHandler: function(viewName, fn) {
		this.formTaskResponseHandlers[viewName] = fn;
	},

	/**
	 * Event handler for clicks on task buttons. Figures out what task to run, get's the right handler to run it.
	 */
	executeFormTask: function() {

		// Whoops, that is actually a listing task (when listings are within detail forms)!
		// Let's just move away before someone notices us...
		if (cbj(this).closest('.kenedo-listing-form').length) {
			return;
		}

		// Alright, looks like we're in the clear. Get the task name
		var task = cbj(this).attr('class').replace('task-','').replace('task','').replace(' ','');
		var viewName = Kenedo.getViewName();

		// Set the task, gotta be done at some point.
		cbj('#task').val(task);

		// Get the handler for tasks
		var formTaskHandler = Kenedo.getFormTaskHandler(viewName);

		// Run the handler
		formTaskHandler(viewName, task);

	},

	defaultFormTaskHandler: function(viewName, task) {

		switch (task) {

			case 'cancel':
				Kenedo.defaultFormTaskHandlerCancel(viewName);
				break;
			case 'store':
				Kenedo.defaultFormTaskHandlerStore(viewName);
				break;
			case 'apply':
				Kenedo.defaultFormTaskHandlerApply(viewName);
				break;
		}

	},

	defaultFormTaskHandlerCancel: function() {

		if (Kenedo.isViewInModal()) {
			window.parent.cbj.colorbox.close();
			return;
		}
		if (Kenedo.isViewAjaxView()) {
			Kenedo.changeStateUrl(Kenedo.getReturnUrl());
		}
		else {
			window.location.href = Kenedo.getReturnUrl();
		}

	},

	defaultFormTaskHandlerStore: function(viewName) {

		if (typeof(window.tinyMCE) != 'undefined') {
			tinyMCE.triggerSave();
		}

		var form = cbj('.kenedo-details-form');
		var xhr = new XMLHttpRequest();
		xhr.onload = Kenedo.getFormTaskResponseHandler(viewName);
		xhr.open("post", form[0].action, true);
		xhr.send(new FormData(form[0]));

	},

	defaultFormTaskHandlerApply: function(viewName) {

		if (typeof(window.tinyMCE) != 'undefined') {
			tinyMCE.triggerSave();
		}

		var form = cbj('.kenedo-details-form');
		var xhr = new XMLHttpRequest();
		xhr.onload = Kenedo.getFormTaskResponseHandler(viewName);
		xhr.open("post", form[0].action, true);
		xhr.send(new FormData(form[0]));

	},

	/**
	 * Default callback function for receiving Kenedoform submits
	 * @see Kenedo.getFormTaskResponseHandler
	 */
	defaultFormTaskResponseHandler: function() {

		var response = JSON.parse(this.responseText);
		var task = Kenedo.getTask();
		var viewName = Kenedo.getViewName();

		// On inserts, run addNewJoinRecord. It checks if we should add the new record to some join drop-down.
		if (response.wasInsert) {
			Kenedo.addNewJoinRecord(response);
		}

		cbj('#kenedo-page').trigger('kenedoFormResponseReceived', {'response': response, 'viewName': viewName, 'task': task} );

		switch (task) {

			case 'store':
				Kenedo.defaultFormTaskResponseHandlerStore(viewName, response);
				break;
			case 'apply':
				Kenedo.defaultFormTaskResponseHandlerApply(viewName, response);
				break;
		}

	},

	/**
	 *
	 * @param {String} viewName
	 * @param {JsonResponses.kenedoStoreResponse} response
	 */
	defaultFormTaskResponseHandlerStore: function(viewName, response) {

		// Show messages
		if (response.success === false) {
			Kenedo.processResponseMessages(response);
			return;
		}

		// If we're in a modal, reload any intra listings and close the modal
		if (Kenedo.isViewInModal()) {
			parent.window.Kenedo.reloadIntraListings();
			parent.window.cbj.colorbox.close();
			return;
		}

		// Redirect to the return URL
		if (Kenedo.isViewAjaxView()) {
			Kenedo.changeStateUrl(Kenedo.getReturnUrl());
		}
		else {
			window.location.href = Kenedo.getReturnUrl();
		}

	},

	/**
	 *
	 * @param {String} viewName
	 * @param {JsonResponses.kenedoStoreResponse} response
	 */
	defaultFormTaskResponseHandlerApply: function(viewName, response) {

		// Show any error or notice messages
		Kenedo.processResponseMessages(response);

		// All done in case storing didn't work out
		if (response.success === false) {
			return;
		}

		// Set all data
		if (response.data) {
			cbj.each(response.data, function(fieldName, value) {
				cbj('#'+fieldName).val(value);
			});
		}

		// If we're in a modal, reload any intra listings
		if (Kenedo.isViewInModal()) {
			parent.window.Kenedo.reloadIntraListings();
			return;
		}

		// The server's controller may set the redirectUrl (normally for changing the URL after an insert - replacing
		// the URL query string's ID value).
		if (response.redirectUrl) {

			// Redirect to the return URL
			if (Kenedo.isViewAjaxView()) {
				Kenedo.changeStateUrl(response.redirectUrl);
			}
			else {
				window.location.href = response.redirectUrl;
			}

		}

	},

	/**
	 * @param {JsonResponses.kenedoStoreResponse} response
	 */
	addNewJoinRecord: function(response) {

		// This input holds the HTML id for the drop-down (Field is set automatically by setting the form_custom_4 param in the URL)
		var joinSelectId = cbj('#form_custom_4').val();

		// Check if the id is actually there, to avoid 'misunderstandings'
		if (joinSelectId) {

			var recordId = response.data.id;

			// Get a title, simply using typical fields (consider using a form_custom field to have it specified clearly)
			var title = (response.data.name) ? response.data.name : '';

			if (!title) {
				title = (response.data.title) ? response.data.title : '';
			}
			if (!title) {
				title = 'New item';
			}

			// Finally, get the drop-down in the parent..
			var select = window.parent.cbj('#'+joinSelectId);

			// ..do a quick check, add the record, set the value
			if (select.is('select')) {
				if (select.find('option[value='+recordId+']').length == 0) {
					select.prepend('<option value="'+recordId+'">'+title+'</option>');
					select.val(recordId).change().trigger('chosen:updated');
				}
			}

		}

	},

	getFormSettings: function() {

		var values = cbj('.kenedo-property :input').serializeArray();
		var settings = [];

		cbj.each(values, function() {
			settings[this.name] = this.value;
		});

		return settings;
	},

	// Select/Deselect all for multi-selects
	selectAllOptions: function(selectbox) {
		cbj('#property-name-' + selectbox).find('option').prop('selected', true);
		cbj('#property-name-' + selectbox).find('input').prop('checked', true);
	},

	deselectAllOptions: function(selectbox) {
		cbj('#property-name-' + selectbox).find('option').prop('selected', false);
		cbj('#property-name-' + selectbox).find('input').prop('checked', false);
		cbj('#property-name-' + selectbox).focus();
	},

	/**
	 * Takes the kenedo form's response data, set's messages and displays them
	 * @param {JsonResponses.kenedoStoreResponse} response
	 */
	processResponseMessages: function(response) {

		Kenedo.clearMessages();
		if (response.success === false) {
			cbj.each(response.errors, function(i, error) {
				Kenedo.addMessage(error, 'error');
			});
		}
		else {
			cbj.each(response.messages, function(i, messages) {
				Kenedo.addMessage(messages, 'notice');
			});
		}
		Kenedo.showMessages();

	},

	/**
	 * Looks into Kenedo.messages and renders the HTML for the messages. Makes them show
	 */
	showMessages: function() {

		if (this.messages.error.length !== 0) {
			cbj('.kenedo-messages-error').html('<ul></ul>');
			cbj.each(this.messages.error, function(i, item) {
				cbj('.kenedo-messages-error ul').append('<li>'+item+'</li>');
			});
			cbj('.kenedo-messages-error:first').show();
		}

		if (this.messages.notice.length !== 0) {
			cbj('.kenedo-messages-notice').html('<ul></ul>');
			cbj.each(this.messages.notice, function(i, item) {
				cbj('.kenedo-messages-notice ul').append('<li>'+item+'</li>');
			});
			cbj('.kenedo-messages-notice:first').show();
		}

		cbj('.kenedo-messages:first').show();
	},

	addMessage: function(message, type) {

		if (typeof(this.messages[type]) == 'undefined') {
			this.messages[type] = [];
		}

		this.messages[type].push(message);

	},

	clearMessages: function() {
		cbj('.kenedo-messages').hide();
		cbj('.kenedo-messages-error').html('').hide();
		cbj('.kenedo-messages-notice').html('').hide();
		this.messages = { error : [], notice : [] };
	}

};

/* KENEDO POPUP - START */

var KenedoPopup = {

	openingDelay : 200,
	closingDelay : 200,

	openers : [],
	closers : [],

	/**
	 * Every popup gets opened by a timeout function. This method clears all opening timeouts.
	 */
	clearOpeners : function() {
		cbj.each(KenedoPopup.openers, function(i,item) {
			if (typeof(item) != 'undefined') {
				window.clearTimeout(item.timeout);
			}

		});
		KenedoPopup.openers = [];
	},

	/**
	 * Every popup gets closed by a timeout function. This method clears all closing timeouts.
	 */
	clearClosers : function() {
		cbj.each(KenedoPopup.closers, function(i,item) {
			if (typeof(item) != 'undefined') {
				window.clearTimeout(item.timeout);
			}

		});
		KenedoPopup.closers = [];
	},

	/**
	 * This method sets a timeout function that actually opens the popup.
	 * Before setting one, all existing opening timeout functions get cleared.
	 */
	scheduleOpening: function() {

		// Get the ID of the popup
		var popupId = cbj(this).attr('id').replace('kenedo-popup-trigger-', '');

		// If that popup has already got a scheduled opening, cancel it
		if (KenedoPopup.openers[popupId]) {
			window.clearTimeout(KenedoPopup.openers[popupId].timeout);
		}

		// Set the timeout, add it to the openers
		KenedoPopup.openers[popupId] = {
			popupId : popupId,
			timeout : window.setTimeout(KenedoPopup.open, KenedoPopup.openingDelay, popupId)
		};

	},

	/**
	 * This method sets a timeout function that actually closes the popup.
	 * Before setting one, all existing closing timeout functions get cleared.
	 */
	scheduleClosing: function() {

		var popupId;

		if (cbj(this).is('.kenedo-popup')) {
			popupId = cbj(this).attr('id').replace('kenedo-popup-', '');
		}
		if (cbj(this).is('.kenedo-popup-trigger')) {
			popupId = cbj(this).attr('id').replace('kenedo-popup-trigger-', '');
		}

		// If that popup has already got a scheduled closing, cancel it
		if (KenedoPopup.closers[popupId]) {
			window.clearTimeout(KenedoPopup.closers[popupId].timeout);
		}

		KenedoPopup.closers[popupId] = {
			popupId : popupId,
			timeout :  window.setTimeout(KenedoPopup.close, KenedoPopup.closingDelay, popupId)
		};

	},

	cancelOpening: function() {

		var popupId;

		if (cbj(this).is('.kenedo-popup')) {
			popupId = cbj(this).attr('id').replace('kenedo-popup-', '');
		}
		if (cbj(this).is('.kenedo-popup-trigger')) {
			popupId = cbj(this).attr('id').replace('kenedo-popup-trigger-', '');
		}

		if (KenedoPopup.openers[popupId]) {
			window.clearTimeout(KenedoPopup.openers[popupId].timeout);
		}

	},

	cancelClosing: function() {

		var popupId;

		if (cbj(this).is('.kenedo-popup')) {
			popupId = cbj(this).attr('id').replace('kenedo-popup-', '');
		}
		if (cbj(this).is('.kenedo-popup-trigger')) {
			popupId = cbj(this).attr('id').replace('kenedo-popup-trigger-', '');
		}

		if (KenedoPopup.closers[popupId]) {
			window.clearTimeout(KenedoPopup.closers[popupId].timeout);
		}

	},

	open : function( popupId ) {

		var popup = cbj('#kenedo-popup-original-' + popupId).clone(true);

		// Delete any existing popups
		cbj('#kenedo-popup-' + popupId).remove();

		popup.hide();
		popup.attr('id', 'kenedo-popup-' + popupId);
		popup.appendTo('body');

		KenedoPopup.position(popupId);

		popup.fadeIn();

		cbj('#kenedo-popup-trigger-' + popupId).trigger('popup-open');

	},

	close : function( popupId ) {

		cbj('#kenedo-popup-' + popupId).fadeOut(200, function() {
			cbj(this).remove();
		});

		cbj('#kenedo-popup-trigger-' + popupId).trigger('popup-close');

	},

	position : function( popupId ) {

		var popupTrigger = cbj('#kenedo-popup-trigger-' + popupId);
		var popup = cbj('#kenedo-popup-' + popupId);

		popup.css('visibility', 'hidden');
		popup.css('display', 'block');

		cbj('body').append(popup);

		var triggerContent = popupTrigger.find('.kenedo-popup-trigger-content');

		var triggerWidth = triggerContent.outerWidth();
		var triggerHeight = triggerContent.outerHeight();
		var triggerPos = triggerContent.offset();

		var popupWidth = popup.find('.kenedo-popup-content').outerWidth();
		var popupHeight = popup.find('.kenedo-popup-content').outerHeight();

		var content = popup.find('.kenedo-popup-content')[0];

		var arrowBoxWidth = 20;
		var arrowBoxHeight = 20;

		// Try to find the dimensions for the rotated arrow box
		try {
			arrowBoxWidth = parseInt(window.getComputedStyle(content, '::after').getPropertyValue('width'));
			arrowBoxHeight = parseInt(window.getComputedStyle(content, '::after').getPropertyValue('height'));
			if (arrowBoxWidth > 50) {
				arrowBoxWidth = arrowBoxHeight = 10;
			}
		} catch (e) {
			console.log('Cannot find out arrow box dimensions.');
		}

		var arrowHeight = Math.sqrt(arrowBoxWidth * arrowBoxWidth + arrowBoxHeight * arrowBoxHeight) / 2;

		popup.outerWidth(popupWidth);
		popup.outerHeight(popupHeight + arrowHeight);

		var viewPortWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		var scrollTop = cbj('body').scrollTop();
		var scrollLeft = cbj('body').scrollLeft();


		var popupLeftEdge = triggerPos.left + triggerWidth / 2 - popupWidth / 2;

		var topPosTopEdge = triggerPos.top - popupHeight - arrowHeight;

		var bottomPosTopEdge = triggerPos.top + triggerHeight + arrowHeight;
		var bottomPosBottomEdge = bottomPosTopEdge + popupHeight;

		var newLeft, delta;

		// If popup goes over the viewport left edge, adjust
		if (popupLeftEdge + scrollLeft < 0) {

			newLeft = 10 + scrollLeft;
			delta = popupLeftEdge - newLeft;

			if (typeof(document.styleSheets[0].addRule) != 'undefined') {
				document.styleSheets[0].addRule('.kenedo-popup-content:before', 'left: ' + delta + 'px');
				document.styleSheets[0].addRule('.kenedo-popup-content:after', 'left: ' + delta + 'px');
			}
			else {
				var styleEl = document.createElement('style');
				document.head.appendChild(styleEl);
				styleEl.sheet.insertRule('.kenedo-popup-content:before {left:'+ delta +'px;}', styleEl.sheet.cssRules.length);
				styleEl.sheet.insertRule('.kenedo-popup-content:after {left:'+ delta +'px;}', styleEl.sheet.cssRules.length);
			}


			popupLeftEdge = newLeft;

		}
		else {
			if (typeof(document.styleSheets[0].addRule) != 'undefined') {
				document.styleSheets[0].addRule('.kenedo-popup-content:before', 'left: auto');
				document.styleSheets[0].addRule('.kenedo-popup-content:after', 'left: auto');
			}
			else {
				var styleEl1 = document.createElement('style');
				document.head.appendChild(styleEl1);
				styleEl1.sheet.insertRule('.kenedo-popup-content:before {left:auto;}', styleEl1.sheet.cssRules.length);
				styleEl1.sheet.insertRule('.kenedo-popup-content:after {left:auto;}', styleEl1.sheet.cssRules.length);
			}
		}

		var popupRightEdge = popupWidth + popupLeftEdge;

		// If popup goes over the viewport right edge, adjust
		if (popupRightEdge > viewPortWidth) {

			newLeft = viewPortWidth - popupWidth - 10;
			delta = popupLeftEdge - newLeft;

			if (typeof(document.styleSheets[0].addRule) != 'undefined') {
				document.styleSheets[0].addRule('.kenedo-popup-content:before','left: ' + delta + 'px');
				document.styleSheets[0].addRule('.kenedo-popup-content:after','left: ' + delta + 'px');
			}
			else {
				var styleEl2 = document.createElement('style');
				document.head.appendChild(styleEl2);
				styleEl2.sheet.insertRule('.kenedo-popup-content:before {left:' + delta + 'px;}', styleEl2.sheet.cssRules.length);
				styleEl2.sheet.insertRule('.kenedo-popup-content:after {left:' + delta + 'px;}', styleEl2.sheet.cssRules.length);
			}

			popupLeftEdge = newLeft;

		}

		var fitsTop = topPosTopEdge > scrollTop;
		var fitsBottom = bottomPosBottomEdge < viewPortHeight + scrollTop;

		// Figure out if popup should show on top or bottom of the trigger
		var showOnTop = null;

		// With class prefer-top and if it fits, to top
		if (popup.hasClass('position-prefer-top') && fitsTop) {
			showOnTop = true;
		}
		// Vice versa
		if (popup.hasClass('position-prefer-bottom') && fitsBottom) {
			showOnTop = false;
		}
		// Preference doesn't go, so use top if it filts, bottom otherwise
		if (showOnTop === null) {
			showOnTop = fitsTop;
		}

		if (showOnTop) {
			popup.addClass('position-top').removeClass('position-bottom');
			popup.offset({left:popupLeftEdge, top:topPosTopEdge});
		}
		else {
			popup.addClass('position-bottom').removeClass('position-top');
			popup.offset({left:popupLeftEdge, top:bottomPosTopEdge});
		}

		popup.css('display', 'none');
		popup.css('visibility', 'visible');

		return popup;

	}

};

/* KENEDO POPUP - END */


/**
 *
 *  Base64 encode / decode
 *  http://www.webtoolkit.info/
 *
 **/


var Base64 = {

	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	// public method for encoding
	encode : function (input) {
		/* jshint -W016 */
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;

		input = Base64._utf8_encode(input);

		while (i < input.length) {

			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output +
				this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
				this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

		}

		return output;
	},

	// public method for decoding
	decode : function (input) {
		/* jshint -W016 */
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/=]/g, "");

		while (i < input.length) {

			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = Base64._utf8_decode(output);

		return output;

	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		/* jshint -W016 */
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		/* jshint -W016 */
		var string = "";
		var i = 0;
		var c = 0;
		var c2 = 0;
		var c3 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}

};

/*!
 * dragtable - jquery ui widget to re-order table columns
 * version 3.0
 *
 * Copyright (c) 2010, Jesse Baird <jebaird@gmail.com>
 * 12/2/2010
 * https://github.com/jebaird/dragtable
 *
 * Minor things fixed by Martin R.
 *
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 *
 *
 * Forked from https://github.com/akottr/dragtable - Andres Koetter akottr@gmail.com
 *
 *
 *
 *
 * quick down and and dirty on how this works
 * ###########################################
 * so when a column is selected we grab all of the cells in that row and clone append them to a semi copy of the parent table and the
 * "real" cells get a place holder class witch is removed when the dragstop event is triggered
 *
 *
 * make it easy to have a button swap columns
 *
 *
 * Events - in order of trigger
 * start - when the user mouses down on handle or th, use in favor of display helper
 * beforeChange - called when a col will be moved
 * change - called after the col has been moved
 * stop - the user mouses up and stops dragging and the drag display is removed from the dom
 *
 *
 *
 *
 * IE notes
 *  ie8 in quirks mode will only drag once after that the events are lost
 *
 */

(function($) {
	$.widget("jb.dragtable", {
		//TODO: implement this
		eventWidgetPrefix: 'dragtable',
		options: {
			//used to the col headers, data contained in here is used to set / get the name of the col
			dataHeader:'data-header',
			//class name that handles have
			handle:'dragtable-drag-handle',
			//draggable items in cols, .dragtable-drag-handle has to match the handle options
			items: 'th:not( :has( .dragtable-drag-handle ) ), .dragtable-drag-handle',
			//if a col header as this class, cols cant be dragged past it
			boundary: 'dragtable-drag-boundary',
			//classnames that get applied to the real td, th
			placeholder: 'dragtable-col-placeholder',

			/*
			 the drag display will be appended to this element,
			 if this is set to  document.body and has been zeroed off the display will seem to jump

			 Its either : parent or a dom node
			 */
			appendTarget: ':parent',
			//if true,this will scroll the appendTarget offsetParent when the dragDisplay is dragged past its boundaries
			scroll: false

		},
		// when a col is dragged use this to find the semantic elements, for speed
		tableElemIndex:{
			head: '0',
			body: '1',
			foot: '2'
		},
		tbodyRegex: /(tbody|TBODY)/,
		theadRegex: /(thead|THEAD)/,
		tfootRegex: /(tfoot|TFOOT)/,

		_create: function() {

			//console.log(this);
			//used start/end of drag
			this.startIndex = null;
			this.endIndex = null;
			//the references to the table cells that are getting dragged
			this.currentColumnCollection = [];
			//the references the position of the first element in the currentColunmCollection position
			this.currentColumnCollectionOffset = {};
			//the div wrapping the drag display table
			this.dragDisplay = $([]);


			var self = this,
				o = self.options,
				el = self.element;

			o.appendTarget = ( o.appendTarget === ':parent' ) ? this.element.parent() : $( o.appendTarget );

			//grab the ths and the handles and bind them
			el.delegate(o.items, 'mousedown.' + self.widgetEventPrefix, function(e){

				var $handle = $(this),
					elementOffsetTop = self.element.position().top;

				//make sure we are working with a th instead of a handle
				if( $handle.hasClass( o.handle ) ){

					$handle = $handle.closest('th');
					//change the target to the th, so the handler can pick up the offsetleft
					e.currentTarget = $handle.closest('th')[0];
				}


				self.getCol( $handle.index() )
					.attr( 'tabindex', -1 )
					.focus()
					.disableSelection()
					.css({
						top: elementOffsetTop,
						//need to account for the scroll left of the append target, other wise the display will be off by that many pix
						left: ( self.currentColumnCollectionOffset.left + o.appendTarget[0].scrollLeft )
					})
					.appendTo( o.appendTarget );



				self._mousemoveHandler( e );
				//############
			});

		},

		/*
		 * e.currentTarget is used for figuring out offsetLeft
		 * getCol must be called before this is
		 *
		 */
		_mousemoveHandler: function( e ){
			//call this first, catch any drag display issues
			this._start( e );

			var self = this,
				o = self.options,
				prevMouseX = e.pageX,
				dragDisplayWidth = self.dragDisplay.outerWidth(),
				halfDragDisplayWidth = dragDisplayWidth / 2,
				//appendTargetOP = o.appendTarget.offsetParent()[0],
				scroll = o.scroll,
				// Get the col count, used to contain col swap
				colCount = self.element[ 0 ]
						.getElementsByTagName( 'thead' )[ 0 ]
						.getElementsByTagName( 'tr' )[ 0 ]
						.getElementsByTagName( 'th' )
						.length - 1;

			$( document ).bind('mousemove.' + self.widgetEventPrefix, function( e ){
				var columnPos = self._setCurrentColumnCollectionOffset(),
					mouseXDiff = e.pageX - prevMouseX,
					appendTarget = o.appendTarget[0],
					left =  ( parseInt( self.dragDisplay[0].style.left ) + mouseXDiff  );
				self.dragDisplay.css( 'left', left );

				/*
				 * when moving left and e.pageX and prevMouseX are the same it will trigger right when moving left
				 *
				 * it should only swap cols when the col dragging is half over the prev/next col
				 */
				var threshold, scrollLeft;

				if( e.pageX  < prevMouseX ){
					//move left
					threshold = columnPos.left - halfDragDisplayWidth;


					//scroll left
					if( left < ( appendTarget.clientWidth  - dragDisplayWidth ) && scroll == true ) {
						scrollLeft =  appendTarget.scrollLeft + mouseXDiff;
						/*
						 * firefox does scroll the body with target being body but chome does
						 */
						if( appendTarget.tagName == 'BODY' ) {
							window.scroll( window.scrollX + scrollLeft, window.scrollY );
						} else {
							appendTarget.scrollLeft = scrollLeft;
						}

					}


					if( left  < threshold ){
						self._swapCol(self.startIndex-1);
					}

				}else{
					//move right
					threshold = columnPos.left + halfDragDisplayWidth ;

					//scroll right
					if( left > (appendTarget.clientWidth - dragDisplayWidth ) && scroll == true ) {
						//console.log(  o.appendTarget[0].clientWidth + (e.pageX - prevMouseX))

						scrollLeft = appendTarget.scrollLeft + mouseXDiff;
						/*
						 * firefox does scroll the body with target being body but chome does
						 */
						if( appendTarget.tagName == 'BODY' ) {
							window.scroll( window.scrollX + scrollLeft, window.scrollY );
						} else {
							appendTarget.scrollLeft = scrollLeft;
						}

					}

					//move to the right only if x is greater than threshold and the current col isn' the last one
					if( left  > threshold  && colCount != self.startIndex ){
						self._swapCol( self.startIndex + 1 );
					}
				}
				//update mouse position
				prevMouseX = e.pageX;

			})
				.one( 'mouseup.' + self.widgetEventPrefix ,function(e ){
					self._stop( e );
				});

		},

		_start: function( e ){

			$( document )
			//move disableselection and cursor to default handlers of the start event
				.disableSelection()
				.css( 'cursor', 'move');

			// guess the width of the column that is getting dragged and apply it to the drag display. fixes issues with
			// cols / tables having fixed widths
			this.dragDisplay.width( this.currentColumnCollection[0].clientWidth );

			return this._eventHelper('start',e);

		},
		_stop: function( e ){

			// issue #25 reorder the stop event order always remove the stop event
			$( document )
				.unbind( 'mousemove.' + this.widgetEventPrefix )
				.enableSelection()
				.css( 'cursor', '');

			// clean up
			this
				.dropCol()
				.dragDisplay.remove();
			// let the world know we have stopped
			this._eventHelper('stop',e,{});


		},

		_setOption: function(option, value) {
			$.Widget.prototype._setOption.apply( this, arguments );

		},

		/*
		 * get the selected index cell out of table row
		 * needs to work as fast as possible. and performance gains in this method are worth the time
		 *  because its used to build the drag display and get the cells on col swap
		 * http://jsperf.com/binary-regex-vs-string-equality/4
		 */
		_getCells: function( elem, index ){
			//console.time('getcells');
			var td,
				parentNodeName,
				ei = this.tableElemIndex,
			//TODO: clean up this format
				tds = {
					//store where the cells came from
					'semantic':{
						'0': [],//head throws error if ei.head or ei['head']
						'1': [],//body
						'2': []//footer
					},
					//keep a ref in a flat array for easy access
					'array':[]
				},
				//cache regex, reduces looking up the chain
				theadRegex = this.theadRegex,
				tbodyRegex = this.tbodyRegex,
				//tfootRegex = this.tfootRegex,


				tdsSemanticHead = tds.semantic[ei.head],
				tdsSemanticBody = tds.semantic[ei.body],
				tdsSemanticFoot = tds.semantic[ei.foot];

			//console.log(index);
			//check does this col exsist
			if(index <= -1 || typeof elem.rows[0].cells[index] == 'undefined'){
				return tds;
			}

			for(var i = 0, length = elem.rows.length; i < length; i++){

				td = elem.rows[i].cells[index];

				//if the row has no cells dont error out;
				if( td == undefined ){
					continue;
				}

				parentNodeName = td.parentNode.parentNode.nodeName;
				tds.array.push(td);
				//faster to leave out ^ and $ in the regular expression
				if( tbodyRegex.test( parentNodeName ) ){

					tdsSemanticBody.push( td );

				}else if( theadRegex.test( parentNodeName ) ){

					tdsSemanticHead.push( td );

				}else if( this.tfootRegex.test( parentNodeName ) ){

					tdsSemanticFoot.push( td );
				}


			}

			return tds;
		},
		/*
		 * returns all element attrs in a string key="value" key2="value"
		 */
		_getElementAttributes: function(element){

			var attrsString = [],
				attrs = element.attributes,
				i = 0,
				length = attrs.length;

			for( ; i < length; i++) {
				attrsString.push( attrs[i].nodeName + '="' + attrs[i].value+'"' );
			}
			return attrsString.join(' ');
		},

		/*
		 * faster than swap nodes
		 * only works if a b parent are the same, works great for columns
		 */
		_swapCells: function(a, b) {
			a.parentNode.insertBefore(b, a);
		},

		/*
		 * used to trigger optional events
		 */
		_eventHelper: function(eventName ,eventObj, additionalData){
			return this._trigger(
				eventName,
				eventObj,
				$.extend({
					column: this.currentColumnCollection,
					order: this.order(),
					startIndex: this.startIndex,
					endIndex: this.endIndex,
					dragDisplay: this.dragDisplay,
					columnOffset: this.currentColumnCollectionOffset
				},additionalData)
			);
		},
		/*
		 * build copy of table and attach the selected col to it, also removes the select col out of the table
		 * @returns copy of table with the selected col
		 *
		 * populates self.dragDisplay
		 *
		 */
		getCol: function(index){
			//console.log('index of col '+index);
			//drag display is just simple html

			var target,
				cells,
				clone,
				tr,
				i,
				length,
				$table = this.element,
				self = this,
				//eIndex = self.tableElemIndex,
				placholderClassnames = ' ' + this.options.placeholder;

			//BUG: IE thinks that this table is disabled, dont know how that happend
			self.dragDisplay = $('<table '+self._getElementAttributes($table[0])+'></table>')
				.addClass('dragtable-drag-col');

			//start and end are the same to start out with
			self.startIndex = self.endIndex = index;


			cells = self._getCells($table[0], index);
			self.currentColumnCollection = cells.array;

			//################################

			$.each(cells.semantic,function(k,collection){
				//dont bother processing if there is nothing here

				if(collection.length == 0){
					return;
				}

				if ( k == '0' ){
					target = document.createElement('thead');
					self.dragDisplay[0].appendChild(target);

				}else if ( k == 1 ) {
					target = document.createElement('tbody');
					self.dragDisplay[0].appendChild(target);

				}else {
					target = document.createElement('tfoot');
					self.dragDisplay[0].appendChild(target);
				}

				for(i = 0,length = collection.length; i < length; i++){

					clone = collection[i].cloneNode(true);
					collection[i].className+=placholderClassnames;
					tr = document.createElement('tr');
					tr.appendChild(clone);

					target.appendChild(tr);

				}
			});


			this._setCurrentColumnCollectionOffset();


			self.dragDisplay  = $('<div class="dragtable-drag-wrapper"></div>').append(self.dragDisplay);
			return self.dragDisplay;
		},


		_setCurrentColumnCollectionOffset: function(){
			this.currentColumnCollectionOffset = $( this.currentColumnCollection[0] ).position();
			return this.currentColumnCollectionOffset;
		},

		/*
		 * move column left or right
		 */
		_swapCol: function( to ){

			//cant swap if same position
			if(to == this.startIndex){
				return false;
			}

			var from = this.startIndex;
			this.endIndex = to;
			//this col cant be moved past me
			var th = this.element.find('th').eq( to );
			//check on th
			if( th.hasClass( this.options.boundary ) == true ){
				return false;
			}
			//check handle element
			if( th.find( '.' + this.options.handle ).hasClass( this.options.boundary ) == true ){
				return false;
			}

			if( this._eventHelper('beforeChange',{}) === false ){
				return false;
			}

			var row2;
			if(from < to) {
				//console.log('move right');
				for(var i = from; i < to; i++) {
					row2 = this._getCells(this.element[0],i+1);
					//  console.log(row2)
					for(var j = 0, length = row2.array.length; j < length; j++){
						this._swapCells(this.currentColumnCollection[j],row2.array[j]);
					}
				}
			} else {
				//console.log('move left');
				for(var i1 = from; i1 > to; i1--) {
					row2 = this._getCells(this.element[0],i1-1);
					for(var j1 = 0, length1 = row2.array.length; j1 < length1; j1++){
						this._swapCells(row2.array[j1],this.currentColumnCollection[j1]);
					}
				}
			}
			this._eventHelper('change',{});

			this.startIndex = this.endIndex;
		},
		/*
		 * called when drag start is finished
		 */
		dropCol: function(){
			//TODO: cache this when the option is set
			var regex = new RegExp("(?:^|\\s)" + this.options.placeholder + "(?!\\S)",'g');
			//remove placeholder class
			//dont use jquery.fn.removeClass for performance reasons
			for(var i = 0, length = this.currentColumnCollection.length; i < length; i++){
				var td = this.currentColumnCollection[i];

				td.className = td.className.replace(regex,'');
			}

			return this;

		},
		/*
		 * get / set the current order of the cols
		 */
		order: function(order){
			var self = this,
				elem = self.element,
				options = self.options,
				headers = elem.find('thead tr:first').children('th');


			if(order == undefined){
				//get
				var ret = [];
				headers.each(function(){
					var header = this.getAttribute(options.dataHeader);
					if(header == null){
						//the attr is missing so grab the text and use that
						header = $(this).text();
					}

					ret.push(header);

				});

				return ret;

			}else{
				//set
				//headers and order have to match up
				if(order.length != headers.length){

					return self;
				}
				for(var i = 0, length = order.length; i < length; i++){

					var start = headers.filter('['+ options.dataHeader +'='+ order[i] +']').index();
					if(start != -1){

						self.startIndex = start;

						self.currentColumnCollection = self._getCells(self.element[0], start).array;

						self._swapCol(i);
					}


				}
				return self;
			}
		},

		destroy: function() {
			var self = this,
				o = self.options;

			this.element.undelegate( o.items, 'mousedown.' + self.widgetEventPrefix );

			$( document ).unbind('.' + self.widgetEventPrefix );

		}


	});

})(cbj);
