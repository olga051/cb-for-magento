/**
 * @module configbox/shapediver
 */
define(['cbj', 'configbox/configurator'], function(cbj, configurator) {

	"use strict";

	/**
	 * @exports configbox/shapediver
	 */
	var module = {

		/**
		 * This one holds the callback functions for ongoing requests
		 */
		callbackFunctions: {},

		/**
		 * Holds timeouts for timeout function of ongoing requests
		 */
		timeouts: {},

		parameterDefs : {},

		messageListenerAdded: false,

		initShapeDiverVis: function() {

			var iframe = module.getIframe();

			cbj(window).on('resize', module.setIframeHeight);

			module.setIframeHeight();

			if (iframe.readyState == 'complete') {
				module.onIframeReady();
			}
			else {
				cbj('#shapediver-vis').on('load', module.onIframeReady);
			}

			module.addMessageListener();

		},

		setIframeHeight: function() {

			var iframe = module.getIframe();

			var relativeHeight = cbj(iframe).data('relative-height');
			if (relativeHeight) {
				var width = parseInt(cbj(iframe).css('width'));
				var height = parseInt(width * relativeHeight / 100);
				cbj(iframe).css('height', height + 'px');
			}

		},

		addMessageListener: function() {

			if (module.messageListenerAdded !== true) {
				module.messageListenerAdded = true;
				cbj(window).on('message', this.receiveMessage);
			}
		},

		onIframeReady: function() {

			// This is the handler that fires when a selection change happens
			cbj(document).on('cbSelectionChange', function(event, questionId, selection) {

				var isControl = configurator.getQuestionPropValue(questionId, 'is_shapediver_control');

				if (isControl == 0) {
					return;
				}

				var parameterId = configurator.getQuestionPropValue(questionId, 'shapediver_parameter_id');

				if (parameterId == '') {
					return;
				}

				var type = configurator.getQuestionPropValue(questionId, 'question_type');

				// Translate color code
				if (type == 'colorpicker') {
					selection = '0x' + selection.replace('#', '') + 'ff';
				}

				if (type == 'upload') {
					// selection = JSON.parse(selection).url;
					selection = cbj('#question-' + questionId).data('file-contents');
				}

				var answers = configurator.getQuestionPropValue(questionId, 'answers');

				if (answers && typeof(answers[selection]) !== 'undefined') {
					var answer = answers[selection];
					selection = answer.shapediver_choice_value;
				}

				module.setParameterValue(parameterId, selection);

			});

		},

		setParameterValue: function(parameterId, value) {

			var payload = {
				command: "setParameterValue",
				arguments: [parameterId, value]
			};

			module.sendMessage(payload);

		},

		/**
		 * Method to send messages to our ShapeDiver iframe with a callback function that
		 * processes the response.
		 *
		 * @param payload
		 * @param {String=} iframeId
		 * @param {Function=} callbackFunction - Optional callback for the response
		 * @param {Function=} timeoutFunction - Optional callback if timeout for the response expired
		 */
		sendMessage: function(payload, iframeId, callbackFunction, timeoutFunction) {

			// Add the measage listener (method method prevents multiple adds)
			module.addMessageListener();

			// We just invent some id
			// var callbackId = Math.random();

			// Until we got payload IDs, we use the command name and hope for the best
			var callbackId = payload.command;

			// If we have a callback function, then we add the callbackId to the payload
			if (callbackFunction) {

				// Here we attach it so that hopefully we get it back in the response
				payload.callbackId = callbackId;

				// Here we store the callback function, so that our receiveMessage can find it
				module.callbackFunctions[callbackId] = callbackFunction;

			}

			// Start a timeout for the timeout function (if there is one)
			if (timeoutFunction) {
				module.timeouts[callbackId] = window.setTimeout(timeoutFunction, 3000);
			}

			// Get the iframe..
			var iframe = module.getIframe(iframeId);

			// ..and off it goes
			iframe.contentWindow.postMessage(payload, "https://www.shapediver.com");

		},

		/**
		 * Listener for incoming messages
		 * @param event
		 */
		receiveMessage: function(event) {

			// Get the message data
			var data = event.data || event.originalEvent.data;

			// Until we got payload IDs, we use the command name and hope for the best
			data.callbackId = data.command || null;

			// See if we got a callback id
			if (data.callbackId) {

				// If so, see if we actually got a callback function for this id
				if (typeof(module.callbackFunctions[data.callbackId]) === 'function') {
					// Call it and remove the callback id
					module.callbackFunctions[data.callbackId](data);
					delete module.callbackFunctions[data.callbackId];
				}

				// Clear timeouts
				if (typeof(module.timeouts[data.callbackId]) !== 'undefined') {
					window.clearTimeout(module.timeouts[data.callbackId]);
					delete module.timeouts[data.callbackId];
				}

			}

		},

		/**
		 *
		 * @param {String=} [id=shapediver-vis] id
		 * @returns {*}
		 */
		getIframe: function(id) {

			if (!id) {
				id = 'shapediver-vis';
			}

			var iframe = window.document.getElementById(id);

			if (iframe) {
				return iframe;
			}
			else {
				return null;
			}

		}

	};

	return module;

});