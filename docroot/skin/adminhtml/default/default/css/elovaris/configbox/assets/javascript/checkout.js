/* jshint -W116 */

/**
 * @module configbox/checkout
 */
define(['cbj'], function(cbj) {

	"use strict";

	/**
	 * @exports configbox/checkout
	 */
	var module = {

		initCheckoutPage: function() {

			cbj(document).on('click', '.trigger-show-order-address-form', function() {
				cbj('.wrapper-order-address .order-address-display').slideUp();
				cbj('.wrapper-order-address .order-address-form').slideDown();
			});

			cbj(document).on('click', '.trigger-save-order-address', function() {

				// Add the spinner to the button
				cbj(this).addClass('processing');

				cbrequire(['configbox/customerform', 'configbox/server'], function(customerform, server) {

					// Get the customer data from the customer form
					var customerData = customerform.getCustomerFormData();

					server.storeOrderAddress(customerData)

						.done(function(response) {

							// Remove the spinner
							cbj('.trigger-save-order-address').removeClass('processing');

							// Show validation issues (or errors)
							if (response.success === false) {
								if (response.validationIssues.length) {
									customerform.displayValidationIssues(response.validationIssues);
									return;
								}
								else {
									window.alert(response.errors.join("\n"));
									return;
								}
							}

							// Remove all invalid classes from fields
							cbj('.order-address-field').removeClass('invalid').removeClass('valid');

							// Refresh the rest of the checkout sub-views
							module.refreshOrderAddress();
							module.refreshCartSummary();
							module.refreshDeliveryOptions();
							module.refreshPaymentOptions();
							module.refreshOrderRecord();

						});

				});

			});

			// Store delivery method
			cbj(document).on('change', '#subview-delivery .option-control', function() {

				cbj(this).closest('li').addClass('processing');
				var id = cbj(this).val();

				cbrequire(['configbox/server'], function(server) {
					server.setDeliveryOption(id)
						.done(function() {
							// Refresh the sub views
							module.refreshPaymentOptions();
							module.refreshOrderRecord();
							cbj('#subview-delivery li.processing').removeClass('processing');
						});
				});

			});

			// Store payment method
			cbj(document).on('change', '#subview-payment .option-control', function() {

				cbj(this).closest('li').addClass('processing');
				var id = cbj(this).val();

				cbrequire(['configbox/server'], function(server) {
					server.setPaymentOption(id)
						.done(function() {
							// Refresh the sub views
							module.refreshOrderRecord();
							cbj('#subview-payment li.processing').removeClass('processing');
						});
				});

			});

			cbj(document).on('click', '.trigger-show-terms', function() {
				cbrequire(['cbj.bootstrap'], function() {
					cbj('#modal-terms').modal();
				});
			});

			cbj(document).on('click', '.trigger-show-refund-policy', function() {
				cbrequire(['cbj.bootstrap'], function() {
					cbj('#modal-refund-policy').modal();
				});
			});

			cbj(document).on('click', '.trigger-place-order', function() {

				// Don't process if clicked already
				if (cbj(this).hasClass('processing') === true) {
					return;
				}
				// Add the css class flag
				cbj(this).addClass('processing');

				var termsAgreementMissing = false;
				var refundsAgreementMissing = false;

				var agreeTermsRequired = cbj('.view-checkout').data('agree-to-terms');
				var agreeRpRequired = cbj('.view-checkout').data('agree-to-rp');
				var textAgreeToTerms = cbj('.view-checkout').data('text-agree-terms');
				var textAgreeToRp = cbj('.view-checkout').data('text-agree-rp');
				var textAgreeToBoth = cbj('.view-checkout').data('text-agree-both');

				if (agreeTermsRequired) {
					var agreed = cbj('#agreement-terms').prop('checked');
					if (agreed === false) {
						termsAgreementMissing = true;
					}
				}
				if (agreeRpRequired) {
					var agreedRp = cbj('#agreement-refund-policy').prop('checked');
					if (agreedRp === false) {
						refundsAgreementMissing = true;
					}
				}

				var agreementsMissingMessage = '';

				// If both terms and refund policy missing
				if (termsAgreementMissing && refundsAgreementMissing) {
					agreementsMissingMessage = textAgreeToBoth;
				}
				// If the terms missing
				if (termsAgreementMissing && !refundsAgreementMissing) {
					agreementsMissingMessage = textAgreeToTerms;
				}
				// If the refund policy missing
				if (!termsAgreementMissing && refundsAgreementMissing) {
					agreementsMissingMessage = textAgreeToRp;
				}

				// Show and bounce if agreements are missing
				if (agreementsMissingMessage) {
					cbj(this).removeClass('processing');
					window.alert(agreementsMissingMessage);
					return false;
				}

				cbrequire(['configbox/server'], function(server) {

					// Place the order (set's the status to 'ordered');
					server.placeOrder()
						.done(function(response){

							if (response.success === false) {
								cbj('.trigger-place-order').removeClass('processing');
								window.alert(response.errors.join("\n"));
							}
							else {
								// Then load the PSP bridge and get going with the payment
								var url = cbj('.view-checkout').data('url-psp-view');
								cbj('.wrapper-psp-bridge').load(url, function(){
									cbj('.trigger-redirect-to-psp').trigger('click');
								});
							}
						});

				});

			});

			// Make sure firing click events on the .trigger-redirect-to-psp link redirect (apparantly there are cases where
			// it's not the case). After order placement the system triggers that click.
			cbj(document).on('click', '.trigger-redirect-to-psp', function(){
				if (typeof(cbj(this).attr('href')) !== 'undefined') {
					window.location = cbj(this).attr('href');
				}
				else {
					cbj(this).closest('form').submit();
				}
			});

		},

		refreshOrderAddress : function() {

			var url = cbj('.view-checkout').data('url-address-view');

			// Refresh the order address display and toggle
			cbj('.wrapper-order-address .order-address-display').load(url, function() {
				cbj('.wrapper-order-address .order-address-display').slideDown();
				cbj('.wrapper-order-address .order-address-form').slideUp();
			});
		},

		refreshCartSummary: function() {
			var url = cbj('.view-cart').data('url-cart-summary');

			if (url) {
				cbj('.wrapper-cart-summary').load(url);
			}
		},

		refreshDeliveryOptions : function() {
			var url = cbj('.view-checkout').data('url-delivery-view');
			cbj('.wrapper-delivery-options').load(url);
		},

		refreshPaymentOptions : function() {
			var url = cbj('.view-checkout').data('url-payment-view');
			cbj('.wrapper-payment-options').load(url);
		},

		refreshOrderRecord : function() {
			var url = cbj('.view-checkout').data('url-order-view');
			cbj('.wrapper-order-record').load(url);
		}

	};

	return module;

});